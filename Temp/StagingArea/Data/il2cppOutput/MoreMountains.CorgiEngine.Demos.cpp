﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`2<UnityEngine.EventSystems.PointerEventData/FramePressState,UnityEngine.EventSystems.PointerEventData>
struct Action_2_t8B45679D16F91472B6F77929A5866C062D119BD7;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule>
struct List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character>
struct List_1_t5DB922CEE6338D263668FE75E45C8566802F493E;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.CheckPoint>
struct List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716;
// System.Collections.Generic.List`1<UnityEngine.Collider2D>
struct List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem>
struct List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.GravityPoint>
struct List_1_t9665259F2DD3EB069F13678BBA381B9041367C51;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// System.Collections.Generic.List`1<MoreMountains.Feedbacks.MMFeedback>
struct List_1_t65DB3FDD9079C29778530442558DE4285EB4A794;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointOfEntry>
struct List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointsOfEntryStorage>
struct List_1_tC2567F05E952D8D17D6046D224FFB01182F23845;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>
struct List_1_t3926283FA9AE49778D95220056CEBFB01D034379;
// System.Collections.Generic.List`1<MoreMountains.Tools.MMInput/IMButton>
struct List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B;
// MoreMountains.Tools.MMEventListener`1<MoreMountains.CorgiEngine.CorgiEngineEvent>
struct MMEventListener_1_tD96F1B385B36A340EAA8B78CAF693A7323D138D5;
// MoreMountains.Tools.MMEventListener`1<MoreMountains.CorgiEngine.CorgiEngineStarEvent>
struct MMEventListener_1_tB2E92B0A5B5B64B336C47834054201E22F9D0E47;
// MoreMountains.Tools.MMSingleton`1<System.Object>
struct MMSingleton_1_tDFC31364B2B8D70B8F6DBDF995CE7F469D1A68EB;
// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.RetroAdventureProgressManager>
struct MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B;
// MoreMountains.Tools.MMStateMachine`1<System.Int32Enum>
struct MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions>
struct MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/MovementStates>
struct MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>
struct MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE;
// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<System.Int32Enum>
struct OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17;
// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions>
struct OnStateChangeDelegate_t9395700B6C9A35CC7C35E6679A2C908B530FA556;
// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<MoreMountains.Tools.MMInput/ButtonStates>
struct OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// MoreMountains.CorgiEngine.AmmoDisplay[]
struct AmmoDisplayU5BU5D_t5C3580C3BC1F66C524424F7F251B3017F375F296;
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// MoreMountains.CorgiEngine.Character[]
struct CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED;
// MoreMountains.CorgiEngine.CharacterAbility[]
struct CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E;
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// MoreMountains.Tools.MMProgressBar[]
struct MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09;
// MoreMountains.CorgiEngine.RetroAdventureScene[]
struct RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// MoreMountains.CorgiEngine.CharacterStates/CharacterConditions[]
struct CharacterConditionsU5BU5D_t3753FF1D9D2ADE37FF1B30173254536B45CAE8B9;
// MoreMountains.CorgiEngine.CharacterStates/MovementStates[]
struct MovementStatesU5BU5D_t0F46853D479ECD30AFB2839EE5061179A2BCF286;
// MoreMountains.Tools.AIBrain
struct AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// MoreMountains.CorgiEngine.AutoRespawn
struct AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0;
// MoreMountains.CorgiEngine.BackgroundTree
struct BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E;
// UnityEngine.EventSystems.BaseInputModule
struct BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.BoxCollider
struct BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9;
// MoreMountains.CorgiEngine.ButtonPrompt
struct ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03;
// MoreMountains.CorgiEngine.CameraController
struct CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// MoreMountains.CorgiEngine.Character
struct Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612;
// MoreMountains.CorgiEngine.CharacterAbility
struct CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7;
// MoreMountains.CorgiEngine.CharacterButtonActivation
struct CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B;
// MoreMountains.CorgiEngine.CharacterDash
struct CharacterDash_t78AF1BB1E2C303712001AD86D2041FC0B7A011AD;
// MoreMountains.CorgiEngine.CharacterFly
struct CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4;
// MoreMountains.CorgiEngine.CharacterGravity
struct CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4;
// MoreMountains.CorgiEngine.CharacterHorizontalMovement
struct CharacterHorizontalMovement_t1CCE148C0A676F29EF20C91D3A9895EC240E5846;
// MoreMountains.CorgiEngine.CharacterPersistence
struct CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC;
// MoreMountains.CorgiEngine.CharacterStates
struct CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F;
// MoreMountains.CorgiEngine.CheckPoint
struct CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// MoreMountains.CorgiEngine.CorgiController
struct CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8;
// MoreMountains.CorgiEngine.CorgiControllerParameters
struct CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8;
// MoreMountains.CorgiEngine.CorgiControllerState
struct CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// MoreMountains.CorgiEngine.DamageOnTouch
struct DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C;
// MoreMountains.CorgiEngine.FinishLevel
struct FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// MoreMountains.CorgiEngine.GUIManager
struct GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5;
// MoreMountains.CorgiEngine.GameManager
struct GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.Gradient
struct Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2;
// MoreMountains.CorgiEngine.GravityPoint
struct GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC;
// MoreMountains.CorgiEngine.GravityZone
struct GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8;
// MoreMountains.CorgiEngine.Health
struct Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// MoreMountains.CorgiEngine.InputManager
struct InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA;
// MoreMountains.InventoryEngine.InventoryInputManager
struct InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC;
// MoreMountains.InventoryEngine.ItemPicker
struct ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7;
// MoreMountains.CorgiEngine.LevelManager
struct LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E;
// MoreMountains.CorgiEngine.LevelMapCharacter
struct LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD;
// MoreMountains.CorgiEngine.LevelMapPathElement
struct LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75;
// MoreMountains.CorgiEngine.LevelSelector
struct LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316;
// MoreMountains.CorgiEngine.LevelSelectorGUI
struct LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE;
// MoreMountains.Tools.MMAdditiveSceneLoadingManagerSettings
struct MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB;
// MoreMountains.Feedbacks.MMFeedbacks
struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914;
// MoreMountains.Feedbacks.MMFeedbacksEvents
struct MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A;
// MoreMountains.Tools.MMHealthBar
struct MMHealthBar_t70CB8C2C73CB0B41B85104B0CC03A99EF0F86917;
// MoreMountains.Tools.MMPathMovement
struct MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B;
// MoreMountains.Tools.MMTouchButton
struct MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469;
// MoreMountains.Tools.MMTweenType
struct MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// MoreMountains.CorgiEngine.Mushroom
struct Mushroom_t46E47C48B440211E5F9FD59FB3742346EBD3CAC9;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E;
// MoreMountains.CorgiEngine.PickableItem
struct PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2;
// MoreMountains.CorgiEngine.Progress
struct Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// MoreMountains.CorgiEngine.RetroAdventureFinishLevel
struct RetroAdventureFinishLevel_t7238A7A8B78449B08A460E4C97F8BC356915E477;
// MoreMountains.CorgiEngine.RetroAdventureGUIManager
struct RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8;
// MoreMountains.CorgiEngine.RetroAdventureLevel
struct RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28;
// MoreMountains.CorgiEngine.RetroAdventureProgressManager
struct RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260;
// MoreMountains.CorgiEngine.RetroAdventureScene
struct RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C;
// MoreMountains.CorgiEngine.RetroCopter
struct RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC;
// MoreMountains.CorgiEngine.RetroDoor
struct RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985;
// MoreMountains.CorgiEngine.RetroNextLevel
struct RetroNextLevel_t49F31286D6C8F872FD3ADAA1F38E01079D88F4F6;
// MoreMountains.CorgiEngine.RetroStar
struct RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// MoreMountains.CorgiEngine.Star
struct Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401;
// System.String
struct String_t;
// MoreMountains.CorgiEngine.SuperHipsterBrosHealth
struct SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92;
// MoreMountains.CorgiEngine.SurfaceModifier
struct SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Type
struct Type_t;
// MoreMountains.CorgiEngine.UncheckReverseGravityInput
struct UncheckReverseGravityInput_tB2E610A441AEFFB7B59E20DD94DA6E5E12FA7D90;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// MoreMountains.CorgiEngine.Health/OnDeathDelegate
struct OnDeathDelegate_t9F947C81F473BA238CBC47BF4A8C58D2291A2B75;
// MoreMountains.CorgiEngine.Health/OnHitDelegate
struct OnHitDelegate_tE3BFE274661AEA8BAF51E9384B69489DE6D10E75;
// MoreMountains.CorgiEngine.Health/OnHitZeroDelegate
struct OnHitZeroDelegate_tD23B05006B609BA538F06C37C939E2E0C1B34314;
// MoreMountains.CorgiEngine.Health/OnReviveDelegate
struct OnReviveDelegate_tF96E17726864907F607E6A0A2C5D837C52A70298;
// MoreMountains.Tools.MMInput/IMButton
struct IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate
struct ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705;
// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate
struct ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B;
// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate
struct ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C;

IL2CPP_EXTERN_C RuntimeClass* EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMSceneLoadingManager_tB8F92FD08BEB060070D775E0CFCB5E074F51129F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral14BE754BF8F5400050442C7A7201D90C19ABEEF1;
IL2CPP_EXTERN_C String_t* _stringLiteral16A70DC8C790D0EFD53AE03FF23CB99D8B7A53CA;
IL2CPP_EXTERN_C String_t* _stringLiteral38D4FF83ACF3D8288F951702EED939E1195211CB;
IL2CPP_EXTERN_C String_t* _stringLiteral455BE6C51A15F36C8D913F896775D15888AC8673;
IL2CPP_EXTERN_C String_t* _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32;
IL2CPP_EXTERN_C String_t* _stringLiteral623CBDCC47C6DB996D3C60509009A4E557A3E1EF;
IL2CPP_EXTERN_C String_t* _stringLiteral9D354CA1036DDA6F701F800C5C1B3A4235D2EDD7;
IL2CPP_EXTERN_C String_t* _stringLiteralAE24C5BE9B741FFFA87D2A951BFE7EA0440461CD;
IL2CPP_EXTERN_C String_t* _stringLiteralCE863626383155D02291456632E72C0FBEC22C3C;
IL2CPP_EXTERN_C String_t* _stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mD7909FB3C98B8C554A48934864C1F8E9065C36BA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m034D1618B31D56CFF4409A30E06845ABCB15C539_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_mB8CB45C5289A0ACF38DAD7B4727F32E4E93DFC30_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4_m90CFBE75068B01DBC3E4D3AA19772E13C30F7717_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4_m1BA5F82EFAF229EA35C6C8F09D91BBAEF9EF53C2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_Awake_m9F53860BDC9DBAE7D1DC680D2A20F93D99BB2719_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1__ctor_m3CC6785DFF822FDD50FB05361D7B6C10F39D84EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_HasInstance_m44412714D79EE1735A3E92160755AC8EEC80B0FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_0_0_0_var;

struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C;
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224;
struct RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tDF066D31BA821E7961D393F1280F8EB3130CC13B 
{
public:

public:
};


// System.Object


// UnityEngine.EventSystems.AbstractEventData
struct AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.EventSystems.AbstractEventData::m_Used
	bool ___m_Used_0;

public:
	inline static int32_t get_offset_of_m_Used_0() { return static_cast<int32_t>(offsetof(AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF, ___m_Used_0)); }
	inline bool get_m_Used_0() const { return ___m_Used_0; }
	inline bool* get_address_of_m_Used_0() { return &___m_Used_0; }
	inline void set_m_Used_0(bool value)
	{
		___m_Used_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// MoreMountains.CorgiEngine.Progress
struct Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Progress::InitialMaximumLives
	int32_t ___InitialMaximumLives_0;
	// System.Int32 MoreMountains.CorgiEngine.Progress::InitialCurrentLives
	int32_t ___InitialCurrentLives_1;
	// System.Int32 MoreMountains.CorgiEngine.Progress::MaximumLives
	int32_t ___MaximumLives_2;
	// System.Int32 MoreMountains.CorgiEngine.Progress::CurrentLives
	int32_t ___CurrentLives_3;
	// MoreMountains.CorgiEngine.RetroAdventureScene[] MoreMountains.CorgiEngine.Progress::Scenes
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* ___Scenes_4;

public:
	inline static int32_t get_offset_of_InitialMaximumLives_0() { return static_cast<int32_t>(offsetof(Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42, ___InitialMaximumLives_0)); }
	inline int32_t get_InitialMaximumLives_0() const { return ___InitialMaximumLives_0; }
	inline int32_t* get_address_of_InitialMaximumLives_0() { return &___InitialMaximumLives_0; }
	inline void set_InitialMaximumLives_0(int32_t value)
	{
		___InitialMaximumLives_0 = value;
	}

	inline static int32_t get_offset_of_InitialCurrentLives_1() { return static_cast<int32_t>(offsetof(Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42, ___InitialCurrentLives_1)); }
	inline int32_t get_InitialCurrentLives_1() const { return ___InitialCurrentLives_1; }
	inline int32_t* get_address_of_InitialCurrentLives_1() { return &___InitialCurrentLives_1; }
	inline void set_InitialCurrentLives_1(int32_t value)
	{
		___InitialCurrentLives_1 = value;
	}

	inline static int32_t get_offset_of_MaximumLives_2() { return static_cast<int32_t>(offsetof(Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42, ___MaximumLives_2)); }
	inline int32_t get_MaximumLives_2() const { return ___MaximumLives_2; }
	inline int32_t* get_address_of_MaximumLives_2() { return &___MaximumLives_2; }
	inline void set_MaximumLives_2(int32_t value)
	{
		___MaximumLives_2 = value;
	}

	inline static int32_t get_offset_of_CurrentLives_3() { return static_cast<int32_t>(offsetof(Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42, ___CurrentLives_3)); }
	inline int32_t get_CurrentLives_3() const { return ___CurrentLives_3; }
	inline int32_t* get_address_of_CurrentLives_3() { return &___CurrentLives_3; }
	inline void set_CurrentLives_3(int32_t value)
	{
		___CurrentLives_3 = value;
	}

	inline static int32_t get_offset_of_Scenes_4() { return static_cast<int32_t>(offsetof(Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42, ___Scenes_4)); }
	inline RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* get_Scenes_4() const { return ___Scenes_4; }
	inline RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D** get_address_of_Scenes_4() { return &___Scenes_4; }
	inline void set_Scenes_4(RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* value)
	{
		___Scenes_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Scenes_4), (void*)value);
	}
};


// MoreMountains.CorgiEngine.RetroAdventureScene
struct RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C  : public RuntimeObject
{
public:
	// System.String MoreMountains.CorgiEngine.RetroAdventureScene::SceneName
	String_t* ___SceneName_0;
	// System.Boolean MoreMountains.CorgiEngine.RetroAdventureScene::LevelComplete
	bool ___LevelComplete_1;
	// System.Boolean MoreMountains.CorgiEngine.RetroAdventureScene::LevelUnlocked
	bool ___LevelUnlocked_2;
	// System.Int32 MoreMountains.CorgiEngine.RetroAdventureScene::MaxStars
	int32_t ___MaxStars_3;
	// System.Boolean[] MoreMountains.CorgiEngine.RetroAdventureScene::CollectedStars
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* ___CollectedStars_4;

public:
	inline static int32_t get_offset_of_SceneName_0() { return static_cast<int32_t>(offsetof(RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C, ___SceneName_0)); }
	inline String_t* get_SceneName_0() const { return ___SceneName_0; }
	inline String_t** get_address_of_SceneName_0() { return &___SceneName_0; }
	inline void set_SceneName_0(String_t* value)
	{
		___SceneName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneName_0), (void*)value);
	}

	inline static int32_t get_offset_of_LevelComplete_1() { return static_cast<int32_t>(offsetof(RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C, ___LevelComplete_1)); }
	inline bool get_LevelComplete_1() const { return ___LevelComplete_1; }
	inline bool* get_address_of_LevelComplete_1() { return &___LevelComplete_1; }
	inline void set_LevelComplete_1(bool value)
	{
		___LevelComplete_1 = value;
	}

	inline static int32_t get_offset_of_LevelUnlocked_2() { return static_cast<int32_t>(offsetof(RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C, ___LevelUnlocked_2)); }
	inline bool get_LevelUnlocked_2() const { return ___LevelUnlocked_2; }
	inline bool* get_address_of_LevelUnlocked_2() { return &___LevelUnlocked_2; }
	inline void set_LevelUnlocked_2(bool value)
	{
		___LevelUnlocked_2 = value;
	}

	inline static int32_t get_offset_of_MaxStars_3() { return static_cast<int32_t>(offsetof(RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C, ___MaxStars_3)); }
	inline int32_t get_MaxStars_3() const { return ___MaxStars_3; }
	inline int32_t* get_address_of_MaxStars_3() { return &___MaxStars_3; }
	inline void set_MaxStars_3(int32_t value)
	{
		___MaxStars_3 = value;
	}

	inline static int32_t get_offset_of_CollectedStars_4() { return static_cast<int32_t>(offsetof(RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C, ___CollectedStars_4)); }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* get_CollectedStars_4() const { return ___CollectedStars_4; }
	inline BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C** get_address_of_CollectedStars_4() { return &___CollectedStars_4; }
	inline void set_CollectedStars_4(BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* value)
	{
		___CollectedStars_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CollectedStars_4), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// MoreMountains.Tools.MMInput/IMButton
struct IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B  : public RuntimeObject
{
public:
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates> MoreMountains.Tools.MMInput/IMButton::<State>k__BackingField
	MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * ___U3CStateU3Ek__BackingField_0;
	// System.String MoreMountains.Tools.MMInput/IMButton::ButtonID
	String_t* ___ButtonID_1;
	// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonDownMethod
	ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 * ___ButtonDownMethod_2;
	// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonPressedMethod
	ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B * ___ButtonPressedMethod_3;
	// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonUpMethod
	ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C * ___ButtonUpMethod_4;
	// System.Single MoreMountains.Tools.MMInput/IMButton::_lastButtonDownAt
	float ____lastButtonDownAt_5;
	// System.Single MoreMountains.Tools.MMInput/IMButton::_lastButtonUpAt
	float ____lastButtonUpAt_6;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___U3CStateU3Ek__BackingField_0)); }
	inline MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE ** get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonID_1() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonID_1)); }
	inline String_t* get_ButtonID_1() const { return ___ButtonID_1; }
	inline String_t** get_address_of_ButtonID_1() { return &___ButtonID_1; }
	inline void set_ButtonID_1(String_t* value)
	{
		___ButtonID_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonID_1), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonDownMethod_2() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonDownMethod_2)); }
	inline ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 * get_ButtonDownMethod_2() const { return ___ButtonDownMethod_2; }
	inline ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 ** get_address_of_ButtonDownMethod_2() { return &___ButtonDownMethod_2; }
	inline void set_ButtonDownMethod_2(ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 * value)
	{
		___ButtonDownMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonDownMethod_2), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPressedMethod_3() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonPressedMethod_3)); }
	inline ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B * get_ButtonPressedMethod_3() const { return ___ButtonPressedMethod_3; }
	inline ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B ** get_address_of_ButtonPressedMethod_3() { return &___ButtonPressedMethod_3; }
	inline void set_ButtonPressedMethod_3(ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B * value)
	{
		___ButtonPressedMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPressedMethod_3), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonUpMethod_4() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonUpMethod_4)); }
	inline ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C * get_ButtonUpMethod_4() const { return ___ButtonUpMethod_4; }
	inline ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C ** get_address_of_ButtonUpMethod_4() { return &___ButtonUpMethod_4; }
	inline void set_ButtonUpMethod_4(ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C * value)
	{
		___ButtonUpMethod_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonUpMethod_4), (void*)value);
	}

	inline static int32_t get_offset_of__lastButtonDownAt_5() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ____lastButtonDownAt_5)); }
	inline float get__lastButtonDownAt_5() const { return ____lastButtonDownAt_5; }
	inline float* get_address_of__lastButtonDownAt_5() { return &____lastButtonDownAt_5; }
	inline void set__lastButtonDownAt_5(float value)
	{
		____lastButtonDownAt_5 = value;
	}

	inline static int32_t get_offset_of__lastButtonUpAt_6() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ____lastButtonUpAt_6)); }
	inline float get__lastButtonUpAt_6() const { return ____lastButtonUpAt_6; }
	inline float* get_address_of__lastButtonUpAt_6() { return &____lastButtonUpAt_6; }
	inline void set__lastButtonUpAt_6(float value)
	{
		____lastButtonUpAt_6 = value;
	}
};


// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E  : public AbstractEventData_tA0B5065DE3430C0031ADE061668E1C7073D718DF
{
public:
	// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.BaseEventData::m_EventSystem
	EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * ___m_EventSystem_1;

public:
	inline static int32_t get_offset_of_m_EventSystem_1() { return static_cast<int32_t>(offsetof(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E, ___m_EventSystem_1)); }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * get_m_EventSystem_1() const { return ___m_EventSystem_1; }
	inline EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C ** get_address_of_m_EventSystem_1() { return &___m_EventSystem_1; }
	inline void set_m_EventSystem_1(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * value)
	{
		___m_EventSystem_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystem_1), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiEngineStarEvent
struct CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF 
{
public:
	// System.String MoreMountains.CorgiEngine.CorgiEngineStarEvent::SceneName
	String_t* ___SceneName_0;
	// System.Int32 MoreMountains.CorgiEngine.CorgiEngineStarEvent::StarID
	int32_t ___StarID_1;

public:
	inline static int32_t get_offset_of_SceneName_0() { return static_cast<int32_t>(offsetof(CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF, ___SceneName_0)); }
	inline String_t* get_SceneName_0() const { return ___SceneName_0; }
	inline String_t** get_address_of_SceneName_0() { return &___SceneName_0; }
	inline void set_SceneName_0(String_t* value)
	{
		___SceneName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneName_0), (void*)value);
	}

	inline static int32_t get_offset_of_StarID_1() { return static_cast<int32_t>(offsetof(CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF, ___StarID_1)); }
	inline int32_t get_StarID_1() const { return ___StarID_1; }
	inline int32_t* get_address_of_StarID_1() { return &___StarID_1; }
	inline void set_StarID_1(int32_t value)
	{
		___StarID_1 = value;
	}
};

struct CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_StaticFields
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineStarEvent MoreMountains.CorgiEngine.CorgiEngineStarEvent::e
	CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF  ___e_2;

public:
	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_StaticFields, ___e_2)); }
	inline CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF  get_e_2() const { return ___e_2; }
	inline CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF * get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF  value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_2))->___SceneName_0), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of MoreMountains.CorgiEngine.CorgiEngineStarEvent
struct CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_marshaled_pinvoke
{
	char* ___SceneName_0;
	int32_t ___StarID_1;
};
// Native definition for COM marshalling of MoreMountains.CorgiEngine.CorgiEngineStarEvent
struct CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_marshaled_com
{
	Il2CppChar* ___SceneName_0;
	int32_t ___StarID_1;
};

// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.SceneManagement.Scene
struct Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE 
{
public:
	// System.Int32 UnityEngine.SceneManagement.Scene::m_Handle
	int32_t ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE, ___m_Handle_0)); }
	inline int32_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline int32_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(int32_t value)
	{
		___m_Handle_0 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B 
{
public:
	// UnityEngine.ParticleSystem UnityEngine.ParticleSystem/MainModule::m_ParticleSystem
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;

public:
	inline static int32_t get_offset_of_m_ParticleSystem_0() { return static_cast<int32_t>(offsetof(MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B, ___m_ParticleSystem_0)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_m_ParticleSystem_0() const { return ___m_ParticleSystem_0; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_m_ParticleSystem_0() { return &___m_ParticleSystem_0; }
	inline void set_m_ParticleSystem_0(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___m_ParticleSystem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParticleSystem_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_pinvoke
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};
// Native definition for COM marshalling of UnityEngine.ParticleSystem/MainModule
struct MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B_marshaled_com
{
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___m_ParticleSystem_0;
};

// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiEngineEventTypes
struct CorgiEngineEventTypes_tB10F6D08B2061073EA7004B64B437F75B232F7A5 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiEngineEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CorgiEngineEventTypes_tB10F6D08B2061073EA7004B64B437F75B232F7A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.ParticleSystemGradientMode
struct ParticleSystemGradientMode_tCF15644F35B8D166D1A9C073E758D24794895497 
{
public:
	// System.Int32 UnityEngine.ParticleSystemGradientMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParticleSystemGradientMode_tCF15644F35B8D166D1A9C073E758D24794895497, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.PauseMethods
struct PauseMethods_t817091D04B272DD80FC9310BCE7E11F78958E569 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.PauseMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PauseMethods_t817091D04B272DD80FC9310BCE7E11F78958E569, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.RaycastHit2D
struct RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Centroid_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Point_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Normal_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// MoreMountains.CorgiEngine.ButtonActivated/ButtonActivatedRequirements
struct ButtonActivatedRequirements_t50772F687A7EA08BA060844877EC546DD0257B97 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated/ButtonActivatedRequirements::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonActivatedRequirements_t50772F687A7EA08BA060844877EC546DD0257B97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.ButtonActivated/InputTypes
struct InputTypes_t7B39F8DC5451B97BCAB5F6472EF98900886BED85 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated/InputTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputTypes_t7B39F8DC5451B97BCAB5F6472EF98900886BED85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Character/CharacterTypes
struct CharacterTypes_t9A5C5F8A30B288ECE56E91FE85D030DCEFCCFA1C 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Character/CharacterTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterTypes_t9A5C5F8A30B288ECE56E91FE85D030DCEFCCFA1C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Character/FacingDirections
struct FacingDirections_t0EB9AC4553B3988723F93C6BBF8E28FDAA96FB0F 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Character/FacingDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FacingDirections_t0EB9AC4553B3988723F93C6BBF8E28FDAA96FB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Character/SpawnFacingDirections
struct SpawnFacingDirections_tCC32C8E497FCD0F662292ECF906CC88C1EDD469C 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Character/SpawnFacingDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpawnFacingDirections_tCC32C8E497FCD0F662292ECF906CC88C1EDD469C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CharacterGravity/TransitionForcesModes
struct TransitionForcesModes_t87637A84AA591394674BBE828FE87322CBE39D7D 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CharacterGravity/TransitionForcesModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransitionForcesModes_t87637A84AA591394674BBE828FE87322CBE39D7D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CharacterStates/CharacterConditions
struct CharacterConditions_t9AF9C636D95BE10970493822C7C0F2D08E6ECFBC 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CharacterStates/CharacterConditions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterConditions_t9AF9C636D95BE10970493822C7C0F2D08E6ECFBC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiController/DetachmentMethods
struct DetachmentMethods_tB1BEBFED5C211907CA24D7D7A54098431911C245 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiController/DetachmentMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DetachmentMethods_tB1BEBFED5C211907CA24D7D7A54098431911C245, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiController/UpdateModes
struct UpdateModes_t49B91119A738485082808C58875A22F0ADFC1463 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiController/UpdateModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateModes_t49B91119A738485082808C58875A22F0ADFC1463, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.InputManager/InputForcedMode
struct InputForcedMode_tA0F203CE522FC1DC3ED85CAFE5A11AAFD0333838 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.InputManager/InputForcedMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputForcedMode_tA0F203CE522FC1DC3ED85CAFE5A11AAFD0333838, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.InputManager/MovementControls
struct MovementControls_tD4A1D5B3031A26CDEB639251D532B1FDFB22E979 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.InputManager/MovementControls::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementControls_tD4A1D5B3031A26CDEB639251D532B1FDFB22E979, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.LevelManager/BoundsModes
struct BoundsModes_t1D11BB4CB88820B8543DDFFC0488700BBF914B52 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.LevelManager/BoundsModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundsModes_t1D11BB4CB88820B8543DDFFC0488700BBF914B52, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.LevelManager/CheckpointDirections
struct CheckpointDirections_t669AD9746BA4D6BC94AEA84726131FD24875AD40 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.LevelManager/CheckpointDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckpointDirections_t669AD9746BA4D6BC94AEA84726131FD24875AD40, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.LevelManager/CheckpointsAxis
struct CheckpointsAxis_t85B8EB1F27266F18E0BFFD55D1B1C67B49D44464 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.LevelManager/CheckpointsAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckpointsAxis_t85B8EB1F27266F18E0BFFD55D1B1C67B49D44464, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks/Directions
struct Directions_t6734A23FBABD110399D0F300811824A2515BC3EB 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbacks/Directions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Directions_t6734A23FBABD110399D0F300811824A2515BC3EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks/InitializationModes
struct InitializationModes_t2C4B197E025D0230877655D43D5636300E479888 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbacks/InitializationModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializationModes_t2C4B197E025D0230877655D43D5636300E479888, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks/SafeModes
struct SafeModes_t27DE69744FFB89E58A263B7318C1A1DA32EBE30E 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbacks/SafeModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SafeModes_t27DE69744FFB89E58A263B7318C1A1DA32EBE30E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMInput/ButtonStates
struct ButtonStates_t3674E31E79E34E31FDF435834C5B1A41761F5FAC 
{
public:
	// System.Int32 MoreMountains.Tools.MMInput/ButtonStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStates_t3674E31E79E34E31FDF435834C5B1A41761F5FAC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMLoadScene/LoadingSceneModes
struct LoadingSceneModes_tE9CEF7BA6532CAAD9216A634C1862F6B5494C77C 
{
public:
	// System.Int32 MoreMountains.Tools.MMLoadScene/LoadingSceneModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadingSceneModes_tE9CEF7BA6532CAAD9216A634C1862F6B5494C77C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMTouchButton/ButtonStates
struct ButtonStates_t3E3C7AF1D7377F9DA6AE12D988FC9454588DC30B 
{
public:
	// System.Int32 MoreMountains.Tools.MMTouchButton/ButtonStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStates_t3E3C7AF1D7377F9DA6AE12D988FC9454588DC30B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMStateMachine`1<System.Int32Enum>
struct MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA  : public RuntimeObject
{
public:
	// System.Boolean MoreMountains.Tools.MMStateMachine`1::<TriggerEvents>k__BackingField
	bool ___U3CTriggerEventsU3Ek__BackingField_0;
	// UnityEngine.GameObject MoreMountains.Tools.MMStateMachine`1::Target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Target_1;
	// T MoreMountains.Tools.MMStateMachine`1::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_2;
	// T MoreMountains.Tools.MMStateMachine`1::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_3;
	// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<T> MoreMountains.Tools.MMStateMachine`1::OnStateChange
	OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 * ___OnStateChange_4;

public:
	inline static int32_t get_offset_of_U3CTriggerEventsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___U3CTriggerEventsU3Ek__BackingField_0)); }
	inline bool get_U3CTriggerEventsU3Ek__BackingField_0() const { return ___U3CTriggerEventsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTriggerEventsU3Ek__BackingField_0() { return &___U3CTriggerEventsU3Ek__BackingField_0; }
	inline void set_U3CTriggerEventsU3Ek__BackingField_0(bool value)
	{
		___U3CTriggerEventsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_Target_1() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___Target_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Target_1() const { return ___Target_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Target_1() { return &___Target_1; }
	inline void set_Target_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___U3CCurrentStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_2() const { return ___U3CCurrentStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_2() { return &___U3CCurrentStateU3Ek__BackingField_2; }
	inline void set_U3CCurrentStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___U3CPreviousStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_3() const { return ___U3CPreviousStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_3() { return &___U3CPreviousStateU3Ek__BackingField_3; }
	inline void set_U3CPreviousStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_OnStateChange_4() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___OnStateChange_4)); }
	inline OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 * get_OnStateChange_4() const { return ___OnStateChange_4; }
	inline OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 ** get_address_of_OnStateChange_4() { return &___OnStateChange_4; }
	inline void set_OnStateChange_4(OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 * value)
	{
		___OnStateChange_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStateChange_4), (void*)value);
	}
};


// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions>
struct MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638  : public RuntimeObject
{
public:
	// System.Boolean MoreMountains.Tools.MMStateMachine`1::<TriggerEvents>k__BackingField
	bool ___U3CTriggerEventsU3Ek__BackingField_0;
	// UnityEngine.GameObject MoreMountains.Tools.MMStateMachine`1::Target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Target_1;
	// T MoreMountains.Tools.MMStateMachine`1::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_2;
	// T MoreMountains.Tools.MMStateMachine`1::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_3;
	// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<T> MoreMountains.Tools.MMStateMachine`1::OnStateChange
	OnStateChangeDelegate_t9395700B6C9A35CC7C35E6679A2C908B530FA556 * ___OnStateChange_4;

public:
	inline static int32_t get_offset_of_U3CTriggerEventsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638, ___U3CTriggerEventsU3Ek__BackingField_0)); }
	inline bool get_U3CTriggerEventsU3Ek__BackingField_0() const { return ___U3CTriggerEventsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTriggerEventsU3Ek__BackingField_0() { return &___U3CTriggerEventsU3Ek__BackingField_0; }
	inline void set_U3CTriggerEventsU3Ek__BackingField_0(bool value)
	{
		___U3CTriggerEventsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_Target_1() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638, ___Target_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Target_1() const { return ___Target_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Target_1() { return &___Target_1; }
	inline void set_Target_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638, ___U3CCurrentStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_2() const { return ___U3CCurrentStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_2() { return &___U3CCurrentStateU3Ek__BackingField_2; }
	inline void set_U3CCurrentStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638, ___U3CPreviousStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_3() const { return ___U3CPreviousStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_3() { return &___U3CPreviousStateU3Ek__BackingField_3; }
	inline void set_U3CPreviousStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_OnStateChange_4() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638, ___OnStateChange_4)); }
	inline OnStateChangeDelegate_t9395700B6C9A35CC7C35E6679A2C908B530FA556 * get_OnStateChange_4() const { return ___OnStateChange_4; }
	inline OnStateChangeDelegate_t9395700B6C9A35CC7C35E6679A2C908B530FA556 ** get_address_of_OnStateChange_4() { return &___OnStateChange_4; }
	inline void set_OnStateChange_4(OnStateChangeDelegate_t9395700B6C9A35CC7C35E6679A2C908B530FA556 * value)
	{
		___OnStateChange_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStateChange_4), (void*)value);
	}
};


// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>
struct MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE  : public RuntimeObject
{
public:
	// System.Boolean MoreMountains.Tools.MMStateMachine`1::<TriggerEvents>k__BackingField
	bool ___U3CTriggerEventsU3Ek__BackingField_0;
	// UnityEngine.GameObject MoreMountains.Tools.MMStateMachine`1::Target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Target_1;
	// T MoreMountains.Tools.MMStateMachine`1::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_2;
	// T MoreMountains.Tools.MMStateMachine`1::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_3;
	// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<T> MoreMountains.Tools.MMStateMachine`1::OnStateChange
	OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 * ___OnStateChange_4;

public:
	inline static int32_t get_offset_of_U3CTriggerEventsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___U3CTriggerEventsU3Ek__BackingField_0)); }
	inline bool get_U3CTriggerEventsU3Ek__BackingField_0() const { return ___U3CTriggerEventsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTriggerEventsU3Ek__BackingField_0() { return &___U3CTriggerEventsU3Ek__BackingField_0; }
	inline void set_U3CTriggerEventsU3Ek__BackingField_0(bool value)
	{
		___U3CTriggerEventsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_Target_1() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___Target_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Target_1() const { return ___Target_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Target_1() { return &___Target_1; }
	inline void set_Target_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___U3CCurrentStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_2() const { return ___U3CCurrentStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_2() { return &___U3CCurrentStateU3Ek__BackingField_2; }
	inline void set_U3CCurrentStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___U3CPreviousStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_3() const { return ___U3CPreviousStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_3() { return &___U3CPreviousStateU3Ek__BackingField_3; }
	inline void set_U3CPreviousStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_OnStateChange_4() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___OnStateChange_4)); }
	inline OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 * get_OnStateChange_4() const { return ___OnStateChange_4; }
	inline OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 ** get_address_of_OnStateChange_4() { return &___OnStateChange_4; }
	inline void set_OnStateChange_4(OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 * value)
	{
		___OnStateChange_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStateChange_4), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// MoreMountains.CorgiEngine.CorgiEngineEvent
struct CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B 
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineEventTypes MoreMountains.CorgiEngine.CorgiEngineEvent::EventType
	int32_t ___EventType_0;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}
};

struct CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_StaticFields
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineEvent MoreMountains.CorgiEngine.CorgiEngineEvent::e
	CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  ___e_1;

public:
	inline static int32_t get_offset_of_e_1() { return static_cast<int32_t>(offsetof(CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_StaticFields, ___e_1)); }
	inline CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  get_e_1() const { return ___e_1; }
	inline CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B * get_address_of_e_1() { return &___e_1; }
	inline void set_e_1(CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  value)
	{
		___e_1 = value;
	}
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// UnityEngine.ParticleSystem/MinMaxGradient
struct MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7 
{
public:
	// UnityEngine.ParticleSystemGradientMode UnityEngine.ParticleSystem/MinMaxGradient::m_Mode
	int32_t ___m_Mode_0;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMin
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___m_GradientMin_1;
	// UnityEngine.Gradient UnityEngine.ParticleSystem/MinMaxGradient::m_GradientMax
	Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * ___m_GradientMax_2;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMin
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_ColorMin_3;
	// UnityEngine.Color UnityEngine.ParticleSystem/MinMaxGradient::m_ColorMax
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_ColorMax_4;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_GradientMin_1() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_GradientMin_1)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_m_GradientMin_1() const { return ___m_GradientMin_1; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_m_GradientMin_1() { return &___m_GradientMin_1; }
	inline void set_m_GradientMin_1(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___m_GradientMin_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GradientMin_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_GradientMax_2() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_GradientMax_2)); }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * get_m_GradientMax_2() const { return ___m_GradientMax_2; }
	inline Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 ** get_address_of_m_GradientMax_2() { return &___m_GradientMax_2; }
	inline void set_m_GradientMax_2(Gradient_t297BAC6722F67728862AE2FBE760A400DA8902F2 * value)
	{
		___m_GradientMax_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_GradientMax_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorMin_3() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_ColorMin_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_ColorMin_3() const { return ___m_ColorMin_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_ColorMin_3() { return &___m_ColorMin_3; }
	inline void set_m_ColorMin_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_ColorMin_3 = value;
	}

	inline static int32_t get_offset_of_m_ColorMax_4() { return static_cast<int32_t>(offsetof(MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7, ___m_ColorMax_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_ColorMax_4() const { return ___m_ColorMax_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_ColorMax_4() { return &___m_ColorMax_4; }
	inline void set_m_ColorMax_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_ColorMax_4 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.ParticleSystem
struct ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// MoreMountains.Tools.MMPersistentSingleton`1<MoreMountains.CorgiEngine.GameManager>
struct MMPersistentSingleton_1_t41D3BEA3FEE7D79C238ABF48390643722E7F3664  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MoreMountains.Tools.MMPersistentSingleton`1::AutomaticallyUnparentOnAwake
	bool ___AutomaticallyUnparentOnAwake_4;
	// System.Boolean MoreMountains.Tools.MMPersistentSingleton`1::_enabled
	bool ____enabled_6;

public:
	inline static int32_t get_offset_of_AutomaticallyUnparentOnAwake_4() { return static_cast<int32_t>(offsetof(MMPersistentSingleton_1_t41D3BEA3FEE7D79C238ABF48390643722E7F3664, ___AutomaticallyUnparentOnAwake_4)); }
	inline bool get_AutomaticallyUnparentOnAwake_4() const { return ___AutomaticallyUnparentOnAwake_4; }
	inline bool* get_address_of_AutomaticallyUnparentOnAwake_4() { return &___AutomaticallyUnparentOnAwake_4; }
	inline void set_AutomaticallyUnparentOnAwake_4(bool value)
	{
		___AutomaticallyUnparentOnAwake_4 = value;
	}

	inline static int32_t get_offset_of__enabled_6() { return static_cast<int32_t>(offsetof(MMPersistentSingleton_1_t41D3BEA3FEE7D79C238ABF48390643722E7F3664, ____enabled_6)); }
	inline bool get__enabled_6() const { return ____enabled_6; }
	inline bool* get_address_of__enabled_6() { return &____enabled_6; }
	inline void set__enabled_6(bool value)
	{
		____enabled_6 = value;
	}
};

struct MMPersistentSingleton_1_t41D3BEA3FEE7D79C238ABF48390643722E7F3664_StaticFields
{
public:
	// T MoreMountains.Tools.MMPersistentSingleton`1::_instance
	GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * ____instance_5;

public:
	inline static int32_t get_offset_of__instance_5() { return static_cast<int32_t>(offsetof(MMPersistentSingleton_1_t41D3BEA3FEE7D79C238ABF48390643722E7F3664_StaticFields, ____instance_5)); }
	inline GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * get__instance_5() const { return ____instance_5; }
	inline GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A ** get_address_of__instance_5() { return &____instance_5; }
	inline void set__instance_5(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * value)
	{
		____instance_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_5), (void*)value);
	}
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.GUIManager>
struct MMSingleton_1_t22F86A56A6541E0ECE39DDD17501869CE6D4CC0D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_t22F86A56A6541E0ECE39DDD17501869CE6D4CC0D_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_t22F86A56A6541E0ECE39DDD17501869CE6D4CC0D_StaticFields, ____instance_4)); }
	inline GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * get__instance_4() const { return ____instance_4; }
	inline GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.InputManager>
struct MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597_StaticFields, ____instance_4)); }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * get__instance_4() const { return ____instance_4; }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.LevelManager>
struct MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB_StaticFields, ____instance_4)); }
	inline LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * get__instance_4() const { return ____instance_4; }
	inline LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.RetroAdventureProgressManager>
struct MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B_StaticFields, ____instance_4)); }
	inline RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * get__instance_4() const { return ____instance_4; }
	inline RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// MoreMountains.CorgiEngine.BackgroundTree
struct BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single MoreMountains.CorgiEngine.BackgroundTree::scaleSpeed
	float ___scaleSpeed_4;
	// System.Single MoreMountains.CorgiEngine.BackgroundTree::scaleDistance
	float ___scaleDistance_5;
	// System.Single MoreMountains.CorgiEngine.BackgroundTree::rotationSpeed
	float ___rotationSpeed_6;
	// System.Single MoreMountains.CorgiEngine.BackgroundTree::rotationAmplitude
	float ___rotationAmplitude_7;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.BackgroundTree::_scaleTarget
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____scaleTarget_8;
	// UnityEngine.Quaternion MoreMountains.CorgiEngine.BackgroundTree::_rotationTarget
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ____rotationTarget_9;
	// System.Single MoreMountains.CorgiEngine.BackgroundTree::_accumulator
	float ____accumulator_10;

public:
	inline static int32_t get_offset_of_scaleSpeed_4() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ___scaleSpeed_4)); }
	inline float get_scaleSpeed_4() const { return ___scaleSpeed_4; }
	inline float* get_address_of_scaleSpeed_4() { return &___scaleSpeed_4; }
	inline void set_scaleSpeed_4(float value)
	{
		___scaleSpeed_4 = value;
	}

	inline static int32_t get_offset_of_scaleDistance_5() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ___scaleDistance_5)); }
	inline float get_scaleDistance_5() const { return ___scaleDistance_5; }
	inline float* get_address_of_scaleDistance_5() { return &___scaleDistance_5; }
	inline void set_scaleDistance_5(float value)
	{
		___scaleDistance_5 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_6() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ___rotationSpeed_6)); }
	inline float get_rotationSpeed_6() const { return ___rotationSpeed_6; }
	inline float* get_address_of_rotationSpeed_6() { return &___rotationSpeed_6; }
	inline void set_rotationSpeed_6(float value)
	{
		___rotationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_rotationAmplitude_7() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ___rotationAmplitude_7)); }
	inline float get_rotationAmplitude_7() const { return ___rotationAmplitude_7; }
	inline float* get_address_of_rotationAmplitude_7() { return &___rotationAmplitude_7; }
	inline void set_rotationAmplitude_7(float value)
	{
		___rotationAmplitude_7 = value;
	}

	inline static int32_t get_offset_of__scaleTarget_8() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ____scaleTarget_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__scaleTarget_8() const { return ____scaleTarget_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__scaleTarget_8() { return &____scaleTarget_8; }
	inline void set__scaleTarget_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____scaleTarget_8 = value;
	}

	inline static int32_t get_offset_of__rotationTarget_9() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ____rotationTarget_9)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get__rotationTarget_9() const { return ____rotationTarget_9; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of__rotationTarget_9() { return &____rotationTarget_9; }
	inline void set__rotationTarget_9(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		____rotationTarget_9 = value;
	}

	inline static int32_t get_offset_of__accumulator_10() { return static_cast<int32_t>(offsetof(BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56, ____accumulator_10)); }
	inline float get__accumulator_10() const { return ____accumulator_10; }
	inline float* get_address_of__accumulator_10() { return &____accumulator_10; }
	inline void set__accumulator_10(float value)
	{
		____accumulator_10 = value;
	}
};


// UnityEngine.BoxCollider2D
struct BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9  : public Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722
{
public:

public:
};


// MoreMountains.CorgiEngine.Character
struct Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.CorgiEngine.Character/CharacterTypes MoreMountains.CorgiEngine.Character::CharacterType
	int32_t ___CharacterType_4;
	// System.String MoreMountains.CorgiEngine.Character::PlayerID
	String_t* ___PlayerID_5;
	// MoreMountains.CorgiEngine.CharacterStates MoreMountains.CorgiEngine.Character::<CharacterState>k__BackingField
	CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * ___U3CCharacterStateU3Ek__BackingField_6;
	// MoreMountains.CorgiEngine.Character/FacingDirections MoreMountains.CorgiEngine.Character::InitialFacingDirection
	int32_t ___InitialFacingDirection_7;
	// MoreMountains.CorgiEngine.Character/SpawnFacingDirections MoreMountains.CorgiEngine.Character::DirectionOnSpawn
	int32_t ___DirectionOnSpawn_8;
	// System.Boolean MoreMountains.CorgiEngine.Character::<IsFacingRight>k__BackingField
	bool ___U3CIsFacingRightU3Ek__BackingField_9;
	// UnityEngine.Animator MoreMountains.CorgiEngine.Character::CharacterAnimator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___CharacterAnimator_10;
	// System.Boolean MoreMountains.CorgiEngine.Character::UseDefaultMecanim
	bool ___UseDefaultMecanim_11;
	// System.Boolean MoreMountains.CorgiEngine.Character::PerformAnimatorSanityChecks
	bool ___PerformAnimatorSanityChecks_12;
	// System.Boolean MoreMountains.CorgiEngine.Character::DisableAnimatorLogs
	bool ___DisableAnimatorLogs_13;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.Character::CharacterModel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CharacterModel_14;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.Character::CameraTarget
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CameraTarget_15;
	// System.Single MoreMountains.CorgiEngine.Character::CameraTargetSpeed
	float ___CameraTargetSpeed_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.Character::AdditionalAbilityNodes
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___AdditionalAbilityNodes_17;
	// System.Boolean MoreMountains.CorgiEngine.Character::FlipModelOnDirectionChange
	bool ___FlipModelOnDirectionChange_18;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::ModelFlipValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___ModelFlipValue_19;
	// System.Boolean MoreMountains.CorgiEngine.Character::RotateModelOnDirectionChange
	bool ___RotateModelOnDirectionChange_20;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::ModelRotationValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___ModelRotationValue_21;
	// System.Single MoreMountains.CorgiEngine.Character::ModelRotationSpeed
	float ___ModelRotationSpeed_22;
	// MoreMountains.CorgiEngine.Health MoreMountains.CorgiEngine.Character::CharacterHealth
	Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * ___CharacterHealth_23;
	// System.Boolean MoreMountains.CorgiEngine.Character::SendStateChangeEvents
	bool ___SendStateChangeEvents_24;
	// System.Boolean MoreMountains.CorgiEngine.Character::SendStateUpdateEvents
	bool ___SendStateUpdateEvents_25;
	// System.Single MoreMountains.CorgiEngine.Character::AirborneDistance
	float ___AirborneDistance_26;
	// System.Single MoreMountains.CorgiEngine.Character::AirborneMinimumTime
	float ___AirborneMinimumTime_27;
	// MoreMountains.Tools.AIBrain MoreMountains.CorgiEngine.Character::CharacterBrain
	AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE * ___CharacterBrain_28;
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/MovementStates> MoreMountains.CorgiEngine.Character::MovementState
	MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * ___MovementState_29;
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions> MoreMountains.CorgiEngine.Character::ConditionState
	MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * ___ConditionState_30;
	// MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.Character::<SceneCamera>k__BackingField
	CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * ___U3CSceneCameraU3Ek__BackingField_31;
	// MoreMountains.CorgiEngine.InputManager MoreMountains.CorgiEngine.Character::<LinkedInputManager>k__BackingField
	InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * ___U3CLinkedInputManagerU3Ek__BackingField_32;
	// UnityEngine.Animator MoreMountains.CorgiEngine.Character::<_animator>k__BackingField
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___U3C_animatorU3Ek__BackingField_33;
	// System.Collections.Generic.HashSet`1<System.Int32> MoreMountains.CorgiEngine.Character::<_animatorParameters>k__BackingField
	HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 * ___U3C_animatorParametersU3Ek__BackingField_34;
	// System.Boolean MoreMountains.CorgiEngine.Character::<CanFlip>k__BackingField
	bool ___U3CCanFlipU3Ek__BackingField_35;
	// System.Int32 MoreMountains.CorgiEngine.Character::_groundedAnimationParameter
	int32_t ____groundedAnimationParameter_52;
	// System.Int32 MoreMountains.CorgiEngine.Character::_airborneSpeedAnimationParameter
	int32_t ____airborneSpeedAnimationParameter_53;
	// System.Int32 MoreMountains.CorgiEngine.Character::_xSpeedSpeedAnimationParameter
	int32_t ____xSpeedSpeedAnimationParameter_54;
	// System.Int32 MoreMountains.CorgiEngine.Character::_ySpeedSpeedAnimationParameter
	int32_t ____ySpeedSpeedAnimationParameter_55;
	// System.Int32 MoreMountains.CorgiEngine.Character::_worldXSpeedSpeedAnimationParameter
	int32_t ____worldXSpeedSpeedAnimationParameter_56;
	// System.Int32 MoreMountains.CorgiEngine.Character::_worldYSpeedSpeedAnimationParameter
	int32_t ____worldYSpeedSpeedAnimationParameter_57;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingLeftAnimationParameter
	int32_t ____collidingLeftAnimationParameter_58;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingRightAnimationParameter
	int32_t ____collidingRightAnimationParameter_59;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingBelowAnimationParameter
	int32_t ____collidingBelowAnimationParameter_60;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingAboveAnimationParameter
	int32_t ____collidingAboveAnimationParameter_61;
	// System.Int32 MoreMountains.CorgiEngine.Character::_idleSpeedAnimationParameter
	int32_t ____idleSpeedAnimationParameter_62;
	// System.Int32 MoreMountains.CorgiEngine.Character::_aliveAnimationParameter
	int32_t ____aliveAnimationParameter_63;
	// System.Int32 MoreMountains.CorgiEngine.Character::_facingRightAnimationParameter
	int32_t ____facingRightAnimationParameter_64;
	// System.Int32 MoreMountains.CorgiEngine.Character::_randomAnimationParameter
	int32_t ____randomAnimationParameter_65;
	// System.Int32 MoreMountains.CorgiEngine.Character::_randomConstantAnimationParameter
	int32_t ____randomConstantAnimationParameter_66;
	// System.Int32 MoreMountains.CorgiEngine.Character::_flipAnimationParameter
	int32_t ____flipAnimationParameter_67;
	// MoreMountains.CorgiEngine.CorgiController MoreMountains.CorgiEngine.Character::_controller
	CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * ____controller_68;
	// UnityEngine.SpriteRenderer MoreMountains.CorgiEngine.Character::_spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ____spriteRenderer_69;
	// UnityEngine.Color MoreMountains.CorgiEngine.Character::_initialColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____initialColor_70;
	// MoreMountains.CorgiEngine.CharacterAbility[] MoreMountains.CorgiEngine.Character::_characterAbilities
	CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E* ____characterAbilities_71;
	// System.Single MoreMountains.CorgiEngine.Character::_originalGravity
	float ____originalGravity_72;
	// System.Boolean MoreMountains.CorgiEngine.Character::_spawnDirectionForced
	bool ____spawnDirectionForced_73;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::_targetModelRotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____targetModelRotation_74;
	// MoreMountains.CorgiEngine.DamageOnTouch MoreMountains.CorgiEngine.Character::_damageOnTouch
	DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 * ____damageOnTouch_75;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::_cameraTargetInitialPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____cameraTargetInitialPosition_76;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::_cameraOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____cameraOffset_77;
	// System.Boolean MoreMountains.CorgiEngine.Character::_abilitiesCachedOnce
	bool ____abilitiesCachedOnce_78;
	// System.Single MoreMountains.CorgiEngine.Character::_animatorRandomNumber
	float ____animatorRandomNumber_79;
	// MoreMountains.CorgiEngine.CharacterPersistence MoreMountains.CorgiEngine.Character::_characterPersistence
	CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * ____characterPersistence_80;
	// MoreMountains.CorgiEngine.CharacterStates/CharacterConditions MoreMountains.CorgiEngine.Character::_conditionStateBeforeFreeze
	int32_t ____conditionStateBeforeFreeze_81;

public:
	inline static int32_t get_offset_of_CharacterType_4() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterType_4)); }
	inline int32_t get_CharacterType_4() const { return ___CharacterType_4; }
	inline int32_t* get_address_of_CharacterType_4() { return &___CharacterType_4; }
	inline void set_CharacterType_4(int32_t value)
	{
		___CharacterType_4 = value;
	}

	inline static int32_t get_offset_of_PlayerID_5() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___PlayerID_5)); }
	inline String_t* get_PlayerID_5() const { return ___PlayerID_5; }
	inline String_t** get_address_of_PlayerID_5() { return &___PlayerID_5; }
	inline void set_PlayerID_5(String_t* value)
	{
		___PlayerID_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCharacterStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CCharacterStateU3Ek__BackingField_6)); }
	inline CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * get_U3CCharacterStateU3Ek__BackingField_6() const { return ___U3CCharacterStateU3Ek__BackingField_6; }
	inline CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F ** get_address_of_U3CCharacterStateU3Ek__BackingField_6() { return &___U3CCharacterStateU3Ek__BackingField_6; }
	inline void set_U3CCharacterStateU3Ek__BackingField_6(CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * value)
	{
		___U3CCharacterStateU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCharacterStateU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_InitialFacingDirection_7() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___InitialFacingDirection_7)); }
	inline int32_t get_InitialFacingDirection_7() const { return ___InitialFacingDirection_7; }
	inline int32_t* get_address_of_InitialFacingDirection_7() { return &___InitialFacingDirection_7; }
	inline void set_InitialFacingDirection_7(int32_t value)
	{
		___InitialFacingDirection_7 = value;
	}

	inline static int32_t get_offset_of_DirectionOnSpawn_8() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___DirectionOnSpawn_8)); }
	inline int32_t get_DirectionOnSpawn_8() const { return ___DirectionOnSpawn_8; }
	inline int32_t* get_address_of_DirectionOnSpawn_8() { return &___DirectionOnSpawn_8; }
	inline void set_DirectionOnSpawn_8(int32_t value)
	{
		___DirectionOnSpawn_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsFacingRightU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CIsFacingRightU3Ek__BackingField_9)); }
	inline bool get_U3CIsFacingRightU3Ek__BackingField_9() const { return ___U3CIsFacingRightU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsFacingRightU3Ek__BackingField_9() { return &___U3CIsFacingRightU3Ek__BackingField_9; }
	inline void set_U3CIsFacingRightU3Ek__BackingField_9(bool value)
	{
		___U3CIsFacingRightU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_CharacterAnimator_10() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterAnimator_10)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_CharacterAnimator_10() const { return ___CharacterAnimator_10; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_CharacterAnimator_10() { return &___CharacterAnimator_10; }
	inline void set_CharacterAnimator_10(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___CharacterAnimator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterAnimator_10), (void*)value);
	}

	inline static int32_t get_offset_of_UseDefaultMecanim_11() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___UseDefaultMecanim_11)); }
	inline bool get_UseDefaultMecanim_11() const { return ___UseDefaultMecanim_11; }
	inline bool* get_address_of_UseDefaultMecanim_11() { return &___UseDefaultMecanim_11; }
	inline void set_UseDefaultMecanim_11(bool value)
	{
		___UseDefaultMecanim_11 = value;
	}

	inline static int32_t get_offset_of_PerformAnimatorSanityChecks_12() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___PerformAnimatorSanityChecks_12)); }
	inline bool get_PerformAnimatorSanityChecks_12() const { return ___PerformAnimatorSanityChecks_12; }
	inline bool* get_address_of_PerformAnimatorSanityChecks_12() { return &___PerformAnimatorSanityChecks_12; }
	inline void set_PerformAnimatorSanityChecks_12(bool value)
	{
		___PerformAnimatorSanityChecks_12 = value;
	}

	inline static int32_t get_offset_of_DisableAnimatorLogs_13() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___DisableAnimatorLogs_13)); }
	inline bool get_DisableAnimatorLogs_13() const { return ___DisableAnimatorLogs_13; }
	inline bool* get_address_of_DisableAnimatorLogs_13() { return &___DisableAnimatorLogs_13; }
	inline void set_DisableAnimatorLogs_13(bool value)
	{
		___DisableAnimatorLogs_13 = value;
	}

	inline static int32_t get_offset_of_CharacterModel_14() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterModel_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CharacterModel_14() const { return ___CharacterModel_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CharacterModel_14() { return &___CharacterModel_14; }
	inline void set_CharacterModel_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CharacterModel_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterModel_14), (void*)value);
	}

	inline static int32_t get_offset_of_CameraTarget_15() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CameraTarget_15)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CameraTarget_15() const { return ___CameraTarget_15; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CameraTarget_15() { return &___CameraTarget_15; }
	inline void set_CameraTarget_15(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CameraTarget_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CameraTarget_15), (void*)value);
	}

	inline static int32_t get_offset_of_CameraTargetSpeed_16() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CameraTargetSpeed_16)); }
	inline float get_CameraTargetSpeed_16() const { return ___CameraTargetSpeed_16; }
	inline float* get_address_of_CameraTargetSpeed_16() { return &___CameraTargetSpeed_16; }
	inline void set_CameraTargetSpeed_16(float value)
	{
		___CameraTargetSpeed_16 = value;
	}

	inline static int32_t get_offset_of_AdditionalAbilityNodes_17() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___AdditionalAbilityNodes_17)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_AdditionalAbilityNodes_17() const { return ___AdditionalAbilityNodes_17; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_AdditionalAbilityNodes_17() { return &___AdditionalAbilityNodes_17; }
	inline void set_AdditionalAbilityNodes_17(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___AdditionalAbilityNodes_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AdditionalAbilityNodes_17), (void*)value);
	}

	inline static int32_t get_offset_of_FlipModelOnDirectionChange_18() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___FlipModelOnDirectionChange_18)); }
	inline bool get_FlipModelOnDirectionChange_18() const { return ___FlipModelOnDirectionChange_18; }
	inline bool* get_address_of_FlipModelOnDirectionChange_18() { return &___FlipModelOnDirectionChange_18; }
	inline void set_FlipModelOnDirectionChange_18(bool value)
	{
		___FlipModelOnDirectionChange_18 = value;
	}

	inline static int32_t get_offset_of_ModelFlipValue_19() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ModelFlipValue_19)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_ModelFlipValue_19() const { return ___ModelFlipValue_19; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_ModelFlipValue_19() { return &___ModelFlipValue_19; }
	inline void set_ModelFlipValue_19(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___ModelFlipValue_19 = value;
	}

	inline static int32_t get_offset_of_RotateModelOnDirectionChange_20() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___RotateModelOnDirectionChange_20)); }
	inline bool get_RotateModelOnDirectionChange_20() const { return ___RotateModelOnDirectionChange_20; }
	inline bool* get_address_of_RotateModelOnDirectionChange_20() { return &___RotateModelOnDirectionChange_20; }
	inline void set_RotateModelOnDirectionChange_20(bool value)
	{
		___RotateModelOnDirectionChange_20 = value;
	}

	inline static int32_t get_offset_of_ModelRotationValue_21() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ModelRotationValue_21)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_ModelRotationValue_21() const { return ___ModelRotationValue_21; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_ModelRotationValue_21() { return &___ModelRotationValue_21; }
	inline void set_ModelRotationValue_21(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___ModelRotationValue_21 = value;
	}

	inline static int32_t get_offset_of_ModelRotationSpeed_22() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ModelRotationSpeed_22)); }
	inline float get_ModelRotationSpeed_22() const { return ___ModelRotationSpeed_22; }
	inline float* get_address_of_ModelRotationSpeed_22() { return &___ModelRotationSpeed_22; }
	inline void set_ModelRotationSpeed_22(float value)
	{
		___ModelRotationSpeed_22 = value;
	}

	inline static int32_t get_offset_of_CharacterHealth_23() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterHealth_23)); }
	inline Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * get_CharacterHealth_23() const { return ___CharacterHealth_23; }
	inline Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 ** get_address_of_CharacterHealth_23() { return &___CharacterHealth_23; }
	inline void set_CharacterHealth_23(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * value)
	{
		___CharacterHealth_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterHealth_23), (void*)value);
	}

	inline static int32_t get_offset_of_SendStateChangeEvents_24() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___SendStateChangeEvents_24)); }
	inline bool get_SendStateChangeEvents_24() const { return ___SendStateChangeEvents_24; }
	inline bool* get_address_of_SendStateChangeEvents_24() { return &___SendStateChangeEvents_24; }
	inline void set_SendStateChangeEvents_24(bool value)
	{
		___SendStateChangeEvents_24 = value;
	}

	inline static int32_t get_offset_of_SendStateUpdateEvents_25() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___SendStateUpdateEvents_25)); }
	inline bool get_SendStateUpdateEvents_25() const { return ___SendStateUpdateEvents_25; }
	inline bool* get_address_of_SendStateUpdateEvents_25() { return &___SendStateUpdateEvents_25; }
	inline void set_SendStateUpdateEvents_25(bool value)
	{
		___SendStateUpdateEvents_25 = value;
	}

	inline static int32_t get_offset_of_AirborneDistance_26() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___AirborneDistance_26)); }
	inline float get_AirborneDistance_26() const { return ___AirborneDistance_26; }
	inline float* get_address_of_AirborneDistance_26() { return &___AirborneDistance_26; }
	inline void set_AirborneDistance_26(float value)
	{
		___AirborneDistance_26 = value;
	}

	inline static int32_t get_offset_of_AirborneMinimumTime_27() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___AirborneMinimumTime_27)); }
	inline float get_AirborneMinimumTime_27() const { return ___AirborneMinimumTime_27; }
	inline float* get_address_of_AirborneMinimumTime_27() { return &___AirborneMinimumTime_27; }
	inline void set_AirborneMinimumTime_27(float value)
	{
		___AirborneMinimumTime_27 = value;
	}

	inline static int32_t get_offset_of_CharacterBrain_28() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterBrain_28)); }
	inline AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE * get_CharacterBrain_28() const { return ___CharacterBrain_28; }
	inline AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE ** get_address_of_CharacterBrain_28() { return &___CharacterBrain_28; }
	inline void set_CharacterBrain_28(AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE * value)
	{
		___CharacterBrain_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterBrain_28), (void*)value);
	}

	inline static int32_t get_offset_of_MovementState_29() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___MovementState_29)); }
	inline MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * get_MovementState_29() const { return ___MovementState_29; }
	inline MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 ** get_address_of_MovementState_29() { return &___MovementState_29; }
	inline void set_MovementState_29(MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * value)
	{
		___MovementState_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MovementState_29), (void*)value);
	}

	inline static int32_t get_offset_of_ConditionState_30() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ConditionState_30)); }
	inline MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * get_ConditionState_30() const { return ___ConditionState_30; }
	inline MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 ** get_address_of_ConditionState_30() { return &___ConditionState_30; }
	inline void set_ConditionState_30(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * value)
	{
		___ConditionState_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionState_30), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSceneCameraU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CSceneCameraU3Ek__BackingField_31)); }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * get_U3CSceneCameraU3Ek__BackingField_31() const { return ___U3CSceneCameraU3Ek__BackingField_31; }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 ** get_address_of_U3CSceneCameraU3Ek__BackingField_31() { return &___U3CSceneCameraU3Ek__BackingField_31; }
	inline void set_U3CSceneCameraU3Ek__BackingField_31(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * value)
	{
		___U3CSceneCameraU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSceneCameraU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLinkedInputManagerU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CLinkedInputManagerU3Ek__BackingField_32)); }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * get_U3CLinkedInputManagerU3Ek__BackingField_32() const { return ___U3CLinkedInputManagerU3Ek__BackingField_32; }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA ** get_address_of_U3CLinkedInputManagerU3Ek__BackingField_32() { return &___U3CLinkedInputManagerU3Ek__BackingField_32; }
	inline void set_U3CLinkedInputManagerU3Ek__BackingField_32(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * value)
	{
		___U3CLinkedInputManagerU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLinkedInputManagerU3Ek__BackingField_32), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_animatorU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3C_animatorU3Ek__BackingField_33)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_U3C_animatorU3Ek__BackingField_33() const { return ___U3C_animatorU3Ek__BackingField_33; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_U3C_animatorU3Ek__BackingField_33() { return &___U3C_animatorU3Ek__BackingField_33; }
	inline void set_U3C_animatorU3Ek__BackingField_33(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___U3C_animatorU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_animatorU3Ek__BackingField_33), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_animatorParametersU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3C_animatorParametersU3Ek__BackingField_34)); }
	inline HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 * get_U3C_animatorParametersU3Ek__BackingField_34() const { return ___U3C_animatorParametersU3Ek__BackingField_34; }
	inline HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 ** get_address_of_U3C_animatorParametersU3Ek__BackingField_34() { return &___U3C_animatorParametersU3Ek__BackingField_34; }
	inline void set_U3C_animatorParametersU3Ek__BackingField_34(HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 * value)
	{
		___U3C_animatorParametersU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_animatorParametersU3Ek__BackingField_34), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCanFlipU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CCanFlipU3Ek__BackingField_35)); }
	inline bool get_U3CCanFlipU3Ek__BackingField_35() const { return ___U3CCanFlipU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CCanFlipU3Ek__BackingField_35() { return &___U3CCanFlipU3Ek__BackingField_35; }
	inline void set_U3CCanFlipU3Ek__BackingField_35(bool value)
	{
		___U3CCanFlipU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__groundedAnimationParameter_52() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____groundedAnimationParameter_52)); }
	inline int32_t get__groundedAnimationParameter_52() const { return ____groundedAnimationParameter_52; }
	inline int32_t* get_address_of__groundedAnimationParameter_52() { return &____groundedAnimationParameter_52; }
	inline void set__groundedAnimationParameter_52(int32_t value)
	{
		____groundedAnimationParameter_52 = value;
	}

	inline static int32_t get_offset_of__airborneSpeedAnimationParameter_53() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____airborneSpeedAnimationParameter_53)); }
	inline int32_t get__airborneSpeedAnimationParameter_53() const { return ____airborneSpeedAnimationParameter_53; }
	inline int32_t* get_address_of__airborneSpeedAnimationParameter_53() { return &____airborneSpeedAnimationParameter_53; }
	inline void set__airborneSpeedAnimationParameter_53(int32_t value)
	{
		____airborneSpeedAnimationParameter_53 = value;
	}

	inline static int32_t get_offset_of__xSpeedSpeedAnimationParameter_54() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____xSpeedSpeedAnimationParameter_54)); }
	inline int32_t get__xSpeedSpeedAnimationParameter_54() const { return ____xSpeedSpeedAnimationParameter_54; }
	inline int32_t* get_address_of__xSpeedSpeedAnimationParameter_54() { return &____xSpeedSpeedAnimationParameter_54; }
	inline void set__xSpeedSpeedAnimationParameter_54(int32_t value)
	{
		____xSpeedSpeedAnimationParameter_54 = value;
	}

	inline static int32_t get_offset_of__ySpeedSpeedAnimationParameter_55() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____ySpeedSpeedAnimationParameter_55)); }
	inline int32_t get__ySpeedSpeedAnimationParameter_55() const { return ____ySpeedSpeedAnimationParameter_55; }
	inline int32_t* get_address_of__ySpeedSpeedAnimationParameter_55() { return &____ySpeedSpeedAnimationParameter_55; }
	inline void set__ySpeedSpeedAnimationParameter_55(int32_t value)
	{
		____ySpeedSpeedAnimationParameter_55 = value;
	}

	inline static int32_t get_offset_of__worldXSpeedSpeedAnimationParameter_56() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____worldXSpeedSpeedAnimationParameter_56)); }
	inline int32_t get__worldXSpeedSpeedAnimationParameter_56() const { return ____worldXSpeedSpeedAnimationParameter_56; }
	inline int32_t* get_address_of__worldXSpeedSpeedAnimationParameter_56() { return &____worldXSpeedSpeedAnimationParameter_56; }
	inline void set__worldXSpeedSpeedAnimationParameter_56(int32_t value)
	{
		____worldXSpeedSpeedAnimationParameter_56 = value;
	}

	inline static int32_t get_offset_of__worldYSpeedSpeedAnimationParameter_57() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____worldYSpeedSpeedAnimationParameter_57)); }
	inline int32_t get__worldYSpeedSpeedAnimationParameter_57() const { return ____worldYSpeedSpeedAnimationParameter_57; }
	inline int32_t* get_address_of__worldYSpeedSpeedAnimationParameter_57() { return &____worldYSpeedSpeedAnimationParameter_57; }
	inline void set__worldYSpeedSpeedAnimationParameter_57(int32_t value)
	{
		____worldYSpeedSpeedAnimationParameter_57 = value;
	}

	inline static int32_t get_offset_of__collidingLeftAnimationParameter_58() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingLeftAnimationParameter_58)); }
	inline int32_t get__collidingLeftAnimationParameter_58() const { return ____collidingLeftAnimationParameter_58; }
	inline int32_t* get_address_of__collidingLeftAnimationParameter_58() { return &____collidingLeftAnimationParameter_58; }
	inline void set__collidingLeftAnimationParameter_58(int32_t value)
	{
		____collidingLeftAnimationParameter_58 = value;
	}

	inline static int32_t get_offset_of__collidingRightAnimationParameter_59() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingRightAnimationParameter_59)); }
	inline int32_t get__collidingRightAnimationParameter_59() const { return ____collidingRightAnimationParameter_59; }
	inline int32_t* get_address_of__collidingRightAnimationParameter_59() { return &____collidingRightAnimationParameter_59; }
	inline void set__collidingRightAnimationParameter_59(int32_t value)
	{
		____collidingRightAnimationParameter_59 = value;
	}

	inline static int32_t get_offset_of__collidingBelowAnimationParameter_60() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingBelowAnimationParameter_60)); }
	inline int32_t get__collidingBelowAnimationParameter_60() const { return ____collidingBelowAnimationParameter_60; }
	inline int32_t* get_address_of__collidingBelowAnimationParameter_60() { return &____collidingBelowAnimationParameter_60; }
	inline void set__collidingBelowAnimationParameter_60(int32_t value)
	{
		____collidingBelowAnimationParameter_60 = value;
	}

	inline static int32_t get_offset_of__collidingAboveAnimationParameter_61() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingAboveAnimationParameter_61)); }
	inline int32_t get__collidingAboveAnimationParameter_61() const { return ____collidingAboveAnimationParameter_61; }
	inline int32_t* get_address_of__collidingAboveAnimationParameter_61() { return &____collidingAboveAnimationParameter_61; }
	inline void set__collidingAboveAnimationParameter_61(int32_t value)
	{
		____collidingAboveAnimationParameter_61 = value;
	}

	inline static int32_t get_offset_of__idleSpeedAnimationParameter_62() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____idleSpeedAnimationParameter_62)); }
	inline int32_t get__idleSpeedAnimationParameter_62() const { return ____idleSpeedAnimationParameter_62; }
	inline int32_t* get_address_of__idleSpeedAnimationParameter_62() { return &____idleSpeedAnimationParameter_62; }
	inline void set__idleSpeedAnimationParameter_62(int32_t value)
	{
		____idleSpeedAnimationParameter_62 = value;
	}

	inline static int32_t get_offset_of__aliveAnimationParameter_63() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____aliveAnimationParameter_63)); }
	inline int32_t get__aliveAnimationParameter_63() const { return ____aliveAnimationParameter_63; }
	inline int32_t* get_address_of__aliveAnimationParameter_63() { return &____aliveAnimationParameter_63; }
	inline void set__aliveAnimationParameter_63(int32_t value)
	{
		____aliveAnimationParameter_63 = value;
	}

	inline static int32_t get_offset_of__facingRightAnimationParameter_64() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____facingRightAnimationParameter_64)); }
	inline int32_t get__facingRightAnimationParameter_64() const { return ____facingRightAnimationParameter_64; }
	inline int32_t* get_address_of__facingRightAnimationParameter_64() { return &____facingRightAnimationParameter_64; }
	inline void set__facingRightAnimationParameter_64(int32_t value)
	{
		____facingRightAnimationParameter_64 = value;
	}

	inline static int32_t get_offset_of__randomAnimationParameter_65() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____randomAnimationParameter_65)); }
	inline int32_t get__randomAnimationParameter_65() const { return ____randomAnimationParameter_65; }
	inline int32_t* get_address_of__randomAnimationParameter_65() { return &____randomAnimationParameter_65; }
	inline void set__randomAnimationParameter_65(int32_t value)
	{
		____randomAnimationParameter_65 = value;
	}

	inline static int32_t get_offset_of__randomConstantAnimationParameter_66() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____randomConstantAnimationParameter_66)); }
	inline int32_t get__randomConstantAnimationParameter_66() const { return ____randomConstantAnimationParameter_66; }
	inline int32_t* get_address_of__randomConstantAnimationParameter_66() { return &____randomConstantAnimationParameter_66; }
	inline void set__randomConstantAnimationParameter_66(int32_t value)
	{
		____randomConstantAnimationParameter_66 = value;
	}

	inline static int32_t get_offset_of__flipAnimationParameter_67() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____flipAnimationParameter_67)); }
	inline int32_t get__flipAnimationParameter_67() const { return ____flipAnimationParameter_67; }
	inline int32_t* get_address_of__flipAnimationParameter_67() { return &____flipAnimationParameter_67; }
	inline void set__flipAnimationParameter_67(int32_t value)
	{
		____flipAnimationParameter_67 = value;
	}

	inline static int32_t get_offset_of__controller_68() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____controller_68)); }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * get__controller_68() const { return ____controller_68; }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 ** get_address_of__controller_68() { return &____controller_68; }
	inline void set__controller_68(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * value)
	{
		____controller_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____controller_68), (void*)value);
	}

	inline static int32_t get_offset_of__spriteRenderer_69() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____spriteRenderer_69)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get__spriteRenderer_69() const { return ____spriteRenderer_69; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of__spriteRenderer_69() { return &____spriteRenderer_69; }
	inline void set__spriteRenderer_69(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		____spriteRenderer_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____spriteRenderer_69), (void*)value);
	}

	inline static int32_t get_offset_of__initialColor_70() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____initialColor_70)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__initialColor_70() const { return ____initialColor_70; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__initialColor_70() { return &____initialColor_70; }
	inline void set__initialColor_70(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____initialColor_70 = value;
	}

	inline static int32_t get_offset_of__characterAbilities_71() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____characterAbilities_71)); }
	inline CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E* get__characterAbilities_71() const { return ____characterAbilities_71; }
	inline CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E** get_address_of__characterAbilities_71() { return &____characterAbilities_71; }
	inline void set__characterAbilities_71(CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E* value)
	{
		____characterAbilities_71 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterAbilities_71), (void*)value);
	}

	inline static int32_t get_offset_of__originalGravity_72() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____originalGravity_72)); }
	inline float get__originalGravity_72() const { return ____originalGravity_72; }
	inline float* get_address_of__originalGravity_72() { return &____originalGravity_72; }
	inline void set__originalGravity_72(float value)
	{
		____originalGravity_72 = value;
	}

	inline static int32_t get_offset_of__spawnDirectionForced_73() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____spawnDirectionForced_73)); }
	inline bool get__spawnDirectionForced_73() const { return ____spawnDirectionForced_73; }
	inline bool* get_address_of__spawnDirectionForced_73() { return &____spawnDirectionForced_73; }
	inline void set__spawnDirectionForced_73(bool value)
	{
		____spawnDirectionForced_73 = value;
	}

	inline static int32_t get_offset_of__targetModelRotation_74() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____targetModelRotation_74)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__targetModelRotation_74() const { return ____targetModelRotation_74; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__targetModelRotation_74() { return &____targetModelRotation_74; }
	inline void set__targetModelRotation_74(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____targetModelRotation_74 = value;
	}

	inline static int32_t get_offset_of__damageOnTouch_75() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____damageOnTouch_75)); }
	inline DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 * get__damageOnTouch_75() const { return ____damageOnTouch_75; }
	inline DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 ** get_address_of__damageOnTouch_75() { return &____damageOnTouch_75; }
	inline void set__damageOnTouch_75(DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 * value)
	{
		____damageOnTouch_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____damageOnTouch_75), (void*)value);
	}

	inline static int32_t get_offset_of__cameraTargetInitialPosition_76() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____cameraTargetInitialPosition_76)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__cameraTargetInitialPosition_76() const { return ____cameraTargetInitialPosition_76; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__cameraTargetInitialPosition_76() { return &____cameraTargetInitialPosition_76; }
	inline void set__cameraTargetInitialPosition_76(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____cameraTargetInitialPosition_76 = value;
	}

	inline static int32_t get_offset_of__cameraOffset_77() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____cameraOffset_77)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__cameraOffset_77() const { return ____cameraOffset_77; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__cameraOffset_77() { return &____cameraOffset_77; }
	inline void set__cameraOffset_77(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____cameraOffset_77 = value;
	}

	inline static int32_t get_offset_of__abilitiesCachedOnce_78() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____abilitiesCachedOnce_78)); }
	inline bool get__abilitiesCachedOnce_78() const { return ____abilitiesCachedOnce_78; }
	inline bool* get_address_of__abilitiesCachedOnce_78() { return &____abilitiesCachedOnce_78; }
	inline void set__abilitiesCachedOnce_78(bool value)
	{
		____abilitiesCachedOnce_78 = value;
	}

	inline static int32_t get_offset_of__animatorRandomNumber_79() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____animatorRandomNumber_79)); }
	inline float get__animatorRandomNumber_79() const { return ____animatorRandomNumber_79; }
	inline float* get_address_of__animatorRandomNumber_79() { return &____animatorRandomNumber_79; }
	inline void set__animatorRandomNumber_79(float value)
	{
		____animatorRandomNumber_79 = value;
	}

	inline static int32_t get_offset_of__characterPersistence_80() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____characterPersistence_80)); }
	inline CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * get__characterPersistence_80() const { return ____characterPersistence_80; }
	inline CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC ** get_address_of__characterPersistence_80() { return &____characterPersistence_80; }
	inline void set__characterPersistence_80(CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * value)
	{
		____characterPersistence_80 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterPersistence_80), (void*)value);
	}

	inline static int32_t get_offset_of__conditionStateBeforeFreeze_81() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____conditionStateBeforeFreeze_81)); }
	inline int32_t get__conditionStateBeforeFreeze_81() const { return ____conditionStateBeforeFreeze_81; }
	inline int32_t* get_address_of__conditionStateBeforeFreeze_81() { return &____conditionStateBeforeFreeze_81; }
	inline void set__conditionStateBeforeFreeze_81(int32_t value)
	{
		____conditionStateBeforeFreeze_81 = value;
	}
};


// MoreMountains.CorgiEngine.CharacterAbility
struct CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.CharacterAbility::AbilityStartFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___AbilityStartFeedbacks_4;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.CharacterAbility::AbilityStopFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___AbilityStopFeedbacks_5;
	// System.Boolean MoreMountains.CorgiEngine.CharacterAbility::AbilityPermitted
	bool ___AbilityPermitted_6;
	// MoreMountains.CorgiEngine.CharacterStates/MovementStates[] MoreMountains.CorgiEngine.CharacterAbility::BlockingMovementStates
	MovementStatesU5BU5D_t0F46853D479ECD30AFB2839EE5061179A2BCF286* ___BlockingMovementStates_7;
	// MoreMountains.CorgiEngine.CharacterStates/CharacterConditions[] MoreMountains.CorgiEngine.CharacterAbility::BlockingConditionStates
	CharacterConditionsU5BU5D_t3753FF1D9D2ADE37FF1B30173254536B45CAE8B9* ___BlockingConditionStates_8;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.CharacterAbility::_character
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ____character_9;
	// MoreMountains.CorgiEngine.Health MoreMountains.CorgiEngine.CharacterAbility::_health
	Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * ____health_10;
	// MoreMountains.CorgiEngine.CharacterHorizontalMovement MoreMountains.CorgiEngine.CharacterAbility::_characterHorizontalMovement
	CharacterHorizontalMovement_t1CCE148C0A676F29EF20C91D3A9895EC240E5846 * ____characterHorizontalMovement_11;
	// MoreMountains.CorgiEngine.CorgiController MoreMountains.CorgiEngine.CharacterAbility::_controller
	CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * ____controller_12;
	// MoreMountains.CorgiEngine.InputManager MoreMountains.CorgiEngine.CharacterAbility::_inputManager
	InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * ____inputManager_13;
	// MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.CharacterAbility::_sceneCamera
	CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * ____sceneCamera_14;
	// UnityEngine.Animator MoreMountains.CorgiEngine.CharacterAbility::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_15;
	// MoreMountains.CorgiEngine.CharacterStates MoreMountains.CorgiEngine.CharacterAbility::_state
	CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * ____state_16;
	// UnityEngine.SpriteRenderer MoreMountains.CorgiEngine.CharacterAbility::_spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ____spriteRenderer_17;
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/MovementStates> MoreMountains.CorgiEngine.CharacterAbility::_movement
	MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * ____movement_18;
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions> MoreMountains.CorgiEngine.CharacterAbility::_condition
	MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * ____condition_19;
	// System.Boolean MoreMountains.CorgiEngine.CharacterAbility::_abilityInitialized
	bool ____abilityInitialized_20;
	// MoreMountains.CorgiEngine.CharacterGravity MoreMountains.CorgiEngine.CharacterAbility::_characterGravity
	CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * ____characterGravity_21;
	// System.Single MoreMountains.CorgiEngine.CharacterAbility::_verticalInput
	float ____verticalInput_22;
	// System.Single MoreMountains.CorgiEngine.CharacterAbility::_horizontalInput
	float ____horizontalInput_23;
	// System.Boolean MoreMountains.CorgiEngine.CharacterAbility::_startFeedbackIsPlaying
	bool ____startFeedbackIsPlaying_24;

public:
	inline static int32_t get_offset_of_AbilityStartFeedbacks_4() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ___AbilityStartFeedbacks_4)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_AbilityStartFeedbacks_4() const { return ___AbilityStartFeedbacks_4; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_AbilityStartFeedbacks_4() { return &___AbilityStartFeedbacks_4; }
	inline void set_AbilityStartFeedbacks_4(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___AbilityStartFeedbacks_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AbilityStartFeedbacks_4), (void*)value);
	}

	inline static int32_t get_offset_of_AbilityStopFeedbacks_5() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ___AbilityStopFeedbacks_5)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_AbilityStopFeedbacks_5() const { return ___AbilityStopFeedbacks_5; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_AbilityStopFeedbacks_5() { return &___AbilityStopFeedbacks_5; }
	inline void set_AbilityStopFeedbacks_5(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___AbilityStopFeedbacks_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AbilityStopFeedbacks_5), (void*)value);
	}

	inline static int32_t get_offset_of_AbilityPermitted_6() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ___AbilityPermitted_6)); }
	inline bool get_AbilityPermitted_6() const { return ___AbilityPermitted_6; }
	inline bool* get_address_of_AbilityPermitted_6() { return &___AbilityPermitted_6; }
	inline void set_AbilityPermitted_6(bool value)
	{
		___AbilityPermitted_6 = value;
	}

	inline static int32_t get_offset_of_BlockingMovementStates_7() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ___BlockingMovementStates_7)); }
	inline MovementStatesU5BU5D_t0F46853D479ECD30AFB2839EE5061179A2BCF286* get_BlockingMovementStates_7() const { return ___BlockingMovementStates_7; }
	inline MovementStatesU5BU5D_t0F46853D479ECD30AFB2839EE5061179A2BCF286** get_address_of_BlockingMovementStates_7() { return &___BlockingMovementStates_7; }
	inline void set_BlockingMovementStates_7(MovementStatesU5BU5D_t0F46853D479ECD30AFB2839EE5061179A2BCF286* value)
	{
		___BlockingMovementStates_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BlockingMovementStates_7), (void*)value);
	}

	inline static int32_t get_offset_of_BlockingConditionStates_8() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ___BlockingConditionStates_8)); }
	inline CharacterConditionsU5BU5D_t3753FF1D9D2ADE37FF1B30173254536B45CAE8B9* get_BlockingConditionStates_8() const { return ___BlockingConditionStates_8; }
	inline CharacterConditionsU5BU5D_t3753FF1D9D2ADE37FF1B30173254536B45CAE8B9** get_address_of_BlockingConditionStates_8() { return &___BlockingConditionStates_8; }
	inline void set_BlockingConditionStates_8(CharacterConditionsU5BU5D_t3753FF1D9D2ADE37FF1B30173254536B45CAE8B9* value)
	{
		___BlockingConditionStates_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BlockingConditionStates_8), (void*)value);
	}

	inline static int32_t get_offset_of__character_9() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____character_9)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get__character_9() const { return ____character_9; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of__character_9() { return &____character_9; }
	inline void set__character_9(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		____character_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____character_9), (void*)value);
	}

	inline static int32_t get_offset_of__health_10() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____health_10)); }
	inline Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * get__health_10() const { return ____health_10; }
	inline Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 ** get_address_of__health_10() { return &____health_10; }
	inline void set__health_10(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * value)
	{
		____health_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____health_10), (void*)value);
	}

	inline static int32_t get_offset_of__characterHorizontalMovement_11() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____characterHorizontalMovement_11)); }
	inline CharacterHorizontalMovement_t1CCE148C0A676F29EF20C91D3A9895EC240E5846 * get__characterHorizontalMovement_11() const { return ____characterHorizontalMovement_11; }
	inline CharacterHorizontalMovement_t1CCE148C0A676F29EF20C91D3A9895EC240E5846 ** get_address_of__characterHorizontalMovement_11() { return &____characterHorizontalMovement_11; }
	inline void set__characterHorizontalMovement_11(CharacterHorizontalMovement_t1CCE148C0A676F29EF20C91D3A9895EC240E5846 * value)
	{
		____characterHorizontalMovement_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterHorizontalMovement_11), (void*)value);
	}

	inline static int32_t get_offset_of__controller_12() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____controller_12)); }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * get__controller_12() const { return ____controller_12; }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 ** get_address_of__controller_12() { return &____controller_12; }
	inline void set__controller_12(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * value)
	{
		____controller_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____controller_12), (void*)value);
	}

	inline static int32_t get_offset_of__inputManager_13() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____inputManager_13)); }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * get__inputManager_13() const { return ____inputManager_13; }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA ** get_address_of__inputManager_13() { return &____inputManager_13; }
	inline void set__inputManager_13(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * value)
	{
		____inputManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____inputManager_13), (void*)value);
	}

	inline static int32_t get_offset_of__sceneCamera_14() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____sceneCamera_14)); }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * get__sceneCamera_14() const { return ____sceneCamera_14; }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 ** get_address_of__sceneCamera_14() { return &____sceneCamera_14; }
	inline void set__sceneCamera_14(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * value)
	{
		____sceneCamera_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sceneCamera_14), (void*)value);
	}

	inline static int32_t get_offset_of__animator_15() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____animator_15)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_15() const { return ____animator_15; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_15() { return &____animator_15; }
	inline void set__animator_15(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_15), (void*)value);
	}

	inline static int32_t get_offset_of__state_16() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____state_16)); }
	inline CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * get__state_16() const { return ____state_16; }
	inline CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F ** get_address_of__state_16() { return &____state_16; }
	inline void set__state_16(CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * value)
	{
		____state_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____state_16), (void*)value);
	}

	inline static int32_t get_offset_of__spriteRenderer_17() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____spriteRenderer_17)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get__spriteRenderer_17() const { return ____spriteRenderer_17; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of__spriteRenderer_17() { return &____spriteRenderer_17; }
	inline void set__spriteRenderer_17(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		____spriteRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____spriteRenderer_17), (void*)value);
	}

	inline static int32_t get_offset_of__movement_18() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____movement_18)); }
	inline MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * get__movement_18() const { return ____movement_18; }
	inline MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 ** get_address_of__movement_18() { return &____movement_18; }
	inline void set__movement_18(MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * value)
	{
		____movement_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movement_18), (void*)value);
	}

	inline static int32_t get_offset_of__condition_19() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____condition_19)); }
	inline MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * get__condition_19() const { return ____condition_19; }
	inline MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 ** get_address_of__condition_19() { return &____condition_19; }
	inline void set__condition_19(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * value)
	{
		____condition_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____condition_19), (void*)value);
	}

	inline static int32_t get_offset_of__abilityInitialized_20() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____abilityInitialized_20)); }
	inline bool get__abilityInitialized_20() const { return ____abilityInitialized_20; }
	inline bool* get_address_of__abilityInitialized_20() { return &____abilityInitialized_20; }
	inline void set__abilityInitialized_20(bool value)
	{
		____abilityInitialized_20 = value;
	}

	inline static int32_t get_offset_of__characterGravity_21() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____characterGravity_21)); }
	inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * get__characterGravity_21() const { return ____characterGravity_21; }
	inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 ** get_address_of__characterGravity_21() { return &____characterGravity_21; }
	inline void set__characterGravity_21(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * value)
	{
		____characterGravity_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterGravity_21), (void*)value);
	}

	inline static int32_t get_offset_of__verticalInput_22() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____verticalInput_22)); }
	inline float get__verticalInput_22() const { return ____verticalInput_22; }
	inline float* get_address_of__verticalInput_22() { return &____verticalInput_22; }
	inline void set__verticalInput_22(float value)
	{
		____verticalInput_22 = value;
	}

	inline static int32_t get_offset_of__horizontalInput_23() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____horizontalInput_23)); }
	inline float get__horizontalInput_23() const { return ____horizontalInput_23; }
	inline float* get_address_of__horizontalInput_23() { return &____horizontalInput_23; }
	inline void set__horizontalInput_23(float value)
	{
		____horizontalInput_23 = value;
	}

	inline static int32_t get_offset_of__startFeedbackIsPlaying_24() { return static_cast<int32_t>(offsetof(CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7, ____startFeedbackIsPlaying_24)); }
	inline bool get__startFeedbackIsPlaying_24() const { return ____startFeedbackIsPlaying_24; }
	inline bool* get_address_of__startFeedbackIsPlaying_24() { return &____startFeedbackIsPlaying_24; }
	inline void set__startFeedbackIsPlaying_24(bool value)
	{
		____startFeedbackIsPlaying_24 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiController
struct CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.CorgiEngine.CorgiControllerState MoreMountains.CorgiEngine.CorgiController::<State>k__BackingField
	CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 * ___U3CStateU3Ek__BackingField_4;
	// MoreMountains.CorgiEngine.CorgiControllerParameters MoreMountains.CorgiEngine.CorgiController::DefaultParameters
	CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * ___DefaultParameters_5;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::PlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___PlatformMask_6;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::MovingPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___MovingPlatformMask_7;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::OneWayPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___OneWayPlatformMask_8;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::MovingOneWayPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___MovingOneWayPlatformMask_9;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::MidHeightOneWayPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___MidHeightOneWayPlatformMask_10;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::StairsMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___StairsMask_11;
	// MoreMountains.CorgiEngine.CorgiController/DetachmentMethods MoreMountains.CorgiEngine.CorgiController::DetachmentMethod
	int32_t ___DetachmentMethod_12;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::SafeSetTransform
	bool ___SafeSetTransform_13;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::AutomaticallySetPhysicsSettings
	bool ___AutomaticallySetPhysicsSettings_14;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::StandingOn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___StandingOn_15;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::<StandingOnLastFrame>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CStandingOnLastFrameU3Ek__BackingField_16;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.CorgiController::<StandingOnCollider>k__BackingField
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___U3CStandingOnColliderU3Ek__BackingField_17;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::<ForcesApplied>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CForcesAppliedU3Ek__BackingField_18;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::<CurrentWallCollider>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CCurrentWallColliderU3Ek__BackingField_19;
	// MoreMountains.CorgiEngine.CorgiController/UpdateModes MoreMountains.CorgiEngine.CorgiController::UpdateMode
	int32_t ___UpdateMode_20;
	// System.Int32 MoreMountains.CorgiEngine.CorgiController::NumberOfHorizontalRays
	int32_t ___NumberOfHorizontalRays_21;
	// System.Int32 MoreMountains.CorgiEngine.CorgiController::NumberOfVerticalRays
	int32_t ___NumberOfVerticalRays_22;
	// System.Single MoreMountains.CorgiEngine.CorgiController::RayOffsetHorizontal
	float ___RayOffsetHorizontal_23;
	// System.Single MoreMountains.CorgiEngine.CorgiController::RayOffsetVertical
	float ___RayOffsetVertical_24;
	// System.Single MoreMountains.CorgiEngine.CorgiController::CrouchedRaycastLengthMultiplier
	float ___CrouchedRaycastLengthMultiplier_25;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::CastRaysOnBothSides
	bool ___CastRaysOnBothSides_26;
	// System.Single MoreMountains.CorgiEngine.CorgiController::DistanceToTheGroundRayMaximumLength
	float ___DistanceToTheGroundRayMaximumLength_27;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::PerformSafetyBoxcast
	bool ___PerformSafetyBoxcast_28;
	// System.Single MoreMountains.CorgiEngine.CorgiController::OnMovingPlatformRaycastLengthMultiplier
	float ___OnMovingPlatformRaycastLengthMultiplier_29;
	// System.Single MoreMountains.CorgiEngine.CorgiController::ObstacleHeightTolerance
	float ___ObstacleHeightTolerance_30;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::StickToSlopes
	bool ___StickToSlopes_31;
	// System.Single MoreMountains.CorgiEngine.CorgiController::StickyRaycastLength
	float ___StickyRaycastLength_32;
	// System.Single MoreMountains.CorgiEngine.CorgiController::StickToSlopesOffsetY
	float ___StickToSlopesOffsetY_33;
	// System.Single MoreMountains.CorgiEngine.CorgiController::TimeAirborne
	float ___TimeAirborne_34;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::AutomaticGravitySettings
	bool ___AutomaticGravitySettings_35;
	// MoreMountains.CorgiEngine.CorgiControllerParameters MoreMountains.CorgiEngine.CorgiController::_overrideParameters
	CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * ____overrideParameters_36;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_speed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____speed_37;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_friction
	float ____friction_38;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_fallSlowFactor
	float ____fallSlowFactor_39;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_currentGravity
	float ____currentGravity_40;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_externalForce
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____externalForce_41;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_newPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____newPosition_42;
	// UnityEngine.Transform MoreMountains.CorgiEngine.CorgiController::_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____transform_43;
	// UnityEngine.BoxCollider2D MoreMountains.CorgiEngine.CorgiController::_boxCollider
	BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * ____boxCollider_44;
	// MoreMountains.CorgiEngine.CharacterGravity MoreMountains.CorgiEngine.CorgiController::_characterGravity
	CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * ____characterGravity_45;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_platformMaskSave
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____platformMaskSave_46;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_raysBelowLayerMaskPlatforms
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____raysBelowLayerMaskPlatforms_47;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_raysBelowLayerMaskPlatformsWithoutOneWay
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____raysBelowLayerMaskPlatformsWithoutOneWay_48;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_raysBelowLayerMaskPlatformsWithoutMidHeight
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____raysBelowLayerMaskPlatformsWithoutMidHeight_49;
	// System.Int32 MoreMountains.CorgiEngine.CorgiController::_savedBelowLayer
	int32_t ____savedBelowLayer_50;
	// MoreMountains.Tools.MMPathMovement MoreMountains.CorgiEngine.CorgiController::_movingPlatform
	MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * ____movingPlatform_51;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_movingPlatformCurrentGravity
	float ____movingPlatformCurrentGravity_52;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_gravityActive
	bool ____gravityActive_53;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.CorgiController::_ignoredCollider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____ignoredCollider_54;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_collisionsOnWithStairs
	bool ____collisionsOnWithStairs_55;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_originalColliderSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____originalColliderSize_58;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_originalColliderOffset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____originalColliderOffset_59;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_originalSizeRaycastOrigin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____originalSizeRaycastOrigin_60;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_crossBelowSlopeAngle
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____crossBelowSlopeAngle_61;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_sideHitsStorage
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____sideHitsStorage_62;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_belowHitsStorage
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____belowHitsStorage_63;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_aboveHitsStorage
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____aboveHitsStorage_64;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_stickRaycastLeft
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____stickRaycastLeft_65;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_stickRaycastRight
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____stickRaycastRight_66;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_stickRaycast
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____stickRaycast_67;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_distanceToTheGroundRaycast
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____distanceToTheGroundRaycast_68;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_movementDirection
	float ____movementDirection_69;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_storedMovementDirection
	float ____storedMovementDirection_70;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_horizontalRayCastFromBottom
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____horizontalRayCastFromBottom_72;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_horizontalRayCastToTop
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____horizontalRayCastToTop_73;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_verticalRayCastFromLeft
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____verticalRayCastFromLeft_74;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_verticalRayCastToRight
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____verticalRayCastToRight_75;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_aboveRayCastStart
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____aboveRayCastStart_76;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_aboveRayCastEnd
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____aboveRayCastEnd_77;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_rayCastOrigin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____rayCastOrigin_78;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderBottomCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderBottomCenterPosition_79;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderLeftCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderLeftCenterPosition_80;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderRightCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderRightCenterPosition_81;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderTopCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderTopCenterPosition_82;
	// MoreMountains.Tools.MMPathMovement MoreMountains.CorgiEngine.CorgiController::_movingPlatformTest
	MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * ____movingPlatformTest_83;
	// MoreMountains.CorgiEngine.SurfaceModifier MoreMountains.CorgiEngine.CorgiController::_frictionTest
	SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 * ____frictionTest_84;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_update
	bool ____update_85;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_raycastNonAlloc
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____raycastNonAlloc_86;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsTopLeftCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsTopLeftCorner_87;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsBottomLeftCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsBottomLeftCorner_88;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsTopRightCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsTopRightCorner_89;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsBottomRightCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsBottomRightCorner_90;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsCenter
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsCenter_91;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_bounds
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____bounds_92;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_boundsWidth
	float ____boundsWidth_93;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_boundsHeight
	float ____boundsHeight_94;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_distanceToTheGround
	float ____distanceToTheGround_95;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_worldSpeed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____worldSpeed_96;
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D> MoreMountains.CorgiEngine.CorgiController::_contactList
	List_1_t3926283FA9AE49778D95220056CEBFB01D034379 * ____contactList_97;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_shouldComputeNewSpeed
	bool ____shouldComputeNewSpeed_98;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CStateU3Ek__BackingField_4)); }
	inline CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 * get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 ** get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 * value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultParameters_5() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___DefaultParameters_5)); }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * get_DefaultParameters_5() const { return ___DefaultParameters_5; }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 ** get_address_of_DefaultParameters_5() { return &___DefaultParameters_5; }
	inline void set_DefaultParameters_5(CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * value)
	{
		___DefaultParameters_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultParameters_5), (void*)value);
	}

	inline static int32_t get_offset_of_PlatformMask_6() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___PlatformMask_6)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_PlatformMask_6() const { return ___PlatformMask_6; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_PlatformMask_6() { return &___PlatformMask_6; }
	inline void set_PlatformMask_6(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___PlatformMask_6 = value;
	}

	inline static int32_t get_offset_of_MovingPlatformMask_7() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___MovingPlatformMask_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_MovingPlatformMask_7() const { return ___MovingPlatformMask_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_MovingPlatformMask_7() { return &___MovingPlatformMask_7; }
	inline void set_MovingPlatformMask_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___MovingPlatformMask_7 = value;
	}

	inline static int32_t get_offset_of_OneWayPlatformMask_8() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___OneWayPlatformMask_8)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_OneWayPlatformMask_8() const { return ___OneWayPlatformMask_8; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_OneWayPlatformMask_8() { return &___OneWayPlatformMask_8; }
	inline void set_OneWayPlatformMask_8(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___OneWayPlatformMask_8 = value;
	}

	inline static int32_t get_offset_of_MovingOneWayPlatformMask_9() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___MovingOneWayPlatformMask_9)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_MovingOneWayPlatformMask_9() const { return ___MovingOneWayPlatformMask_9; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_MovingOneWayPlatformMask_9() { return &___MovingOneWayPlatformMask_9; }
	inline void set_MovingOneWayPlatformMask_9(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___MovingOneWayPlatformMask_9 = value;
	}

	inline static int32_t get_offset_of_MidHeightOneWayPlatformMask_10() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___MidHeightOneWayPlatformMask_10)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_MidHeightOneWayPlatformMask_10() const { return ___MidHeightOneWayPlatformMask_10; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_MidHeightOneWayPlatformMask_10() { return &___MidHeightOneWayPlatformMask_10; }
	inline void set_MidHeightOneWayPlatformMask_10(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___MidHeightOneWayPlatformMask_10 = value;
	}

	inline static int32_t get_offset_of_StairsMask_11() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StairsMask_11)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_StairsMask_11() const { return ___StairsMask_11; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_StairsMask_11() { return &___StairsMask_11; }
	inline void set_StairsMask_11(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___StairsMask_11 = value;
	}

	inline static int32_t get_offset_of_DetachmentMethod_12() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___DetachmentMethod_12)); }
	inline int32_t get_DetachmentMethod_12() const { return ___DetachmentMethod_12; }
	inline int32_t* get_address_of_DetachmentMethod_12() { return &___DetachmentMethod_12; }
	inline void set_DetachmentMethod_12(int32_t value)
	{
		___DetachmentMethod_12 = value;
	}

	inline static int32_t get_offset_of_SafeSetTransform_13() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___SafeSetTransform_13)); }
	inline bool get_SafeSetTransform_13() const { return ___SafeSetTransform_13; }
	inline bool* get_address_of_SafeSetTransform_13() { return &___SafeSetTransform_13; }
	inline void set_SafeSetTransform_13(bool value)
	{
		___SafeSetTransform_13 = value;
	}

	inline static int32_t get_offset_of_AutomaticallySetPhysicsSettings_14() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___AutomaticallySetPhysicsSettings_14)); }
	inline bool get_AutomaticallySetPhysicsSettings_14() const { return ___AutomaticallySetPhysicsSettings_14; }
	inline bool* get_address_of_AutomaticallySetPhysicsSettings_14() { return &___AutomaticallySetPhysicsSettings_14; }
	inline void set_AutomaticallySetPhysicsSettings_14(bool value)
	{
		___AutomaticallySetPhysicsSettings_14 = value;
	}

	inline static int32_t get_offset_of_StandingOn_15() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StandingOn_15)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_StandingOn_15() const { return ___StandingOn_15; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_StandingOn_15() { return &___StandingOn_15; }
	inline void set_StandingOn_15(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___StandingOn_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StandingOn_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStandingOnLastFrameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CStandingOnLastFrameU3Ek__BackingField_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CStandingOnLastFrameU3Ek__BackingField_16() const { return ___U3CStandingOnLastFrameU3Ek__BackingField_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CStandingOnLastFrameU3Ek__BackingField_16() { return &___U3CStandingOnLastFrameU3Ek__BackingField_16; }
	inline void set_U3CStandingOnLastFrameU3Ek__BackingField_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CStandingOnLastFrameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStandingOnLastFrameU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStandingOnColliderU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CStandingOnColliderU3Ek__BackingField_17)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_U3CStandingOnColliderU3Ek__BackingField_17() const { return ___U3CStandingOnColliderU3Ek__BackingField_17; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_U3CStandingOnColliderU3Ek__BackingField_17() { return &___U3CStandingOnColliderU3Ek__BackingField_17; }
	inline void set_U3CStandingOnColliderU3Ek__BackingField_17(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___U3CStandingOnColliderU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStandingOnColliderU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CForcesAppliedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CForcesAppliedU3Ek__BackingField_18)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CForcesAppliedU3Ek__BackingField_18() const { return ___U3CForcesAppliedU3Ek__BackingField_18; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CForcesAppliedU3Ek__BackingField_18() { return &___U3CForcesAppliedU3Ek__BackingField_18; }
	inline void set_U3CForcesAppliedU3Ek__BackingField_18(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CForcesAppliedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentWallColliderU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CCurrentWallColliderU3Ek__BackingField_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CCurrentWallColliderU3Ek__BackingField_19() const { return ___U3CCurrentWallColliderU3Ek__BackingField_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CCurrentWallColliderU3Ek__BackingField_19() { return &___U3CCurrentWallColliderU3Ek__BackingField_19; }
	inline void set_U3CCurrentWallColliderU3Ek__BackingField_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CCurrentWallColliderU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentWallColliderU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_UpdateMode_20() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___UpdateMode_20)); }
	inline int32_t get_UpdateMode_20() const { return ___UpdateMode_20; }
	inline int32_t* get_address_of_UpdateMode_20() { return &___UpdateMode_20; }
	inline void set_UpdateMode_20(int32_t value)
	{
		___UpdateMode_20 = value;
	}

	inline static int32_t get_offset_of_NumberOfHorizontalRays_21() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___NumberOfHorizontalRays_21)); }
	inline int32_t get_NumberOfHorizontalRays_21() const { return ___NumberOfHorizontalRays_21; }
	inline int32_t* get_address_of_NumberOfHorizontalRays_21() { return &___NumberOfHorizontalRays_21; }
	inline void set_NumberOfHorizontalRays_21(int32_t value)
	{
		___NumberOfHorizontalRays_21 = value;
	}

	inline static int32_t get_offset_of_NumberOfVerticalRays_22() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___NumberOfVerticalRays_22)); }
	inline int32_t get_NumberOfVerticalRays_22() const { return ___NumberOfVerticalRays_22; }
	inline int32_t* get_address_of_NumberOfVerticalRays_22() { return &___NumberOfVerticalRays_22; }
	inline void set_NumberOfVerticalRays_22(int32_t value)
	{
		___NumberOfVerticalRays_22 = value;
	}

	inline static int32_t get_offset_of_RayOffsetHorizontal_23() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___RayOffsetHorizontal_23)); }
	inline float get_RayOffsetHorizontal_23() const { return ___RayOffsetHorizontal_23; }
	inline float* get_address_of_RayOffsetHorizontal_23() { return &___RayOffsetHorizontal_23; }
	inline void set_RayOffsetHorizontal_23(float value)
	{
		___RayOffsetHorizontal_23 = value;
	}

	inline static int32_t get_offset_of_RayOffsetVertical_24() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___RayOffsetVertical_24)); }
	inline float get_RayOffsetVertical_24() const { return ___RayOffsetVertical_24; }
	inline float* get_address_of_RayOffsetVertical_24() { return &___RayOffsetVertical_24; }
	inline void set_RayOffsetVertical_24(float value)
	{
		___RayOffsetVertical_24 = value;
	}

	inline static int32_t get_offset_of_CrouchedRaycastLengthMultiplier_25() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___CrouchedRaycastLengthMultiplier_25)); }
	inline float get_CrouchedRaycastLengthMultiplier_25() const { return ___CrouchedRaycastLengthMultiplier_25; }
	inline float* get_address_of_CrouchedRaycastLengthMultiplier_25() { return &___CrouchedRaycastLengthMultiplier_25; }
	inline void set_CrouchedRaycastLengthMultiplier_25(float value)
	{
		___CrouchedRaycastLengthMultiplier_25 = value;
	}

	inline static int32_t get_offset_of_CastRaysOnBothSides_26() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___CastRaysOnBothSides_26)); }
	inline bool get_CastRaysOnBothSides_26() const { return ___CastRaysOnBothSides_26; }
	inline bool* get_address_of_CastRaysOnBothSides_26() { return &___CastRaysOnBothSides_26; }
	inline void set_CastRaysOnBothSides_26(bool value)
	{
		___CastRaysOnBothSides_26 = value;
	}

	inline static int32_t get_offset_of_DistanceToTheGroundRayMaximumLength_27() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___DistanceToTheGroundRayMaximumLength_27)); }
	inline float get_DistanceToTheGroundRayMaximumLength_27() const { return ___DistanceToTheGroundRayMaximumLength_27; }
	inline float* get_address_of_DistanceToTheGroundRayMaximumLength_27() { return &___DistanceToTheGroundRayMaximumLength_27; }
	inline void set_DistanceToTheGroundRayMaximumLength_27(float value)
	{
		___DistanceToTheGroundRayMaximumLength_27 = value;
	}

	inline static int32_t get_offset_of_PerformSafetyBoxcast_28() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___PerformSafetyBoxcast_28)); }
	inline bool get_PerformSafetyBoxcast_28() const { return ___PerformSafetyBoxcast_28; }
	inline bool* get_address_of_PerformSafetyBoxcast_28() { return &___PerformSafetyBoxcast_28; }
	inline void set_PerformSafetyBoxcast_28(bool value)
	{
		___PerformSafetyBoxcast_28 = value;
	}

	inline static int32_t get_offset_of_OnMovingPlatformRaycastLengthMultiplier_29() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___OnMovingPlatformRaycastLengthMultiplier_29)); }
	inline float get_OnMovingPlatformRaycastLengthMultiplier_29() const { return ___OnMovingPlatformRaycastLengthMultiplier_29; }
	inline float* get_address_of_OnMovingPlatformRaycastLengthMultiplier_29() { return &___OnMovingPlatformRaycastLengthMultiplier_29; }
	inline void set_OnMovingPlatformRaycastLengthMultiplier_29(float value)
	{
		___OnMovingPlatformRaycastLengthMultiplier_29 = value;
	}

	inline static int32_t get_offset_of_ObstacleHeightTolerance_30() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___ObstacleHeightTolerance_30)); }
	inline float get_ObstacleHeightTolerance_30() const { return ___ObstacleHeightTolerance_30; }
	inline float* get_address_of_ObstacleHeightTolerance_30() { return &___ObstacleHeightTolerance_30; }
	inline void set_ObstacleHeightTolerance_30(float value)
	{
		___ObstacleHeightTolerance_30 = value;
	}

	inline static int32_t get_offset_of_StickToSlopes_31() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StickToSlopes_31)); }
	inline bool get_StickToSlopes_31() const { return ___StickToSlopes_31; }
	inline bool* get_address_of_StickToSlopes_31() { return &___StickToSlopes_31; }
	inline void set_StickToSlopes_31(bool value)
	{
		___StickToSlopes_31 = value;
	}

	inline static int32_t get_offset_of_StickyRaycastLength_32() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StickyRaycastLength_32)); }
	inline float get_StickyRaycastLength_32() const { return ___StickyRaycastLength_32; }
	inline float* get_address_of_StickyRaycastLength_32() { return &___StickyRaycastLength_32; }
	inline void set_StickyRaycastLength_32(float value)
	{
		___StickyRaycastLength_32 = value;
	}

	inline static int32_t get_offset_of_StickToSlopesOffsetY_33() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StickToSlopesOffsetY_33)); }
	inline float get_StickToSlopesOffsetY_33() const { return ___StickToSlopesOffsetY_33; }
	inline float* get_address_of_StickToSlopesOffsetY_33() { return &___StickToSlopesOffsetY_33; }
	inline void set_StickToSlopesOffsetY_33(float value)
	{
		___StickToSlopesOffsetY_33 = value;
	}

	inline static int32_t get_offset_of_TimeAirborne_34() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___TimeAirborne_34)); }
	inline float get_TimeAirborne_34() const { return ___TimeAirborne_34; }
	inline float* get_address_of_TimeAirborne_34() { return &___TimeAirborne_34; }
	inline void set_TimeAirborne_34(float value)
	{
		___TimeAirborne_34 = value;
	}

	inline static int32_t get_offset_of_AutomaticGravitySettings_35() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___AutomaticGravitySettings_35)); }
	inline bool get_AutomaticGravitySettings_35() const { return ___AutomaticGravitySettings_35; }
	inline bool* get_address_of_AutomaticGravitySettings_35() { return &___AutomaticGravitySettings_35; }
	inline void set_AutomaticGravitySettings_35(bool value)
	{
		___AutomaticGravitySettings_35 = value;
	}

	inline static int32_t get_offset_of__overrideParameters_36() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____overrideParameters_36)); }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * get__overrideParameters_36() const { return ____overrideParameters_36; }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 ** get_address_of__overrideParameters_36() { return &____overrideParameters_36; }
	inline void set__overrideParameters_36(CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * value)
	{
		____overrideParameters_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____overrideParameters_36), (void*)value);
	}

	inline static int32_t get_offset_of__speed_37() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____speed_37)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__speed_37() const { return ____speed_37; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__speed_37() { return &____speed_37; }
	inline void set__speed_37(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____speed_37 = value;
	}

	inline static int32_t get_offset_of__friction_38() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____friction_38)); }
	inline float get__friction_38() const { return ____friction_38; }
	inline float* get_address_of__friction_38() { return &____friction_38; }
	inline void set__friction_38(float value)
	{
		____friction_38 = value;
	}

	inline static int32_t get_offset_of__fallSlowFactor_39() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____fallSlowFactor_39)); }
	inline float get__fallSlowFactor_39() const { return ____fallSlowFactor_39; }
	inline float* get_address_of__fallSlowFactor_39() { return &____fallSlowFactor_39; }
	inline void set__fallSlowFactor_39(float value)
	{
		____fallSlowFactor_39 = value;
	}

	inline static int32_t get_offset_of__currentGravity_40() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____currentGravity_40)); }
	inline float get__currentGravity_40() const { return ____currentGravity_40; }
	inline float* get_address_of__currentGravity_40() { return &____currentGravity_40; }
	inline void set__currentGravity_40(float value)
	{
		____currentGravity_40 = value;
	}

	inline static int32_t get_offset_of__externalForce_41() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____externalForce_41)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__externalForce_41() const { return ____externalForce_41; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__externalForce_41() { return &____externalForce_41; }
	inline void set__externalForce_41(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____externalForce_41 = value;
	}

	inline static int32_t get_offset_of__newPosition_42() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____newPosition_42)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__newPosition_42() const { return ____newPosition_42; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__newPosition_42() { return &____newPosition_42; }
	inline void set__newPosition_42(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____newPosition_42 = value;
	}

	inline static int32_t get_offset_of__transform_43() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____transform_43)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__transform_43() const { return ____transform_43; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__transform_43() { return &____transform_43; }
	inline void set__transform_43(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____transform_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transform_43), (void*)value);
	}

	inline static int32_t get_offset_of__boxCollider_44() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boxCollider_44)); }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * get__boxCollider_44() const { return ____boxCollider_44; }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 ** get_address_of__boxCollider_44() { return &____boxCollider_44; }
	inline void set__boxCollider_44(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * value)
	{
		____boxCollider_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____boxCollider_44), (void*)value);
	}

	inline static int32_t get_offset_of__characterGravity_45() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____characterGravity_45)); }
	inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * get__characterGravity_45() const { return ____characterGravity_45; }
	inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 ** get_address_of__characterGravity_45() { return &____characterGravity_45; }
	inline void set__characterGravity_45(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * value)
	{
		____characterGravity_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterGravity_45), (void*)value);
	}

	inline static int32_t get_offset_of__platformMaskSave_46() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____platformMaskSave_46)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__platformMaskSave_46() const { return ____platformMaskSave_46; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__platformMaskSave_46() { return &____platformMaskSave_46; }
	inline void set__platformMaskSave_46(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____platformMaskSave_46 = value;
	}

	inline static int32_t get_offset_of__raysBelowLayerMaskPlatforms_47() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raysBelowLayerMaskPlatforms_47)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__raysBelowLayerMaskPlatforms_47() const { return ____raysBelowLayerMaskPlatforms_47; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__raysBelowLayerMaskPlatforms_47() { return &____raysBelowLayerMaskPlatforms_47; }
	inline void set__raysBelowLayerMaskPlatforms_47(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____raysBelowLayerMaskPlatforms_47 = value;
	}

	inline static int32_t get_offset_of__raysBelowLayerMaskPlatformsWithoutOneWay_48() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raysBelowLayerMaskPlatformsWithoutOneWay_48)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__raysBelowLayerMaskPlatformsWithoutOneWay_48() const { return ____raysBelowLayerMaskPlatformsWithoutOneWay_48; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__raysBelowLayerMaskPlatformsWithoutOneWay_48() { return &____raysBelowLayerMaskPlatformsWithoutOneWay_48; }
	inline void set__raysBelowLayerMaskPlatformsWithoutOneWay_48(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____raysBelowLayerMaskPlatformsWithoutOneWay_48 = value;
	}

	inline static int32_t get_offset_of__raysBelowLayerMaskPlatformsWithoutMidHeight_49() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raysBelowLayerMaskPlatformsWithoutMidHeight_49)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__raysBelowLayerMaskPlatformsWithoutMidHeight_49() const { return ____raysBelowLayerMaskPlatformsWithoutMidHeight_49; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__raysBelowLayerMaskPlatformsWithoutMidHeight_49() { return &____raysBelowLayerMaskPlatformsWithoutMidHeight_49; }
	inline void set__raysBelowLayerMaskPlatformsWithoutMidHeight_49(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____raysBelowLayerMaskPlatformsWithoutMidHeight_49 = value;
	}

	inline static int32_t get_offset_of__savedBelowLayer_50() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____savedBelowLayer_50)); }
	inline int32_t get__savedBelowLayer_50() const { return ____savedBelowLayer_50; }
	inline int32_t* get_address_of__savedBelowLayer_50() { return &____savedBelowLayer_50; }
	inline void set__savedBelowLayer_50(int32_t value)
	{
		____savedBelowLayer_50 = value;
	}

	inline static int32_t get_offset_of__movingPlatform_51() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movingPlatform_51)); }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * get__movingPlatform_51() const { return ____movingPlatform_51; }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B ** get_address_of__movingPlatform_51() { return &____movingPlatform_51; }
	inline void set__movingPlatform_51(MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * value)
	{
		____movingPlatform_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movingPlatform_51), (void*)value);
	}

	inline static int32_t get_offset_of__movingPlatformCurrentGravity_52() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movingPlatformCurrentGravity_52)); }
	inline float get__movingPlatformCurrentGravity_52() const { return ____movingPlatformCurrentGravity_52; }
	inline float* get_address_of__movingPlatformCurrentGravity_52() { return &____movingPlatformCurrentGravity_52; }
	inline void set__movingPlatformCurrentGravity_52(float value)
	{
		____movingPlatformCurrentGravity_52 = value;
	}

	inline static int32_t get_offset_of__gravityActive_53() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____gravityActive_53)); }
	inline bool get__gravityActive_53() const { return ____gravityActive_53; }
	inline bool* get_address_of__gravityActive_53() { return &____gravityActive_53; }
	inline void set__gravityActive_53(bool value)
	{
		____gravityActive_53 = value;
	}

	inline static int32_t get_offset_of__ignoredCollider_54() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____ignoredCollider_54)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__ignoredCollider_54() const { return ____ignoredCollider_54; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__ignoredCollider_54() { return &____ignoredCollider_54; }
	inline void set__ignoredCollider_54(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____ignoredCollider_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ignoredCollider_54), (void*)value);
	}

	inline static int32_t get_offset_of__collisionsOnWithStairs_55() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____collisionsOnWithStairs_55)); }
	inline bool get__collisionsOnWithStairs_55() const { return ____collisionsOnWithStairs_55; }
	inline bool* get_address_of__collisionsOnWithStairs_55() { return &____collisionsOnWithStairs_55; }
	inline void set__collisionsOnWithStairs_55(bool value)
	{
		____collisionsOnWithStairs_55 = value;
	}

	inline static int32_t get_offset_of__originalColliderSize_58() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____originalColliderSize_58)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__originalColliderSize_58() const { return ____originalColliderSize_58; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__originalColliderSize_58() { return &____originalColliderSize_58; }
	inline void set__originalColliderSize_58(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____originalColliderSize_58 = value;
	}

	inline static int32_t get_offset_of__originalColliderOffset_59() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____originalColliderOffset_59)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__originalColliderOffset_59() const { return ____originalColliderOffset_59; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__originalColliderOffset_59() { return &____originalColliderOffset_59; }
	inline void set__originalColliderOffset_59(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____originalColliderOffset_59 = value;
	}

	inline static int32_t get_offset_of__originalSizeRaycastOrigin_60() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____originalSizeRaycastOrigin_60)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__originalSizeRaycastOrigin_60() const { return ____originalSizeRaycastOrigin_60; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__originalSizeRaycastOrigin_60() { return &____originalSizeRaycastOrigin_60; }
	inline void set__originalSizeRaycastOrigin_60(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____originalSizeRaycastOrigin_60 = value;
	}

	inline static int32_t get_offset_of__crossBelowSlopeAngle_61() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____crossBelowSlopeAngle_61)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__crossBelowSlopeAngle_61() const { return ____crossBelowSlopeAngle_61; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__crossBelowSlopeAngle_61() { return &____crossBelowSlopeAngle_61; }
	inline void set__crossBelowSlopeAngle_61(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____crossBelowSlopeAngle_61 = value;
	}

	inline static int32_t get_offset_of__sideHitsStorage_62() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____sideHitsStorage_62)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__sideHitsStorage_62() const { return ____sideHitsStorage_62; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__sideHitsStorage_62() { return &____sideHitsStorage_62; }
	inline void set__sideHitsStorage_62(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____sideHitsStorage_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sideHitsStorage_62), (void*)value);
	}

	inline static int32_t get_offset_of__belowHitsStorage_63() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____belowHitsStorage_63)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__belowHitsStorage_63() const { return ____belowHitsStorage_63; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__belowHitsStorage_63() { return &____belowHitsStorage_63; }
	inline void set__belowHitsStorage_63(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____belowHitsStorage_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____belowHitsStorage_63), (void*)value);
	}

	inline static int32_t get_offset_of__aboveHitsStorage_64() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____aboveHitsStorage_64)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__aboveHitsStorage_64() const { return ____aboveHitsStorage_64; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__aboveHitsStorage_64() { return &____aboveHitsStorage_64; }
	inline void set__aboveHitsStorage_64(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____aboveHitsStorage_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____aboveHitsStorage_64), (void*)value);
	}

	inline static int32_t get_offset_of__stickRaycastLeft_65() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____stickRaycastLeft_65)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__stickRaycastLeft_65() const { return ____stickRaycastLeft_65; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__stickRaycastLeft_65() { return &____stickRaycastLeft_65; }
	inline void set__stickRaycastLeft_65(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____stickRaycastLeft_65 = value;
	}

	inline static int32_t get_offset_of__stickRaycastRight_66() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____stickRaycastRight_66)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__stickRaycastRight_66() const { return ____stickRaycastRight_66; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__stickRaycastRight_66() { return &____stickRaycastRight_66; }
	inline void set__stickRaycastRight_66(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____stickRaycastRight_66 = value;
	}

	inline static int32_t get_offset_of__stickRaycast_67() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____stickRaycast_67)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__stickRaycast_67() const { return ____stickRaycast_67; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__stickRaycast_67() { return &____stickRaycast_67; }
	inline void set__stickRaycast_67(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____stickRaycast_67 = value;
	}

	inline static int32_t get_offset_of__distanceToTheGroundRaycast_68() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____distanceToTheGroundRaycast_68)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__distanceToTheGroundRaycast_68() const { return ____distanceToTheGroundRaycast_68; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__distanceToTheGroundRaycast_68() { return &____distanceToTheGroundRaycast_68; }
	inline void set__distanceToTheGroundRaycast_68(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____distanceToTheGroundRaycast_68 = value;
	}

	inline static int32_t get_offset_of__movementDirection_69() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movementDirection_69)); }
	inline float get__movementDirection_69() const { return ____movementDirection_69; }
	inline float* get_address_of__movementDirection_69() { return &____movementDirection_69; }
	inline void set__movementDirection_69(float value)
	{
		____movementDirection_69 = value;
	}

	inline static int32_t get_offset_of__storedMovementDirection_70() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____storedMovementDirection_70)); }
	inline float get__storedMovementDirection_70() const { return ____storedMovementDirection_70; }
	inline float* get_address_of__storedMovementDirection_70() { return &____storedMovementDirection_70; }
	inline void set__storedMovementDirection_70(float value)
	{
		____storedMovementDirection_70 = value;
	}

	inline static int32_t get_offset_of__horizontalRayCastFromBottom_72() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____horizontalRayCastFromBottom_72)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__horizontalRayCastFromBottom_72() const { return ____horizontalRayCastFromBottom_72; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__horizontalRayCastFromBottom_72() { return &____horizontalRayCastFromBottom_72; }
	inline void set__horizontalRayCastFromBottom_72(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____horizontalRayCastFromBottom_72 = value;
	}

	inline static int32_t get_offset_of__horizontalRayCastToTop_73() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____horizontalRayCastToTop_73)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__horizontalRayCastToTop_73() const { return ____horizontalRayCastToTop_73; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__horizontalRayCastToTop_73() { return &____horizontalRayCastToTop_73; }
	inline void set__horizontalRayCastToTop_73(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____horizontalRayCastToTop_73 = value;
	}

	inline static int32_t get_offset_of__verticalRayCastFromLeft_74() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____verticalRayCastFromLeft_74)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__verticalRayCastFromLeft_74() const { return ____verticalRayCastFromLeft_74; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__verticalRayCastFromLeft_74() { return &____verticalRayCastFromLeft_74; }
	inline void set__verticalRayCastFromLeft_74(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____verticalRayCastFromLeft_74 = value;
	}

	inline static int32_t get_offset_of__verticalRayCastToRight_75() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____verticalRayCastToRight_75)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__verticalRayCastToRight_75() const { return ____verticalRayCastToRight_75; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__verticalRayCastToRight_75() { return &____verticalRayCastToRight_75; }
	inline void set__verticalRayCastToRight_75(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____verticalRayCastToRight_75 = value;
	}

	inline static int32_t get_offset_of__aboveRayCastStart_76() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____aboveRayCastStart_76)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__aboveRayCastStart_76() const { return ____aboveRayCastStart_76; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__aboveRayCastStart_76() { return &____aboveRayCastStart_76; }
	inline void set__aboveRayCastStart_76(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____aboveRayCastStart_76 = value;
	}

	inline static int32_t get_offset_of__aboveRayCastEnd_77() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____aboveRayCastEnd_77)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__aboveRayCastEnd_77() const { return ____aboveRayCastEnd_77; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__aboveRayCastEnd_77() { return &____aboveRayCastEnd_77; }
	inline void set__aboveRayCastEnd_77(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____aboveRayCastEnd_77 = value;
	}

	inline static int32_t get_offset_of__rayCastOrigin_78() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____rayCastOrigin_78)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__rayCastOrigin_78() const { return ____rayCastOrigin_78; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__rayCastOrigin_78() { return &____rayCastOrigin_78; }
	inline void set__rayCastOrigin_78(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____rayCastOrigin_78 = value;
	}

	inline static int32_t get_offset_of__colliderBottomCenterPosition_79() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderBottomCenterPosition_79)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderBottomCenterPosition_79() const { return ____colliderBottomCenterPosition_79; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderBottomCenterPosition_79() { return &____colliderBottomCenterPosition_79; }
	inline void set__colliderBottomCenterPosition_79(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderBottomCenterPosition_79 = value;
	}

	inline static int32_t get_offset_of__colliderLeftCenterPosition_80() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderLeftCenterPosition_80)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderLeftCenterPosition_80() const { return ____colliderLeftCenterPosition_80; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderLeftCenterPosition_80() { return &____colliderLeftCenterPosition_80; }
	inline void set__colliderLeftCenterPosition_80(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderLeftCenterPosition_80 = value;
	}

	inline static int32_t get_offset_of__colliderRightCenterPosition_81() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderRightCenterPosition_81)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderRightCenterPosition_81() const { return ____colliderRightCenterPosition_81; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderRightCenterPosition_81() { return &____colliderRightCenterPosition_81; }
	inline void set__colliderRightCenterPosition_81(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderRightCenterPosition_81 = value;
	}

	inline static int32_t get_offset_of__colliderTopCenterPosition_82() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderTopCenterPosition_82)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderTopCenterPosition_82() const { return ____colliderTopCenterPosition_82; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderTopCenterPosition_82() { return &____colliderTopCenterPosition_82; }
	inline void set__colliderTopCenterPosition_82(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderTopCenterPosition_82 = value;
	}

	inline static int32_t get_offset_of__movingPlatformTest_83() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movingPlatformTest_83)); }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * get__movingPlatformTest_83() const { return ____movingPlatformTest_83; }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B ** get_address_of__movingPlatformTest_83() { return &____movingPlatformTest_83; }
	inline void set__movingPlatformTest_83(MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * value)
	{
		____movingPlatformTest_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movingPlatformTest_83), (void*)value);
	}

	inline static int32_t get_offset_of__frictionTest_84() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____frictionTest_84)); }
	inline SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 * get__frictionTest_84() const { return ____frictionTest_84; }
	inline SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 ** get_address_of__frictionTest_84() { return &____frictionTest_84; }
	inline void set__frictionTest_84(SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 * value)
	{
		____frictionTest_84 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frictionTest_84), (void*)value);
	}

	inline static int32_t get_offset_of__update_85() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____update_85)); }
	inline bool get__update_85() const { return ____update_85; }
	inline bool* get_address_of__update_85() { return &____update_85; }
	inline void set__update_85(bool value)
	{
		____update_85 = value;
	}

	inline static int32_t get_offset_of__raycastNonAlloc_86() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raycastNonAlloc_86)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__raycastNonAlloc_86() const { return ____raycastNonAlloc_86; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__raycastNonAlloc_86() { return &____raycastNonAlloc_86; }
	inline void set__raycastNonAlloc_86(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____raycastNonAlloc_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____raycastNonAlloc_86), (void*)value);
	}

	inline static int32_t get_offset_of__boundsTopLeftCorner_87() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsTopLeftCorner_87)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsTopLeftCorner_87() const { return ____boundsTopLeftCorner_87; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsTopLeftCorner_87() { return &____boundsTopLeftCorner_87; }
	inline void set__boundsTopLeftCorner_87(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsTopLeftCorner_87 = value;
	}

	inline static int32_t get_offset_of__boundsBottomLeftCorner_88() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsBottomLeftCorner_88)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsBottomLeftCorner_88() const { return ____boundsBottomLeftCorner_88; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsBottomLeftCorner_88() { return &____boundsBottomLeftCorner_88; }
	inline void set__boundsBottomLeftCorner_88(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsBottomLeftCorner_88 = value;
	}

	inline static int32_t get_offset_of__boundsTopRightCorner_89() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsTopRightCorner_89)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsTopRightCorner_89() const { return ____boundsTopRightCorner_89; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsTopRightCorner_89() { return &____boundsTopRightCorner_89; }
	inline void set__boundsTopRightCorner_89(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsTopRightCorner_89 = value;
	}

	inline static int32_t get_offset_of__boundsBottomRightCorner_90() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsBottomRightCorner_90)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsBottomRightCorner_90() const { return ____boundsBottomRightCorner_90; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsBottomRightCorner_90() { return &____boundsBottomRightCorner_90; }
	inline void set__boundsBottomRightCorner_90(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsBottomRightCorner_90 = value;
	}

	inline static int32_t get_offset_of__boundsCenter_91() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsCenter_91)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsCenter_91() const { return ____boundsCenter_91; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsCenter_91() { return &____boundsCenter_91; }
	inline void set__boundsCenter_91(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsCenter_91 = value;
	}

	inline static int32_t get_offset_of__bounds_92() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____bounds_92)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__bounds_92() const { return ____bounds_92; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__bounds_92() { return &____bounds_92; }
	inline void set__bounds_92(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____bounds_92 = value;
	}

	inline static int32_t get_offset_of__boundsWidth_93() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsWidth_93)); }
	inline float get__boundsWidth_93() const { return ____boundsWidth_93; }
	inline float* get_address_of__boundsWidth_93() { return &____boundsWidth_93; }
	inline void set__boundsWidth_93(float value)
	{
		____boundsWidth_93 = value;
	}

	inline static int32_t get_offset_of__boundsHeight_94() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsHeight_94)); }
	inline float get__boundsHeight_94() const { return ____boundsHeight_94; }
	inline float* get_address_of__boundsHeight_94() { return &____boundsHeight_94; }
	inline void set__boundsHeight_94(float value)
	{
		____boundsHeight_94 = value;
	}

	inline static int32_t get_offset_of__distanceToTheGround_95() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____distanceToTheGround_95)); }
	inline float get__distanceToTheGround_95() const { return ____distanceToTheGround_95; }
	inline float* get_address_of__distanceToTheGround_95() { return &____distanceToTheGround_95; }
	inline void set__distanceToTheGround_95(float value)
	{
		____distanceToTheGround_95 = value;
	}

	inline static int32_t get_offset_of__worldSpeed_96() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____worldSpeed_96)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__worldSpeed_96() const { return ____worldSpeed_96; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__worldSpeed_96() { return &____worldSpeed_96; }
	inline void set__worldSpeed_96(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____worldSpeed_96 = value;
	}

	inline static int32_t get_offset_of__contactList_97() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____contactList_97)); }
	inline List_1_t3926283FA9AE49778D95220056CEBFB01D034379 * get__contactList_97() const { return ____contactList_97; }
	inline List_1_t3926283FA9AE49778D95220056CEBFB01D034379 ** get_address_of__contactList_97() { return &____contactList_97; }
	inline void set__contactList_97(List_1_t3926283FA9AE49778D95220056CEBFB01D034379 * value)
	{
		____contactList_97 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____contactList_97), (void*)value);
	}

	inline static int32_t get_offset_of__shouldComputeNewSpeed_98() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____shouldComputeNewSpeed_98)); }
	inline bool get__shouldComputeNewSpeed_98() const { return ____shouldComputeNewSpeed_98; }
	inline bool* get_address_of__shouldComputeNewSpeed_98() { return &____shouldComputeNewSpeed_98; }
	inline void set__shouldComputeNewSpeed_98(bool value)
	{
		____shouldComputeNewSpeed_98 = value;
	}
};


// MoreMountains.CorgiEngine.Health
struct Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Health::CurrentHealth
	int32_t ___CurrentHealth_4;
	// System.Int32 MoreMountains.CorgiEngine.Health::InitialHealth
	int32_t ___InitialHealth_5;
	// System.Int32 MoreMountains.CorgiEngine.Health::MaximumHealth
	int32_t ___MaximumHealth_6;
	// System.Boolean MoreMountains.CorgiEngine.Health::Invulnerable
	bool ___Invulnerable_7;
	// System.Boolean MoreMountains.CorgiEngine.Health::TemporarilyInvulnerable
	bool ___TemporarilyInvulnerable_8;
	// System.Boolean MoreMountains.CorgiEngine.Health::PostDamageInvulnerable
	bool ___PostDamageInvulnerable_9;
	// System.Boolean MoreMountains.CorgiEngine.Health::ImmuneToDamage
	bool ___ImmuneToDamage_10;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Health::DamageFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___DamageFeedbacks_11;
	// System.Boolean MoreMountains.CorgiEngine.Health::FeedbackIsProportionalToDamage
	bool ___FeedbackIsProportionalToDamage_12;
	// System.Boolean MoreMountains.CorgiEngine.Health::FlickerSpriteOnHit
	bool ___FlickerSpriteOnHit_13;
	// UnityEngine.Color MoreMountains.CorgiEngine.Health::FlickerColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___FlickerColor_14;
	// System.Boolean MoreMountains.CorgiEngine.Health::ImmuneToKnockback
	bool ___ImmuneToKnockback_15;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Health::DeathFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___DeathFeedbacks_16;
	// System.Boolean MoreMountains.CorgiEngine.Health::DestroyOnDeath
	bool ___DestroyOnDeath_17;
	// System.Single MoreMountains.CorgiEngine.Health::DelayBeforeDestruction
	float ___DelayBeforeDestruction_18;
	// System.Boolean MoreMountains.CorgiEngine.Health::CollisionsOffOnDeath
	bool ___CollisionsOffOnDeath_19;
	// System.Boolean MoreMountains.CorgiEngine.Health::GravityOffOnDeath
	bool ___GravityOffOnDeath_20;
	// System.Int32 MoreMountains.CorgiEngine.Health::PointsWhenDestroyed
	int32_t ___PointsWhenDestroyed_21;
	// System.Boolean MoreMountains.CorgiEngine.Health::RespawnAtInitialLocation
	bool ___RespawnAtInitialLocation_22;
	// System.Boolean MoreMountains.CorgiEngine.Health::ApplyDeathForce
	bool ___ApplyDeathForce_23;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.Health::DeathForce
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___DeathForce_24;
	// System.Boolean MoreMountains.CorgiEngine.Health::ResetForcesOnDeath
	bool ___ResetForcesOnDeath_25;
	// System.Boolean MoreMountains.CorgiEngine.Health::ResetColorOnRevive
	bool ___ResetColorOnRevive_26;
	// System.String MoreMountains.CorgiEngine.Health::ColorMaterialPropertyName
	String_t* ___ColorMaterialPropertyName_27;
	// System.Boolean MoreMountains.CorgiEngine.Health::UseMaterialPropertyBlocks
	bool ___UseMaterialPropertyBlocks_28;
	// System.Int32 MoreMountains.CorgiEngine.Health::<LastDamage>k__BackingField
	int32_t ___U3CLastDamageU3Ek__BackingField_29;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Health::<LastDamageDirection>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CLastDamageDirectionU3Ek__BackingField_30;
	// MoreMountains.CorgiEngine.Health/OnHitDelegate MoreMountains.CorgiEngine.Health::OnHit
	OnHitDelegate_tE3BFE274661AEA8BAF51E9384B69489DE6D10E75 * ___OnHit_31;
	// MoreMountains.CorgiEngine.Health/OnHitZeroDelegate MoreMountains.CorgiEngine.Health::OnHitZero
	OnHitZeroDelegate_tD23B05006B609BA538F06C37C939E2E0C1B34314 * ___OnHitZero_32;
	// MoreMountains.CorgiEngine.Health/OnReviveDelegate MoreMountains.CorgiEngine.Health::OnRevive
	OnReviveDelegate_tF96E17726864907F607E6A0A2C5D837C52A70298 * ___OnRevive_33;
	// MoreMountains.CorgiEngine.Health/OnDeathDelegate MoreMountains.CorgiEngine.Health::OnDeath
	OnDeathDelegate_t9F947C81F473BA238CBC47BF4A8C58D2291A2B75 * ___OnDeath_34;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Health::_initialPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____initialPosition_35;
	// UnityEngine.Color MoreMountains.CorgiEngine.Health::_initialColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____initialColor_36;
	// UnityEngine.Renderer MoreMountains.CorgiEngine.Health::_renderer
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ____renderer_37;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.Health::_character
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ____character_38;
	// MoreMountains.CorgiEngine.CorgiController MoreMountains.CorgiEngine.Health::_controller
	CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * ____controller_39;
	// MoreMountains.Tools.MMHealthBar MoreMountains.CorgiEngine.Health::_healthBar
	MMHealthBar_t70CB8C2C73CB0B41B85104B0CC03A99EF0F86917 * ____healthBar_40;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.Health::_collider2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____collider2D_41;
	// System.Boolean MoreMountains.CorgiEngine.Health::_initialized
	bool ____initialized_42;
	// MoreMountains.CorgiEngine.AutoRespawn MoreMountains.CorgiEngine.Health::_autoRespawn
	AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 * ____autoRespawn_43;
	// UnityEngine.Animator MoreMountains.CorgiEngine.Health::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_44;
	// MoreMountains.CorgiEngine.CharacterPersistence MoreMountains.CorgiEngine.Health::_characterPersistence
	CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * ____characterPersistence_45;
	// UnityEngine.MaterialPropertyBlock MoreMountains.CorgiEngine.Health::_propertyBlock
	MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * ____propertyBlock_46;
	// System.Boolean MoreMountains.CorgiEngine.Health::_hasColorProperty
	bool ____hasColorProperty_47;

public:
	inline static int32_t get_offset_of_CurrentHealth_4() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___CurrentHealth_4)); }
	inline int32_t get_CurrentHealth_4() const { return ___CurrentHealth_4; }
	inline int32_t* get_address_of_CurrentHealth_4() { return &___CurrentHealth_4; }
	inline void set_CurrentHealth_4(int32_t value)
	{
		___CurrentHealth_4 = value;
	}

	inline static int32_t get_offset_of_InitialHealth_5() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___InitialHealth_5)); }
	inline int32_t get_InitialHealth_5() const { return ___InitialHealth_5; }
	inline int32_t* get_address_of_InitialHealth_5() { return &___InitialHealth_5; }
	inline void set_InitialHealth_5(int32_t value)
	{
		___InitialHealth_5 = value;
	}

	inline static int32_t get_offset_of_MaximumHealth_6() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___MaximumHealth_6)); }
	inline int32_t get_MaximumHealth_6() const { return ___MaximumHealth_6; }
	inline int32_t* get_address_of_MaximumHealth_6() { return &___MaximumHealth_6; }
	inline void set_MaximumHealth_6(int32_t value)
	{
		___MaximumHealth_6 = value;
	}

	inline static int32_t get_offset_of_Invulnerable_7() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___Invulnerable_7)); }
	inline bool get_Invulnerable_7() const { return ___Invulnerable_7; }
	inline bool* get_address_of_Invulnerable_7() { return &___Invulnerable_7; }
	inline void set_Invulnerable_7(bool value)
	{
		___Invulnerable_7 = value;
	}

	inline static int32_t get_offset_of_TemporarilyInvulnerable_8() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___TemporarilyInvulnerable_8)); }
	inline bool get_TemporarilyInvulnerable_8() const { return ___TemporarilyInvulnerable_8; }
	inline bool* get_address_of_TemporarilyInvulnerable_8() { return &___TemporarilyInvulnerable_8; }
	inline void set_TemporarilyInvulnerable_8(bool value)
	{
		___TemporarilyInvulnerable_8 = value;
	}

	inline static int32_t get_offset_of_PostDamageInvulnerable_9() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___PostDamageInvulnerable_9)); }
	inline bool get_PostDamageInvulnerable_9() const { return ___PostDamageInvulnerable_9; }
	inline bool* get_address_of_PostDamageInvulnerable_9() { return &___PostDamageInvulnerable_9; }
	inline void set_PostDamageInvulnerable_9(bool value)
	{
		___PostDamageInvulnerable_9 = value;
	}

	inline static int32_t get_offset_of_ImmuneToDamage_10() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___ImmuneToDamage_10)); }
	inline bool get_ImmuneToDamage_10() const { return ___ImmuneToDamage_10; }
	inline bool* get_address_of_ImmuneToDamage_10() { return &___ImmuneToDamage_10; }
	inline void set_ImmuneToDamage_10(bool value)
	{
		___ImmuneToDamage_10 = value;
	}

	inline static int32_t get_offset_of_DamageFeedbacks_11() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___DamageFeedbacks_11)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_DamageFeedbacks_11() const { return ___DamageFeedbacks_11; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_DamageFeedbacks_11() { return &___DamageFeedbacks_11; }
	inline void set_DamageFeedbacks_11(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___DamageFeedbacks_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DamageFeedbacks_11), (void*)value);
	}

	inline static int32_t get_offset_of_FeedbackIsProportionalToDamage_12() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___FeedbackIsProportionalToDamage_12)); }
	inline bool get_FeedbackIsProportionalToDamage_12() const { return ___FeedbackIsProportionalToDamage_12; }
	inline bool* get_address_of_FeedbackIsProportionalToDamage_12() { return &___FeedbackIsProportionalToDamage_12; }
	inline void set_FeedbackIsProportionalToDamage_12(bool value)
	{
		___FeedbackIsProportionalToDamage_12 = value;
	}

	inline static int32_t get_offset_of_FlickerSpriteOnHit_13() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___FlickerSpriteOnHit_13)); }
	inline bool get_FlickerSpriteOnHit_13() const { return ___FlickerSpriteOnHit_13; }
	inline bool* get_address_of_FlickerSpriteOnHit_13() { return &___FlickerSpriteOnHit_13; }
	inline void set_FlickerSpriteOnHit_13(bool value)
	{
		___FlickerSpriteOnHit_13 = value;
	}

	inline static int32_t get_offset_of_FlickerColor_14() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___FlickerColor_14)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_FlickerColor_14() const { return ___FlickerColor_14; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_FlickerColor_14() { return &___FlickerColor_14; }
	inline void set_FlickerColor_14(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___FlickerColor_14 = value;
	}

	inline static int32_t get_offset_of_ImmuneToKnockback_15() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___ImmuneToKnockback_15)); }
	inline bool get_ImmuneToKnockback_15() const { return ___ImmuneToKnockback_15; }
	inline bool* get_address_of_ImmuneToKnockback_15() { return &___ImmuneToKnockback_15; }
	inline void set_ImmuneToKnockback_15(bool value)
	{
		___ImmuneToKnockback_15 = value;
	}

	inline static int32_t get_offset_of_DeathFeedbacks_16() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___DeathFeedbacks_16)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_DeathFeedbacks_16() const { return ___DeathFeedbacks_16; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_DeathFeedbacks_16() { return &___DeathFeedbacks_16; }
	inline void set_DeathFeedbacks_16(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___DeathFeedbacks_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeathFeedbacks_16), (void*)value);
	}

	inline static int32_t get_offset_of_DestroyOnDeath_17() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___DestroyOnDeath_17)); }
	inline bool get_DestroyOnDeath_17() const { return ___DestroyOnDeath_17; }
	inline bool* get_address_of_DestroyOnDeath_17() { return &___DestroyOnDeath_17; }
	inline void set_DestroyOnDeath_17(bool value)
	{
		___DestroyOnDeath_17 = value;
	}

	inline static int32_t get_offset_of_DelayBeforeDestruction_18() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___DelayBeforeDestruction_18)); }
	inline float get_DelayBeforeDestruction_18() const { return ___DelayBeforeDestruction_18; }
	inline float* get_address_of_DelayBeforeDestruction_18() { return &___DelayBeforeDestruction_18; }
	inline void set_DelayBeforeDestruction_18(float value)
	{
		___DelayBeforeDestruction_18 = value;
	}

	inline static int32_t get_offset_of_CollisionsOffOnDeath_19() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___CollisionsOffOnDeath_19)); }
	inline bool get_CollisionsOffOnDeath_19() const { return ___CollisionsOffOnDeath_19; }
	inline bool* get_address_of_CollisionsOffOnDeath_19() { return &___CollisionsOffOnDeath_19; }
	inline void set_CollisionsOffOnDeath_19(bool value)
	{
		___CollisionsOffOnDeath_19 = value;
	}

	inline static int32_t get_offset_of_GravityOffOnDeath_20() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___GravityOffOnDeath_20)); }
	inline bool get_GravityOffOnDeath_20() const { return ___GravityOffOnDeath_20; }
	inline bool* get_address_of_GravityOffOnDeath_20() { return &___GravityOffOnDeath_20; }
	inline void set_GravityOffOnDeath_20(bool value)
	{
		___GravityOffOnDeath_20 = value;
	}

	inline static int32_t get_offset_of_PointsWhenDestroyed_21() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___PointsWhenDestroyed_21)); }
	inline int32_t get_PointsWhenDestroyed_21() const { return ___PointsWhenDestroyed_21; }
	inline int32_t* get_address_of_PointsWhenDestroyed_21() { return &___PointsWhenDestroyed_21; }
	inline void set_PointsWhenDestroyed_21(int32_t value)
	{
		___PointsWhenDestroyed_21 = value;
	}

	inline static int32_t get_offset_of_RespawnAtInitialLocation_22() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___RespawnAtInitialLocation_22)); }
	inline bool get_RespawnAtInitialLocation_22() const { return ___RespawnAtInitialLocation_22; }
	inline bool* get_address_of_RespawnAtInitialLocation_22() { return &___RespawnAtInitialLocation_22; }
	inline void set_RespawnAtInitialLocation_22(bool value)
	{
		___RespawnAtInitialLocation_22 = value;
	}

	inline static int32_t get_offset_of_ApplyDeathForce_23() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___ApplyDeathForce_23)); }
	inline bool get_ApplyDeathForce_23() const { return ___ApplyDeathForce_23; }
	inline bool* get_address_of_ApplyDeathForce_23() { return &___ApplyDeathForce_23; }
	inline void set_ApplyDeathForce_23(bool value)
	{
		___ApplyDeathForce_23 = value;
	}

	inline static int32_t get_offset_of_DeathForce_24() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___DeathForce_24)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_DeathForce_24() const { return ___DeathForce_24; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_DeathForce_24() { return &___DeathForce_24; }
	inline void set_DeathForce_24(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___DeathForce_24 = value;
	}

	inline static int32_t get_offset_of_ResetForcesOnDeath_25() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___ResetForcesOnDeath_25)); }
	inline bool get_ResetForcesOnDeath_25() const { return ___ResetForcesOnDeath_25; }
	inline bool* get_address_of_ResetForcesOnDeath_25() { return &___ResetForcesOnDeath_25; }
	inline void set_ResetForcesOnDeath_25(bool value)
	{
		___ResetForcesOnDeath_25 = value;
	}

	inline static int32_t get_offset_of_ResetColorOnRevive_26() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___ResetColorOnRevive_26)); }
	inline bool get_ResetColorOnRevive_26() const { return ___ResetColorOnRevive_26; }
	inline bool* get_address_of_ResetColorOnRevive_26() { return &___ResetColorOnRevive_26; }
	inline void set_ResetColorOnRevive_26(bool value)
	{
		___ResetColorOnRevive_26 = value;
	}

	inline static int32_t get_offset_of_ColorMaterialPropertyName_27() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___ColorMaterialPropertyName_27)); }
	inline String_t* get_ColorMaterialPropertyName_27() const { return ___ColorMaterialPropertyName_27; }
	inline String_t** get_address_of_ColorMaterialPropertyName_27() { return &___ColorMaterialPropertyName_27; }
	inline void set_ColorMaterialPropertyName_27(String_t* value)
	{
		___ColorMaterialPropertyName_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ColorMaterialPropertyName_27), (void*)value);
	}

	inline static int32_t get_offset_of_UseMaterialPropertyBlocks_28() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___UseMaterialPropertyBlocks_28)); }
	inline bool get_UseMaterialPropertyBlocks_28() const { return ___UseMaterialPropertyBlocks_28; }
	inline bool* get_address_of_UseMaterialPropertyBlocks_28() { return &___UseMaterialPropertyBlocks_28; }
	inline void set_UseMaterialPropertyBlocks_28(bool value)
	{
		___UseMaterialPropertyBlocks_28 = value;
	}

	inline static int32_t get_offset_of_U3CLastDamageU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___U3CLastDamageU3Ek__BackingField_29)); }
	inline int32_t get_U3CLastDamageU3Ek__BackingField_29() const { return ___U3CLastDamageU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CLastDamageU3Ek__BackingField_29() { return &___U3CLastDamageU3Ek__BackingField_29; }
	inline void set_U3CLastDamageU3Ek__BackingField_29(int32_t value)
	{
		___U3CLastDamageU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CLastDamageDirectionU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___U3CLastDamageDirectionU3Ek__BackingField_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CLastDamageDirectionU3Ek__BackingField_30() const { return ___U3CLastDamageDirectionU3Ek__BackingField_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CLastDamageDirectionU3Ek__BackingField_30() { return &___U3CLastDamageDirectionU3Ek__BackingField_30; }
	inline void set_U3CLastDamageDirectionU3Ek__BackingField_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CLastDamageDirectionU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_OnHit_31() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___OnHit_31)); }
	inline OnHitDelegate_tE3BFE274661AEA8BAF51E9384B69489DE6D10E75 * get_OnHit_31() const { return ___OnHit_31; }
	inline OnHitDelegate_tE3BFE274661AEA8BAF51E9384B69489DE6D10E75 ** get_address_of_OnHit_31() { return &___OnHit_31; }
	inline void set_OnHit_31(OnHitDelegate_tE3BFE274661AEA8BAF51E9384B69489DE6D10E75 * value)
	{
		___OnHit_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHit_31), (void*)value);
	}

	inline static int32_t get_offset_of_OnHitZero_32() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___OnHitZero_32)); }
	inline OnHitZeroDelegate_tD23B05006B609BA538F06C37C939E2E0C1B34314 * get_OnHitZero_32() const { return ___OnHitZero_32; }
	inline OnHitZeroDelegate_tD23B05006B609BA538F06C37C939E2E0C1B34314 ** get_address_of_OnHitZero_32() { return &___OnHitZero_32; }
	inline void set_OnHitZero_32(OnHitZeroDelegate_tD23B05006B609BA538F06C37C939E2E0C1B34314 * value)
	{
		___OnHitZero_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnHitZero_32), (void*)value);
	}

	inline static int32_t get_offset_of_OnRevive_33() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___OnRevive_33)); }
	inline OnReviveDelegate_tF96E17726864907F607E6A0A2C5D837C52A70298 * get_OnRevive_33() const { return ___OnRevive_33; }
	inline OnReviveDelegate_tF96E17726864907F607E6A0A2C5D837C52A70298 ** get_address_of_OnRevive_33() { return &___OnRevive_33; }
	inline void set_OnRevive_33(OnReviveDelegate_tF96E17726864907F607E6A0A2C5D837C52A70298 * value)
	{
		___OnRevive_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRevive_33), (void*)value);
	}

	inline static int32_t get_offset_of_OnDeath_34() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ___OnDeath_34)); }
	inline OnDeathDelegate_t9F947C81F473BA238CBC47BF4A8C58D2291A2B75 * get_OnDeath_34() const { return ___OnDeath_34; }
	inline OnDeathDelegate_t9F947C81F473BA238CBC47BF4A8C58D2291A2B75 ** get_address_of_OnDeath_34() { return &___OnDeath_34; }
	inline void set_OnDeath_34(OnDeathDelegate_t9F947C81F473BA238CBC47BF4A8C58D2291A2B75 * value)
	{
		___OnDeath_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnDeath_34), (void*)value);
	}

	inline static int32_t get_offset_of__initialPosition_35() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____initialPosition_35)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__initialPosition_35() const { return ____initialPosition_35; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__initialPosition_35() { return &____initialPosition_35; }
	inline void set__initialPosition_35(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____initialPosition_35 = value;
	}

	inline static int32_t get_offset_of__initialColor_36() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____initialColor_36)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__initialColor_36() const { return ____initialColor_36; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__initialColor_36() { return &____initialColor_36; }
	inline void set__initialColor_36(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____initialColor_36 = value;
	}

	inline static int32_t get_offset_of__renderer_37() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____renderer_37)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get__renderer_37() const { return ____renderer_37; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of__renderer_37() { return &____renderer_37; }
	inline void set__renderer_37(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		____renderer_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____renderer_37), (void*)value);
	}

	inline static int32_t get_offset_of__character_38() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____character_38)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get__character_38() const { return ____character_38; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of__character_38() { return &____character_38; }
	inline void set__character_38(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		____character_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____character_38), (void*)value);
	}

	inline static int32_t get_offset_of__controller_39() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____controller_39)); }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * get__controller_39() const { return ____controller_39; }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 ** get_address_of__controller_39() { return &____controller_39; }
	inline void set__controller_39(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * value)
	{
		____controller_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____controller_39), (void*)value);
	}

	inline static int32_t get_offset_of__healthBar_40() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____healthBar_40)); }
	inline MMHealthBar_t70CB8C2C73CB0B41B85104B0CC03A99EF0F86917 * get__healthBar_40() const { return ____healthBar_40; }
	inline MMHealthBar_t70CB8C2C73CB0B41B85104B0CC03A99EF0F86917 ** get_address_of__healthBar_40() { return &____healthBar_40; }
	inline void set__healthBar_40(MMHealthBar_t70CB8C2C73CB0B41B85104B0CC03A99EF0F86917 * value)
	{
		____healthBar_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____healthBar_40), (void*)value);
	}

	inline static int32_t get_offset_of__collider2D_41() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____collider2D_41)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__collider2D_41() const { return ____collider2D_41; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__collider2D_41() { return &____collider2D_41; }
	inline void set__collider2D_41(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____collider2D_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider2D_41), (void*)value);
	}

	inline static int32_t get_offset_of__initialized_42() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____initialized_42)); }
	inline bool get__initialized_42() const { return ____initialized_42; }
	inline bool* get_address_of__initialized_42() { return &____initialized_42; }
	inline void set__initialized_42(bool value)
	{
		____initialized_42 = value;
	}

	inline static int32_t get_offset_of__autoRespawn_43() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____autoRespawn_43)); }
	inline AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 * get__autoRespawn_43() const { return ____autoRespawn_43; }
	inline AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 ** get_address_of__autoRespawn_43() { return &____autoRespawn_43; }
	inline void set__autoRespawn_43(AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 * value)
	{
		____autoRespawn_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____autoRespawn_43), (void*)value);
	}

	inline static int32_t get_offset_of__animator_44() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____animator_44)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_44() const { return ____animator_44; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_44() { return &____animator_44; }
	inline void set__animator_44(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_44), (void*)value);
	}

	inline static int32_t get_offset_of__characterPersistence_45() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____characterPersistence_45)); }
	inline CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * get__characterPersistence_45() const { return ____characterPersistence_45; }
	inline CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC ** get_address_of__characterPersistence_45() { return &____characterPersistence_45; }
	inline void set__characterPersistence_45(CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * value)
	{
		____characterPersistence_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterPersistence_45), (void*)value);
	}

	inline static int32_t get_offset_of__propertyBlock_46() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____propertyBlock_46)); }
	inline MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * get__propertyBlock_46() const { return ____propertyBlock_46; }
	inline MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 ** get_address_of__propertyBlock_46() { return &____propertyBlock_46; }
	inline void set__propertyBlock_46(MaterialPropertyBlock_t6C45FC5DE951DA662BBB7A55EEB3DEF33C5431A0 * value)
	{
		____propertyBlock_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____propertyBlock_46), (void*)value);
	}

	inline static int32_t get_offset_of__hasColorProperty_47() { return static_cast<int32_t>(offsetof(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51, ____hasColorProperty_47)); }
	inline bool get__hasColorProperty_47() const { return ____hasColorProperty_47; }
	inline bool* get_address_of__hasColorProperty_47() { return &____hasColorProperty_47; }
	inline void set__hasColorProperty_47(bool value)
	{
		____hasColorProperty_47 = value;
	}
};


// MoreMountains.CorgiEngine.LevelMapCharacter
struct LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String MoreMountains.CorgiEngine.LevelMapCharacter::PlayerID
	String_t* ___PlayerID_4;
	// System.Single MoreMountains.CorgiEngine.LevelMapCharacter::CharacterSpeed
	float ___CharacterSpeed_5;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::StartingPoint
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___StartingPoint_6;
	// System.Boolean MoreMountains.CorgiEngine.LevelMapCharacter::<CollidingWithAPathElement>k__BackingField
	bool ___U3CCollidingWithAPathElementU3Ek__BackingField_7;
	// System.Boolean MoreMountains.CorgiEngine.LevelMapCharacter::CharacterIsFacingRight
	bool ___CharacterIsFacingRight_8;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::<LastVisitedPathElement>k__BackingField
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___U3CLastVisitedPathElementU3Ek__BackingField_9;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::_destination
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ____destination_10;
	// System.Boolean MoreMountains.CorgiEngine.LevelMapCharacter::_shouldMove
	bool ____shouldMove_11;
	// System.Single MoreMountains.CorgiEngine.LevelMapCharacter::_horizontalMove
	float ____horizontalMove_12;
	// System.Single MoreMountains.CorgiEngine.LevelMapCharacter::_verticalMove
	float ____verticalMove_13;
	// System.Single MoreMountains.CorgiEngine.LevelMapCharacter::_threshold
	float ____threshold_14;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.LevelMapCharacter::_offset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____offset_15;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.LevelMapCharacter::_positionLastFrame
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____positionLastFrame_16;
	// System.Single MoreMountains.CorgiEngine.LevelMapCharacter::_currentSpeed
	float ____currentSpeed_17;
	// System.String MoreMountains.CorgiEngine.LevelMapCharacter::_movement
	String_t* ____movement_18;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::_currentPathElement
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ____currentPathElement_19;
	// UnityEngine.Rigidbody2D MoreMountains.CorgiEngine.LevelMapCharacter::_rigidbody2D
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ____rigidbody2D_20;
	// UnityEngine.BoxCollider2D MoreMountains.CorgiEngine.LevelMapCharacter::_boxCollider2D
	BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * ____boxCollider2D_21;
	// UnityEngine.Animator MoreMountains.CorgiEngine.LevelMapCharacter::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_22;

public:
	inline static int32_t get_offset_of_PlayerID_4() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ___PlayerID_4)); }
	inline String_t* get_PlayerID_4() const { return ___PlayerID_4; }
	inline String_t** get_address_of_PlayerID_4() { return &___PlayerID_4; }
	inline void set_PlayerID_4(String_t* value)
	{
		___PlayerID_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_4), (void*)value);
	}

	inline static int32_t get_offset_of_CharacterSpeed_5() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ___CharacterSpeed_5)); }
	inline float get_CharacterSpeed_5() const { return ___CharacterSpeed_5; }
	inline float* get_address_of_CharacterSpeed_5() { return &___CharacterSpeed_5; }
	inline void set_CharacterSpeed_5(float value)
	{
		___CharacterSpeed_5 = value;
	}

	inline static int32_t get_offset_of_StartingPoint_6() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ___StartingPoint_6)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get_StartingPoint_6() const { return ___StartingPoint_6; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of_StartingPoint_6() { return &___StartingPoint_6; }
	inline void set_StartingPoint_6(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		___StartingPoint_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StartingPoint_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCollidingWithAPathElementU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ___U3CCollidingWithAPathElementU3Ek__BackingField_7)); }
	inline bool get_U3CCollidingWithAPathElementU3Ek__BackingField_7() const { return ___U3CCollidingWithAPathElementU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CCollidingWithAPathElementU3Ek__BackingField_7() { return &___U3CCollidingWithAPathElementU3Ek__BackingField_7; }
	inline void set_U3CCollidingWithAPathElementU3Ek__BackingField_7(bool value)
	{
		___U3CCollidingWithAPathElementU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_CharacterIsFacingRight_8() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ___CharacterIsFacingRight_8)); }
	inline bool get_CharacterIsFacingRight_8() const { return ___CharacterIsFacingRight_8; }
	inline bool* get_address_of_CharacterIsFacingRight_8() { return &___CharacterIsFacingRight_8; }
	inline void set_CharacterIsFacingRight_8(bool value)
	{
		___CharacterIsFacingRight_8 = value;
	}

	inline static int32_t get_offset_of_U3CLastVisitedPathElementU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ___U3CLastVisitedPathElementU3Ek__BackingField_9)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get_U3CLastVisitedPathElementU3Ek__BackingField_9() const { return ___U3CLastVisitedPathElementU3Ek__BackingField_9; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of_U3CLastVisitedPathElementU3Ek__BackingField_9() { return &___U3CLastVisitedPathElementU3Ek__BackingField_9; }
	inline void set_U3CLastVisitedPathElementU3Ek__BackingField_9(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		___U3CLastVisitedPathElementU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLastVisitedPathElementU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of__destination_10() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____destination_10)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get__destination_10() const { return ____destination_10; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of__destination_10() { return &____destination_10; }
	inline void set__destination_10(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		____destination_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____destination_10), (void*)value);
	}

	inline static int32_t get_offset_of__shouldMove_11() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____shouldMove_11)); }
	inline bool get__shouldMove_11() const { return ____shouldMove_11; }
	inline bool* get_address_of__shouldMove_11() { return &____shouldMove_11; }
	inline void set__shouldMove_11(bool value)
	{
		____shouldMove_11 = value;
	}

	inline static int32_t get_offset_of__horizontalMove_12() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____horizontalMove_12)); }
	inline float get__horizontalMove_12() const { return ____horizontalMove_12; }
	inline float* get_address_of__horizontalMove_12() { return &____horizontalMove_12; }
	inline void set__horizontalMove_12(float value)
	{
		____horizontalMove_12 = value;
	}

	inline static int32_t get_offset_of__verticalMove_13() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____verticalMove_13)); }
	inline float get__verticalMove_13() const { return ____verticalMove_13; }
	inline float* get_address_of__verticalMove_13() { return &____verticalMove_13; }
	inline void set__verticalMove_13(float value)
	{
		____verticalMove_13 = value;
	}

	inline static int32_t get_offset_of__threshold_14() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____threshold_14)); }
	inline float get__threshold_14() const { return ____threshold_14; }
	inline float* get_address_of__threshold_14() { return &____threshold_14; }
	inline void set__threshold_14(float value)
	{
		____threshold_14 = value;
	}

	inline static int32_t get_offset_of__offset_15() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____offset_15)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__offset_15() const { return ____offset_15; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__offset_15() { return &____offset_15; }
	inline void set__offset_15(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____offset_15 = value;
	}

	inline static int32_t get_offset_of__positionLastFrame_16() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____positionLastFrame_16)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__positionLastFrame_16() const { return ____positionLastFrame_16; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__positionLastFrame_16() { return &____positionLastFrame_16; }
	inline void set__positionLastFrame_16(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____positionLastFrame_16 = value;
	}

	inline static int32_t get_offset_of__currentSpeed_17() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____currentSpeed_17)); }
	inline float get__currentSpeed_17() const { return ____currentSpeed_17; }
	inline float* get_address_of__currentSpeed_17() { return &____currentSpeed_17; }
	inline void set__currentSpeed_17(float value)
	{
		____currentSpeed_17 = value;
	}

	inline static int32_t get_offset_of__movement_18() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____movement_18)); }
	inline String_t* get__movement_18() const { return ____movement_18; }
	inline String_t** get_address_of__movement_18() { return &____movement_18; }
	inline void set__movement_18(String_t* value)
	{
		____movement_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movement_18), (void*)value);
	}

	inline static int32_t get_offset_of__currentPathElement_19() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____currentPathElement_19)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get__currentPathElement_19() const { return ____currentPathElement_19; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of__currentPathElement_19() { return &____currentPathElement_19; }
	inline void set__currentPathElement_19(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		____currentPathElement_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentPathElement_19), (void*)value);
	}

	inline static int32_t get_offset_of__rigidbody2D_20() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____rigidbody2D_20)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get__rigidbody2D_20() const { return ____rigidbody2D_20; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of__rigidbody2D_20() { return &____rigidbody2D_20; }
	inline void set__rigidbody2D_20(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		____rigidbody2D_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rigidbody2D_20), (void*)value);
	}

	inline static int32_t get_offset_of__boxCollider2D_21() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____boxCollider2D_21)); }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * get__boxCollider2D_21() const { return ____boxCollider2D_21; }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 ** get_address_of__boxCollider2D_21() { return &____boxCollider2D_21; }
	inline void set__boxCollider2D_21(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * value)
	{
		____boxCollider2D_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____boxCollider2D_21), (void*)value);
	}

	inline static int32_t get_offset_of__animator_22() { return static_cast<int32_t>(offsetof(LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD, ____animator_22)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_22() const { return ____animator_22; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_22() { return &____animator_22; }
	inline void set__animator_22(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_22), (void*)value);
	}
};


// MoreMountains.CorgiEngine.LevelMapPathElement
struct LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::AutomaticMovement
	bool ___AutomaticMovement_4;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapPathElement::Up
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___Up_5;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapPathElement::Right
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___Right_6;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapPathElement::Down
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___Down_7;
	// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapPathElement::Left
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___Left_8;

public:
	inline static int32_t get_offset_of_AutomaticMovement_4() { return static_cast<int32_t>(offsetof(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75, ___AutomaticMovement_4)); }
	inline bool get_AutomaticMovement_4() const { return ___AutomaticMovement_4; }
	inline bool* get_address_of_AutomaticMovement_4() { return &___AutomaticMovement_4; }
	inline void set_AutomaticMovement_4(bool value)
	{
		___AutomaticMovement_4 = value;
	}

	inline static int32_t get_offset_of_Up_5() { return static_cast<int32_t>(offsetof(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75, ___Up_5)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get_Up_5() const { return ___Up_5; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of_Up_5() { return &___Up_5; }
	inline void set_Up_5(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		___Up_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Up_5), (void*)value);
	}

	inline static int32_t get_offset_of_Right_6() { return static_cast<int32_t>(offsetof(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75, ___Right_6)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get_Right_6() const { return ___Right_6; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of_Right_6() { return &___Right_6; }
	inline void set_Right_6(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		___Right_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Right_6), (void*)value);
	}

	inline static int32_t get_offset_of_Down_7() { return static_cast<int32_t>(offsetof(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75, ___Down_7)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get_Down_7() const { return ___Down_7; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of_Down_7() { return &___Down_7; }
	inline void set_Down_7(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		___Down_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Down_7), (void*)value);
	}

	inline static int32_t get_offset_of_Left_8() { return static_cast<int32_t>(offsetof(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75, ___Left_8)); }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * get_Left_8() const { return ___Left_8; }
	inline LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 ** get_address_of_Left_8() { return &___Left_8; }
	inline void set_Left_8(LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * value)
	{
		___Left_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Left_8), (void*)value);
	}
};


// MoreMountains.CorgiEngine.LevelSelector
struct LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String MoreMountains.CorgiEngine.LevelSelector::LevelName
	String_t* ___LevelName_4;
	// System.Boolean MoreMountains.CorgiEngine.LevelSelector::Fade
	bool ___Fade_5;
	// System.Boolean MoreMountains.CorgiEngine.LevelSelector::Save
	bool ___Save_6;

public:
	inline static int32_t get_offset_of_LevelName_4() { return static_cast<int32_t>(offsetof(LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316, ___LevelName_4)); }
	inline String_t* get_LevelName_4() const { return ___LevelName_4; }
	inline String_t** get_address_of_LevelName_4() { return &___LevelName_4; }
	inline void set_LevelName_4(String_t* value)
	{
		___LevelName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelName_4), (void*)value);
	}

	inline static int32_t get_offset_of_Fade_5() { return static_cast<int32_t>(offsetof(LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316, ___Fade_5)); }
	inline bool get_Fade_5() const { return ___Fade_5; }
	inline bool* get_address_of_Fade_5() { return &___Fade_5; }
	inline void set_Fade_5(bool value)
	{
		___Fade_5 = value;
	}

	inline static int32_t get_offset_of_Save_6() { return static_cast<int32_t>(offsetof(LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316, ___Save_6)); }
	inline bool get_Save_6() const { return ___Save_6; }
	inline bool* get_address_of_Save_6() { return &___Save_6; }
	inline void set_Save_6(bool value)
	{
		___Save_6 = value;
	}
};


// MoreMountains.CorgiEngine.LevelSelectorGUI
struct LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Image MoreMountains.CorgiEngine.LevelSelectorGUI::LevelNamePanel
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___LevelNamePanel_4;
	// UnityEngine.UI.Text MoreMountains.CorgiEngine.LevelSelectorGUI::LevelNameText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___LevelNameText_5;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.LevelSelectorGUI::LevelNameOffset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___LevelNameOffset_6;

public:
	inline static int32_t get_offset_of_LevelNamePanel_4() { return static_cast<int32_t>(offsetof(LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE, ___LevelNamePanel_4)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_LevelNamePanel_4() const { return ___LevelNamePanel_4; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_LevelNamePanel_4() { return &___LevelNamePanel_4; }
	inline void set_LevelNamePanel_4(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___LevelNamePanel_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelNamePanel_4), (void*)value);
	}

	inline static int32_t get_offset_of_LevelNameText_5() { return static_cast<int32_t>(offsetof(LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE, ___LevelNameText_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_LevelNameText_5() const { return ___LevelNameText_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_LevelNameText_5() { return &___LevelNameText_5; }
	inline void set_LevelNameText_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___LevelNameText_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelNameText_5), (void*)value);
	}

	inline static int32_t get_offset_of_LevelNameOffset_6() { return static_cast<int32_t>(offsetof(LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE, ___LevelNameOffset_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_LevelNameOffset_6() const { return ___LevelNameOffset_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_LevelNameOffset_6() { return &___LevelNameOffset_6; }
	inline void set_LevelNameOffset_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___LevelNameOffset_6 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks
struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<MoreMountains.Feedbacks.MMFeedback> MoreMountains.Feedbacks.MMFeedbacks::Feedbacks
	List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 * ___Feedbacks_4;
	// MoreMountains.Feedbacks.MMFeedbacks/InitializationModes MoreMountains.Feedbacks.MMFeedbacks::InitializationMode
	int32_t ___InitializationMode_5;
	// MoreMountains.Feedbacks.MMFeedbacks/SafeModes MoreMountains.Feedbacks.MMFeedbacks::SafeMode
	int32_t ___SafeMode_6;
	// MoreMountains.Feedbacks.MMFeedbacks/Directions MoreMountains.Feedbacks.MMFeedbacks::Direction
	int32_t ___Direction_7;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::AutoChangeDirectionOnEnd
	bool ___AutoChangeDirectionOnEnd_8;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::AutoPlayOnStart
	bool ___AutoPlayOnStart_9;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::AutoPlayOnEnable
	bool ___AutoPlayOnEnable_10;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::DurationMultiplier
	float ___DurationMultiplier_11;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::DisplayFullDurationDetails
	bool ___DisplayFullDurationDetails_12;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::CooldownDuration
	float ___CooldownDuration_13;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::InitialDelay
	float ___InitialDelay_14;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::CanPlayWhileAlreadyPlaying
	bool ___CanPlayWhileAlreadyPlaying_15;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::FeedbacksIntensity
	float ___FeedbacksIntensity_16;
	// MoreMountains.Feedbacks.MMFeedbacksEvents MoreMountains.Feedbacks.MMFeedbacks::Events
	MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A * ___Events_17;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::DebugActive
	bool ___DebugActive_19;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_20;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<InScriptDrivenPause>k__BackingField
	bool ___U3CInScriptDrivenPauseU3Ek__BackingField_21;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<ContainsLoop>k__BackingField
	bool ___U3CContainsLoopU3Ek__BackingField_22;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<ShouldRevertOnNextPlay>k__BackingField
	bool ___U3CShouldRevertOnNextPlayU3Ek__BackingField_23;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_startTime
	float ____startTime_24;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_holdingMax
	float ____holdingMax_25;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_lastStartAt
	float ____lastStartAt_26;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::_pauseFound
	bool ____pauseFound_27;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_totalDuration
	float ____totalDuration_28;

public:
	inline static int32_t get_offset_of_Feedbacks_4() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___Feedbacks_4)); }
	inline List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 * get_Feedbacks_4() const { return ___Feedbacks_4; }
	inline List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 ** get_address_of_Feedbacks_4() { return &___Feedbacks_4; }
	inline void set_Feedbacks_4(List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 * value)
	{
		___Feedbacks_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Feedbacks_4), (void*)value);
	}

	inline static int32_t get_offset_of_InitializationMode_5() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___InitializationMode_5)); }
	inline int32_t get_InitializationMode_5() const { return ___InitializationMode_5; }
	inline int32_t* get_address_of_InitializationMode_5() { return &___InitializationMode_5; }
	inline void set_InitializationMode_5(int32_t value)
	{
		___InitializationMode_5 = value;
	}

	inline static int32_t get_offset_of_SafeMode_6() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___SafeMode_6)); }
	inline int32_t get_SafeMode_6() const { return ___SafeMode_6; }
	inline int32_t* get_address_of_SafeMode_6() { return &___SafeMode_6; }
	inline void set_SafeMode_6(int32_t value)
	{
		___SafeMode_6 = value;
	}

	inline static int32_t get_offset_of_Direction_7() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___Direction_7)); }
	inline int32_t get_Direction_7() const { return ___Direction_7; }
	inline int32_t* get_address_of_Direction_7() { return &___Direction_7; }
	inline void set_Direction_7(int32_t value)
	{
		___Direction_7 = value;
	}

	inline static int32_t get_offset_of_AutoChangeDirectionOnEnd_8() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___AutoChangeDirectionOnEnd_8)); }
	inline bool get_AutoChangeDirectionOnEnd_8() const { return ___AutoChangeDirectionOnEnd_8; }
	inline bool* get_address_of_AutoChangeDirectionOnEnd_8() { return &___AutoChangeDirectionOnEnd_8; }
	inline void set_AutoChangeDirectionOnEnd_8(bool value)
	{
		___AutoChangeDirectionOnEnd_8 = value;
	}

	inline static int32_t get_offset_of_AutoPlayOnStart_9() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___AutoPlayOnStart_9)); }
	inline bool get_AutoPlayOnStart_9() const { return ___AutoPlayOnStart_9; }
	inline bool* get_address_of_AutoPlayOnStart_9() { return &___AutoPlayOnStart_9; }
	inline void set_AutoPlayOnStart_9(bool value)
	{
		___AutoPlayOnStart_9 = value;
	}

	inline static int32_t get_offset_of_AutoPlayOnEnable_10() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___AutoPlayOnEnable_10)); }
	inline bool get_AutoPlayOnEnable_10() const { return ___AutoPlayOnEnable_10; }
	inline bool* get_address_of_AutoPlayOnEnable_10() { return &___AutoPlayOnEnable_10; }
	inline void set_AutoPlayOnEnable_10(bool value)
	{
		___AutoPlayOnEnable_10 = value;
	}

	inline static int32_t get_offset_of_DurationMultiplier_11() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___DurationMultiplier_11)); }
	inline float get_DurationMultiplier_11() const { return ___DurationMultiplier_11; }
	inline float* get_address_of_DurationMultiplier_11() { return &___DurationMultiplier_11; }
	inline void set_DurationMultiplier_11(float value)
	{
		___DurationMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_DisplayFullDurationDetails_12() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___DisplayFullDurationDetails_12)); }
	inline bool get_DisplayFullDurationDetails_12() const { return ___DisplayFullDurationDetails_12; }
	inline bool* get_address_of_DisplayFullDurationDetails_12() { return &___DisplayFullDurationDetails_12; }
	inline void set_DisplayFullDurationDetails_12(bool value)
	{
		___DisplayFullDurationDetails_12 = value;
	}

	inline static int32_t get_offset_of_CooldownDuration_13() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___CooldownDuration_13)); }
	inline float get_CooldownDuration_13() const { return ___CooldownDuration_13; }
	inline float* get_address_of_CooldownDuration_13() { return &___CooldownDuration_13; }
	inline void set_CooldownDuration_13(float value)
	{
		___CooldownDuration_13 = value;
	}

	inline static int32_t get_offset_of_InitialDelay_14() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___InitialDelay_14)); }
	inline float get_InitialDelay_14() const { return ___InitialDelay_14; }
	inline float* get_address_of_InitialDelay_14() { return &___InitialDelay_14; }
	inline void set_InitialDelay_14(float value)
	{
		___InitialDelay_14 = value;
	}

	inline static int32_t get_offset_of_CanPlayWhileAlreadyPlaying_15() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___CanPlayWhileAlreadyPlaying_15)); }
	inline bool get_CanPlayWhileAlreadyPlaying_15() const { return ___CanPlayWhileAlreadyPlaying_15; }
	inline bool* get_address_of_CanPlayWhileAlreadyPlaying_15() { return &___CanPlayWhileAlreadyPlaying_15; }
	inline void set_CanPlayWhileAlreadyPlaying_15(bool value)
	{
		___CanPlayWhileAlreadyPlaying_15 = value;
	}

	inline static int32_t get_offset_of_FeedbacksIntensity_16() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___FeedbacksIntensity_16)); }
	inline float get_FeedbacksIntensity_16() const { return ___FeedbacksIntensity_16; }
	inline float* get_address_of_FeedbacksIntensity_16() { return &___FeedbacksIntensity_16; }
	inline void set_FeedbacksIntensity_16(float value)
	{
		___FeedbacksIntensity_16 = value;
	}

	inline static int32_t get_offset_of_Events_17() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___Events_17)); }
	inline MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A * get_Events_17() const { return ___Events_17; }
	inline MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A ** get_address_of_Events_17() { return &___Events_17; }
	inline void set_Events_17(MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A * value)
	{
		___Events_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Events_17), (void*)value);
	}

	inline static int32_t get_offset_of_DebugActive_19() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___DebugActive_19)); }
	inline bool get_DebugActive_19() const { return ___DebugActive_19; }
	inline bool* get_address_of_DebugActive_19() { return &___DebugActive_19; }
	inline void set_DebugActive_19(bool value)
	{
		___DebugActive_19 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CIsPlayingU3Ek__BackingField_20)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_20() const { return ___U3CIsPlayingU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_20() { return &___U3CIsPlayingU3Ek__BackingField_20; }
	inline void set_U3CIsPlayingU3Ek__BackingField_20(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CInScriptDrivenPauseU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CInScriptDrivenPauseU3Ek__BackingField_21)); }
	inline bool get_U3CInScriptDrivenPauseU3Ek__BackingField_21() const { return ___U3CInScriptDrivenPauseU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CInScriptDrivenPauseU3Ek__BackingField_21() { return &___U3CInScriptDrivenPauseU3Ek__BackingField_21; }
	inline void set_U3CInScriptDrivenPauseU3Ek__BackingField_21(bool value)
	{
		___U3CInScriptDrivenPauseU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CContainsLoopU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CContainsLoopU3Ek__BackingField_22)); }
	inline bool get_U3CContainsLoopU3Ek__BackingField_22() const { return ___U3CContainsLoopU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CContainsLoopU3Ek__BackingField_22() { return &___U3CContainsLoopU3Ek__BackingField_22; }
	inline void set_U3CContainsLoopU3Ek__BackingField_22(bool value)
	{
		___U3CContainsLoopU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CShouldRevertOnNextPlayU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CShouldRevertOnNextPlayU3Ek__BackingField_23)); }
	inline bool get_U3CShouldRevertOnNextPlayU3Ek__BackingField_23() const { return ___U3CShouldRevertOnNextPlayU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CShouldRevertOnNextPlayU3Ek__BackingField_23() { return &___U3CShouldRevertOnNextPlayU3Ek__BackingField_23; }
	inline void set_U3CShouldRevertOnNextPlayU3Ek__BackingField_23(bool value)
	{
		___U3CShouldRevertOnNextPlayU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of__startTime_24() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____startTime_24)); }
	inline float get__startTime_24() const { return ____startTime_24; }
	inline float* get_address_of__startTime_24() { return &____startTime_24; }
	inline void set__startTime_24(float value)
	{
		____startTime_24 = value;
	}

	inline static int32_t get_offset_of__holdingMax_25() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____holdingMax_25)); }
	inline float get__holdingMax_25() const { return ____holdingMax_25; }
	inline float* get_address_of__holdingMax_25() { return &____holdingMax_25; }
	inline void set__holdingMax_25(float value)
	{
		____holdingMax_25 = value;
	}

	inline static int32_t get_offset_of__lastStartAt_26() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____lastStartAt_26)); }
	inline float get__lastStartAt_26() const { return ____lastStartAt_26; }
	inline float* get_address_of__lastStartAt_26() { return &____lastStartAt_26; }
	inline void set__lastStartAt_26(float value)
	{
		____lastStartAt_26 = value;
	}

	inline static int32_t get_offset_of__pauseFound_27() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____pauseFound_27)); }
	inline bool get__pauseFound_27() const { return ____pauseFound_27; }
	inline bool* get_address_of__pauseFound_27() { return &____pauseFound_27; }
	inline void set__pauseFound_27(bool value)
	{
		____pauseFound_27 = value;
	}

	inline static int32_t get_offset_of__totalDuration_28() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____totalDuration_28)); }
	inline float get__totalDuration_28() const { return ____totalDuration_28; }
	inline float* get_address_of__totalDuration_28() { return &____totalDuration_28; }
	inline void set__totalDuration_28(float value)
	{
		____totalDuration_28 = value;
	}
};

struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914_StaticFields
{
public:
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::GlobalMMFeedbacksActive
	bool ___GlobalMMFeedbacksActive_18;

public:
	inline static int32_t get_offset_of_GlobalMMFeedbacksActive_18() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914_StaticFields, ___GlobalMMFeedbacksActive_18)); }
	inline bool get_GlobalMMFeedbacksActive_18() const { return ___GlobalMMFeedbacksActive_18; }
	inline bool* get_address_of_GlobalMMFeedbacksActive_18() { return &___GlobalMMFeedbacksActive_18; }
	inline void set_GlobalMMFeedbacksActive_18(bool value)
	{
		___GlobalMMFeedbacksActive_18 = value;
	}
};


// MoreMountains.Tools.MMMonoBehaviour
struct MMMonoBehaviour_tCF4A374C5050AB4B331002CA4B9CF6ACA984B03C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MoreMountains.Tools.MMTouchButton
struct MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchButton::ButtonPressedFirstTime
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___ButtonPressedFirstTime_4;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchButton::ButtonReleased
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___ButtonReleased_5;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMTouchButton::ButtonPressed
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___ButtonPressed_6;
	// UnityEngine.Sprite MoreMountains.Tools.MMTouchButton::DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___DisabledSprite_7;
	// System.Boolean MoreMountains.Tools.MMTouchButton::DisabledChangeColor
	bool ___DisabledChangeColor_8;
	// UnityEngine.Color MoreMountains.Tools.MMTouchButton::DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DisabledColor_9;
	// UnityEngine.Sprite MoreMountains.Tools.MMTouchButton::PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___PressedSprite_10;
	// System.Boolean MoreMountains.Tools.MMTouchButton::PressedChangeColor
	bool ___PressedChangeColor_11;
	// UnityEngine.Color MoreMountains.Tools.MMTouchButton::PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PressedColor_12;
	// UnityEngine.Sprite MoreMountains.Tools.MMTouchButton::HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___HighlightedSprite_13;
	// System.Boolean MoreMountains.Tools.MMTouchButton::HighlightedChangeColor
	bool ___HighlightedChangeColor_14;
	// UnityEngine.Color MoreMountains.Tools.MMTouchButton::HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___HighlightedColor_15;
	// System.Single MoreMountains.Tools.MMTouchButton::PressedOpacity
	float ___PressedOpacity_16;
	// System.Single MoreMountains.Tools.MMTouchButton::IdleOpacity
	float ___IdleOpacity_17;
	// System.Single MoreMountains.Tools.MMTouchButton::DisabledOpacity
	float ___DisabledOpacity_18;
	// System.Single MoreMountains.Tools.MMTouchButton::PressedFirstTimeDelay
	float ___PressedFirstTimeDelay_19;
	// System.Single MoreMountains.Tools.MMTouchButton::ReleasedDelay
	float ___ReleasedDelay_20;
	// System.Single MoreMountains.Tools.MMTouchButton::BufferDuration
	float ___BufferDuration_21;
	// UnityEngine.Animator MoreMountains.Tools.MMTouchButton::Animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___Animator_22;
	// System.String MoreMountains.Tools.MMTouchButton::IdleAnimationParameterName
	String_t* ___IdleAnimationParameterName_23;
	// System.String MoreMountains.Tools.MMTouchButton::DisabledAnimationParameterName
	String_t* ___DisabledAnimationParameterName_24;
	// System.String MoreMountains.Tools.MMTouchButton::PressedAnimationParameterName
	String_t* ___PressedAnimationParameterName_25;
	// System.Boolean MoreMountains.Tools.MMTouchButton::MouseMode
	bool ___MouseMode_26;
	// System.Boolean MoreMountains.Tools.MMTouchButton::<ReturnToInitialSpriteAutomatically>k__BackingField
	bool ___U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27;
	// MoreMountains.Tools.MMTouchButton/ButtonStates MoreMountains.Tools.MMTouchButton::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_28;
	// System.Boolean MoreMountains.Tools.MMTouchButton::_zonePressed
	bool ____zonePressed_29;
	// UnityEngine.CanvasGroup MoreMountains.Tools.MMTouchButton::_canvasGroup
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ____canvasGroup_30;
	// System.Single MoreMountains.Tools.MMTouchButton::_initialOpacity
	float ____initialOpacity_31;
	// UnityEngine.Animator MoreMountains.Tools.MMTouchButton::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_32;
	// UnityEngine.UI.Image MoreMountains.Tools.MMTouchButton::_image
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ____image_33;
	// UnityEngine.Sprite MoreMountains.Tools.MMTouchButton::_initialSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ____initialSprite_34;
	// UnityEngine.Color MoreMountains.Tools.MMTouchButton::_initialColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____initialColor_35;
	// System.Single MoreMountains.Tools.MMTouchButton::_lastClickTimestamp
	float ____lastClickTimestamp_36;
	// UnityEngine.UI.Selectable MoreMountains.Tools.MMTouchButton::_selectable
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ____selectable_37;
	// System.Action`2<UnityEngine.EventSystems.PointerEventData/FramePressState,UnityEngine.EventSystems.PointerEventData> MoreMountains.Tools.MMTouchButton::ButtonStateChange
	Action_2_t8B45679D16F91472B6F77929A5866C062D119BD7 * ___ButtonStateChange_38;

public:
	inline static int32_t get_offset_of_ButtonPressedFirstTime_4() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___ButtonPressedFirstTime_4)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_ButtonPressedFirstTime_4() const { return ___ButtonPressedFirstTime_4; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_ButtonPressedFirstTime_4() { return &___ButtonPressedFirstTime_4; }
	inline void set_ButtonPressedFirstTime_4(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___ButtonPressedFirstTime_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPressedFirstTime_4), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonReleased_5() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___ButtonReleased_5)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_ButtonReleased_5() const { return ___ButtonReleased_5; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_ButtonReleased_5() { return &___ButtonReleased_5; }
	inline void set_ButtonReleased_5(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___ButtonReleased_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonReleased_5), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPressed_6() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___ButtonPressed_6)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_ButtonPressed_6() const { return ___ButtonPressed_6; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_ButtonPressed_6() { return &___ButtonPressed_6; }
	inline void set_ButtonPressed_6(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___ButtonPressed_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPressed_6), (void*)value);
	}

	inline static int32_t get_offset_of_DisabledSprite_7() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___DisabledSprite_7)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_DisabledSprite_7() const { return ___DisabledSprite_7; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_DisabledSprite_7() { return &___DisabledSprite_7; }
	inline void set_DisabledSprite_7(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___DisabledSprite_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisabledSprite_7), (void*)value);
	}

	inline static int32_t get_offset_of_DisabledChangeColor_8() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___DisabledChangeColor_8)); }
	inline bool get_DisabledChangeColor_8() const { return ___DisabledChangeColor_8; }
	inline bool* get_address_of_DisabledChangeColor_8() { return &___DisabledChangeColor_8; }
	inline void set_DisabledChangeColor_8(bool value)
	{
		___DisabledChangeColor_8 = value;
	}

	inline static int32_t get_offset_of_DisabledColor_9() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___DisabledColor_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DisabledColor_9() const { return ___DisabledColor_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DisabledColor_9() { return &___DisabledColor_9; }
	inline void set_DisabledColor_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DisabledColor_9 = value;
	}

	inline static int32_t get_offset_of_PressedSprite_10() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___PressedSprite_10)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_PressedSprite_10() const { return ___PressedSprite_10; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_PressedSprite_10() { return &___PressedSprite_10; }
	inline void set_PressedSprite_10(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___PressedSprite_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PressedSprite_10), (void*)value);
	}

	inline static int32_t get_offset_of_PressedChangeColor_11() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___PressedChangeColor_11)); }
	inline bool get_PressedChangeColor_11() const { return ___PressedChangeColor_11; }
	inline bool* get_address_of_PressedChangeColor_11() { return &___PressedChangeColor_11; }
	inline void set_PressedChangeColor_11(bool value)
	{
		___PressedChangeColor_11 = value;
	}

	inline static int32_t get_offset_of_PressedColor_12() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___PressedColor_12)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PressedColor_12() const { return ___PressedColor_12; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PressedColor_12() { return &___PressedColor_12; }
	inline void set_PressedColor_12(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PressedColor_12 = value;
	}

	inline static int32_t get_offset_of_HighlightedSprite_13() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___HighlightedSprite_13)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_HighlightedSprite_13() const { return ___HighlightedSprite_13; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_HighlightedSprite_13() { return &___HighlightedSprite_13; }
	inline void set_HighlightedSprite_13(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___HighlightedSprite_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HighlightedSprite_13), (void*)value);
	}

	inline static int32_t get_offset_of_HighlightedChangeColor_14() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___HighlightedChangeColor_14)); }
	inline bool get_HighlightedChangeColor_14() const { return ___HighlightedChangeColor_14; }
	inline bool* get_address_of_HighlightedChangeColor_14() { return &___HighlightedChangeColor_14; }
	inline void set_HighlightedChangeColor_14(bool value)
	{
		___HighlightedChangeColor_14 = value;
	}

	inline static int32_t get_offset_of_HighlightedColor_15() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___HighlightedColor_15)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_HighlightedColor_15() const { return ___HighlightedColor_15; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_HighlightedColor_15() { return &___HighlightedColor_15; }
	inline void set_HighlightedColor_15(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___HighlightedColor_15 = value;
	}

	inline static int32_t get_offset_of_PressedOpacity_16() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___PressedOpacity_16)); }
	inline float get_PressedOpacity_16() const { return ___PressedOpacity_16; }
	inline float* get_address_of_PressedOpacity_16() { return &___PressedOpacity_16; }
	inline void set_PressedOpacity_16(float value)
	{
		___PressedOpacity_16 = value;
	}

	inline static int32_t get_offset_of_IdleOpacity_17() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___IdleOpacity_17)); }
	inline float get_IdleOpacity_17() const { return ___IdleOpacity_17; }
	inline float* get_address_of_IdleOpacity_17() { return &___IdleOpacity_17; }
	inline void set_IdleOpacity_17(float value)
	{
		___IdleOpacity_17 = value;
	}

	inline static int32_t get_offset_of_DisabledOpacity_18() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___DisabledOpacity_18)); }
	inline float get_DisabledOpacity_18() const { return ___DisabledOpacity_18; }
	inline float* get_address_of_DisabledOpacity_18() { return &___DisabledOpacity_18; }
	inline void set_DisabledOpacity_18(float value)
	{
		___DisabledOpacity_18 = value;
	}

	inline static int32_t get_offset_of_PressedFirstTimeDelay_19() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___PressedFirstTimeDelay_19)); }
	inline float get_PressedFirstTimeDelay_19() const { return ___PressedFirstTimeDelay_19; }
	inline float* get_address_of_PressedFirstTimeDelay_19() { return &___PressedFirstTimeDelay_19; }
	inline void set_PressedFirstTimeDelay_19(float value)
	{
		___PressedFirstTimeDelay_19 = value;
	}

	inline static int32_t get_offset_of_ReleasedDelay_20() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___ReleasedDelay_20)); }
	inline float get_ReleasedDelay_20() const { return ___ReleasedDelay_20; }
	inline float* get_address_of_ReleasedDelay_20() { return &___ReleasedDelay_20; }
	inline void set_ReleasedDelay_20(float value)
	{
		___ReleasedDelay_20 = value;
	}

	inline static int32_t get_offset_of_BufferDuration_21() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___BufferDuration_21)); }
	inline float get_BufferDuration_21() const { return ___BufferDuration_21; }
	inline float* get_address_of_BufferDuration_21() { return &___BufferDuration_21; }
	inline void set_BufferDuration_21(float value)
	{
		___BufferDuration_21 = value;
	}

	inline static int32_t get_offset_of_Animator_22() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___Animator_22)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_Animator_22() const { return ___Animator_22; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_Animator_22() { return &___Animator_22; }
	inline void set_Animator_22(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___Animator_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Animator_22), (void*)value);
	}

	inline static int32_t get_offset_of_IdleAnimationParameterName_23() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___IdleAnimationParameterName_23)); }
	inline String_t* get_IdleAnimationParameterName_23() const { return ___IdleAnimationParameterName_23; }
	inline String_t** get_address_of_IdleAnimationParameterName_23() { return &___IdleAnimationParameterName_23; }
	inline void set_IdleAnimationParameterName_23(String_t* value)
	{
		___IdleAnimationParameterName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___IdleAnimationParameterName_23), (void*)value);
	}

	inline static int32_t get_offset_of_DisabledAnimationParameterName_24() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___DisabledAnimationParameterName_24)); }
	inline String_t* get_DisabledAnimationParameterName_24() const { return ___DisabledAnimationParameterName_24; }
	inline String_t** get_address_of_DisabledAnimationParameterName_24() { return &___DisabledAnimationParameterName_24; }
	inline void set_DisabledAnimationParameterName_24(String_t* value)
	{
		___DisabledAnimationParameterName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DisabledAnimationParameterName_24), (void*)value);
	}

	inline static int32_t get_offset_of_PressedAnimationParameterName_25() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___PressedAnimationParameterName_25)); }
	inline String_t* get_PressedAnimationParameterName_25() const { return ___PressedAnimationParameterName_25; }
	inline String_t** get_address_of_PressedAnimationParameterName_25() { return &___PressedAnimationParameterName_25; }
	inline void set_PressedAnimationParameterName_25(String_t* value)
	{
		___PressedAnimationParameterName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PressedAnimationParameterName_25), (void*)value);
	}

	inline static int32_t get_offset_of_MouseMode_26() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___MouseMode_26)); }
	inline bool get_MouseMode_26() const { return ___MouseMode_26; }
	inline bool* get_address_of_MouseMode_26() { return &___MouseMode_26; }
	inline void set_MouseMode_26(bool value)
	{
		___MouseMode_26 = value;
	}

	inline static int32_t get_offset_of_U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27)); }
	inline bool get_U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27() const { return ___U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27() { return &___U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27; }
	inline void set_U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27(bool value)
	{
		___U3CReturnToInitialSpriteAutomaticallyU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___U3CCurrentStateU3Ek__BackingField_28)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_28() const { return ___U3CCurrentStateU3Ek__BackingField_28; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_28() { return &___U3CCurrentStateU3Ek__BackingField_28; }
	inline void set_U3CCurrentStateU3Ek__BackingField_28(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of__zonePressed_29() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____zonePressed_29)); }
	inline bool get__zonePressed_29() const { return ____zonePressed_29; }
	inline bool* get_address_of__zonePressed_29() { return &____zonePressed_29; }
	inline void set__zonePressed_29(bool value)
	{
		____zonePressed_29 = value;
	}

	inline static int32_t get_offset_of__canvasGroup_30() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____canvasGroup_30)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get__canvasGroup_30() const { return ____canvasGroup_30; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of__canvasGroup_30() { return &____canvasGroup_30; }
	inline void set__canvasGroup_30(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		____canvasGroup_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____canvasGroup_30), (void*)value);
	}

	inline static int32_t get_offset_of__initialOpacity_31() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____initialOpacity_31)); }
	inline float get__initialOpacity_31() const { return ____initialOpacity_31; }
	inline float* get_address_of__initialOpacity_31() { return &____initialOpacity_31; }
	inline void set__initialOpacity_31(float value)
	{
		____initialOpacity_31 = value;
	}

	inline static int32_t get_offset_of__animator_32() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____animator_32)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_32() const { return ____animator_32; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_32() { return &____animator_32; }
	inline void set__animator_32(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_32), (void*)value);
	}

	inline static int32_t get_offset_of__image_33() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____image_33)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get__image_33() const { return ____image_33; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of__image_33() { return &____image_33; }
	inline void set__image_33(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		____image_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____image_33), (void*)value);
	}

	inline static int32_t get_offset_of__initialSprite_34() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____initialSprite_34)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get__initialSprite_34() const { return ____initialSprite_34; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of__initialSprite_34() { return &____initialSprite_34; }
	inline void set__initialSprite_34(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		____initialSprite_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____initialSprite_34), (void*)value);
	}

	inline static int32_t get_offset_of__initialColor_35() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____initialColor_35)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__initialColor_35() const { return ____initialColor_35; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__initialColor_35() { return &____initialColor_35; }
	inline void set__initialColor_35(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____initialColor_35 = value;
	}

	inline static int32_t get_offset_of__lastClickTimestamp_36() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____lastClickTimestamp_36)); }
	inline float get__lastClickTimestamp_36() const { return ____lastClickTimestamp_36; }
	inline float* get_address_of__lastClickTimestamp_36() { return &____lastClickTimestamp_36; }
	inline void set__lastClickTimestamp_36(float value)
	{
		____lastClickTimestamp_36 = value;
	}

	inline static int32_t get_offset_of__selectable_37() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ____selectable_37)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get__selectable_37() const { return ____selectable_37; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of__selectable_37() { return &____selectable_37; }
	inline void set__selectable_37(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		____selectable_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____selectable_37), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonStateChange_38() { return static_cast<int32_t>(offsetof(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469, ___ButtonStateChange_38)); }
	inline Action_2_t8B45679D16F91472B6F77929A5866C062D119BD7 * get_ButtonStateChange_38() const { return ___ButtonStateChange_38; }
	inline Action_2_t8B45679D16F91472B6F77929A5866C062D119BD7 ** get_address_of_ButtonStateChange_38() { return &___ButtonStateChange_38; }
	inline void set_ButtonStateChange_38(Action_2_t8B45679D16F91472B6F77929A5866C062D119BD7 * value)
	{
		___ButtonStateChange_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonStateChange_38), (void*)value);
	}
};


// MoreMountains.CorgiEngine.PickableItem
struct PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.PickableItem::PickFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___PickFeedbacks_4;
	// System.Boolean MoreMountains.CorgiEngine.PickableItem::DisableObjectOnPick
	bool ___DisableObjectOnPick_5;
	// System.Boolean MoreMountains.CorgiEngine.PickableItem::DisableRendererOnPick
	bool ___DisableRendererOnPick_6;
	// System.Boolean MoreMountains.CorgiEngine.PickableItem::DisableColliderOnPick
	bool ___DisableColliderOnPick_7;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.PickableItem::_collider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____collider_8;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.PickableItem::_pickingCollider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____pickingCollider_9;
	// UnityEngine.Renderer MoreMountains.CorgiEngine.PickableItem::_renderer
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ____renderer_10;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.PickableItem::_character
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ____character_11;
	// System.Boolean MoreMountains.CorgiEngine.PickableItem::_pickable
	bool ____pickable_12;
	// MoreMountains.InventoryEngine.ItemPicker MoreMountains.CorgiEngine.PickableItem::_itemPicker
	ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7 * ____itemPicker_13;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.PickableItem::_collidingObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____collidingObject_14;
	// MoreMountains.CorgiEngine.AutoRespawn MoreMountains.CorgiEngine.PickableItem::_autoRespawn
	AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 * ____autoRespawn_15;

public:
	inline static int32_t get_offset_of_PickFeedbacks_4() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ___PickFeedbacks_4)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_PickFeedbacks_4() const { return ___PickFeedbacks_4; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_PickFeedbacks_4() { return &___PickFeedbacks_4; }
	inline void set_PickFeedbacks_4(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___PickFeedbacks_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PickFeedbacks_4), (void*)value);
	}

	inline static int32_t get_offset_of_DisableObjectOnPick_5() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ___DisableObjectOnPick_5)); }
	inline bool get_DisableObjectOnPick_5() const { return ___DisableObjectOnPick_5; }
	inline bool* get_address_of_DisableObjectOnPick_5() { return &___DisableObjectOnPick_5; }
	inline void set_DisableObjectOnPick_5(bool value)
	{
		___DisableObjectOnPick_5 = value;
	}

	inline static int32_t get_offset_of_DisableRendererOnPick_6() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ___DisableRendererOnPick_6)); }
	inline bool get_DisableRendererOnPick_6() const { return ___DisableRendererOnPick_6; }
	inline bool* get_address_of_DisableRendererOnPick_6() { return &___DisableRendererOnPick_6; }
	inline void set_DisableRendererOnPick_6(bool value)
	{
		___DisableRendererOnPick_6 = value;
	}

	inline static int32_t get_offset_of_DisableColliderOnPick_7() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ___DisableColliderOnPick_7)); }
	inline bool get_DisableColliderOnPick_7() const { return ___DisableColliderOnPick_7; }
	inline bool* get_address_of_DisableColliderOnPick_7() { return &___DisableColliderOnPick_7; }
	inline void set_DisableColliderOnPick_7(bool value)
	{
		___DisableColliderOnPick_7 = value;
	}

	inline static int32_t get_offset_of__collider_8() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____collider_8)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__collider_8() const { return ____collider_8; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__collider_8() { return &____collider_8; }
	inline void set__collider_8(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____collider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider_8), (void*)value);
	}

	inline static int32_t get_offset_of__pickingCollider_9() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____pickingCollider_9)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__pickingCollider_9() const { return ____pickingCollider_9; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__pickingCollider_9() { return &____pickingCollider_9; }
	inline void set__pickingCollider_9(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____pickingCollider_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____pickingCollider_9), (void*)value);
	}

	inline static int32_t get_offset_of__renderer_10() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____renderer_10)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get__renderer_10() const { return ____renderer_10; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of__renderer_10() { return &____renderer_10; }
	inline void set__renderer_10(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		____renderer_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____renderer_10), (void*)value);
	}

	inline static int32_t get_offset_of__character_11() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____character_11)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get__character_11() const { return ____character_11; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of__character_11() { return &____character_11; }
	inline void set__character_11(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		____character_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____character_11), (void*)value);
	}

	inline static int32_t get_offset_of__pickable_12() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____pickable_12)); }
	inline bool get__pickable_12() const { return ____pickable_12; }
	inline bool* get_address_of__pickable_12() { return &____pickable_12; }
	inline void set__pickable_12(bool value)
	{
		____pickable_12 = value;
	}

	inline static int32_t get_offset_of__itemPicker_13() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____itemPicker_13)); }
	inline ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7 * get__itemPicker_13() const { return ____itemPicker_13; }
	inline ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7 ** get_address_of__itemPicker_13() { return &____itemPicker_13; }
	inline void set__itemPicker_13(ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7 * value)
	{
		____itemPicker_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____itemPicker_13), (void*)value);
	}

	inline static int32_t get_offset_of__collidingObject_14() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____collidingObject_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__collidingObject_14() const { return ____collidingObject_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__collidingObject_14() { return &____collidingObject_14; }
	inline void set__collidingObject_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____collidingObject_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collidingObject_14), (void*)value);
	}

	inline static int32_t get_offset_of__autoRespawn_15() { return static_cast<int32_t>(offsetof(PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2, ____autoRespawn_15)); }
	inline AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 * get__autoRespawn_15() const { return ____autoRespawn_15; }
	inline AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 ** get_address_of__autoRespawn_15() { return &____autoRespawn_15; }
	inline void set__autoRespawn_15(AutoRespawn_tF3A277CA99CB5FEADDA10A233807E3D13754D6A0 * value)
	{
		____autoRespawn_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____autoRespawn_15), (void*)value);
	}
};


// MoreMountains.CorgiEngine.RetroAdventureLevel
struct RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String MoreMountains.CorgiEngine.RetroAdventureLevel::SceneName
	String_t* ___SceneName_4;
	// UnityEngine.UI.Image MoreMountains.CorgiEngine.RetroAdventureLevel::LockedIcon
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___LockedIcon_5;
	// MoreMountains.Tools.MMTouchButton MoreMountains.CorgiEngine.RetroAdventureLevel::PlayButton
	MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469 * ___PlayButton_6;
	// UnityEngine.UI.Image MoreMountains.CorgiEngine.RetroAdventureLevel::ScenePreview
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___ScenePreview_7;
	// UnityEngine.Material MoreMountains.CorgiEngine.RetroAdventureLevel::OffMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___OffMaterial_8;
	// UnityEngine.UI.Image[] MoreMountains.CorgiEngine.RetroAdventureLevel::Stars
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___Stars_9;
	// UnityEngine.Color MoreMountains.CorgiEngine.RetroAdventureLevel::StarOffColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___StarOffColor_10;
	// UnityEngine.Color MoreMountains.CorgiEngine.RetroAdventureLevel::StarOnColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___StarOnColor_11;

public:
	inline static int32_t get_offset_of_SceneName_4() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___SceneName_4)); }
	inline String_t* get_SceneName_4() const { return ___SceneName_4; }
	inline String_t** get_address_of_SceneName_4() { return &___SceneName_4; }
	inline void set_SceneName_4(String_t* value)
	{
		___SceneName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneName_4), (void*)value);
	}

	inline static int32_t get_offset_of_LockedIcon_5() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___LockedIcon_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_LockedIcon_5() const { return ___LockedIcon_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_LockedIcon_5() { return &___LockedIcon_5; }
	inline void set_LockedIcon_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___LockedIcon_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LockedIcon_5), (void*)value);
	}

	inline static int32_t get_offset_of_PlayButton_6() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___PlayButton_6)); }
	inline MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469 * get_PlayButton_6() const { return ___PlayButton_6; }
	inline MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469 ** get_address_of_PlayButton_6() { return &___PlayButton_6; }
	inline void set_PlayButton_6(MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469 * value)
	{
		___PlayButton_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayButton_6), (void*)value);
	}

	inline static int32_t get_offset_of_ScenePreview_7() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___ScenePreview_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_ScenePreview_7() const { return ___ScenePreview_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_ScenePreview_7() { return &___ScenePreview_7; }
	inline void set_ScenePreview_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___ScenePreview_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ScenePreview_7), (void*)value);
	}

	inline static int32_t get_offset_of_OffMaterial_8() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___OffMaterial_8)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_OffMaterial_8() const { return ___OffMaterial_8; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_OffMaterial_8() { return &___OffMaterial_8; }
	inline void set_OffMaterial_8(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___OffMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OffMaterial_8), (void*)value);
	}

	inline static int32_t get_offset_of_Stars_9() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___Stars_9)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_Stars_9() const { return ___Stars_9; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_Stars_9() { return &___Stars_9; }
	inline void set_Stars_9(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___Stars_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Stars_9), (void*)value);
	}

	inline static int32_t get_offset_of_StarOffColor_10() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___StarOffColor_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_StarOffColor_10() const { return ___StarOffColor_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_StarOffColor_10() { return &___StarOffColor_10; }
	inline void set_StarOffColor_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___StarOffColor_10 = value;
	}

	inline static int32_t get_offset_of_StarOnColor_11() { return static_cast<int32_t>(offsetof(RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28, ___StarOnColor_11)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_StarOnColor_11() const { return ___StarOnColor_11; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_StarOnColor_11() { return &___StarOnColor_11; }
	inline void set_StarOnColor_11(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___StarOnColor_11 = value;
	}
};


// MoreMountains.CorgiEngine.RetroDoor
struct RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Color MoreMountains.CorgiEngine.RetroDoor::DoorColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DoorColor_4;
	// UnityEngine.SpriteRenderer MoreMountains.CorgiEngine.RetroDoor::DoorLightModel
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___DoorLightModel_5;
	// UnityEngine.ParticleSystem MoreMountains.CorgiEngine.RetroDoor::DoorParticles
	ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * ___DoorParticles_6;

public:
	inline static int32_t get_offset_of_DoorColor_4() { return static_cast<int32_t>(offsetof(RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985, ___DoorColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DoorColor_4() const { return ___DoorColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DoorColor_4() { return &___DoorColor_4; }
	inline void set_DoorColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DoorColor_4 = value;
	}

	inline static int32_t get_offset_of_DoorLightModel_5() { return static_cast<int32_t>(offsetof(RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985, ___DoorLightModel_5)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_DoorLightModel_5() const { return ___DoorLightModel_5; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_DoorLightModel_5() { return &___DoorLightModel_5; }
	inline void set_DoorLightModel_5(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___DoorLightModel_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DoorLightModel_5), (void*)value);
	}

	inline static int32_t get_offset_of_DoorParticles_6() { return static_cast<int32_t>(offsetof(RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985, ___DoorParticles_6)); }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * get_DoorParticles_6() const { return ___DoorParticles_6; }
	inline ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E ** get_address_of_DoorParticles_6() { return &___DoorParticles_6; }
	inline void set_DoorParticles_6(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * value)
	{
		___DoorParticles_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DoorParticles_6), (void*)value);
	}
};


// MoreMountains.CorgiEngine.RetroNextLevel
struct RetroNextLevel_t49F31286D6C8F872FD3ADAA1F38E01079D88F4F6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MoreMountains.CorgiEngine.UncheckReverseGravityInput
struct UncheckReverseGravityInput_tB2E610A441AEFFB7B59E20DD94DA6E5E12FA7D90  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MoreMountains.CorgiEngine.ButtonActivated
struct ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5  : public MMMonoBehaviour_tCF4A374C5050AB4B331002CA4B9CF6ACA984B03C
{
public:
	// MoreMountains.CorgiEngine.ButtonActivated/ButtonActivatedRequirements MoreMountains.CorgiEngine.ButtonActivated::ButtonActivatedRequirement
	int32_t ___ButtonActivatedRequirement_4;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::RequiresPlayerType
	bool ___RequiresPlayerType_5;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::RequiresButtonActivationAbility
	bool ___RequiresButtonActivationAbility_6;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::PreventJumpsWhileInThisZone
	bool ___PreventJumpsWhileInThisZone_7;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::Activable
	bool ___Activable_8;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AutoActivation
	bool ___AutoActivation_9;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AutoActivationAndButtonInteraction
	bool ___AutoActivationAndButtonInteraction_10;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::CanOnlyActivateIfGrounded
	bool ___CanOnlyActivateIfGrounded_11;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::ShouldUpdateState
	bool ___ShouldUpdateState_12;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::OnlyOneActivationAtOnce
	bool ___OnlyOneActivationAtOnce_13;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AlsoPerformChecksOnStay
	bool ___AlsoPerformChecksOnStay_14;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::UnlimitedActivations
	bool ___UnlimitedActivations_15;
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated::MaxNumberOfActivations
	int32_t ___MaxNumberOfActivations_16;
	// System.Single MoreMountains.CorgiEngine.ButtonActivated::DelayBetweenUses
	float ___DelayBetweenUses_17;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::DisableAfterUse
	bool ___DisableAfterUse_18;
	// MoreMountains.CorgiEngine.ButtonActivated/InputTypes MoreMountains.CorgiEngine.ButtonActivated::InputType
	int32_t ___InputType_19;
	// System.String MoreMountains.CorgiEngine.ButtonActivated::InputButton
	String_t* ___InputButton_20;
	// UnityEngine.KeyCode MoreMountains.CorgiEngine.ButtonActivated::InputKey
	int32_t ___InputKey_21;
	// System.String MoreMountains.CorgiEngine.ButtonActivated::AnimationTriggerParameterName
	String_t* ___AnimationTriggerParameterName_22;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::UseVisualPrompt
	bool ___UseVisualPrompt_23;
	// MoreMountains.CorgiEngine.ButtonPrompt MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptPrefab
	ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * ___ButtonPromptPrefab_24;
	// System.String MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptText
	String_t* ___ButtonPromptText_25;
	// UnityEngine.Color MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ButtonPromptColor_26;
	// UnityEngine.Color MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ButtonPromptTextColor_27;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AlwaysShowPrompt
	bool ___AlwaysShowPrompt_28;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::ShowPromptWhenColliding
	bool ___ShowPromptWhenColliding_29;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::HidePromptAfterUse
	bool ___HidePromptAfterUse_30;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.ButtonActivated::PromptRelativePosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___PromptRelativePosition_31;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::ActivationFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___ActivationFeedback_32;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::DeniedFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___DeniedFeedback_33;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::EnterFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___EnterFeedback_34;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::ExitFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___ExitFeedback_35;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.ButtonActivated::OnActivation
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnActivation_36;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.ButtonActivated::OnExit
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnExit_37;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.ButtonActivated::OnStay
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStay_38;
	// UnityEngine.Animator MoreMountains.CorgiEngine.ButtonActivated::_buttonPromptAnimator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____buttonPromptAnimator_39;
	// MoreMountains.CorgiEngine.ButtonPrompt MoreMountains.CorgiEngine.ButtonActivated::_buttonPrompt
	ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * ____buttonPrompt_40;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::_promptHiddenForever
	bool ____promptHiddenForever_41;
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated::_numberOfActivationsLeft
	int32_t ____numberOfActivationsLeft_42;
	// System.Single MoreMountains.CorgiEngine.ButtonActivated::_lastActivationTimestamp
	float ____lastActivationTimestamp_43;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.ButtonActivated::_buttonActivatedZoneCollider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____buttonActivatedZoneCollider_44;
	// MoreMountains.CorgiEngine.CharacterButtonActivation MoreMountains.CorgiEngine.ButtonActivated::_characterButtonActivation
	CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B * ____characterButtonActivation_45;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.ButtonActivated::_currentCharacter
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ____currentCharacter_46;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.ButtonActivated::_collidingObjects
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ____collidingObjects_47;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.ButtonActivated::_stayingGameObjects
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ____stayingGameObjects_48;
	// System.Collections.Generic.List`1<UnityEngine.Collider2D> MoreMountains.CorgiEngine.ButtonActivated::_enteredColliders
	List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 * ____enteredColliders_49;

public:
	inline static int32_t get_offset_of_ButtonActivatedRequirement_4() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonActivatedRequirement_4)); }
	inline int32_t get_ButtonActivatedRequirement_4() const { return ___ButtonActivatedRequirement_4; }
	inline int32_t* get_address_of_ButtonActivatedRequirement_4() { return &___ButtonActivatedRequirement_4; }
	inline void set_ButtonActivatedRequirement_4(int32_t value)
	{
		___ButtonActivatedRequirement_4 = value;
	}

	inline static int32_t get_offset_of_RequiresPlayerType_5() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___RequiresPlayerType_5)); }
	inline bool get_RequiresPlayerType_5() const { return ___RequiresPlayerType_5; }
	inline bool* get_address_of_RequiresPlayerType_5() { return &___RequiresPlayerType_5; }
	inline void set_RequiresPlayerType_5(bool value)
	{
		___RequiresPlayerType_5 = value;
	}

	inline static int32_t get_offset_of_RequiresButtonActivationAbility_6() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___RequiresButtonActivationAbility_6)); }
	inline bool get_RequiresButtonActivationAbility_6() const { return ___RequiresButtonActivationAbility_6; }
	inline bool* get_address_of_RequiresButtonActivationAbility_6() { return &___RequiresButtonActivationAbility_6; }
	inline void set_RequiresButtonActivationAbility_6(bool value)
	{
		___RequiresButtonActivationAbility_6 = value;
	}

	inline static int32_t get_offset_of_PreventJumpsWhileInThisZone_7() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___PreventJumpsWhileInThisZone_7)); }
	inline bool get_PreventJumpsWhileInThisZone_7() const { return ___PreventJumpsWhileInThisZone_7; }
	inline bool* get_address_of_PreventJumpsWhileInThisZone_7() { return &___PreventJumpsWhileInThisZone_7; }
	inline void set_PreventJumpsWhileInThisZone_7(bool value)
	{
		___PreventJumpsWhileInThisZone_7 = value;
	}

	inline static int32_t get_offset_of_Activable_8() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___Activable_8)); }
	inline bool get_Activable_8() const { return ___Activable_8; }
	inline bool* get_address_of_Activable_8() { return &___Activable_8; }
	inline void set_Activable_8(bool value)
	{
		___Activable_8 = value;
	}

	inline static int32_t get_offset_of_AutoActivation_9() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AutoActivation_9)); }
	inline bool get_AutoActivation_9() const { return ___AutoActivation_9; }
	inline bool* get_address_of_AutoActivation_9() { return &___AutoActivation_9; }
	inline void set_AutoActivation_9(bool value)
	{
		___AutoActivation_9 = value;
	}

	inline static int32_t get_offset_of_AutoActivationAndButtonInteraction_10() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AutoActivationAndButtonInteraction_10)); }
	inline bool get_AutoActivationAndButtonInteraction_10() const { return ___AutoActivationAndButtonInteraction_10; }
	inline bool* get_address_of_AutoActivationAndButtonInteraction_10() { return &___AutoActivationAndButtonInteraction_10; }
	inline void set_AutoActivationAndButtonInteraction_10(bool value)
	{
		___AutoActivationAndButtonInteraction_10 = value;
	}

	inline static int32_t get_offset_of_CanOnlyActivateIfGrounded_11() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___CanOnlyActivateIfGrounded_11)); }
	inline bool get_CanOnlyActivateIfGrounded_11() const { return ___CanOnlyActivateIfGrounded_11; }
	inline bool* get_address_of_CanOnlyActivateIfGrounded_11() { return &___CanOnlyActivateIfGrounded_11; }
	inline void set_CanOnlyActivateIfGrounded_11(bool value)
	{
		___CanOnlyActivateIfGrounded_11 = value;
	}

	inline static int32_t get_offset_of_ShouldUpdateState_12() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ShouldUpdateState_12)); }
	inline bool get_ShouldUpdateState_12() const { return ___ShouldUpdateState_12; }
	inline bool* get_address_of_ShouldUpdateState_12() { return &___ShouldUpdateState_12; }
	inline void set_ShouldUpdateState_12(bool value)
	{
		___ShouldUpdateState_12 = value;
	}

	inline static int32_t get_offset_of_OnlyOneActivationAtOnce_13() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnlyOneActivationAtOnce_13)); }
	inline bool get_OnlyOneActivationAtOnce_13() const { return ___OnlyOneActivationAtOnce_13; }
	inline bool* get_address_of_OnlyOneActivationAtOnce_13() { return &___OnlyOneActivationAtOnce_13; }
	inline void set_OnlyOneActivationAtOnce_13(bool value)
	{
		___OnlyOneActivationAtOnce_13 = value;
	}

	inline static int32_t get_offset_of_AlsoPerformChecksOnStay_14() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AlsoPerformChecksOnStay_14)); }
	inline bool get_AlsoPerformChecksOnStay_14() const { return ___AlsoPerformChecksOnStay_14; }
	inline bool* get_address_of_AlsoPerformChecksOnStay_14() { return &___AlsoPerformChecksOnStay_14; }
	inline void set_AlsoPerformChecksOnStay_14(bool value)
	{
		___AlsoPerformChecksOnStay_14 = value;
	}

	inline static int32_t get_offset_of_UnlimitedActivations_15() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___UnlimitedActivations_15)); }
	inline bool get_UnlimitedActivations_15() const { return ___UnlimitedActivations_15; }
	inline bool* get_address_of_UnlimitedActivations_15() { return &___UnlimitedActivations_15; }
	inline void set_UnlimitedActivations_15(bool value)
	{
		___UnlimitedActivations_15 = value;
	}

	inline static int32_t get_offset_of_MaxNumberOfActivations_16() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___MaxNumberOfActivations_16)); }
	inline int32_t get_MaxNumberOfActivations_16() const { return ___MaxNumberOfActivations_16; }
	inline int32_t* get_address_of_MaxNumberOfActivations_16() { return &___MaxNumberOfActivations_16; }
	inline void set_MaxNumberOfActivations_16(int32_t value)
	{
		___MaxNumberOfActivations_16 = value;
	}

	inline static int32_t get_offset_of_DelayBetweenUses_17() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___DelayBetweenUses_17)); }
	inline float get_DelayBetweenUses_17() const { return ___DelayBetweenUses_17; }
	inline float* get_address_of_DelayBetweenUses_17() { return &___DelayBetweenUses_17; }
	inline void set_DelayBetweenUses_17(float value)
	{
		___DelayBetweenUses_17 = value;
	}

	inline static int32_t get_offset_of_DisableAfterUse_18() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___DisableAfterUse_18)); }
	inline bool get_DisableAfterUse_18() const { return ___DisableAfterUse_18; }
	inline bool* get_address_of_DisableAfterUse_18() { return &___DisableAfterUse_18; }
	inline void set_DisableAfterUse_18(bool value)
	{
		___DisableAfterUse_18 = value;
	}

	inline static int32_t get_offset_of_InputType_19() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___InputType_19)); }
	inline int32_t get_InputType_19() const { return ___InputType_19; }
	inline int32_t* get_address_of_InputType_19() { return &___InputType_19; }
	inline void set_InputType_19(int32_t value)
	{
		___InputType_19 = value;
	}

	inline static int32_t get_offset_of_InputButton_20() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___InputButton_20)); }
	inline String_t* get_InputButton_20() const { return ___InputButton_20; }
	inline String_t** get_address_of_InputButton_20() { return &___InputButton_20; }
	inline void set_InputButton_20(String_t* value)
	{
		___InputButton_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputButton_20), (void*)value);
	}

	inline static int32_t get_offset_of_InputKey_21() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___InputKey_21)); }
	inline int32_t get_InputKey_21() const { return ___InputKey_21; }
	inline int32_t* get_address_of_InputKey_21() { return &___InputKey_21; }
	inline void set_InputKey_21(int32_t value)
	{
		___InputKey_21 = value;
	}

	inline static int32_t get_offset_of_AnimationTriggerParameterName_22() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AnimationTriggerParameterName_22)); }
	inline String_t* get_AnimationTriggerParameterName_22() const { return ___AnimationTriggerParameterName_22; }
	inline String_t** get_address_of_AnimationTriggerParameterName_22() { return &___AnimationTriggerParameterName_22; }
	inline void set_AnimationTriggerParameterName_22(String_t* value)
	{
		___AnimationTriggerParameterName_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AnimationTriggerParameterName_22), (void*)value);
	}

	inline static int32_t get_offset_of_UseVisualPrompt_23() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___UseVisualPrompt_23)); }
	inline bool get_UseVisualPrompt_23() const { return ___UseVisualPrompt_23; }
	inline bool* get_address_of_UseVisualPrompt_23() { return &___UseVisualPrompt_23; }
	inline void set_UseVisualPrompt_23(bool value)
	{
		___UseVisualPrompt_23 = value;
	}

	inline static int32_t get_offset_of_ButtonPromptPrefab_24() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptPrefab_24)); }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * get_ButtonPromptPrefab_24() const { return ___ButtonPromptPrefab_24; }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 ** get_address_of_ButtonPromptPrefab_24() { return &___ButtonPromptPrefab_24; }
	inline void set_ButtonPromptPrefab_24(ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * value)
	{
		___ButtonPromptPrefab_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPromptPrefab_24), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPromptText_25() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptText_25)); }
	inline String_t* get_ButtonPromptText_25() const { return ___ButtonPromptText_25; }
	inline String_t** get_address_of_ButtonPromptText_25() { return &___ButtonPromptText_25; }
	inline void set_ButtonPromptText_25(String_t* value)
	{
		___ButtonPromptText_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPromptText_25), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPromptColor_26() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptColor_26)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ButtonPromptColor_26() const { return ___ButtonPromptColor_26; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ButtonPromptColor_26() { return &___ButtonPromptColor_26; }
	inline void set_ButtonPromptColor_26(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ButtonPromptColor_26 = value;
	}

	inline static int32_t get_offset_of_ButtonPromptTextColor_27() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptTextColor_27)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ButtonPromptTextColor_27() const { return ___ButtonPromptTextColor_27; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ButtonPromptTextColor_27() { return &___ButtonPromptTextColor_27; }
	inline void set_ButtonPromptTextColor_27(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ButtonPromptTextColor_27 = value;
	}

	inline static int32_t get_offset_of_AlwaysShowPrompt_28() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AlwaysShowPrompt_28)); }
	inline bool get_AlwaysShowPrompt_28() const { return ___AlwaysShowPrompt_28; }
	inline bool* get_address_of_AlwaysShowPrompt_28() { return &___AlwaysShowPrompt_28; }
	inline void set_AlwaysShowPrompt_28(bool value)
	{
		___AlwaysShowPrompt_28 = value;
	}

	inline static int32_t get_offset_of_ShowPromptWhenColliding_29() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ShowPromptWhenColliding_29)); }
	inline bool get_ShowPromptWhenColliding_29() const { return ___ShowPromptWhenColliding_29; }
	inline bool* get_address_of_ShowPromptWhenColliding_29() { return &___ShowPromptWhenColliding_29; }
	inline void set_ShowPromptWhenColliding_29(bool value)
	{
		___ShowPromptWhenColliding_29 = value;
	}

	inline static int32_t get_offset_of_HidePromptAfterUse_30() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___HidePromptAfterUse_30)); }
	inline bool get_HidePromptAfterUse_30() const { return ___HidePromptAfterUse_30; }
	inline bool* get_address_of_HidePromptAfterUse_30() { return &___HidePromptAfterUse_30; }
	inline void set_HidePromptAfterUse_30(bool value)
	{
		___HidePromptAfterUse_30 = value;
	}

	inline static int32_t get_offset_of_PromptRelativePosition_31() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___PromptRelativePosition_31)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_PromptRelativePosition_31() const { return ___PromptRelativePosition_31; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_PromptRelativePosition_31() { return &___PromptRelativePosition_31; }
	inline void set_PromptRelativePosition_31(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___PromptRelativePosition_31 = value;
	}

	inline static int32_t get_offset_of_ActivationFeedback_32() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ActivationFeedback_32)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_ActivationFeedback_32() const { return ___ActivationFeedback_32; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_ActivationFeedback_32() { return &___ActivationFeedback_32; }
	inline void set_ActivationFeedback_32(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___ActivationFeedback_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ActivationFeedback_32), (void*)value);
	}

	inline static int32_t get_offset_of_DeniedFeedback_33() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___DeniedFeedback_33)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_DeniedFeedback_33() const { return ___DeniedFeedback_33; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_DeniedFeedback_33() { return &___DeniedFeedback_33; }
	inline void set_DeniedFeedback_33(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___DeniedFeedback_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeniedFeedback_33), (void*)value);
	}

	inline static int32_t get_offset_of_EnterFeedback_34() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___EnterFeedback_34)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_EnterFeedback_34() const { return ___EnterFeedback_34; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_EnterFeedback_34() { return &___EnterFeedback_34; }
	inline void set_EnterFeedback_34(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___EnterFeedback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterFeedback_34), (void*)value);
	}

	inline static int32_t get_offset_of_ExitFeedback_35() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ExitFeedback_35)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_ExitFeedback_35() const { return ___ExitFeedback_35; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_ExitFeedback_35() { return &___ExitFeedback_35; }
	inline void set_ExitFeedback_35(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___ExitFeedback_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ExitFeedback_35), (void*)value);
	}

	inline static int32_t get_offset_of_OnActivation_36() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnActivation_36)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnActivation_36() const { return ___OnActivation_36; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnActivation_36() { return &___OnActivation_36; }
	inline void set_OnActivation_36(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnActivation_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnActivation_36), (void*)value);
	}

	inline static int32_t get_offset_of_OnExit_37() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnExit_37)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnExit_37() const { return ___OnExit_37; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnExit_37() { return &___OnExit_37; }
	inline void set_OnExit_37(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnExit_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnExit_37), (void*)value);
	}

	inline static int32_t get_offset_of_OnStay_38() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnStay_38)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStay_38() const { return ___OnStay_38; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStay_38() { return &___OnStay_38; }
	inline void set_OnStay_38(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStay_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStay_38), (void*)value);
	}

	inline static int32_t get_offset_of__buttonPromptAnimator_39() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____buttonPromptAnimator_39)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__buttonPromptAnimator_39() const { return ____buttonPromptAnimator_39; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__buttonPromptAnimator_39() { return &____buttonPromptAnimator_39; }
	inline void set__buttonPromptAnimator_39(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____buttonPromptAnimator_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonPromptAnimator_39), (void*)value);
	}

	inline static int32_t get_offset_of__buttonPrompt_40() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____buttonPrompt_40)); }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * get__buttonPrompt_40() const { return ____buttonPrompt_40; }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 ** get_address_of__buttonPrompt_40() { return &____buttonPrompt_40; }
	inline void set__buttonPrompt_40(ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * value)
	{
		____buttonPrompt_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonPrompt_40), (void*)value);
	}

	inline static int32_t get_offset_of__promptHiddenForever_41() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____promptHiddenForever_41)); }
	inline bool get__promptHiddenForever_41() const { return ____promptHiddenForever_41; }
	inline bool* get_address_of__promptHiddenForever_41() { return &____promptHiddenForever_41; }
	inline void set__promptHiddenForever_41(bool value)
	{
		____promptHiddenForever_41 = value;
	}

	inline static int32_t get_offset_of__numberOfActivationsLeft_42() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____numberOfActivationsLeft_42)); }
	inline int32_t get__numberOfActivationsLeft_42() const { return ____numberOfActivationsLeft_42; }
	inline int32_t* get_address_of__numberOfActivationsLeft_42() { return &____numberOfActivationsLeft_42; }
	inline void set__numberOfActivationsLeft_42(int32_t value)
	{
		____numberOfActivationsLeft_42 = value;
	}

	inline static int32_t get_offset_of__lastActivationTimestamp_43() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____lastActivationTimestamp_43)); }
	inline float get__lastActivationTimestamp_43() const { return ____lastActivationTimestamp_43; }
	inline float* get_address_of__lastActivationTimestamp_43() { return &____lastActivationTimestamp_43; }
	inline void set__lastActivationTimestamp_43(float value)
	{
		____lastActivationTimestamp_43 = value;
	}

	inline static int32_t get_offset_of__buttonActivatedZoneCollider_44() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____buttonActivatedZoneCollider_44)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__buttonActivatedZoneCollider_44() const { return ____buttonActivatedZoneCollider_44; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__buttonActivatedZoneCollider_44() { return &____buttonActivatedZoneCollider_44; }
	inline void set__buttonActivatedZoneCollider_44(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____buttonActivatedZoneCollider_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonActivatedZoneCollider_44), (void*)value);
	}

	inline static int32_t get_offset_of__characterButtonActivation_45() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____characterButtonActivation_45)); }
	inline CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B * get__characterButtonActivation_45() const { return ____characterButtonActivation_45; }
	inline CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B ** get_address_of__characterButtonActivation_45() { return &____characterButtonActivation_45; }
	inline void set__characterButtonActivation_45(CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B * value)
	{
		____characterButtonActivation_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterButtonActivation_45), (void*)value);
	}

	inline static int32_t get_offset_of__currentCharacter_46() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____currentCharacter_46)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get__currentCharacter_46() const { return ____currentCharacter_46; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of__currentCharacter_46() { return &____currentCharacter_46; }
	inline void set__currentCharacter_46(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		____currentCharacter_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentCharacter_46), (void*)value);
	}

	inline static int32_t get_offset_of__collidingObjects_47() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____collidingObjects_47)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get__collidingObjects_47() const { return ____collidingObjects_47; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of__collidingObjects_47() { return &____collidingObjects_47; }
	inline void set__collidingObjects_47(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		____collidingObjects_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collidingObjects_47), (void*)value);
	}

	inline static int32_t get_offset_of__stayingGameObjects_48() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____stayingGameObjects_48)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get__stayingGameObjects_48() const { return ____stayingGameObjects_48; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of__stayingGameObjects_48() { return &____stayingGameObjects_48; }
	inline void set__stayingGameObjects_48(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		____stayingGameObjects_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stayingGameObjects_48), (void*)value);
	}

	inline static int32_t get_offset_of__enteredColliders_49() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____enteredColliders_49)); }
	inline List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 * get__enteredColliders_49() const { return ____enteredColliders_49; }
	inline List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 ** get_address_of__enteredColliders_49() { return &____enteredColliders_49; }
	inline void set__enteredColliders_49(List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 * value)
	{
		____enteredColliders_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____enteredColliders_49), (void*)value);
	}
};


// MoreMountains.CorgiEngine.CharacterFly
struct CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4  : public CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7
{
public:
	// System.Single MoreMountains.CorgiEngine.CharacterFly::FlySpeed
	float ___FlySpeed_25;
	// System.Single MoreMountains.CorgiEngine.CharacterFly::<MovementSpeedMultiplier>k__BackingField
	float ___U3CMovementSpeedMultiplierU3Ek__BackingField_26;
	// System.Boolean MoreMountains.CorgiEngine.CharacterFly::AlwaysFlying
	bool ___AlwaysFlying_27;
	// System.Boolean MoreMountains.CorgiEngine.CharacterFly::StopFlyingOnDeath
	bool ___StopFlyingOnDeath_28;
	// System.Single MoreMountains.CorgiEngine.CharacterFly::_horizontalMovement
	float ____horizontalMovement_29;
	// System.Single MoreMountains.CorgiEngine.CharacterFly::_verticalMovement
	float ____verticalMovement_30;
	// System.Boolean MoreMountains.CorgiEngine.CharacterFly::_flying
	bool ____flying_31;
	// System.Int32 MoreMountains.CorgiEngine.CharacterFly::_flyingAnimationParameter
	int32_t ____flyingAnimationParameter_34;
	// System.Int32 MoreMountains.CorgiEngine.CharacterFly::_flySpeedAnimationParameter
	int32_t ____flySpeedAnimationParameter_35;

public:
	inline static int32_t get_offset_of_FlySpeed_25() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ___FlySpeed_25)); }
	inline float get_FlySpeed_25() const { return ___FlySpeed_25; }
	inline float* get_address_of_FlySpeed_25() { return &___FlySpeed_25; }
	inline void set_FlySpeed_25(float value)
	{
		___FlySpeed_25 = value;
	}

	inline static int32_t get_offset_of_U3CMovementSpeedMultiplierU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ___U3CMovementSpeedMultiplierU3Ek__BackingField_26)); }
	inline float get_U3CMovementSpeedMultiplierU3Ek__BackingField_26() const { return ___U3CMovementSpeedMultiplierU3Ek__BackingField_26; }
	inline float* get_address_of_U3CMovementSpeedMultiplierU3Ek__BackingField_26() { return &___U3CMovementSpeedMultiplierU3Ek__BackingField_26; }
	inline void set_U3CMovementSpeedMultiplierU3Ek__BackingField_26(float value)
	{
		___U3CMovementSpeedMultiplierU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_AlwaysFlying_27() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ___AlwaysFlying_27)); }
	inline bool get_AlwaysFlying_27() const { return ___AlwaysFlying_27; }
	inline bool* get_address_of_AlwaysFlying_27() { return &___AlwaysFlying_27; }
	inline void set_AlwaysFlying_27(bool value)
	{
		___AlwaysFlying_27 = value;
	}

	inline static int32_t get_offset_of_StopFlyingOnDeath_28() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ___StopFlyingOnDeath_28)); }
	inline bool get_StopFlyingOnDeath_28() const { return ___StopFlyingOnDeath_28; }
	inline bool* get_address_of_StopFlyingOnDeath_28() { return &___StopFlyingOnDeath_28; }
	inline void set_StopFlyingOnDeath_28(bool value)
	{
		___StopFlyingOnDeath_28 = value;
	}

	inline static int32_t get_offset_of__horizontalMovement_29() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ____horizontalMovement_29)); }
	inline float get__horizontalMovement_29() const { return ____horizontalMovement_29; }
	inline float* get_address_of__horizontalMovement_29() { return &____horizontalMovement_29; }
	inline void set__horizontalMovement_29(float value)
	{
		____horizontalMovement_29 = value;
	}

	inline static int32_t get_offset_of__verticalMovement_30() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ____verticalMovement_30)); }
	inline float get__verticalMovement_30() const { return ____verticalMovement_30; }
	inline float* get_address_of__verticalMovement_30() { return &____verticalMovement_30; }
	inline void set__verticalMovement_30(float value)
	{
		____verticalMovement_30 = value;
	}

	inline static int32_t get_offset_of__flying_31() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ____flying_31)); }
	inline bool get__flying_31() const { return ____flying_31; }
	inline bool* get_address_of__flying_31() { return &____flying_31; }
	inline void set__flying_31(bool value)
	{
		____flying_31 = value;
	}

	inline static int32_t get_offset_of__flyingAnimationParameter_34() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ____flyingAnimationParameter_34)); }
	inline int32_t get__flyingAnimationParameter_34() const { return ____flyingAnimationParameter_34; }
	inline int32_t* get_address_of__flyingAnimationParameter_34() { return &____flyingAnimationParameter_34; }
	inline void set__flyingAnimationParameter_34(int32_t value)
	{
		____flyingAnimationParameter_34 = value;
	}

	inline static int32_t get_offset_of__flySpeedAnimationParameter_35() { return static_cast<int32_t>(offsetof(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4, ____flySpeedAnimationParameter_35)); }
	inline int32_t get__flySpeedAnimationParameter_35() const { return ____flySpeedAnimationParameter_35; }
	inline int32_t* get_address_of__flySpeedAnimationParameter_35() { return &____flySpeedAnimationParameter_35; }
	inline void set__flySpeedAnimationParameter_35(int32_t value)
	{
		____flySpeedAnimationParameter_35 = value;
	}
};


// MoreMountains.CorgiEngine.CharacterGravity
struct CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4  : public CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7
{
public:
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::SubjectToGravityPoints
	bool ___SubjectToGravityPoints_25;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::SubjectToGravityZones
	bool ___SubjectToGravityZones_26;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::ReverseHorizontalInputWhenUpsideDown
	bool ___ReverseHorizontalInputWhenUpsideDown_27;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::ReverseVerticalInputWhenUpsideDown
	bool ___ReverseVerticalInputWhenUpsideDown_28;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::ReverseInputOnGravityPoints
	bool ___ReverseInputOnGravityPoints_29;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::InitialGravityAngle
	float ___InitialGravityAngle_30;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::RotationSpeed
	float ___RotationSpeed_31;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::InactiveBufferDuration
	float ___InactiveBufferDuration_32;
	// MoreMountains.CorgiEngine.CharacterGravity/TransitionForcesModes MoreMountains.CorgiEngine.CharacterGravity::TransitionForcesMode
	int32_t ___TransitionForcesMode_33;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::ResetCharacterStateOnGravityChange
	bool ___ResetCharacterStateOnGravityChange_34;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::DrawDebugGravityArrow
	bool ___DrawDebugGravityArrow_35;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::<InGravityPointRange>k__BackingField
	bool ___U3CInGravityPointRangeU3Ek__BackingField_36;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.GravityPoint> MoreMountains.CorgiEngine.CharacterGravity::_gravityPoints
	List_1_t9665259F2DD3EB069F13678BBA381B9041367C51 * ____gravityPoints_37;
	// MoreMountains.CorgiEngine.GravityPoint MoreMountains.CorgiEngine.CharacterGravity::_closestGravityPoint
	GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * ____closestGravityPoint_38;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CharacterGravity::_gravityPointDirection
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____gravityPointDirection_39;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::_inAGravityZone
	bool ____inAGravityZone_40;
	// MoreMountains.CorgiEngine.GravityZone MoreMountains.CorgiEngine.CharacterGravity::_currentGravityZone
	GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 * ____currentGravityZone_41;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_defaultGravityAngle
	float ____defaultGravityAngle_42;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_currentGravityAngle
	float ____currentGravityAngle_43;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_overrideGravityAngle
	float ____overrideGravityAngle_44;
	// System.Boolean MoreMountains.CorgiEngine.CharacterGravity::_gravityOverridden
	bool ____gravityOverridden_45;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_rotationDirection
	float ____rotationDirection_46;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CharacterGravity::_newRotationAngle
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____newRotationAngle_48;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_entryTimeStampZones
	float ____entryTimeStampZones_49;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_entryTimeStampPoints
	float ____entryTimeStampPoints_50;
	// MoreMountains.CorgiEngine.GravityPoint MoreMountains.CorgiEngine.CharacterGravity::_lastGravityPoint
	GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * ____lastGravityPoint_51;
	// MoreMountains.CorgiEngine.GravityPoint MoreMountains.CorgiEngine.CharacterGravity::_newGravityPoint
	GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * ____newGravityPoint_52;
	// System.Single MoreMountains.CorgiEngine.CharacterGravity::_previousGravityAngle
	float ____previousGravityAngle_53;
	// MoreMountains.CorgiEngine.GravityZone MoreMountains.CorgiEngine.CharacterGravity::_cachedGravityZone
	GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 * ____cachedGravityZone_54;
	// MoreMountains.CorgiEngine.CharacterDash MoreMountains.CorgiEngine.CharacterGravity::_characterDash
	CharacterDash_t78AF1BB1E2C303712001AD86D2041FC0B7A011AD * ____characterDash_55;

public:
	inline static int32_t get_offset_of_SubjectToGravityPoints_25() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___SubjectToGravityPoints_25)); }
	inline bool get_SubjectToGravityPoints_25() const { return ___SubjectToGravityPoints_25; }
	inline bool* get_address_of_SubjectToGravityPoints_25() { return &___SubjectToGravityPoints_25; }
	inline void set_SubjectToGravityPoints_25(bool value)
	{
		___SubjectToGravityPoints_25 = value;
	}

	inline static int32_t get_offset_of_SubjectToGravityZones_26() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___SubjectToGravityZones_26)); }
	inline bool get_SubjectToGravityZones_26() const { return ___SubjectToGravityZones_26; }
	inline bool* get_address_of_SubjectToGravityZones_26() { return &___SubjectToGravityZones_26; }
	inline void set_SubjectToGravityZones_26(bool value)
	{
		___SubjectToGravityZones_26 = value;
	}

	inline static int32_t get_offset_of_ReverseHorizontalInputWhenUpsideDown_27() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___ReverseHorizontalInputWhenUpsideDown_27)); }
	inline bool get_ReverseHorizontalInputWhenUpsideDown_27() const { return ___ReverseHorizontalInputWhenUpsideDown_27; }
	inline bool* get_address_of_ReverseHorizontalInputWhenUpsideDown_27() { return &___ReverseHorizontalInputWhenUpsideDown_27; }
	inline void set_ReverseHorizontalInputWhenUpsideDown_27(bool value)
	{
		___ReverseHorizontalInputWhenUpsideDown_27 = value;
	}

	inline static int32_t get_offset_of_ReverseVerticalInputWhenUpsideDown_28() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___ReverseVerticalInputWhenUpsideDown_28)); }
	inline bool get_ReverseVerticalInputWhenUpsideDown_28() const { return ___ReverseVerticalInputWhenUpsideDown_28; }
	inline bool* get_address_of_ReverseVerticalInputWhenUpsideDown_28() { return &___ReverseVerticalInputWhenUpsideDown_28; }
	inline void set_ReverseVerticalInputWhenUpsideDown_28(bool value)
	{
		___ReverseVerticalInputWhenUpsideDown_28 = value;
	}

	inline static int32_t get_offset_of_ReverseInputOnGravityPoints_29() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___ReverseInputOnGravityPoints_29)); }
	inline bool get_ReverseInputOnGravityPoints_29() const { return ___ReverseInputOnGravityPoints_29; }
	inline bool* get_address_of_ReverseInputOnGravityPoints_29() { return &___ReverseInputOnGravityPoints_29; }
	inline void set_ReverseInputOnGravityPoints_29(bool value)
	{
		___ReverseInputOnGravityPoints_29 = value;
	}

	inline static int32_t get_offset_of_InitialGravityAngle_30() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___InitialGravityAngle_30)); }
	inline float get_InitialGravityAngle_30() const { return ___InitialGravityAngle_30; }
	inline float* get_address_of_InitialGravityAngle_30() { return &___InitialGravityAngle_30; }
	inline void set_InitialGravityAngle_30(float value)
	{
		___InitialGravityAngle_30 = value;
	}

	inline static int32_t get_offset_of_RotationSpeed_31() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___RotationSpeed_31)); }
	inline float get_RotationSpeed_31() const { return ___RotationSpeed_31; }
	inline float* get_address_of_RotationSpeed_31() { return &___RotationSpeed_31; }
	inline void set_RotationSpeed_31(float value)
	{
		___RotationSpeed_31 = value;
	}

	inline static int32_t get_offset_of_InactiveBufferDuration_32() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___InactiveBufferDuration_32)); }
	inline float get_InactiveBufferDuration_32() const { return ___InactiveBufferDuration_32; }
	inline float* get_address_of_InactiveBufferDuration_32() { return &___InactiveBufferDuration_32; }
	inline void set_InactiveBufferDuration_32(float value)
	{
		___InactiveBufferDuration_32 = value;
	}

	inline static int32_t get_offset_of_TransitionForcesMode_33() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___TransitionForcesMode_33)); }
	inline int32_t get_TransitionForcesMode_33() const { return ___TransitionForcesMode_33; }
	inline int32_t* get_address_of_TransitionForcesMode_33() { return &___TransitionForcesMode_33; }
	inline void set_TransitionForcesMode_33(int32_t value)
	{
		___TransitionForcesMode_33 = value;
	}

	inline static int32_t get_offset_of_ResetCharacterStateOnGravityChange_34() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___ResetCharacterStateOnGravityChange_34)); }
	inline bool get_ResetCharacterStateOnGravityChange_34() const { return ___ResetCharacterStateOnGravityChange_34; }
	inline bool* get_address_of_ResetCharacterStateOnGravityChange_34() { return &___ResetCharacterStateOnGravityChange_34; }
	inline void set_ResetCharacterStateOnGravityChange_34(bool value)
	{
		___ResetCharacterStateOnGravityChange_34 = value;
	}

	inline static int32_t get_offset_of_DrawDebugGravityArrow_35() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___DrawDebugGravityArrow_35)); }
	inline bool get_DrawDebugGravityArrow_35() const { return ___DrawDebugGravityArrow_35; }
	inline bool* get_address_of_DrawDebugGravityArrow_35() { return &___DrawDebugGravityArrow_35; }
	inline void set_DrawDebugGravityArrow_35(bool value)
	{
		___DrawDebugGravityArrow_35 = value;
	}

	inline static int32_t get_offset_of_U3CInGravityPointRangeU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ___U3CInGravityPointRangeU3Ek__BackingField_36)); }
	inline bool get_U3CInGravityPointRangeU3Ek__BackingField_36() const { return ___U3CInGravityPointRangeU3Ek__BackingField_36; }
	inline bool* get_address_of_U3CInGravityPointRangeU3Ek__BackingField_36() { return &___U3CInGravityPointRangeU3Ek__BackingField_36; }
	inline void set_U3CInGravityPointRangeU3Ek__BackingField_36(bool value)
	{
		___U3CInGravityPointRangeU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of__gravityPoints_37() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____gravityPoints_37)); }
	inline List_1_t9665259F2DD3EB069F13678BBA381B9041367C51 * get__gravityPoints_37() const { return ____gravityPoints_37; }
	inline List_1_t9665259F2DD3EB069F13678BBA381B9041367C51 ** get_address_of__gravityPoints_37() { return &____gravityPoints_37; }
	inline void set__gravityPoints_37(List_1_t9665259F2DD3EB069F13678BBA381B9041367C51 * value)
	{
		____gravityPoints_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____gravityPoints_37), (void*)value);
	}

	inline static int32_t get_offset_of__closestGravityPoint_38() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____closestGravityPoint_38)); }
	inline GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * get__closestGravityPoint_38() const { return ____closestGravityPoint_38; }
	inline GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC ** get_address_of__closestGravityPoint_38() { return &____closestGravityPoint_38; }
	inline void set__closestGravityPoint_38(GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * value)
	{
		____closestGravityPoint_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____closestGravityPoint_38), (void*)value);
	}

	inline static int32_t get_offset_of__gravityPointDirection_39() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____gravityPointDirection_39)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__gravityPointDirection_39() const { return ____gravityPointDirection_39; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__gravityPointDirection_39() { return &____gravityPointDirection_39; }
	inline void set__gravityPointDirection_39(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____gravityPointDirection_39 = value;
	}

	inline static int32_t get_offset_of__inAGravityZone_40() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____inAGravityZone_40)); }
	inline bool get__inAGravityZone_40() const { return ____inAGravityZone_40; }
	inline bool* get_address_of__inAGravityZone_40() { return &____inAGravityZone_40; }
	inline void set__inAGravityZone_40(bool value)
	{
		____inAGravityZone_40 = value;
	}

	inline static int32_t get_offset_of__currentGravityZone_41() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____currentGravityZone_41)); }
	inline GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 * get__currentGravityZone_41() const { return ____currentGravityZone_41; }
	inline GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 ** get_address_of__currentGravityZone_41() { return &____currentGravityZone_41; }
	inline void set__currentGravityZone_41(GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 * value)
	{
		____currentGravityZone_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentGravityZone_41), (void*)value);
	}

	inline static int32_t get_offset_of__defaultGravityAngle_42() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____defaultGravityAngle_42)); }
	inline float get__defaultGravityAngle_42() const { return ____defaultGravityAngle_42; }
	inline float* get_address_of__defaultGravityAngle_42() { return &____defaultGravityAngle_42; }
	inline void set__defaultGravityAngle_42(float value)
	{
		____defaultGravityAngle_42 = value;
	}

	inline static int32_t get_offset_of__currentGravityAngle_43() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____currentGravityAngle_43)); }
	inline float get__currentGravityAngle_43() const { return ____currentGravityAngle_43; }
	inline float* get_address_of__currentGravityAngle_43() { return &____currentGravityAngle_43; }
	inline void set__currentGravityAngle_43(float value)
	{
		____currentGravityAngle_43 = value;
	}

	inline static int32_t get_offset_of__overrideGravityAngle_44() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____overrideGravityAngle_44)); }
	inline float get__overrideGravityAngle_44() const { return ____overrideGravityAngle_44; }
	inline float* get_address_of__overrideGravityAngle_44() { return &____overrideGravityAngle_44; }
	inline void set__overrideGravityAngle_44(float value)
	{
		____overrideGravityAngle_44 = value;
	}

	inline static int32_t get_offset_of__gravityOverridden_45() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____gravityOverridden_45)); }
	inline bool get__gravityOverridden_45() const { return ____gravityOverridden_45; }
	inline bool* get_address_of__gravityOverridden_45() { return &____gravityOverridden_45; }
	inline void set__gravityOverridden_45(bool value)
	{
		____gravityOverridden_45 = value;
	}

	inline static int32_t get_offset_of__rotationDirection_46() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____rotationDirection_46)); }
	inline float get__rotationDirection_46() const { return ____rotationDirection_46; }
	inline float* get_address_of__rotationDirection_46() { return &____rotationDirection_46; }
	inline void set__rotationDirection_46(float value)
	{
		____rotationDirection_46 = value;
	}

	inline static int32_t get_offset_of__newRotationAngle_48() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____newRotationAngle_48)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__newRotationAngle_48() const { return ____newRotationAngle_48; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__newRotationAngle_48() { return &____newRotationAngle_48; }
	inline void set__newRotationAngle_48(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____newRotationAngle_48 = value;
	}

	inline static int32_t get_offset_of__entryTimeStampZones_49() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____entryTimeStampZones_49)); }
	inline float get__entryTimeStampZones_49() const { return ____entryTimeStampZones_49; }
	inline float* get_address_of__entryTimeStampZones_49() { return &____entryTimeStampZones_49; }
	inline void set__entryTimeStampZones_49(float value)
	{
		____entryTimeStampZones_49 = value;
	}

	inline static int32_t get_offset_of__entryTimeStampPoints_50() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____entryTimeStampPoints_50)); }
	inline float get__entryTimeStampPoints_50() const { return ____entryTimeStampPoints_50; }
	inline float* get_address_of__entryTimeStampPoints_50() { return &____entryTimeStampPoints_50; }
	inline void set__entryTimeStampPoints_50(float value)
	{
		____entryTimeStampPoints_50 = value;
	}

	inline static int32_t get_offset_of__lastGravityPoint_51() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____lastGravityPoint_51)); }
	inline GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * get__lastGravityPoint_51() const { return ____lastGravityPoint_51; }
	inline GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC ** get_address_of__lastGravityPoint_51() { return &____lastGravityPoint_51; }
	inline void set__lastGravityPoint_51(GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * value)
	{
		____lastGravityPoint_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastGravityPoint_51), (void*)value);
	}

	inline static int32_t get_offset_of__newGravityPoint_52() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____newGravityPoint_52)); }
	inline GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * get__newGravityPoint_52() const { return ____newGravityPoint_52; }
	inline GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC ** get_address_of__newGravityPoint_52() { return &____newGravityPoint_52; }
	inline void set__newGravityPoint_52(GravityPoint_t9F77A204ED4AA6DCBA25CBBCEF785125B6EB6DFC * value)
	{
		____newGravityPoint_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____newGravityPoint_52), (void*)value);
	}

	inline static int32_t get_offset_of__previousGravityAngle_53() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____previousGravityAngle_53)); }
	inline float get__previousGravityAngle_53() const { return ____previousGravityAngle_53; }
	inline float* get_address_of__previousGravityAngle_53() { return &____previousGravityAngle_53; }
	inline void set__previousGravityAngle_53(float value)
	{
		____previousGravityAngle_53 = value;
	}

	inline static int32_t get_offset_of__cachedGravityZone_54() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____cachedGravityZone_54)); }
	inline GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 * get__cachedGravityZone_54() const { return ____cachedGravityZone_54; }
	inline GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 ** get_address_of__cachedGravityZone_54() { return &____cachedGravityZone_54; }
	inline void set__cachedGravityZone_54(GravityZone_t699EC25E4DA6EC3526FDF5E3ABB78F764E00A1F8 * value)
	{
		____cachedGravityZone_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cachedGravityZone_54), (void*)value);
	}

	inline static int32_t get_offset_of__characterDash_55() { return static_cast<int32_t>(offsetof(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4, ____characterDash_55)); }
	inline CharacterDash_t78AF1BB1E2C303712001AD86D2041FC0B7A011AD * get__characterDash_55() const { return ____characterDash_55; }
	inline CharacterDash_t78AF1BB1E2C303712001AD86D2041FC0B7A011AD ** get_address_of__characterDash_55() { return &____characterDash_55; }
	inline void set__characterDash_55(CharacterDash_t78AF1BB1E2C303712001AD86D2041FC0B7A011AD * value)
	{
		____characterDash_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterDash_55), (void*)value);
	}
};


// UnityEngine.EventSystems.EventSystem
struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.BaseInputModule> UnityEngine.EventSystems.EventSystem::m_SystemInputModules
	List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * ___m_SystemInputModules_4;
	// UnityEngine.EventSystems.BaseInputModule UnityEngine.EventSystems.EventSystem::m_CurrentInputModule
	BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * ___m_CurrentInputModule_5;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_FirstSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_FirstSelected_7;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_sendNavigationEvents
	bool ___m_sendNavigationEvents_8;
	// System.Int32 UnityEngine.EventSystems.EventSystem::m_DragThreshold
	int32_t ___m_DragThreshold_9;
	// UnityEngine.GameObject UnityEngine.EventSystems.EventSystem::m_CurrentSelected
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_CurrentSelected_10;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_HasFocus
	bool ___m_HasFocus_11;
	// System.Boolean UnityEngine.EventSystems.EventSystem::m_SelectionGuard
	bool ___m_SelectionGuard_12;
	// UnityEngine.EventSystems.BaseEventData UnityEngine.EventSystems.EventSystem::m_DummyData
	BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___m_DummyData_13;

public:
	inline static int32_t get_offset_of_m_SystemInputModules_4() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SystemInputModules_4)); }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * get_m_SystemInputModules_4() const { return ___m_SystemInputModules_4; }
	inline List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 ** get_address_of_m_SystemInputModules_4() { return &___m_SystemInputModules_4; }
	inline void set_m_SystemInputModules_4(List_1_t39946D94B66FAE9B0DED5D3A84AD116AF9DDDCC1 * value)
	{
		___m_SystemInputModules_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SystemInputModules_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_CurrentInputModule_5() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentInputModule_5)); }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * get_m_CurrentInputModule_5() const { return ___m_CurrentInputModule_5; }
	inline BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 ** get_address_of_m_CurrentInputModule_5() { return &___m_CurrentInputModule_5; }
	inline void set_m_CurrentInputModule_5(BaseInputModule_t395DEB45C225A941B2C831CBDB000A23E5899924 * value)
	{
		___m_CurrentInputModule_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentInputModule_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_FirstSelected_7() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_FirstSelected_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_FirstSelected_7() const { return ___m_FirstSelected_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_FirstSelected_7() { return &___m_FirstSelected_7; }
	inline void set_m_FirstSelected_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_FirstSelected_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FirstSelected_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_sendNavigationEvents_8() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_sendNavigationEvents_8)); }
	inline bool get_m_sendNavigationEvents_8() const { return ___m_sendNavigationEvents_8; }
	inline bool* get_address_of_m_sendNavigationEvents_8() { return &___m_sendNavigationEvents_8; }
	inline void set_m_sendNavigationEvents_8(bool value)
	{
		___m_sendNavigationEvents_8 = value;
	}

	inline static int32_t get_offset_of_m_DragThreshold_9() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DragThreshold_9)); }
	inline int32_t get_m_DragThreshold_9() const { return ___m_DragThreshold_9; }
	inline int32_t* get_address_of_m_DragThreshold_9() { return &___m_DragThreshold_9; }
	inline void set_m_DragThreshold_9(int32_t value)
	{
		___m_DragThreshold_9 = value;
	}

	inline static int32_t get_offset_of_m_CurrentSelected_10() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_CurrentSelected_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_CurrentSelected_10() const { return ___m_CurrentSelected_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_CurrentSelected_10() { return &___m_CurrentSelected_10; }
	inline void set_m_CurrentSelected_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_CurrentSelected_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CurrentSelected_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_HasFocus_11() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_HasFocus_11)); }
	inline bool get_m_HasFocus_11() const { return ___m_HasFocus_11; }
	inline bool* get_address_of_m_HasFocus_11() { return &___m_HasFocus_11; }
	inline void set_m_HasFocus_11(bool value)
	{
		___m_HasFocus_11 = value;
	}

	inline static int32_t get_offset_of_m_SelectionGuard_12() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_SelectionGuard_12)); }
	inline bool get_m_SelectionGuard_12() const { return ___m_SelectionGuard_12; }
	inline bool* get_address_of_m_SelectionGuard_12() { return &___m_SelectionGuard_12; }
	inline void set_m_SelectionGuard_12(bool value)
	{
		___m_SelectionGuard_12 = value;
	}

	inline static int32_t get_offset_of_m_DummyData_13() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C, ___m_DummyData_13)); }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * get_m_DummyData_13() const { return ___m_DummyData_13; }
	inline BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E ** get_address_of_m_DummyData_13() { return &___m_DummyData_13; }
	inline void set_m_DummyData_13(BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * value)
	{
		___m_DummyData_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DummyData_13), (void*)value);
	}
};

struct EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.EventSystems.EventSystem> UnityEngine.EventSystems.EventSystem::m_EventSystems
	List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * ___m_EventSystems_6;
	// System.Comparison`1<UnityEngine.EventSystems.RaycastResult> UnityEngine.EventSystems.EventSystem::s_RaycastComparer
	Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * ___s_RaycastComparer_14;

public:
	inline static int32_t get_offset_of_m_EventSystems_6() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___m_EventSystems_6)); }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * get_m_EventSystems_6() const { return ___m_EventSystems_6; }
	inline List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 ** get_address_of_m_EventSystems_6() { return &___m_EventSystems_6; }
	inline void set_m_EventSystems_6(List_1_tEF3D2378B547F18609950BEABF54AF34FBBC9733 * value)
	{
		___m_EventSystems_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_EventSystems_6), (void*)value);
	}

	inline static int32_t get_offset_of_s_RaycastComparer_14() { return static_cast<int32_t>(offsetof(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_StaticFields, ___s_RaycastComparer_14)); }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * get_s_RaycastComparer_14() const { return ___s_RaycastComparer_14; }
	inline Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 ** get_address_of_s_RaycastComparer_14() { return &___s_RaycastComparer_14; }
	inline void set_s_RaycastComparer_14(Comparison_1_t47C8B3739FFDD51D29B281A2FD2C36A57DDF9E38 * value)
	{
		___s_RaycastComparer_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_RaycastComparer_14), (void*)value);
	}
};


// MoreMountains.CorgiEngine.GUIManager
struct GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5  : public MMSingleton_1_t22F86A56A6541E0ECE39DDD17501869CE6D4CC0D
{
public:
	// UnityEngine.GameObject MoreMountains.CorgiEngine.GUIManager::HUD
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___HUD_5;
	// MoreMountains.Tools.MMProgressBar[] MoreMountains.CorgiEngine.GUIManager::HealthBars
	MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE* ___HealthBars_6;
	// MoreMountains.Tools.MMProgressBar[] MoreMountains.CorgiEngine.GUIManager::JetPackBars
	MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE* ___JetPackBars_7;
	// MoreMountains.CorgiEngine.AmmoDisplay[] MoreMountains.CorgiEngine.GUIManager::AmmoDisplays
	AmmoDisplayU5BU5D_t5C3580C3BC1F66C524424F7F251B3017F375F296* ___AmmoDisplays_8;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.GUIManager::PauseScreen
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___PauseScreen_9;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.GUIManager::TimeSplash
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___TimeSplash_10;
	// UnityEngine.CanvasGroup MoreMountains.CorgiEngine.GUIManager::Buttons
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___Buttons_11;
	// UnityEngine.CanvasGroup MoreMountains.CorgiEngine.GUIManager::Arrows
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___Arrows_12;
	// UnityEngine.CanvasGroup MoreMountains.CorgiEngine.GUIManager::Joystick
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___Joystick_13;
	// UnityEngine.UI.Text MoreMountains.CorgiEngine.GUIManager::PointsText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___PointsText_14;
	// UnityEngine.UI.Text MoreMountains.CorgiEngine.GUIManager::LevelText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___LevelText_15;
	// System.String MoreMountains.CorgiEngine.GUIManager::PointsPattern
	String_t* ___PointsPattern_16;
	// System.Single MoreMountains.CorgiEngine.GUIManager::_initialJoystickAlpha
	float ____initialJoystickAlpha_17;
	// System.Single MoreMountains.CorgiEngine.GUIManager::_initialArrowsAlpha
	float ____initialArrowsAlpha_18;
	// System.Single MoreMountains.CorgiEngine.GUIManager::_initialButtonsAlpha
	float ____initialButtonsAlpha_19;

public:
	inline static int32_t get_offset_of_HUD_5() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___HUD_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_HUD_5() const { return ___HUD_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_HUD_5() { return &___HUD_5; }
	inline void set_HUD_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___HUD_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HUD_5), (void*)value);
	}

	inline static int32_t get_offset_of_HealthBars_6() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___HealthBars_6)); }
	inline MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE* get_HealthBars_6() const { return ___HealthBars_6; }
	inline MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE** get_address_of_HealthBars_6() { return &___HealthBars_6; }
	inline void set_HealthBars_6(MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE* value)
	{
		___HealthBars_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HealthBars_6), (void*)value);
	}

	inline static int32_t get_offset_of_JetPackBars_7() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___JetPackBars_7)); }
	inline MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE* get_JetPackBars_7() const { return ___JetPackBars_7; }
	inline MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE** get_address_of_JetPackBars_7() { return &___JetPackBars_7; }
	inline void set_JetPackBars_7(MMProgressBarU5BU5D_t9B960ECAF7819580017B2AAAA7334F3F537095AE* value)
	{
		___JetPackBars_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___JetPackBars_7), (void*)value);
	}

	inline static int32_t get_offset_of_AmmoDisplays_8() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___AmmoDisplays_8)); }
	inline AmmoDisplayU5BU5D_t5C3580C3BC1F66C524424F7F251B3017F375F296* get_AmmoDisplays_8() const { return ___AmmoDisplays_8; }
	inline AmmoDisplayU5BU5D_t5C3580C3BC1F66C524424F7F251B3017F375F296** get_address_of_AmmoDisplays_8() { return &___AmmoDisplays_8; }
	inline void set_AmmoDisplays_8(AmmoDisplayU5BU5D_t5C3580C3BC1F66C524424F7F251B3017F375F296* value)
	{
		___AmmoDisplays_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AmmoDisplays_8), (void*)value);
	}

	inline static int32_t get_offset_of_PauseScreen_9() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___PauseScreen_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_PauseScreen_9() const { return ___PauseScreen_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_PauseScreen_9() { return &___PauseScreen_9; }
	inline void set_PauseScreen_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___PauseScreen_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PauseScreen_9), (void*)value);
	}

	inline static int32_t get_offset_of_TimeSplash_10() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___TimeSplash_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_TimeSplash_10() const { return ___TimeSplash_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_TimeSplash_10() { return &___TimeSplash_10; }
	inline void set_TimeSplash_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___TimeSplash_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TimeSplash_10), (void*)value);
	}

	inline static int32_t get_offset_of_Buttons_11() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___Buttons_11)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_Buttons_11() const { return ___Buttons_11; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_Buttons_11() { return &___Buttons_11; }
	inline void set_Buttons_11(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___Buttons_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Buttons_11), (void*)value);
	}

	inline static int32_t get_offset_of_Arrows_12() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___Arrows_12)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_Arrows_12() const { return ___Arrows_12; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_Arrows_12() { return &___Arrows_12; }
	inline void set_Arrows_12(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___Arrows_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Arrows_12), (void*)value);
	}

	inline static int32_t get_offset_of_Joystick_13() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___Joystick_13)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_Joystick_13() const { return ___Joystick_13; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_Joystick_13() { return &___Joystick_13; }
	inline void set_Joystick_13(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___Joystick_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Joystick_13), (void*)value);
	}

	inline static int32_t get_offset_of_PointsText_14() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___PointsText_14)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_PointsText_14() const { return ___PointsText_14; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_PointsText_14() { return &___PointsText_14; }
	inline void set_PointsText_14(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___PointsText_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PointsText_14), (void*)value);
	}

	inline static int32_t get_offset_of_LevelText_15() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___LevelText_15)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_LevelText_15() const { return ___LevelText_15; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_LevelText_15() { return &___LevelText_15; }
	inline void set_LevelText_15(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___LevelText_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelText_15), (void*)value);
	}

	inline static int32_t get_offset_of_PointsPattern_16() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ___PointsPattern_16)); }
	inline String_t* get_PointsPattern_16() const { return ___PointsPattern_16; }
	inline String_t** get_address_of_PointsPattern_16() { return &___PointsPattern_16; }
	inline void set_PointsPattern_16(String_t* value)
	{
		___PointsPattern_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PointsPattern_16), (void*)value);
	}

	inline static int32_t get_offset_of__initialJoystickAlpha_17() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ____initialJoystickAlpha_17)); }
	inline float get__initialJoystickAlpha_17() const { return ____initialJoystickAlpha_17; }
	inline float* get_address_of__initialJoystickAlpha_17() { return &____initialJoystickAlpha_17; }
	inline void set__initialJoystickAlpha_17(float value)
	{
		____initialJoystickAlpha_17 = value;
	}

	inline static int32_t get_offset_of__initialArrowsAlpha_18() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ____initialArrowsAlpha_18)); }
	inline float get__initialArrowsAlpha_18() const { return ____initialArrowsAlpha_18; }
	inline float* get_address_of__initialArrowsAlpha_18() { return &____initialArrowsAlpha_18; }
	inline void set__initialArrowsAlpha_18(float value)
	{
		____initialArrowsAlpha_18 = value;
	}

	inline static int32_t get_offset_of__initialButtonsAlpha_19() { return static_cast<int32_t>(offsetof(GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5, ____initialButtonsAlpha_19)); }
	inline float get__initialButtonsAlpha_19() const { return ____initialButtonsAlpha_19; }
	inline float* get_address_of__initialButtonsAlpha_19() { return &____initialButtonsAlpha_19; }
	inline void set__initialButtonsAlpha_19(float value)
	{
		____initialButtonsAlpha_19 = value;
	}
};


// MoreMountains.CorgiEngine.GameManager
struct GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A  : public MMPersistentSingleton_1_t41D3BEA3FEE7D79C238ABF48390643722E7F3664
{
public:
	// System.Int32 MoreMountains.CorgiEngine.GameManager::TargetFrameRate
	int32_t ___TargetFrameRate_7;
	// System.Int32 MoreMountains.CorgiEngine.GameManager::MaximumLives
	int32_t ___MaximumLives_8;
	// System.Int32 MoreMountains.CorgiEngine.GameManager::CurrentLives
	int32_t ___CurrentLives_9;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::ResetLivesOnGameOver
	bool ___ResetLivesOnGameOver_10;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::ResetPersistentCharacterOnGameOver
	bool ___ResetPersistentCharacterOnGameOver_11;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::ResetStoredCharacterOnGameOver
	bool ___ResetStoredCharacterOnGameOver_12;
	// System.String MoreMountains.CorgiEngine.GameManager::GameOverScene
	String_t* ___GameOverScene_13;
	// System.Int32 MoreMountains.CorgiEngine.GameManager::<Points>k__BackingField
	int32_t ___U3CPointsU3Ek__BackingField_14;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::<Paused>k__BackingField
	bool ___U3CPausedU3Ek__BackingField_15;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::<StoredLevelMapPosition>k__BackingField
	bool ___U3CStoredLevelMapPositionU3Ek__BackingField_16;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.GameManager::<LevelMapPosition>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CLevelMapPositionU3Ek__BackingField_17;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.GameManager::<StoredCharacter>k__BackingField
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___U3CStoredCharacterU3Ek__BackingField_18;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.GameManager::<PersistentCharacter>k__BackingField
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___U3CPersistentCharacterU3Ek__BackingField_19;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointsOfEntryStorage> MoreMountains.CorgiEngine.GameManager::<PointsOfEntry>k__BackingField
	List_1_tC2567F05E952D8D17D6046D224FFB01182F23845 * ___U3CPointsOfEntryU3Ek__BackingField_20;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::_inventoryOpen
	bool ____inventoryOpen_21;
	// System.Boolean MoreMountains.CorgiEngine.GameManager::_pauseMenuOpen
	bool ____pauseMenuOpen_22;
	// MoreMountains.InventoryEngine.InventoryInputManager MoreMountains.CorgiEngine.GameManager::_inventoryInputManager
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC * ____inventoryInputManager_23;
	// System.Int32 MoreMountains.CorgiEngine.GameManager::_initialMaximumLives
	int32_t ____initialMaximumLives_24;
	// System.Int32 MoreMountains.CorgiEngine.GameManager::_initialCurrentLives
	int32_t ____initialCurrentLives_25;

public:
	inline static int32_t get_offset_of_TargetFrameRate_7() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___TargetFrameRate_7)); }
	inline int32_t get_TargetFrameRate_7() const { return ___TargetFrameRate_7; }
	inline int32_t* get_address_of_TargetFrameRate_7() { return &___TargetFrameRate_7; }
	inline void set_TargetFrameRate_7(int32_t value)
	{
		___TargetFrameRate_7 = value;
	}

	inline static int32_t get_offset_of_MaximumLives_8() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___MaximumLives_8)); }
	inline int32_t get_MaximumLives_8() const { return ___MaximumLives_8; }
	inline int32_t* get_address_of_MaximumLives_8() { return &___MaximumLives_8; }
	inline void set_MaximumLives_8(int32_t value)
	{
		___MaximumLives_8 = value;
	}

	inline static int32_t get_offset_of_CurrentLives_9() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___CurrentLives_9)); }
	inline int32_t get_CurrentLives_9() const { return ___CurrentLives_9; }
	inline int32_t* get_address_of_CurrentLives_9() { return &___CurrentLives_9; }
	inline void set_CurrentLives_9(int32_t value)
	{
		___CurrentLives_9 = value;
	}

	inline static int32_t get_offset_of_ResetLivesOnGameOver_10() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___ResetLivesOnGameOver_10)); }
	inline bool get_ResetLivesOnGameOver_10() const { return ___ResetLivesOnGameOver_10; }
	inline bool* get_address_of_ResetLivesOnGameOver_10() { return &___ResetLivesOnGameOver_10; }
	inline void set_ResetLivesOnGameOver_10(bool value)
	{
		___ResetLivesOnGameOver_10 = value;
	}

	inline static int32_t get_offset_of_ResetPersistentCharacterOnGameOver_11() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___ResetPersistentCharacterOnGameOver_11)); }
	inline bool get_ResetPersistentCharacterOnGameOver_11() const { return ___ResetPersistentCharacterOnGameOver_11; }
	inline bool* get_address_of_ResetPersistentCharacterOnGameOver_11() { return &___ResetPersistentCharacterOnGameOver_11; }
	inline void set_ResetPersistentCharacterOnGameOver_11(bool value)
	{
		___ResetPersistentCharacterOnGameOver_11 = value;
	}

	inline static int32_t get_offset_of_ResetStoredCharacterOnGameOver_12() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___ResetStoredCharacterOnGameOver_12)); }
	inline bool get_ResetStoredCharacterOnGameOver_12() const { return ___ResetStoredCharacterOnGameOver_12; }
	inline bool* get_address_of_ResetStoredCharacterOnGameOver_12() { return &___ResetStoredCharacterOnGameOver_12; }
	inline void set_ResetStoredCharacterOnGameOver_12(bool value)
	{
		___ResetStoredCharacterOnGameOver_12 = value;
	}

	inline static int32_t get_offset_of_GameOverScene_13() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___GameOverScene_13)); }
	inline String_t* get_GameOverScene_13() const { return ___GameOverScene_13; }
	inline String_t** get_address_of_GameOverScene_13() { return &___GameOverScene_13; }
	inline void set_GameOverScene_13(String_t* value)
	{
		___GameOverScene_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GameOverScene_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPointsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CPointsU3Ek__BackingField_14)); }
	inline int32_t get_U3CPointsU3Ek__BackingField_14() const { return ___U3CPointsU3Ek__BackingField_14; }
	inline int32_t* get_address_of_U3CPointsU3Ek__BackingField_14() { return &___U3CPointsU3Ek__BackingField_14; }
	inline void set_U3CPointsU3Ek__BackingField_14(int32_t value)
	{
		___U3CPointsU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CPausedU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CPausedU3Ek__BackingField_15)); }
	inline bool get_U3CPausedU3Ek__BackingField_15() const { return ___U3CPausedU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CPausedU3Ek__BackingField_15() { return &___U3CPausedU3Ek__BackingField_15; }
	inline void set_U3CPausedU3Ek__BackingField_15(bool value)
	{
		___U3CPausedU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CStoredLevelMapPositionU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CStoredLevelMapPositionU3Ek__BackingField_16)); }
	inline bool get_U3CStoredLevelMapPositionU3Ek__BackingField_16() const { return ___U3CStoredLevelMapPositionU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CStoredLevelMapPositionU3Ek__BackingField_16() { return &___U3CStoredLevelMapPositionU3Ek__BackingField_16; }
	inline void set_U3CStoredLevelMapPositionU3Ek__BackingField_16(bool value)
	{
		___U3CStoredLevelMapPositionU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CLevelMapPositionU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CLevelMapPositionU3Ek__BackingField_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CLevelMapPositionU3Ek__BackingField_17() const { return ___U3CLevelMapPositionU3Ek__BackingField_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CLevelMapPositionU3Ek__BackingField_17() { return &___U3CLevelMapPositionU3Ek__BackingField_17; }
	inline void set_U3CLevelMapPositionU3Ek__BackingField_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CLevelMapPositionU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CStoredCharacterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CStoredCharacterU3Ek__BackingField_18)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get_U3CStoredCharacterU3Ek__BackingField_18() const { return ___U3CStoredCharacterU3Ek__BackingField_18; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of_U3CStoredCharacterU3Ek__BackingField_18() { return &___U3CStoredCharacterU3Ek__BackingField_18; }
	inline void set_U3CStoredCharacterU3Ek__BackingField_18(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		___U3CStoredCharacterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStoredCharacterU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPersistentCharacterU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CPersistentCharacterU3Ek__BackingField_19)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get_U3CPersistentCharacterU3Ek__BackingField_19() const { return ___U3CPersistentCharacterU3Ek__BackingField_19; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of_U3CPersistentCharacterU3Ek__BackingField_19() { return &___U3CPersistentCharacterU3Ek__BackingField_19; }
	inline void set_U3CPersistentCharacterU3Ek__BackingField_19(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		___U3CPersistentCharacterU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPersistentCharacterU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPointsOfEntryU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ___U3CPointsOfEntryU3Ek__BackingField_20)); }
	inline List_1_tC2567F05E952D8D17D6046D224FFB01182F23845 * get_U3CPointsOfEntryU3Ek__BackingField_20() const { return ___U3CPointsOfEntryU3Ek__BackingField_20; }
	inline List_1_tC2567F05E952D8D17D6046D224FFB01182F23845 ** get_address_of_U3CPointsOfEntryU3Ek__BackingField_20() { return &___U3CPointsOfEntryU3Ek__BackingField_20; }
	inline void set_U3CPointsOfEntryU3Ek__BackingField_20(List_1_tC2567F05E952D8D17D6046D224FFB01182F23845 * value)
	{
		___U3CPointsOfEntryU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPointsOfEntryU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of__inventoryOpen_21() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ____inventoryOpen_21)); }
	inline bool get__inventoryOpen_21() const { return ____inventoryOpen_21; }
	inline bool* get_address_of__inventoryOpen_21() { return &____inventoryOpen_21; }
	inline void set__inventoryOpen_21(bool value)
	{
		____inventoryOpen_21 = value;
	}

	inline static int32_t get_offset_of__pauseMenuOpen_22() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ____pauseMenuOpen_22)); }
	inline bool get__pauseMenuOpen_22() const { return ____pauseMenuOpen_22; }
	inline bool* get_address_of__pauseMenuOpen_22() { return &____pauseMenuOpen_22; }
	inline void set__pauseMenuOpen_22(bool value)
	{
		____pauseMenuOpen_22 = value;
	}

	inline static int32_t get_offset_of__inventoryInputManager_23() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ____inventoryInputManager_23)); }
	inline InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC * get__inventoryInputManager_23() const { return ____inventoryInputManager_23; }
	inline InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC ** get_address_of__inventoryInputManager_23() { return &____inventoryInputManager_23; }
	inline void set__inventoryInputManager_23(InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC * value)
	{
		____inventoryInputManager_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____inventoryInputManager_23), (void*)value);
	}

	inline static int32_t get_offset_of__initialMaximumLives_24() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ____initialMaximumLives_24)); }
	inline int32_t get__initialMaximumLives_24() const { return ____initialMaximumLives_24; }
	inline int32_t* get_address_of__initialMaximumLives_24() { return &____initialMaximumLives_24; }
	inline void set__initialMaximumLives_24(int32_t value)
	{
		____initialMaximumLives_24 = value;
	}

	inline static int32_t get_offset_of__initialCurrentLives_25() { return static_cast<int32_t>(offsetof(GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A, ____initialCurrentLives_25)); }
	inline int32_t get__initialCurrentLives_25() const { return ____initialCurrentLives_25; }
	inline int32_t* get_address_of__initialCurrentLives_25() { return &____initialCurrentLives_25; }
	inline void set__initialCurrentLives_25(int32_t value)
	{
		____initialCurrentLives_25 = value;
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// MoreMountains.CorgiEngine.InputManager
struct InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA  : public MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597
{
public:
	// System.Boolean MoreMountains.CorgiEngine.InputManager::InputDetectionActive
	bool ___InputDetectionActive_5;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::ResetButtonStatesOnFocusLoss
	bool ___ResetButtonStatesOnFocusLoss_6;
	// System.String MoreMountains.CorgiEngine.InputManager::PlayerID
	String_t* ___PlayerID_7;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::AutoMobileDetection
	bool ___AutoMobileDetection_8;
	// MoreMountains.CorgiEngine.InputManager/InputForcedMode MoreMountains.CorgiEngine.InputManager::ForcedMode
	int32_t ___ForcedMode_9;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::HideMobileControlsInEditor
	bool ___HideMobileControlsInEditor_10;
	// MoreMountains.CorgiEngine.InputManager/MovementControls MoreMountains.CorgiEngine.InputManager::MovementControl
	int32_t ___MovementControl_11;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::DelayedButtonPresses
	bool ___DelayedButtonPresses_12;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::<IsMobile>k__BackingField
	bool ___U3CIsMobileU3Ek__BackingField_13;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::SmoothMovement
	bool ___SmoothMovement_14;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::Threshold
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___Threshold_15;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<JumpButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CJumpButtonU3Ek__BackingField_16;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SwimButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSwimButtonU3Ek__BackingField_17;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<GlideButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CGlideButtonU3Ek__BackingField_18;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<InteractButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CInteractButtonU3Ek__BackingField_19;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<JetpackButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CJetpackButtonU3Ek__BackingField_20;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<FlyButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CFlyButtonU3Ek__BackingField_21;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<RunButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CRunButtonU3Ek__BackingField_22;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<DashButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CDashButtonU3Ek__BackingField_23;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<RollButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CRollButtonU3Ek__BackingField_24;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<GrabButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CGrabButtonU3Ek__BackingField_25;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<ThrowButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CThrowButtonU3Ek__BackingField_26;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<ShootButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CShootButtonU3Ek__BackingField_27;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SecondaryShootButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSecondaryShootButtonU3Ek__BackingField_28;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<ReloadButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CReloadButtonU3Ek__BackingField_29;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<PushButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CPushButtonU3Ek__BackingField_30;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<GripButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CGripButtonU3Ek__BackingField_31;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<PauseButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CPauseButtonU3Ek__BackingField_32;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SwitchCharacterButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSwitchCharacterButtonU3Ek__BackingField_33;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SwitchWeaponButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSwitchWeaponButtonU3Ek__BackingField_34;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<TimeControlButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CTimeControlButtonU3Ek__BackingField_35;
	// MoreMountains.Tools.MMInput/ButtonStates MoreMountains.CorgiEngine.InputManager::<ShootAxis>k__BackingField
	int32_t ___U3CShootAxisU3Ek__BackingField_36;
	// MoreMountains.Tools.MMInput/ButtonStates MoreMountains.CorgiEngine.InputManager::<SecondaryShootAxis>k__BackingField
	int32_t ___U3CSecondaryShootAxisU3Ek__BackingField_37;
	// System.Collections.Generic.List`1<MoreMountains.Tools.MMInput/IMButton> MoreMountains.CorgiEngine.InputManager::ButtonList
	List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B * ___ButtonList_38;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::_primaryMovement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____primaryMovement_39;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::_secondaryMovement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____secondaryMovement_40;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisHorizontal
	String_t* ____axisHorizontal_41;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisVertical
	String_t* ____axisVertical_42;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisSecondaryHorizontal
	String_t* ____axisSecondaryHorizontal_43;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisSecondaryVertical
	String_t* ____axisSecondaryVertical_44;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisShoot
	String_t* ____axisShoot_45;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisShootSecondary
	String_t* ____axisShootSecondary_46;

public:
	inline static int32_t get_offset_of_InputDetectionActive_5() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___InputDetectionActive_5)); }
	inline bool get_InputDetectionActive_5() const { return ___InputDetectionActive_5; }
	inline bool* get_address_of_InputDetectionActive_5() { return &___InputDetectionActive_5; }
	inline void set_InputDetectionActive_5(bool value)
	{
		___InputDetectionActive_5 = value;
	}

	inline static int32_t get_offset_of_ResetButtonStatesOnFocusLoss_6() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___ResetButtonStatesOnFocusLoss_6)); }
	inline bool get_ResetButtonStatesOnFocusLoss_6() const { return ___ResetButtonStatesOnFocusLoss_6; }
	inline bool* get_address_of_ResetButtonStatesOnFocusLoss_6() { return &___ResetButtonStatesOnFocusLoss_6; }
	inline void set_ResetButtonStatesOnFocusLoss_6(bool value)
	{
		___ResetButtonStatesOnFocusLoss_6 = value;
	}

	inline static int32_t get_offset_of_PlayerID_7() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___PlayerID_7)); }
	inline String_t* get_PlayerID_7() const { return ___PlayerID_7; }
	inline String_t** get_address_of_PlayerID_7() { return &___PlayerID_7; }
	inline void set_PlayerID_7(String_t* value)
	{
		___PlayerID_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_7), (void*)value);
	}

	inline static int32_t get_offset_of_AutoMobileDetection_8() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___AutoMobileDetection_8)); }
	inline bool get_AutoMobileDetection_8() const { return ___AutoMobileDetection_8; }
	inline bool* get_address_of_AutoMobileDetection_8() { return &___AutoMobileDetection_8; }
	inline void set_AutoMobileDetection_8(bool value)
	{
		___AutoMobileDetection_8 = value;
	}

	inline static int32_t get_offset_of_ForcedMode_9() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___ForcedMode_9)); }
	inline int32_t get_ForcedMode_9() const { return ___ForcedMode_9; }
	inline int32_t* get_address_of_ForcedMode_9() { return &___ForcedMode_9; }
	inline void set_ForcedMode_9(int32_t value)
	{
		___ForcedMode_9 = value;
	}

	inline static int32_t get_offset_of_HideMobileControlsInEditor_10() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___HideMobileControlsInEditor_10)); }
	inline bool get_HideMobileControlsInEditor_10() const { return ___HideMobileControlsInEditor_10; }
	inline bool* get_address_of_HideMobileControlsInEditor_10() { return &___HideMobileControlsInEditor_10; }
	inline void set_HideMobileControlsInEditor_10(bool value)
	{
		___HideMobileControlsInEditor_10 = value;
	}

	inline static int32_t get_offset_of_MovementControl_11() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___MovementControl_11)); }
	inline int32_t get_MovementControl_11() const { return ___MovementControl_11; }
	inline int32_t* get_address_of_MovementControl_11() { return &___MovementControl_11; }
	inline void set_MovementControl_11(int32_t value)
	{
		___MovementControl_11 = value;
	}

	inline static int32_t get_offset_of_DelayedButtonPresses_12() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___DelayedButtonPresses_12)); }
	inline bool get_DelayedButtonPresses_12() const { return ___DelayedButtonPresses_12; }
	inline bool* get_address_of_DelayedButtonPresses_12() { return &___DelayedButtonPresses_12; }
	inline void set_DelayedButtonPresses_12(bool value)
	{
		___DelayedButtonPresses_12 = value;
	}

	inline static int32_t get_offset_of_U3CIsMobileU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CIsMobileU3Ek__BackingField_13)); }
	inline bool get_U3CIsMobileU3Ek__BackingField_13() const { return ___U3CIsMobileU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsMobileU3Ek__BackingField_13() { return &___U3CIsMobileU3Ek__BackingField_13; }
	inline void set_U3CIsMobileU3Ek__BackingField_13(bool value)
	{
		___U3CIsMobileU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_SmoothMovement_14() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___SmoothMovement_14)); }
	inline bool get_SmoothMovement_14() const { return ___SmoothMovement_14; }
	inline bool* get_address_of_SmoothMovement_14() { return &___SmoothMovement_14; }
	inline void set_SmoothMovement_14(bool value)
	{
		___SmoothMovement_14 = value;
	}

	inline static int32_t get_offset_of_Threshold_15() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___Threshold_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_Threshold_15() const { return ___Threshold_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_Threshold_15() { return &___Threshold_15; }
	inline void set_Threshold_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___Threshold_15 = value;
	}

	inline static int32_t get_offset_of_U3CJumpButtonU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CJumpButtonU3Ek__BackingField_16)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CJumpButtonU3Ek__BackingField_16() const { return ___U3CJumpButtonU3Ek__BackingField_16; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CJumpButtonU3Ek__BackingField_16() { return &___U3CJumpButtonU3Ek__BackingField_16; }
	inline void set_U3CJumpButtonU3Ek__BackingField_16(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CJumpButtonU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CJumpButtonU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwimButtonU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSwimButtonU3Ek__BackingField_17)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSwimButtonU3Ek__BackingField_17() const { return ___U3CSwimButtonU3Ek__BackingField_17; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSwimButtonU3Ek__BackingField_17() { return &___U3CSwimButtonU3Ek__BackingField_17; }
	inline void set_U3CSwimButtonU3Ek__BackingField_17(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSwimButtonU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwimButtonU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGlideButtonU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CGlideButtonU3Ek__BackingField_18)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CGlideButtonU3Ek__BackingField_18() const { return ___U3CGlideButtonU3Ek__BackingField_18; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CGlideButtonU3Ek__BackingField_18() { return &___U3CGlideButtonU3Ek__BackingField_18; }
	inline void set_U3CGlideButtonU3Ek__BackingField_18(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CGlideButtonU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGlideButtonU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CInteractButtonU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CInteractButtonU3Ek__BackingField_19)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CInteractButtonU3Ek__BackingField_19() const { return ___U3CInteractButtonU3Ek__BackingField_19; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CInteractButtonU3Ek__BackingField_19() { return &___U3CInteractButtonU3Ek__BackingField_19; }
	inline void set_U3CInteractButtonU3Ek__BackingField_19(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CInteractButtonU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInteractButtonU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CJetpackButtonU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CJetpackButtonU3Ek__BackingField_20)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CJetpackButtonU3Ek__BackingField_20() const { return ___U3CJetpackButtonU3Ek__BackingField_20; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CJetpackButtonU3Ek__BackingField_20() { return &___U3CJetpackButtonU3Ek__BackingField_20; }
	inline void set_U3CJetpackButtonU3Ek__BackingField_20(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CJetpackButtonU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CJetpackButtonU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFlyButtonU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CFlyButtonU3Ek__BackingField_21)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CFlyButtonU3Ek__BackingField_21() const { return ___U3CFlyButtonU3Ek__BackingField_21; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CFlyButtonU3Ek__BackingField_21() { return &___U3CFlyButtonU3Ek__BackingField_21; }
	inline void set_U3CFlyButtonU3Ek__BackingField_21(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CFlyButtonU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFlyButtonU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRunButtonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CRunButtonU3Ek__BackingField_22)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CRunButtonU3Ek__BackingField_22() const { return ___U3CRunButtonU3Ek__BackingField_22; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CRunButtonU3Ek__BackingField_22() { return &___U3CRunButtonU3Ek__BackingField_22; }
	inline void set_U3CRunButtonU3Ek__BackingField_22(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CRunButtonU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRunButtonU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDashButtonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CDashButtonU3Ek__BackingField_23)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CDashButtonU3Ek__BackingField_23() const { return ___U3CDashButtonU3Ek__BackingField_23; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CDashButtonU3Ek__BackingField_23() { return &___U3CDashButtonU3Ek__BackingField_23; }
	inline void set_U3CDashButtonU3Ek__BackingField_23(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CDashButtonU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDashButtonU3Ek__BackingField_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRollButtonU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CRollButtonU3Ek__BackingField_24)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CRollButtonU3Ek__BackingField_24() const { return ___U3CRollButtonU3Ek__BackingField_24; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CRollButtonU3Ek__BackingField_24() { return &___U3CRollButtonU3Ek__BackingField_24; }
	inline void set_U3CRollButtonU3Ek__BackingField_24(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CRollButtonU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRollButtonU3Ek__BackingField_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGrabButtonU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CGrabButtonU3Ek__BackingField_25)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CGrabButtonU3Ek__BackingField_25() const { return ___U3CGrabButtonU3Ek__BackingField_25; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CGrabButtonU3Ek__BackingField_25() { return &___U3CGrabButtonU3Ek__BackingField_25; }
	inline void set_U3CGrabButtonU3Ek__BackingField_25(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CGrabButtonU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGrabButtonU3Ek__BackingField_25), (void*)value);
	}

	inline static int32_t get_offset_of_U3CThrowButtonU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CThrowButtonU3Ek__BackingField_26)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CThrowButtonU3Ek__BackingField_26() const { return ___U3CThrowButtonU3Ek__BackingField_26; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CThrowButtonU3Ek__BackingField_26() { return &___U3CThrowButtonU3Ek__BackingField_26; }
	inline void set_U3CThrowButtonU3Ek__BackingField_26(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CThrowButtonU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CThrowButtonU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3CShootButtonU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CShootButtonU3Ek__BackingField_27)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CShootButtonU3Ek__BackingField_27() const { return ___U3CShootButtonU3Ek__BackingField_27; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CShootButtonU3Ek__BackingField_27() { return &___U3CShootButtonU3Ek__BackingField_27; }
	inline void set_U3CShootButtonU3Ek__BackingField_27(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CShootButtonU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CShootButtonU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSecondaryShootButtonU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSecondaryShootButtonU3Ek__BackingField_28)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSecondaryShootButtonU3Ek__BackingField_28() const { return ___U3CSecondaryShootButtonU3Ek__BackingField_28; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSecondaryShootButtonU3Ek__BackingField_28() { return &___U3CSecondaryShootButtonU3Ek__BackingField_28; }
	inline void set_U3CSecondaryShootButtonU3Ek__BackingField_28(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSecondaryShootButtonU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSecondaryShootButtonU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of_U3CReloadButtonU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CReloadButtonU3Ek__BackingField_29)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CReloadButtonU3Ek__BackingField_29() const { return ___U3CReloadButtonU3Ek__BackingField_29; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CReloadButtonU3Ek__BackingField_29() { return &___U3CReloadButtonU3Ek__BackingField_29; }
	inline void set_U3CReloadButtonU3Ek__BackingField_29(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CReloadButtonU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CReloadButtonU3Ek__BackingField_29), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPushButtonU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CPushButtonU3Ek__BackingField_30)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CPushButtonU3Ek__BackingField_30() const { return ___U3CPushButtonU3Ek__BackingField_30; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CPushButtonU3Ek__BackingField_30() { return &___U3CPushButtonU3Ek__BackingField_30; }
	inline void set_U3CPushButtonU3Ek__BackingField_30(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CPushButtonU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPushButtonU3Ek__BackingField_30), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGripButtonU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CGripButtonU3Ek__BackingField_31)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CGripButtonU3Ek__BackingField_31() const { return ___U3CGripButtonU3Ek__BackingField_31; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CGripButtonU3Ek__BackingField_31() { return &___U3CGripButtonU3Ek__BackingField_31; }
	inline void set_U3CGripButtonU3Ek__BackingField_31(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CGripButtonU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGripButtonU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPauseButtonU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CPauseButtonU3Ek__BackingField_32)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CPauseButtonU3Ek__BackingField_32() const { return ___U3CPauseButtonU3Ek__BackingField_32; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CPauseButtonU3Ek__BackingField_32() { return &___U3CPauseButtonU3Ek__BackingField_32; }
	inline void set_U3CPauseButtonU3Ek__BackingField_32(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CPauseButtonU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPauseButtonU3Ek__BackingField_32), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwitchCharacterButtonU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSwitchCharacterButtonU3Ek__BackingField_33)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSwitchCharacterButtonU3Ek__BackingField_33() const { return ___U3CSwitchCharacterButtonU3Ek__BackingField_33; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSwitchCharacterButtonU3Ek__BackingField_33() { return &___U3CSwitchCharacterButtonU3Ek__BackingField_33; }
	inline void set_U3CSwitchCharacterButtonU3Ek__BackingField_33(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSwitchCharacterButtonU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwitchCharacterButtonU3Ek__BackingField_33), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwitchWeaponButtonU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSwitchWeaponButtonU3Ek__BackingField_34)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSwitchWeaponButtonU3Ek__BackingField_34() const { return ___U3CSwitchWeaponButtonU3Ek__BackingField_34; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSwitchWeaponButtonU3Ek__BackingField_34() { return &___U3CSwitchWeaponButtonU3Ek__BackingField_34; }
	inline void set_U3CSwitchWeaponButtonU3Ek__BackingField_34(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSwitchWeaponButtonU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwitchWeaponButtonU3Ek__BackingField_34), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTimeControlButtonU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CTimeControlButtonU3Ek__BackingField_35)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CTimeControlButtonU3Ek__BackingField_35() const { return ___U3CTimeControlButtonU3Ek__BackingField_35; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CTimeControlButtonU3Ek__BackingField_35() { return &___U3CTimeControlButtonU3Ek__BackingField_35; }
	inline void set_U3CTimeControlButtonU3Ek__BackingField_35(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CTimeControlButtonU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTimeControlButtonU3Ek__BackingField_35), (void*)value);
	}

	inline static int32_t get_offset_of_U3CShootAxisU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CShootAxisU3Ek__BackingField_36)); }
	inline int32_t get_U3CShootAxisU3Ek__BackingField_36() const { return ___U3CShootAxisU3Ek__BackingField_36; }
	inline int32_t* get_address_of_U3CShootAxisU3Ek__BackingField_36() { return &___U3CShootAxisU3Ek__BackingField_36; }
	inline void set_U3CShootAxisU3Ek__BackingField_36(int32_t value)
	{
		___U3CShootAxisU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CSecondaryShootAxisU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSecondaryShootAxisU3Ek__BackingField_37)); }
	inline int32_t get_U3CSecondaryShootAxisU3Ek__BackingField_37() const { return ___U3CSecondaryShootAxisU3Ek__BackingField_37; }
	inline int32_t* get_address_of_U3CSecondaryShootAxisU3Ek__BackingField_37() { return &___U3CSecondaryShootAxisU3Ek__BackingField_37; }
	inline void set_U3CSecondaryShootAxisU3Ek__BackingField_37(int32_t value)
	{
		___U3CSecondaryShootAxisU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_ButtonList_38() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___ButtonList_38)); }
	inline List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B * get_ButtonList_38() const { return ___ButtonList_38; }
	inline List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B ** get_address_of_ButtonList_38() { return &___ButtonList_38; }
	inline void set_ButtonList_38(List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B * value)
	{
		___ButtonList_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonList_38), (void*)value);
	}

	inline static int32_t get_offset_of__primaryMovement_39() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____primaryMovement_39)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__primaryMovement_39() const { return ____primaryMovement_39; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__primaryMovement_39() { return &____primaryMovement_39; }
	inline void set__primaryMovement_39(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____primaryMovement_39 = value;
	}

	inline static int32_t get_offset_of__secondaryMovement_40() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____secondaryMovement_40)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__secondaryMovement_40() const { return ____secondaryMovement_40; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__secondaryMovement_40() { return &____secondaryMovement_40; }
	inline void set__secondaryMovement_40(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____secondaryMovement_40 = value;
	}

	inline static int32_t get_offset_of__axisHorizontal_41() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisHorizontal_41)); }
	inline String_t* get__axisHorizontal_41() const { return ____axisHorizontal_41; }
	inline String_t** get_address_of__axisHorizontal_41() { return &____axisHorizontal_41; }
	inline void set__axisHorizontal_41(String_t* value)
	{
		____axisHorizontal_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisHorizontal_41), (void*)value);
	}

	inline static int32_t get_offset_of__axisVertical_42() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisVertical_42)); }
	inline String_t* get__axisVertical_42() const { return ____axisVertical_42; }
	inline String_t** get_address_of__axisVertical_42() { return &____axisVertical_42; }
	inline void set__axisVertical_42(String_t* value)
	{
		____axisVertical_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisVertical_42), (void*)value);
	}

	inline static int32_t get_offset_of__axisSecondaryHorizontal_43() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisSecondaryHorizontal_43)); }
	inline String_t* get__axisSecondaryHorizontal_43() const { return ____axisSecondaryHorizontal_43; }
	inline String_t** get_address_of__axisSecondaryHorizontal_43() { return &____axisSecondaryHorizontal_43; }
	inline void set__axisSecondaryHorizontal_43(String_t* value)
	{
		____axisSecondaryHorizontal_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisSecondaryHorizontal_43), (void*)value);
	}

	inline static int32_t get_offset_of__axisSecondaryVertical_44() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisSecondaryVertical_44)); }
	inline String_t* get__axisSecondaryVertical_44() const { return ____axisSecondaryVertical_44; }
	inline String_t** get_address_of__axisSecondaryVertical_44() { return &____axisSecondaryVertical_44; }
	inline void set__axisSecondaryVertical_44(String_t* value)
	{
		____axisSecondaryVertical_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisSecondaryVertical_44), (void*)value);
	}

	inline static int32_t get_offset_of__axisShoot_45() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisShoot_45)); }
	inline String_t* get__axisShoot_45() const { return ____axisShoot_45; }
	inline String_t** get_address_of__axisShoot_45() { return &____axisShoot_45; }
	inline void set__axisShoot_45(String_t* value)
	{
		____axisShoot_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisShoot_45), (void*)value);
	}

	inline static int32_t get_offset_of__axisShootSecondary_46() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisShootSecondary_46)); }
	inline String_t* get__axisShootSecondary_46() const { return ____axisShootSecondary_46; }
	inline String_t** get_address_of__axisShootSecondary_46() { return &____axisShootSecondary_46; }
	inline void set__axisShootSecondary_46(String_t* value)
	{
		____axisShootSecondary_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisShootSecondary_46), (void*)value);
	}
};


// MoreMountains.CorgiEngine.LevelManager
struct LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E  : public MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB
{
public:
	// MoreMountains.CorgiEngine.Character[] MoreMountains.CorgiEngine.LevelManager::PlayerPrefabs
	CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* ___PlayerPrefabs_5;
	// System.Boolean MoreMountains.CorgiEngine.LevelManager::AutoAttributePlayerIDs
	bool ___AutoAttributePlayerIDs_6;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character> MoreMountains.CorgiEngine.LevelManager::SceneCharacters
	List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * ___SceneCharacters_7;
	// MoreMountains.CorgiEngine.CheckPoint MoreMountains.CorgiEngine.LevelManager::DebugSpawn
	CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * ___DebugSpawn_8;
	// MoreMountains.CorgiEngine.LevelManager/CheckpointsAxis MoreMountains.CorgiEngine.LevelManager::CheckpointAttributionAxis
	int32_t ___CheckpointAttributionAxis_9;
	// MoreMountains.CorgiEngine.LevelManager/CheckpointDirections MoreMountains.CorgiEngine.LevelManager::CheckpointAttributionDirection
	int32_t ___CheckpointAttributionDirection_10;
	// MoreMountains.CorgiEngine.CheckPoint MoreMountains.CorgiEngine.LevelManager::CurrentCheckPoint
	CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * ___CurrentCheckPoint_11;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointOfEntry> MoreMountains.CorgiEngine.LevelManager::PointsOfEntry
	List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 * ___PointsOfEntry_12;
	// System.Single MoreMountains.CorgiEngine.LevelManager::IntroFadeDuration
	float ___IntroFadeDuration_13;
	// System.Single MoreMountains.CorgiEngine.LevelManager::OutroFadeDuration
	float ___OutroFadeDuration_14;
	// System.Int32 MoreMountains.CorgiEngine.LevelManager::FaderID
	int32_t ___FaderID_15;
	// MoreMountains.Tools.MMTweenType MoreMountains.CorgiEngine.LevelManager::FadeTween
	MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * ___FadeTween_16;
	// System.Single MoreMountains.CorgiEngine.LevelManager::RespawnDelay
	float ___RespawnDelay_17;
	// MoreMountains.CorgiEngine.LevelManager/BoundsModes MoreMountains.CorgiEngine.LevelManager::BoundsMode
	int32_t ___BoundsMode_18;
	// UnityEngine.Bounds MoreMountains.CorgiEngine.LevelManager::LevelBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___LevelBounds_19;
	// System.Boolean MoreMountains.CorgiEngine.LevelManager::ConvertToColliderBoundsButton
	bool ___ConvertToColliderBoundsButton_20;
	// UnityEngine.Collider MoreMountains.CorgiEngine.LevelManager::<BoundsCollider>k__BackingField
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___U3CBoundsColliderU3Ek__BackingField_21;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.LevelManager::<BoundsCollider2D>k__BackingField
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___U3CBoundsCollider2DU3Ek__BackingField_22;
	// MoreMountains.Tools.MMLoadScene/LoadingSceneModes MoreMountains.CorgiEngine.LevelManager::LoadingSceneMode
	int32_t ___LoadingSceneMode_23;
	// System.String MoreMountains.CorgiEngine.LevelManager::LoadingSceneName
	String_t* ___LoadingSceneName_24;
	// MoreMountains.Tools.MMAdditiveSceneLoadingManagerSettings MoreMountains.CorgiEngine.LevelManager::AdditiveLoadingSettings
	MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB * ___AdditiveLoadingSettings_25;
	// MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.LevelManager::<LevelCameraController>k__BackingField
	CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * ___U3CLevelCameraControllerU3Ek__BackingField_26;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character> MoreMountains.CorgiEngine.LevelManager::<Players>k__BackingField
	List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * ___U3CPlayersU3Ek__BackingField_27;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.CheckPoint> MoreMountains.CorgiEngine.LevelManager::<Checkpoints>k__BackingField
	List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 * ___U3CCheckpointsU3Ek__BackingField_28;
	// System.DateTime MoreMountains.CorgiEngine.LevelManager::_started
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ____started_29;
	// System.Int32 MoreMountains.CorgiEngine.LevelManager::_savedPoints
	int32_t ____savedPoints_30;
	// System.String MoreMountains.CorgiEngine.LevelManager::_nextLevel
	String_t* ____nextLevel_31;
	// UnityEngine.BoxCollider MoreMountains.CorgiEngine.LevelManager::_collider
	BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * ____collider_32;
	// UnityEngine.BoxCollider2D MoreMountains.CorgiEngine.LevelManager::_collider2D
	BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * ____collider2D_33;
	// UnityEngine.Bounds MoreMountains.CorgiEngine.LevelManager::_originalBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ____originalBounds_34;

public:
	inline static int32_t get_offset_of_PlayerPrefabs_5() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___PlayerPrefabs_5)); }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* get_PlayerPrefabs_5() const { return ___PlayerPrefabs_5; }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED** get_address_of_PlayerPrefabs_5() { return &___PlayerPrefabs_5; }
	inline void set_PlayerPrefabs_5(CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* value)
	{
		___PlayerPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerPrefabs_5), (void*)value);
	}

	inline static int32_t get_offset_of_AutoAttributePlayerIDs_6() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___AutoAttributePlayerIDs_6)); }
	inline bool get_AutoAttributePlayerIDs_6() const { return ___AutoAttributePlayerIDs_6; }
	inline bool* get_address_of_AutoAttributePlayerIDs_6() { return &___AutoAttributePlayerIDs_6; }
	inline void set_AutoAttributePlayerIDs_6(bool value)
	{
		___AutoAttributePlayerIDs_6 = value;
	}

	inline static int32_t get_offset_of_SceneCharacters_7() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___SceneCharacters_7)); }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * get_SceneCharacters_7() const { return ___SceneCharacters_7; }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E ** get_address_of_SceneCharacters_7() { return &___SceneCharacters_7; }
	inline void set_SceneCharacters_7(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * value)
	{
		___SceneCharacters_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneCharacters_7), (void*)value);
	}

	inline static int32_t get_offset_of_DebugSpawn_8() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___DebugSpawn_8)); }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * get_DebugSpawn_8() const { return ___DebugSpawn_8; }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 ** get_address_of_DebugSpawn_8() { return &___DebugSpawn_8; }
	inline void set_DebugSpawn_8(CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * value)
	{
		___DebugSpawn_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DebugSpawn_8), (void*)value);
	}

	inline static int32_t get_offset_of_CheckpointAttributionAxis_9() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___CheckpointAttributionAxis_9)); }
	inline int32_t get_CheckpointAttributionAxis_9() const { return ___CheckpointAttributionAxis_9; }
	inline int32_t* get_address_of_CheckpointAttributionAxis_9() { return &___CheckpointAttributionAxis_9; }
	inline void set_CheckpointAttributionAxis_9(int32_t value)
	{
		___CheckpointAttributionAxis_9 = value;
	}

	inline static int32_t get_offset_of_CheckpointAttributionDirection_10() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___CheckpointAttributionDirection_10)); }
	inline int32_t get_CheckpointAttributionDirection_10() const { return ___CheckpointAttributionDirection_10; }
	inline int32_t* get_address_of_CheckpointAttributionDirection_10() { return &___CheckpointAttributionDirection_10; }
	inline void set_CheckpointAttributionDirection_10(int32_t value)
	{
		___CheckpointAttributionDirection_10 = value;
	}

	inline static int32_t get_offset_of_CurrentCheckPoint_11() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___CurrentCheckPoint_11)); }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * get_CurrentCheckPoint_11() const { return ___CurrentCheckPoint_11; }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 ** get_address_of_CurrentCheckPoint_11() { return &___CurrentCheckPoint_11; }
	inline void set_CurrentCheckPoint_11(CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * value)
	{
		___CurrentCheckPoint_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentCheckPoint_11), (void*)value);
	}

	inline static int32_t get_offset_of_PointsOfEntry_12() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___PointsOfEntry_12)); }
	inline List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 * get_PointsOfEntry_12() const { return ___PointsOfEntry_12; }
	inline List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 ** get_address_of_PointsOfEntry_12() { return &___PointsOfEntry_12; }
	inline void set_PointsOfEntry_12(List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 * value)
	{
		___PointsOfEntry_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PointsOfEntry_12), (void*)value);
	}

	inline static int32_t get_offset_of_IntroFadeDuration_13() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___IntroFadeDuration_13)); }
	inline float get_IntroFadeDuration_13() const { return ___IntroFadeDuration_13; }
	inline float* get_address_of_IntroFadeDuration_13() { return &___IntroFadeDuration_13; }
	inline void set_IntroFadeDuration_13(float value)
	{
		___IntroFadeDuration_13 = value;
	}

	inline static int32_t get_offset_of_OutroFadeDuration_14() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___OutroFadeDuration_14)); }
	inline float get_OutroFadeDuration_14() const { return ___OutroFadeDuration_14; }
	inline float* get_address_of_OutroFadeDuration_14() { return &___OutroFadeDuration_14; }
	inline void set_OutroFadeDuration_14(float value)
	{
		___OutroFadeDuration_14 = value;
	}

	inline static int32_t get_offset_of_FaderID_15() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___FaderID_15)); }
	inline int32_t get_FaderID_15() const { return ___FaderID_15; }
	inline int32_t* get_address_of_FaderID_15() { return &___FaderID_15; }
	inline void set_FaderID_15(int32_t value)
	{
		___FaderID_15 = value;
	}

	inline static int32_t get_offset_of_FadeTween_16() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___FadeTween_16)); }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * get_FadeTween_16() const { return ___FadeTween_16; }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 ** get_address_of_FadeTween_16() { return &___FadeTween_16; }
	inline void set_FadeTween_16(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * value)
	{
		___FadeTween_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FadeTween_16), (void*)value);
	}

	inline static int32_t get_offset_of_RespawnDelay_17() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___RespawnDelay_17)); }
	inline float get_RespawnDelay_17() const { return ___RespawnDelay_17; }
	inline float* get_address_of_RespawnDelay_17() { return &___RespawnDelay_17; }
	inline void set_RespawnDelay_17(float value)
	{
		___RespawnDelay_17 = value;
	}

	inline static int32_t get_offset_of_BoundsMode_18() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___BoundsMode_18)); }
	inline int32_t get_BoundsMode_18() const { return ___BoundsMode_18; }
	inline int32_t* get_address_of_BoundsMode_18() { return &___BoundsMode_18; }
	inline void set_BoundsMode_18(int32_t value)
	{
		___BoundsMode_18 = value;
	}

	inline static int32_t get_offset_of_LevelBounds_19() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___LevelBounds_19)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_LevelBounds_19() const { return ___LevelBounds_19; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_LevelBounds_19() { return &___LevelBounds_19; }
	inline void set_LevelBounds_19(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___LevelBounds_19 = value;
	}

	inline static int32_t get_offset_of_ConvertToColliderBoundsButton_20() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___ConvertToColliderBoundsButton_20)); }
	inline bool get_ConvertToColliderBoundsButton_20() const { return ___ConvertToColliderBoundsButton_20; }
	inline bool* get_address_of_ConvertToColliderBoundsButton_20() { return &___ConvertToColliderBoundsButton_20; }
	inline void set_ConvertToColliderBoundsButton_20(bool value)
	{
		___ConvertToColliderBoundsButton_20 = value;
	}

	inline static int32_t get_offset_of_U3CBoundsColliderU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CBoundsColliderU3Ek__BackingField_21)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_U3CBoundsColliderU3Ek__BackingField_21() const { return ___U3CBoundsColliderU3Ek__BackingField_21; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_U3CBoundsColliderU3Ek__BackingField_21() { return &___U3CBoundsColliderU3Ek__BackingField_21; }
	inline void set_U3CBoundsColliderU3Ek__BackingField_21(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___U3CBoundsColliderU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBoundsColliderU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBoundsCollider2DU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CBoundsCollider2DU3Ek__BackingField_22)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_U3CBoundsCollider2DU3Ek__BackingField_22() const { return ___U3CBoundsCollider2DU3Ek__BackingField_22; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_U3CBoundsCollider2DU3Ek__BackingField_22() { return &___U3CBoundsCollider2DU3Ek__BackingField_22; }
	inline void set_U3CBoundsCollider2DU3Ek__BackingField_22(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___U3CBoundsCollider2DU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBoundsCollider2DU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_LoadingSceneMode_23() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___LoadingSceneMode_23)); }
	inline int32_t get_LoadingSceneMode_23() const { return ___LoadingSceneMode_23; }
	inline int32_t* get_address_of_LoadingSceneMode_23() { return &___LoadingSceneMode_23; }
	inline void set_LoadingSceneMode_23(int32_t value)
	{
		___LoadingSceneMode_23 = value;
	}

	inline static int32_t get_offset_of_LoadingSceneName_24() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___LoadingSceneName_24)); }
	inline String_t* get_LoadingSceneName_24() const { return ___LoadingSceneName_24; }
	inline String_t** get_address_of_LoadingSceneName_24() { return &___LoadingSceneName_24; }
	inline void set_LoadingSceneName_24(String_t* value)
	{
		___LoadingSceneName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoadingSceneName_24), (void*)value);
	}

	inline static int32_t get_offset_of_AdditiveLoadingSettings_25() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___AdditiveLoadingSettings_25)); }
	inline MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB * get_AdditiveLoadingSettings_25() const { return ___AdditiveLoadingSettings_25; }
	inline MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB ** get_address_of_AdditiveLoadingSettings_25() { return &___AdditiveLoadingSettings_25; }
	inline void set_AdditiveLoadingSettings_25(MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB * value)
	{
		___AdditiveLoadingSettings_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AdditiveLoadingSettings_25), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLevelCameraControllerU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CLevelCameraControllerU3Ek__BackingField_26)); }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * get_U3CLevelCameraControllerU3Ek__BackingField_26() const { return ___U3CLevelCameraControllerU3Ek__BackingField_26; }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 ** get_address_of_U3CLevelCameraControllerU3Ek__BackingField_26() { return &___U3CLevelCameraControllerU3Ek__BackingField_26; }
	inline void set_U3CLevelCameraControllerU3Ek__BackingField_26(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * value)
	{
		___U3CLevelCameraControllerU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLevelCameraControllerU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPlayersU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CPlayersU3Ek__BackingField_27)); }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * get_U3CPlayersU3Ek__BackingField_27() const { return ___U3CPlayersU3Ek__BackingField_27; }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E ** get_address_of_U3CPlayersU3Ek__BackingField_27() { return &___U3CPlayersU3Ek__BackingField_27; }
	inline void set_U3CPlayersU3Ek__BackingField_27(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * value)
	{
		___U3CPlayersU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPlayersU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCheckpointsU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CCheckpointsU3Ek__BackingField_28)); }
	inline List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 * get_U3CCheckpointsU3Ek__BackingField_28() const { return ___U3CCheckpointsU3Ek__BackingField_28; }
	inline List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 ** get_address_of_U3CCheckpointsU3Ek__BackingField_28() { return &___U3CCheckpointsU3Ek__BackingField_28; }
	inline void set_U3CCheckpointsU3Ek__BackingField_28(List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 * value)
	{
		___U3CCheckpointsU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCheckpointsU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of__started_29() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____started_29)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get__started_29() const { return ____started_29; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of__started_29() { return &____started_29; }
	inline void set__started_29(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		____started_29 = value;
	}

	inline static int32_t get_offset_of__savedPoints_30() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____savedPoints_30)); }
	inline int32_t get__savedPoints_30() const { return ____savedPoints_30; }
	inline int32_t* get_address_of__savedPoints_30() { return &____savedPoints_30; }
	inline void set__savedPoints_30(int32_t value)
	{
		____savedPoints_30 = value;
	}

	inline static int32_t get_offset_of__nextLevel_31() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____nextLevel_31)); }
	inline String_t* get__nextLevel_31() const { return ____nextLevel_31; }
	inline String_t** get_address_of__nextLevel_31() { return &____nextLevel_31; }
	inline void set__nextLevel_31(String_t* value)
	{
		____nextLevel_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nextLevel_31), (void*)value);
	}

	inline static int32_t get_offset_of__collider_32() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____collider_32)); }
	inline BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * get__collider_32() const { return ____collider_32; }
	inline BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 ** get_address_of__collider_32() { return &____collider_32; }
	inline void set__collider_32(BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * value)
	{
		____collider_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider_32), (void*)value);
	}

	inline static int32_t get_offset_of__collider2D_33() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____collider2D_33)); }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * get__collider2D_33() const { return ____collider2D_33; }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 ** get_address_of__collider2D_33() { return &____collider2D_33; }
	inline void set__collider2D_33(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * value)
	{
		____collider2D_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider2D_33), (void*)value);
	}

	inline static int32_t get_offset_of__originalBounds_34() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____originalBounds_34)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get__originalBounds_34() const { return ____originalBounds_34; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of__originalBounds_34() { return &____originalBounds_34; }
	inline void set__originalBounds_34(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		____originalBounds_34 = value;
	}
};


// MoreMountains.CorgiEngine.Mushroom
struct Mushroom_t46E47C48B440211E5F9FD59FB3742346EBD3CAC9  : public PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2
{
public:

public:
};


// MoreMountains.CorgiEngine.RetroAdventureProgressManager
struct RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260  : public MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B
{
public:
	// System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::<InitialMaximumLives>k__BackingField
	int32_t ___U3CInitialMaximumLivesU3Ek__BackingField_5;
	// System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::<InitialCurrentLives>k__BackingField
	int32_t ___U3CInitialCurrentLivesU3Ek__BackingField_6;
	// MoreMountains.CorgiEngine.RetroAdventureScene[] MoreMountains.CorgiEngine.RetroAdventureProgressManager::Scenes
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* ___Scenes_7;
	// System.Boolean MoreMountains.CorgiEngine.RetroAdventureProgressManager::CreateSaveGameBtn
	bool ___CreateSaveGameBtn_8;
	// System.Single MoreMountains.CorgiEngine.RetroAdventureProgressManager::<CurrentStars>k__BackingField
	float ___U3CCurrentStarsU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CInitialMaximumLivesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260, ___U3CInitialMaximumLivesU3Ek__BackingField_5)); }
	inline int32_t get_U3CInitialMaximumLivesU3Ek__BackingField_5() const { return ___U3CInitialMaximumLivesU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CInitialMaximumLivesU3Ek__BackingField_5() { return &___U3CInitialMaximumLivesU3Ek__BackingField_5; }
	inline void set_U3CInitialMaximumLivesU3Ek__BackingField_5(int32_t value)
	{
		___U3CInitialMaximumLivesU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CInitialCurrentLivesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260, ___U3CInitialCurrentLivesU3Ek__BackingField_6)); }
	inline int32_t get_U3CInitialCurrentLivesU3Ek__BackingField_6() const { return ___U3CInitialCurrentLivesU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CInitialCurrentLivesU3Ek__BackingField_6() { return &___U3CInitialCurrentLivesU3Ek__BackingField_6; }
	inline void set_U3CInitialCurrentLivesU3Ek__BackingField_6(int32_t value)
	{
		___U3CInitialCurrentLivesU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_Scenes_7() { return static_cast<int32_t>(offsetof(RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260, ___Scenes_7)); }
	inline RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* get_Scenes_7() const { return ___Scenes_7; }
	inline RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D** get_address_of_Scenes_7() { return &___Scenes_7; }
	inline void set_Scenes_7(RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* value)
	{
		___Scenes_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Scenes_7), (void*)value);
	}

	inline static int32_t get_offset_of_CreateSaveGameBtn_8() { return static_cast<int32_t>(offsetof(RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260, ___CreateSaveGameBtn_8)); }
	inline bool get_CreateSaveGameBtn_8() const { return ___CreateSaveGameBtn_8; }
	inline bool* get_address_of_CreateSaveGameBtn_8() { return &___CreateSaveGameBtn_8; }
	inline void set_CreateSaveGameBtn_8(bool value)
	{
		___CreateSaveGameBtn_8 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentStarsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260, ___U3CCurrentStarsU3Ek__BackingField_9)); }
	inline float get_U3CCurrentStarsU3Ek__BackingField_9() const { return ___U3CCurrentStarsU3Ek__BackingField_9; }
	inline float* get_address_of_U3CCurrentStarsU3Ek__BackingField_9() { return &___U3CCurrentStarsU3Ek__BackingField_9; }
	inline void set_U3CCurrentStarsU3Ek__BackingField_9(float value)
	{
		___U3CCurrentStarsU3Ek__BackingField_9 = value;
	}
};


// MoreMountains.CorgiEngine.RetroCopter
struct RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC  : public CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7
{
public:
	// System.Single MoreMountains.CorgiEngine.RetroCopter::MaximumAllowedAngle
	float ___MaximumAllowedAngle_25;
	// System.Single MoreMountains.CorgiEngine.RetroCopter::CharacterRotationSpeed
	float ___CharacterRotationSpeed_26;
	// MoreMountains.CorgiEngine.CharacterFly MoreMountains.CorgiEngine.RetroCopter::_characterFly
	CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * ____characterFly_27;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.RetroCopter::_model
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____model_28;
	// System.Single MoreMountains.CorgiEngine.RetroCopter::_currentAngle
	float ____currentAngle_29;
	// UnityEngine.Quaternion MoreMountains.CorgiEngine.RetroCopter::_newRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ____newRotation_30;

public:
	inline static int32_t get_offset_of_MaximumAllowedAngle_25() { return static_cast<int32_t>(offsetof(RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC, ___MaximumAllowedAngle_25)); }
	inline float get_MaximumAllowedAngle_25() const { return ___MaximumAllowedAngle_25; }
	inline float* get_address_of_MaximumAllowedAngle_25() { return &___MaximumAllowedAngle_25; }
	inline void set_MaximumAllowedAngle_25(float value)
	{
		___MaximumAllowedAngle_25 = value;
	}

	inline static int32_t get_offset_of_CharacterRotationSpeed_26() { return static_cast<int32_t>(offsetof(RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC, ___CharacterRotationSpeed_26)); }
	inline float get_CharacterRotationSpeed_26() const { return ___CharacterRotationSpeed_26; }
	inline float* get_address_of_CharacterRotationSpeed_26() { return &___CharacterRotationSpeed_26; }
	inline void set_CharacterRotationSpeed_26(float value)
	{
		___CharacterRotationSpeed_26 = value;
	}

	inline static int32_t get_offset_of__characterFly_27() { return static_cast<int32_t>(offsetof(RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC, ____characterFly_27)); }
	inline CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * get__characterFly_27() const { return ____characterFly_27; }
	inline CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 ** get_address_of__characterFly_27() { return &____characterFly_27; }
	inline void set__characterFly_27(CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * value)
	{
		____characterFly_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterFly_27), (void*)value);
	}

	inline static int32_t get_offset_of__model_28() { return static_cast<int32_t>(offsetof(RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC, ____model_28)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__model_28() const { return ____model_28; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__model_28() { return &____model_28; }
	inline void set__model_28(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____model_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____model_28), (void*)value);
	}

	inline static int32_t get_offset_of__currentAngle_29() { return static_cast<int32_t>(offsetof(RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC, ____currentAngle_29)); }
	inline float get__currentAngle_29() const { return ____currentAngle_29; }
	inline float* get_address_of__currentAngle_29() { return &____currentAngle_29; }
	inline void set__currentAngle_29(float value)
	{
		____currentAngle_29 = value;
	}

	inline static int32_t get_offset_of__newRotation_30() { return static_cast<int32_t>(offsetof(RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC, ____newRotation_30)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get__newRotation_30() const { return ____newRotation_30; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of__newRotation_30() { return &____newRotation_30; }
	inline void set__newRotation_30(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		____newRotation_30 = value;
	}
};


// MoreMountains.CorgiEngine.Star
struct Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401  : public PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Star::StarID
	int32_t ___StarID_16;

public:
	inline static int32_t get_offset_of_StarID_16() { return static_cast<int32_t>(offsetof(Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401, ___StarID_16)); }
	inline int32_t get_StarID_16() const { return ___StarID_16; }
	inline int32_t* get_address_of_StarID_16() { return &___StarID_16; }
	inline void set_StarID_16(int32_t value)
	{
		___StarID_16 = value;
	}
};


// MoreMountains.CorgiEngine.SuperHipsterBrosHealth
struct SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92  : public Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51
{
public:
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.SuperHipsterBrosHealth::_initialScale
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____initialScale_48;

public:
	inline static int32_t get_offset_of__initialScale_48() { return static_cast<int32_t>(offsetof(SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92, ____initialScale_48)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__initialScale_48() const { return ____initialScale_48; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__initialScale_48() { return &____initialScale_48; }
	inline void set__initialScale_48(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____initialScale_48 = value;
	}
};


// MoreMountains.CorgiEngine.FinishLevel
struct FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7  : public ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5
{
public:
	// System.String MoreMountains.CorgiEngine.FinishLevel::LevelName
	String_t* ___LevelName_50;
	// System.Single MoreMountains.CorgiEngine.FinishLevel::DelayBeforeTransition
	float ___DelayBeforeTransition_51;
	// System.Boolean MoreMountains.CorgiEngine.FinishLevel::TriggerFade
	bool ___TriggerFade_52;
	// System.Int32 MoreMountains.CorgiEngine.FinishLevel::FaderID
	int32_t ___FaderID_53;
	// MoreMountains.Tools.MMTweenType MoreMountains.CorgiEngine.FinishLevel::FadeTween
	MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * ___FadeTween_54;
	// System.Boolean MoreMountains.CorgiEngine.FinishLevel::FreezeTime
	bool ___FreezeTime_55;
	// System.Boolean MoreMountains.CorgiEngine.FinishLevel::FreezeCharacter
	bool ___FreezeCharacter_56;
	// UnityEngine.WaitForSeconds MoreMountains.CorgiEngine.FinishLevel::_delayWaitForSeconds
	WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * ____delayWaitForSeconds_57;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.FinishLevel::_character
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ____character_58;

public:
	inline static int32_t get_offset_of_LevelName_50() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___LevelName_50)); }
	inline String_t* get_LevelName_50() const { return ___LevelName_50; }
	inline String_t** get_address_of_LevelName_50() { return &___LevelName_50; }
	inline void set_LevelName_50(String_t* value)
	{
		___LevelName_50 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelName_50), (void*)value);
	}

	inline static int32_t get_offset_of_DelayBeforeTransition_51() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___DelayBeforeTransition_51)); }
	inline float get_DelayBeforeTransition_51() const { return ___DelayBeforeTransition_51; }
	inline float* get_address_of_DelayBeforeTransition_51() { return &___DelayBeforeTransition_51; }
	inline void set_DelayBeforeTransition_51(float value)
	{
		___DelayBeforeTransition_51 = value;
	}

	inline static int32_t get_offset_of_TriggerFade_52() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___TriggerFade_52)); }
	inline bool get_TriggerFade_52() const { return ___TriggerFade_52; }
	inline bool* get_address_of_TriggerFade_52() { return &___TriggerFade_52; }
	inline void set_TriggerFade_52(bool value)
	{
		___TriggerFade_52 = value;
	}

	inline static int32_t get_offset_of_FaderID_53() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___FaderID_53)); }
	inline int32_t get_FaderID_53() const { return ___FaderID_53; }
	inline int32_t* get_address_of_FaderID_53() { return &___FaderID_53; }
	inline void set_FaderID_53(int32_t value)
	{
		___FaderID_53 = value;
	}

	inline static int32_t get_offset_of_FadeTween_54() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___FadeTween_54)); }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * get_FadeTween_54() const { return ___FadeTween_54; }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 ** get_address_of_FadeTween_54() { return &___FadeTween_54; }
	inline void set_FadeTween_54(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * value)
	{
		___FadeTween_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FadeTween_54), (void*)value);
	}

	inline static int32_t get_offset_of_FreezeTime_55() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___FreezeTime_55)); }
	inline bool get_FreezeTime_55() const { return ___FreezeTime_55; }
	inline bool* get_address_of_FreezeTime_55() { return &___FreezeTime_55; }
	inline void set_FreezeTime_55(bool value)
	{
		___FreezeTime_55 = value;
	}

	inline static int32_t get_offset_of_FreezeCharacter_56() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ___FreezeCharacter_56)); }
	inline bool get_FreezeCharacter_56() const { return ___FreezeCharacter_56; }
	inline bool* get_address_of_FreezeCharacter_56() { return &___FreezeCharacter_56; }
	inline void set_FreezeCharacter_56(bool value)
	{
		___FreezeCharacter_56 = value;
	}

	inline static int32_t get_offset_of__delayWaitForSeconds_57() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ____delayWaitForSeconds_57)); }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * get__delayWaitForSeconds_57() const { return ____delayWaitForSeconds_57; }
	inline WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 ** get_address_of__delayWaitForSeconds_57() { return &____delayWaitForSeconds_57; }
	inline void set__delayWaitForSeconds_57(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * value)
	{
		____delayWaitForSeconds_57 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____delayWaitForSeconds_57), (void*)value);
	}

	inline static int32_t get_offset_of__character_58() { return static_cast<int32_t>(offsetof(FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7, ____character_58)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get__character_58() const { return ____character_58; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of__character_58() { return &____character_58; }
	inline void set__character_58(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		____character_58 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____character_58), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// MoreMountains.CorgiEngine.RetroAdventureGUIManager
struct RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8  : public GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5
{
public:
	// UnityEngine.UI.Text MoreMountains.CorgiEngine.RetroAdventureGUIManager::StarDisplayText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___StarDisplayText_20;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.RetroAdventureGUIManager::LevelCompleteSplash
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___LevelCompleteSplash_21;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.RetroAdventureGUIManager::LevelCompleteSplashFocus
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___LevelCompleteSplashFocus_22;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.RetroAdventureGUIManager::Inventories
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Inventories_23;
	// UnityEngine.UI.Image[] MoreMountains.CorgiEngine.RetroAdventureGUIManager::Stars
	ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* ___Stars_24;
	// UnityEngine.Color MoreMountains.CorgiEngine.RetroAdventureGUIManager::StarOnColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___StarOnColor_25;
	// UnityEngine.Color MoreMountains.CorgiEngine.RetroAdventureGUIManager::StarOffColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___StarOffColor_26;

public:
	inline static int32_t get_offset_of_StarDisplayText_20() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___StarDisplayText_20)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_StarDisplayText_20() const { return ___StarDisplayText_20; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_StarDisplayText_20() { return &___StarDisplayText_20; }
	inline void set_StarDisplayText_20(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___StarDisplayText_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StarDisplayText_20), (void*)value);
	}

	inline static int32_t get_offset_of_LevelCompleteSplash_21() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___LevelCompleteSplash_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_LevelCompleteSplash_21() const { return ___LevelCompleteSplash_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_LevelCompleteSplash_21() { return &___LevelCompleteSplash_21; }
	inline void set_LevelCompleteSplash_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___LevelCompleteSplash_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelCompleteSplash_21), (void*)value);
	}

	inline static int32_t get_offset_of_LevelCompleteSplashFocus_22() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___LevelCompleteSplashFocus_22)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_LevelCompleteSplashFocus_22() const { return ___LevelCompleteSplashFocus_22; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_LevelCompleteSplashFocus_22() { return &___LevelCompleteSplashFocus_22; }
	inline void set_LevelCompleteSplashFocus_22(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___LevelCompleteSplashFocus_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LevelCompleteSplashFocus_22), (void*)value);
	}

	inline static int32_t get_offset_of_Inventories_23() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___Inventories_23)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Inventories_23() const { return ___Inventories_23; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Inventories_23() { return &___Inventories_23; }
	inline void set_Inventories_23(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Inventories_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Inventories_23), (void*)value);
	}

	inline static int32_t get_offset_of_Stars_24() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___Stars_24)); }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* get_Stars_24() const { return ___Stars_24; }
	inline ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224** get_address_of_Stars_24() { return &___Stars_24; }
	inline void set_Stars_24(ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* value)
	{
		___Stars_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Stars_24), (void*)value);
	}

	inline static int32_t get_offset_of_StarOnColor_25() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___StarOnColor_25)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_StarOnColor_25() const { return ___StarOnColor_25; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_StarOnColor_25() { return &___StarOnColor_25; }
	inline void set_StarOnColor_25(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___StarOnColor_25 = value;
	}

	inline static int32_t get_offset_of_StarOffColor_26() { return static_cast<int32_t>(offsetof(RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8, ___StarOffColor_26)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_StarOffColor_26() const { return ___StarOffColor_26; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_StarOffColor_26() { return &___StarOffColor_26; }
	inline void set_StarOffColor_26(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___StarOffColor_26 = value;
	}
};


// MoreMountains.CorgiEngine.RetroStar
struct RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B  : public Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401
{
public:

public:
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};


// MoreMountains.CorgiEngine.RetroAdventureFinishLevel
struct RetroAdventureFinishLevel_t7238A7A8B78449B08A460E4C97F8BC356915E477  : public FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7
{
public:

public:
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// MoreMountains.CorgiEngine.RetroAdventureScene[]
struct RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * m_Items[1];

public:
	inline RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.UI.Image[]
struct ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * m_Items[1];

public:
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Boolean[]
struct BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) bool m_Items[1];

public:
	inline bool GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline bool* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, bool value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline bool GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline bool* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, bool value)
	{
		m_Items[index] = value;
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMPersistentSingleton`1<System.Object>::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MMPersistentSingleton_1_get_Instance_m194FA72623166888CABF4EC94EA2B4654BF73A4A_gshared (const RuntimeMethod* method);
// System.Boolean MoreMountains.Tools.MMSingleton`1<System.Object>::get_HasInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MMSingleton_1_get_HasInstance_mB78637D86379B68203F768178DBCECD3F9BC0184_gshared (const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMSingleton`1<System.Object>::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared (const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMStateMachine`1<System.Int32Enum>::get_CurrentState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t MMStateMachine_1_get_CurrentState_mF8651D0B592EB2D243E9374DADBFD68A0D0C3C69_gshared_inline (MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<System.Object>::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSingleton_1_Awake_mEB353F0FE1AE4048D7B6C466737EE678ED8475F7_gshared (MMSingleton_1_tDFC31364B2B8D70B8F6DBDF995CE7F469D1A68EB * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.CorgiEngineStarEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.CorgiEngineStarEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSingleton_1__ctor_m3A954A61821E2E1FB2A7C0F581642256050B2794_gshared (MMSingleton_1_tDFC31364B2B8D70B8F6DBDF995CE7F469D1A68EB * __this, const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentNoAlloc<System.Object>(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObjectExtensions_MMGetComponentNoAlloc_TisRuntimeObject_m4490C39D71D5B192FDC4A00FBCA384ADF9C5F70D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method);

// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::RotateTowards(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_RotateTowards_mE536473CFE4F8C16245C57C467B95882BE8CF5DC (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___from0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___to1, float ___maxDegreesDelta2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Quaternion_op_Equality_m7EC909C253064DBECF7DB83BCF7C2E42163685BE (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___lhs0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rhs1, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * Component_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mD7909FB3C98B8C554A48934864C1F8E9065C36BA (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8 (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMPersistentSingleton`1<MoreMountains.CorgiEngine.GameManager>::get_Instance()
inline GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB (const RuntimeMethod* method)
{
	return ((  GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * (*) (const RuntimeMethod*))MMPersistentSingleton_1_get_Instance_m194FA72623166888CABF4EC94EA2B4654BF73A4A_gshared)(method);
}
// System.Boolean MoreMountains.CorgiEngine.GameManager::get_StoredLevelMapPosition()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_StoredLevelMapPosition_m08B6869E7E9E4A0290804DE50C16976CC0E26F4E_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 MoreMountains.CorgiEngine.GameManager::get_LevelMapPosition()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GameManager_get_LevelMapPosition_m28E4FCF400D62585F90B3D8D311510C4EF76BBF1_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Boolean MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.InputManager>::get_HasInstance()
inline bool MMSingleton_1_get_HasInstance_m44412714D79EE1735A3E92160755AC8EEC80B0FB (const RuntimeMethod* method)
{
	return ((  bool (*) (const RuntimeMethod*))MMSingleton_1_get_HasInstance_mB78637D86379B68203F768178DBCECD3F9BC0184_gshared)(method);
}
// !0 MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.InputManager>::get_Instance()
inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169 (const RuntimeMethod* method)
{
	return ((  InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * (*) (const RuntimeMethod*))MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared)(method);
}
// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::get_PrimaryMovement()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputManager_get_PrimaryMovement_m67D3A20043621ACB7B8436051BB09487FFAC5DFD_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// System.Boolean MoreMountains.CorgiEngine.LevelMapCharacter::get_CollidingWithAPathElement()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_JumpButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates> MoreMountains.Tools.MMInput/IMButton::get_State()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline (IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * __this, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::get_CurrentState()
inline int32_t MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_inline (MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE *, const RuntimeMethod*))MMStateMachine_1_get_CurrentState_mF8651D0B592EB2D243E9374DADBFD68A0D0C3C69_gshared_inline)(__this, method);
}
// System.Boolean System.String::IsNullOrWhiteSpace(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrWhiteSpace_m4C4266AE4097F5B61FF72A8D0D083611231B9CA6 (String_t* ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_MoveTowards_mFB45EE30324E487925CA26EE6C001F0A3D257796 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<MoreMountains.CorgiEngine.LevelSelector>()
inline LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316 * Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !0 MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.GUIManager>::get_Instance()
inline GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461 (const RuntimeMethod* method)
{
	return ((  GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * (*) (const RuntimeMethod*))MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared)(method);
}
// !!0 UnityEngine.Component::GetComponent<MoreMountains.CorgiEngine.LevelSelectorGUI>()
inline LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void MoreMountains.CorgiEngine.GameManager::set_StoredLevelMapPosition(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_StoredLevelMapPosition_m5B58AFCCA29FDD58077A16E59D1A38246853AA67_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.GameManager::set_LevelMapPosition(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_LevelMapPosition_m6F6B297ED7D9522CAD10244448B7F59C0587B1AC_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * __this, bool ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<MoreMountains.CorgiEngine.LevelMapCharacter>()
inline LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::set_CollidingWithAPathElement(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, bool ___value0, const RuntimeMethod* method);
// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::get_LastVisitedPathElement()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::set_LastVisitedPathElement(MoreMountains.CorgiEngine.LevelMapPathElement)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___value0, const RuntimeMethod* method);
// System.String System.String::ToUpper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToUpper_m4BC629F8059C3E0C4E3F7C7E04DB50EBB0C1A05A (String_t* __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_blue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___from0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___to1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<MoreMountains.CorgiEngine.Character>()
inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * Component_GetComponent_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m034D1618B31D56CFF4409A30E06845ABCB15C539 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<MoreMountains.CorgiEngine.SuperHipsterBrosHealth>()
inline SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void MoreMountains.CorgiEngine.PickableItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PickableItem__ctor_m28A81D4FBEFCE32B10E6DDA43A87624CA4CE02A1 (PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineEvent::Trigger(MoreMountains.CorgiEngine.CorgiEngineEventTypes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineEvent_Trigger_mB2F63DDFFCA55CD90EA578E552FBC063E561F633 (int32_t ___eventType0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMGameEvent::Trigger(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGameEvent_Trigger_m6E62092C8F6B0FC44688EA24BA7CED9B73D6BF24 (String_t* ___newName0, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.LevelManager>::get_Instance()
inline LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B (const RuntimeMethod* method)
{
	return ((  LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * (*) (const RuntimeMethod*))MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared)(method);
}
// System.Void MoreMountains.CorgiEngine.FinishLevel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FinishLevel__ctor_m01928DBC9628666ADFA3760A58298DC85566498F (FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7 * __this, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.RetroAdventureProgressManager>::get_Instance()
inline RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373 (const RuntimeMethod* method)
{
	return ((  RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * (*) (const RuntimeMethod*))MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared)(method);
}
// System.Single MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_CurrentStars()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method);
// System.String System.Single::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010 (float* __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.EventSystems.EventSystem UnityEngine.EventSystems.EventSystem::get_current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * EventSystem_get_current_m4B9C11F490297AE55428038DACD240596D6CE5F2 (const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.EventSystem::set_sendNavigationEvents(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void EventSystem_set_sendNavigationEvents_mC4AF68C06C2A8E1017142D7C9C9AA29018F56F96_inline (EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (const RuntimeMethod* method);
// System.String UnityEngine.SceneManagement.Scene::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8 (Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE * __this, const RuntimeMethod* method);
// System.Void UnityEngine.EventSystems.EventSystem::SetSelectedGameObject(UnityEngine.GameObject,UnityEngine.EventSystems.BaseEventData)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventSystem_SetSelectedGameObject_m7F0F2E78C18FD468E8B5083AFDA6E9D9364D3D5F (EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___selected0, BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E * ___pointer1, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.GUIManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIManager_OnEnable_mA6F3F63E914526C542DF29CD4373F1F0D01BC96B (GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_gshared)(___caller0, method);
}
// System.Void MoreMountains.CorgiEngine.GUIManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIManager_OnDisable_mB9C57ECD69FFDDE22F22081A9566F88E87E98356 (GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_gshared)(___caller0, method);
}
// System.Void MoreMountains.CorgiEngine.GUIManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GUIManager__ctor_m6FF23ADF78E962A688E7A29256CB50F232F9E84B (GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSceneLoadingManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSceneLoadingManager_LoadScene_m5C529AAC4340DE7EDFFCEA2914D9D4F87BE9BBC7 (String_t* ___sceneToLoad0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.RetroAdventureProgressManager>::Awake()
inline void MMSingleton_1_Awake_m9F53860BDC9DBAE7D1DC680D2A20F93D99BB2719 (MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B * __this, const RuntimeMethod* method)
{
	((  void (*) (MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B *, const RuntimeMethod*))MMSingleton_1_Awake_mEB353F0FE1AE4048D7B6C466737EE678ED8475F7_gshared)(__this, method);
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_CurrentStars(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, float ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.Progress::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Progress__ctor_m328101047C95644D26CFCC3D5B11384D08734D4F (Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * __this, const RuntimeMethod* method);
// System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_InitialMaximumLives()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method);
// System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_InitialCurrentLives()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSaveLoadManager::Save(System.Object,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSaveLoadManager_Save_m95F6B8019AF2B7F28C45ADCF5CA0590C03173BED (RuntimeObject * ___saveObject0, String_t* ___fileName1, String_t* ___foldername2, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Object MoreMountains.Tools.MMSaveLoadManager::Load(System.Type,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MMSaveLoadManager_Load_m507199BE6138E42AFF712DAEB3744BA9CEAC4B4A (Type_t * ___objectType0, String_t* ___fileName1, String_t* ___foldername2, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_InitialMaximumLives(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_InitialCurrentLives(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSaveLoadManager::DeleteSaveFolder(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSaveLoadManager_DeleteSaveFolder_mBD53EECEC7ED5EAD3AB6A01871C1B7C33D7243B1 (String_t* ___folderName0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.CorgiEngineStarEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.CorgiEngineStarEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.RetroAdventureProgressManager>::.ctor()
inline void MMSingleton_1__ctor_m3CC6785DFF822FDD50FB05361D7B6C10F39D84EE (MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B * __this, const RuntimeMethod* method)
{
	((  void (*) (MMSingleton_1_t83657692C3E15C3724715A40B19012EDAA76A19B *, const RuntimeMethod*))MMSingleton_1__ctor_m3A954A61821E2E1FB2A7C0F581642256050B2794_gshared)(__this, method);
}
// System.Void MoreMountains.CorgiEngine.CharacterAbility::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterAbility_Initialization_m490E4A987E5686DAE37ADCEA55ADC474E3F91BDC (CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7 * __this, const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentNoAlloc<MoreMountains.CorgiEngine.CharacterFly>(UnityEngine.GameObject)
inline CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4_m90CFBE75068B01DBC3E4D3AA19772E13C30F7717 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method)
{
	return ((  CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObjectExtensions_MMGetComponentNoAlloc_TisRuntimeObject_m4490C39D71D5B192FDC4A00FBCA384ADF9C5F70D_gshared)(___this0, method);
}
// System.Void MoreMountains.CorgiEngine.CharacterAbility::ProcessAbility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterAbility_ProcessAbility_m665947A1A59C3538FAD48919DE6A17CE62A28D6D (CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_Speed()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline (CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * __this, const RuntimeMethod* method);
// System.Single MoreMountains.Tools.MMMaths::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMMaths_Remap_mBEAF91CCEABC9ACD132FC4FF072A8A463B2F6C60 (float ___x0, float ___A1, float ___B2, float ___C3, float ___D4, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___euler0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Lerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Lerp_mBFA4C4D2574C8140AA840273D3E6565D66F6F261 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CharacterAbility::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterAbility__ctor_m99A9BC4D760B415E996417AB4658940BF317D8A7 (CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33 (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0 (ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * __this, const RuntimeMethod* method);
// UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color0, const RuntimeMethod* method);
// System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42 (MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B * __this, MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  ___value0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1 (const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.PickableItem::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PickableItem_Start_mA869FA4809079960DFE30E24B97B084A52AB9F32 (PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.Star::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Star__ctor_m778DAC91D16195F08F69B41C82CD752E3F82078B (Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.Health::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Health_Initialization_m5FA1B03AFC34508F445DE3E0EC3A8C57353B7237 (Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color32__ctor_m9D07EC69256BB7ED2784E543848DE7B8484A5C94 (Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color32_op_Implicit_m63F14F1A14B1A9A3EE4D154413EE229D3E001623 (Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___c0, const RuntimeMethod* method);
// System.Collections.IEnumerator MoreMountains.Tools.MMImage::Flicker(UnityEngine.Renderer,UnityEngine.Color,UnityEngine.Color,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MMImage_Flicker_m7CD500764E575C625AA2CE9CFC7CE73C04614FAD (Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___renderer0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___initialColor1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___flickerColor2, float ___flickerSpeed3, float ___flickerDuration4, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Collider2D>()
inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * Component_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_mB8CB45C5289A0ACF38DAD7B4727F32E4E93DFC30 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.Health::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Health__ctor_mC4AD594519EE02992BED5B75AC1ADDB3CC0F25AB (Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * __this, const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentNoAlloc<MoreMountains.CorgiEngine.CharacterGravity>(UnityEngine.GameObject)
inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4_m1BA5F82EFAF229EA35C6C8F09D91BBAEF9EF53C2 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method)
{
	return ((  CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObjectExtensions_MMGetComponentNoAlloc_TisRuntimeObject_m4490C39D71D5B192FDC4A00FBCA384ADF9C5F70D_gshared)(___this0, method);
}
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.BackgroundTree::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackgroundTree_Start_m898A0B450B7B456E5A74AA54E44E951C4C35E076 (BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56 * __this, const RuntimeMethod* method)
{
	{
		// _scaleTarget = WiggleScale( );
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = VirtFuncInvoker0< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(6 /* UnityEngine.Vector3 MoreMountains.CorgiEngine.BackgroundTree::WiggleScale() */, __this);
		__this->set__scaleTarget_8(L_0);
		// _rotationTarget = WiggleRotate();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_1;
		L_1 = VirtFuncInvoker0< Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  >::Invoke(7 /* UnityEngine.Quaternion MoreMountains.CorgiEngine.BackgroundTree::WiggleRotate() */, __this);
		__this->set__rotationTarget_9(L_1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.BackgroundTree::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackgroundTree_Update_mB290C176CD97B64E083B3EEFCBDB6D5DE96F14DE (BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	{
		// _accumulator += Time.deltaTime;
		float L_0 = __this->get__accumulator_10();
		float L_1;
		L_1 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set__accumulator_10(((float)il2cpp_codegen_add((float)L_0, (float)L_1)));
		// if(_accumulator >= scaleSpeed)
		float L_2 = __this->get__accumulator_10();
		float L_3 = __this->get_scaleSpeed_4();
		if ((!(((float)L_2) >= ((float)L_3))))
		{
			goto IL_003f;
		}
	}
	{
		// _scaleTarget = WiggleScale();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = VirtFuncInvoker0< Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(6 /* UnityEngine.Vector3 MoreMountains.CorgiEngine.BackgroundTree::WiggleScale() */, __this);
		__this->set__scaleTarget_8(L_4);
		// _accumulator -= scaleSpeed;
		float L_5 = __this->get__accumulator_10();
		float L_6 = __this->get_scaleSpeed_4();
		__this->set__accumulator_10(((float)il2cpp_codegen_subtract((float)L_5, (float)L_6)));
	}

IL_003f:
	{
		// float norm = Time.deltaTime/scaleSpeed;
		float L_7;
		L_7 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_8 = __this->get_scaleSpeed_4();
		V_0 = ((float)((float)L_7/(float)L_8));
		// Vector3 newLocalScale=Vector3.Lerp(transform.localScale, _scaleTarget, norm);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9;
		L_9 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = __this->get__scaleTarget_8();
		float L_12 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline(L_10, L_11, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		// transform.localScale = newLocalScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = V_1;
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_14, L_15, /*hidden argument*/NULL);
		// float normRotation = Time.deltaTime*rotationSpeed;
		float L_16;
		L_16 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_17 = __this->get_rotationSpeed_6();
		V_2 = ((float)il2cpp_codegen_multiply((float)L_16, (float)L_17));
		// transform.rotation = Quaternion.RotateTowards( transform.rotation, _rotationTarget , normRotation );
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_20;
		L_20 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_19, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_21 = __this->get__rotationTarget_9();
		float L_22 = V_2;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23;
		L_23 = Quaternion_RotateTowards_mE536473CFE4F8C16245C57C467B95882BE8CF5DC(L_20, L_21, L_22, /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_18, L_23, /*hidden argument*/NULL);
		// if(transform.rotation == _rotationTarget)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_25;
		L_25 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_24, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_26 = __this->get__rotationTarget_9();
		bool L_27;
		L_27 = Quaternion_op_Equality_m7EC909C253064DBECF7DB83BCF7C2E42163685BE(L_25, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00c3;
		}
	}
	{
		// _rotationTarget = WiggleRotate();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_28;
		L_28 = VirtFuncInvoker0< Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  >::Invoke(7 /* UnityEngine.Quaternion MoreMountains.CorgiEngine.BackgroundTree::WiggleRotate() */, __this);
		__this->set__rotationTarget_9(L_28);
	}

IL_00c3:
	{
		// }
		return;
	}
}
// UnityEngine.Vector3 MoreMountains.CorgiEngine.BackgroundTree::WiggleScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  BackgroundTree_WiggleScale_m88A20BAE284AA98537CADE6E2C2D4504B6ACA87A (BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56 * __this, const RuntimeMethod* method)
{
	{
		// return new Vector3((1 + Random.Range(-scaleDistance,scaleDistance)),(1 + Random.Range(-scaleDistance,scaleDistance)),1);
		float L_0 = __this->get_scaleDistance_5();
		float L_1 = __this->get_scaleDistance_5();
		float L_2;
		L_2 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_0)), L_1, /*hidden argument*/NULL);
		float L_3 = __this->get_scaleDistance_5();
		float L_4 = __this->get_scaleDistance_5();
		float L_5;
		L_5 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_3)), L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((float)il2cpp_codegen_add((float)(1.0f), (float)L_2)), ((float)il2cpp_codegen_add((float)(1.0f), (float)L_5)), (1.0f), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Quaternion MoreMountains.CorgiEngine.BackgroundTree::WiggleRotate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  BackgroundTree_WiggleRotate_m1D0E88C06C72CDE0809A6DA6B3704CF1CF9355C8 (BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56 * __this, const RuntimeMethod* method)
{
	{
		// return Quaternion.Euler(0f, 0f, Random.Range(-rotationAmplitude,rotationAmplitude));
		float L_0 = __this->get_rotationAmplitude_7();
		float L_1 = __this->get_rotationAmplitude_7();
		float L_2;
		L_2 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(((-L_0)), L_1, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_3;
		L_3 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), (0.0f), L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Void MoreMountains.CorgiEngine.BackgroundTree::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BackgroundTree__ctor_m2BD03FF226E6859560729C172C9C4EB9B289A884 (BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56 * __this, const RuntimeMethod* method)
{
	{
		// public float scaleSpeed = 0.5f;
		__this->set_scaleSpeed_4((0.5f));
		// public float scaleDistance = 0.01f;
		__this->set_scaleDistance_5((0.00999999978f));
		// public float rotationSpeed = 1f;
		__this->set_rotationSpeed_6((1.0f));
		// public float rotationAmplitude = 3f;
		__this->set_rotationAmplitude_7((3.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.CorgiEngine.LevelMapCharacter::get_CollidingWithAPathElement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// public bool CollidingWithAPathElement { get; set; }
		bool L_0 = __this->get_U3CCollidingWithAPathElementU3Ek__BackingField_7();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::set_CollidingWithAPathElement(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool CollidingWithAPathElement { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CCollidingWithAPathElementU3Ek__BackingField_7(L_0);
		return;
	}
}
// MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::get_LastVisitedPathElement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// public LevelMapPathElement LastVisitedPathElement { get; set; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_U3CLastVisitedPathElementU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::set_LastVisitedPathElement(MoreMountains.CorgiEngine.LevelMapPathElement)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___value0, const RuntimeMethod* method)
{
	{
		// public LevelMapPathElement LastVisitedPathElement { get; set; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = ___value0;
		__this->set_U3CLastVisitedPathElementU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_Start_m926EC44D3202BF153F681E4048311DDB1A5E8CFA (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mD7909FB3C98B8C554A48934864C1F8E9065C36BA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// _rigidbody2D = GetComponent<Rigidbody2D>();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_0;
		L_0 = Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var);
		__this->set__rigidbody2D_20(L_0);
		// _boxCollider2D = GetComponent<BoxCollider2D>();
		BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * L_1;
		L_1 = Component_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mD7909FB3C98B8C554A48934864C1F8E9065C36BA(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_mD7909FB3C98B8C554A48934864C1F8E9065C36BA_RuntimeMethod_var);
		__this->set__boxCollider2D_21(L_1);
		// _animator = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2;
		L_2 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set__animator_22(L_2);
		// _offset = _boxCollider2D.bounds.center - transform.position;
		BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * L_3 = __this->get__boxCollider2D_21();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_4;
		L_4 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_5, L_7, /*hidden argument*/NULL);
		__this->set__offset_15(L_8);
		// if (GameManager.Instance.StoredLevelMapPosition)
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_9;
		L_9 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		bool L_10;
		L_10 = GameManager_get_StoredLevelMapPosition_m08B6869E7E9E4A0290804DE50C16976CC0E26F4E_inline(L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0074;
		}
	}
	{
		// transform.position=GameManager.Instance.LevelMapPosition;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_12;
		L_12 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_13;
		L_13 = GameManager_get_LevelMapPosition_m28E4FCF400D62585F90B3D8D311510C4EF76BBF1_inline(L_12, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_13, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_11, L_14, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0074:
	{
		// transform.position=StartingPoint.transform.position-_offset;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_16 = __this->get_StartingPoint_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = __this->get__offset_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_18, L_19, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_15, L_20, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_Update_mE43EF1D69903EFBF6571A30CD2E0A9AD96EC5067 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// InputMovement();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::InputMovement() */, __this);
		// MoveCharacter();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::MoveCharacter() */, __this);
		// AnimateCharacter();
		VirtActionInvoker0::Invoke(8 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::AnimateCharacter() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::InputMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_InputMovement_m7683BFCCD876AF2572300C9C89FEF114441D0E27 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_HasInstance_m44412714D79EE1735A3E92160755AC8EEC80B0FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16A70DC8C790D0EFD53AE03FF23CB99D8B7A53CA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral455BE6C51A15F36C8D913F896775D15888AC8673);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D354CA1036DDA6F701F800C5C1B3A4235D2EDD7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAE24C5BE9B741FFFA87D2A951BFE7EA0440461CD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (InputManager.HasInstance)
		bool L_0;
		L_0 = MMSingleton_1_get_HasInstance_m44412714D79EE1735A3E92160755AC8EEC80B0FB(/*hidden argument*/MMSingleton_1_get_HasInstance_m44412714D79EE1735A3E92160755AC8EEC80B0FB_RuntimeMethod_var);
		if (!L_0)
		{
			goto IL_0031;
		}
	}
	{
		// _horizontalMove = InputManager.Instance.PrimaryMovement.x;
		InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * L_1;
		L_1 = MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169(/*hidden argument*/MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		L_2 = InputManager_get_PrimaryMovement_m67D3A20043621ACB7B8436051BB09487FFAC5DFD_inline(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_0();
		__this->set__horizontalMove_12(L_3);
		// _verticalMove = InputManager.Instance.PrimaryMovement.y;
		InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * L_4;
		L_4 = MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169(/*hidden argument*/MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169_RuntimeMethod_var);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = InputManager_get_PrimaryMovement_m67D3A20043621ACB7B8436051BB09487FFAC5DFD_inline(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_y_1();
		__this->set__verticalMove_13(L_6);
	}

IL_0031:
	{
		// if (!CollidingWithAPathElement)
		bool L_7;
		L_7 = LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2_inline(__this, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003a;
		}
	}
	{
		// return;
		return;
	}

IL_003a:
	{
		// if (InputManager.Instance.JumpButton.State.CurrentState == MMInput.ButtonStates.ButtonDown)
		InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * L_8;
		L_8 = MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169(/*hidden argument*/MMSingleton_1_get_Instance_mAD646776F39AA7098D575CB667C77D4D8F77A169_RuntimeMethod_var);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_9;
		L_9 = InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline(L_8, /*hidden argument*/NULL);
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_10;
		L_10 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_9, /*hidden argument*/NULL);
		int32_t L_11;
		L_11 = MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_inline(L_10, /*hidden argument*/MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var);
		if ((!(((uint32_t)L_11) == ((uint32_t)1))))
		{
			goto IL_0057;
		}
	}
	{
		// ButtonPressed();
		VirtActionInvoker0::Invoke(12 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::ButtonPressed() */, __this);
	}

IL_0057:
	{
		// _movement = "";
		__this->set__movement_18(_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		// if ( (Mathf.Abs(_horizontalMove)>_threshold) || (Mathf.Abs(_verticalMove)>_threshold) )
		float L_12 = __this->get__horizontalMove_12();
		float L_13;
		L_13 = fabsf(L_12);
		float L_14 = __this->get__threshold_14();
		if ((((float)L_13) > ((float)L_14)))
		{
			goto IL_0088;
		}
	}
	{
		float L_15 = __this->get__verticalMove_13();
		float L_16;
		L_16 = fabsf(L_15);
		float L_17 = __this->get__threshold_14();
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_00ee;
		}
	}

IL_0088:
	{
		// if (_horizontalMove>_threshold)
		float L_18 = __this->get__horizontalMove_12();
		float L_19 = __this->get__threshold_14();
		if ((!(((float)L_18) > ((float)L_19))))
		{
			goto IL_00a1;
		}
	}
	{
		// _movement="Right";
		__this->set__movement_18(_stringLiteral16A70DC8C790D0EFD53AE03FF23CB99D8B7A53CA);
	}

IL_00a1:
	{
		// if (_horizontalMove<-_threshold)
		float L_20 = __this->get__horizontalMove_12();
		float L_21 = __this->get__threshold_14();
		if ((!(((float)L_20) < ((float)((-L_21))))))
		{
			goto IL_00bb;
		}
	}
	{
		// _movement="Left";
		__this->set__movement_18(_stringLiteralAE24C5BE9B741FFFA87D2A951BFE7EA0440461CD);
	}

IL_00bb:
	{
		// if (_verticalMove>_threshold)
		float L_22 = __this->get__verticalMove_13();
		float L_23 = __this->get__threshold_14();
		if ((!(((float)L_22) > ((float)L_23))))
		{
			goto IL_00d4;
		}
	}
	{
		// _movement="Up";
		__this->set__movement_18(_stringLiteral9D354CA1036DDA6F701F800C5C1B3A4235D2EDD7);
	}

IL_00d4:
	{
		// if (_verticalMove<-_threshold)
		float L_24 = __this->get__verticalMove_13();
		float L_25 = __this->get__threshold_14();
		if ((!(((float)L_24) < ((float)((-L_25))))))
		{
			goto IL_00ee;
		}
	}
	{
		// _movement="Down";
		__this->set__movement_18(_stringLiteral455BE6C51A15F36C8D913F896775D15888AC8673);
	}

IL_00ee:
	{
		// if (String.IsNullOrWhiteSpace(_movement)){return;}
		String_t* L_26 = __this->get__movement_18();
		bool L_27;
		L_27 = String_IsNullOrWhiteSpace_m4C4266AE4097F5B61FF72A8D0D083611231B9CA6(L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00fc;
		}
	}
	{
		// if (String.IsNullOrWhiteSpace(_movement)){return;}
		return;
	}

IL_00fc:
	{
		// if (_currentPathElement.AutomaticMovement) { return; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_28 = __this->get__currentPathElement_19();
		bool L_29 = L_28->get_AutomaticMovement_4();
		if (!L_29)
		{
			goto IL_010a;
		}
	}
	{
		// if (_currentPathElement.AutomaticMovement) { return; }
		return;
	}

IL_010a:
	{
		// if ( (_movement=="Up") && (_currentPathElement.CanGoUp()) )
		String_t* L_30 = __this->get__movement_18();
		bool L_31;
		L_31 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_30, _stringLiteral9D354CA1036DDA6F701F800C5C1B3A4235D2EDD7, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0141;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_32 = __this->get__currentPathElement_19();
		bool L_33;
		L_33 = VirtFuncInvoker0< bool >::Invoke(4 /* System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoUp() */, L_32);
		if (!L_33)
		{
			goto IL_0141;
		}
	}
	{
		// _destination=_currentPathElement.Up; _shouldMove=true;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_34 = __this->get__currentPathElement_19();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_35 = L_34->get_Up_5();
		__this->set__destination_10(L_35);
		// _destination=_currentPathElement.Up; _shouldMove=true;
		__this->set__shouldMove_11((bool)1);
	}

IL_0141:
	{
		// if ( (_movement=="Right") && (_currentPathElement.CanGoRight()) )
		String_t* L_36 = __this->get__movement_18();
		bool L_37;
		L_37 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_36, _stringLiteral16A70DC8C790D0EFD53AE03FF23CB99D8B7A53CA, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0178;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_38 = __this->get__currentPathElement_19();
		bool L_39;
		L_39 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoRight() */, L_38);
		if (!L_39)
		{
			goto IL_0178;
		}
	}
	{
		// _destination=_currentPathElement.Right; _shouldMove=true;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_40 = __this->get__currentPathElement_19();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_41 = L_40->get_Right_6();
		__this->set__destination_10(L_41);
		// _destination=_currentPathElement.Right; _shouldMove=true;
		__this->set__shouldMove_11((bool)1);
	}

IL_0178:
	{
		// if ( (_movement=="Down") && (_currentPathElement.CanGoDown()) )
		String_t* L_42 = __this->get__movement_18();
		bool L_43;
		L_43 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_42, _stringLiteral455BE6C51A15F36C8D913F896775D15888AC8673, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_01af;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_44 = __this->get__currentPathElement_19();
		bool L_45;
		L_45 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoDown() */, L_44);
		if (!L_45)
		{
			goto IL_01af;
		}
	}
	{
		// _destination=_currentPathElement.Down; _shouldMove=true;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_46 = __this->get__currentPathElement_19();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_47 = L_46->get_Down_7();
		__this->set__destination_10(L_47);
		// _destination=_currentPathElement.Down; _shouldMove=true;
		__this->set__shouldMove_11((bool)1);
	}

IL_01af:
	{
		// if ( (_movement=="Left") && (_currentPathElement.CanGoLeft()) )
		String_t* L_48 = __this->get__movement_18();
		bool L_49;
		L_49 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_48, _stringLiteralAE24C5BE9B741FFFA87D2A951BFE7EA0440461CD, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_01e6;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_50 = __this->get__currentPathElement_19();
		bool L_51;
		L_51 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoLeft() */, L_50);
		if (!L_51)
		{
			goto IL_01e6;
		}
	}
	{
		// _destination=_currentPathElement.Left; _shouldMove=true;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_52 = __this->get__currentPathElement_19();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_53 = L_52->get_Left_8();
		__this->set__destination_10(L_53);
		// _destination=_currentPathElement.Left; _shouldMove=true;
		__this->set__shouldMove_11((bool)1);
	}

IL_01e6:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::MoveCharacter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_MoveCharacter_mA687E9B0FCA8DD8302D9C093E7F9A893C479B8B7 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// if (!_shouldMove) { return; }
		bool L_0 = __this->get__shouldMove_11();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!_shouldMove) { return; }
		return;
	}

IL_0009:
	{
		// transform.position = Vector3.MoveTowards(transform.position,_destination.transform.position-_offset,Time.deltaTime*CharacterSpeed);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_4 = __this->get__destination_10();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = __this->get__offset_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_6, L_7, /*hidden argument*/NULL);
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_10 = __this->get_CharacterSpeed_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_MoveTowards_mFB45EE30324E487925CA26EE6C001F0A3D257796(L_3, L_8, ((float)il2cpp_codegen_multiply((float)L_9, (float)L_10)), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_1, L_11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::AnimateCharacter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_AnimateCharacter_mAFBB1072005CB90FA0EF66C66D3716D0DB547648 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_destination==null)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get__destination_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		// {    return; }
		return;
	}

IL_000f:
	{
		// if ( _destination.transform.position.x -_offset.x <  transform.position.x)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_2 = __this->get__destination_10();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_6 = __this->get_address_of__offset_15();
		float L_7 = L_6->get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_x_2();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7))) < ((float)L_10))))
		{
			goto IL_0050;
		}
	}
	{
		// if (CharacterIsFacingRight)
		bool L_11 = __this->get_CharacterIsFacingRight_8();
		if (!L_11)
		{
			goto IL_0050;
		}
	}
	{
		// Flip();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Flip() */, __this);
	}

IL_0050:
	{
		// if ( _destination.transform.position.x -_offset.x >  transform.position.x)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_12 = __this->get__destination_10();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_12, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_13, /*hidden argument*/NULL);
		float L_15 = L_14.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_16 = __this->get_address_of__offset_15();
		float L_17 = L_16->get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_x_2();
		if ((!(((float)((float)il2cpp_codegen_subtract((float)L_15, (float)L_17))) > ((float)L_20))))
		{
			goto IL_0091;
		}
	}
	{
		// if (!CharacterIsFacingRight)
		bool L_21 = __this->get_CharacterIsFacingRight_8();
		if (L_21)
		{
			goto IL_0091;
		}
	}
	{
		// Flip();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Flip() */, __this);
	}

IL_0091:
	{
		// if (_animator!=null)
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_22 = __this->get__animator_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_23;
		L_23 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_22, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_23)
		{
			goto IL_00e5;
		}
	}
	{
		// if (_positionLastFrame!=transform.position)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = __this->get__positionLastFrame_16();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_25;
		L_25 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26;
		L_26 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_25, /*hidden argument*/NULL);
		bool L_27;
		L_27 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_24, L_26, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00c4;
		}
	}
	{
		// _currentSpeed=1f;
		__this->set__currentSpeed_17((1.0f));
		// }
		goto IL_00cf;
	}

IL_00c4:
	{
		// _currentSpeed=0f;
		__this->set__currentSpeed_17((0.0f));
	}

IL_00cf:
	{
		// _animator.SetFloat("Speed", _currentSpeed);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_28 = __this->get__animator_22();
		float L_29 = __this->get__currentSpeed_17();
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_28, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_29, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		// _positionLastFrame=transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_30;
		L_30 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		L_31 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_30, /*hidden argument*/NULL);
		__this->set__positionLastFrame_16(L_31);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetCurrentPathElement(MoreMountains.CorgiEngine.LevelMapPathElement)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_SetCurrentPathElement_mB60CFD5D0371B7DCF20D67BB8894545A8EB4991A (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___pathElement0, const RuntimeMethod* method)
{
	{
		// _currentPathElement=pathElement;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = ___pathElement0;
		__this->set__currentPathElement_19(L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetDestination(MoreMountains.CorgiEngine.LevelMapPathElement)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_SetDestination_m9DA1462F26BB41CD9617379754985D880A36B85C (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___newDestination0, const RuntimeMethod* method)
{
	{
		// if (transform.position != _destination.transform.position-_offset)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_2 = __this->get__destination_10();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = __this->get__offset_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_4, L_5, /*hidden argument*/NULL);
		bool L_7;
		L_7 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_1, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002e;
		}
	}
	{
		// return;
		return;
	}

IL_002e:
	{
		// _destination=newDestination;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_8 = ___newDestination0;
		__this->set__destination_10(L_8);
		// _shouldMove=true;
		__this->set__shouldMove_11((bool)1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_Flip_m73C6581590C0D1FA2EDA378C1F0E832660A40ACB (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// transform.localScale = new Vector3(-transform.localScale.x,transform.localScale.y,transform.localScale.z);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_y_3();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_10), ((-L_3)), L_6, L_9, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_0, L_10, /*hidden argument*/NULL);
		// CharacterIsFacingRight = transform.localScale.x > 0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_x_2();
		__this->set_CharacterIsFacingRight_8((bool)((((float)L_13) > ((float)(0.0f)))? 1 : 0));
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::ButtonPressed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_ButtonPressed_m3A3752479794487EB647F9928AFC09110FDBA758 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_currentPathElement.GetComponent<LevelSelector>()!=null)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get__currentPathElement_19();
		LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316 * L_1;
		L_1 = Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8(L_0, /*hidden argument*/Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0069;
		}
	}
	{
		// if (GUIManager.Instance.GetComponent<LevelSelectorGUI>() != null)
		GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * L_3;
		L_3 = MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461(/*hidden argument*/MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * L_4;
		L_4 = Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772(L_3, /*hidden argument*/Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0034;
		}
	}
	{
		// GUIManager.Instance.GetComponent<LevelSelectorGUI>().TurnOffLevelName();
		GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * L_6;
		L_6 = MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461(/*hidden argument*/MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * L_7;
		L_7 = Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772(L_6, /*hidden argument*/Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::TurnOffLevelName() */, L_7);
	}

IL_0034:
	{
		// GameManager.Instance.StoredLevelMapPosition=true;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_8;
		L_8 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		GameManager_set_StoredLevelMapPosition_m5B58AFCCA29FDD58077A16E59D1A38246853AA67_inline(L_8, (bool)1, /*hidden argument*/NULL);
		// GameManager.Instance.LevelMapPosition=transform.position;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_9;
		L_9 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12;
		L_12 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_11, /*hidden argument*/NULL);
		GameManager_set_LevelMapPosition_m6F6B297ED7D9522CAD10244448B7F59C0587B1AC_inline(L_9, L_12, /*hidden argument*/NULL);
		// _currentPathElement.GetComponent<LevelSelector>().GoToLevel();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_13 = __this->get__currentPathElement_19();
		LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316 * L_14;
		L_14 = Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8(L_13, /*hidden argument*/Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(4 /* System.Void MoreMountains.CorgiEngine.LevelSelector::GoToLevel() */, L_14);
	}

IL_0069:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetHorizontalMove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_SetHorizontalMove_mFE3466F76D6D632C24780058459422258EC9E312 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// _horizontalMove = value;
		float L_0 = ___value0;
		__this->set__horizontalMove_12(L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetVerticalMove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter_SetVerticalMove_m3CF33D93147CA1639C7C8E6356D44D664DC0CE20 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// _verticalMove = value;
		float L_0 = ___value0;
		__this->set__verticalMove_13(L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapCharacter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapCharacter__ctor_m53FE1564EB41AA1CE960C7571CBFEDA9E14B6735 (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral623CBDCC47C6DB996D3C60509009A4E557A3E1EF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string PlayerID = "Player1";
		__this->set_PlayerID_4(_stringLiteral623CBDCC47C6DB996D3C60509009A4E557A3E1EF);
		// public bool CharacterIsFacingRight=true;
		__this->set_CharacterIsFacingRight_8((bool)1);
		// protected float _threshold=0.1f;
		__this->set__threshold_14((0.100000001f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LevelMapPathElement_CanGoUp_m1ADE0670DC5696D0CF682AF6358F448A8D230C56 (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Up!=null) { return true; } else { return false; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_Up_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Up!=null) { return true; } else { return false; }
		return (bool)1;
	}

IL_0010:
	{
		// if (Up!=null) { return true; } else { return false; }
		return (bool)0;
	}
}
// System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoRight()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LevelMapPathElement_CanGoRight_mE141BCE2495ED21BB77B1D1A6E89BBAB0C5A6407 (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Right!=null) { return true; } else { return false; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_Right_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Right!=null) { return true; } else { return false; }
		return (bool)1;
	}

IL_0010:
	{
		// if (Right!=null) { return true; } else { return false; }
		return (bool)0;
	}
}
// System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoDown()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LevelMapPathElement_CanGoDown_m918CEF6BA0991A242D98CCDC94451BD54DEF1099 (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Down!=null) { return true; } else { return false; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_Down_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Down!=null) { return true; } else { return false; }
		return (bool)1;
	}

IL_0010:
	{
		// if (Down!=null) { return true; } else { return false; }
		return (bool)0;
	}
}
// System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoLeft()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LevelMapPathElement_CanGoLeft_m3FB80BE75E0C5A5AF592B72D749976C08338C6C7 (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Left!=null) { return true; } else { return false; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_Left_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		// if (Left!=null) { return true; } else { return false; }
		return (bool)1;
	}

IL_0010:
	{
		// if (Left!=null) { return true; } else { return false; }
		return (bool)0;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapPathElement::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapPathElement_Start_m8ABEC4640D4F01094DFDC31EEB2E85366D8B4BEF (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GetComponent<SpriteRenderer>().enabled=false;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0;
		L_0 = Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF_m7224DFA0D29BF2205FEA6E432D9BCB4133F6E491_RuntimeMethod_var);
		Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapPathElement_OnTriggerStay2D_m6F7EA546A8DFB866A95F32C3E335F41CFA185C0D (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * V_0 = NULL;
	{
		// LevelMapCharacter mapCharacter=collider.GetComponent<LevelMapCharacter>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_1;
		L_1 = Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247(L_0, /*hidden argument*/Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var);
		V_0 = L_1;
		// if (mapCharacter==null)
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		return;
	}

IL_0011:
	{
		// mapCharacter.CollidingWithAPathElement=true;
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_4 = V_0;
		LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899_inline(L_4, (bool)1, /*hidden argument*/NULL);
		// mapCharacter.SetCurrentPathElement(this);
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_5 = V_0;
		VirtActionInvoker1< LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * >::Invoke(9 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetCurrentPathElement(MoreMountains.CorgiEngine.LevelMapPathElement) */, L_5, __this);
		// if (AutomaticMovement)
		bool L_6 = __this->get_AutomaticMovement_4();
		if (!L_6)
		{
			goto IL_00de;
		}
	}
	{
		// if (mapCharacter.LastVisitedPathElement!=Up && Up!=null) { mapCharacter.SetDestination(Up); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_7 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_8;
		L_8 = LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583_inline(L_7, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_9 = __this->get_Up_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0057;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_11 = __this->get_Up_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_12;
		L_12 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_11, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0057;
		}
	}
	{
		// if (mapCharacter.LastVisitedPathElement!=Up && Up!=null) { mapCharacter.SetDestination(Up); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_13 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_14 = __this->get_Up_5();
		VirtActionInvoker1< LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetDestination(MoreMountains.CorgiEngine.LevelMapPathElement) */, L_13, L_14);
	}

IL_0057:
	{
		// if (mapCharacter.LastVisitedPathElement!=Right && Right!=null) { mapCharacter.SetDestination(Right); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_15 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_16;
		L_16 = LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583_inline(L_15, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_17 = __this->get_Right_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_18;
		L_18 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_0084;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_19 = __this->get_Right_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_20;
		L_20 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_19, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0084;
		}
	}
	{
		// if (mapCharacter.LastVisitedPathElement!=Right && Right!=null) { mapCharacter.SetDestination(Right); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_21 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_22 = __this->get_Right_6();
		VirtActionInvoker1< LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetDestination(MoreMountains.CorgiEngine.LevelMapPathElement) */, L_21, L_22);
	}

IL_0084:
	{
		// if (mapCharacter.LastVisitedPathElement!=Down && Down!=null) { mapCharacter.SetDestination(Down); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_23 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_24;
		L_24 = LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583_inline(L_23, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_25 = __this->get_Down_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_26;
		L_26 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_24, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00b1;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_27 = __this->get_Down_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_28;
		L_28 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_27, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00b1;
		}
	}
	{
		// if (mapCharacter.LastVisitedPathElement!=Down && Down!=null) { mapCharacter.SetDestination(Down); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_29 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_30 = __this->get_Down_7();
		VirtActionInvoker1< LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetDestination(MoreMountains.CorgiEngine.LevelMapPathElement) */, L_29, L_30);
	}

IL_00b1:
	{
		// if (mapCharacter.LastVisitedPathElement!=Left && Left!=null) { mapCharacter.SetDestination(Left); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_31 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_32;
		L_32 = LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583_inline(L_31, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_33 = __this->get_Left_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_34;
		L_34 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_32, L_33, /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_00de;
		}
	}
	{
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_35 = __this->get_Left_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_36;
		L_36 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_35, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_00de;
		}
	}
	{
		// if (mapCharacter.LastVisitedPathElement!=Left && Left!=null) { mapCharacter.SetDestination(Left); }
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_37 = V_0;
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_38 = __this->get_Left_8();
		VirtActionInvoker1< LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetDestination(MoreMountains.CorgiEngine.LevelMapPathElement) */, L_37, L_38);
	}

IL_00de:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnTriggerExit2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapPathElement_OnTriggerExit2D_mDF3BB11711CD5EC59BD7450356027ABD8E0D8DF9 (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * V_0 = NULL;
	{
		// LevelMapCharacter mapCharacter=collider.GetComponent<LevelMapCharacter>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_1;
		L_1 = Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247(L_0, /*hidden argument*/Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var);
		V_0 = L_1;
		// if (mapCharacter==null)
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		return;
	}

IL_0011:
	{
		// GUIManager.Instance.GetComponent<LevelSelectorGUI>().TurnOffLevelName();
		GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * L_4;
		L_4 = MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461(/*hidden argument*/MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * L_5;
		L_5 = Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772(L_4, /*hidden argument*/Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::TurnOffLevelName() */, L_5);
		// mapCharacter.CollidingWithAPathElement=false;
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_6 = V_0;
		LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899_inline(L_6, (bool)0, /*hidden argument*/NULL);
		// mapCharacter.SetCurrentPathElement(null);
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_7 = V_0;
		VirtActionInvoker1< LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * >::Invoke(9 /* System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetCurrentPathElement(MoreMountains.CorgiEngine.LevelMapPathElement) */, L_7, (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 *)NULL);
		// mapCharacter.LastVisitedPathElement=this;
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_8 = V_0;
		LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2_inline(L_8, __this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapPathElement_OnTriggerEnter2D_m4B2DA6ED322FF90AC270B46FA890442A5BD8DCFB (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LevelMapCharacter mapCharacter=collider.GetComponent<LevelMapCharacter>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * L_1;
		L_1 = Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247(L_0, /*hidden argument*/Component_GetComponent_TisLevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_m4EC123D671B5235A9CAB9F2116B773D1D9D8E247_RuntimeMethod_var);
		// if (mapCharacter==null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// return;
		return;
	}

IL_000f:
	{
		// if (GetComponent<LevelSelector>() != null)
		LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316 * L_3;
		L_3 = Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8(__this, /*hidden argument*/Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004e;
		}
	}
	{
		// if (GUIManager.Instance.GetComponent<LevelSelectorGUI>() != null)
		GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * L_5;
		L_5 = MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461(/*hidden argument*/MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * L_6;
		L_6 = Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772(L_5, /*hidden argument*/Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_004e;
		}
	}
	{
		// GUIManager.Instance.GetComponent<LevelSelectorGUI>().SetLevelName(GetComponent<LevelSelector>().LevelName.ToUpper());
		GUIManager_t93D6F01994618020A658B1BAB837BC847828A7B5 * L_8;
		L_8 = MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461(/*hidden argument*/MMSingleton_1_get_Instance_m3BF255442298A120E3DF85523FCF8E3448712461_RuntimeMethod_var);
		LevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE * L_9;
		L_9 = Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772(L_8, /*hidden argument*/Component_GetComponent_TisLevelSelectorGUI_t5FA9AE502E020F2F0AB5E2A724A10C5A1284D5AE_m6A18F46239CD427FA177993E73BFF8C4E99CF772_RuntimeMethod_var);
		LevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316 * L_10;
		L_10 = Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8(__this, /*hidden argument*/Component_GetComponent_TisLevelSelector_t7EF22E95EFB5428F140D51FDD60DDB6322F0F316_mF695649EA341534022868E85232BA10A2D4B13D8_RuntimeMethod_var);
		String_t* L_11 = L_10->get_LevelName_4();
		String_t* L_12;
		L_12 = String_ToUpper_m4BC629F8059C3E0C4E3F7C7E04DB50EBB0C1A05A(L_11, /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(5 /* System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::SetLevelName(System.String) */, L_9, L_12);
	}

IL_004e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapPathElement_OnDrawGizmos_m170BE035B712DF066E8C2EB5936A3CA6998BA673 (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Up!=null)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_Up_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0057;
		}
	}
	{
		// if (Up.Down==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_2 = __this->get_Up_5();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_3 = L_2->get_Down_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, __this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		// if (Up.Down==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_5;
		L_5 = Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_5, /*hidden argument*/NULL);
		// if (Up.Down==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		goto IL_0037;
	}

IL_002d:
	{
		// if (Up.Down==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_6;
		L_6 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_6, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// Gizmos.DrawLine(transform.position,Up.transform.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_9 = __this->get_Up_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0057:
	{
		// if (Right!=null)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_12 = __this->get_Right_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_13;
		L_13 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_12, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_00ae;
		}
	}
	{
		// if (Right.Left==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_14 = __this->get_Right_6();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_15 = L_14->get_Left_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_16;
		L_16 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_15, __this, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0084;
		}
	}
	{
		// if (Right.Left==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_17;
		L_17 = Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_17, /*hidden argument*/NULL);
		// if (Right.Left==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		goto IL_008e;
	}

IL_0084:
	{
		// if (Right.Left==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_18;
		L_18 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_18, /*hidden argument*/NULL);
	}

IL_008e:
	{
		// Gizmos.DrawLine(transform.position,Right.transform.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_19, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_21 = __this->get_Right_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_21, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_22, /*hidden argument*/NULL);
		Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823(L_20, L_23, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		// if (Down!=null)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_24 = __this->get_Down_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_24, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0105;
		}
	}
	{
		// if (Down.Up==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_26 = __this->get_Down_7();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_27 = L_26->get_Up_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_28;
		L_28 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_27, __this, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_00db;
		}
	}
	{
		// if (Down.Up==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_29;
		L_29 = Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_29, /*hidden argument*/NULL);
		// if (Down.Up==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		goto IL_00e5;
	}

IL_00db:
	{
		// if (Down.Up==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_30;
		L_30 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_30, /*hidden argument*/NULL);
	}

IL_00e5:
	{
		// Gizmos.DrawLine(transform.position,Down.transform.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31;
		L_31 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_33 = __this->get_Down_7();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34;
		L_34 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_33, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
		Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823(L_32, L_35, /*hidden argument*/NULL);
	}

IL_0105:
	{
		// if (Left!=null)
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_36 = __this->get_Left_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_37;
		L_37 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_36, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_015c;
		}
	}
	{
		// if (Left.Right==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_38 = __this->get_Left_8();
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_39 = L_38->get_Right_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_40;
		L_40 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_39, __this, /*hidden argument*/NULL);
		if (!L_40)
		{
			goto IL_0132;
		}
	}
	{
		// if (Left.Right==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_41;
		L_41 = Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_41, /*hidden argument*/NULL);
		// if (Left.Right==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		goto IL_013c;
	}

IL_0132:
	{
		// if (Left.Right==this){Gizmos.color = Color.blue;} else {Gizmos.color = Color.red;}
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_42;
		L_42 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F(L_42, /*hidden argument*/NULL);
	}

IL_013c:
	{
		// Gizmos.DrawLine(transform.position,Left.transform.position);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_43;
		L_43 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_44;
		L_44 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_43, /*hidden argument*/NULL);
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_45 = __this->get_Left_8();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_46;
		L_46 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_45, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47;
		L_47 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_46, /*hidden argument*/NULL);
		Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823(L_44, L_47, /*hidden argument*/NULL);
	}

IL_015c:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.LevelMapPathElement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelMapPathElement__ctor_mAE02AF04C3FB0204EF09814117DA5D05789DF8BA (LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.CorgiEngine.Mushroom::CheckIfPickable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Mushroom_CheckIfPickable_m4B5C49A62BD2D4D937959ADABF99C1ADA0C021E2 (Mushroom_t46E47C48B440211E5F9FD59FB3742346EBD3CAC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m034D1618B31D56CFF4409A30E06845ABCB15C539_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _character = _pickingCollider.GetComponent<Character>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ((PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 *)__this)->get__pickingCollider_9();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_1;
		L_1 = Component_GetComponent_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m034D1618B31D56CFF4409A30E06845ABCB15C539(L_0, /*hidden argument*/Component_GetComponent_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m034D1618B31D56CFF4409A30E06845ABCB15C539_RuntimeMethod_var);
		((PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 *)__this)->set__character_11(L_1);
		// if ((_character == null) || (_pickingCollider.GetComponent<SuperHipsterBrosHealth>() == null))
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_2 = ((PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 *)__this)->get__character_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0032;
		}
	}
	{
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_4 = ((PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 *)__this)->get__pickingCollider_9();
		SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * L_5;
		L_5 = Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC(L_4, /*hidden argument*/Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0034;
		}
	}

IL_0032:
	{
		// return false;
		return (bool)0;
	}

IL_0034:
	{
		// if (_character.CharacterType != Character.CharacterTypes.Player)
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_7 = ((PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 *)__this)->get__character_11();
		int32_t L_8 = L_7->get_CharacterType_4();
		if (!L_8)
		{
			goto IL_0043;
		}
	}
	{
		// return false;
		return (bool)0;
	}

IL_0043:
	{
		// return true;
		return (bool)1;
	}
}
// System.Void MoreMountains.CorgiEngine.Mushroom::Pick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mushroom_Pick_m00F6FA0FF0A52045F46377126EE885E7B9AA9279 (Mushroom_t46E47C48B440211E5F9FD59FB3742346EBD3CAC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _pickingCollider.GetComponent<SuperHipsterBrosHealth>().Grow(2f);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ((PickableItem_t484D7AA90CD6F7AD1B7DAE02CA4F9B53E349B3C2 *)__this)->get__pickingCollider_9();
		SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * L_1;
		L_1 = Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC(L_0, /*hidden argument*/Component_GetComponent_TisSuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92_mCAAF7CBB79DEBB8740B01EE7C6DCDB2E55CE38CC_RuntimeMethod_var);
		VirtActionInvoker1< float >::Invoke(25 /* System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Grow(System.Single) */, L_1, (2.0f));
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Mushroom::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Mushroom__ctor_m41FF2F1C0AACCC9AE564060D7F1820D7357054F6 (Mushroom_t46E47C48B440211E5F9FD59FB3742346EBD3CAC9 * __this, const RuntimeMethod* method)
{
	{
		PickableItem__ctor_m28A81D4FBEFCE32B10E6DDA43A87624CA4CE02A1(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.Progress::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Progress__ctor_m328101047C95644D26CFCC3D5B11384D08734D4F (Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroAdventureFinishLevel::GoToNextLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureFinishLevel_GoToNextLevel_m4E372B402D42CB81BF5C2AC233802060A5A8F6A5 (RetroAdventureFinishLevel_t7238A7A8B78449B08A460E4C97F8BC356915E477 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE863626383155D02291456632E72C0FBEC22C3C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CorgiEngineEvent.Trigger(CorgiEngineEventTypes.LevelComplete);
		CorgiEngineEvent_Trigger_mB2F63DDFFCA55CD90EA578E552FBC063E561F633(2, /*hidden argument*/NULL);
		// MMGameEvent.Trigger("Save");
		MMGameEvent_Trigger_m6E62092C8F6B0FC44688EA24BA7CED9B73D6BF24(_stringLiteralCE863626383155D02291456632E72C0FBEC22C3C, /*hidden argument*/NULL);
		// LevelManager.Instance.SetNextLevel (LevelName);
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_0;
		L_0 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		String_t* L_1 = ((FinishLevel_t59E193898379869F4799BDE98B3ECE0CB60EF0A7 *)__this)->get_LevelName_50();
		VirtActionInvoker1< String_t* >::Invoke(14 /* System.Void MoreMountains.CorgiEngine.LevelManager::SetNextLevel(System.String) */, L_0, L_1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureFinishLevel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureFinishLevel__ctor_mE00FE3EDFD027B723B602F9DBE5C8E5408D2E61B (RetroAdventureFinishLevel_t7238A7A8B78449B08A460E4C97F8BC356915E477 * __this, const RuntimeMethod* method)
{
	{
		FinishLevel__ctor_m01928DBC9628666ADFA3760A58298DC85566498F(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager_Update_m535B6E047596ED9365DE4C0807B54585FD094781 (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, const RuntimeMethod* method)
{
	{
		// UpdateStars ();
		VirtActionInvoker0::Invoke(26 /* System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::UpdateStars() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::UpdateStars()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager_UpdateStars_mCCFD3A83212DF75B13A96F08F42E0C760E08960B (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// StarDisplayText.text = RetroAdventureProgressManager.Instance.CurrentStars.ToString();
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_0 = __this->get_StarDisplayText_20();
		RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * L_1;
		L_1 = MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373(/*hidden argument*/MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		float L_2;
		L_2 = RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC_inline(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3;
		L_3 = Single_ToString_m80E7ABED4F4D73F2BE19DDB80D3D92FCD8DFA010((float*)(&V_0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_3);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::LevelComplete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager_LevelComplete_m33DFE5D7582F17210D62C4C8313D55EF0769A255 (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* V_0 = NULL;
	int32_t V_1 = 0;
	RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * V_2 = NULL;
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B7_0 = NULL;
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B6_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B8_0;
	memset((&G_B8_0), 0, sizeof(G_B8_0));
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B8_1 = NULL;
	{
		// if (Inventories != null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_Inventories_23();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// Inventories.SetActive (false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_Inventories_23();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// EventSystem.current.sendNavigationEvents=true;
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_il2cpp_TypeInfo_var);
		EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * L_3;
		L_3 = EventSystem_get_current_m4B9C11F490297AE55428038DACD240596D6CE5F2(/*hidden argument*/NULL);
		EventSystem_set_sendNavigationEvents_mC4AF68C06C2A8E1017142D7C9C9AA29018F56F96_inline(L_3, (bool)1, /*hidden argument*/NULL);
		// GameManager.Instance.Pause (PauseMethods.NoPauseMenu);
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_4;
		L_4 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		VirtActionInvoker1< int32_t >::Invoke(17 /* System.Void MoreMountains.CorgiEngine.GameManager::Pause(MoreMountains.CorgiEngine.PauseMethods) */, L_4, 1);
		// LevelCompleteSplash.SetActive (true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_LevelCompleteSplash_21();
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * L_6;
		L_6 = MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373(/*hidden argument*/MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_7 = L_6->get_Scenes_7();
		V_0 = L_7;
		V_1 = 0;
		goto IL_00ab;
	}

IL_004b:
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_8 = V_0;
		int32_t L_9 = V_1;
		int32_t L_10 = L_9;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_11 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		V_2 = L_11;
		// if (scene.SceneName == SceneManager.GetActiveScene().name)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_12 = V_2;
		String_t* L_13 = L_12->get_SceneName_0();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_14;
		L_14 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_3 = L_14;
		String_t* L_15;
		L_15 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_3), /*hidden argument*/NULL);
		bool L_16;
		L_16 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_13, L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00a7;
		}
	}
	{
		// for (int i=0; i<Stars.Length; i++)
		V_4 = 0;
		goto IL_009b;
	}

IL_006e:
	{
		// Stars [i].color = (scene.CollectedStars [i]) ? StarOnColor : StarOffColor;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_17 = __this->get_Stars_24();
		int32_t L_18 = V_4;
		int32_t L_19 = L_18;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_20 = (L_17)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_19));
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_21 = V_2;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_22 = L_21->get_CollectedStars_4();
		int32_t L_23 = V_4;
		int32_t L_24 = L_23;
		uint8_t L_25 = (uint8_t)(L_22)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_24));
		G_B6_0 = L_20;
		if (L_25)
		{
			G_B7_0 = L_20;
			goto IL_008a;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_26 = __this->get_StarOffColor_26();
		G_B8_0 = L_26;
		G_B8_1 = G_B6_0;
		goto IL_0090;
	}

IL_008a:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_27 = __this->get_StarOnColor_25();
		G_B8_0 = L_27;
		G_B8_1 = G_B7_0;
	}

IL_0090:
	{
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B8_1, G_B8_0);
		// for (int i=0; i<Stars.Length; i++)
		int32_t L_28 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
	}

IL_009b:
	{
		// for (int i=0; i<Stars.Length; i++)
		int32_t L_29 = V_4;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_30 = __this->get_Stars_24();
		if ((((int32_t)L_29) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_30)->max_length))))))
		{
			goto IL_006e;
		}
	}

IL_00a7:
	{
		int32_t L_31 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_00ab:
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		int32_t L_32 = V_1;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_33 = V_0;
		if ((((int32_t)L_32) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_33)->max_length))))))
		{
			goto IL_004b;
		}
	}
	{
		// if (LevelCompleteSplashFocus != null)
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_34 = __this->get_LevelCompleteSplashFocus_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_35;
		L_35 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_34, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_00d0;
		}
	}
	{
		// EventSystem.current.SetSelectedGameObject(LevelCompleteSplashFocus, null);
		IL2CPP_RUNTIME_CLASS_INIT(EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C_il2cpp_TypeInfo_var);
		EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * L_36;
		L_36 = EventSystem_get_current_m4B9C11F490297AE55428038DACD240596D6CE5F2(/*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_37 = __this->get_LevelCompleteSplashFocus_22();
		EventSystem_SetSelectedGameObject_m7F0F2E78C18FD468E8B5083AFDA6E9D9364D3D5F(L_36, L_37, (BaseEventData_t722C48843CF21B50E06CC0E2E679415E38A7444E *)NULL, /*hidden argument*/NULL);
	}

IL_00d0:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager_OnMMEvent_m5813C52BDB5C6426DF724D4626A485E2921BB0E7 (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  ___corgiEngineEvent0, const RuntimeMethod* method)
{
	{
		// switch (corgiEngineEvent.EventType)
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_0 = ___corgiEngineEvent0;
		int32_t L_1 = L_0.get_EventType_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_000f;
		}
	}
	{
		// LevelComplete ();
		VirtActionInvoker0::Invoke(27 /* System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::LevelComplete() */, __this);
	}

IL_000f:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager_OnEnable_mA69C59DD6FC22949285D4A6D543E9CEB4FBB0C00 (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnEnable();
		GUIManager_OnEnable_mA6F3F63E914526C542DF29CD4373F1F0D01BC96B(__this, /*hidden argument*/NULL);
		// this.MMEventStartListening<CorgiEngineEvent>();
		EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager_OnDisable_m7E4A376D7074CA96CAF9646EF3C89071812448CA (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnDisable();
		GUIManager_OnDisable_mB9C57ECD69FFDDE22F22081A9566F88E87E98356(__this, /*hidden argument*/NULL);
		// this.MMEventStopListening<CorgiEngineEvent>();
		EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureGUIManager__ctor_mCEB46DF00172D3B7A2013E4D3B017F24EB5AA252 (RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8 * __this, const RuntimeMethod* method)
{
	{
		GUIManager__ctor_m6FF23ADF78E962A688E7A29256CB50F232F9E84B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::GoToLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureLevel_GoToLevel_m0F5B54EA595A6D047F7298741688A5EA42EC8277 (RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSceneLoadingManager_tB8F92FD08BEB060070D775E0CFCB5E074F51129F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MMSceneLoadingManager.LoadScene(SceneName);
		String_t* L_0 = __this->get_SceneName_4();
		IL2CPP_RUNTIME_CLASS_INIT(MMSceneLoadingManager_tB8F92FD08BEB060070D775E0CFCB5E074F51129F_il2cpp_TypeInfo_var);
		MMSceneLoadingManager_LoadScene_m5C529AAC4340DE7EDFFCEA2914D9D4F87BE9BBC7(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureLevel_Start_mD38C63C4C3098B5D161FD6DFB064301955D6FEF0 (RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28 * __this, const RuntimeMethod* method)
{
	{
		// InitialSetup ();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::InitialSetup() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::InitialSetup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureLevel_InitialSetup_m0DC4B4FA1E2E13AE6CE25C4A75D903578C1B7DC4 (RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral14BE754BF8F5400050442C7A7201D90C19ABEEF1);
		s_Il2CppMethodInitialized = true;
	}
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* V_0 = NULL;
	int32_t V_1 = 0;
	RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * V_2 = NULL;
	int32_t V_3 = 0;
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B8_0 = NULL;
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B7_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B9_0;
	memset((&G_B9_0), 0, sizeof(G_B9_0));
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * G_B9_1 = NULL;
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * L_0;
		L_0 = MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373(/*hidden argument*/MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_1 = L_0->get_Scenes_7();
		V_0 = L_1;
		V_1 = 0;
		goto IL_00f0;
	}

IL_0012:
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		// if (scene.SceneName == SceneName)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_6 = V_2;
		String_t* L_7 = L_6->get_SceneName_0();
		String_t* L_8 = __this->get_SceneName_4();
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00ec;
		}
	}
	{
		// if (scene.LevelUnlocked)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_10 = V_2;
		bool L_11 = L_10->get_LevelUnlocked_2();
		if (!L_11)
		{
			goto IL_006d;
		}
	}
	{
		// LockedIcon.gameObject.SetActive (false);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_12 = __this->get_LockedIcon_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_12, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_13, (bool)0, /*hidden argument*/NULL);
		// ScenePreview.material = null;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_14 = __this->get_ScenePreview_7();
		VirtActionInvoker1< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, L_14, (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)NULL);
		// ScenePreview.material.SetFloat("_EffectAmount",0);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_15 = __this->get_ScenePreview_7();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_16;
		L_16 = VirtFuncInvoker0< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_15);
		Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768(L_16, _stringLiteral14BE754BF8F5400050442C7A7201D90C19ABEEF1, (0.0f), /*hidden argument*/NULL);
		// }
		goto IL_00b4;
	}

IL_006d:
	{
		// LockedIcon.gameObject.SetActive (true);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_17 = __this->get_LockedIcon_5();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18;
		L_18 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_17, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_18, (bool)1, /*hidden argument*/NULL);
		// ScenePreview.material = OffMaterial;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_19 = __this->get_ScenePreview_7();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_20 = __this->get_OffMaterial_8();
		VirtActionInvoker1< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(33 /* System.Void UnityEngine.UI.Graphic::set_material(UnityEngine.Material) */, L_19, L_20);
		// ScenePreview.material.SetFloat("_EffectAmount",1);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_21 = __this->get_ScenePreview_7();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_22;
		L_22 = VirtFuncInvoker0< Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(32 /* UnityEngine.Material UnityEngine.UI.Graphic::get_material() */, L_21);
		Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768(L_22, _stringLiteral14BE754BF8F5400050442C7A7201D90C19ABEEF1, (1.0f), /*hidden argument*/NULL);
		// PlayButton.DisableButton ();
		MMTouchButton_t5D435B673C54A6B33DFF49073BFDE5B9563BF469 * L_23 = __this->get_PlayButton_6();
		VirtActionInvoker0::Invoke(22 /* System.Void MoreMountains.Tools.MMTouchButton::DisableButton() */, L_23);
	}

IL_00b4:
	{
		// for (int i=0; i<Stars.Length; i++)
		V_3 = 0;
		goto IL_00e1;
	}

IL_00b8:
	{
		// Stars [i].color = (scene.CollectedStars [i]) ? StarOnColor : StarOffColor;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_24 = __this->get_Stars_9();
		int32_t L_25 = V_3;
		int32_t L_26 = L_25;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_27 = (L_24)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_26));
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_28 = V_2;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_29 = L_28->get_CollectedStars_4();
		int32_t L_30 = V_3;
		int32_t L_31 = L_30;
		uint8_t L_32 = (uint8_t)(L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_31));
		G_B7_0 = L_27;
		if (L_32)
		{
			G_B8_0 = L_27;
			goto IL_00d2;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_33 = __this->get_StarOffColor_10();
		G_B9_0 = L_33;
		G_B9_1 = G_B7_0;
		goto IL_00d8;
	}

IL_00d2:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_34 = __this->get_StarOnColor_11();
		G_B9_0 = L_34;
		G_B9_1 = G_B8_0;
	}

IL_00d8:
	{
		VirtActionInvoker1< Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  >::Invoke(23 /* System.Void UnityEngine.UI.Graphic::set_color(UnityEngine.Color) */, G_B9_1, G_B9_0);
		// for (int i=0; i<Stars.Length; i++)
		int32_t L_35 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_35, (int32_t)1));
	}

IL_00e1:
	{
		// for (int i=0; i<Stars.Length; i++)
		int32_t L_36 = V_3;
		ImageU5BU5D_t173C9D1F1D57DABC8260713678F7094C9E7FD224* L_37 = __this->get_Stars_9();
		if ((((int32_t)L_36) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_37)->max_length))))))
		{
			goto IL_00b8;
		}
	}

IL_00ec:
	{
		int32_t L_38 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00f0:
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		int32_t L_39 = V_1;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_40 = V_0;
		if ((((int32_t)L_39) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_40)->max_length))))))
		{
			goto IL_0012;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureLevel__ctor_mD3D9CE8D5D720F3D4DD7A672F21ECAB2E4090950 (RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_InitialMaximumLives()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// public int InitialMaximumLives { get; set; }
		int32_t L_0 = __this->get_U3CInitialMaximumLivesU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_InitialMaximumLives(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int InitialMaximumLives { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CInitialMaximumLivesU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_InitialCurrentLives()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// public int InitialCurrentLives { get; set; }
		int32_t L_0 = __this->get_U3CInitialCurrentLivesU3Ek__BackingField_6();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_InitialCurrentLives(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int InitialCurrentLives { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CInitialCurrentLivesU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Single MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_CurrentStars()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// public float CurrentStars { get; protected set; }
		float L_0 = __this->get_U3CCurrentStarsU3Ek__BackingField_9();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_CurrentStars(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentStars { get; protected set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentStarsU3Ek__BackingField_9(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_Awake_m297B452272540A6B67E70D2965D335BE73F95738 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_Awake_m9F53860BDC9DBAE7D1DC680D2A20F93D99BB2719_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Awake ();
		MMSingleton_1_Awake_m9F53860BDC9DBAE7D1DC680D2A20F93D99BB2719(__this, /*hidden argument*/MMSingleton_1_Awake_m9F53860BDC9DBAE7D1DC680D2A20F93D99BB2719_RuntimeMethod_var);
		// LoadSavedProgress ();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::LoadSavedProgress() */, __this);
		// InitializeStars ();
		VirtActionInvoker0::Invoke(8 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::InitializeStars() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::LevelComplete()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_LevelComplete_m269033BC6F6CB023B473ADDC31AFA23350E44362 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// for (int i = 0; i < Scenes.Length; i++)
		V_0 = 0;
		goto IL_0062;
	}

IL_0004:
	{
		// if (Scenes[i].SceneName == SceneManager.GetActiveScene().name)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_0 = __this->get_Scenes_7();
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		String_t* L_4 = L_3->get_SceneName_0();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_5;
		L_5 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_1 = L_5;
		String_t* L_6;
		L_6 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_1), /*hidden argument*/NULL);
		bool L_7;
		L_7 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005e;
		}
	}
	{
		// Scenes[i].LevelComplete = true;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_8 = __this->get_Scenes_7();
		int32_t L_9 = V_0;
		int32_t L_10 = L_9;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_11 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_10));
		L_11->set_LevelComplete_1((bool)1);
		// Scenes[i].LevelUnlocked = true;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_12 = __this->get_Scenes_7();
		int32_t L_13 = V_0;
		int32_t L_14 = L_13;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_15 = (L_12)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_14));
		L_15->set_LevelUnlocked_2((bool)1);
		// if (i < Scenes.Length - 1)
		int32_t L_16 = V_0;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_17 = __this->get_Scenes_7();
		if ((((int32_t)L_16) >= ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_17)->max_length))), (int32_t)1)))))
		{
			goto IL_005e;
		}
	}
	{
		// Scenes [i + 1].LevelUnlocked = true;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_18 = __this->get_Scenes_7();
		int32_t L_19 = V_0;
		int32_t L_20 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_21 = (L_18)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_20));
		L_21->set_LevelUnlocked_2((bool)1);
	}

IL_005e:
	{
		// for (int i = 0; i < Scenes.Length; i++)
		int32_t L_22 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0062:
	{
		// for (int i = 0; i < Scenes.Length; i++)
		int32_t L_23 = V_0;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_24 = __this->get_Scenes_7();
		if ((((int32_t)L_23) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::InitializeStars()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_InitializeStars_m0AA36F967AC3A29EDECF74A195167FE1F35CF14A (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* V_0 = NULL;
	int32_t V_1 = 0;
	RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * V_2 = NULL;
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* V_5 = NULL;
	int32_t V_6 = 0;
	{
		// foreach (RetroAdventureScene scene in Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_0 = __this->get_Scenes_7();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0061;
	}

IL_000b:
	{
		// foreach (RetroAdventureScene scene in Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// if (scene.SceneName == SceneManager.GetActiveScene().name)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_5 = V_2;
		String_t* L_6 = L_5->get_SceneName_0();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_7;
		L_7 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_3 = L_7;
		String_t* L_8;
		L_8 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_3), /*hidden argument*/NULL);
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_005d;
		}
	}
	{
		// int stars = 0;
		V_4 = 0;
		// foreach (bool star in scene.CollectedStars)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_10 = V_2;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_11 = L_10->get_CollectedStars_4();
		V_5 = L_11;
		V_6 = 0;
		goto IL_004c;
	}

IL_0039:
	{
		// foreach (bool star in scene.CollectedStars)
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_12 = V_5;
		int32_t L_13 = V_6;
		int32_t L_14 = L_13;
		uint8_t L_15 = (uint8_t)(L_12)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_14));
		// if (star) { stars++; }
		if (!L_15)
		{
			goto IL_0046;
		}
	}
	{
		// if (star) { stars++; }
		int32_t L_16 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0046:
	{
		int32_t L_17 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_17, (int32_t)1));
	}

IL_004c:
	{
		// foreach (bool star in scene.CollectedStars)
		int32_t L_18 = V_6;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_19 = V_5;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_19)->max_length))))))
		{
			goto IL_0039;
		}
	}
	{
		// CurrentStars = stars;
		int32_t L_20 = V_4;
		RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56_inline(__this, ((float)((float)L_20)), /*hidden argument*/NULL);
	}

IL_005d:
	{
		int32_t L_21 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_0061:
	{
		// foreach (RetroAdventureScene scene in Scenes)
		int32_t L_22 = V_1;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_23 = V_0;
		if ((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::SaveProgress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_SaveProgress_mD5271DD17C484FB3A916D1C1426E98FC37431649 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral38D4FF83ACF3D8288F951702EED939E1195211CB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Progress progress = new Progress ();
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_0 = (Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 *)il2cpp_codegen_object_new(Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_il2cpp_TypeInfo_var);
		Progress__ctor_m328101047C95644D26CFCC3D5B11384D08734D4F(L_0, /*hidden argument*/NULL);
		// progress.MaximumLives = GameManager.Instance.MaximumLives;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_1 = L_0;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_2;
		L_2 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		int32_t L_3 = L_2->get_MaximumLives_8();
		L_1->set_MaximumLives_2(L_3);
		// progress.CurrentLives = GameManager.Instance.CurrentLives;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_4 = L_1;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_5;
		L_5 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		int32_t L_6 = L_5->get_CurrentLives_9();
		L_4->set_CurrentLives_3(L_6);
		// progress.InitialMaximumLives = InitialMaximumLives;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_7 = L_4;
		int32_t L_8;
		L_8 = RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB_inline(__this, /*hidden argument*/NULL);
		L_7->set_InitialMaximumLives_0(L_8);
		// progress.InitialCurrentLives = InitialCurrentLives;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_9 = L_7;
		int32_t L_10;
		L_10 = RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52_inline(__this, /*hidden argument*/NULL);
		L_9->set_InitialCurrentLives_1(L_10);
		// progress.Scenes = Scenes;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_11 = L_9;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_12 = __this->get_Scenes_7();
		L_11->set_Scenes_4(L_12);
		// MMSaveLoadManager.Save(progress, _saveFileName, _saveFolderName);
		IL2CPP_RUNTIME_CLASS_INIT(MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var);
		MMSaveLoadManager_Save_m95F6B8019AF2B7F28C45ADCF5CA0590C03173BED(L_11, _stringLiteral38D4FF83ACF3D8288F951702EED939E1195211CB, _stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::CreateSaveGame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_CreateSaveGame_mB8949DA72B6E82FF2563B16D0C9412040B106242 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// SaveProgress();
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::SaveProgress() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::LoadSavedProgress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_LoadSavedProgress_m0794BAACFEAC07D49704DA9A148F9B662B13EA0D (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral38D4FF83ACF3D8288F951702EED939E1195211CB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11);
		s_Il2CppMethodInitialized = true;
	}
	Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * V_0 = NULL;
	{
		// Progress progress = (Progress)MMSaveLoadManager.Load(typeof(Progress), _saveFileName, _saveFolderName);
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1;
		L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var);
		RuntimeObject * L_2;
		L_2 = MMSaveLoadManager_Load_m507199BE6138E42AFF712DAEB3744BA9CEAC4B4A(L_1, _stringLiteral38D4FF83ACF3D8288F951702EED939E1195211CB, _stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11, /*hidden argument*/NULL);
		V_0 = ((Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 *)CastclassClass((RuntimeObject*)L_2, Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42_il2cpp_TypeInfo_var));
		// if (progress != null)
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_3 = V_0;
		if (!L_3)
		{
			goto IL_0067;
		}
	}
	{
		// GameManager.Instance.MaximumLives = progress.MaximumLives;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_4;
		L_4 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_5 = V_0;
		int32_t L_6 = L_5->get_MaximumLives_2();
		L_4->set_MaximumLives_8(L_6);
		// GameManager.Instance.CurrentLives = progress.CurrentLives;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_7;
		L_7 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_8 = V_0;
		int32_t L_9 = L_8->get_CurrentLives_3();
		L_7->set_CurrentLives_9(L_9);
		// InitialMaximumLives = progress.InitialMaximumLives;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_10 = V_0;
		int32_t L_11 = L_10->get_InitialMaximumLives_0();
		RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223_inline(__this, L_11, /*hidden argument*/NULL);
		// InitialCurrentLives = progress.InitialCurrentLives;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_12 = V_0;
		int32_t L_13 = L_12->get_InitialCurrentLives_1();
		RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C_inline(__this, L_13, /*hidden argument*/NULL);
		// Scenes = progress.Scenes;
		Progress_tFEBAA8CA8BF761C640061BD7EA2D34653490BD42 * L_14 = V_0;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_15 = L_14->get_Scenes_4();
		__this->set_Scenes_7(L_15);
		// }
		return;
	}

IL_0067:
	{
		// InitialMaximumLives = GameManager.Instance.MaximumLives;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_16;
		L_16 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		int32_t L_17 = L_16->get_MaximumLives_8();
		RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223_inline(__this, L_17, /*hidden argument*/NULL);
		// InitialCurrentLives = GameManager.Instance.CurrentLives;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_18;
		L_18 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		int32_t L_19 = L_18->get_CurrentLives_9();
		RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C_inline(__this, L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineStarEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_OnMMEvent_mBD4A63105FFBB77336C3CA91494BE56AEEDCFC52 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF  ___corgiStarEvent0, const RuntimeMethod* method)
{
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* V_0 = NULL;
	int32_t V_1 = 0;
	RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * V_2 = NULL;
	float V_3 = 0.0f;
	{
		// foreach (RetroAdventureScene scene in Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_0 = __this->get_Scenes_7();
		V_0 = L_0;
		V_1 = 0;
		goto IL_0048;
	}

IL_000b:
	{
		// foreach (RetroAdventureScene scene in Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = L_2;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		V_2 = L_4;
		// if (scene.SceneName == corgiStarEvent.SceneName)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_5 = V_2;
		String_t* L_6 = L_5->get_SceneName_0();
		CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF  L_7 = ___corgiStarEvent0;
		String_t* L_8 = L_7.get_SceneName_0();
		bool L_9;
		L_9 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_6, L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0044;
		}
	}
	{
		// scene.CollectedStars [corgiStarEvent.StarID] = true;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_10 = V_2;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_11 = L_10->get_CollectedStars_4();
		CorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF  L_12 = ___corgiStarEvent0;
		int32_t L_13 = L_12.get_StarID_1();
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_13), (bool)1);
		// CurrentStars++;
		float L_14;
		L_14 = RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC_inline(__this, /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = V_3;
		RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56_inline(__this, ((float)il2cpp_codegen_add((float)L_15, (float)(1.0f))), /*hidden argument*/NULL);
	}

IL_0044:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_0048:
	{
		// foreach (RetroAdventureScene scene in Scenes)
		int32_t L_17 = V_1;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_18 = V_0;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))
		{
			goto IL_000b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_OnMMEvent_mFD2CFF7F7D5A54D5977775A9F1D0B151EC2EE6E4 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  ___gameEvent0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// switch (gameEvent.EventType)
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_0 = ___gameEvent0;
		int32_t L_1 = L_0.get_EventType_0();
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)9))))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}

IL_0011:
	{
		// LevelComplete ();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::LevelComplete() */, __this);
		// SaveProgress ();
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::SaveProgress() */, __this);
		// break;
		return;
	}

IL_001e:
	{
		// GameOver ();
		VirtActionInvoker0::Invoke(14 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::GameOver() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::GameOver()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_GameOver_m0F44C908A39FB4A9B91FE9699E1DA563E27B8F11 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// ResetProgress ();
		VirtActionInvoker0::Invoke(16 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::ResetProgress() */, __this);
		// ResetLives ();
		VirtActionInvoker0::Invoke(15 /* System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::ResetLives() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::ResetLives()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_ResetLives_m5216E3EA0AF1A655F740AF028D0E121DB8C7ACE1 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameManager.Instance.MaximumLives = InitialMaximumLives;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_0;
		L_0 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		int32_t L_1;
		L_1 = RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB_inline(__this, /*hidden argument*/NULL);
		L_0->set_MaximumLives_8(L_1);
		// GameManager.Instance.CurrentLives = InitialCurrentLives;
		GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * L_2;
		L_2 = MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB(/*hidden argument*/MMPersistentSingleton_1_get_Instance_mBD8335C9644650F32F5F30A0D104FD6DABD1BBCB_RuntimeMethod_var);
		int32_t L_3;
		L_3 = RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52_inline(__this, /*hidden argument*/NULL);
		L_2->set_CurrentLives_9(L_3);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::ResetProgress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_ResetProgress_m4307B34F9DD80AF725E7C3AD51D9002364D15928 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MMSaveLoadManager.DeleteSaveFolder ("MMRetroAdventureProgress");
		IL2CPP_RUNTIME_CLASS_INIT(MMSaveLoadManager_t21D74949DF6C6D2A3B58CE2EF856A3D21D5C08D2_il2cpp_TypeInfo_var);
		MMSaveLoadManager_DeleteSaveFolder_mBD53EECEC7ED5EAD3AB6A01871C1B7C33D7243B1(_stringLiteralD2C0AB64791CFC9136554651B07ACFDD2459BE11, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_OnEnable_mC654DC829247D18D50F7034358F5F0BE9E7F7B0A (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStartListening<CorgiEngineStarEvent> ();
		EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mD8F047770A8E5DFC943E7733363EC8FFF26C59DE_RuntimeMethod_var);
		// this.MMEventStartListening<CorgiEngineEvent>();
		EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_OnDisable_m1378CA6151546C02FC41D13F65BAD9BA383B0C4A (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStopListening<CorgiEngineStarEvent> ();
		EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisCorgiEngineStarEvent_tBD9D9BF8DABB649A0B36252A7066855E3A0DA3DF_mECD77A2B057F20A7BD65E50107C57E60CD68727D_RuntimeMethod_var);
		// this.MMEventStopListening<CorgiEngineEvent>();
		EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureProgressManager__ctor_mB3F495CE08D74760707ED4C5BE97746B96BF2B32 (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1__ctor_m3CC6785DFF822FDD50FB05361D7B6C10F39D84EE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MMSingleton_1__ctor_m3CC6785DFF822FDD50FB05361D7B6C10F39D84EE(__this, /*hidden argument*/MMSingleton_1__ctor_m3CC6785DFF822FDD50FB05361D7B6C10F39D84EE_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroAdventureScene::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroAdventureScene__ctor_mFFA7165F7D3FC78DD571B766BD87533FB8B4285F (RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroCopter::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroCopter_Initialization_mCB539802F71E0140B7068037F583B388981836DC (RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4_m90CFBE75068B01DBC3E4D3AA19772E13C30F7717_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		CharacterAbility_Initialization_m490E4A987E5686DAE37ADCEA55ADC474E3F91BDC(__this, /*hidden argument*/NULL);
		// _model = _character.CharacterModel;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_0 = ((CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7 *)__this)->get__character_9();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = L_0->get_CharacterModel_14();
		__this->set__model_28(L_1);
		// _characterFly = this.gameObject.MMGetComponentNoAlloc<CharacterFly>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * L_3;
		L_3 = GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4_m90CFBE75068B01DBC3E4D3AA19772E13C30F7717(L_2, /*hidden argument*/GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4_m90CFBE75068B01DBC3E4D3AA19772E13C30F7717_RuntimeMethod_var);
		__this->set__characterFly_27(L_3);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroCopter::ProcessAbility()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroCopter_ProcessAbility_mD5EBA674023BA4A28339893E4EC5D20687D4299D (RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ProcessAbility();
		CharacterAbility_ProcessAbility_m665947A1A59C3538FAD48919DE6A17CE62A28D6D(__this, /*hidden argument*/NULL);
		// if ((_model == null) || (_characterFly == null))
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get__model_28();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * L_2 = __this->get__characterFly_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0023;
		}
	}

IL_0022:
	{
		// return;
		return;
	}

IL_0023:
	{
		// _currentAngle = MMMaths.Remap(_controller.Speed.x, 0f, _characterFly.FlySpeed, 0f, MaximumAllowedAngle);
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_4 = ((CharacterAbility_t5686DEBA47E5860E7545D28C633C44A7D01000A7 *)__this)->get__controller_12();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_x_0();
		CharacterFly_t360869EA90DAD176051A1C0448041D12DA5AAFB4 * L_7 = __this->get__characterFly_27();
		float L_8 = L_7->get_FlySpeed_25();
		float L_9 = __this->get_MaximumAllowedAngle_25();
		float L_10;
		L_10 = MMMaths_Remap_mBEAF91CCEABC9ACD132FC4FF072A8A463B2F6C60(L_6, (0.0f), L_8, (0.0f), L_9, /*hidden argument*/NULL);
		__this->set__currentAngle_29(L_10);
		// _newRotation = Quaternion.Euler(_currentAngle * Vector3.forward);
		float L_11 = __this->get__currentAngle_29();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline(L_11, L_12, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_14;
		L_14 = Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB(L_13, /*hidden argument*/NULL);
		__this->set__newRotation_30(L_14);
		// _model.transform.rotation = Quaternion.Lerp(_model.transform.rotation, _newRotation, CharacterRotationSpeed * Time.deltaTime);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = __this->get__model_28();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_15, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_17 = __this->get__model_28();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_17, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_19;
		L_19 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_18, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_20 = __this->get__newRotation_30();
		float L_21 = __this->get_CharacterRotationSpeed_26();
		float L_22;
		L_22 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23;
		L_23 = Quaternion_Lerp_mBFA4C4D2574C8140AA840273D3E6565D66F6F261(L_19, L_20, ((float)il2cpp_codegen_multiply((float)L_21, (float)L_22)), /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_16, L_23, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroCopter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroCopter__ctor_mFB3A9D8C6C5369BDD999CF9A4FF89E65C0A733CA (RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC * __this, const RuntimeMethod* method)
{
	{
		// public float MaximumAllowedAngle = -30f;
		__this->set_MaximumAllowedAngle_25((-30.0f));
		// public float CharacterRotationSpeed = 10f;
		__this->set_CharacterRotationSpeed_26((10.0f));
		CharacterAbility__ctor_m99A9BC4D760B415E996417AB4658940BF317D8A7(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroDoor::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroDoor_Awake_m0AE22E765B0DF59C15D5A2FE260D24F29228FE16 (RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985 * __this, const RuntimeMethod* method)
{
	MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// DoorLightModel.color = DoorColor;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_DoorLightModel_5();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = __this->get_DoorColor_4();
		SpriteRenderer_set_color_mF2888B03FBD14DAD540AB3F6617231712EB5CD33(L_0, L_1, /*hidden argument*/NULL);
		// ParticleSystem.MainModule mainModule = DoorParticles.main;
		ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E * L_2 = __this->get_DoorParticles_6();
		MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B  L_3;
		L_3 = ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// mainModule.startColor = DoorColor;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_4 = __this->get_DoorColor_4();
		MinMaxGradient_tF4530B26F29D9635D670A33B9EE581EAC48C12B7  L_5;
		L_5 = MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA(L_4, /*hidden argument*/NULL);
		MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42((MainModule_t671F49558CB1A3CFAAD637A7927C076EC2E61F0B *)(&V_0), L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroDoor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroDoor__ctor_mBD851611EE3F48A13A7C792B731C55751B7FFAF6 (RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985 * __this, const RuntimeMethod* method)
{
	{
		// public Color DoorColor = Color.yellow;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1(/*hidden argument*/NULL);
		__this->set_DoorColor_4(L_0);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroNextLevel::NextLevel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroNextLevel_NextLevel_m69D42A79000BD005B73C938B6C559BE114065C55 (RetroNextLevel_t49F31286D6C8F872FD3ADAA1F38E01079D88F4F6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// LevelManager.Instance.GotoNextLevel ();
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_0;
		L_0 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		VirtActionInvoker0::Invoke(15 /* System.Void MoreMountains.CorgiEngine.LevelManager::GotoNextLevel() */, L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroNextLevel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroNextLevel__ctor_m87098EEA866DE6507D4C779B02D15DDC8DA04D9F (RetroNextLevel_t49F31286D6C8F872FD3ADAA1F38E01079D88F4F6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.RetroStar::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroStar_Start_mF1EB43419CFC05D037CBEEB30E83533495EB727E (RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B * __this, const RuntimeMethod* method)
{
	{
		// base.Start ();
		PickableItem_Start_mA869FA4809079960DFE30E24B97B084A52AB9F32(__this, /*hidden argument*/NULL);
		// DisableIfAlreadyCollected ();
		VirtActionInvoker0::Invoke(10 /* System.Void MoreMountains.CorgiEngine.RetroStar::DisableIfAlreadyCollected() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroStar::DisableIfAlreadyCollected()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroStar_DisableIfAlreadyCollected_mB70C9C58119152E68436AEF45D4D329498DE89C2 (RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* V_0 = NULL;
	int32_t V_1 = 0;
	RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * V_2 = NULL;
	Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  V_3;
	memset((&V_3), 0, sizeof(V_3));
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * L_0;
		L_0 = MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373(/*hidden argument*/MMSingleton_1_get_Instance_m12C7EAA0444D13C60463DC3D4DF03338D8042373_RuntimeMethod_var);
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_1 = L_0->get_Scenes_7();
		V_0 = L_1;
		V_1 = 0;
		goto IL_0056;
	}

IL_000f:
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = L_3;
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		// if (scene.SceneName == SceneManager.GetActiveScene().name)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_6 = V_2;
		String_t* L_7 = L_6->get_SceneName_0();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE  L_8;
		L_8 = SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4(/*hidden argument*/NULL);
		V_3 = L_8;
		String_t* L_9;
		L_9 = Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8((Scene_t5495AD2FDC587DB2E94D9BDE2B85868BFB9A92EE *)(&V_3), /*hidden argument*/NULL);
		bool L_10;
		L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_7, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0052;
		}
	}
	{
		// if (scene.CollectedStars.Length >= StarID)
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_11 = V_2;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_12 = L_11->get_CollectedStars_4();
		int32_t L_13 = ((Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401 *)__this)->get_StarID_16();
		if ((((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length)))) < ((int32_t)L_13)))
		{
			goto IL_0052;
		}
	}
	{
		// if (scene.CollectedStars[StarID])
		RetroAdventureScene_tCDB3159928F62E72E0F2C012D278450A7C56192C * L_14 = V_2;
		BooleanU5BU5D_tEC7BAF93C44F875016DAADC8696EE3A465644D3C* L_15 = L_14->get_CollectedStars_4();
		int32_t L_16 = ((Star_t161E31B4F684A8E60F22B1CA69F2A22FB52FB401 *)__this)->get_StarID_16();
		int32_t L_17 = L_16;
		uint8_t L_18 = (uint8_t)(L_15)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_17));
		if (!L_18)
		{
			goto IL_0052;
		}
	}
	{
		// Disable ();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.RetroStar::Disable() */, __this);
	}

IL_0052:
	{
		int32_t L_19 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_0056:
	{
		// foreach (RetroAdventureScene scene in RetroAdventureProgressManager.Instance.Scenes)
		int32_t L_20 = V_1;
		RetroAdventureSceneU5BU5D_tA89F4A4E870BBDE39868C7E1CDDD8C437905EB2D* L_21 = V_0;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_21)->max_length))))))
		{
			goto IL_000f;
		}
	}
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroStar::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroStar_Disable_m1FA46910EF55FCF7F6217F0FECBA338D561CFBFA (RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B * __this, const RuntimeMethod* method)
{
	{
		// this.gameObject.SetActive (false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.RetroStar::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RetroStar__ctor_m51FCC9FB2918C70D1997D3B5F9F82331D9611305 (RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B * __this, const RuntimeMethod* method)
{
	{
		Star__ctor_m778DAC91D16195F08F69B41C82CD752E3F82078B(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth_Initialization_m6AC48ED265AD32EA71256AE63A00737508593C8E (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, const RuntimeMethod* method)
{
	{
		// base.Initialization();
		Health_Initialization_m5FA1B03AFC34508F445DE3E0EC3A8C57353B7237(__this, /*hidden argument*/NULL);
		// _initialScale = transform.localScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_0, /*hidden argument*/NULL);
		__this->set__initialScale_48(L_1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Damage(System.Int32,UnityEngine.GameObject,System.Single,System.Single,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth_Damage_mA9E773BA8F793D9E897E13D44234702BFB3208DA (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, int32_t ___damage0, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___instigator1, float ___flickerDuration2, float ___invincibilityDuration3, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___damageDirection4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_0;
	memset((&V_0), 0, sizeof(V_0));
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B6_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B5_0 = NULL;
	{
		// if (transform.localScale.y==_initialScale.y)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_3 = __this->get_address_of__initialScale_48();
		float L_4 = L_3->get_y_3();
		if ((!(((float)L_2) == ((float)L_4))))
		{
			goto IL_002e;
		}
	}
	{
		// LevelManager.Instance.KillPlayer(_character);
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_5;
		L_5 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_6 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__character_38();
		VirtActionInvoker1< Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * >::Invoke(18 /* System.Void MoreMountains.CorgiEngine.LevelManager::KillPlayer(MoreMountains.CorgiEngine.Character) */, L_5, L_6);
		// }
		return;
	}

IL_002e:
	{
		// DamageDisabled();
		VirtActionInvoker0::Invoke(17 /* System.Void MoreMountains.CorgiEngine.Health::DamageDisabled() */, __this);
		// StartCoroutine(DamageEnabled(0.5f));
		RuntimeObject* L_7;
		L_7 = VirtFuncInvoker1< RuntimeObject*, float >::Invoke(22 /* System.Collections.IEnumerator MoreMountains.CorgiEngine.Health::DamageEnabled(System.Single) */, __this, (0.5f));
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_8;
		L_8 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_7, /*hidden argument*/NULL);
		// Shrink(2f);
		VirtActionInvoker1< float >::Invoke(26 /* System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Shrink(System.Single) */, __this, (2.0f));
		// if (GetComponent<Renderer>() != null)
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_9;
		L_9 = Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C_m436E5B0F17DDEF3CC61F77DEA82B1A92668AF019_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_9, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_009b;
		}
	}
	{
		// Color flickerColor = new Color32(255, 20, 20, 255);
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Color32__ctor_m9D07EC69256BB7ED2784E543848DE7B8484A5C94((&L_11), (uint8_t)((int32_t)255), (uint8_t)((int32_t)20), (uint8_t)((int32_t)20), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_12;
		L_12 = Color32_op_Implicit_m63F14F1A14B1A9A3EE4D154413EE229D3E001623(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		// StartCoroutine(MMImage.Flicker(_renderer,_initialColor,flickerColor,0.05f,0.5f));
		Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * L_13 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__renderer_37();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_14 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__initialColor_36();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_15 = V_0;
		RuntimeObject* L_16;
		L_16 = MMImage_Flicker_m7CD500764E575C625AA2CE9CFC7CE73C04614FAD(L_13, L_14, L_15, (0.0500000007f), (0.5f), /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_17;
		L_17 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_16, /*hidden argument*/NULL);
	}

IL_009b:
	{
		// DamageFeedbacks?.PlayFeedbacks();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_18 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get_DamageFeedbacks_11();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_19 = L_18;
		G_B5_0 = L_19;
		if (L_19)
		{
			G_B6_0 = L_19;
			goto IL_00a6;
		}
	}
	{
		return;
	}

IL_00a6:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B6_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Grow(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth_Grow_m42039F01DB484C347D26BF1F562672F0D11B1C9F (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, float ___growthFactor0, const RuntimeMethod* method)
{
	{
		// transform.localScale *= growthFactor;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = L_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		float L_3 = ___growthFactor0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_2, L_3, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_1, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Shrink(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth_Shrink_m83E2B699753362A96EB8D0991A635630E0731C48 (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, float ___shrinkFactor0, const RuntimeMethod* method)
{
	{
		// transform.localScale = transform.localScale / shrinkFactor;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		float L_3 = ___shrinkFactor0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_2, L_3, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_0, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::ResetScale(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth_ResetScale_m61B52A7BDBDB04396508823157B6207559B77188 (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, float ___growthFactor0, const RuntimeMethod* method)
{
	{
		// transform.localScale = _initialScale;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get__initialScale_48();
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Kill()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth_Kill_m1B5B0969B2DDEF808EE1C7EAA52D16D9FB2291E7 (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_mB8CB45C5289A0ACF38DAD7B4727F32E4E93DFC30_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _controller.SetForce(new Vector2(0, 0));
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_0 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__controller_39();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_1), (0.0f), (0.0f), /*hidden argument*/NULL);
		VirtActionInvoker1< Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  >::Invoke(30 /* System.Void MoreMountains.CorgiEngine.CorgiController::SetForce(UnityEngine.Vector2) */, L_0, L_1);
		// _controller.CollisionsOff();
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_2 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__controller_39();
		VirtActionInvoker0::Invoke(60 /* System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOff() */, L_2);
		// GetComponent<Collider2D>().enabled=false;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_3;
		L_3 = Component_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_mB8CB45C5289A0ACF38DAD7B4727F32E4E93DFC30(__this, /*hidden argument*/Component_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_mB8CB45C5289A0ACF38DAD7B4727F32E4E93DFC30_RuntimeMethod_var);
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_3, (bool)0, /*hidden argument*/NULL);
		// _character.ConditionState.ChangeState(CharacterStates.CharacterConditions.Dead);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_4 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__character_38();
		MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * L_5 = L_4->get_ConditionState_30();
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions>::ChangeState(!0) */, L_5, 4);
		// CurrentHealth=0;
		((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->set_CurrentHealth_4(0);
		// _controller.ResetParameters();
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_6 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__controller_39();
		VirtActionInvoker0::Invoke(67 /* System.Void MoreMountains.CorgiEngine.CorgiController::ResetParameters() */, L_6);
		// _controller.SetForce(new Vector2(0, 20));
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_7 = ((Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 *)__this)->get__controller_39();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), (0.0f), (20.0f), /*hidden argument*/NULL);
		VirtActionInvoker1< Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  >::Invoke(30 /* System.Void MoreMountains.CorgiEngine.CorgiController::SetForce(UnityEngine.Vector2) */, L_7, L_8);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SuperHipsterBrosHealth__ctor_m7F0074239E8783F7E7A70F5BFA71289CE0ECD12A (SuperHipsterBrosHealth_tA3AACB8CD830C9A5DF511EB6F3A056EC68BF3A92 * __this, const RuntimeMethod* method)
{
	{
		Health__ctor_mC4AD594519EE02992BED5B75AC1ADDB3CC0F25AB(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.UncheckReverseGravityInput::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UncheckReverseGravityInput_OnTriggerEnter2D_m963D8CB6D83DCE51E0A9B2B06D6A2DF8D84DF48D (UncheckReverseGravityInput_tB2E610A441AEFFB7B59E20DD94DA6E5E12FA7D90 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4_m1BA5F82EFAF229EA35C6C8F09D91BBAEF9EF53C2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * V_0 = NULL;
	{
		// CharacterGravity characterGravity = collider.gameObject.MMGetComponentNoAlloc<CharacterGravity> ();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * L_2;
		L_2 = GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4_m1BA5F82EFAF229EA35C6C8F09D91BBAEF9EF53C2(L_1, /*hidden argument*/GameObjectExtensions_MMGetComponentNoAlloc_TisCharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4_m1BA5F82EFAF229EA35C6C8F09D91BBAEF9EF53C2_RuntimeMethod_var);
		V_0 = L_2;
		// if (characterGravity == null)
		CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0016;
		}
	}
	{
		// return;
		return;
	}

IL_0016:
	{
		// characterGravity.ReverseHorizontalInputWhenUpsideDown = false;
		CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * L_5 = V_0;
		L_5->set_ReverseHorizontalInputWhenUpsideDown_27((bool)0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.UncheckReverseGravityInput::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UncheckReverseGravityInput__ctor_m157A9AA2942E17B6BD1EF0559297EBAC84188477 (UncheckReverseGravityInput_tB2E610A441AEFFB7B59E20DD94DA6E5E12FA7D90 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, float ___t2, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		float L_1;
		L_1 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___b1;
		float L_5 = L_4.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_x_2();
		float L_8 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___a0;
		float L_10 = L_9.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = ___b1;
		float L_12 = L_11.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = ___a0;
		float L_14 = L_13.get_y_3();
		float L_15 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = ___a0;
		float L_17 = L_16.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = ___b1;
		float L_19 = L_18.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = ___a0;
		float L_21 = L_20.get_z_4();
		float L_22 = ___t2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		memset((&L_23), 0, sizeof(L_23));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_23), ((float)il2cpp_codegen_add((float)L_3, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), (float)L_8)))), ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_12, (float)L_14)), (float)L_15)))), ((float)il2cpp_codegen_add((float)L_17, (float)((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_subtract((float)L_19, (float)L_21)), (float)L_22)))), /*hidden argument*/NULL);
		V_0 = L_23;
		goto IL_0053;
	}

IL_0053:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_0;
		return L_24;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool GameManager_get_StoredLevelMapPosition_m08B6869E7E9E4A0290804DE50C16976CC0E26F4E_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, const RuntimeMethod* method)
{
	{
		// public bool StoredLevelMapPosition{ get; set; }
		bool L_0 = __this->get_U3CStoredLevelMapPositionU3Ek__BackingField_16();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GameManager_get_LevelMapPosition_m28E4FCF400D62585F90B3D8D311510C4EF76BBF1_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 LevelMapPosition { get; set; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_U3CLevelMapPositionU3Ek__BackingField_17();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputManager_get_PrimaryMovement_m67D3A20043621ACB7B8436051BB09487FFAC5DFD_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 PrimaryMovement {get { return _primaryMovement; } }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__primaryMovement_39();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// public bool CollidingWithAPathElement { get; set; }
		bool L_0 = __this->get_U3CCollidingWithAPathElementU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton JumpButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CJumpButtonU3Ek__BackingField_16();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline (IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * __this, const RuntimeMethod* method)
{
	{
		// public MMStateMachine<MMInput.ButtonStates> State {get;protected set;}
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_0 = __this->get_U3CStateU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_StoredLevelMapPosition_m5B58AFCCA29FDD58077A16E59D1A38246853AA67_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool StoredLevelMapPosition{ get; set; }
		bool L_0 = ___value0;
		__this->set_U3CStoredLevelMapPositionU3Ek__BackingField_16(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GameManager_set_LevelMapPosition_m6F6B297ED7D9522CAD10244448B7F59C0587B1AC_inline (GameManager_tDF1CED744FD1957DC7A5E3674EC7D4D906F4965A * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector2 LevelMapPosition { get; set; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___value0;
		__this->set_U3CLevelMapPositionU3Ek__BackingField_17(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool CollidingWithAPathElement { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CCollidingWithAPathElementU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, const RuntimeMethod* method)
{
	{
		// public LevelMapPathElement LastVisitedPathElement { get; set; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = __this->get_U3CLastVisitedPathElementU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2_inline (LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD * __this, LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * ___value0, const RuntimeMethod* method)
{
	{
		// public LevelMapPathElement LastVisitedPathElement { get; set; }
		LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75 * L_0 = ___value0;
		__this->set_U3CLastVisitedPathElementU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// public float CurrentStars { get; protected set; }
		float L_0 = __this->get_U3CCurrentStarsU3Ek__BackingField_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void EventSystem_set_sendNavigationEvents_mC4AF68C06C2A8E1017142D7C9C9AA29018F56F96_inline (EventSystem_t5DC458FCD0355A74CDCCE79287B38B9C4278E39C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { m_sendNavigationEvents = value; }
		bool L_0 = ___value0;
		__this->set_m_sendNavigationEvents_8(L_0);
		// set { m_sendNavigationEvents = value; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public float CurrentStars { get; protected set; }
		float L_0 = ___value0;
		__this->set_U3CCurrentStarsU3Ek__BackingField_9(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// public int InitialMaximumLives { get; set; }
		int32_t L_0 = __this->get_U3CInitialMaximumLivesU3Ek__BackingField_5();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, const RuntimeMethod* method)
{
	{
		// public int InitialCurrentLives { get; set; }
		int32_t L_0 = __this->get_U3CInitialCurrentLivesU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int InitialMaximumLives { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CInitialMaximumLivesU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C_inline (RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int InitialCurrentLives { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CInitialCurrentLivesU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline (CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 Speed { get{ return _speed; } }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__speed_37();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F_inline (float ___d0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a1;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a1;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a1;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t MMStateMachine_1_get_CurrentState_mF8651D0B592EB2D243E9374DADBFD68A0D0C3C69_gshared_inline (MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA * __this, const RuntimeMethod* method)
{
	{
		// public T CurrentState { get; protected set; }
		int32_t L_0 = (int32_t)__this->get_U3CCurrentStateU3Ek__BackingField_2();
		return (int32_t)L_0;
	}
}

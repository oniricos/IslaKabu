﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void EventTester::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void EventTester_OnMMEvent_mF92B414CD5422F9CFFFAE25EC9018746E80E2BC3 (void);
// 0x00000002 System.Void EventTester::OnEnable()
extern void EventTester_OnEnable_mBFC8F3776EACF1586CE4F47F51D3EEF1EE35183E (void);
// 0x00000003 System.Void EventTester::OnDisable()
extern void EventTester_OnDisable_m072D526C9DF1C1F17793E1353A4AF80FA485D651 (void);
// 0x00000004 System.Void EventTester::.ctor()
extern void EventTester__ctor_mB54C11E9EF22D875D991DD0FB4234D80163114D8 (void);
// 0x00000005 UnityEngine.GameObject MoreMountains.InventoryEngine.Inventory::get_Owner()
extern void Inventory_get_Owner_mE01A17C278F484A328297B5708967E25C9795D25 (void);
// 0x00000006 System.Void MoreMountains.InventoryEngine.Inventory::set_Owner(UnityEngine.GameObject)
extern void Inventory_set_Owner_m676D8E71B989E750CF110AB1DE22944089FB2B39 (void);
// 0x00000007 System.Int32 MoreMountains.InventoryEngine.Inventory::get_NumberOfFreeSlots()
extern void Inventory_get_NumberOfFreeSlots_mC3201F8417819104CB608598AF712BFE7E2DCEE5 (void);
// 0x00000008 System.Int32 MoreMountains.InventoryEngine.Inventory::get_NumberOfFilledSlots()
extern void Inventory_get_NumberOfFilledSlots_mB04B9C73858D05F7CC34E28888AAAEAD68D4E1F0 (void);
// 0x00000009 System.Int32 MoreMountains.InventoryEngine.Inventory::NumberOfStackableSlots(System.String,System.Int32)
extern void Inventory_NumberOfStackableSlots_m567626C28C2F43871517B511174FFB84080A7DE3 (void);
// 0x0000000A MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.Inventory::FindInventory(System.String,System.String)
extern void Inventory_FindInventory_m38D814B1C53E9544BE319693FDD581ECE248E854 (void);
// 0x0000000B System.Void MoreMountains.InventoryEngine.Inventory::Awake()
extern void Inventory_Awake_mD5A36301D4B209847958CB86F0ADE635E4C1A519 (void);
// 0x0000000C System.Void MoreMountains.InventoryEngine.Inventory::RegisterInventory()
extern void Inventory_RegisterInventory_mE0B0410E5B78D97B5BC39179EF96F3288776E405 (void);
// 0x0000000D System.Void MoreMountains.InventoryEngine.Inventory::SetOwner(UnityEngine.GameObject)
extern void Inventory_SetOwner_mB217EB475226AA95136D55A1A4B53402A4D357AF (void);
// 0x0000000E System.Boolean MoreMountains.InventoryEngine.Inventory::AddItem(MoreMountains.InventoryEngine.InventoryItem,System.Int32)
extern void Inventory_AddItem_mEA9FCAA7D7910B677678C71F4C2F56C4B69C15C9 (void);
// 0x0000000F System.Boolean MoreMountains.InventoryEngine.Inventory::AddItemAt(MoreMountains.InventoryEngine.InventoryItem,System.Int32,System.Int32)
extern void Inventory_AddItemAt_mDBE8BFBD1B76AA1CAEBE56E5399467E28C69796C (void);
// 0x00000010 System.Boolean MoreMountains.InventoryEngine.Inventory::MoveItem(System.Int32,System.Int32)
extern void Inventory_MoveItem_m3CE8F497BE26B387D40AEE8E10E6FF097128FE26 (void);
// 0x00000011 System.Boolean MoreMountains.InventoryEngine.Inventory::MoveItemToInventory(System.Int32,MoreMountains.InventoryEngine.Inventory,System.Int32)
extern void Inventory_MoveItemToInventory_m939B9891EF2384C0DEC431EF566721C5E98AF5B7 (void);
// 0x00000012 System.Boolean MoreMountains.InventoryEngine.Inventory::RemoveItem(System.Int32,System.Int32)
extern void Inventory_RemoveItem_m9A0FF0AB1E1F6C974CCBDBBBD8780D14A2F31FCB (void);
// 0x00000013 System.Boolean MoreMountains.InventoryEngine.Inventory::RemoveItemByID(System.String,System.Int32)
extern void Inventory_RemoveItemByID_mB36E972F2401C4193C6B08D3F15AEF55CE3EB6B0 (void);
// 0x00000014 System.Boolean MoreMountains.InventoryEngine.Inventory::DestroyItem(System.Int32)
extern void Inventory_DestroyItem_mE59C28BA96B1A6323ED1D9269725F46215455667 (void);
// 0x00000015 System.Void MoreMountains.InventoryEngine.Inventory::EmptyInventory()
extern void Inventory_EmptyInventory_m44F479C47DBB48F377FEE164D749B19A3BF9508A (void);
// 0x00000016 System.Boolean MoreMountains.InventoryEngine.Inventory::AddItemToArray(MoreMountains.InventoryEngine.InventoryItem,System.Int32)
extern void Inventory_AddItemToArray_mB09D8A3CA4065491C7793BCBFD0F96B5219F065E (void);
// 0x00000017 System.Boolean MoreMountains.InventoryEngine.Inventory::RemoveItemFromArray(System.Int32)
extern void Inventory_RemoveItemFromArray_m56A9D9FBBBC07EB06BBD2A5876348190E1F07DC2 (void);
// 0x00000018 System.Void MoreMountains.InventoryEngine.Inventory::ResizeArray(System.Int32)
extern void Inventory_ResizeArray_mD298A2AE200112335D5599912901F1EA6DA94CC9 (void);
// 0x00000019 System.Int32 MoreMountains.InventoryEngine.Inventory::GetQuantity(System.String)
extern void Inventory_GetQuantity_m8C4C0F188C23AE701D9BC03AC7075F2ED98AEAE7 (void);
// 0x0000001A System.Collections.Generic.List`1<System.Int32> MoreMountains.InventoryEngine.Inventory::InventoryContains(System.String)
extern void Inventory_InventoryContains_m08B4257DE759D7C7FEB53AD1D8EE73B45FC8253E (void);
// 0x0000001B System.Collections.Generic.List`1<System.Int32> MoreMountains.InventoryEngine.Inventory::InventoryContains(MoreMountains.InventoryEngine.ItemClasses)
extern void Inventory_InventoryContains_m98E68473557C6D05A2B5FC5328381BA256684669 (void);
// 0x0000001C System.Void MoreMountains.InventoryEngine.Inventory::SaveInventory()
extern void Inventory_SaveInventory_m28E3C10D07D3121DCE0B53F40ABF526157E1E95A (void);
// 0x0000001D System.Void MoreMountains.InventoryEngine.Inventory::LoadSavedInventory()
extern void Inventory_LoadSavedInventory_mD94C2093D5093EE6922E580548293B1668D78699 (void);
// 0x0000001E System.Void MoreMountains.InventoryEngine.Inventory::FillSerializedInventory(MoreMountains.InventoryEngine.SerializedInventory)
extern void Inventory_FillSerializedInventory_mC5EB6C471663F5BAFB0A9E39814BC7518D1BFE7D (void);
// 0x0000001F System.Void MoreMountains.InventoryEngine.Inventory::ExtractSerializedInventory(MoreMountains.InventoryEngine.SerializedInventory)
extern void Inventory_ExtractSerializedInventory_m764021A6F7B57071E9D29553DE01E734A6768445 (void);
// 0x00000020 System.String MoreMountains.InventoryEngine.Inventory::DetermineSaveName()
extern void Inventory_DetermineSaveName_m3D46016AEEAC9DB0D0676784AD51BE69D3F18097 (void);
// 0x00000021 System.Void MoreMountains.InventoryEngine.Inventory::ResetSavedInventory()
extern void Inventory_ResetSavedInventory_m8DCFC812C38259CFCF5A65616EB5ED190EAEBA90 (void);
// 0x00000022 System.Boolean MoreMountains.InventoryEngine.Inventory::UseItem(MoreMountains.InventoryEngine.InventoryItem,System.Int32,MoreMountains.InventoryEngine.InventorySlot)
extern void Inventory_UseItem_mA6817664748AA5DC60689E372A5A8D4641D1F831 (void);
// 0x00000023 System.Boolean MoreMountains.InventoryEngine.Inventory::UseItem(System.String)
extern void Inventory_UseItem_m8C1A8A8685F9E4803CB340E34DA051B5396F287A (void);
// 0x00000024 System.Void MoreMountains.InventoryEngine.Inventory::EquipItem(MoreMountains.InventoryEngine.InventoryItem,System.Int32,MoreMountains.InventoryEngine.InventorySlot)
extern void Inventory_EquipItem_m27DB599753763F1C1B57B02F255298DDF3473AB5 (void);
// 0x00000025 System.Void MoreMountains.InventoryEngine.Inventory::DropItem(MoreMountains.InventoryEngine.InventoryItem,System.Int32,MoreMountains.InventoryEngine.InventorySlot)
extern void Inventory_DropItem_mB75B8B604D284D4E49AE564B27A6BEEBCEE01118 (void);
// 0x00000026 System.Void MoreMountains.InventoryEngine.Inventory::DestroyItem(MoreMountains.InventoryEngine.InventoryItem,System.Int32,MoreMountains.InventoryEngine.InventorySlot)
extern void Inventory_DestroyItem_m2A588BE8753242823897BBA24C7310B46FFCC1A6 (void);
// 0x00000027 System.Void MoreMountains.InventoryEngine.Inventory::UnEquipItem(MoreMountains.InventoryEngine.InventoryItem,System.Int32,MoreMountains.InventoryEngine.InventorySlot)
extern void Inventory_UnEquipItem_mA5B4851209DE2608D440090EE75ACC2348F00E1B (void);
// 0x00000028 System.Void MoreMountains.InventoryEngine.Inventory::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void Inventory_OnMMEvent_m5539D4B1C2A16366A6C97DD53C9BF7F0614E3A55 (void);
// 0x00000029 System.Void MoreMountains.InventoryEngine.Inventory::OnMMEvent(MoreMountains.Tools.MMGameEvent)
extern void Inventory_OnMMEvent_mFF376C5CBC331556BDD28EA5D296C557B76D77F3 (void);
// 0x0000002A System.Void MoreMountains.InventoryEngine.Inventory::OnEnable()
extern void Inventory_OnEnable_mD958CFD2663A0698694BE6C27BF169E392752FDB (void);
// 0x0000002B System.Void MoreMountains.InventoryEngine.Inventory::OnDisable()
extern void Inventory_OnDisable_mA592794C5E3CAEB288206A5A351F39980CB7B245 (void);
// 0x0000002C System.Void MoreMountains.InventoryEngine.Inventory::.ctor()
extern void Inventory__ctor_m5135BCDC2311B5A29AF8661937DB5F8275F274F3 (void);
// 0x0000002D System.Void MoreMountains.InventoryEngine.InventoryCharacterIdentifier::.ctor()
extern void InventoryCharacterIdentifier__ctor_m20C762BFDE93F2535700AFBB69FEEA818289BD83 (void);
// 0x0000002E System.Void MoreMountains.InventoryEngine.MMInventoryEvent::.ctor(MoreMountains.InventoryEngine.MMInventoryEventType,MoreMountains.InventoryEngine.InventorySlot,System.String,MoreMountains.InventoryEngine.InventoryItem,System.Int32,System.Int32,System.String)
extern void MMInventoryEvent__ctor_m8FA291F8E8B9BC49693B8F01CEEF6CBCA2F6A953 (void);
// 0x0000002F System.Void MoreMountains.InventoryEngine.MMInventoryEvent::Trigger(MoreMountains.InventoryEngine.MMInventoryEventType,MoreMountains.InventoryEngine.InventorySlot,System.String,MoreMountains.InventoryEngine.InventoryItem,System.Int32,System.Int32,System.String)
extern void MMInventoryEvent_Trigger_m77BAD1039CC50ABAF4F9E3B7B45D201F034FE9B7 (void);
// 0x00000030 MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryInputActions::get_TargetInventory()
extern void InventoryInputActions_get_TargetInventory_m061A817D01C593A793BF92EC1BA9C5C1FE45B1DA (void);
// 0x00000031 System.Void MoreMountains.InventoryEngine.InventoryInputActions::Start()
extern void InventoryInputActions_Start_m24E2A3265D73FD66B8BB60BF7CF7F5267B627BBE (void);
// 0x00000032 System.Void MoreMountains.InventoryEngine.InventoryInputActions::Initialization()
extern void InventoryInputActions_Initialization_m4A17B27A98BFBD7D11364973BCFA5C8661CF68E8 (void);
// 0x00000033 System.Void MoreMountains.InventoryEngine.InventoryInputActions::Update()
extern void InventoryInputActions_Update_mB5C519F7EF3AFF6ED5F294FD9D0FD3D9D2B6F49D (void);
// 0x00000034 System.Void MoreMountains.InventoryEngine.InventoryInputActions::DetectInput()
extern void InventoryInputActions_DetectInput_m4DCE327B0EE593564A66D6D45BEBEE1429EB5A93 (void);
// 0x00000035 System.Void MoreMountains.InventoryEngine.InventoryInputActions::ExecuteAction(MoreMountains.InventoryEngine.InventoryInputActions/InventoryInputActionsBindings)
extern void InventoryInputActions_ExecuteAction_m85A9BAC88A85AA154095D89A63663FBD4671EA17 (void);
// 0x00000036 System.Void MoreMountains.InventoryEngine.InventoryInputActions::.ctor()
extern void InventoryInputActions__ctor_m55FB3CE564413772FB97EEBEA4BF72B4B390FAC5 (void);
// 0x00000037 System.Void MoreMountains.InventoryEngine.InventoryInputActions/InventoryInputActionsBindings::.ctor()
extern void InventoryInputActionsBindings__ctor_m597BEE899605DA444EA955C0D9F5331747DFE4FF (void);
// 0x00000038 MoreMountains.InventoryEngine.InventorySlot MoreMountains.InventoryEngine.InventoryInputManager::get_CurrentlySelectedInventorySlot()
extern void InventoryInputManager_get_CurrentlySelectedInventorySlot_mE06883E42278A8B2A4DFF27879C4CFD2EF38AEC8 (void);
// 0x00000039 System.Void MoreMountains.InventoryEngine.InventoryInputManager::set_CurrentlySelectedInventorySlot(MoreMountains.InventoryEngine.InventorySlot)
extern void InventoryInputManager_set_CurrentlySelectedInventorySlot_m7651E8F8A7EF0159C3CC275FFFFA09F56DE108B4 (void);
// 0x0000003A System.Void MoreMountains.InventoryEngine.InventoryInputManager::Start()
extern void InventoryInputManager_Start_m028A4BD0940DDBE740438CF8E1CF65AD4115457C (void);
// 0x0000003B System.Void MoreMountains.InventoryEngine.InventoryInputManager::Update()
extern void InventoryInputManager_Update_m197D4AA17B6FAD1769A489E363251D1121710A8D (void);
// 0x0000003C System.Void MoreMountains.InventoryEngine.InventoryInputManager::CheckCurrentlySelectedSlot()
extern void InventoryInputManager_CheckCurrentlySelectedSlot_m44793586BE4FBFB6E1F24189EDF32FAC001D03DF (void);
// 0x0000003D System.Void MoreMountains.InventoryEngine.InventoryInputManager::HandleButtons()
extern void InventoryInputManager_HandleButtons_m68D53A055FE612693134694C39656BD595E8BB7A (void);
// 0x0000003E System.Void MoreMountains.InventoryEngine.InventoryInputManager::SetButtonState(UnityEngine.UI.Button,System.Boolean)
extern void InventoryInputManager_SetButtonState_m745030EF572FA4FB9824AADD228F8A51E410EC4F (void);
// 0x0000003F System.Void MoreMountains.InventoryEngine.InventoryInputManager::ToggleInventory()
extern void InventoryInputManager_ToggleInventory_mFE9B9E69477B863061B855F8B1B8DC091FC8320A (void);
// 0x00000040 System.Void MoreMountains.InventoryEngine.InventoryInputManager::OpenInventory()
extern void InventoryInputManager_OpenInventory_m8E82922578FEA7439B9237AECC3256EC6C675BAD (void);
// 0x00000041 System.Void MoreMountains.InventoryEngine.InventoryInputManager::CloseInventory()
extern void InventoryInputManager_CloseInventory_m61E71EA02936E0139D55E82DDE4EA888D8163DAD (void);
// 0x00000042 System.Void MoreMountains.InventoryEngine.InventoryInputManager::HandleInventoryInput()
extern void InventoryInputManager_HandleInventoryInput_mB5DBE2A5358C299460390FD318E599BF3A31298F (void);
// 0x00000043 System.Void MoreMountains.InventoryEngine.InventoryInputManager::HandleHotbarsInput()
extern void InventoryInputManager_HandleHotbarsInput_mF30D17BFC07B67AD7E6C7DA71EB7F3B8D70A0A42 (void);
// 0x00000044 System.Void MoreMountains.InventoryEngine.InventoryInputManager::EquipOrUse()
extern void InventoryInputManager_EquipOrUse_mD093ED2D924A06675772718CE7426D170E407245 (void);
// 0x00000045 System.Void MoreMountains.InventoryEngine.InventoryInputManager::Equip()
extern void InventoryInputManager_Equip_m571F83356D24D12E6573B28592D9464CC7DF3E55 (void);
// 0x00000046 System.Void MoreMountains.InventoryEngine.InventoryInputManager::Use()
extern void InventoryInputManager_Use_m264F9A71B88CCC0E15F8D1E9087A7A2F29A34EE2 (void);
// 0x00000047 System.Void MoreMountains.InventoryEngine.InventoryInputManager::UnEquip()
extern void InventoryInputManager_UnEquip_mAEFA5421C46FB506A0D5FF3DB126A84190F995BA (void);
// 0x00000048 System.Void MoreMountains.InventoryEngine.InventoryInputManager::Move()
extern void InventoryInputManager_Move_m9B2E0E012B6D6B212D7EEF9B5428DCCD8408712C (void);
// 0x00000049 System.Void MoreMountains.InventoryEngine.InventoryInputManager::Drop()
extern void InventoryInputManager_Drop_mF6F52A2D73FFC8D60F8F957F5DC60F1271398A69 (void);
// 0x0000004A System.Void MoreMountains.InventoryEngine.InventoryInputManager::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void InventoryInputManager_OnMMEvent_m9D220D6A2A2E481525289BC56B92EFD419A0164C (void);
// 0x0000004B System.Void MoreMountains.InventoryEngine.InventoryInputManager::OnEnable()
extern void InventoryInputManager_OnEnable_m0C13CA6B356B1DD06BE1C8744473D72FA55B2385 (void);
// 0x0000004C System.Void MoreMountains.InventoryEngine.InventoryInputManager::OnDisable()
extern void InventoryInputManager_OnDisable_mCFD53FF0B86560C463A39C74C5D59C53D3D70597 (void);
// 0x0000004D System.Void MoreMountains.InventoryEngine.InventoryInputManager::.ctor()
extern void InventoryInputManager__ctor_mB0787C44C06830BE9D0E903551DDB81C6AE12106 (void);
// 0x0000004E System.Boolean MoreMountains.InventoryEngine.InventoryItem::get_IsUsable()
extern void InventoryItem_get_IsUsable_m16380763D3EAA35A273839E385A674A141624491 (void);
// 0x0000004F System.Boolean MoreMountains.InventoryEngine.InventoryItem::get_IsEquippable()
extern void InventoryItem_get_IsEquippable_m500FC17BDF833237433042CBD5C68D6451F2D1CC (void);
// 0x00000050 MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::TargetInventory(System.String)
extern void InventoryItem_TargetInventory_mB0069D82520BD2E9BA11ED16E2FD652607A7983A (void);
// 0x00000051 MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::TargetEquipmentInventory(System.String)
extern void InventoryItem_TargetEquipmentInventory_mC3385609E729C44777FA97EC97687BCBBDE7C3C9 (void);
// 0x00000052 System.Boolean MoreMountains.InventoryEngine.InventoryItem::IsNull(MoreMountains.InventoryEngine.InventoryItem)
extern void InventoryItem_IsNull_m0335EC480FD5315FDE70E3A00BD543BC857B55DA (void);
// 0x00000053 MoreMountains.InventoryEngine.InventoryItem MoreMountains.InventoryEngine.InventoryItem::Copy()
extern void InventoryItem_Copy_m63C35406FBB2D5ABB4648B5728A7720BCBB1407E (void);
// 0x00000054 System.Void MoreMountains.InventoryEngine.InventoryItem::SpawnPrefab(System.String)
extern void InventoryItem_SpawnPrefab_m0A56D11842F1C4F8DA3118654B804CB5618E7FD8 (void);
// 0x00000055 System.Boolean MoreMountains.InventoryEngine.InventoryItem::Pick(System.String)
extern void InventoryItem_Pick_m010FCA68C59013AD74CB876EB1F0BC308B2EA529 (void);
// 0x00000056 System.Boolean MoreMountains.InventoryEngine.InventoryItem::Use(System.String)
extern void InventoryItem_Use_m7A779F6DA2B9B7BBC9D0674DF6F8C222615DBF20 (void);
// 0x00000057 System.Boolean MoreMountains.InventoryEngine.InventoryItem::Equip(System.String)
extern void InventoryItem_Equip_mF7910C91F206C62FAC4BAC0C06478EF0D723E37A (void);
// 0x00000058 System.Boolean MoreMountains.InventoryEngine.InventoryItem::UnEquip(System.String)
extern void InventoryItem_UnEquip_m074921C7579A067FDE957FDA1D6CE4FBCD61368A (void);
// 0x00000059 System.Void MoreMountains.InventoryEngine.InventoryItem::Swap(System.String)
extern void InventoryItem_Swap_mD36D5CA699E6AF86CF431FC860A80FA4447BFF1C (void);
// 0x0000005A System.Boolean MoreMountains.InventoryEngine.InventoryItem::Drop(System.String)
extern void InventoryItem_Drop_m63C9A4FE3FB5269511F2A2F1F7028E41AB34328B (void);
// 0x0000005B System.Void MoreMountains.InventoryEngine.InventoryItem::.ctor()
extern void InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01 (void);
// 0x0000005C System.Void MoreMountains.InventoryEngine.ItemPicker::Start()
extern void ItemPicker_Start_m696A12FA7CBCA206D5FA6E3D204B0BA2F099CB7E (void);
// 0x0000005D System.Void MoreMountains.InventoryEngine.ItemPicker::Initialization()
extern void ItemPicker_Initialization_m850F639C7192CC5A54D9F940BEA597FAF08E322A (void);
// 0x0000005E System.Void MoreMountains.InventoryEngine.ItemPicker::OnTriggerEnter(UnityEngine.Collider)
extern void ItemPicker_OnTriggerEnter_mB80C958DCB79E57F283D03D191326B1F2A3A2490 (void);
// 0x0000005F System.Void MoreMountains.InventoryEngine.ItemPicker::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemPicker_OnTriggerEnter2D_mDAD0F620FE5F9015AC2D95F7BAF99AAF9B105258 (void);
// 0x00000060 System.Void MoreMountains.InventoryEngine.ItemPicker::Pick()
extern void ItemPicker_Pick_mC9DC714B7D08D743CF65C192907C928F435F4CF1 (void);
// 0x00000061 System.Void MoreMountains.InventoryEngine.ItemPicker::Pick(System.String,System.String)
extern void ItemPicker_Pick_mF400AD17E6BD63FEA6AAB8117B7CD1A8F309A3F4 (void);
// 0x00000062 System.Void MoreMountains.InventoryEngine.ItemPicker::PickSuccess()
extern void ItemPicker_PickSuccess_mFA49FB96AF49F6E2F5C7AE114C0DF5FCDCFDE181 (void);
// 0x00000063 System.Void MoreMountains.InventoryEngine.ItemPicker::PickFail()
extern void ItemPicker_PickFail_m04E1B9CBBA1DE618090679C3817E7F01C813ADAD (void);
// 0x00000064 System.Void MoreMountains.InventoryEngine.ItemPicker::DisableObjectIfNeeded()
extern void ItemPicker_DisableObjectIfNeeded_mF04545D7F39872925F8493C1BE6BCE6595C5CBEE (void);
// 0x00000065 System.Void MoreMountains.InventoryEngine.ItemPicker::DetermineMaxQuantity()
extern void ItemPicker_DetermineMaxQuantity_mDD4EFF9C0D207D790272224F84824E66720CE61C (void);
// 0x00000066 System.Boolean MoreMountains.InventoryEngine.ItemPicker::Pickable()
extern void ItemPicker_Pickable_mD28816CD4F84B0237679B6CA6D90C21A79D4F5B0 (void);
// 0x00000067 System.Void MoreMountains.InventoryEngine.ItemPicker::FindTargetInventory(System.String,System.String)
extern void ItemPicker_FindTargetInventory_m67CD8A264ABAE6D8CD2C5E491F648F4984EA900D (void);
// 0x00000068 System.Void MoreMountains.InventoryEngine.ItemPicker::.ctor()
extern void ItemPicker__ctor_mAF7AF572BC79860D43E20616CCB7400931FF4473 (void);
// 0x00000069 System.Void MoreMountains.InventoryEngine.SerializedInventory::.ctor()
extern void SerializedInventory__ctor_mA821BEE6F3F5A2F09191C19D8C8B290221B3F776 (void);
// 0x0000006A System.Boolean MoreMountains.InventoryEngine.InventoryDetails::get_Hidden()
extern void InventoryDetails_get_Hidden_mCE81253E9F4208C1F5F2BEC300BEB2B5CCA66226 (void);
// 0x0000006B System.Void MoreMountains.InventoryEngine.InventoryDetails::set_Hidden(System.Boolean)
extern void InventoryDetails_set_Hidden_m9AD3E32E93E637B972DBB06255E347C2E9092A12 (void);
// 0x0000006C System.Void MoreMountains.InventoryEngine.InventoryDetails::Start()
extern void InventoryDetails_Start_m001C2770DDABCFBEBD243E68B0853ACF92F12E62 (void);
// 0x0000006D System.Void MoreMountains.InventoryEngine.InventoryDetails::DisplayDetails(MoreMountains.InventoryEngine.InventoryItem)
extern void InventoryDetails_DisplayDetails_mBEA6002FC95AF2BF51607A2FA191C4AA3E41B1F5 (void);
// 0x0000006E System.Collections.IEnumerator MoreMountains.InventoryEngine.InventoryDetails::FillDetailFields(MoreMountains.InventoryEngine.InventoryItem,System.Single)
extern void InventoryDetails_FillDetailFields_m28C0F7CDA206A0D61631B6B534618C56A9390BFE (void);
// 0x0000006F System.Collections.IEnumerator MoreMountains.InventoryEngine.InventoryDetails::FillDetailFieldsWithDefaults(System.Single)
extern void InventoryDetails_FillDetailFieldsWithDefaults_m7E866BA1CC220EF12FB1DC643D40D26595FC798E (void);
// 0x00000070 System.Void MoreMountains.InventoryEngine.InventoryDetails::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void InventoryDetails_OnMMEvent_m784775BDBA947C42D7406FBD90A6151D2BFAC106 (void);
// 0x00000071 System.Void MoreMountains.InventoryEngine.InventoryDetails::OnEnable()
extern void InventoryDetails_OnEnable_m8A9182A4ED9B006872CDBC8ACBB398BE221B5970 (void);
// 0x00000072 System.Void MoreMountains.InventoryEngine.InventoryDetails::OnDisable()
extern void InventoryDetails_OnDisable_m93AE84DEAC1AD06D8ECCD2342D0486FDA7CDE123 (void);
// 0x00000073 System.Void MoreMountains.InventoryEngine.InventoryDetails::.ctor()
extern void InventoryDetails__ctor_m254BF58D79AEF1C8A3A4664C05671C99D2B5A649 (void);
// 0x00000074 System.Void MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFields>d__23::.ctor(System.Int32)
extern void U3CFillDetailFieldsU3Ed__23__ctor_m30FD6F764B454974959D118019025E6BDCF52937 (void);
// 0x00000075 System.Void MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFields>d__23::System.IDisposable.Dispose()
extern void U3CFillDetailFieldsU3Ed__23_System_IDisposable_Dispose_mB1DC5B14A9DF711E14850109BAABF4C60E420B6B (void);
// 0x00000076 System.Boolean MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFields>d__23::MoveNext()
extern void U3CFillDetailFieldsU3Ed__23_MoveNext_m31BCB89E543B1C74E8C5DB1FFB08B6B315E14699 (void);
// 0x00000077 System.Object MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFields>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFillDetailFieldsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m368AA5564217C2D7DE19BD1CB4C88E7D5282B76B (void);
// 0x00000078 System.Void MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFields>d__23::System.Collections.IEnumerator.Reset()
extern void U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_Reset_m713299BF6202DADB0DA364E926981B4A8DAB8C4E (void);
// 0x00000079 System.Object MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFields>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_get_Current_m4F2A03610ECA92C775228D6821CC7DFDBB601EA1 (void);
// 0x0000007A System.Void MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFieldsWithDefaults>d__24::.ctor(System.Int32)
extern void U3CFillDetailFieldsWithDefaultsU3Ed__24__ctor_m1F16C86295811C4D6AAC91674C187CCC050291B1 (void);
// 0x0000007B System.Void MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFieldsWithDefaults>d__24::System.IDisposable.Dispose()
extern void U3CFillDetailFieldsWithDefaultsU3Ed__24_System_IDisposable_Dispose_m76EA674ABC0192E325573EC331D4BFBD827C90A8 (void);
// 0x0000007C System.Boolean MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFieldsWithDefaults>d__24::MoveNext()
extern void U3CFillDetailFieldsWithDefaultsU3Ed__24_MoveNext_mE101C0FD68AB61EA962609428B6BFE022EF63A31 (void);
// 0x0000007D System.Object MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFieldsWithDefaults>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8D81549F524FC8A566A13D76A26F0C2BCBBA4CD (void);
// 0x0000007E System.Void MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFieldsWithDefaults>d__24::System.Collections.IEnumerator.Reset()
extern void U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_Reset_mC5189493E7C2E1A47226CC7DBB9BDB5EA1711A24 (void);
// 0x0000007F System.Object MoreMountains.InventoryEngine.InventoryDetails/<FillDetailFieldsWithDefaults>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_get_Current_m0F3E0BF9F8914A43406764CD643B3E857434AD2C (void);
// 0x00000080 MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryDisplay::get_TargetInventory()
extern void InventoryDisplay_get_TargetInventory_m454C72A0FEB4A3435D1489B16C2B156D700CFC37 (void);
// 0x00000081 System.Int32 MoreMountains.InventoryEngine.InventoryDisplay::get_InventorySize()
extern void InventoryDisplay_get_InventorySize_m9C98C1D5C4FD5261A016B311B5B44A5843961AF4 (void);
// 0x00000082 System.Void MoreMountains.InventoryEngine.InventoryDisplay::set_InventorySize(System.Int32)
extern void InventoryDisplay_set_InventorySize_m34C38B4108E43E79D19FAC9B554D501D1F276544 (void);
// 0x00000083 UnityEngine.UI.GridLayoutGroup MoreMountains.InventoryEngine.InventoryDisplay::get_InventoryGrid()
extern void InventoryDisplay_get_InventoryGrid_m4E69ECE8EF1203241368A1E3FAB2B81D92BA91F1 (void);
// 0x00000084 System.Void MoreMountains.InventoryEngine.InventoryDisplay::set_InventoryGrid(UnityEngine.UI.GridLayoutGroup)
extern void InventoryDisplay_set_InventoryGrid_mDFD36F2E09257D3CA0323B7A9F0000A828B13597 (void);
// 0x00000085 MoreMountains.InventoryEngine.InventoryDisplayTitle MoreMountains.InventoryEngine.InventoryDisplay::get_InventoryTitle()
extern void InventoryDisplay_get_InventoryTitle_m5B57607A8D6E4EF0FAF5EAB02D3F08995899588F (void);
// 0x00000086 System.Void MoreMountains.InventoryEngine.InventoryDisplay::set_InventoryTitle(MoreMountains.InventoryEngine.InventoryDisplayTitle)
extern void InventoryDisplay_set_InventoryTitle_m6630BC853FD5B0CE7BDF9A74CF50037D201845D8 (void);
// 0x00000087 UnityEngine.RectTransform MoreMountains.InventoryEngine.InventoryDisplay::get_InventoryRectTransform()
extern void InventoryDisplay_get_InventoryRectTransform_mE738A8697B3F9DC76F44EEA4B475CB89A3BF7611 (void);
// 0x00000088 System.Collections.Generic.List`1<MoreMountains.InventoryEngine.InventorySlot> MoreMountains.InventoryEngine.InventoryDisplay::get_SlotContainer()
extern void InventoryDisplay_get_SlotContainer_mCBDA7C6985A5BBB5097809A1CE1FDC236ED44C7A (void);
// 0x00000089 System.Void MoreMountains.InventoryEngine.InventoryDisplay::set_SlotContainer(System.Collections.Generic.List`1<MoreMountains.InventoryEngine.InventorySlot>)
extern void InventoryDisplay_set_SlotContainer_m8CC56240D696518E779B8F2E5BC36EE57D7D73E2 (void);
// 0x0000008A MoreMountains.InventoryEngine.InventoryDisplay MoreMountains.InventoryEngine.InventoryDisplay::get_ReturnInventory()
extern void InventoryDisplay_get_ReturnInventory_m6689389CB4D14E29E9CA8033F5D98BDEE29981B5 (void);
// 0x0000008B System.Void MoreMountains.InventoryEngine.InventoryDisplay::set_ReturnInventory(MoreMountains.InventoryEngine.InventoryDisplay)
extern void InventoryDisplay_set_ReturnInventory_m93121DC8B33F8C67FA13434D5409621D2242E937 (void);
// 0x0000008C System.Boolean MoreMountains.InventoryEngine.InventoryDisplay::get_IsOpen()
extern void InventoryDisplay_get_IsOpen_mE5A66A580266D5B8DDE024EBA7EDB8C5CBC385EC (void);
// 0x0000008D System.Void MoreMountains.InventoryEngine.InventoryDisplay::set_IsOpen(System.Boolean)
extern void InventoryDisplay_set_IsOpen_m83EC1F7DEAE4E995F250F127E29F310A36763017 (void);
// 0x0000008E System.Void MoreMountains.InventoryEngine.InventoryDisplay::SetupInventoryDisplay()
extern void InventoryDisplay_SetupInventoryDisplay_mC604B5D6A51F86B4DAE053D7639E39278E80B21D (void);
// 0x0000008F System.Void MoreMountains.InventoryEngine.InventoryDisplay::Awake()
extern void InventoryDisplay_Awake_mECAB07600E1F425B56E2D44C8883DAABB5EF9E11 (void);
// 0x00000090 System.Void MoreMountains.InventoryEngine.InventoryDisplay::RedrawInventoryDisplay()
extern void InventoryDisplay_RedrawInventoryDisplay_m74E336860F313C49A47546CFDA19571BEF4F4056 (void);
// 0x00000091 System.Void MoreMountains.InventoryEngine.InventoryDisplay::InitializeSprites()
extern void InventoryDisplay_InitializeSprites_m7A68D62FAD8D67AC292A014537ACB7E3F036865D (void);
// 0x00000092 System.Void MoreMountains.InventoryEngine.InventoryDisplay::DrawInventoryTitle()
extern void InventoryDisplay_DrawInventoryTitle_mC10078E0A97AA17BD7BACA98DC3A5658801A0CF8 (void);
// 0x00000093 System.Void MoreMountains.InventoryEngine.InventoryDisplay::AddGridLayoutGroup()
extern void InventoryDisplay_AddGridLayoutGroup_m432557A31BD4FB505C9F76871E9CD23638E46922 (void);
// 0x00000094 System.Void MoreMountains.InventoryEngine.InventoryDisplay::ResizeInventoryDisplay()
extern void InventoryDisplay_ResizeInventoryDisplay_mBABCBD685FBD11B56385B0B0021E155243787CDA (void);
// 0x00000095 System.Void MoreMountains.InventoryEngine.InventoryDisplay::DrawInventoryContent()
extern void InventoryDisplay_DrawInventoryContent_mA735D726B517D67D7C47105482C1A7CFA09DDA1D (void);
// 0x00000096 System.Void MoreMountains.InventoryEngine.InventoryDisplay::ContentHasChanged()
extern void InventoryDisplay_ContentHasChanged_m4C0E399C841EB10A4328A11A9AC0BE2DB3416B4B (void);
// 0x00000097 System.Void MoreMountains.InventoryEngine.InventoryDisplay::FillLastUpdateContent()
extern void InventoryDisplay_FillLastUpdateContent_m035F3F0773DFDB60B9A0717A09FF0D5186B8DC4A (void);
// 0x00000098 System.Void MoreMountains.InventoryEngine.InventoryDisplay::UpdateInventoryContent()
extern void InventoryDisplay_UpdateInventoryContent_m88E90335C699E2BA8CD8D7CAB4270D03FF8683FE (void);
// 0x00000099 System.Void MoreMountains.InventoryEngine.InventoryDisplay::UpdateSlot(System.Int32)
extern void InventoryDisplay_UpdateSlot_m838C5DE6CB682B25491FBF4AAA4169AE92F30152 (void);
// 0x0000009A System.Void MoreMountains.InventoryEngine.InventoryDisplay::InitializeSlotPrefab()
extern void InventoryDisplay_InitializeSlotPrefab_m412AA7BF8329B9506906C9E2E0CDD5A14DFC4659 (void);
// 0x0000009B System.Void MoreMountains.InventoryEngine.InventoryDisplay::DrawSlot(System.Int32)
extern void InventoryDisplay_DrawSlot_m96FF41AD01768FF87341D3006D52D17798FAD1AA (void);
// 0x0000009C System.Void MoreMountains.InventoryEngine.InventoryDisplay::SetupSlotNavigation()
extern void InventoryDisplay_SetupSlotNavigation_mECBF46FE9DC5E118B78684558C25DA8A7A3FE5BF (void);
// 0x0000009D System.Void MoreMountains.InventoryEngine.InventoryDisplay::Focus()
extern void InventoryDisplay_Focus_mB12028C4D5BDB33B43DAF5D4FCC4A94185F46506 (void);
// 0x0000009E MoreMountains.InventoryEngine.InventorySlot MoreMountains.InventoryEngine.InventoryDisplay::CurrentlySelectedInventorySlot()
extern void InventoryDisplay_CurrentlySelectedInventorySlot_m9C56046AE467B2496BB269E46D65B187B70F47A5 (void);
// 0x0000009F System.Void MoreMountains.InventoryEngine.InventoryDisplay::SetCurrentlySelectedSlot(MoreMountains.InventoryEngine.InventorySlot)
extern void InventoryDisplay_SetCurrentlySelectedSlot_m10F06D1F9DFE5F9B2272ACA9F7BCB80CC4C0FD5E (void);
// 0x000000A0 MoreMountains.InventoryEngine.InventoryDisplay MoreMountains.InventoryEngine.InventoryDisplay::GoToInventory(System.Int32)
extern void InventoryDisplay_GoToInventory_m9421C0B9598A4E8BA41D4A886FB6C1D38410DB74 (void);
// 0x000000A1 System.Void MoreMountains.InventoryEngine.InventoryDisplay::SetReturnInventory(MoreMountains.InventoryEngine.InventoryDisplay)
extern void InventoryDisplay_SetReturnInventory_mB0A923E1810ACD286109A21A606AC46DA377C19C (void);
// 0x000000A2 System.Void MoreMountains.InventoryEngine.InventoryDisplay::ReturnInventoryFocus()
extern void InventoryDisplay_ReturnInventoryFocus_m7C7ECF0309D967F57CA8A209A5B46A40067577EC (void);
// 0x000000A3 System.Void MoreMountains.InventoryEngine.InventoryDisplay::DisableAllBut(MoreMountains.InventoryEngine.ItemClasses)
extern void InventoryDisplay_DisableAllBut_mBB5D6FFC966E95EE98BD154AF7E0299E853A5C4F (void);
// 0x000000A4 System.Void MoreMountains.InventoryEngine.InventoryDisplay::ResetDisabledStates()
extern void InventoryDisplay_ResetDisabledStates_mD7541C14DBB52399F19DA4EEE4313119363F8EF1 (void);
// 0x000000A5 System.Void MoreMountains.InventoryEngine.InventoryDisplay::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void InventoryDisplay_OnMMEvent_m22C4290CEA93D36122AF9A26FF48365DAF5A8969 (void);
// 0x000000A6 System.Void MoreMountains.InventoryEngine.InventoryDisplay::OnEnable()
extern void InventoryDisplay_OnEnable_m12078B90CCEB6D62660962C670E4DFA55BFEDAAB (void);
// 0x000000A7 System.Void MoreMountains.InventoryEngine.InventoryDisplay::OnDisable()
extern void InventoryDisplay_OnDisable_m1ED8FAC633917D3B2118D17AAE62DDF8A1CF993D (void);
// 0x000000A8 System.Void MoreMountains.InventoryEngine.InventoryDisplay::.ctor()
extern void InventoryDisplay__ctor_m1AA25A57B8F4A3E955DC65A5C26666EAF842BD8C (void);
// 0x000000A9 System.Void MoreMountains.InventoryEngine.InventoryDisplay/ItemQuantity::.ctor(System.String,System.Int32)
extern void ItemQuantity__ctor_m416007BCA62D9EBC2C9E04712A29E64D7BABCD83 (void);
// 0x000000AA System.Void MoreMountains.InventoryEngine.InventoryDisplayGrid::.ctor()
extern void InventoryDisplayGrid__ctor_m3B7838EEE2A3557628E999FBC7A83C0CD617B90F (void);
// 0x000000AB System.Void MoreMountains.InventoryEngine.InventoryDisplayTitle::.ctor()
extern void InventoryDisplayTitle__ctor_m86BEEEF4991302D72EE45F8DE65BA37354153784 (void);
// 0x000000AC System.Void MoreMountains.InventoryEngine.InventoryHotbar::Action()
extern void InventoryHotbar_Action_mE74024E2B6CBADE45CD60230C193308164561B6A (void);
// 0x000000AD System.Void MoreMountains.InventoryEngine.InventoryHotbar::.ctor()
extern void InventoryHotbar__ctor_m86F0FAEC1AABD227DD986EBD32B14F4DFB8C8D4A (void);
// 0x000000AE System.Void MoreMountains.InventoryEngine.InventorySelectionMarker::Start()
extern void InventorySelectionMarker_Start_mCA94990932E98D5EF30396B1AF1E7E63561228B6 (void);
// 0x000000AF System.Void MoreMountains.InventoryEngine.InventorySelectionMarker::Update()
extern void InventorySelectionMarker_Update_m0DCF41A41FC05FF9FBF8EF70D5ADEDBF09CB337F (void);
// 0x000000B0 System.Void MoreMountains.InventoryEngine.InventorySelectionMarker::.ctor()
extern void InventorySelectionMarker__ctor_m0150D44AE4DD1EB7374B0A78FBE00E3B50D62221 (void);
// 0x000000B1 System.Void MoreMountains.InventoryEngine.InventorySlot::Awake()
extern void InventorySlot_Awake_m18C81734F9B73154152710DAB6FC41551B2315F2 (void);
// 0x000000B2 System.Void MoreMountains.InventoryEngine.InventorySlot::Start()
extern void InventorySlot_Start_m4B8425375E78375AD49DE8F829B868C42833BD29 (void);
// 0x000000B3 System.Void MoreMountains.InventoryEngine.InventorySlot::DrawIcon(MoreMountains.InventoryEngine.InventoryItem,System.Int32)
extern void InventorySlot_DrawIcon_m8EA474B4FC023DEEC44F7A5D0790407154BBAB71 (void);
// 0x000000B4 System.Void MoreMountains.InventoryEngine.InventorySlot::SetIcon(UnityEngine.Sprite)
extern void InventorySlot_SetIcon_m2913043BAB8139176B295890D9B32B3F42855A11 (void);
// 0x000000B5 System.Void MoreMountains.InventoryEngine.InventorySlot::SetQuantity(System.Int32)
extern void InventorySlot_SetQuantity_m42015A479B9790FD631F8846E4CB61DA8DD21A3E (void);
// 0x000000B6 System.Void MoreMountains.InventoryEngine.InventorySlot::DisableIconAndQuantity()
extern void InventorySlot_DisableIconAndQuantity_m016E167387EA082CD39E7F74886444E85DB9CCE7 (void);
// 0x000000B7 System.Void MoreMountains.InventoryEngine.InventorySlot::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void InventorySlot_OnSelect_m1E1714097AE6E3B657AA4022224BEFD3EE6A0A41 (void);
// 0x000000B8 System.Void MoreMountains.InventoryEngine.InventorySlot::SlotClicked()
extern void InventorySlot_SlotClicked_m0365840A5FF982A6F0400FB4D77232549DD44F8F (void);
// 0x000000B9 System.Void MoreMountains.InventoryEngine.InventorySlot::Move()
extern void InventorySlot_Move_m01354D64D6EF412DBDF89F72F9F2AF8877A395C2 (void);
// 0x000000BA System.Void MoreMountains.InventoryEngine.InventorySlot::Use()
extern void InventorySlot_Use_mC1C61E7FDF185784B928F21D9DFFB42ADBF9904C (void);
// 0x000000BB System.Void MoreMountains.InventoryEngine.InventorySlot::Equip()
extern void InventorySlot_Equip_m7D94C8C981A643BF2395453AB6694F0D1F1ADA46 (void);
// 0x000000BC System.Void MoreMountains.InventoryEngine.InventorySlot::UnEquip()
extern void InventorySlot_UnEquip_mA020DCE0F759D85CD1BE6E985B7F91F27C987D17 (void);
// 0x000000BD System.Void MoreMountains.InventoryEngine.InventorySlot::Drop()
extern void InventorySlot_Drop_m023B4529B18DE76F8FAA977E7B2A608C022F1B35 (void);
// 0x000000BE System.Void MoreMountains.InventoryEngine.InventorySlot::DisableSlot()
extern void InventorySlot_DisableSlot_m6075A40F6D1058C78F1A6D42A4F90F96F4B97E62 (void);
// 0x000000BF System.Void MoreMountains.InventoryEngine.InventorySlot::EnableSlot()
extern void InventorySlot_EnableSlot_mB9D1287D7876AD9549912443425BA0C4F4025EF3 (void);
// 0x000000C0 System.Boolean MoreMountains.InventoryEngine.InventorySlot::Equippable()
extern void InventorySlot_Equippable_mC319798418CE3332496E2F2965F623511F8C04F6 (void);
// 0x000000C1 System.Boolean MoreMountains.InventoryEngine.InventorySlot::Usable()
extern void InventorySlot_Usable_m92CFB19362F8E84044956C62E5EFDA018E1FCBBB (void);
// 0x000000C2 System.Boolean MoreMountains.InventoryEngine.InventorySlot::Movable()
extern void InventorySlot_Movable_m9C79721D4B6CEC1F4C1D6D7E89B2A2E3AAF43993 (void);
// 0x000000C3 System.Boolean MoreMountains.InventoryEngine.InventorySlot::Droppable()
extern void InventorySlot_Droppable_m59BA356C9785B329365070C2963ED1D60526247B (void);
// 0x000000C4 System.Boolean MoreMountains.InventoryEngine.InventorySlot::Unequippable()
extern void InventorySlot_Unequippable_m5FC7CC6BB0FA146EFDF266FAFE4AC6D4BC835195 (void);
// 0x000000C5 System.Void MoreMountains.InventoryEngine.InventorySlot::.ctor()
extern void InventorySlot__ctor_mE2A8E3676FB184BFF2FC057F5ED03999DE1CB59F (void);
// 0x000000C6 System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::Start()
extern void InventorySoundPlayer_Start_m84724F2734D39520067C3B6BEBBDDE2C2CFC464D (void);
// 0x000000C7 System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::SetupInventorySoundPlayer()
extern void InventorySoundPlayer_SetupInventorySoundPlayer_m10AC35BD5806782A59376F2E50014FD0DDF936F4 (void);
// 0x000000C8 System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::AddAudioSource()
extern void InventorySoundPlayer_AddAudioSource_m28619391F10E548DB2EE9266F1248365AAF1A9CF (void);
// 0x000000C9 System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::PlaySound(System.String)
extern void InventorySoundPlayer_PlaySound_m0A9C10F0E8F69CA3FCDBEF071AE70E35C82D7498 (void);
// 0x000000CA System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::PlaySound(UnityEngine.AudioClip,System.Single)
extern void InventorySoundPlayer_PlaySound_mC5D95B1EEA80448EBD742F5DC9E8BA3EAD8ED58F (void);
// 0x000000CB System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void InventorySoundPlayer_OnMMEvent_m34BA0DB526E47E4AC537103846D936901F5D9BB2 (void);
// 0x000000CC System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::OnEnable()
extern void InventorySoundPlayer_OnEnable_m4490ECDC303B336932FE7ACE408B427E8EBA2C2E (void);
// 0x000000CD System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::OnDisable()
extern void InventorySoundPlayer_OnDisable_mAD4D313F8EBEE6A5F7BFCD160CB6CEC30627E402 (void);
// 0x000000CE System.Void MoreMountains.InventoryEngine.InventorySoundPlayer::.ctor()
extern void InventorySoundPlayer__ctor_m8CCAEDF3FE2A846F6FAE934E2D04841678C632AA (void);
// 0x000000CF System.Void MoreMountains.InventoryEngine.BaseItem::.ctor()
extern void BaseItem__ctor_m8D346286FB04A1650D19852D8414300CBBFB15E1 (void);
// 0x000000D0 System.Void MoreMountains.InventoryEngine.InventoryTester::AddItemTest()
extern void InventoryTester_AddItemTest_m310CE520788F3A6B81938C124D442B02B59EDBDD (void);
// 0x000000D1 System.Void MoreMountains.InventoryEngine.InventoryTester::AddItemAtTest()
extern void InventoryTester_AddItemAtTest_m811E21A0D8FA6726BC622E92BE3FE79645DFE6C6 (void);
// 0x000000D2 System.Void MoreMountains.InventoryEngine.InventoryTester::MoveItemTest()
extern void InventoryTester_MoveItemTest_mA8BC837E1BEA25CBC3F9F5E67DE09A2679AE7D84 (void);
// 0x000000D3 System.Void MoreMountains.InventoryEngine.InventoryTester::MoveItemToInventory()
extern void InventoryTester_MoveItemToInventory_m947937188D171E2C59C6FD98C896563A086C6D84 (void);
// 0x000000D4 System.Void MoreMountains.InventoryEngine.InventoryTester::RemoveItemTest()
extern void InventoryTester_RemoveItemTest_mD84DD558866924406CD343A36DFE64334A91629C (void);
// 0x000000D5 System.Void MoreMountains.InventoryEngine.InventoryTester::EmptyInventoryTest()
extern void InventoryTester_EmptyInventoryTest_m935E8667DB80CEB2AC52FF32BCF97DB9635ECA82 (void);
// 0x000000D6 System.Void MoreMountains.InventoryEngine.InventoryTester::.ctor()
extern void InventoryTester__ctor_m03B6FF27BCC837C20FE1FD19444322E336CE6F90 (void);
// 0x000000D7 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mCCF04A7FCB7DDD3D5C8F8959347CCE34B5FB30BE (void);
static Il2CppMethodPointer s_methodPointers[215] = 
{
	EventTester_OnMMEvent_mF92B414CD5422F9CFFFAE25EC9018746E80E2BC3,
	EventTester_OnEnable_mBFC8F3776EACF1586CE4F47F51D3EEF1EE35183E,
	EventTester_OnDisable_m072D526C9DF1C1F17793E1353A4AF80FA485D651,
	EventTester__ctor_mB54C11E9EF22D875D991DD0FB4234D80163114D8,
	Inventory_get_Owner_mE01A17C278F484A328297B5708967E25C9795D25,
	Inventory_set_Owner_m676D8E71B989E750CF110AB1DE22944089FB2B39,
	Inventory_get_NumberOfFreeSlots_mC3201F8417819104CB608598AF712BFE7E2DCEE5,
	Inventory_get_NumberOfFilledSlots_mB04B9C73858D05F7CC34E28888AAAEAD68D4E1F0,
	Inventory_NumberOfStackableSlots_m567626C28C2F43871517B511174FFB84080A7DE3,
	Inventory_FindInventory_m38D814B1C53E9544BE319693FDD581ECE248E854,
	Inventory_Awake_mD5A36301D4B209847958CB86F0ADE635E4C1A519,
	Inventory_RegisterInventory_mE0B0410E5B78D97B5BC39179EF96F3288776E405,
	Inventory_SetOwner_mB217EB475226AA95136D55A1A4B53402A4D357AF,
	Inventory_AddItem_mEA9FCAA7D7910B677678C71F4C2F56C4B69C15C9,
	Inventory_AddItemAt_mDBE8BFBD1B76AA1CAEBE56E5399467E28C69796C,
	Inventory_MoveItem_m3CE8F497BE26B387D40AEE8E10E6FF097128FE26,
	Inventory_MoveItemToInventory_m939B9891EF2384C0DEC431EF566721C5E98AF5B7,
	Inventory_RemoveItem_m9A0FF0AB1E1F6C974CCBDBBBD8780D14A2F31FCB,
	Inventory_RemoveItemByID_mB36E972F2401C4193C6B08D3F15AEF55CE3EB6B0,
	Inventory_DestroyItem_mE59C28BA96B1A6323ED1D9269725F46215455667,
	Inventory_EmptyInventory_m44F479C47DBB48F377FEE164D749B19A3BF9508A,
	Inventory_AddItemToArray_mB09D8A3CA4065491C7793BCBFD0F96B5219F065E,
	Inventory_RemoveItemFromArray_m56A9D9FBBBC07EB06BBD2A5876348190E1F07DC2,
	Inventory_ResizeArray_mD298A2AE200112335D5599912901F1EA6DA94CC9,
	Inventory_GetQuantity_m8C4C0F188C23AE701D9BC03AC7075F2ED98AEAE7,
	Inventory_InventoryContains_m08B4257DE759D7C7FEB53AD1D8EE73B45FC8253E,
	Inventory_InventoryContains_m98E68473557C6D05A2B5FC5328381BA256684669,
	Inventory_SaveInventory_m28E3C10D07D3121DCE0B53F40ABF526157E1E95A,
	Inventory_LoadSavedInventory_mD94C2093D5093EE6922E580548293B1668D78699,
	Inventory_FillSerializedInventory_mC5EB6C471663F5BAFB0A9E39814BC7518D1BFE7D,
	Inventory_ExtractSerializedInventory_m764021A6F7B57071E9D29553DE01E734A6768445,
	Inventory_DetermineSaveName_m3D46016AEEAC9DB0D0676784AD51BE69D3F18097,
	Inventory_ResetSavedInventory_m8DCFC812C38259CFCF5A65616EB5ED190EAEBA90,
	Inventory_UseItem_mA6817664748AA5DC60689E372A5A8D4641D1F831,
	Inventory_UseItem_m8C1A8A8685F9E4803CB340E34DA051B5396F287A,
	Inventory_EquipItem_m27DB599753763F1C1B57B02F255298DDF3473AB5,
	Inventory_DropItem_mB75B8B604D284D4E49AE564B27A6BEEBCEE01118,
	Inventory_DestroyItem_m2A588BE8753242823897BBA24C7310B46FFCC1A6,
	Inventory_UnEquipItem_mA5B4851209DE2608D440090EE75ACC2348F00E1B,
	Inventory_OnMMEvent_m5539D4B1C2A16366A6C97DD53C9BF7F0614E3A55,
	Inventory_OnMMEvent_mFF376C5CBC331556BDD28EA5D296C557B76D77F3,
	Inventory_OnEnable_mD958CFD2663A0698694BE6C27BF169E392752FDB,
	Inventory_OnDisable_mA592794C5E3CAEB288206A5A351F39980CB7B245,
	Inventory__ctor_m5135BCDC2311B5A29AF8661937DB5F8275F274F3,
	InventoryCharacterIdentifier__ctor_m20C762BFDE93F2535700AFBB69FEEA818289BD83,
	MMInventoryEvent__ctor_m8FA291F8E8B9BC49693B8F01CEEF6CBCA2F6A953,
	MMInventoryEvent_Trigger_m77BAD1039CC50ABAF4F9E3B7B45D201F034FE9B7,
	InventoryInputActions_get_TargetInventory_m061A817D01C593A793BF92EC1BA9C5C1FE45B1DA,
	InventoryInputActions_Start_m24E2A3265D73FD66B8BB60BF7CF7F5267B627BBE,
	InventoryInputActions_Initialization_m4A17B27A98BFBD7D11364973BCFA5C8661CF68E8,
	InventoryInputActions_Update_mB5C519F7EF3AFF6ED5F294FD9D0FD3D9D2B6F49D,
	InventoryInputActions_DetectInput_m4DCE327B0EE593564A66D6D45BEBEE1429EB5A93,
	InventoryInputActions_ExecuteAction_m85A9BAC88A85AA154095D89A63663FBD4671EA17,
	InventoryInputActions__ctor_m55FB3CE564413772FB97EEBEA4BF72B4B390FAC5,
	InventoryInputActionsBindings__ctor_m597BEE899605DA444EA955C0D9F5331747DFE4FF,
	InventoryInputManager_get_CurrentlySelectedInventorySlot_mE06883E42278A8B2A4DFF27879C4CFD2EF38AEC8,
	InventoryInputManager_set_CurrentlySelectedInventorySlot_m7651E8F8A7EF0159C3CC275FFFFA09F56DE108B4,
	InventoryInputManager_Start_m028A4BD0940DDBE740438CF8E1CF65AD4115457C,
	InventoryInputManager_Update_m197D4AA17B6FAD1769A489E363251D1121710A8D,
	InventoryInputManager_CheckCurrentlySelectedSlot_m44793586BE4FBFB6E1F24189EDF32FAC001D03DF,
	InventoryInputManager_HandleButtons_m68D53A055FE612693134694C39656BD595E8BB7A,
	InventoryInputManager_SetButtonState_m745030EF572FA4FB9824AADD228F8A51E410EC4F,
	InventoryInputManager_ToggleInventory_mFE9B9E69477B863061B855F8B1B8DC091FC8320A,
	InventoryInputManager_OpenInventory_m8E82922578FEA7439B9237AECC3256EC6C675BAD,
	InventoryInputManager_CloseInventory_m61E71EA02936E0139D55E82DDE4EA888D8163DAD,
	InventoryInputManager_HandleInventoryInput_mB5DBE2A5358C299460390FD318E599BF3A31298F,
	InventoryInputManager_HandleHotbarsInput_mF30D17BFC07B67AD7E6C7DA71EB7F3B8D70A0A42,
	InventoryInputManager_EquipOrUse_mD093ED2D924A06675772718CE7426D170E407245,
	InventoryInputManager_Equip_m571F83356D24D12E6573B28592D9464CC7DF3E55,
	InventoryInputManager_Use_m264F9A71B88CCC0E15F8D1E9087A7A2F29A34EE2,
	InventoryInputManager_UnEquip_mAEFA5421C46FB506A0D5FF3DB126A84190F995BA,
	InventoryInputManager_Move_m9B2E0E012B6D6B212D7EEF9B5428DCCD8408712C,
	InventoryInputManager_Drop_mF6F52A2D73FFC8D60F8F957F5DC60F1271398A69,
	InventoryInputManager_OnMMEvent_m9D220D6A2A2E481525289BC56B92EFD419A0164C,
	InventoryInputManager_OnEnable_m0C13CA6B356B1DD06BE1C8744473D72FA55B2385,
	InventoryInputManager_OnDisable_mCFD53FF0B86560C463A39C74C5D59C53D3D70597,
	InventoryInputManager__ctor_mB0787C44C06830BE9D0E903551DDB81C6AE12106,
	InventoryItem_get_IsUsable_m16380763D3EAA35A273839E385A674A141624491,
	InventoryItem_get_IsEquippable_m500FC17BDF833237433042CBD5C68D6451F2D1CC,
	InventoryItem_TargetInventory_mB0069D82520BD2E9BA11ED16E2FD652607A7983A,
	InventoryItem_TargetEquipmentInventory_mC3385609E729C44777FA97EC97687BCBBDE7C3C9,
	InventoryItem_IsNull_m0335EC480FD5315FDE70E3A00BD543BC857B55DA,
	InventoryItem_Copy_m63C35406FBB2D5ABB4648B5728A7720BCBB1407E,
	InventoryItem_SpawnPrefab_m0A56D11842F1C4F8DA3118654B804CB5618E7FD8,
	InventoryItem_Pick_m010FCA68C59013AD74CB876EB1F0BC308B2EA529,
	InventoryItem_Use_m7A779F6DA2B9B7BBC9D0674DF6F8C222615DBF20,
	InventoryItem_Equip_mF7910C91F206C62FAC4BAC0C06478EF0D723E37A,
	InventoryItem_UnEquip_m074921C7579A067FDE957FDA1D6CE4FBCD61368A,
	InventoryItem_Swap_mD36D5CA699E6AF86CF431FC860A80FA4447BFF1C,
	InventoryItem_Drop_m63C9A4FE3FB5269511F2A2F1F7028E41AB34328B,
	InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01,
	ItemPicker_Start_m696A12FA7CBCA206D5FA6E3D204B0BA2F099CB7E,
	ItemPicker_Initialization_m850F639C7192CC5A54D9F940BEA597FAF08E322A,
	ItemPicker_OnTriggerEnter_mB80C958DCB79E57F283D03D191326B1F2A3A2490,
	ItemPicker_OnTriggerEnter2D_mDAD0F620FE5F9015AC2D95F7BAF99AAF9B105258,
	ItemPicker_Pick_mC9DC714B7D08D743CF65C192907C928F435F4CF1,
	ItemPicker_Pick_mF400AD17E6BD63FEA6AAB8117B7CD1A8F309A3F4,
	ItemPicker_PickSuccess_mFA49FB96AF49F6E2F5C7AE114C0DF5FCDCFDE181,
	ItemPicker_PickFail_m04E1B9CBBA1DE618090679C3817E7F01C813ADAD,
	ItemPicker_DisableObjectIfNeeded_mF04545D7F39872925F8493C1BE6BCE6595C5CBEE,
	ItemPicker_DetermineMaxQuantity_mDD4EFF9C0D207D790272224F84824E66720CE61C,
	ItemPicker_Pickable_mD28816CD4F84B0237679B6CA6D90C21A79D4F5B0,
	ItemPicker_FindTargetInventory_m67CD8A264ABAE6D8CD2C5E491F648F4984EA900D,
	ItemPicker__ctor_mAF7AF572BC79860D43E20616CCB7400931FF4473,
	SerializedInventory__ctor_mA821BEE6F3F5A2F09191C19D8C8B290221B3F776,
	InventoryDetails_get_Hidden_mCE81253E9F4208C1F5F2BEC300BEB2B5CCA66226,
	InventoryDetails_set_Hidden_m9AD3E32E93E637B972DBB06255E347C2E9092A12,
	InventoryDetails_Start_m001C2770DDABCFBEBD243E68B0853ACF92F12E62,
	InventoryDetails_DisplayDetails_mBEA6002FC95AF2BF51607A2FA191C4AA3E41B1F5,
	InventoryDetails_FillDetailFields_m28C0F7CDA206A0D61631B6B534618C56A9390BFE,
	InventoryDetails_FillDetailFieldsWithDefaults_m7E866BA1CC220EF12FB1DC643D40D26595FC798E,
	InventoryDetails_OnMMEvent_m784775BDBA947C42D7406FBD90A6151D2BFAC106,
	InventoryDetails_OnEnable_m8A9182A4ED9B006872CDBC8ACBB398BE221B5970,
	InventoryDetails_OnDisable_m93AE84DEAC1AD06D8ECCD2342D0486FDA7CDE123,
	InventoryDetails__ctor_m254BF58D79AEF1C8A3A4664C05671C99D2B5A649,
	U3CFillDetailFieldsU3Ed__23__ctor_m30FD6F764B454974959D118019025E6BDCF52937,
	U3CFillDetailFieldsU3Ed__23_System_IDisposable_Dispose_mB1DC5B14A9DF711E14850109BAABF4C60E420B6B,
	U3CFillDetailFieldsU3Ed__23_MoveNext_m31BCB89E543B1C74E8C5DB1FFB08B6B315E14699,
	U3CFillDetailFieldsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m368AA5564217C2D7DE19BD1CB4C88E7D5282B76B,
	U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_Reset_m713299BF6202DADB0DA364E926981B4A8DAB8C4E,
	U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_get_Current_m4F2A03610ECA92C775228D6821CC7DFDBB601EA1,
	U3CFillDetailFieldsWithDefaultsU3Ed__24__ctor_m1F16C86295811C4D6AAC91674C187CCC050291B1,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_System_IDisposable_Dispose_m76EA674ABC0192E325573EC331D4BFBD827C90A8,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_MoveNext_mE101C0FD68AB61EA962609428B6BFE022EF63A31,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8D81549F524FC8A566A13D76A26F0C2BCBBA4CD,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_Reset_mC5189493E7C2E1A47226CC7DBB9BDB5EA1711A24,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_get_Current_m0F3E0BF9F8914A43406764CD643B3E857434AD2C,
	InventoryDisplay_get_TargetInventory_m454C72A0FEB4A3435D1489B16C2B156D700CFC37,
	InventoryDisplay_get_InventorySize_m9C98C1D5C4FD5261A016B311B5B44A5843961AF4,
	InventoryDisplay_set_InventorySize_m34C38B4108E43E79D19FAC9B554D501D1F276544,
	InventoryDisplay_get_InventoryGrid_m4E69ECE8EF1203241368A1E3FAB2B81D92BA91F1,
	InventoryDisplay_set_InventoryGrid_mDFD36F2E09257D3CA0323B7A9F0000A828B13597,
	InventoryDisplay_get_InventoryTitle_m5B57607A8D6E4EF0FAF5EAB02D3F08995899588F,
	InventoryDisplay_set_InventoryTitle_m6630BC853FD5B0CE7BDF9A74CF50037D201845D8,
	InventoryDisplay_get_InventoryRectTransform_mE738A8697B3F9DC76F44EEA4B475CB89A3BF7611,
	InventoryDisplay_get_SlotContainer_mCBDA7C6985A5BBB5097809A1CE1FDC236ED44C7A,
	InventoryDisplay_set_SlotContainer_m8CC56240D696518E779B8F2E5BC36EE57D7D73E2,
	InventoryDisplay_get_ReturnInventory_m6689389CB4D14E29E9CA8033F5D98BDEE29981B5,
	InventoryDisplay_set_ReturnInventory_m93121DC8B33F8C67FA13434D5409621D2242E937,
	InventoryDisplay_get_IsOpen_mE5A66A580266D5B8DDE024EBA7EDB8C5CBC385EC,
	InventoryDisplay_set_IsOpen_m83EC1F7DEAE4E995F250F127E29F310A36763017,
	InventoryDisplay_SetupInventoryDisplay_mC604B5D6A51F86B4DAE053D7639E39278E80B21D,
	InventoryDisplay_Awake_mECAB07600E1F425B56E2D44C8883DAABB5EF9E11,
	InventoryDisplay_RedrawInventoryDisplay_m74E336860F313C49A47546CFDA19571BEF4F4056,
	InventoryDisplay_InitializeSprites_m7A68D62FAD8D67AC292A014537ACB7E3F036865D,
	InventoryDisplay_DrawInventoryTitle_mC10078E0A97AA17BD7BACA98DC3A5658801A0CF8,
	InventoryDisplay_AddGridLayoutGroup_m432557A31BD4FB505C9F76871E9CD23638E46922,
	InventoryDisplay_ResizeInventoryDisplay_mBABCBD685FBD11B56385B0B0021E155243787CDA,
	InventoryDisplay_DrawInventoryContent_mA735D726B517D67D7C47105482C1A7CFA09DDA1D,
	InventoryDisplay_ContentHasChanged_m4C0E399C841EB10A4328A11A9AC0BE2DB3416B4B,
	InventoryDisplay_FillLastUpdateContent_m035F3F0773DFDB60B9A0717A09FF0D5186B8DC4A,
	InventoryDisplay_UpdateInventoryContent_m88E90335C699E2BA8CD8D7CAB4270D03FF8683FE,
	InventoryDisplay_UpdateSlot_m838C5DE6CB682B25491FBF4AAA4169AE92F30152,
	InventoryDisplay_InitializeSlotPrefab_m412AA7BF8329B9506906C9E2E0CDD5A14DFC4659,
	InventoryDisplay_DrawSlot_m96FF41AD01768FF87341D3006D52D17798FAD1AA,
	InventoryDisplay_SetupSlotNavigation_mECBF46FE9DC5E118B78684558C25DA8A7A3FE5BF,
	InventoryDisplay_Focus_mB12028C4D5BDB33B43DAF5D4FCC4A94185F46506,
	InventoryDisplay_CurrentlySelectedInventorySlot_m9C56046AE467B2496BB269E46D65B187B70F47A5,
	InventoryDisplay_SetCurrentlySelectedSlot_m10F06D1F9DFE5F9B2272ACA9F7BCB80CC4C0FD5E,
	InventoryDisplay_GoToInventory_m9421C0B9598A4E8BA41D4A886FB6C1D38410DB74,
	InventoryDisplay_SetReturnInventory_mB0A923E1810ACD286109A21A606AC46DA377C19C,
	InventoryDisplay_ReturnInventoryFocus_m7C7ECF0309D967F57CA8A209A5B46A40067577EC,
	InventoryDisplay_DisableAllBut_mBB5D6FFC966E95EE98BD154AF7E0299E853A5C4F,
	InventoryDisplay_ResetDisabledStates_mD7541C14DBB52399F19DA4EEE4313119363F8EF1,
	InventoryDisplay_OnMMEvent_m22C4290CEA93D36122AF9A26FF48365DAF5A8969,
	InventoryDisplay_OnEnable_m12078B90CCEB6D62660962C670E4DFA55BFEDAAB,
	InventoryDisplay_OnDisable_m1ED8FAC633917D3B2118D17AAE62DDF8A1CF993D,
	InventoryDisplay__ctor_m1AA25A57B8F4A3E955DC65A5C26666EAF842BD8C,
	ItemQuantity__ctor_m416007BCA62D9EBC2C9E04712A29E64D7BABCD83,
	InventoryDisplayGrid__ctor_m3B7838EEE2A3557628E999FBC7A83C0CD617B90F,
	InventoryDisplayTitle__ctor_m86BEEEF4991302D72EE45F8DE65BA37354153784,
	InventoryHotbar_Action_mE74024E2B6CBADE45CD60230C193308164561B6A,
	InventoryHotbar__ctor_m86F0FAEC1AABD227DD986EBD32B14F4DFB8C8D4A,
	InventorySelectionMarker_Start_mCA94990932E98D5EF30396B1AF1E7E63561228B6,
	InventorySelectionMarker_Update_m0DCF41A41FC05FF9FBF8EF70D5ADEDBF09CB337F,
	InventorySelectionMarker__ctor_m0150D44AE4DD1EB7374B0A78FBE00E3B50D62221,
	InventorySlot_Awake_m18C81734F9B73154152710DAB6FC41551B2315F2,
	InventorySlot_Start_m4B8425375E78375AD49DE8F829B868C42833BD29,
	InventorySlot_DrawIcon_m8EA474B4FC023DEEC44F7A5D0790407154BBAB71,
	InventorySlot_SetIcon_m2913043BAB8139176B295890D9B32B3F42855A11,
	InventorySlot_SetQuantity_m42015A479B9790FD631F8846E4CB61DA8DD21A3E,
	InventorySlot_DisableIconAndQuantity_m016E167387EA082CD39E7F74886444E85DB9CCE7,
	InventorySlot_OnSelect_m1E1714097AE6E3B657AA4022224BEFD3EE6A0A41,
	InventorySlot_SlotClicked_m0365840A5FF982A6F0400FB4D77232549DD44F8F,
	InventorySlot_Move_m01354D64D6EF412DBDF89F72F9F2AF8877A395C2,
	InventorySlot_Use_mC1C61E7FDF185784B928F21D9DFFB42ADBF9904C,
	InventorySlot_Equip_m7D94C8C981A643BF2395453AB6694F0D1F1ADA46,
	InventorySlot_UnEquip_mA020DCE0F759D85CD1BE6E985B7F91F27C987D17,
	InventorySlot_Drop_m023B4529B18DE76F8FAA977E7B2A608C022F1B35,
	InventorySlot_DisableSlot_m6075A40F6D1058C78F1A6D42A4F90F96F4B97E62,
	InventorySlot_EnableSlot_mB9D1287D7876AD9549912443425BA0C4F4025EF3,
	InventorySlot_Equippable_mC319798418CE3332496E2F2965F623511F8C04F6,
	InventorySlot_Usable_m92CFB19362F8E84044956C62E5EFDA018E1FCBBB,
	InventorySlot_Movable_m9C79721D4B6CEC1F4C1D6D7E89B2A2E3AAF43993,
	InventorySlot_Droppable_m59BA356C9785B329365070C2963ED1D60526247B,
	InventorySlot_Unequippable_m5FC7CC6BB0FA146EFDF266FAFE4AC6D4BC835195,
	InventorySlot__ctor_mE2A8E3676FB184BFF2FC057F5ED03999DE1CB59F,
	InventorySoundPlayer_Start_m84724F2734D39520067C3B6BEBBDDE2C2CFC464D,
	InventorySoundPlayer_SetupInventorySoundPlayer_m10AC35BD5806782A59376F2E50014FD0DDF936F4,
	InventorySoundPlayer_AddAudioSource_m28619391F10E548DB2EE9266F1248365AAF1A9CF,
	InventorySoundPlayer_PlaySound_m0A9C10F0E8F69CA3FCDBEF071AE70E35C82D7498,
	InventorySoundPlayer_PlaySound_mC5D95B1EEA80448EBD742F5DC9E8BA3EAD8ED58F,
	InventorySoundPlayer_OnMMEvent_m34BA0DB526E47E4AC537103846D936901F5D9BB2,
	InventorySoundPlayer_OnEnable_m4490ECDC303B336932FE7ACE408B427E8EBA2C2E,
	InventorySoundPlayer_OnDisable_mAD4D313F8EBEE6A5F7BFCD160CB6CEC30627E402,
	InventorySoundPlayer__ctor_m8CCAEDF3FE2A846F6FAE934E2D04841678C632AA,
	BaseItem__ctor_m8D346286FB04A1650D19852D8414300CBBFB15E1,
	InventoryTester_AddItemTest_m310CE520788F3A6B81938C124D442B02B59EDBDD,
	InventoryTester_AddItemAtTest_m811E21A0D8FA6726BC622E92BE3FE79645DFE6C6,
	InventoryTester_MoveItemTest_mA8BC837E1BEA25CBC3F9F5E67DE09A2679AE7D84,
	InventoryTester_MoveItemToInventory_m947937188D171E2C59C6FD98C896563A086C6D84,
	InventoryTester_RemoveItemTest_mD84DD558866924406CD343A36DFE64334A91629C,
	InventoryTester_EmptyInventoryTest_m935E8667DB80CEB2AC52FF32BCF97DB9635ECA82,
	InventoryTester__ctor_m03B6FF27BCC837C20FE1FD19444322E336CE6F90,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mCCF04A7FCB7DDD3D5C8F8959347CCE34B5FB30BE,
};
extern void MMInventoryEvent__ctor_m8FA291F8E8B9BC49693B8F01CEEF6CBCA2F6A953_AdjustorThunk (void);
extern void ItemQuantity__ctor_m416007BCA62D9EBC2C9E04712A29E64D7BABCD83_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[2] = 
{
	{ 0x0600002E, MMInventoryEvent__ctor_m8FA291F8E8B9BC49693B8F01CEEF6CBCA2F6A953_AdjustorThunk },
	{ 0x060000A9, ItemQuantity__ctor_m416007BCA62D9EBC2C9E04712A29E64D7BABCD83_AdjustorThunk },
};
static const int32_t s_InvokerIndices[215] = 
{
	2798,
	3506,
	3506,
	3506,
	3445,
	2815,
	3424,
	3424,
	1131,
	4723,
	3506,
	3506,
	2815,
	1297,
	907,
	1274,
	889,
	1274,
	1297,
	2406,
	3506,
	1297,
	2406,
	2775,
	2035,
	2209,
	2201,
	3506,
	3506,
	2815,
	2815,
	3445,
	3506,
	908,
	2427,
	1010,
	1010,
	1010,
	1010,
	2798,
	2797,
	3506,
	3506,
	3506,
	3506,
	102,
	3693,
	3445,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	1700,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2798,
	3506,
	3506,
	3506,
	3473,
	3473,
	2209,
	2209,
	5247,
	3445,
	2815,
	2427,
	2427,
	2427,
	2427,
	2815,
	2427,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	1695,
	3506,
	3506,
	3506,
	3506,
	3473,
	1695,
	3506,
	3506,
	3473,
	2837,
	3506,
	2815,
	1222,
	2211,
	2798,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3424,
	2775,
	3445,
	2815,
	3445,
	2815,
	3445,
	3445,
	2815,
	3445,
	2815,
	3473,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	2775,
	3506,
	3506,
	3445,
	2815,
	2201,
	2815,
	3506,
	2775,
	3506,
	2798,
	3506,
	3506,
	3506,
	1689,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1689,
	2815,
	2775,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3473,
	3473,
	3473,
	3473,
	3506,
	3506,
	3506,
	3506,
	2815,
	1701,
	2798,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	5127,
};
extern const CustomAttributesCacheGenerator g_MoreMountains_InventoryEngine_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_InventoryEngine_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_InventoryEngine_CodeGenModule = 
{
	"MoreMountains.InventoryEngine.dll",
	215,
	s_methodPointers,
	2,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_MoreMountains_InventoryEngine_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

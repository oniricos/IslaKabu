﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Collections.BitArray
struct BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// MoreMountains.Feedbacks.FeedbackHelpAttribute
struct FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124;
// MoreMountains.Feedbacks.FeedbackPathAttribute
struct FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9;
// UnityEngine.GradientUsageAttribute
struct GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// MoreMountains.Feedbacks.MMFEnumConditionAttribute
struct MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_0_0_0_var;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// MoreMountains.Feedbacks.FeedbackHelpAttribute
struct FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String MoreMountains.Feedbacks.FeedbackHelpAttribute::HelpText
	String_t* ___HelpText_0;

public:
	inline static int32_t get_offset_of_HelpText_0() { return static_cast<int32_t>(offsetof(FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124, ___HelpText_0)); }
	inline String_t* get_HelpText_0() const { return ___HelpText_0; }
	inline String_t** get_address_of_HelpText_0() { return &___HelpText_0; }
	inline void set_HelpText_0(String_t* value)
	{
		___HelpText_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HelpText_0), (void*)value);
	}
};


// MoreMountains.Feedbacks.FeedbackPathAttribute
struct FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String MoreMountains.Feedbacks.FeedbackPathAttribute::Path
	String_t* ___Path_0;
	// System.String MoreMountains.Feedbacks.FeedbackPathAttribute::Name
	String_t* ___Name_1;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Path_0), (void*)value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_1), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ColorSpace
struct ColorSpace_tAD694F94295170CB332A0F99BBE086F4AC8C15BA 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorSpace_tAD694F94295170CB332A0F99BBE086F4AC8C15BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// MoreMountains.Feedbacks.MMFEnumConditionAttribute
struct MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Feedbacks.MMFEnumConditionAttribute::ConditionEnum
	String_t* ___ConditionEnum_0;
	// System.Boolean MoreMountains.Feedbacks.MMFEnumConditionAttribute::Hidden
	bool ___Hidden_1;
	// System.Collections.BitArray MoreMountains.Feedbacks.MMFEnumConditionAttribute::bitArray
	BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * ___bitArray_2;

public:
	inline static int32_t get_offset_of_ConditionEnum_0() { return static_cast<int32_t>(offsetof(MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D, ___ConditionEnum_0)); }
	inline String_t* get_ConditionEnum_0() const { return ___ConditionEnum_0; }
	inline String_t** get_address_of_ConditionEnum_0() { return &___ConditionEnum_0; }
	inline void set_ConditionEnum_0(String_t* value)
	{
		___ConditionEnum_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionEnum_0), (void*)value);
	}

	inline static int32_t get_offset_of_Hidden_1() { return static_cast<int32_t>(offsetof(MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D, ___Hidden_1)); }
	inline bool get_Hidden_1() const { return ___Hidden_1; }
	inline bool* get_address_of_Hidden_1() { return &___Hidden_1; }
	inline void set_Hidden_1(bool value)
	{
		___Hidden_1 = value;
	}

	inline static int32_t get_offset_of_bitArray_2() { return static_cast<int32_t>(offsetof(MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D, ___bitArray_2)); }
	inline BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * get_bitArray_2() const { return ___bitArray_2; }
	inline BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 ** get_address_of_bitArray_2() { return &___bitArray_2; }
	inline void set_bitArray_2(BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * value)
	{
		___bitArray_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bitArray_2), (void*)value);
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// UnityEngine.GradientUsageAttribute
struct GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Boolean UnityEngine.GradientUsageAttribute::hdr
	bool ___hdr_0;
	// UnityEngine.ColorSpace UnityEngine.GradientUsageAttribute::colorSpace
	int32_t ___colorSpace_1;

public:
	inline static int32_t get_offset_of_hdr_0() { return static_cast<int32_t>(offsetof(GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E, ___hdr_0)); }
	inline bool get_hdr_0() const { return ___hdr_0; }
	inline bool* get_address_of_hdr_0() { return &___hdr_0; }
	inline void set_hdr_0(bool value)
	{
		___hdr_0 = value;
	}

	inline static int32_t get_offset_of_colorSpace_1() { return static_cast<int32_t>(offsetof(GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E, ___colorSpace_1)); }
	inline int32_t get_colorSpace_1() const { return ___colorSpace_1; }
	inline int32_t* get_address_of_colorSpace_1() { return &___colorSpace_1; }
	inline void set_colorSpace_1(int32_t value)
	{
		___colorSpace_1 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.FeedbackHelpAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94 (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * __this, String_t* ___helpText0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.FeedbackPathAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * __this, String_t* ___path0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFEnumConditionAttribute::.ctor(System.String,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * __this, String_t* ___conditionBoolean0, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___enumValues1, const RuntimeMethod* method);
// System.Void UnityEngine.GradientUsageAttribute::.ctor(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GradientUsageAttribute__ctor_mE3620E225CE59463A2E072736F45D79F22CC4682 (GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E * __this, bool ___hdr0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, const RuntimeMethod* method);
static void MoreMountains_Feedbacks_TextMeshPro_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x70\x61\x63\x69\x6E\x67\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
}
static void MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_CharacterSpacingCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[2];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_InstantFontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_ColorMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x63\x6F\x6C\x6F\x72\x20\x6D\x6F\x64\x65\x20\x3A\x4E\x6F\x6E\x65\x20\x3A\x20\x6E\x6F\x74\x68\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x68\x61\x70\x70\x65\x6E\x2C\x67\x72\x61\x64\x69\x65\x6E\x74\x20\x3A\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x73\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x20\x6F\x6E\x20\x74\x68\x61\x74\x20\x67\x72\x61\x64\x69\x65\x6E\x74\x2C\x20\x66\x72\x6F\x6D\x20\x6C\x65\x66\x74\x20\x74\x6F\x20\x72\x69\x67\x68\x74\x2C\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x3A\x20\x6C\x65\x72\x70\x73\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x6F\x6E\x65\x20"), NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 2);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x6F\x77\x20\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x63\x68\x61\x6E\x67\x65\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65"), NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_InstantColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x61\x70\x70\x6C\x79"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_ColorGradient(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x67\x72\x61\x64\x69\x65\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E * tmp = (GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E *)cache->attributes[2];
		GradientUsageAttribute__ctor_mE3620E225CE59463A2E072736F45D79F22CC4682(tmp, true, NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_DestinationColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6C\x6F\x72\x20\x77\x68\x65\x6E\x20\x69\x6E\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_ColorCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x75\x73\x65\x20\x77\x68\x65\x6E\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x69\x6E\x67\x20\x74\x6F\x77\x61\x72\x64\x73\x20\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6C\x6F\x72"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_AllowAdditivePlays(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x74\x68\x61\x74\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x74\x72\x69\x67\x67\x65\x72\x20\x69\x74\x2C\x20\x65\x76\x65\x6E\x20\x69\x66\x20\x69\x74\x27\x73\x20\x69\x6E\x20\x70\x72\x6F\x67\x72\x65\x73\x73\x2E\x20\x49\x66\x20\x69\x74\x27\x73\x20\x66\x61\x6C\x73\x65\x2C\x20\x69\x74\x27\x6C\x6C\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x61\x6E\x79\x20\x6E\x65\x77\x20\x50\x6C\x61\x79\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x6F\x6E\x65\x20\x69\x73\x20\x6F\x76\x65\x72"), NULL);
	}
}
static void MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_MMFeedbackTMPColor_ChangeColor_m445785B10D4037807ABE2C9394AB3394EDF04E8C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_0_0_0_var), NULL);
	}
}
static void U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16__ctor_m1CBC0C9A5CD5055E1F054D636358F07B114E6A83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_IDisposable_Dispose_m38AEF37E41C45DD45A68C98AA62FF78D773A6C99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m392ACFD9517520E5DD2EB15BB00F3F10F829F164(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_Reset_m5FE1C15D1386C54411D93804C47B20FF19E33E59(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_get_Current_mE14D11EDD5A98BFC33F2612699B95420846710EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[0];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x44\x69\x6C\x61\x74\x65"), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x64\x69\x6C\x61\x74\x65\x20\x61\x20\x54\x4D\x50\x20\x74\x65\x78\x74\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_RelativeValues(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x6C\x61\x74\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x76\x61\x6C\x75\x65\x73\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x72\x65\x6C\x61\x74\x69\x76\x65"), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_DilateCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_InstantDilate(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_AllowAdditivePlays(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x74\x68\x61\x74\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x74\x72\x69\x67\x67\x65\x72\x20\x69\x74\x2C\x20\x65\x76\x65\x6E\x20\x69\x66\x20\x69\x74\x27\x73\x20\x69\x6E\x20\x70\x72\x6F\x67\x72\x65\x73\x73\x2E\x20\x49\x66\x20\x69\x74\x27\x73\x20\x66\x61\x6C\x73\x65\x2C\x20\x69\x74\x27\x6C\x6C\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x61\x6E\x79\x20\x6E\x65\x77\x20\x50\x6C\x61\x79\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x6F\x6E\x65\x20\x69\x73\x20\x6F\x76\x65\x72"), NULL);
	}
}
static void MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_MMFeedbackTMPDilate_ApplyValueOverTime_m5B398948042D985703E780C5ED8C96883BEB0F04(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_0_0_0_var), NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16__ctor_m22220E09093D9A95B0E997AA43661D12C758D547(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_IDisposable_Dispose_mBA58AEC4BDC58017FB1A2B99B538C954BED5CFC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DDC43095ACCA8210EAFFDA72D765927E6FE3262(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_Reset_m844F29520D86819CA224CEC5FFB1CE230104F6FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_get_Current_m28A4505C16D7708F704E7E342D969ABBF86F4F85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[1];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x46\x6F\x6E\x74\x20\x53\x69\x7A\x65"), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[2];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x66\x6F\x6E\x74\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
}
static void MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_FontSizeCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x6E\x74\x20\x53\x69\x7A\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[2];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_InstantFontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[0];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x4C\x69\x6E\x65\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x6C\x69\x6E\x65\x20\x73\x70\x61\x63\x69\x6E\x67\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_LineSpacingCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x67\x72\x61\x70\x68\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_InstantFontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[0];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x27\x73\x20\x6F\x75\x74\x6C\x69\x6E\x65\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x4F\x75\x74\x6C\x69\x6E\x65\x20\x43\x6F\x6C\x6F\x72"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_ColorMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x6C\x69\x6E\x65\x20\x43\x6F\x6C\x6F\x72"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x63\x6F\x6C\x6F\x72\x20\x6D\x6F\x64\x65\x20\x3A\x4E\x6F\x6E\x65\x20\x3A\x20\x6E\x6F\x74\x68\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x68\x61\x70\x70\x65\x6E\x2C\x67\x72\x61\x64\x69\x65\x6E\x74\x20\x3A\x20\x65\x76\x61\x6C\x75\x61\x74\x65\x73\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x20\x6F\x6E\x20\x74\x68\x61\x74\x20\x67\x72\x61\x64\x69\x65\x6E\x74\x2C\x20\x66\x72\x6F\x6D\x20\x6C\x65\x66\x74\x20\x74\x6F\x20\x72\x69\x67\x68\x74\x2C\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x3A\x20\x6C\x65\x72\x70\x73\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x6F\x6E\x65\x20"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 2);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x6F\x77\x20\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x63\x68\x61\x6E\x67\x65\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_InstantColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x61\x70\x70\x6C\x79"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_ColorGradient(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E * tmp = (GradientUsageAttribute_t6A5149CB6DDD8B948B3291F6F35D7F1A4A82787E *)cache->attributes[0];
		GradientUsageAttribute__ctor_mE3620E225CE59463A2E072736F45D79F22CC4682(tmp, true, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x67\x72\x61\x64\x69\x65\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[2];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_DestinationColor(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6C\x6F\x72\x20\x77\x68\x65\x6E\x20\x69\x6E\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x65\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_ColorCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x75\x73\x65\x20\x77\x68\x65\x6E\x20\x69\x6E\x74\x65\x72\x70\x6F\x6C\x61\x74\x69\x6E\x67\x20\x74\x6F\x77\x61\x72\x64\x73\x20\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x63\x6F\x6C\x6F\x72"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_AllowAdditivePlays(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x74\x68\x61\x74\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x74\x72\x69\x67\x67\x65\x72\x20\x69\x74\x2C\x20\x65\x76\x65\x6E\x20\x69\x66\x20\x69\x74\x27\x73\x20\x69\x6E\x20\x70\x72\x6F\x67\x72\x65\x73\x73\x2E\x20\x49\x66\x20\x69\x74\x27\x73\x20\x66\x61\x6C\x73\x65\x2C\x20\x69\x74\x27\x6C\x6C\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x61\x6E\x79\x20\x6E\x65\x77\x20\x50\x6C\x61\x79\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x6F\x6E\x65\x20\x69\x73\x20\x6F\x76\x65\x72"), NULL);
	}
}
static void MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_MMFeedbackTMPOutlineColor_ChangeColor_mEBF1532CAE65605B72487A7CA05EE0554098ECBE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_0_0_0_var), NULL);
	}
}
static void U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16__ctor_mA236F443A5487392D51A4B2829D92455C4602CA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_IDisposable_Dispose_mA2B147A4CD0456E7AD3C65C7840FF372E3AC925B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EAAA3A67B04C6EE45B2F6C2E58E542CF61E3B6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_Reset_mB84E4AFEE815E8DFA1F461C10DCFB25C996BA858(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_get_Current_m16FB30E79B81434D8062827FE2BDCF9CBE9F47A7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[0];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x4F\x75\x74\x6C\x69\x6E\x65\x20\x57\x69\x64\x74\x68"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[2];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x6F\x75\x74\x6C\x69\x6E\x65\x20\x77\x69\x64\x74\x68\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
}
static void MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_OutlineWidthCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x75\x74\x6C\x69\x6E\x65\x20\x57\x69\x64\x74\x68"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[2];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_InstantFontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x70\x61\x72\x61\x67\x72\x61\x70\x68\x20\x73\x70\x61\x63\x69\x6E\x67\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x50\x61\x72\x61\x67\x72\x61\x70\x68\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
}
static void MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_ParagraphSpacingCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x61\x72\x61\x67\x72\x61\x70\x68\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[2];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_InstantFontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[0];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x53\x6F\x66\x74\x6E\x65\x73\x73"), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x74\x77\x65\x61\x6B\x20\x74\x68\x65\x20\x73\x6F\x66\x74\x6E\x65\x73\x73\x20\x6F\x66\x20\x61\x20\x54\x4D\x50\x20\x74\x65\x78\x74\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_RelativeValues(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x66\x74\x6E\x65\x73\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x76\x61\x6C\x75\x65\x73\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x72\x65\x6C\x61\x74\x69\x76\x65"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_SoftnessCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_InstantSoftness(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_AllowAdditivePlays(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x63\x61\x6C\x6C\x69\x6E\x67\x20\x74\x68\x61\x74\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x74\x72\x69\x67\x67\x65\x72\x20\x69\x74\x2C\x20\x65\x76\x65\x6E\x20\x69\x66\x20\x69\x74\x27\x73\x20\x69\x6E\x20\x70\x72\x6F\x67\x72\x65\x73\x73\x2E\x20\x49\x66\x20\x69\x74\x27\x73\x20\x66\x61\x6C\x73\x65\x2C\x20\x69\x74\x27\x6C\x6C\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x61\x6E\x79\x20\x6E\x65\x77\x20\x50\x6C\x61\x79\x20\x75\x6E\x74\x69\x6C\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x6F\x6E\x65\x20\x69\x73\x20\x6F\x76\x65\x72"), NULL);
	}
}
static void MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_MMFeedbackTMPSoftness_ApplyValueOverTime_mB4AE2C72A49C99A92A498FB1FB131406EC4906B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_0_0_0_var), NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16__ctor_mDE63AFEF1AB112D0F61E9D50B362D1949B7D9402(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_IDisposable_Dispose_m0885939C65609356B431FC0943130E35FD8A6EEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF06122D14B4BC787DDC6CB47ED1B7DE8C1944FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_Reset_mCE9401278792035029134BAE65ABD4CD748A1EF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_get_Current_m29E5AEA0171EDC8476D5D8898C2602A2DB99EA1D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MMFeedbackTMPText_t323301F9F6BBDAE7DB4BC3115372ED6FB9E54DDF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x6C\x65\x74\x20\x79\x6F\x75\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x74\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x54\x65\x78\x74"), NULL);
	}
}
static void MMFeedbackTMPText_t323301F9F6BBDAE7DB4BC3115372ED6FB9E54DDF_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x65\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F"), NULL);
	}
}
static void MMFeedbackTMPText_t323301F9F6BBDAE7DB4BC3115372ED6FB9E54DDF_CustomAttributesCacheGenerator_NewText(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6E\x65\x77\x20\x74\x65\x78\x74\x20\x74\x6F\x20\x72\x65\x70\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x6F\x6C\x64\x20\x6F\x6E\x65\x20\x77\x69\x74\x68"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x6C\x65\x74\x20\x79\x6F\x75\x20\x72\x65\x76\x65\x61\x6C\x20\x77\x6F\x72\x64\x73\x2C\x20\x6C\x69\x6E\x65\x73\x2C\x20\x6F\x72\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x73\x20\x69\x6E\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x2C\x20\x6F\x6E\x65\x20\x61\x74\x20\x61\x20\x74\x69\x6D\x65"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x54\x65\x78\x74\x20\x52\x65\x76\x65\x61\x6C"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x77\x65\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_ReplaceText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x70\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x54\x4D\x50\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x74\x65\x78\x74\x20\x6F\x6E\x20\x70\x6C\x61\x79"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x6E\x67\x65\x20\x54\x65\x78\x74"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_NewText(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6E\x65\x77\x20\x74\x65\x78\x74\x20\x74\x6F\x20\x72\x65\x70\x6C\x61\x63\x65\x20\x74\x68\x65\x20\x6F\x6C\x64\x20\x6F\x6E\x65\x20\x77\x69\x74\x68"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_RevealMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x76\x65\x61\x6C"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x77\x61\x79\x20\x74\x6F\x20\x72\x65\x76\x65\x61\x6C\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x28\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x62\x79\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x2C\x20\x77\x6F\x72\x64\x20\x62\x79\x20\x77\x6F\x72\x64\x2C\x20\x6F\x72\x20\x6C\x69\x6E\x65\x20\x62\x79\x20\x6C\x69\x6E\x65\x29"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_DurationMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x74\x6F\x20\x64\x65\x66\x69\x6E\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x62\x79\x20\x74\x68\x65\x20\x74\x69\x6D\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x77\x6F\x20\x75\x6E\x69\x74\x20\x72\x65\x76\x65\x61\x6C\x73\x2C\x20\x6F\x72\x20\x62\x79\x20\x74\x68\x65\x20\x74\x6F\x74\x61\x6C\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x74\x68\x65\x20\x72\x65\x76\x65\x61\x6C\x20\x73\x68\x6F\x75\x6C\x64\x20\x74\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_IntervalBetweenReveals(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x44\x75\x72\x61\x74\x69\x6F\x6E\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x69\x6E\x74\x65\x72\x76\x61\x6C\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x77\x6F\x20\x72\x65\x76\x65\x61\x6C\x73"), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_RevealDuration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x6F\x74\x61\x6C\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x72\x65\x76\x65\x61\x6C\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x44\x75\x72\x61\x74\x69\x6F\x6E\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_MMFeedbackTMPTextReveal_RevealCharacters_m57FF9E3484C29614CF4E6DFA467714889FD1B2C0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_0_0_0_var), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_MMFeedbackTMPTextReveal_RevealLines_mE85BCA7CF269F1B12CF9EBC9BA659508F52A55BD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_0_0_0_var), NULL);
	}
}
static void MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_MMFeedbackTMPTextReveal_RevealWords_mC719553615176A96EB6B91ECD23837FAE6C8AB18(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_0_0_0_var), NULL);
	}
}
static void U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15__ctor_mF242B92619528256837C58C8C1E3C0AED5DAC43F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_IDisposable_Dispose_m311F076295869393038D552E3100DC59098F48FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1D32E7A9D8311FD051BE58044FC412F435251B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_Collections_IEnumerator_Reset_m9E630D4A2AC8AA1E555A6778F3D8C8B262CFE24E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_Collections_IEnumerator_get_Current_m9F2EDE14530F4F42CF98296865856BC13102D3B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16__ctor_m655E4173573D185BE3DE1ED51BDD24D79880209C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_IDisposable_Dispose_m95CAB613C4FE8F7A77D8DB9857DE0979FBAAC98E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7FEDE70233990DCF5293098F5AD97C3B6CFC6AEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_Collections_IEnumerator_Reset_mC2E10F09681E416804B96876C75293DE33F5C278(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_Collections_IEnumerator_get_Current_mDA1F15F51D88901E719130E40783E0FD6593F8C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17__ctor_m3BB4E433C184C2618323CD3DF138764228D97FFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_IDisposable_Dispose_m305437706EC85160CEE40E97F28F3E4967E31545(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2987EF75446EDE3D6DCDE24CB19A21C8E00FFEB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_Collections_IEnumerator_Reset_m4369749E1C088572A3C844D574C9853626D4F08B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_Collections_IEnumerator_get_Current_m2299E1046465B96769BCF50187D75F917819DA8D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x6C\x65\x74\x73\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x74\x68\x65\x20\x77\x6F\x72\x64\x20\x73\x70\x61\x63\x69\x6E\x67\x20\x6F\x66\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x54\x4D\x50\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74\x4D\x65\x73\x68\x20\x50\x72\x6F\x2F\x54\x4D\x50\x20\x57\x6F\x72\x64\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
}
static void MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_TargetTMPText(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x54\x4D\x50\x5F\x54\x65\x78\x74\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74"), NULL);
	}
}
static void MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_WordSpacingCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x57\x6F\x72\x64\x20\x53\x70\x61\x63\x69\x6E\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x74\x77\x65\x65\x6E\x20\x6F\x6E"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[2];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_RemapZero(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_RemapOne(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_InstantFontSize(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x69\x6E\x20\x69\x6E\x73\x74\x61\x6E\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_TextMeshPro_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_TextMeshPro_AttributeGenerators[135] = 
{
	MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator,
	U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator,
	U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator,
	MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator,
	MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator,
	U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator,
	MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator,
	MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator,
	U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator,
	MMFeedbackTMPText_t323301F9F6BBDAE7DB4BC3115372ED6FB9E54DDF_CustomAttributesCacheGenerator,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator,
	U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator,
	U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator,
	U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator,
	MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator,
	MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_CharacterSpacingCurve,
	MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPCharacterSpacing_t287CA64D557CE0A7CA815445B1355390241B4ECE_CustomAttributesCacheGenerator_InstantFontSize,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_ColorMode,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_Duration,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_InstantColor,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_ColorGradient,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_DestinationColor,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_ColorCurve,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_AllowAdditivePlays,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_RelativeValues,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_Mode,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_Duration,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_DilateCurve,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_InstantDilate,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_AllowAdditivePlays,
	MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_FontSizeCurve,
	MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPFontSize_t744C390B7B12D21BFFCC23C9EE4DEFA58EC4F927_CustomAttributesCacheGenerator_InstantFontSize,
	MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_LineSpacingCurve,
	MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPLineSpacing_tC6F9E88782BBF300FF9F144D27FA24D41D18120D_CustomAttributesCacheGenerator_InstantFontSize,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_ColorMode,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_Duration,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_InstantColor,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_ColorGradient,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_DestinationColor,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_ColorCurve,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_AllowAdditivePlays,
	MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_OutlineWidthCurve,
	MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPOutlineWidth_t7C5C8AF8793687B21720A83D6CDB2B4FD84276EF_CustomAttributesCacheGenerator_InstantFontSize,
	MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_ParagraphSpacingCurve,
	MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPParagraphSpacing_t59CBDC0216191F92F3E10F79BD650ACCFEF3A5ED_CustomAttributesCacheGenerator_InstantFontSize,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_RelativeValues,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_Mode,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_Duration,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_SoftnessCurve,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_InstantSoftness,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_AllowAdditivePlays,
	MMFeedbackTMPText_t323301F9F6BBDAE7DB4BC3115372ED6FB9E54DDF_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPText_t323301F9F6BBDAE7DB4BC3115372ED6FB9E54DDF_CustomAttributesCacheGenerator_NewText,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_ReplaceText,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_NewText,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_RevealMode,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_DurationMode,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_IntervalBetweenReveals,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_RevealDuration,
	MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_TargetTMPText,
	MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_WordSpacingCurve,
	MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_RemapZero,
	MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_RemapOne,
	MMFeedbackTMPWordSpacing_tFA55469CA2F687B5B60E2C32FD00D550C9428B6C_CustomAttributesCacheGenerator_InstantFontSize,
	MMFeedbackTMPColor_tC593D416DB0DD37AB77700420F557B50AF1EDBDB_CustomAttributesCacheGenerator_MMFeedbackTMPColor_ChangeColor_m445785B10D4037807ABE2C9394AB3394EDF04E8C,
	U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16__ctor_m1CBC0C9A5CD5055E1F054D636358F07B114E6A83,
	U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_IDisposable_Dispose_m38AEF37E41C45DD45A68C98AA62FF78D773A6C99,
	U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m392ACFD9517520E5DD2EB15BB00F3F10F829F164,
	U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_Reset_m5FE1C15D1386C54411D93804C47B20FF19E33E59,
	U3CChangeColorU3Ed__16_t799F633D17C2F90DC1C0989E6E9717BDD949491E_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_get_Current_mE14D11EDD5A98BFC33F2612699B95420846710EB,
	MMFeedbackTMPDilate_t283E70A053DB31D6AA5EA3BF6081A9A27B8ED0A7_CustomAttributesCacheGenerator_MMFeedbackTMPDilate_ApplyValueOverTime_m5B398948042D985703E780C5ED8C96883BEB0F04,
	U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16__ctor_m22220E09093D9A95B0E997AA43661D12C758D547,
	U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_IDisposable_Dispose_mBA58AEC4BDC58017FB1A2B99B538C954BED5CFC6,
	U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6DDC43095ACCA8210EAFFDA72D765927E6FE3262,
	U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_Reset_m844F29520D86819CA224CEC5FFB1CE230104F6FB,
	U3CApplyValueOverTimeU3Ed__16_tB22ADF75FCC75047CF55357B5C9D3CF62F2C5F6C_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_get_Current_m28A4505C16D7708F704E7E342D969ABBF86F4F85,
	MMFeedbackTMPOutlineColor_tC4AD0D40AAB5B6766F8AEC0CC1193458DABB59F3_CustomAttributesCacheGenerator_MMFeedbackTMPOutlineColor_ChangeColor_mEBF1532CAE65605B72487A7CA05EE0554098ECBE,
	U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16__ctor_mA236F443A5487392D51A4B2829D92455C4602CA2,
	U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_IDisposable_Dispose_mA2B147A4CD0456E7AD3C65C7840FF372E3AC925B,
	U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3EAAA3A67B04C6EE45B2F6C2E58E542CF61E3B6F,
	U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_Reset_mB84E4AFEE815E8DFA1F461C10DCFB25C996BA858,
	U3CChangeColorU3Ed__16_t7AB3A960609E7ED5515D74EC50D98E8C4DD11839_CustomAttributesCacheGenerator_U3CChangeColorU3Ed__16_System_Collections_IEnumerator_get_Current_m16FB30E79B81434D8062827FE2BDCF9CBE9F47A7,
	MMFeedbackTMPSoftness_t198B24C26D72AEEE09E008BAF5E8DDC4248F3EB3_CustomAttributesCacheGenerator_MMFeedbackTMPSoftness_ApplyValueOverTime_mB4AE2C72A49C99A92A498FB1FB131406EC4906B3,
	U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16__ctor_mDE63AFEF1AB112D0F61E9D50B362D1949B7D9402,
	U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_IDisposable_Dispose_m0885939C65609356B431FC0943130E35FD8A6EEB,
	U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBF06122D14B4BC787DDC6CB47ED1B7DE8C1944FF,
	U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_Reset_mCE9401278792035029134BAE65ABD4CD748A1EF2,
	U3CApplyValueOverTimeU3Ed__16_t1B15EF24375DA6C8B4A4CCF07498CBF393933D01_CustomAttributesCacheGenerator_U3CApplyValueOverTimeU3Ed__16_System_Collections_IEnumerator_get_Current_m29E5AEA0171EDC8476D5D8898C2602A2DB99EA1D,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_MMFeedbackTMPTextReveal_RevealCharacters_m57FF9E3484C29614CF4E6DFA467714889FD1B2C0,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_MMFeedbackTMPTextReveal_RevealLines_mE85BCA7CF269F1B12CF9EBC9BA659508F52A55BD,
	MMFeedbackTMPTextReveal_t287E4D025F164891B81E94D20432C6DFA6B5F663_CustomAttributesCacheGenerator_MMFeedbackTMPTextReveal_RevealWords_mC719553615176A96EB6B91ECD23837FAE6C8AB18,
	U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15__ctor_mF242B92619528256837C58C8C1E3C0AED5DAC43F,
	U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_IDisposable_Dispose_m311F076295869393038D552E3100DC59098F48FB,
	U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1D32E7A9D8311FD051BE58044FC412F435251B2,
	U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_Collections_IEnumerator_Reset_m9E630D4A2AC8AA1E555A6778F3D8C8B262CFE24E,
	U3CRevealCharactersU3Ed__15_tF057F038AF194B76B14DAD48D76F955A6005147F_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__15_System_Collections_IEnumerator_get_Current_m9F2EDE14530F4F42CF98296865856BC13102D3B7,
	U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16__ctor_m655E4173573D185BE3DE1ED51BDD24D79880209C,
	U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_IDisposable_Dispose_m95CAB613C4FE8F7A77D8DB9857DE0979FBAAC98E,
	U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7FEDE70233990DCF5293098F5AD97C3B6CFC6AEA,
	U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_Collections_IEnumerator_Reset_mC2E10F09681E416804B96876C75293DE33F5C278,
	U3CRevealLinesU3Ed__16_tCC54B04E6C5CECBD5130CC5BCC90C733C9194D39_CustomAttributesCacheGenerator_U3CRevealLinesU3Ed__16_System_Collections_IEnumerator_get_Current_mDA1F15F51D88901E719130E40783E0FD6593F8C4,
	U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17__ctor_m3BB4E433C184C2618323CD3DF138764228D97FFA,
	U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_IDisposable_Dispose_m305437706EC85160CEE40E97F28F3E4967E31545,
	U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA2987EF75446EDE3D6DCDE24CB19A21C8E00FFEB,
	U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_Collections_IEnumerator_Reset_m4369749E1C088572A3C844D574C9853626D4F08B,
	U3CRevealWordsU3Ed__17_tE3B27012A95EB709D91805E4113DFFB1F73BDC05_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__17_System_Collections_IEnumerator_get_Current_m2299E1046465B96769BCF50187D75F917819DA8D,
	MoreMountains_Feedbacks_TextMeshPro_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.AudioSource ClickSound::get_source()
extern void ClickSound_get_source_m51C3EF83C207057CA300546C2275C5867EF63850 (void);
// 0x00000002 UnityEngine.UI.Button ClickSound::get_btn()
extern void ClickSound_get_btn_mDE7FD029C2E70F977C664476C1DADF30231CE7F8 (void);
// 0x00000003 System.Void ClickSound::Start()
extern void ClickSound_Start_m6EBC1CC4674AFE32773FB36258F99F5F9F21276C (void);
// 0x00000004 System.Void ClickSound::PlaySound()
extern void ClickSound_PlaySound_m456648F2F10E328C5C37D1137463C622A68DA540 (void);
// 0x00000005 System.Void ClickSound::.ctor()
extern void ClickSound__ctor_mCE0A14248C9C47EC74657CBE2FC9E2B9A74F29CC (void);
// 0x00000006 System.Void NivelManager::Start()
extern void NivelManager_Start_m03644C8C88DB10707F1F9440E4C13AFE3CB2C1F2 (void);
// 0x00000007 System.Void NivelManager::Update()
extern void NivelManager_Update_mA004D198E57253EEC43B93BF7351DB17547B3903 (void);
// 0x00000008 System.Void NivelManager::.ctor()
extern void NivelManager__ctor_mE80CA6CEE7A831C19E1A85B6A4DCE98EDB5B20AB (void);
// 0x00000009 System.Void Aerena::Awake()
extern void Aerena_Awake_mBB9380B98607DC6F8FEF46B2CD7CF59C3BE56FDB (void);
// 0x0000000A System.Void Aerena::Start()
extern void Aerena_Start_m871A4FBF15D54BDA1CC44B26CC1A36C046D5197C (void);
// 0x0000000B System.Void Aerena::Update()
extern void Aerena_Update_mBFFEE7F7BBFFC555A693272A8110F83B3FF6864A (void);
// 0x0000000C System.Void Aerena::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Aerena_OnTriggerEnter2D_m851CBC3D9BFDC66FF68985E62F6FBD8919483A87 (void);
// 0x0000000D System.Void Aerena::Coin()
extern void Aerena_Coin_mF9AABD01A2AC8B5A108EC59D94B8B7AB52235A18 (void);
// 0x0000000E System.Void Aerena::.ctor()
extern void Aerena__ctor_mF543EB95A3ACF66D0D19D2EEDAB28FFB77F6B2F2 (void);
// 0x0000000F System.Void AnimacionFinal::Start()
extern void AnimacionFinal_Start_m5E87664D45E188E5589BB49525A6E0754640490F (void);
// 0x00000010 System.Void AnimacionFinal::Awake()
extern void AnimacionFinal_Awake_m3321438F2EC88EE60F6BBDD820CC3D1A404E543F (void);
// 0x00000011 System.Void AnimacionFinal::Update()
extern void AnimacionFinal_Update_m5C163721FB83852F6FB2BC16BD5DE764072A8586 (void);
// 0x00000012 System.Void AnimacionFinal::PrenderLlamas()
extern void AnimacionFinal_PrenderLlamas_m93AAFB82A9011B4DDD21166A010CC6D1457216F5 (void);
// 0x00000013 System.Void AnimacionFinal::.ctor()
extern void AnimacionFinal__ctor_m005A00AB71B5D39A6B7C2562DF20555793F2C44E (void);
// 0x00000014 System.Void AnimacionKabu::Start()
extern void AnimacionKabu_Start_m37BC476F5DC3E288D0A769045755FFF6A5CA9E72 (void);
// 0x00000015 System.Void AnimacionKabu::Update()
extern void AnimacionKabu_Update_m43BBD91A5A36A85AE3B3276A2770E0F1AFFD0B75 (void);
// 0x00000016 System.Void AnimacionKabu::.ctor()
extern void AnimacionKabu__ctor_m0F6AA9877E3C0F0D9883427F7C3BDF2751A036B7 (void);
// 0x00000017 System.Void BarraDeSalud::Update()
extern void BarraDeSalud_Update_m5FD1B9A9A6DF48A0BC56E67487450FBEA58EE711 (void);
// 0x00000018 System.Void BarraDeSalud::.ctor()
extern void BarraDeSalud__ctor_m04B27A0BFEE610A1EC7B953844A194CBFAE9B92B (void);
// 0x00000019 System.Void BrujaFinal::Start()
extern void BrujaFinal_Start_mE2CC920DB3906F7BFB4D8FC90C2EFC568FD0CFED (void);
// 0x0000001A System.Void BrujaFinal::Update()
extern void BrujaFinal_Update_m7B85C66A08DE6828EF2E6F5D973F22D7EA1E3465 (void);
// 0x0000001B System.Void BrujaFinal::BrujaHuevo()
extern void BrujaFinal_BrujaHuevo_mD92ED3142E0E61CB6E039FEE3EA8D161A023AB25 (void);
// 0x0000001C System.Void BrujaFinal::.ctor()
extern void BrujaFinal__ctor_m97155CC5177EF1F0932AFEB4F75A0F3E044F4138 (void);
// 0x0000001D System.Void Burbuja::Start()
extern void Burbuja_Start_m21E291E7F0D1905CFDD5A7F9F36A6CF7A358C15F (void);
// 0x0000001E System.Collections.IEnumerator Burbuja::MoverIzqDr()
extern void Burbuja_MoverIzqDr_m5A8511933345071EC18815BED888F8B0E38387E1 (void);
// 0x0000001F System.Void Burbuja::.ctor()
extern void Burbuja__ctor_m324824725D1F36D15F4F9F06D3B000AD76189FF8 (void);
// 0x00000020 System.Void Burbuja/<MoverIzqDr>d__2::.ctor(System.Int32)
extern void U3CMoverIzqDrU3Ed__2__ctor_mFF418106ED642001DE5EB4EF9B1DA81E6FC9F95B (void);
// 0x00000021 System.Void Burbuja/<MoverIzqDr>d__2::System.IDisposable.Dispose()
extern void U3CMoverIzqDrU3Ed__2_System_IDisposable_Dispose_m6BD167F2FD4C89A730E88EE2143851AD001A3181 (void);
// 0x00000022 System.Boolean Burbuja/<MoverIzqDr>d__2::MoveNext()
extern void U3CMoverIzqDrU3Ed__2_MoveNext_mFF27C64447726CB028A6453632C89A29F7522A66 (void);
// 0x00000023 System.Object Burbuja/<MoverIzqDr>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoverIzqDrU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87099B322323E87F8E5FA22020DD9F716F1DB421 (void);
// 0x00000024 System.Void Burbuja/<MoverIzqDr>d__2::System.Collections.IEnumerator.Reset()
extern void U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_Reset_m6811DBECCC2CE3550F38BA1B956DCEA51E2FB330 (void);
// 0x00000025 System.Object Burbuja/<MoverIzqDr>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_get_Current_m5773AEC31ABAB38671A73E5086BE573F14817574 (void);
// 0x00000026 System.Void CalaveraCkeck::Update()
extern void CalaveraCkeck_Update_m7D5A0F71786F63760389307AE83824BD3F67D94E (void);
// 0x00000027 System.Void CalaveraCkeck::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CalaveraCkeck_OnTriggerEnter2D_mDA269996CFB2783228B9A4236D689AF0A6551A6D (void);
// 0x00000028 System.Void CalaveraCkeck::.ctor()
extern void CalaveraCkeck__ctor_m18661E7078F91212D59421E0B971815D89E50CEB (void);
// 0x00000029 System.Void ComprobarVida::Start()
extern void ComprobarVida_Start_m2DA341ACFCEDB7AB9F79685FAEF8B646F229CAEA (void);
// 0x0000002A System.Void ComprobarVida::Update()
extern void ComprobarVida_Update_m71EB10FB879308AD84EF444CD53034246F23EA4A (void);
// 0x0000002B System.Void ComprobarVida::.ctor()
extern void ComprobarVida__ctor_m5BCA0B1AAC6BB0D2557D791AC92A2960299317B5 (void);
// 0x0000002C System.Void ContadorCorazones::Start()
extern void ContadorCorazones_Start_m3015BBDEE7A5B15CD045C89BF05B57B0E009CEE7 (void);
// 0x0000002D System.Void ContadorCorazones::Update()
extern void ContadorCorazones_Update_m081E965A6FBF490B0ECAA0992D0D0A086D40E910 (void);
// 0x0000002E System.Void ContadorCorazones::.ctor()
extern void ContadorCorazones__ctor_mF9557B936523FD942F9FF305AD5944071F11F92D (void);
// 0x0000002F System.Void ContadorFinal::Awake()
extern void ContadorFinal_Awake_m9D526D60C3B7B4206B0560480AFD9C1D9C8C6776 (void);
// 0x00000030 System.Void ContadorFinal::Start()
extern void ContadorFinal_Start_mD0D9613032B363C44AD5D9537C7AAC945911EB4C (void);
// 0x00000031 System.Void ContadorFinal::Update()
extern void ContadorFinal_Update_m9EF547AA2DA811751881060C7B336E3A64B5A950 (void);
// 0x00000032 System.Collections.IEnumerator ContadorFinal::EjecutarSuma()
extern void ContadorFinal_EjecutarSuma_mFE44F1E853276A4C77872A0E012DF9A86248EE98 (void);
// 0x00000033 System.Void ContadorFinal::.ctor()
extern void ContadorFinal__ctor_mFF798710CD9FC2A1CBFA37EA61D3AB33710A3012 (void);
// 0x00000034 System.Void ContadorFinal::.cctor()
extern void ContadorFinal__cctor_m62F2CB90FB1E433C504B198F37FD61BC68263B08 (void);
// 0x00000035 System.Void ContadorFinal/<EjecutarSuma>d__9::.ctor(System.Int32)
extern void U3CEjecutarSumaU3Ed__9__ctor_m555B5D67B42B1BA719A4FDAC995620EB3494F48E (void);
// 0x00000036 System.Void ContadorFinal/<EjecutarSuma>d__9::System.IDisposable.Dispose()
extern void U3CEjecutarSumaU3Ed__9_System_IDisposable_Dispose_m51E2F857F838E5B63D1EB6C280C14F1AA38FA396 (void);
// 0x00000037 System.Boolean ContadorFinal/<EjecutarSuma>d__9::MoveNext()
extern void U3CEjecutarSumaU3Ed__9_MoveNext_mC2517BF140BDD05607C0E4B3BE1DADC6314A6E19 (void);
// 0x00000038 System.Object ContadorFinal/<EjecutarSuma>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEjecutarSumaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC01B665FC29861D3CFEF4E6B5E3C38CD1E10DE5A (void);
// 0x00000039 System.Void ContadorFinal/<EjecutarSuma>d__9::System.Collections.IEnumerator.Reset()
extern void U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_Reset_mAB9D23DD640D14BFD07A18375B560886D30407F4 (void);
// 0x0000003A System.Object ContadorFinal/<EjecutarSuma>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_get_Current_mECBDCC521AAF9ADDE4EB030C11D055488FCB5E01 (void);
// 0x0000003B System.Void CursorLock::Update()
extern void CursorLock_Update_m5E8B43504485031D65EEEA4668FDB473666840AE (void);
// 0x0000003C System.Void CursorLock::.ctor()
extern void CursorLock__ctor_m4A60DC59BD7EFBBB124D55CD0A594D812EBB782B (void);
// 0x0000003D System.Void PlayerData::.ctor()
extern void PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B (void);
// 0x0000003E System.Void DesactivarCamara::Start()
extern void DesactivarCamara_Start_m78C34EF74D077CAF15227CB205DEE7409B03ADBF (void);
// 0x0000003F System.Void DesactivarCamara::Update()
extern void DesactivarCamara_Update_mBA7294D3F491ACE27018165F7BD0AC25C8630248 (void);
// 0x00000040 System.Void DesactivarCamara::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DesactivarCamara_OnTriggerEnter2D_mED8007E438A7E1EF02D8E802D61CF48A0D7D4DC5 (void);
// 0x00000041 System.Void DesactivarCamara::.ctor()
extern void DesactivarCamara__ctor_m870198D621355DF7B47D90FBFA5CF14F3EDF02C8 (void);
// 0x00000042 System.Void Destrutror::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Destrutror_OnTriggerEnter2D_mE930BECDA2709747596D8C6D2BF0BCA92DD37EA2 (void);
// 0x00000043 System.Void Destrutror::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Destrutror_OnCollisionEnter2D_mE4D242EDB4345D6FC623E88B53ECC590B5B7A2F8 (void);
// 0x00000044 System.Void Destrutror::.ctor()
extern void Destrutror__ctor_m92579A5DBE9D00AFAD30106A093DE650F862A1BC (void);
// 0x00000045 System.Void Disparo::Start()
extern void Disparo_Start_m0ECDBFACBCABC713D5208A6F070E45E32449606A (void);
// 0x00000046 System.Void Disparo::FixedUpdate()
extern void Disparo_FixedUpdate_mF7DB30AD4C32AB2F96EBD824C44A33DAAEDC6CAB (void);
// 0x00000047 System.Void Disparo::Update()
extern void Disparo_Update_m2CA799DC04953298FB7A06AFD0156B08EE8B9306 (void);
// 0x00000048 System.Void Disparo::OnDrawGizmos()
extern void Disparo_OnDrawGizmos_m7AAAE17FE890BA9322A3E14E4653D3334989F1C0 (void);
// 0x00000049 System.Void Disparo::.ctor()
extern void Disparo__ctor_mBC55BB0F26E7F927F5C4368FB34B452812D63ADF (void);
// 0x0000004A System.Void EfectoParallax::Start()
extern void EfectoParallax_Start_mA21F95890F1A256DA31ACBC8285B73800233DB5F (void);
// 0x0000004B System.Void EfectoParallax::LateUpdate()
extern void EfectoParallax_LateUpdate_mCC8C9758224F9779327713B1016A5EA1679BD122 (void);
// 0x0000004C System.Void EfectoParallax::.ctor()
extern void EfectoParallax__ctor_m2D30262B84F59DEB814A67CBA450A5F896D58C2E (void);
// 0x0000004D System.Void Emision::Start()
extern void Emision_Start_m43580E21BB703F526CE48C5BD4940BF25215131A (void);
// 0x0000004E System.Void Emision::Update()
extern void Emision_Update_m2A3137A540670AD61633C5017B8DD8F598B753DA (void);
// 0x0000004F System.Void Emision::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Emision_OnTriggerEnter2D_m8C93A84DA9602886EF4C73EFD9407487BCAD737D (void);
// 0x00000050 System.Void Emision::Explosion()
extern void Emision_Explosion_m4ED29A98D1E21FF6F19DD8A50446A25A5413FDE9 (void);
// 0x00000051 System.Void Emision::.ctor()
extern void Emision__ctor_mD7AB77DA889D0369233D1FECD2F59277B3CA350D (void);
// 0x00000052 System.Void Emision2::Start()
extern void Emision2_Start_mA3BD410D7721CFD29D9E61B7DD4038A8A8898862 (void);
// 0x00000053 System.Void Emision2::Update()
extern void Emision2_Update_m38811A90CE581C91586890E1C5EA2A30E56B8FCF (void);
// 0x00000054 System.Void Emision2::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Emision2_OnTriggerEnter2D_m270CDAA93E54A2EAE737FB910B7CDB7265AA50AC (void);
// 0x00000055 System.Void Emision2::Explosion()
extern void Emision2_Explosion_mFB4CD601FC6B99F151C8F6DC0543AAAC5D0197C7 (void);
// 0x00000056 System.Void Emision2::.ctor()
extern void Emision2__ctor_mAF808A16DBEAC5037E9757B79DE68D809CA6F49F (void);
// 0x00000057 System.Void Enredadera::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Enredadera_OnTriggerEnter2D_m78C7A0259297468299250DD1F37F2275F356D76D (void);
// 0x00000058 System.Void Enredadera::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Enredadera_OnTriggerExit2D_m2A1C353D2A8AB3B0F1216D1DAEC4712380540F71 (void);
// 0x00000059 System.Void Enredadera::.ctor()
extern void Enredadera__ctor_mEC098001E29867498F08F04414F013A168B37D66 (void);
// 0x0000005A System.Void EntradaSpawn::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void EntradaSpawn_OnTriggerEnter2D_m44CE81607DAE1FD77B80A24A774F3387EBB5045F (void);
// 0x0000005B System.Void EntradaSpawn::.ctor()
extern void EntradaSpawn__ctor_mAE7D0433B473710C09372EEFCCE958800F9AD0F5 (void);
// 0x0000005C System.Void EsporaPlataforma::Start()
extern void EsporaPlataforma_Start_mD39DEA128A19087F4B31C5F4966C642297BEEFA3 (void);
// 0x0000005D System.Void EsporaPlataforma::Update()
extern void EsporaPlataforma_Update_mA183FBDF285F3D043634E0530A032D7A0618458D (void);
// 0x0000005E System.Void EsporaPlataforma::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void EsporaPlataforma_OnTriggerEnter2D_mA215C7D87C50F2F718BADB8F163C7D2AEECBA797 (void);
// 0x0000005F System.Collections.IEnumerator EsporaPlataforma::PlayerArriba()
extern void EsporaPlataforma_PlayerArriba_mC18CA67F1ABF04F16C77688C489BDC2557BFF05F (void);
// 0x00000060 System.Void EsporaPlataforma::Destruir()
extern void EsporaPlataforma_Destruir_m70855487BD35A05CC5B5C08D751414730A2E2395 (void);
// 0x00000061 System.Void EsporaPlataforma::.ctor()
extern void EsporaPlataforma__ctor_m3744439D8416E418A85FA33C55F4B2A6D7E3BAF9 (void);
// 0x00000062 System.Void EsporaPlataforma/<PlayerArriba>d__8::.ctor(System.Int32)
extern void U3CPlayerArribaU3Ed__8__ctor_m299B668FEB88DA495D32B2611BEFCE92196184C8 (void);
// 0x00000063 System.Void EsporaPlataforma/<PlayerArriba>d__8::System.IDisposable.Dispose()
extern void U3CPlayerArribaU3Ed__8_System_IDisposable_Dispose_mAE4D097CFF0CD28C27C270DC6C0234C976BF65B1 (void);
// 0x00000064 System.Boolean EsporaPlataforma/<PlayerArriba>d__8::MoveNext()
extern void U3CPlayerArribaU3Ed__8_MoveNext_mFF52A60CF025F1AE3688E3056312906D1214E4A4 (void);
// 0x00000065 System.Object EsporaPlataforma/<PlayerArriba>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerArribaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m190919B43D3B6C4971D2B06CD2626862F446211E (void);
// 0x00000066 System.Void EsporaPlataforma/<PlayerArriba>d__8::System.Collections.IEnumerator.Reset()
extern void U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_Reset_mD517702B85C053A9320DED1EFF9909B108DD4268 (void);
// 0x00000067 System.Object EsporaPlataforma/<PlayerArriba>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_get_Current_m7DAD340B862A764D7B16D3BDCCBEC9418CBB479D (void);
// 0x00000068 System.Void Estrellas::Start()
extern void Estrellas_Start_m21FAD5DAA28E7A1B8BA7C61BD940B1E3EB817516 (void);
// 0x00000069 System.Void Estrellas::Awake()
extern void Estrellas_Awake_m4B1C9C06979DA365B2CB8C1CEA96C354694EFD67 (void);
// 0x0000006A System.Void Estrellas::Update()
extern void Estrellas_Update_mB589FB355D8B7B5B2039F60674067B881FE85780 (void);
// 0x0000006B System.Collections.IEnumerator Estrellas::Star()
extern void Estrellas_Star_mE2BC7E35D0F8BFD0911800B7E308C46F14C1B285 (void);
// 0x0000006C System.Void Estrellas::.ctor()
extern void Estrellas__ctor_m6A9872B7DECDB4C5329F512EBBA7CD45039AD67D (void);
// 0x0000006D System.Void Estrellas/<Star>d__17::.ctor(System.Int32)
extern void U3CStarU3Ed__17__ctor_mAC92040ACB86FA97FAC8781D22D6CB531D259157 (void);
// 0x0000006E System.Void Estrellas/<Star>d__17::System.IDisposable.Dispose()
extern void U3CStarU3Ed__17_System_IDisposable_Dispose_m26583F744D3AFF0D05AD5D694ACBB24C816EF83D (void);
// 0x0000006F System.Boolean Estrellas/<Star>d__17::MoveNext()
extern void U3CStarU3Ed__17_MoveNext_mDDEEDBCAC1AB2368E58BAB5F8E79B90478FDC029 (void);
// 0x00000070 System.Object Estrellas/<Star>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStarU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EA8D7A57AB7E6EDD3DDD6DA65B8A5E9F6E0840F (void);
// 0x00000071 System.Void Estrellas/<Star>d__17::System.Collections.IEnumerator.Reset()
extern void U3CStarU3Ed__17_System_Collections_IEnumerator_Reset_m3A2F5A257120B3DE339BC03EAB7092076161BDBD (void);
// 0x00000072 System.Object Estrellas/<Star>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CStarU3Ed__17_System_Collections_IEnumerator_get_Current_m01F25A7B6083895784BDE39B6C16C7D953F5451D (void);
// 0x00000073 System.Void GymArte::Start()
extern void GymArte_Start_m64CE42B0135FDA4F026CE7BBAE79ABA0AAEED656 (void);
// 0x00000074 System.Void GymArte::Update()
extern void GymArte_Update_m985D489C15051E37D4A4B6E05112370292E33F6F (void);
// 0x00000075 System.Void GymArte::.ctor()
extern void GymArte__ctor_m00FB0BC61172DB590B76134E17C73DED27F3B987 (void);
// 0x00000076 System.Void Huevo::Awake()
extern void Huevo_Awake_mF4786ACEDB25EC740F588A8E578F485FB4A69664 (void);
// 0x00000077 System.Void Huevo::Start()
extern void Huevo_Start_m774B045179F1D1C968D02DB59E5E2A723421609C (void);
// 0x00000078 System.Void Huevo::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Huevo_OnTriggerEnter2D_mC680CBDE9CF0DEA6588093FBAC1CE830903412C6 (void);
// 0x00000079 System.Void Huevo::Victoria()
extern void Huevo_Victoria_m9707A88A08AA34C47C22CEFF5A5EDF5B1B2400DC (void);
// 0x0000007A System.Void Huevo::.ctor()
extern void Huevo__ctor_m510D44E85D48C26E6F94A377B2568D896B6B006C (void);
// 0x0000007B System.Void KabuBasico::Start()
extern void KabuBasico_Start_mB9802AC5703A95573430505112E53A688CF7B49C (void);
// 0x0000007C System.Void KabuBasico::Update()
extern void KabuBasico_Update_mC6FF6220A8AB41C7C2D3EE7B38151E41E3F0073A (void);
// 0x0000007D System.Collections.IEnumerator KabuBasico::PararYDisparar()
extern void KabuBasico_PararYDisparar_m098F975AB2D83E3736C8D78453D8997DE57DCB07 (void);
// 0x0000007E System.Void KabuBasico::.ctor()
extern void KabuBasico__ctor_m05302C97D7AF46B87BAEF43F55318250EBE46E1F (void);
// 0x0000007F System.Void KabuBasico/<PararYDisparar>d__5::.ctor(System.Int32)
extern void U3CPararYDispararU3Ed__5__ctor_m28B78F6217A3C7D8E4BB4F68EA15F9D5E26F4CB6 (void);
// 0x00000080 System.Void KabuBasico/<PararYDisparar>d__5::System.IDisposable.Dispose()
extern void U3CPararYDispararU3Ed__5_System_IDisposable_Dispose_m3494AFC9EB836374DF75AD4B274764BB53B6E894 (void);
// 0x00000081 System.Boolean KabuBasico/<PararYDisparar>d__5::MoveNext()
extern void U3CPararYDispararU3Ed__5_MoveNext_m4454F74E7DFC2B11059B009EF2E72A681F467550 (void);
// 0x00000082 System.Object KabuBasico/<PararYDisparar>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPararYDispararU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E2003B8D3BE6EF61331DB120E9AE8BB915C712F (void);
// 0x00000083 System.Void KabuBasico/<PararYDisparar>d__5::System.Collections.IEnumerator.Reset()
extern void U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_Reset_mF0BA0302C68431F2AFFB14674A875A6AF12F6884 (void);
// 0x00000084 System.Object KabuBasico/<PararYDisparar>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_get_Current_m25FE5DDBAB2FC720A89EF7D744E38A3A77E40DCC (void);
// 0x00000085 System.Void KabuEmisor::Start()
extern void KabuEmisor_Start_m7BA3116646417DF9330F6546A7B3E8F0A6641DC3 (void);
// 0x00000086 System.Void KabuEmisor::Update()
extern void KabuEmisor_Update_m7A764E859CA7EA31BD45C66DD8A1E096A42A7F80 (void);
// 0x00000087 System.Collections.IEnumerator KabuEmisor::PararYDisparar()
extern void KabuEmisor_PararYDisparar_m28F90EA2256A32A214247BA59F0C5FF70D96E29F (void);
// 0x00000088 System.Void KabuEmisor::OnDestroy()
extern void KabuEmisor_OnDestroy_m4DF76AF772924FAD743C486F548682568AA9873F (void);
// 0x00000089 System.Void KabuEmisor::.ctor()
extern void KabuEmisor__ctor_m3255FDD102106E951261CD91610BD90B7FD1B8EA (void);
// 0x0000008A System.Void KabuEmisor/<PararYDisparar>d__12::.ctor(System.Int32)
extern void U3CPararYDispararU3Ed__12__ctor_m2D700458181046A503F701ADFC8BA8DC9D3A9EB9 (void);
// 0x0000008B System.Void KabuEmisor/<PararYDisparar>d__12::System.IDisposable.Dispose()
extern void U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m7B7085B9C5D6AE8BAFC79EE702AAD249E6997E9D (void);
// 0x0000008C System.Boolean KabuEmisor/<PararYDisparar>d__12::MoveNext()
extern void U3CPararYDispararU3Ed__12_MoveNext_mD3799453B2844BF1A6FAB366F0FC490BC77DF120 (void);
// 0x0000008D System.Object KabuEmisor/<PararYDisparar>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6799E0EB5EAFE14DADFD640BB72A0A09470B96C (void);
// 0x0000008E System.Void KabuEmisor/<PararYDisparar>d__12::System.Collections.IEnumerator.Reset()
extern void U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mF54B2836FF863E4F1CDF46F6498B9E100C6E4FE7 (void);
// 0x0000008F System.Object KabuEmisor/<PararYDisparar>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_mF32B2BB1AD709293B55180110157472741FCCA06 (void);
// 0x00000090 System.Void KabuEmisorRafaga::Start()
extern void KabuEmisorRafaga_Start_m6BCB064C7F018898C123D6C29D52579046FCF730 (void);
// 0x00000091 System.Void KabuEmisorRafaga::Update()
extern void KabuEmisorRafaga_Update_m0EB4EF4D47F090E9C5E7F4B90C7E12383F1E100A (void);
// 0x00000092 System.Collections.IEnumerator KabuEmisorRafaga::PararYDisparar()
extern void KabuEmisorRafaga_PararYDisparar_m09C14B3FB2BFFE4EB0CF8E22B11D8C83820993F0 (void);
// 0x00000093 System.Void KabuEmisorRafaga::OnDestroy()
extern void KabuEmisorRafaga_OnDestroy_m5DAFE634D96C0F55A0D8330C352BEC82657C2A09 (void);
// 0x00000094 System.Void KabuEmisorRafaga::.ctor()
extern void KabuEmisorRafaga__ctor_m550B1E4C9FDBF722E0BDE49D37884777B4143E75 (void);
// 0x00000095 System.Void KabuEmisorRafaga/<PararYDisparar>d__12::.ctor(System.Int32)
extern void U3CPararYDispararU3Ed__12__ctor_mF857BF4F98F66D816DFF39B81B6DF35DA228D900 (void);
// 0x00000096 System.Void KabuEmisorRafaga/<PararYDisparar>d__12::System.IDisposable.Dispose()
extern void U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m5259FEE7B1C494B388D7E2FC7D7D9476BC4A10CD (void);
// 0x00000097 System.Boolean KabuEmisorRafaga/<PararYDisparar>d__12::MoveNext()
extern void U3CPararYDispararU3Ed__12_MoveNext_m9D81A42D6A10E3DE69909EBD269A1F442D96CF45 (void);
// 0x00000098 System.Object KabuEmisorRafaga/<PararYDisparar>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9C65E1438072C80B375B1894990FFC643263294 (void);
// 0x00000099 System.Void KabuEmisorRafaga/<PararYDisparar>d__12::System.Collections.IEnumerator.Reset()
extern void U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mA55C1772F133A8694C145E9CE918316A12606C97 (void);
// 0x0000009A System.Object KabuEmisorRafaga/<PararYDisparar>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_m0B368BD51F913A2888E0C3E70EEBE509DFA14945 (void);
// 0x0000009B System.Void KabuOnda::Start()
extern void KabuOnda_Start_mB43520216374BD9B76CADB542924512CE6959B75 (void);
// 0x0000009C System.Collections.IEnumerator KabuOnda::MoverIzqDr()
extern void KabuOnda_MoverIzqDr_m4FE6E82C5F835C766A913EEB83567A117E4E710F (void);
// 0x0000009D System.Void KabuOnda::Sonido()
extern void KabuOnda_Sonido_mBFF25C329A9ECE4D775D65365E89D2922A6BCD6B (void);
// 0x0000009E System.Void KabuOnda::.ctor()
extern void KabuOnda__ctor_m9181A42449B0F21663C28C20FE45A21A907C5F5B (void);
// 0x0000009F System.Void KabuOnda/<MoverIzqDr>d__3::.ctor(System.Int32)
extern void U3CMoverIzqDrU3Ed__3__ctor_m1F2F458C54014365FD64467AEB95A749273D292B (void);
// 0x000000A0 System.Void KabuOnda/<MoverIzqDr>d__3::System.IDisposable.Dispose()
extern void U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_mEE21FEE22A97265ACCD39394E3894557F0E4942C (void);
// 0x000000A1 System.Boolean KabuOnda/<MoverIzqDr>d__3::MoveNext()
extern void U3CMoverIzqDrU3Ed__3_MoveNext_mD2A77E812412F04568D7763AE82EC984617F5C90 (void);
// 0x000000A2 System.Object KabuOnda/<MoverIzqDr>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10A5F520A464818C6522A42F2DC28C5769C38401 (void);
// 0x000000A3 System.Void KabuOnda/<MoverIzqDr>d__3::System.Collections.IEnumerator.Reset()
extern void U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mFB3F2FF2AEF004BC485745191C954172F0C3C3FB (void);
// 0x000000A4 System.Object KabuOnda/<MoverIzqDr>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_m104439B07E2DDEB5A7A71252CF2073525EF3EDD3 (void);
// 0x000000A5 System.Void Limite::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Limite_OnTriggerEnter2D_mACB2BD02E93EB41F2161F38EC58B3D4B0AE03909 (void);
// 0x000000A6 System.Void Limite::.ctor()
extern void Limite__ctor_mDF58361B1CFB459A25077872F9E30DDA4D30062B (void);
// 0x000000A7 System.Void MenuDePausa::Awake()
extern void MenuDePausa_Awake_mD5AD2922E4B442FA8932B341A4D03A8E5F1CCC56 (void);
// 0x000000A8 System.Void MenuDePausa::Start()
extern void MenuDePausa_Start_m8BEADAF1034AA545CB5CF10D0FA69DDBDAA8D572 (void);
// 0x000000A9 System.Void MenuDePausa::Update()
extern void MenuDePausa_Update_m30F445189721D5DDCF74D159A0A2C47DADA4DC51 (void);
// 0x000000AA System.Void MenuDePausa::Pause()
extern void MenuDePausa_Pause_mC660B479C0E8FABA3E5B7C1A8DE1FA36FE5C9993 (void);
// 0x000000AB System.Void MenuDePausa::Resume()
extern void MenuDePausa_Resume_m88FAE5DC414A903B6B079CBC6F0071E56FB2EBC7 (void);
// 0x000000AC System.Void MenuDePausa::QuitarSeleccion()
extern void MenuDePausa_QuitarSeleccion_m646AC1B84664B08DFDC364C82C9BFB1B3E2D99F9 (void);
// 0x000000AD System.Void MenuDePausa::.ctor()
extern void MenuDePausa__ctor_mBBDD90D3C9C732C6630F9ABDE73DA58421D8775C (void);
// 0x000000AE System.Void MenuDePausa::.cctor()
extern void MenuDePausa__cctor_m507B5CAB8DE74A4F0E7CCA34B2FB2E145D3E04DD (void);
// 0x000000AF System.Void MenuManager::Start()
extern void MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2 (void);
// 0x000000B0 System.Void MenuManager::Update()
extern void MenuManager_Update_m732CF78C4240CF880048E66B580BE32A72FB8779 (void);
// 0x000000B1 System.Void MenuManager::SceneToLoad(System.String)
extern void MenuManager_SceneToLoad_m8606731327B8C5EDC227C04CC88E74F137FFB32F (void);
// 0x000000B2 System.Void MenuManager::CloseGame()
extern void MenuManager_CloseGame_mB38B520F96BA7471AF49700A240CD2F870D717C9 (void);
// 0x000000B3 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x000000B4 System.Void MovimientoKabu::Update()
extern void MovimientoKabu_Update_m06F86C6407ACB772186EA9F14F88A9CD908F7877 (void);
// 0x000000B5 System.Void MovimientoKabu::.ctor()
extern void MovimientoKabu__ctor_mFE08588578A6189AB7F233B37D8CDAC4B0C43C33 (void);
// 0x000000B6 System.Void MovimientoSeta::Start()
extern void MovimientoSeta_Start_m7CDF7FEFD7BAEA0FC170780B463EDFB06DCB3982 (void);
// 0x000000B7 System.Collections.IEnumerator MovimientoSeta::MoverIzqDr()
extern void MovimientoSeta_MoverIzqDr_mDD6840527C7DE1E9CE743CEB3B1498BA712792C1 (void);
// 0x000000B8 System.Void MovimientoSeta::.ctor()
extern void MovimientoSeta__ctor_mC8AD7F4B7BE6CAA9EF14DC75B2C4C2DCFE184B3A (void);
// 0x000000B9 System.Void MovimientoSeta/<MoverIzqDr>d__3::.ctor(System.Int32)
extern void U3CMoverIzqDrU3Ed__3__ctor_m96CB5E50F03A4DA2462445C3D8C9B0C4954BFD1A (void);
// 0x000000BA System.Void MovimientoSeta/<MoverIzqDr>d__3::System.IDisposable.Dispose()
extern void U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_m2C33D7423D8CF1A4186256039D15E77B6E68C72B (void);
// 0x000000BB System.Boolean MovimientoSeta/<MoverIzqDr>d__3::MoveNext()
extern void U3CMoverIzqDrU3Ed__3_MoveNext_m38555BA9F8673069D41C0EEF248A7DCAC4C0369D (void);
// 0x000000BC System.Object MovimientoSeta/<MoverIzqDr>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA534FFD4507218231C17A4A0D559A5ABB5B9ECA3 (void);
// 0x000000BD System.Void MovimientoSeta/<MoverIzqDr>d__3::System.Collections.IEnumerator.Reset()
extern void U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mF33DE026F075998BD681BB1C01F0C891CCA9EEA7 (void);
// 0x000000BE System.Object MovimientoSeta/<MoverIzqDr>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_mBB7FCDF227060896BDC0F40A549A034C56C875B4 (void);
// 0x000000BF System.Void MuerteManager::Start()
extern void MuerteManager_Start_m35B338B8BDD6E3EBEA20269875329C697CD6644D (void);
// 0x000000C0 System.Void MuerteManager::Update()
extern void MuerteManager_Update_mA9F18861C409A80C2845B915832CE1BBFC84B4ED (void);
// 0x000000C1 System.Void MuerteManager::.ctor()
extern void MuerteManager__ctor_m51E663102FE4B66F1B0D775BF09E635F05FFD40B (void);
// 0x000000C2 System.Void NubeEspora::Start()
extern void NubeEspora_Start_m1053F2F10272993E61466433B6714839F0B45C59 (void);
// 0x000000C3 System.Void NubeEspora::Update()
extern void NubeEspora_Update_m42099376B17BB3CA1828EA172D412D08579DE5F9 (void);
// 0x000000C4 System.Void NubeEspora::Desvanecer()
extern void NubeEspora_Desvanecer_m6058CBCA5409F686B5391718B2240FEBC709D73A (void);
// 0x000000C5 System.Void NubeEspora::.ctor()
extern void NubeEspora__ctor_m7EDA7516D7A857263ADD3E6A8427B58B2864DE4A (void);
// 0x000000C6 System.Void Parallax::Start()
extern void Parallax_Start_m7C7DC681755C4BBB7BF918CC67A2A5F2360F5DF2 (void);
// 0x000000C7 System.Void Parallax::Update()
extern void Parallax_Update_m208D10754A83F3949A0DE958EBB01C4B63BC337B (void);
// 0x000000C8 System.Void Parallax::.ctor()
extern void Parallax__ctor_m0690485E7F8E13945AF42671FF1C937CB60D4B36 (void);
// 0x000000C9 System.Void Pause::Awake()
extern void Pause_Awake_m67921AD16309350CBDAD9823D8AA58CDCE8C3A1E (void);
// 0x000000CA System.Void Pause::Start()
extern void Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC (void);
// 0x000000CB System.Void Pause::Update()
extern void Pause_Update_m2BEBA55A60F1B469B29DD631505236F1B5F21FEE (void);
// 0x000000CC System.Void Pause::.ctor()
extern void Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928 (void);
// 0x000000CD System.Void Pez::Start()
extern void Pez_Start_m145DB705FFF71DFC6419F539E8D2D51007D89BAF (void);
// 0x000000CE System.Void Pez::Update()
extern void Pez_Update_mFA0AC730C36918F01AAAD2D4C6C80D082A0E5E2B (void);
// 0x000000CF System.Collections.IEnumerator Pez::Salta()
extern void Pez_Salta_m871747B9548F5058B2B6F71E0402E979E21F1651 (void);
// 0x000000D0 System.Void Pez::.ctor()
extern void Pez__ctor_m24A360F4612947A601A6479D7CAC3C8E84E175F7 (void);
// 0x000000D1 System.Void Pez/<Salta>d__10::.ctor(System.Int32)
extern void U3CSaltaU3Ed__10__ctor_m0307177566364F8231E65778A7EFF36B440BDA85 (void);
// 0x000000D2 System.Void Pez/<Salta>d__10::System.IDisposable.Dispose()
extern void U3CSaltaU3Ed__10_System_IDisposable_Dispose_mEACEE99BB082F725001956990EE7532B0F101703 (void);
// 0x000000D3 System.Boolean Pez/<Salta>d__10::MoveNext()
extern void U3CSaltaU3Ed__10_MoveNext_m4A6CA4C799B91369C23E64BB2E256CEBF671218F (void);
// 0x000000D4 System.Object Pez/<Salta>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaltaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED10B84D5D0F45C7D0218B89912F50347CFCD0A2 (void);
// 0x000000D5 System.Void Pez/<Salta>d__10::System.Collections.IEnumerator.Reset()
extern void U3CSaltaU3Ed__10_System_Collections_IEnumerator_Reset_m0CAB56F87D69775FDBA924C157D8BA9984D6FCA0 (void);
// 0x000000D6 System.Object Pez/<Salta>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CSaltaU3Ed__10_System_Collections_IEnumerator_get_Current_mD7CB6FAB5E7BD41C0EC578DE9FA4A185A778138F (void);
// 0x000000D7 System.Collections.IEnumerator PlantaCarnivora::Plantacarnivora()
extern void PlantaCarnivora_Plantacarnivora_m615E1F73C291B71F36330C85ADEA77064B5F00EA (void);
// 0x000000D8 System.Void PlantaCarnivora::Start()
extern void PlantaCarnivora_Start_mA4386071B70A0DF9B314BE9FA6C4661D0D7EF332 (void);
// 0x000000D9 System.Void PlantaCarnivora::.ctor()
extern void PlantaCarnivora__ctor_m600A8FB3DB69284DBC2FFEFA28040DB21004DD89 (void);
// 0x000000DA System.Void PlantaCarnivora/<Plantacarnivora>d__3::.ctor(System.Int32)
extern void U3CPlantacarnivoraU3Ed__3__ctor_mA1FD778DB89B77632E8E9FAC03AA6CADB30A6262 (void);
// 0x000000DB System.Void PlantaCarnivora/<Plantacarnivora>d__3::System.IDisposable.Dispose()
extern void U3CPlantacarnivoraU3Ed__3_System_IDisposable_Dispose_mEE53C5431D11E1570FB3E2B3FD320C948929A789 (void);
// 0x000000DC System.Boolean PlantaCarnivora/<Plantacarnivora>d__3::MoveNext()
extern void U3CPlantacarnivoraU3Ed__3_MoveNext_m7A6A70AFAA7D5C152C8A1E062222AA93DF1047F1 (void);
// 0x000000DD System.Object PlantaCarnivora/<Plantacarnivora>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlantacarnivoraU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26A31B3F8FA250958606C9826236F2F2F49DE751 (void);
// 0x000000DE System.Void PlantaCarnivora/<Plantacarnivora>d__3::System.Collections.IEnumerator.Reset()
extern void U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_Reset_mD90975CD3BF73D486C1E5E51D671E7C066164778 (void);
// 0x000000DF System.Object PlantaCarnivora/<Plantacarnivora>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_get_Current_mE44BC670E424CB47A3392906F03C1870F5810EED (void);
// 0x000000E0 System.Void PlantaEspora::Start()
extern void PlantaEspora_Start_mB303D2C6E4AD8B8B1A9C80DEA6596EA9C9FD7EFD (void);
// 0x000000E1 System.Void PlantaEspora::Update()
extern void PlantaEspora_Update_mFC1D93A7452CD4EF2D76B4920A4AF78EC0B3F52F (void);
// 0x000000E2 System.Collections.IEnumerator PlantaEspora::InstanciaEsporas()
extern void PlantaEspora_InstanciaEsporas_m206FCB2862197C6D0A89C8315150CF66AD64B9D6 (void);
// 0x000000E3 System.Void PlantaEspora::Instancia()
extern void PlantaEspora_Instancia_m936ACC4AA4FDDF2E58F201DF1CE9651281833163 (void);
// 0x000000E4 System.Void PlantaEspora::.ctor()
extern void PlantaEspora__ctor_m666F38ED96ACFC96FBECC9573559BBB903D08FBE (void);
// 0x000000E5 System.Void PlantaEspora/<InstanciaEsporas>d__6::.ctor(System.Int32)
extern void U3CInstanciaEsporasU3Ed__6__ctor_mD138B51F39DA5DED79627D873CA8864FED69952A (void);
// 0x000000E6 System.Void PlantaEspora/<InstanciaEsporas>d__6::System.IDisposable.Dispose()
extern void U3CInstanciaEsporasU3Ed__6_System_IDisposable_Dispose_m2A6E5460A7B2CAED82982944943BAC197A5276D4 (void);
// 0x000000E7 System.Boolean PlantaEspora/<InstanciaEsporas>d__6::MoveNext()
extern void U3CInstanciaEsporasU3Ed__6_MoveNext_mC2ED3711DC7734CF5E908AF500FB9A4D58F156A3 (void);
// 0x000000E8 System.Object PlantaEspora/<InstanciaEsporas>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInstanciaEsporasU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8300C6B902901FDBD298B132B2EE4F344C8BE0B4 (void);
// 0x000000E9 System.Void PlantaEspora/<InstanciaEsporas>d__6::System.Collections.IEnumerator.Reset()
extern void U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_Reset_m88576E6176233477BD4612172744765EAE8E413B (void);
// 0x000000EA System.Object PlantaEspora/<InstanciaEsporas>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_get_Current_mBB6C82B823E03BAC16DD428ED16B53F003CE7D37 (void);
// 0x000000EB System.Void PlataformaCae::Start()
extern void PlataformaCae_Start_m47ED6A173654300E986E9F74EE5583ED2F718E28 (void);
// 0x000000EC System.Void PlataformaCae::Update()
extern void PlataformaCae_Update_mB975C521BA2883009FD7B06BB183A7034D8B7AF1 (void);
// 0x000000ED System.Void PlataformaCae::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlataformaCae_OnTriggerEnter2D_m81ABE4EDC45F9ECC3F5DEC2CF0DCC0D5306CA5F9 (void);
// 0x000000EE System.Collections.IEnumerator PlataformaCae::PlayerArriba()
extern void PlataformaCae_PlayerArriba_m6B299C877DDF407B84EB3D2843A319570A3F7314 (void);
// 0x000000EF System.Void PlataformaCae::Destruir()
extern void PlataformaCae_Destruir_mE966C31BD9E2B4463E479F225B98B4660887943E (void);
// 0x000000F0 System.Void PlataformaCae::.ctor()
extern void PlataformaCae__ctor_m6F893A5723384822637A191AC304CDE117AF12F5 (void);
// 0x000000F1 System.Void PlataformaCae/<PlayerArriba>d__10::.ctor(System.Int32)
extern void U3CPlayerArribaU3Ed__10__ctor_m1CC2DF67A3E6CEB1DBFE36F189607B2886A1BDD3 (void);
// 0x000000F2 System.Void PlataformaCae/<PlayerArriba>d__10::System.IDisposable.Dispose()
extern void U3CPlayerArribaU3Ed__10_System_IDisposable_Dispose_m654D1CD2F1EF73741B21CBAE8602DD3C23D957F9 (void);
// 0x000000F3 System.Boolean PlataformaCae/<PlayerArriba>d__10::MoveNext()
extern void U3CPlayerArribaU3Ed__10_MoveNext_mE32EF6FD670C77E8B1C38E8E235154A06A21A49D (void);
// 0x000000F4 System.Object PlataformaCae/<PlayerArriba>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerArribaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m778E4A1A205EAC3F0ED1CFDBAB69A619839BDA8E (void);
// 0x000000F5 System.Void PlataformaCae/<PlayerArriba>d__10::System.Collections.IEnumerator.Reset()
extern void U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_Reset_m621E3B3090BDE42B7043AE2ABA1843E7D2E75907 (void);
// 0x000000F6 System.Object PlataformaCae/<PlayerArriba>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_get_Current_m935DF9075C85D1D5204F3CBE076F59C6040CCD27 (void);
// 0x000000F7 System.Void Pocion::Awake()
extern void Pocion_Awake_m2DE17FDAD35AF8A8C813FBAECF62461C3FE43B0B (void);
// 0x000000F8 System.Void Pocion::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Pocion_OnTriggerEnter2D_m14B967C04D892BAC20C46D6B4DF68B2A150CEDB8 (void);
// 0x000000F9 System.Void Pocion::.ctor()
extern void Pocion__ctor_m15F926FA3C39FB309F8CE8AF761058B316A96399 (void);
// 0x000000FA System.Void Salamandra::Start()
extern void Salamandra_Start_m4EC24CE300740534443B873D561095CF27576D0F (void);
// 0x000000FB System.Void Salamandra::Update()
extern void Salamandra_Update_mCF0469A18C7CF95C9D22AE05C2663838DB6DA604 (void);
// 0x000000FC System.Collections.IEnumerator Salamandra::DisparaSalta()
extern void Salamandra_DisparaSalta_m313C54FB7866FB593AB9DD00299606A15951E5FA (void);
// 0x000000FD System.Void Salamandra::OnFinishAnim()
extern void Salamandra_OnFinishAnim_m710220CC12607F986D12A4BC4CC2DA2B5813EDE1 (void);
// 0x000000FE System.Void Salamandra::.ctor()
extern void Salamandra__ctor_mBB8EDD97DBAE5EE68BBAAC2FA195B44758E6AEDC (void);
// 0x000000FF System.Void Salamandra/<DisparaSalta>d__11::.ctor(System.Int32)
extern void U3CDisparaSaltaU3Ed__11__ctor_mD9001ABB51207AF811B15948966A3EAE6D1730AC (void);
// 0x00000100 System.Void Salamandra/<DisparaSalta>d__11::System.IDisposable.Dispose()
extern void U3CDisparaSaltaU3Ed__11_System_IDisposable_Dispose_m5CC9EBA93013AD7E12FC6C0137613B304F6970FF (void);
// 0x00000101 System.Boolean Salamandra/<DisparaSalta>d__11::MoveNext()
extern void U3CDisparaSaltaU3Ed__11_MoveNext_m59383403DCB3466073644DE905F143CFEF695799 (void);
// 0x00000102 System.Object Salamandra/<DisparaSalta>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisparaSaltaU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03C65B1CD417703E42F52AB854B415BCEE0E547D (void);
// 0x00000103 System.Void Salamandra/<DisparaSalta>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_Reset_mCCE91D7C239B131EB0F3E1A48DFF350E49D8B719 (void);
// 0x00000104 System.Object Salamandra/<DisparaSalta>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_get_Current_m069AD940AD90A91ABEF88800EFD5446C7C2FB455 (void);
// 0x00000105 System.Void SalidaSpawn::Start()
extern void SalidaSpawn_Start_m8F123A52984AF96BC794EAEDCDEE9462DC5D3E4C (void);
// 0x00000106 System.Void SalidaSpawn::Update()
extern void SalidaSpawn_Update_mC06D209A158087BC90515A579209BA5E576B86B9 (void);
// 0x00000107 System.Void SalidaSpawn::.ctor()
extern void SalidaSpawn__ctor_m360436F0C2797BED5D11E911C92D83BDAF997509 (void);
// 0x00000108 System.Void SetaExplosiva::Awake()
extern void SetaExplosiva_Awake_m430E12E5120193DD6DAA4C511E3C5555CD9E6A0E (void);
// 0x00000109 System.Void SetaExplosiva::Update()
extern void SetaExplosiva_Update_m5017F2CB0730ACA3740185763BC6B110839E44E2 (void);
// 0x0000010A System.Collections.IEnumerator SetaExplosiva::MoveExpl(UnityEngine.Vector2)
extern void SetaExplosiva_MoveExpl_mB966BBEBABB361812827BB32A8B975949A978756 (void);
// 0x0000010B System.Void SetaExplosiva::Explosion()
extern void SetaExplosiva_Explosion_m335D79B75A79ECC63DC724CF1D5ECA53F777BBAD (void);
// 0x0000010C System.Void SetaExplosiva::OnDrawGizmos()
extern void SetaExplosiva_OnDrawGizmos_m1C5CCAFEB42B83776472EB883227A760834509D8 (void);
// 0x0000010D System.Void SetaExplosiva::.ctor()
extern void SetaExplosiva__ctor_mBFE3E46A55F4339A7832F56BA549A6A27DB10B52 (void);
// 0x0000010E System.Void SetaExplosiva/<MoveExpl>d__20::.ctor(System.Int32)
extern void U3CMoveExplU3Ed__20__ctor_m1AD6BC85565B85E3097D42B879DECCC2ECF3EA19 (void);
// 0x0000010F System.Void SetaExplosiva/<MoveExpl>d__20::System.IDisposable.Dispose()
extern void U3CMoveExplU3Ed__20_System_IDisposable_Dispose_mB6769B0CE3DFAD199D3175811B6FAE67C028085B (void);
// 0x00000110 System.Boolean SetaExplosiva/<MoveExpl>d__20::MoveNext()
extern void U3CMoveExplU3Ed__20_MoveNext_mC1A9618B27C4C954188C014326580DFE2CC4131D (void);
// 0x00000111 System.Object SetaExplosiva/<MoveExpl>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveExplU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0289ACBC55AC3361224DFCBCDC5E240F82DD30F5 (void);
// 0x00000112 System.Void SetaExplosiva/<MoveExpl>d__20::System.Collections.IEnumerator.Reset()
extern void U3CMoveExplU3Ed__20_System_Collections_IEnumerator_Reset_m27878F4BCBD7CA8ACF397C1E2B0F76C8327E4182 (void);
// 0x00000113 System.Object SetaExplosiva/<MoveExpl>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CMoveExplU3Ed__20_System_Collections_IEnumerator_get_Current_m79B6D36E92E37A5F984F552BCA6C96B736848F12 (void);
// 0x00000114 System.Void SpawnBurbujas::Start()
extern void SpawnBurbujas_Start_mDCC654B2468F663B213C170CF0C9F83D89E6DFAF (void);
// 0x00000115 System.Collections.IEnumerator SpawnBurbujas::InstanciaVoladores()
extern void SpawnBurbujas_InstanciaVoladores_m8E4D3E9E79075B79CC7764F778B63845ABE86DC2 (void);
// 0x00000116 System.Void SpawnBurbujas::.ctor()
extern void SpawnBurbujas__ctor_m6D5B2D7545045F7C0541B988CD4F62E2A2CA54A9 (void);
// 0x00000117 System.Void SpawnBurbujas/<InstanciaVoladores>d__5::.ctor(System.Int32)
extern void U3CInstanciaVoladoresU3Ed__5__ctor_m566BC2C949479B61D744B9C4A6F22A77258986EC (void);
// 0x00000118 System.Void SpawnBurbujas/<InstanciaVoladores>d__5::System.IDisposable.Dispose()
extern void U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mF94EEE8EE391F6B50244657DB59C85F52537E58A (void);
// 0x00000119 System.Boolean SpawnBurbujas/<InstanciaVoladores>d__5::MoveNext()
extern void U3CInstanciaVoladoresU3Ed__5_MoveNext_m67699B5998E2684BF1D27C49D43DC12AF0792BC2 (void);
// 0x0000011A System.Object SpawnBurbujas/<InstanciaVoladores>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5F857AE3CAD2009297B73E6FF08D60551492A68 (void);
// 0x0000011B System.Void SpawnBurbujas/<InstanciaVoladores>d__5::System.Collections.IEnumerator.Reset()
extern void U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mB9573B135681505A6F5D8DE51C2BA457F86B8B39 (void);
// 0x0000011C System.Object SpawnBurbujas/<InstanciaVoladores>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_mCDC3EFAE58163D472B6C186624B99AF52E770AD4 (void);
// 0x0000011D System.Void SpawnEnemigos::Start()
extern void SpawnEnemigos_Start_m59D6028B21239E2C22A624A745223A7945580D43 (void);
// 0x0000011E System.Void SpawnEnemigos::Update()
extern void SpawnEnemigos_Update_mE7B9641B26F3185435E3F438538594C2500DC598 (void);
// 0x0000011F System.Void SpawnEnemigos::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void SpawnEnemigos_OnTriggerEnter2D_m6DA9C84FB2675476B4AB78D434FA45BB6D345E66 (void);
// 0x00000120 System.Void SpawnEnemigos::KabuAnimacion()
extern void SpawnEnemigos_KabuAnimacion_m1FF600F410927329C9B7D171948789D8567E5091 (void);
// 0x00000121 System.Void SpawnEnemigos::EliminarKabu()
extern void SpawnEnemigos_EliminarKabu_m467B8F19DBE16AB3747F97A8BB7BE87872734514 (void);
// 0x00000122 System.Void SpawnEnemigos::SonidoTronco()
extern void SpawnEnemigos_SonidoTronco_m570BA303D07A54449533F76D1D34048BC345E3B0 (void);
// 0x00000123 System.Void SpawnEnemigos::InstanciarKabu()
extern void SpawnEnemigos_InstanciarKabu_mB1C601B73B9129830825914634ED807537C687A3 (void);
// 0x00000124 System.Void SpawnEnemigos::DestruirSpawn()
extern void SpawnEnemigos_DestruirSpawn_m1163BAC679DFDEA0C9B9E0B11B2D51A167ABA13F (void);
// 0x00000125 System.Void SpawnEnemigos::.ctor()
extern void SpawnEnemigos__ctor_mFC4D5E2344D8CCE500CCB5D5A378CB00D477D4F9 (void);
// 0x00000126 System.Void SpawnFinal::Start()
extern void SpawnFinal_Start_m4DACB494DF2056617D135907F62C5D95FF0E6DBE (void);
// 0x00000127 System.Void SpawnFinal::Update()
extern void SpawnFinal_Update_mD8812F2C3C836EEF8F32B0DF9A6E77C57801AE38 (void);
// 0x00000128 System.Void SpawnFinal::.ctor()
extern void SpawnFinal__ctor_mAD6B5887960879435071FDA0C134E1939914BAE6 (void);
// 0x00000129 System.Void SpawnManager::Start()
extern void SpawnManager_Start_mFA050B5C59C7CC47F4430CA0F5A646051D5442CB (void);
// 0x0000012A System.Void SpawnManager::Update()
extern void SpawnManager_Update_m68C1739F474F0D29A0BF533BD44BA82786A3C96B (void);
// 0x0000012B System.Void SpawnManager::RestablecerSpawn()
extern void SpawnManager_RestablecerSpawn_m287AB9D5AE01BA9903B2CBFF7C402AFF9885795A (void);
// 0x0000012C System.Void SpawnManager::.ctor()
extern void SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59 (void);
// 0x0000012D System.Void SpawnPlanta::Start()
extern void SpawnPlanta_Start_mEB3EB86177E92775F735E24A841D8FF2A924CC5F (void);
// 0x0000012E System.Void SpawnPlanta::Update()
extern void SpawnPlanta_Update_m5CFA7377FC579931ABE23E273541732471329BB6 (void);
// 0x0000012F System.Void SpawnPlanta::CheckIfIsNull()
extern void SpawnPlanta_CheckIfIsNull_mFAE920114611373AF412FA4ED339C631CFCEC85D (void);
// 0x00000130 System.Collections.IEnumerator SpawnPlanta::Spawn()
extern void SpawnPlanta_Spawn_m9A239121B02E5C48C32F6A03109EA6B985D31778 (void);
// 0x00000131 System.Void SpawnPlanta::.ctor()
extern void SpawnPlanta__ctor_m7C759E4D7FF0C67D17935931DBCE8E25F888288E (void);
// 0x00000132 System.Void SpawnPlanta/<Spawn>d__7::.ctor(System.Int32)
extern void U3CSpawnU3Ed__7__ctor_m8F2FCB7CD60C3C16C3684449CC19D8A62C0FB30C (void);
// 0x00000133 System.Void SpawnPlanta/<Spawn>d__7::System.IDisposable.Dispose()
extern void U3CSpawnU3Ed__7_System_IDisposable_Dispose_mDD5B5C4073274DD53D03539305093E05A62ABB80 (void);
// 0x00000134 System.Boolean SpawnPlanta/<Spawn>d__7::MoveNext()
extern void U3CSpawnU3Ed__7_MoveNext_m82D7F6B030003F6E7340BFC57532380BED2A61C9 (void);
// 0x00000135 System.Object SpawnPlanta/<Spawn>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41D02349281E8475CBA467DAD138C068AB0B46B2 (void);
// 0x00000136 System.Void SpawnPlanta/<Spawn>d__7::System.Collections.IEnumerator.Reset()
extern void U3CSpawnU3Ed__7_System_Collections_IEnumerator_Reset_m725CDFFF87F29DF99BD34EC091B742DE77E0C331 (void);
// 0x00000137 System.Object SpawnPlanta/<Spawn>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnU3Ed__7_System_Collections_IEnumerator_get_Current_mE08A8F64462C87D5CA0515DEA6AD51C86A7EC342 (void);
// 0x00000138 System.Void SpawnVoladores::Start()
extern void SpawnVoladores_Start_mA03B682121F1F2663C67F4D8E5E5251AA5068C6B (void);
// 0x00000139 System.Collections.IEnumerator SpawnVoladores::InstanciaVoladores()
extern void SpawnVoladores_InstanciaVoladores_m5D18C26DBDBE5B44417CC3D1FC50C8F2DC89BFF7 (void);
// 0x0000013A System.Void SpawnVoladores::.ctor()
extern void SpawnVoladores__ctor_m04E7A18A9DD082BA5D86FF9B440D1CC168028A57 (void);
// 0x0000013B System.Void SpawnVoladores/<InstanciaVoladores>d__5::.ctor(System.Int32)
extern void U3CInstanciaVoladoresU3Ed__5__ctor_m6A47693939320F7DACB1B4C4CBDB197C79CA6D80 (void);
// 0x0000013C System.Void SpawnVoladores/<InstanciaVoladores>d__5::System.IDisposable.Dispose()
extern void U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mA4EC4C5F217E444594EB675DE05504890EDDA72C (void);
// 0x0000013D System.Boolean SpawnVoladores/<InstanciaVoladores>d__5::MoveNext()
extern void U3CInstanciaVoladoresU3Ed__5_MoveNext_m0A45830AEF008E451BFAF01FD0B8C5667610EA4E (void);
// 0x0000013E System.Object SpawnVoladores/<InstanciaVoladores>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF179B25FE4C16D4D2884B3FA9667F13E64D8A246 (void);
// 0x0000013F System.Void SpawnVoladores/<InstanciaVoladores>d__5::System.Collections.IEnumerator.Reset()
extern void U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mA33C0021369B8C22FEE3E1FFA897E2F744C5C139 (void);
// 0x00000140 System.Object SpawnVoladores/<InstanciaVoladores>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_m717DEBC2BB5F4397E38DDBAF05883A8899CE1ECC (void);
// 0x00000141 System.Void Tiempo::Awake()
extern void Tiempo_Awake_m584A2ED078D185D377441DD5A83C125CF70F8215 (void);
// 0x00000142 System.Void Tiempo::Start()
extern void Tiempo_Start_m80358B28A5CAF70175E75B14DC455AF2907D8A89 (void);
// 0x00000143 System.Void Tiempo::Update()
extern void Tiempo_Update_m6660FE1492BE781F3FA7D73D4E8182837D8C1A3E (void);
// 0x00000144 System.Void Tiempo::.ctor()
extern void Tiempo__ctor_m058FEB22437013AC463A1F4176DEC0540D7385C4 (void);
// 0x00000145 System.Void TiempoDisparo::Awake()
extern void TiempoDisparo_Awake_m0C74CE21A83D45066D7BE9B62FA9C6512CE818C8 (void);
// 0x00000146 System.Void TiempoDisparo::Start()
extern void TiempoDisparo_Start_mF3825EC2E2BB0F6195470AE7BBB3B6927727AB08 (void);
// 0x00000147 System.Void TiempoDisparo::Update()
extern void TiempoDisparo_Update_m066C453D72A22727F0C30CE86CBBCCDE92E82493 (void);
// 0x00000148 System.Void TiempoDisparo::LateUpdate()
extern void TiempoDisparo_LateUpdate_m56CF264DCB4292D1CAB379785B9F8A87A884F0F5 (void);
// 0x00000149 System.Void TiempoDisparo::.ctor()
extern void TiempoDisparo__ctor_mA250BD751DA36719B4E04297135E9567A438A0E0 (void);
// 0x0000014A System.Void UiQuieto::Start()
extern void UiQuieto_Start_mF17F349B01CD90245009C5FAA5406AE9C7371489 (void);
// 0x0000014B System.Void UiQuieto::Update()
extern void UiQuieto_Update_m91AFEB31A6DAB43FB7B470B35D335D6FBBE44B43 (void);
// 0x0000014C System.Collections.IEnumerator UiQuieto::DispararIzq()
extern void UiQuieto_DispararIzq_m161DD5C46E0D042FFFBD2CB880C084BE7A3EE7F2 (void);
// 0x0000014D System.Collections.IEnumerator UiQuieto::DispararDrc()
extern void UiQuieto_DispararDrc_m559535A821ABE56F02C4AC2B2CB2CD7E1158EF85 (void);
// 0x0000014E System.Void UiQuieto::.ctor()
extern void UiQuieto__ctor_mB87FDF75A470DA393000ED614CCCCBAB2ECDE336 (void);
// 0x0000014F System.Void UiQuieto/<DispararIzq>d__11::.ctor(System.Int32)
extern void U3CDispararIzqU3Ed__11__ctor_m3C73C4D06B254E45AEA5179164A8B11AEE17FD06 (void);
// 0x00000150 System.Void UiQuieto/<DispararIzq>d__11::System.IDisposable.Dispose()
extern void U3CDispararIzqU3Ed__11_System_IDisposable_Dispose_mF422CF6AE6D3C5467F30E2A9425C88B5103DCAD8 (void);
// 0x00000151 System.Boolean UiQuieto/<DispararIzq>d__11::MoveNext()
extern void U3CDispararIzqU3Ed__11_MoveNext_mE8F02EE25A8DE0958E4AD9410337B599E1015D63 (void);
// 0x00000152 System.Object UiQuieto/<DispararIzq>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDispararIzqU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD72EB319620E3C152D71A1B166D134C606B21D (void);
// 0x00000153 System.Void UiQuieto/<DispararIzq>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_Reset_m2169F2504BDFFD82B5EBF4CD0B1C644A50E9B14A (void);
// 0x00000154 System.Object UiQuieto/<DispararIzq>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_get_Current_mD25026294FDF7D508B82F96AFE59540070282C6F (void);
// 0x00000155 System.Void UiQuieto/<DispararDrc>d__12::.ctor(System.Int32)
extern void U3CDispararDrcU3Ed__12__ctor_m80D8E27DB0037C73FA28E09D588266AE3ECBDDBA (void);
// 0x00000156 System.Void UiQuieto/<DispararDrc>d__12::System.IDisposable.Dispose()
extern void U3CDispararDrcU3Ed__12_System_IDisposable_Dispose_mFA330C43C7F50AACD3CBF37DFB682861F5B395B3 (void);
// 0x00000157 System.Boolean UiQuieto/<DispararDrc>d__12::MoveNext()
extern void U3CDispararDrcU3Ed__12_MoveNext_m5311219D5FD9164B75AA76FC4E10503B7D3AAA71 (void);
// 0x00000158 System.Object UiQuieto/<DispararDrc>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDispararDrcU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D48A12FF98EBACE1C0146CFDDD151E9C80432A1 (void);
// 0x00000159 System.Void UiQuieto/<DispararDrc>d__12::System.Collections.IEnumerator.Reset()
extern void U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_Reset_mAB2E8B6BC2CC8C357054304C212B17198FD7495C (void);
// 0x0000015A System.Object UiQuieto/<DispararDrc>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_get_Current_mA18797AA09FFA1698A00A87A9356C39733BD70CB (void);
// 0x0000015B System.Void Victoria::Awake()
extern void Victoria_Awake_mE4E3313885EA8FDAA6BCA20CA8DA0D7CB73A0402 (void);
// 0x0000015C System.Void Victoria::Update()
extern void Victoria_Update_m077EF43A05F247F8CF89C46EA4DCE95D62625ADE (void);
// 0x0000015D System.Void Victoria::Huevo()
extern void Victoria_Huevo_mE8CF1A9CFDF565750A1F8089799C8A4A588198A0 (void);
// 0x0000015E System.Void Victoria::ActivarAudio()
extern void Victoria_ActivarAudio_m57A7D91C6358A84F6CD45A60B6AE8EB8AC0764CF (void);
// 0x0000015F System.Void Victoria::Victory()
extern void Victoria_Victory_m29DA938E6A4D1BF0F59937FD5659FBEB425A9DA2 (void);
// 0x00000160 System.Void Victoria::ActivarCanvas()
extern void Victoria_ActivarCanvas_m68FA57B22822DEEF043407523488A0FEAEE2F516 (void);
// 0x00000161 System.Void Victoria::DeathCanvas()
extern void Victoria_DeathCanvas_mE909D51632E28407384769D8A2A2846CA6B71AC9 (void);
// 0x00000162 System.Void Victoria::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Victoria_OnTriggerEnter2D_mF8F67B14DDDEA2EEF33B3DFFBE51AE560CF3FF8C (void);
// 0x00000163 System.Void Victoria::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Victoria_OnTriggerExit2D_mA042A273FFC7640F12DEA5166D961ED871CCE5FC (void);
// 0x00000164 System.Void Victoria::EjecutarSonidoRecoger()
extern void Victoria_EjecutarSonidoRecoger_m29A70774497861A2E3F3A9072417832F32279D37 (void);
// 0x00000165 System.Void Victoria::EjecutarSonidoExplosion()
extern void Victoria_EjecutarSonidoExplosion_m86A2A41C4EA3F15336D77BABEE24E537E33D2387 (void);
// 0x00000166 System.Void Victoria::.ctor()
extern void Victoria__ctor_m0C443C7F68D21BA5A7B896A9E45601A088A9B2D3 (void);
// 0x00000167 System.Void Volver::Update()
extern void Volver_Update_m39A5AA72134B9187B6823DA7D7F73A9CE5FBC6E0 (void);
// 0x00000168 System.Void Volver::.ctor()
extern void Volver__ctor_m156D76D394AFB7AA21D7458A0AB306C8566937A2 (void);
// 0x00000169 System.Void contadorAerena::Awake()
extern void contadorAerena_Awake_mE85C5009D1F3DCD1EAD0624362D1A1E3F95FAF65 (void);
// 0x0000016A System.Void contadorAerena::Start()
extern void contadorAerena_Start_m46881FA8B6AE9E2682AA4EB9A17C56FB902DB51A (void);
// 0x0000016B System.Void contadorAerena::Update()
extern void contadorAerena_Update_m32C574663AC59DF8CE0E33DD9A1BA671CB3FF951 (void);
// 0x0000016C System.Void contadorAerena::ReloadPoints()
extern void contadorAerena_ReloadPoints_m20EB306011EBE87812DE7324DC71847A4C8F9A82 (void);
// 0x0000016D System.Int32 contadorAerena::VerPuntos()
extern void contadorAerena_VerPuntos_mA9D87553A9AA44413A0C1C791EC42031F4FDE384 (void);
// 0x0000016E System.Collections.IEnumerator contadorAerena::EjecutarSuma()
extern void contadorAerena_EjecutarSuma_mEC3153784BA9B13858B65F778A0E0A83378150ED (void);
// 0x0000016F System.Void contadorAerena::.ctor()
extern void contadorAerena__ctor_m5BE8F99DE25B2DEA7B50B7B20F03DF5C423626D1 (void);
// 0x00000170 System.Void contadorAerena::.cctor()
extern void contadorAerena__cctor_mAC90DA8DCE00F21AC7CEA3064E77F1D8418ECA31 (void);
// 0x00000171 System.Void contadorAerena/<EjecutarSuma>d__8::.ctor(System.Int32)
extern void U3CEjecutarSumaU3Ed__8__ctor_mB673D2B05A6FB785A327713F2319440CFC0E92C5 (void);
// 0x00000172 System.Void contadorAerena/<EjecutarSuma>d__8::System.IDisposable.Dispose()
extern void U3CEjecutarSumaU3Ed__8_System_IDisposable_Dispose_mD1CB9B1643C7430D4E3FFCFA7DDB7A2153BC6FA8 (void);
// 0x00000173 System.Boolean contadorAerena/<EjecutarSuma>d__8::MoveNext()
extern void U3CEjecutarSumaU3Ed__8_MoveNext_mC561D36B4977F561199A623485DAC77EDC597F46 (void);
// 0x00000174 System.Object contadorAerena/<EjecutarSuma>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEjecutarSumaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m680481E7056FF135C717AD069C76864E13EF1E96 (void);
// 0x00000175 System.Void contadorAerena/<EjecutarSuma>d__8::System.Collections.IEnumerator.Reset()
extern void U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_Reset_m3744EBE3720715716DC0495BC10DB972486551D8 (void);
// 0x00000176 System.Object contadorAerena/<EjecutarSuma>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_get_Current_mD1812047E81A84A1D89D2A3CDE3FE94CCCCA954F (void);
// 0x00000177 System.Void UnityEngine.RuleTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void RuleTile_GetTileData_m50D33194EA7D3BADA2928897ED54AB319F334AAA (void);
// 0x00000178 System.Single UnityEngine.RuleTile::GetPerlinValue(UnityEngine.Vector3Int,System.Single,System.Single)
extern void RuleTile_GetPerlinValue_m7C412F7FEE2F0AAF258B18A13679E709AB404662 (void);
// 0x00000179 System.Boolean UnityEngine.RuleTile::GetTileAnimationData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileAnimationData&)
extern void RuleTile_GetTileAnimationData_m0EA7CD728A9180521A15DCBFDE7192517BA3CCE7 (void);
// 0x0000017A System.Void UnityEngine.RuleTile::RefreshTile(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap)
extern void RuleTile_RefreshTile_m142E725163EC4019DFBE1812F2595DDDCE01B88C (void);
// 0x0000017B System.Boolean UnityEngine.RuleTile::RuleMatches(UnityEngine.RuleTile/TilingRule,UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Matrix4x4&)
extern void RuleTile_RuleMatches_mBB7C6052F1F5047FC25AE39F41624C6CB33C0772 (void);
// 0x0000017C UnityEngine.Matrix4x4 UnityEngine.RuleTile::ApplyRandomTransform(UnityEngine.RuleTile/TilingRule/Transform,UnityEngine.Matrix4x4,System.Single,UnityEngine.Vector3Int)
extern void RuleTile_ApplyRandomTransform_mBA108C0E27F2E9E747636D841A0FBD81C4F8C1FD (void);
// 0x0000017D System.Boolean UnityEngine.RuleTile::RuleMatches(UnityEngine.RuleTile/TilingRule,UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,System.Int32)
extern void RuleTile_RuleMatches_m6EA07CE71995B163A01104244A384B3BB643198A (void);
// 0x0000017E System.Boolean UnityEngine.RuleTile::RuleMatches(UnityEngine.RuleTile/TilingRule,UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,System.Boolean,System.Boolean)
extern void RuleTile_RuleMatches_m0C41241F706F4EF6E6455EF0CCE69F3493F74378 (void);
// 0x0000017F System.Int32 UnityEngine.RuleTile::GetIndexOfOffset(UnityEngine.Vector3Int)
extern void RuleTile_GetIndexOfOffset_mED67AC50C40C31DA975BAB387E49F8FB959CC9D2 (void);
// 0x00000180 UnityEngine.Vector3Int UnityEngine.RuleTile::GetRotatedPos(UnityEngine.Vector3Int,System.Int32)
extern void RuleTile_GetRotatedPos_mAC842975698C071E1299C648D57DEEB7C7477461 (void);
// 0x00000181 UnityEngine.Vector3Int UnityEngine.RuleTile::GetMirroredPos(UnityEngine.Vector3Int,System.Boolean,System.Boolean)
extern void RuleTile_GetMirroredPos_m66149C28029F1751A7881F1D927CC5BFE0479E77 (void);
// 0x00000182 System.Void UnityEngine.RuleTile::.ctor()
extern void RuleTile__ctor_m34CFBB8747C6D6A1BD90FD066ACF39862F2F1C64 (void);
// 0x00000183 System.Void UnityEngine.RuleTile/TilingRule::.ctor()
extern void TilingRule__ctor_mB20CA5F08CDAC9601B00AB275F7761E575AFCB1A (void);
// 0x00000184 System.Collections.Generic.Dictionary`2<UnityEngine.Tilemaps.GridInformation/GridInformationKey,UnityEngine.Tilemaps.GridInformation/GridInformationValue> UnityEngine.Tilemaps.GridInformation::get_PositionProperties()
extern void GridInformation_get_PositionProperties_m7190DB7106273E84E31332A86A17A8781852B8B1 (void);
// 0x00000185 System.Void UnityEngine.Tilemaps.GridInformation::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GridInformation_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mC4E74A564BFFFECBDC83DE7EDE0096E537D8DB9A (void);
// 0x00000186 System.Void UnityEngine.Tilemaps.GridInformation::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GridInformation_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2657C5497BEC07021027160F7E3E2688ACE51579 (void);
// 0x00000187 System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,T)
// 0x00000188 System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,System.Int32)
extern void GridInformation_SetPositionProperty_mEE998C5A244C390A55E60C20545798CE9743EE67 (void);
// 0x00000189 System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,System.String)
extern void GridInformation_SetPositionProperty_m8E2E89508E38BE4780164294DFE31A4312541AE1 (void);
// 0x0000018A System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,System.Single)
extern void GridInformation_SetPositionProperty_m71851671BC25B559EC6BF25B2EFECD2949C44CAE (void);
// 0x0000018B System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,System.Double)
extern void GridInformation_SetPositionProperty_m4BB5E3451092941E84D76CA4C5E0477754EBC1B8 (void);
// 0x0000018C System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,UnityEngine.Object)
extern void GridInformation_SetPositionProperty_m316A61EA775AA8F5E90CD84DED09C384980C7462 (void);
// 0x0000018D System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,UnityEngine.Color)
extern void GridInformation_SetPositionProperty_mE184947C1C587A4B7C5DE8ADB0EC4D8A9D5C505E (void);
// 0x0000018E System.Boolean UnityEngine.Tilemaps.GridInformation::SetPositionProperty(UnityEngine.Vector3Int,System.String,UnityEngine.Tilemaps.GridInformationType,System.Object)
extern void GridInformation_SetPositionProperty_m9CAF712A1C1DB2B9B48D569C52305C4FDB00ABCB (void);
// 0x0000018F T UnityEngine.Tilemaps.GridInformation::GetPositionProperty(UnityEngine.Vector3Int,System.String,T)
// 0x00000190 System.Int32 UnityEngine.Tilemaps.GridInformation::GetPositionProperty(UnityEngine.Vector3Int,System.String,System.Int32)
extern void GridInformation_GetPositionProperty_mFA777C4CEC9512601A13C4EA476BD7C6905B176D (void);
// 0x00000191 System.String UnityEngine.Tilemaps.GridInformation::GetPositionProperty(UnityEngine.Vector3Int,System.String,System.String)
extern void GridInformation_GetPositionProperty_m519892FF2188B3130EFB7A3C8855E416F8901C16 (void);
// 0x00000192 System.Single UnityEngine.Tilemaps.GridInformation::GetPositionProperty(UnityEngine.Vector3Int,System.String,System.Single)
extern void GridInformation_GetPositionProperty_m943897E0D7AA87889FA1A81FD403456B1CECB23E (void);
// 0x00000193 System.Double UnityEngine.Tilemaps.GridInformation::GetPositionProperty(UnityEngine.Vector3Int,System.String,System.Double)
extern void GridInformation_GetPositionProperty_m76C26EAE210C2E447646ED4916314A5DA8BF25A9 (void);
// 0x00000194 UnityEngine.Color UnityEngine.Tilemaps.GridInformation::GetPositionProperty(UnityEngine.Vector3Int,System.String,UnityEngine.Color)
extern void GridInformation_GetPositionProperty_m053FD4ECDBD4BCD51F14C89242BC7D1312680C0D (void);
// 0x00000195 System.Boolean UnityEngine.Tilemaps.GridInformation::ErasePositionProperty(UnityEngine.Vector3Int,System.String)
extern void GridInformation_ErasePositionProperty_m3181035846566601E81DE18922B7FA5D90F4885D (void);
// 0x00000196 System.Void UnityEngine.Tilemaps.GridInformation::Reset()
extern void GridInformation_Reset_m05B24CDA814739C06EA5162AB54A7B8BED601E1D (void);
// 0x00000197 UnityEngine.Vector3Int[] UnityEngine.Tilemaps.GridInformation::GetAllPositions(System.String)
extern void GridInformation_GetAllPositions_m046F3724E25576F1585754F4BCFF4BD71F19A14A (void);
// 0x00000198 System.Void UnityEngine.Tilemaps.GridInformation::.ctor()
extern void GridInformation__ctor_mD1EA81F9131ED1FA582BAA08465911133887724F (void);
// 0x00000199 System.Void UnityEngine.Tilemaps.GridInformation/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_mC7B788FDBBC7B457576723353A2B16433FFD1515 (void);
// 0x0000019A System.Boolean UnityEngine.Tilemaps.GridInformation/<>c__DisplayClass35_0::<GetAllPositions>b__0(UnityEngine.Tilemaps.GridInformation/GridInformationKey)
extern void U3CU3Ec__DisplayClass35_0_U3CGetAllPositionsU3Eb__0_mD969FC8652F814CFC140A8D53FDF9410108CD1BF (void);
// 0x0000019B System.Void UnityEngine.Tilemaps.GridInformation/<>c::.cctor()
extern void U3CU3Ec__cctor_mCEF019CDC263EF40D212949986924DB706C94D16 (void);
// 0x0000019C System.Void UnityEngine.Tilemaps.GridInformation/<>c::.ctor()
extern void U3CU3Ec__ctor_mDF3D8D615FB5D76B90DF131D4B61CF0DD9CF29F4 (void);
// 0x0000019D UnityEngine.Vector3Int UnityEngine.Tilemaps.GridInformation/<>c::<GetAllPositions>b__35_1(UnityEngine.Tilemaps.GridInformation/GridInformationKey)
extern void U3CU3Ec_U3CGetAllPositionsU3Eb__35_1_mBC7428A560B883736342512BD8F37B3397E803DC (void);
// 0x0000019E System.Void UnityEngine.Tilemaps.AnimatedTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void AnimatedTile_GetTileData_mD611724E4E114405C9177FF5E3C03A32570F3AFC (void);
// 0x0000019F System.Boolean UnityEngine.Tilemaps.AnimatedTile::GetTileAnimationData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileAnimationData&)
extern void AnimatedTile_GetTileAnimationData_m8655D1439CA8A7F8C8B036EF3C8E1988426A07CF (void);
// 0x000001A0 System.Void UnityEngine.Tilemaps.AnimatedTile::.ctor()
extern void AnimatedTile__ctor_mD8826B5666F0E9DC2AE8C0BA71902EBB5A0A28CF (void);
// 0x000001A1 System.Void UnityEngine.Tilemaps.PipelineTile::RefreshTile(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap)
extern void PipelineTile_RefreshTile_m9F14F64B50D3F33A2AE3ABFEA1ABE9CB1E8327F9 (void);
// 0x000001A2 System.Void UnityEngine.Tilemaps.PipelineTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void PipelineTile_GetTileData_m89CF29041CCFBC42FD50A241A0403819E2492241 (void);
// 0x000001A3 System.Void UnityEngine.Tilemaps.PipelineTile::UpdateTile(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void PipelineTile_UpdateTile_mC0AE2A8F9C2B73539996FBEBD3F17C58C72B8ED0 (void);
// 0x000001A4 System.Boolean UnityEngine.Tilemaps.PipelineTile::TileValue(UnityEngine.Tilemaps.ITilemap,UnityEngine.Vector3Int)
extern void PipelineTile_TileValue_m6C6B60A314F3E57D9F24D59112F660503EE93924 (void);
// 0x000001A5 System.Int32 UnityEngine.Tilemaps.PipelineTile::GetIndex(System.Byte)
extern void PipelineTile_GetIndex_mDACC064FBF779BAECAECEDC42E07C273DF111C45 (void);
// 0x000001A6 UnityEngine.Matrix4x4 UnityEngine.Tilemaps.PipelineTile::GetTransform(System.Byte)
extern void PipelineTile_GetTransform_m834617C6CD253BE67EE31CE11C5012E062013B67 (void);
// 0x000001A7 System.Void UnityEngine.Tilemaps.PipelineTile::.ctor()
extern void PipelineTile__ctor_m886E466F7195F7DE374FF68863324BA76FF26C3E (void);
// 0x000001A8 System.Void UnityEngine.Tilemaps.RandomTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void RandomTile_GetTileData_mAA02D6E857322D28436E7F4EFB23EC7C106C97A0 (void);
// 0x000001A9 System.Void UnityEngine.Tilemaps.RandomTile::.ctor()
extern void RandomTile__ctor_m756265F30DF0F9EBD5D6A59A9A6A9AB5EC7222C5 (void);
// 0x000001AA System.Void UnityEngine.Tilemaps.TerrainTile::RefreshTile(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap)
extern void TerrainTile_RefreshTile_m848140DAA98343D31A5620F7E0EA2C3E84B5FE5B (void);
// 0x000001AB System.Void UnityEngine.Tilemaps.TerrainTile::GetTileData(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void TerrainTile_GetTileData_mD6E4EB65B6743330F23661417B3166E3F0699B4A (void);
// 0x000001AC System.Void UnityEngine.Tilemaps.TerrainTile::UpdateTile(UnityEngine.Vector3Int,UnityEngine.Tilemaps.ITilemap,UnityEngine.Tilemaps.TileData&)
extern void TerrainTile_UpdateTile_m6051434702FE50654875F99AB4ADCAD21BAA139E (void);
// 0x000001AD System.Boolean UnityEngine.Tilemaps.TerrainTile::TileValue(UnityEngine.Tilemaps.ITilemap,UnityEngine.Vector3Int)
extern void TerrainTile_TileValue_m1F1B2B8553B8B2EF0CD12C6D3601FB10542DD3CE (void);
// 0x000001AE System.Int32 UnityEngine.Tilemaps.TerrainTile::GetIndex(System.Byte)
extern void TerrainTile_GetIndex_m3D963C3BBCFAA861EA358C0B35D4AA0343C8C0BF (void);
// 0x000001AF UnityEngine.Matrix4x4 UnityEngine.Tilemaps.TerrainTile::GetTransform(System.Byte)
extern void TerrainTile_GetTransform_m1B4CACCADF647D47B930B07E6CFEBC4BC9B5FC08 (void);
// 0x000001B0 System.Void UnityEngine.Tilemaps.TerrainTile::.ctor()
extern void TerrainTile__ctor_m02CAE0CA65CE51921F9BCAA255B13188F96D6244 (void);
// 0x000001B1 System.Void MoreMountains.Feedbacks.DemoBall::Start()
extern void DemoBall_Start_m055C519C0BEA3CDCD8420F274702B2E00AF4F0BD (void);
// 0x000001B2 System.Collections.IEnumerator MoreMountains.Feedbacks.DemoBall::ProgrammedDeath()
extern void DemoBall_ProgrammedDeath_m2DF4845C32A8963BAB27D7F0BFACC5784712BE92 (void);
// 0x000001B3 System.Void MoreMountains.Feedbacks.DemoBall::.ctor()
extern void DemoBall__ctor_m6204C9FCE5AF59F772854A3DD33C78507B164D86 (void);
// 0x000001B4 System.Void MoreMountains.Feedbacks.DemoBall/<ProgrammedDeath>d__3::.ctor(System.Int32)
extern void U3CProgrammedDeathU3Ed__3__ctor_m594CF3FB53C5D8B5ED586479D34EE72E4943BF2F (void);
// 0x000001B5 System.Void MoreMountains.Feedbacks.DemoBall/<ProgrammedDeath>d__3::System.IDisposable.Dispose()
extern void U3CProgrammedDeathU3Ed__3_System_IDisposable_Dispose_m543CD3EBD9CAC69065A27E5644411AFE8B6C7B6A (void);
// 0x000001B6 System.Boolean MoreMountains.Feedbacks.DemoBall/<ProgrammedDeath>d__3::MoveNext()
extern void U3CProgrammedDeathU3Ed__3_MoveNext_m5D434C9C203D4F0DFB0A9A79A44DDC2A03208F28 (void);
// 0x000001B7 System.Object MoreMountains.Feedbacks.DemoBall/<ProgrammedDeath>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProgrammedDeathU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC82FC1704348F2794A1AEB9E8C43335F3189EDEA (void);
// 0x000001B8 System.Void MoreMountains.Feedbacks.DemoBall/<ProgrammedDeath>d__3::System.Collections.IEnumerator.Reset()
extern void U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_Reset_m62594DC3054FFC736C7286487579485F6501901F (void);
// 0x000001B9 System.Object MoreMountains.Feedbacks.DemoBall/<ProgrammedDeath>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_get_Current_mA0F24F509E8DF2CC5413AC39750F9F5B91179303 (void);
// 0x000001BA System.Void MoreMountains.Feedbacks.DemoButton::OnEnable()
extern void DemoButton_OnEnable_mA24ED168DCA5A8707E2351E56BC167D222B9125E (void);
// 0x000001BB System.Void MoreMountains.Feedbacks.DemoButton::HandleWebGL()
extern void DemoButton_HandleWebGL_mC9058210C103DFACBCC6FD0CAB270330D647FD24 (void);
// 0x000001BC System.Void MoreMountains.Feedbacks.DemoButton::.ctor()
extern void DemoButton__ctor_mA0B3EF6D1C4B93BD9070111ABBDC8F63ADF74DAD (void);
// 0x000001BD System.Void MoreMountains.Feedbacks.DemoGhost::OnAnimationEnd()
extern void DemoGhost_OnAnimationEnd_m57D8F1F7423CF533C405120AA5E2D7CA40C58775 (void);
// 0x000001BE System.Void MoreMountains.Feedbacks.DemoGhost::.ctor()
extern void DemoGhost__ctor_m7BA344F22206526F8FBDE367F2EBE4D6A515B865 (void);
// 0x000001BF System.Void MoreMountains.Feedbacks.DemoPackageTester::Awake()
extern void DemoPackageTester_Awake_m76D03BF81A53F2DB72982AFA35A01365D0F9BF07 (void);
// 0x000001C0 System.Void MoreMountains.Feedbacks.DemoPackageTester::TestForDependencies()
extern void DemoPackageTester_TestForDependencies_m4F479E0A244A626276ED55AC0ACDF9EC1B32A254 (void);
// 0x000001C1 System.Void MoreMountains.Feedbacks.DemoPackageTester::.ctor()
extern void DemoPackageTester__ctor_m1A8560B283E5D5D002ED1F8498BCD3BD294B6472 (void);
static Il2CppMethodPointer s_methodPointers[449] = 
{
	ClickSound_get_source_m51C3EF83C207057CA300546C2275C5867EF63850,
	ClickSound_get_btn_mDE7FD029C2E70F977C664476C1DADF30231CE7F8,
	ClickSound_Start_m6EBC1CC4674AFE32773FB36258F99F5F9F21276C,
	ClickSound_PlaySound_m456648F2F10E328C5C37D1137463C622A68DA540,
	ClickSound__ctor_mCE0A14248C9C47EC74657CBE2FC9E2B9A74F29CC,
	NivelManager_Start_m03644C8C88DB10707F1F9440E4C13AFE3CB2C1F2,
	NivelManager_Update_mA004D198E57253EEC43B93BF7351DB17547B3903,
	NivelManager__ctor_mE80CA6CEE7A831C19E1A85B6A4DCE98EDB5B20AB,
	Aerena_Awake_mBB9380B98607DC6F8FEF46B2CD7CF59C3BE56FDB,
	Aerena_Start_m871A4FBF15D54BDA1CC44B26CC1A36C046D5197C,
	Aerena_Update_mBFFEE7F7BBFFC555A693272A8110F83B3FF6864A,
	Aerena_OnTriggerEnter2D_m851CBC3D9BFDC66FF68985E62F6FBD8919483A87,
	Aerena_Coin_mF9AABD01A2AC8B5A108EC59D94B8B7AB52235A18,
	Aerena__ctor_mF543EB95A3ACF66D0D19D2EEDAB28FFB77F6B2F2,
	AnimacionFinal_Start_m5E87664D45E188E5589BB49525A6E0754640490F,
	AnimacionFinal_Awake_m3321438F2EC88EE60F6BBDD820CC3D1A404E543F,
	AnimacionFinal_Update_m5C163721FB83852F6FB2BC16BD5DE764072A8586,
	AnimacionFinal_PrenderLlamas_m93AAFB82A9011B4DDD21166A010CC6D1457216F5,
	AnimacionFinal__ctor_m005A00AB71B5D39A6B7C2562DF20555793F2C44E,
	AnimacionKabu_Start_m37BC476F5DC3E288D0A769045755FFF6A5CA9E72,
	AnimacionKabu_Update_m43BBD91A5A36A85AE3B3276A2770E0F1AFFD0B75,
	AnimacionKabu__ctor_m0F6AA9877E3C0F0D9883427F7C3BDF2751A036B7,
	BarraDeSalud_Update_m5FD1B9A9A6DF48A0BC56E67487450FBEA58EE711,
	BarraDeSalud__ctor_m04B27A0BFEE610A1EC7B953844A194CBFAE9B92B,
	BrujaFinal_Start_mE2CC920DB3906F7BFB4D8FC90C2EFC568FD0CFED,
	BrujaFinal_Update_m7B85C66A08DE6828EF2E6F5D973F22D7EA1E3465,
	BrujaFinal_BrujaHuevo_mD92ED3142E0E61CB6E039FEE3EA8D161A023AB25,
	BrujaFinal__ctor_m97155CC5177EF1F0932AFEB4F75A0F3E044F4138,
	Burbuja_Start_m21E291E7F0D1905CFDD5A7F9F36A6CF7A358C15F,
	Burbuja_MoverIzqDr_m5A8511933345071EC18815BED888F8B0E38387E1,
	Burbuja__ctor_m324824725D1F36D15F4F9F06D3B000AD76189FF8,
	U3CMoverIzqDrU3Ed__2__ctor_mFF418106ED642001DE5EB4EF9B1DA81E6FC9F95B,
	U3CMoverIzqDrU3Ed__2_System_IDisposable_Dispose_m6BD167F2FD4C89A730E88EE2143851AD001A3181,
	U3CMoverIzqDrU3Ed__2_MoveNext_mFF27C64447726CB028A6453632C89A29F7522A66,
	U3CMoverIzqDrU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87099B322323E87F8E5FA22020DD9F716F1DB421,
	U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_Reset_m6811DBECCC2CE3550F38BA1B956DCEA51E2FB330,
	U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_get_Current_m5773AEC31ABAB38671A73E5086BE573F14817574,
	CalaveraCkeck_Update_m7D5A0F71786F63760389307AE83824BD3F67D94E,
	CalaveraCkeck_OnTriggerEnter2D_mDA269996CFB2783228B9A4236D689AF0A6551A6D,
	CalaveraCkeck__ctor_m18661E7078F91212D59421E0B971815D89E50CEB,
	ComprobarVida_Start_m2DA341ACFCEDB7AB9F79685FAEF8B646F229CAEA,
	ComprobarVida_Update_m71EB10FB879308AD84EF444CD53034246F23EA4A,
	ComprobarVida__ctor_m5BCA0B1AAC6BB0D2557D791AC92A2960299317B5,
	ContadorCorazones_Start_m3015BBDEE7A5B15CD045C89BF05B57B0E009CEE7,
	ContadorCorazones_Update_m081E965A6FBF490B0ECAA0992D0D0A086D40E910,
	ContadorCorazones__ctor_mF9557B936523FD942F9FF305AD5944071F11F92D,
	ContadorFinal_Awake_m9D526D60C3B7B4206B0560480AFD9C1D9C8C6776,
	ContadorFinal_Start_mD0D9613032B363C44AD5D9537C7AAC945911EB4C,
	ContadorFinal_Update_m9EF547AA2DA811751881060C7B336E3A64B5A950,
	ContadorFinal_EjecutarSuma_mFE44F1E853276A4C77872A0E012DF9A86248EE98,
	ContadorFinal__ctor_mFF798710CD9FC2A1CBFA37EA61D3AB33710A3012,
	ContadorFinal__cctor_m62F2CB90FB1E433C504B198F37FD61BC68263B08,
	U3CEjecutarSumaU3Ed__9__ctor_m555B5D67B42B1BA719A4FDAC995620EB3494F48E,
	U3CEjecutarSumaU3Ed__9_System_IDisposable_Dispose_m51E2F857F838E5B63D1EB6C280C14F1AA38FA396,
	U3CEjecutarSumaU3Ed__9_MoveNext_mC2517BF140BDD05607C0E4B3BE1DADC6314A6E19,
	U3CEjecutarSumaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC01B665FC29861D3CFEF4E6B5E3C38CD1E10DE5A,
	U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_Reset_mAB9D23DD640D14BFD07A18375B560886D30407F4,
	U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_get_Current_mECBDCC521AAF9ADDE4EB030C11D055488FCB5E01,
	CursorLock_Update_m5E8B43504485031D65EEEA4668FDB473666840AE,
	CursorLock__ctor_m4A60DC59BD7EFBBB124D55CD0A594D812EBB782B,
	PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B,
	DesactivarCamara_Start_m78C34EF74D077CAF15227CB205DEE7409B03ADBF,
	DesactivarCamara_Update_mBA7294D3F491ACE27018165F7BD0AC25C8630248,
	DesactivarCamara_OnTriggerEnter2D_mED8007E438A7E1EF02D8E802D61CF48A0D7D4DC5,
	DesactivarCamara__ctor_m870198D621355DF7B47D90FBFA5CF14F3EDF02C8,
	Destrutror_OnTriggerEnter2D_mE930BECDA2709747596D8C6D2BF0BCA92DD37EA2,
	Destrutror_OnCollisionEnter2D_mE4D242EDB4345D6FC623E88B53ECC590B5B7A2F8,
	Destrutror__ctor_m92579A5DBE9D00AFAD30106A093DE650F862A1BC,
	Disparo_Start_m0ECDBFACBCABC713D5208A6F070E45E32449606A,
	Disparo_FixedUpdate_mF7DB30AD4C32AB2F96EBD824C44A33DAAEDC6CAB,
	Disparo_Update_m2CA799DC04953298FB7A06AFD0156B08EE8B9306,
	Disparo_OnDrawGizmos_m7AAAE17FE890BA9322A3E14E4653D3334989F1C0,
	Disparo__ctor_mBC55BB0F26E7F927F5C4368FB34B452812D63ADF,
	EfectoParallax_Start_mA21F95890F1A256DA31ACBC8285B73800233DB5F,
	EfectoParallax_LateUpdate_mCC8C9758224F9779327713B1016A5EA1679BD122,
	EfectoParallax__ctor_m2D30262B84F59DEB814A67CBA450A5F896D58C2E,
	Emision_Start_m43580E21BB703F526CE48C5BD4940BF25215131A,
	Emision_Update_m2A3137A540670AD61633C5017B8DD8F598B753DA,
	Emision_OnTriggerEnter2D_m8C93A84DA9602886EF4C73EFD9407487BCAD737D,
	Emision_Explosion_m4ED29A98D1E21FF6F19DD8A50446A25A5413FDE9,
	Emision__ctor_mD7AB77DA889D0369233D1FECD2F59277B3CA350D,
	Emision2_Start_mA3BD410D7721CFD29D9E61B7DD4038A8A8898862,
	Emision2_Update_m38811A90CE581C91586890E1C5EA2A30E56B8FCF,
	Emision2_OnTriggerEnter2D_m270CDAA93E54A2EAE737FB910B7CDB7265AA50AC,
	Emision2_Explosion_mFB4CD601FC6B99F151C8F6DC0543AAAC5D0197C7,
	Emision2__ctor_mAF808A16DBEAC5037E9757B79DE68D809CA6F49F,
	Enredadera_OnTriggerEnter2D_m78C7A0259297468299250DD1F37F2275F356D76D,
	Enredadera_OnTriggerExit2D_m2A1C353D2A8AB3B0F1216D1DAEC4712380540F71,
	Enredadera__ctor_mEC098001E29867498F08F04414F013A168B37D66,
	EntradaSpawn_OnTriggerEnter2D_m44CE81607DAE1FD77B80A24A774F3387EBB5045F,
	EntradaSpawn__ctor_mAE7D0433B473710C09372EEFCCE958800F9AD0F5,
	EsporaPlataforma_Start_mD39DEA128A19087F4B31C5F4966C642297BEEFA3,
	EsporaPlataforma_Update_mA183FBDF285F3D043634E0530A032D7A0618458D,
	EsporaPlataforma_OnTriggerEnter2D_mA215C7D87C50F2F718BADB8F163C7D2AEECBA797,
	EsporaPlataforma_PlayerArriba_mC18CA67F1ABF04F16C77688C489BDC2557BFF05F,
	EsporaPlataforma_Destruir_m70855487BD35A05CC5B5C08D751414730A2E2395,
	EsporaPlataforma__ctor_m3744439D8416E418A85FA33C55F4B2A6D7E3BAF9,
	U3CPlayerArribaU3Ed__8__ctor_m299B668FEB88DA495D32B2611BEFCE92196184C8,
	U3CPlayerArribaU3Ed__8_System_IDisposable_Dispose_mAE4D097CFF0CD28C27C270DC6C0234C976BF65B1,
	U3CPlayerArribaU3Ed__8_MoveNext_mFF52A60CF025F1AE3688E3056312906D1214E4A4,
	U3CPlayerArribaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m190919B43D3B6C4971D2B06CD2626862F446211E,
	U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_Reset_mD517702B85C053A9320DED1EFF9909B108DD4268,
	U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_get_Current_m7DAD340B862A764D7B16D3BDCCBEC9418CBB479D,
	Estrellas_Start_m21FAD5DAA28E7A1B8BA7C61BD940B1E3EB817516,
	Estrellas_Awake_m4B1C9C06979DA365B2CB8C1CEA96C354694EFD67,
	Estrellas_Update_mB589FB355D8B7B5B2039F60674067B881FE85780,
	Estrellas_Star_mE2BC7E35D0F8BFD0911800B7E308C46F14C1B285,
	Estrellas__ctor_m6A9872B7DECDB4C5329F512EBBA7CD45039AD67D,
	U3CStarU3Ed__17__ctor_mAC92040ACB86FA97FAC8781D22D6CB531D259157,
	U3CStarU3Ed__17_System_IDisposable_Dispose_m26583F744D3AFF0D05AD5D694ACBB24C816EF83D,
	U3CStarU3Ed__17_MoveNext_mDDEEDBCAC1AB2368E58BAB5F8E79B90478FDC029,
	U3CStarU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EA8D7A57AB7E6EDD3DDD6DA65B8A5E9F6E0840F,
	U3CStarU3Ed__17_System_Collections_IEnumerator_Reset_m3A2F5A257120B3DE339BC03EAB7092076161BDBD,
	U3CStarU3Ed__17_System_Collections_IEnumerator_get_Current_m01F25A7B6083895784BDE39B6C16C7D953F5451D,
	GymArte_Start_m64CE42B0135FDA4F026CE7BBAE79ABA0AAEED656,
	GymArte_Update_m985D489C15051E37D4A4B6E05112370292E33F6F,
	GymArte__ctor_m00FB0BC61172DB590B76134E17C73DED27F3B987,
	Huevo_Awake_mF4786ACEDB25EC740F588A8E578F485FB4A69664,
	Huevo_Start_m774B045179F1D1C968D02DB59E5E2A723421609C,
	Huevo_OnTriggerEnter2D_mC680CBDE9CF0DEA6588093FBAC1CE830903412C6,
	Huevo_Victoria_m9707A88A08AA34C47C22CEFF5A5EDF5B1B2400DC,
	Huevo__ctor_m510D44E85D48C26E6F94A377B2568D896B6B006C,
	KabuBasico_Start_mB9802AC5703A95573430505112E53A688CF7B49C,
	KabuBasico_Update_mC6FF6220A8AB41C7C2D3EE7B38151E41E3F0073A,
	KabuBasico_PararYDisparar_m098F975AB2D83E3736C8D78453D8997DE57DCB07,
	KabuBasico__ctor_m05302C97D7AF46B87BAEF43F55318250EBE46E1F,
	U3CPararYDispararU3Ed__5__ctor_m28B78F6217A3C7D8E4BB4F68EA15F9D5E26F4CB6,
	U3CPararYDispararU3Ed__5_System_IDisposable_Dispose_m3494AFC9EB836374DF75AD4B274764BB53B6E894,
	U3CPararYDispararU3Ed__5_MoveNext_m4454F74E7DFC2B11059B009EF2E72A681F467550,
	U3CPararYDispararU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E2003B8D3BE6EF61331DB120E9AE8BB915C712F,
	U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_Reset_mF0BA0302C68431F2AFFB14674A875A6AF12F6884,
	U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_get_Current_m25FE5DDBAB2FC720A89EF7D744E38A3A77E40DCC,
	KabuEmisor_Start_m7BA3116646417DF9330F6546A7B3E8F0A6641DC3,
	KabuEmisor_Update_m7A764E859CA7EA31BD45C66DD8A1E096A42A7F80,
	KabuEmisor_PararYDisparar_m28F90EA2256A32A214247BA59F0C5FF70D96E29F,
	KabuEmisor_OnDestroy_m4DF76AF772924FAD743C486F548682568AA9873F,
	KabuEmisor__ctor_m3255FDD102106E951261CD91610BD90B7FD1B8EA,
	U3CPararYDispararU3Ed__12__ctor_m2D700458181046A503F701ADFC8BA8DC9D3A9EB9,
	U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m7B7085B9C5D6AE8BAFC79EE702AAD249E6997E9D,
	U3CPararYDispararU3Ed__12_MoveNext_mD3799453B2844BF1A6FAB366F0FC490BC77DF120,
	U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6799E0EB5EAFE14DADFD640BB72A0A09470B96C,
	U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mF54B2836FF863E4F1CDF46F6498B9E100C6E4FE7,
	U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_mF32B2BB1AD709293B55180110157472741FCCA06,
	KabuEmisorRafaga_Start_m6BCB064C7F018898C123D6C29D52579046FCF730,
	KabuEmisorRafaga_Update_m0EB4EF4D47F090E9C5E7F4B90C7E12383F1E100A,
	KabuEmisorRafaga_PararYDisparar_m09C14B3FB2BFFE4EB0CF8E22B11D8C83820993F0,
	KabuEmisorRafaga_OnDestroy_m5DAFE634D96C0F55A0D8330C352BEC82657C2A09,
	KabuEmisorRafaga__ctor_m550B1E4C9FDBF722E0BDE49D37884777B4143E75,
	U3CPararYDispararU3Ed__12__ctor_mF857BF4F98F66D816DFF39B81B6DF35DA228D900,
	U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m5259FEE7B1C494B388D7E2FC7D7D9476BC4A10CD,
	U3CPararYDispararU3Ed__12_MoveNext_m9D81A42D6A10E3DE69909EBD269A1F442D96CF45,
	U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9C65E1438072C80B375B1894990FFC643263294,
	U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mA55C1772F133A8694C145E9CE918316A12606C97,
	U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_m0B368BD51F913A2888E0C3E70EEBE509DFA14945,
	KabuOnda_Start_mB43520216374BD9B76CADB542924512CE6959B75,
	KabuOnda_MoverIzqDr_m4FE6E82C5F835C766A913EEB83567A117E4E710F,
	KabuOnda_Sonido_mBFF25C329A9ECE4D775D65365E89D2922A6BCD6B,
	KabuOnda__ctor_m9181A42449B0F21663C28C20FE45A21A907C5F5B,
	U3CMoverIzqDrU3Ed__3__ctor_m1F2F458C54014365FD64467AEB95A749273D292B,
	U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_mEE21FEE22A97265ACCD39394E3894557F0E4942C,
	U3CMoverIzqDrU3Ed__3_MoveNext_mD2A77E812412F04568D7763AE82EC984617F5C90,
	U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10A5F520A464818C6522A42F2DC28C5769C38401,
	U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mFB3F2FF2AEF004BC485745191C954172F0C3C3FB,
	U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_m104439B07E2DDEB5A7A71252CF2073525EF3EDD3,
	Limite_OnTriggerEnter2D_mACB2BD02E93EB41F2161F38EC58B3D4B0AE03909,
	Limite__ctor_mDF58361B1CFB459A25077872F9E30DDA4D30062B,
	MenuDePausa_Awake_mD5AD2922E4B442FA8932B341A4D03A8E5F1CCC56,
	MenuDePausa_Start_m8BEADAF1034AA545CB5CF10D0FA69DDBDAA8D572,
	MenuDePausa_Update_m30F445189721D5DDCF74D159A0A2C47DADA4DC51,
	MenuDePausa_Pause_mC660B479C0E8FABA3E5B7C1A8DE1FA36FE5C9993,
	MenuDePausa_Resume_m88FAE5DC414A903B6B079CBC6F0071E56FB2EBC7,
	MenuDePausa_QuitarSeleccion_m646AC1B84664B08DFDC364C82C9BFB1B3E2D99F9,
	MenuDePausa__ctor_mBBDD90D3C9C732C6630F9ABDE73DA58421D8775C,
	MenuDePausa__cctor_m507B5CAB8DE74A4F0E7CCA34B2FB2E145D3E04DD,
	MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2,
	MenuManager_Update_m732CF78C4240CF880048E66B580BE32A72FB8779,
	MenuManager_SceneToLoad_m8606731327B8C5EDC227C04CC88E74F137FFB32F,
	MenuManager_CloseGame_mB38B520F96BA7471AF49700A240CD2F870D717C9,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	MovimientoKabu_Update_m06F86C6407ACB772186EA9F14F88A9CD908F7877,
	MovimientoKabu__ctor_mFE08588578A6189AB7F233B37D8CDAC4B0C43C33,
	MovimientoSeta_Start_m7CDF7FEFD7BAEA0FC170780B463EDFB06DCB3982,
	MovimientoSeta_MoverIzqDr_mDD6840527C7DE1E9CE743CEB3B1498BA712792C1,
	MovimientoSeta__ctor_mC8AD7F4B7BE6CAA9EF14DC75B2C4C2DCFE184B3A,
	U3CMoverIzqDrU3Ed__3__ctor_m96CB5E50F03A4DA2462445C3D8C9B0C4954BFD1A,
	U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_m2C33D7423D8CF1A4186256039D15E77B6E68C72B,
	U3CMoverIzqDrU3Ed__3_MoveNext_m38555BA9F8673069D41C0EEF248A7DCAC4C0369D,
	U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA534FFD4507218231C17A4A0D559A5ABB5B9ECA3,
	U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mF33DE026F075998BD681BB1C01F0C891CCA9EEA7,
	U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_mBB7FCDF227060896BDC0F40A549A034C56C875B4,
	MuerteManager_Start_m35B338B8BDD6E3EBEA20269875329C697CD6644D,
	MuerteManager_Update_mA9F18861C409A80C2845B915832CE1BBFC84B4ED,
	MuerteManager__ctor_m51E663102FE4B66F1B0D775BF09E635F05FFD40B,
	NubeEspora_Start_m1053F2F10272993E61466433B6714839F0B45C59,
	NubeEspora_Update_m42099376B17BB3CA1828EA172D412D08579DE5F9,
	NubeEspora_Desvanecer_m6058CBCA5409F686B5391718B2240FEBC709D73A,
	NubeEspora__ctor_m7EDA7516D7A857263ADD3E6A8427B58B2864DE4A,
	Parallax_Start_m7C7DC681755C4BBB7BF918CC67A2A5F2360F5DF2,
	Parallax_Update_m208D10754A83F3949A0DE958EBB01C4B63BC337B,
	Parallax__ctor_m0690485E7F8E13945AF42671FF1C937CB60D4B36,
	Pause_Awake_m67921AD16309350CBDAD9823D8AA58CDCE8C3A1E,
	Pause_Start_mAF3951FCCD204C9094502892B23E1BB368C938BC,
	Pause_Update_m2BEBA55A60F1B469B29DD631505236F1B5F21FEE,
	Pause__ctor_m0A16764376F8C9A6D19DE0BB24A41FA81F587928,
	Pez_Start_m145DB705FFF71DFC6419F539E8D2D51007D89BAF,
	Pez_Update_mFA0AC730C36918F01AAAD2D4C6C80D082A0E5E2B,
	Pez_Salta_m871747B9548F5058B2B6F71E0402E979E21F1651,
	Pez__ctor_m24A360F4612947A601A6479D7CAC3C8E84E175F7,
	U3CSaltaU3Ed__10__ctor_m0307177566364F8231E65778A7EFF36B440BDA85,
	U3CSaltaU3Ed__10_System_IDisposable_Dispose_mEACEE99BB082F725001956990EE7532B0F101703,
	U3CSaltaU3Ed__10_MoveNext_m4A6CA4C799B91369C23E64BB2E256CEBF671218F,
	U3CSaltaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED10B84D5D0F45C7D0218B89912F50347CFCD0A2,
	U3CSaltaU3Ed__10_System_Collections_IEnumerator_Reset_m0CAB56F87D69775FDBA924C157D8BA9984D6FCA0,
	U3CSaltaU3Ed__10_System_Collections_IEnumerator_get_Current_mD7CB6FAB5E7BD41C0EC578DE9FA4A185A778138F,
	PlantaCarnivora_Plantacarnivora_m615E1F73C291B71F36330C85ADEA77064B5F00EA,
	PlantaCarnivora_Start_mA4386071B70A0DF9B314BE9FA6C4661D0D7EF332,
	PlantaCarnivora__ctor_m600A8FB3DB69284DBC2FFEFA28040DB21004DD89,
	U3CPlantacarnivoraU3Ed__3__ctor_mA1FD778DB89B77632E8E9FAC03AA6CADB30A6262,
	U3CPlantacarnivoraU3Ed__3_System_IDisposable_Dispose_mEE53C5431D11E1570FB3E2B3FD320C948929A789,
	U3CPlantacarnivoraU3Ed__3_MoveNext_m7A6A70AFAA7D5C152C8A1E062222AA93DF1047F1,
	U3CPlantacarnivoraU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26A31B3F8FA250958606C9826236F2F2F49DE751,
	U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_Reset_mD90975CD3BF73D486C1E5E51D671E7C066164778,
	U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_get_Current_mE44BC670E424CB47A3392906F03C1870F5810EED,
	PlantaEspora_Start_mB303D2C6E4AD8B8B1A9C80DEA6596EA9C9FD7EFD,
	PlantaEspora_Update_mFC1D93A7452CD4EF2D76B4920A4AF78EC0B3F52F,
	PlantaEspora_InstanciaEsporas_m206FCB2862197C6D0A89C8315150CF66AD64B9D6,
	PlantaEspora_Instancia_m936ACC4AA4FDDF2E58F201DF1CE9651281833163,
	PlantaEspora__ctor_m666F38ED96ACFC96FBECC9573559BBB903D08FBE,
	U3CInstanciaEsporasU3Ed__6__ctor_mD138B51F39DA5DED79627D873CA8864FED69952A,
	U3CInstanciaEsporasU3Ed__6_System_IDisposable_Dispose_m2A6E5460A7B2CAED82982944943BAC197A5276D4,
	U3CInstanciaEsporasU3Ed__6_MoveNext_mC2ED3711DC7734CF5E908AF500FB9A4D58F156A3,
	U3CInstanciaEsporasU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8300C6B902901FDBD298B132B2EE4F344C8BE0B4,
	U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_Reset_m88576E6176233477BD4612172744765EAE8E413B,
	U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_get_Current_mBB6C82B823E03BAC16DD428ED16B53F003CE7D37,
	PlataformaCae_Start_m47ED6A173654300E986E9F74EE5583ED2F718E28,
	PlataformaCae_Update_mB975C521BA2883009FD7B06BB183A7034D8B7AF1,
	PlataformaCae_OnTriggerEnter2D_m81ABE4EDC45F9ECC3F5DEC2CF0DCC0D5306CA5F9,
	PlataformaCae_PlayerArriba_m6B299C877DDF407B84EB3D2843A319570A3F7314,
	PlataformaCae_Destruir_mE966C31BD9E2B4463E479F225B98B4660887943E,
	PlataformaCae__ctor_m6F893A5723384822637A191AC304CDE117AF12F5,
	U3CPlayerArribaU3Ed__10__ctor_m1CC2DF67A3E6CEB1DBFE36F189607B2886A1BDD3,
	U3CPlayerArribaU3Ed__10_System_IDisposable_Dispose_m654D1CD2F1EF73741B21CBAE8602DD3C23D957F9,
	U3CPlayerArribaU3Ed__10_MoveNext_mE32EF6FD670C77E8B1C38E8E235154A06A21A49D,
	U3CPlayerArribaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m778E4A1A205EAC3F0ED1CFDBAB69A619839BDA8E,
	U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_Reset_m621E3B3090BDE42B7043AE2ABA1843E7D2E75907,
	U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_get_Current_m935DF9075C85D1D5204F3CBE076F59C6040CCD27,
	Pocion_Awake_m2DE17FDAD35AF8A8C813FBAECF62461C3FE43B0B,
	Pocion_OnTriggerEnter2D_m14B967C04D892BAC20C46D6B4DF68B2A150CEDB8,
	Pocion__ctor_m15F926FA3C39FB309F8CE8AF761058B316A96399,
	Salamandra_Start_m4EC24CE300740534443B873D561095CF27576D0F,
	Salamandra_Update_mCF0469A18C7CF95C9D22AE05C2663838DB6DA604,
	Salamandra_DisparaSalta_m313C54FB7866FB593AB9DD00299606A15951E5FA,
	Salamandra_OnFinishAnim_m710220CC12607F986D12A4BC4CC2DA2B5813EDE1,
	Salamandra__ctor_mBB8EDD97DBAE5EE68BBAAC2FA195B44758E6AEDC,
	U3CDisparaSaltaU3Ed__11__ctor_mD9001ABB51207AF811B15948966A3EAE6D1730AC,
	U3CDisparaSaltaU3Ed__11_System_IDisposable_Dispose_m5CC9EBA93013AD7E12FC6C0137613B304F6970FF,
	U3CDisparaSaltaU3Ed__11_MoveNext_m59383403DCB3466073644DE905F143CFEF695799,
	U3CDisparaSaltaU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03C65B1CD417703E42F52AB854B415BCEE0E547D,
	U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_Reset_mCCE91D7C239B131EB0F3E1A48DFF350E49D8B719,
	U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_get_Current_m069AD940AD90A91ABEF88800EFD5446C7C2FB455,
	SalidaSpawn_Start_m8F123A52984AF96BC794EAEDCDEE9462DC5D3E4C,
	SalidaSpawn_Update_mC06D209A158087BC90515A579209BA5E576B86B9,
	SalidaSpawn__ctor_m360436F0C2797BED5D11E911C92D83BDAF997509,
	SetaExplosiva_Awake_m430E12E5120193DD6DAA4C511E3C5555CD9E6A0E,
	SetaExplosiva_Update_m5017F2CB0730ACA3740185763BC6B110839E44E2,
	SetaExplosiva_MoveExpl_mB966BBEBABB361812827BB32A8B975949A978756,
	SetaExplosiva_Explosion_m335D79B75A79ECC63DC724CF1D5ECA53F777BBAD,
	SetaExplosiva_OnDrawGizmos_m1C5CCAFEB42B83776472EB883227A760834509D8,
	SetaExplosiva__ctor_mBFE3E46A55F4339A7832F56BA549A6A27DB10B52,
	U3CMoveExplU3Ed__20__ctor_m1AD6BC85565B85E3097D42B879DECCC2ECF3EA19,
	U3CMoveExplU3Ed__20_System_IDisposable_Dispose_mB6769B0CE3DFAD199D3175811B6FAE67C028085B,
	U3CMoveExplU3Ed__20_MoveNext_mC1A9618B27C4C954188C014326580DFE2CC4131D,
	U3CMoveExplU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0289ACBC55AC3361224DFCBCDC5E240F82DD30F5,
	U3CMoveExplU3Ed__20_System_Collections_IEnumerator_Reset_m27878F4BCBD7CA8ACF397C1E2B0F76C8327E4182,
	U3CMoveExplU3Ed__20_System_Collections_IEnumerator_get_Current_m79B6D36E92E37A5F984F552BCA6C96B736848F12,
	SpawnBurbujas_Start_mDCC654B2468F663B213C170CF0C9F83D89E6DFAF,
	SpawnBurbujas_InstanciaVoladores_m8E4D3E9E79075B79CC7764F778B63845ABE86DC2,
	SpawnBurbujas__ctor_m6D5B2D7545045F7C0541B988CD4F62E2A2CA54A9,
	U3CInstanciaVoladoresU3Ed__5__ctor_m566BC2C949479B61D744B9C4A6F22A77258986EC,
	U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mF94EEE8EE391F6B50244657DB59C85F52537E58A,
	U3CInstanciaVoladoresU3Ed__5_MoveNext_m67699B5998E2684BF1D27C49D43DC12AF0792BC2,
	U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5F857AE3CAD2009297B73E6FF08D60551492A68,
	U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mB9573B135681505A6F5D8DE51C2BA457F86B8B39,
	U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_mCDC3EFAE58163D472B6C186624B99AF52E770AD4,
	SpawnEnemigos_Start_m59D6028B21239E2C22A624A745223A7945580D43,
	SpawnEnemigos_Update_mE7B9641B26F3185435E3F438538594C2500DC598,
	SpawnEnemigos_OnTriggerEnter2D_m6DA9C84FB2675476B4AB78D434FA45BB6D345E66,
	SpawnEnemigos_KabuAnimacion_m1FF600F410927329C9B7D171948789D8567E5091,
	SpawnEnemigos_EliminarKabu_m467B8F19DBE16AB3747F97A8BB7BE87872734514,
	SpawnEnemigos_SonidoTronco_m570BA303D07A54449533F76D1D34048BC345E3B0,
	SpawnEnemigos_InstanciarKabu_mB1C601B73B9129830825914634ED807537C687A3,
	SpawnEnemigos_DestruirSpawn_m1163BAC679DFDEA0C9B9E0B11B2D51A167ABA13F,
	SpawnEnemigos__ctor_mFC4D5E2344D8CCE500CCB5D5A378CB00D477D4F9,
	SpawnFinal_Start_m4DACB494DF2056617D135907F62C5D95FF0E6DBE,
	SpawnFinal_Update_mD8812F2C3C836EEF8F32B0DF9A6E77C57801AE38,
	SpawnFinal__ctor_mAD6B5887960879435071FDA0C134E1939914BAE6,
	SpawnManager_Start_mFA050B5C59C7CC47F4430CA0F5A646051D5442CB,
	SpawnManager_Update_m68C1739F474F0D29A0BF533BD44BA82786A3C96B,
	SpawnManager_RestablecerSpawn_m287AB9D5AE01BA9903B2CBFF7C402AFF9885795A,
	SpawnManager__ctor_mBCD48EEAB1EB733A88D47C85ACC373C796F20E59,
	SpawnPlanta_Start_mEB3EB86177E92775F735E24A841D8FF2A924CC5F,
	SpawnPlanta_Update_m5CFA7377FC579931ABE23E273541732471329BB6,
	SpawnPlanta_CheckIfIsNull_mFAE920114611373AF412FA4ED339C631CFCEC85D,
	SpawnPlanta_Spawn_m9A239121B02E5C48C32F6A03109EA6B985D31778,
	SpawnPlanta__ctor_m7C759E4D7FF0C67D17935931DBCE8E25F888288E,
	U3CSpawnU3Ed__7__ctor_m8F2FCB7CD60C3C16C3684449CC19D8A62C0FB30C,
	U3CSpawnU3Ed__7_System_IDisposable_Dispose_mDD5B5C4073274DD53D03539305093E05A62ABB80,
	U3CSpawnU3Ed__7_MoveNext_m82D7F6B030003F6E7340BFC57532380BED2A61C9,
	U3CSpawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41D02349281E8475CBA467DAD138C068AB0B46B2,
	U3CSpawnU3Ed__7_System_Collections_IEnumerator_Reset_m725CDFFF87F29DF99BD34EC091B742DE77E0C331,
	U3CSpawnU3Ed__7_System_Collections_IEnumerator_get_Current_mE08A8F64462C87D5CA0515DEA6AD51C86A7EC342,
	SpawnVoladores_Start_mA03B682121F1F2663C67F4D8E5E5251AA5068C6B,
	SpawnVoladores_InstanciaVoladores_m5D18C26DBDBE5B44417CC3D1FC50C8F2DC89BFF7,
	SpawnVoladores__ctor_m04E7A18A9DD082BA5D86FF9B440D1CC168028A57,
	U3CInstanciaVoladoresU3Ed__5__ctor_m6A47693939320F7DACB1B4C4CBDB197C79CA6D80,
	U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mA4EC4C5F217E444594EB675DE05504890EDDA72C,
	U3CInstanciaVoladoresU3Ed__5_MoveNext_m0A45830AEF008E451BFAF01FD0B8C5667610EA4E,
	U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF179B25FE4C16D4D2884B3FA9667F13E64D8A246,
	U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mA33C0021369B8C22FEE3E1FFA897E2F744C5C139,
	U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_m717DEBC2BB5F4397E38DDBAF05883A8899CE1ECC,
	Tiempo_Awake_m584A2ED078D185D377441DD5A83C125CF70F8215,
	Tiempo_Start_m80358B28A5CAF70175E75B14DC455AF2907D8A89,
	Tiempo_Update_m6660FE1492BE781F3FA7D73D4E8182837D8C1A3E,
	Tiempo__ctor_m058FEB22437013AC463A1F4176DEC0540D7385C4,
	TiempoDisparo_Awake_m0C74CE21A83D45066D7BE9B62FA9C6512CE818C8,
	TiempoDisparo_Start_mF3825EC2E2BB0F6195470AE7BBB3B6927727AB08,
	TiempoDisparo_Update_m066C453D72A22727F0C30CE86CBBCCDE92E82493,
	TiempoDisparo_LateUpdate_m56CF264DCB4292D1CAB379785B9F8A87A884F0F5,
	TiempoDisparo__ctor_mA250BD751DA36719B4E04297135E9567A438A0E0,
	UiQuieto_Start_mF17F349B01CD90245009C5FAA5406AE9C7371489,
	UiQuieto_Update_m91AFEB31A6DAB43FB7B470B35D335D6FBBE44B43,
	UiQuieto_DispararIzq_m161DD5C46E0D042FFFBD2CB880C084BE7A3EE7F2,
	UiQuieto_DispararDrc_m559535A821ABE56F02C4AC2B2CB2CD7E1158EF85,
	UiQuieto__ctor_mB87FDF75A470DA393000ED614CCCCBAB2ECDE336,
	U3CDispararIzqU3Ed__11__ctor_m3C73C4D06B254E45AEA5179164A8B11AEE17FD06,
	U3CDispararIzqU3Ed__11_System_IDisposable_Dispose_mF422CF6AE6D3C5467F30E2A9425C88B5103DCAD8,
	U3CDispararIzqU3Ed__11_MoveNext_mE8F02EE25A8DE0958E4AD9410337B599E1015D63,
	U3CDispararIzqU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD72EB319620E3C152D71A1B166D134C606B21D,
	U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_Reset_m2169F2504BDFFD82B5EBF4CD0B1C644A50E9B14A,
	U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_get_Current_mD25026294FDF7D508B82F96AFE59540070282C6F,
	U3CDispararDrcU3Ed__12__ctor_m80D8E27DB0037C73FA28E09D588266AE3ECBDDBA,
	U3CDispararDrcU3Ed__12_System_IDisposable_Dispose_mFA330C43C7F50AACD3CBF37DFB682861F5B395B3,
	U3CDispararDrcU3Ed__12_MoveNext_m5311219D5FD9164B75AA76FC4E10503B7D3AAA71,
	U3CDispararDrcU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D48A12FF98EBACE1C0146CFDDD151E9C80432A1,
	U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_Reset_mAB2E8B6BC2CC8C357054304C212B17198FD7495C,
	U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_get_Current_mA18797AA09FFA1698A00A87A9356C39733BD70CB,
	Victoria_Awake_mE4E3313885EA8FDAA6BCA20CA8DA0D7CB73A0402,
	Victoria_Update_m077EF43A05F247F8CF89C46EA4DCE95D62625ADE,
	Victoria_Huevo_mE8CF1A9CFDF565750A1F8089799C8A4A588198A0,
	Victoria_ActivarAudio_m57A7D91C6358A84F6CD45A60B6AE8EB8AC0764CF,
	Victoria_Victory_m29DA938E6A4D1BF0F59937FD5659FBEB425A9DA2,
	Victoria_ActivarCanvas_m68FA57B22822DEEF043407523488A0FEAEE2F516,
	Victoria_DeathCanvas_mE909D51632E28407384769D8A2A2846CA6B71AC9,
	Victoria_OnTriggerEnter2D_mF8F67B14DDDEA2EEF33B3DFFBE51AE560CF3FF8C,
	Victoria_OnTriggerExit2D_mA042A273FFC7640F12DEA5166D961ED871CCE5FC,
	Victoria_EjecutarSonidoRecoger_m29A70774497861A2E3F3A9072417832F32279D37,
	Victoria_EjecutarSonidoExplosion_m86A2A41C4EA3F15336D77BABEE24E537E33D2387,
	Victoria__ctor_m0C443C7F68D21BA5A7B896A9E45601A088A9B2D3,
	Volver_Update_m39A5AA72134B9187B6823DA7D7F73A9CE5FBC6E0,
	Volver__ctor_m156D76D394AFB7AA21D7458A0AB306C8566937A2,
	contadorAerena_Awake_mE85C5009D1F3DCD1EAD0624362D1A1E3F95FAF65,
	contadorAerena_Start_m46881FA8B6AE9E2682AA4EB9A17C56FB902DB51A,
	contadorAerena_Update_m32C574663AC59DF8CE0E33DD9A1BA671CB3FF951,
	contadorAerena_ReloadPoints_m20EB306011EBE87812DE7324DC71847A4C8F9A82,
	contadorAerena_VerPuntos_mA9D87553A9AA44413A0C1C791EC42031F4FDE384,
	contadorAerena_EjecutarSuma_mEC3153784BA9B13858B65F778A0E0A83378150ED,
	contadorAerena__ctor_m5BE8F99DE25B2DEA7B50B7B20F03DF5C423626D1,
	contadorAerena__cctor_mAC90DA8DCE00F21AC7CEA3064E77F1D8418ECA31,
	U3CEjecutarSumaU3Ed__8__ctor_mB673D2B05A6FB785A327713F2319440CFC0E92C5,
	U3CEjecutarSumaU3Ed__8_System_IDisposable_Dispose_mD1CB9B1643C7430D4E3FFCFA7DDB7A2153BC6FA8,
	U3CEjecutarSumaU3Ed__8_MoveNext_mC561D36B4977F561199A623485DAC77EDC597F46,
	U3CEjecutarSumaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m680481E7056FF135C717AD069C76864E13EF1E96,
	U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_Reset_m3744EBE3720715716DC0495BC10DB972486551D8,
	U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_get_Current_mD1812047E81A84A1D89D2A3CDE3FE94CCCCA954F,
	RuleTile_GetTileData_m50D33194EA7D3BADA2928897ED54AB319F334AAA,
	RuleTile_GetPerlinValue_m7C412F7FEE2F0AAF258B18A13679E709AB404662,
	RuleTile_GetTileAnimationData_m0EA7CD728A9180521A15DCBFDE7192517BA3CCE7,
	RuleTile_RefreshTile_m142E725163EC4019DFBE1812F2595DDDCE01B88C,
	RuleTile_RuleMatches_mBB7C6052F1F5047FC25AE39F41624C6CB33C0772,
	RuleTile_ApplyRandomTransform_mBA108C0E27F2E9E747636D841A0FBD81C4F8C1FD,
	RuleTile_RuleMatches_m6EA07CE71995B163A01104244A384B3BB643198A,
	RuleTile_RuleMatches_m0C41241F706F4EF6E6455EF0CCE69F3493F74378,
	RuleTile_GetIndexOfOffset_mED67AC50C40C31DA975BAB387E49F8FB959CC9D2,
	RuleTile_GetRotatedPos_mAC842975698C071E1299C648D57DEEB7C7477461,
	RuleTile_GetMirroredPos_m66149C28029F1751A7881F1D927CC5BFE0479E77,
	RuleTile__ctor_m34CFBB8747C6D6A1BD90FD066ACF39862F2F1C64,
	TilingRule__ctor_mB20CA5F08CDAC9601B00AB275F7761E575AFCB1A,
	GridInformation_get_PositionProperties_m7190DB7106273E84E31332A86A17A8781852B8B1,
	GridInformation_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mC4E74A564BFFFECBDC83DE7EDE0096E537D8DB9A,
	GridInformation_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2657C5497BEC07021027160F7E3E2688ACE51579,
	NULL,
	GridInformation_SetPositionProperty_mEE998C5A244C390A55E60C20545798CE9743EE67,
	GridInformation_SetPositionProperty_m8E2E89508E38BE4780164294DFE31A4312541AE1,
	GridInformation_SetPositionProperty_m71851671BC25B559EC6BF25B2EFECD2949C44CAE,
	GridInformation_SetPositionProperty_m4BB5E3451092941E84D76CA4C5E0477754EBC1B8,
	GridInformation_SetPositionProperty_m316A61EA775AA8F5E90CD84DED09C384980C7462,
	GridInformation_SetPositionProperty_mE184947C1C587A4B7C5DE8ADB0EC4D8A9D5C505E,
	GridInformation_SetPositionProperty_m9CAF712A1C1DB2B9B48D569C52305C4FDB00ABCB,
	NULL,
	GridInformation_GetPositionProperty_mFA777C4CEC9512601A13C4EA476BD7C6905B176D,
	GridInformation_GetPositionProperty_m519892FF2188B3130EFB7A3C8855E416F8901C16,
	GridInformation_GetPositionProperty_m943897E0D7AA87889FA1A81FD403456B1CECB23E,
	GridInformation_GetPositionProperty_m76C26EAE210C2E447646ED4916314A5DA8BF25A9,
	GridInformation_GetPositionProperty_m053FD4ECDBD4BCD51F14C89242BC7D1312680C0D,
	GridInformation_ErasePositionProperty_m3181035846566601E81DE18922B7FA5D90F4885D,
	GridInformation_Reset_m05B24CDA814739C06EA5162AB54A7B8BED601E1D,
	GridInformation_GetAllPositions_m046F3724E25576F1585754F4BCFF4BD71F19A14A,
	GridInformation__ctor_mD1EA81F9131ED1FA582BAA08465911133887724F,
	U3CU3Ec__DisplayClass35_0__ctor_mC7B788FDBBC7B457576723353A2B16433FFD1515,
	U3CU3Ec__DisplayClass35_0_U3CGetAllPositionsU3Eb__0_mD969FC8652F814CFC140A8D53FDF9410108CD1BF,
	U3CU3Ec__cctor_mCEF019CDC263EF40D212949986924DB706C94D16,
	U3CU3Ec__ctor_mDF3D8D615FB5D76B90DF131D4B61CF0DD9CF29F4,
	U3CU3Ec_U3CGetAllPositionsU3Eb__35_1_mBC7428A560B883736342512BD8F37B3397E803DC,
	AnimatedTile_GetTileData_mD611724E4E114405C9177FF5E3C03A32570F3AFC,
	AnimatedTile_GetTileAnimationData_m8655D1439CA8A7F8C8B036EF3C8E1988426A07CF,
	AnimatedTile__ctor_mD8826B5666F0E9DC2AE8C0BA71902EBB5A0A28CF,
	PipelineTile_RefreshTile_m9F14F64B50D3F33A2AE3ABFEA1ABE9CB1E8327F9,
	PipelineTile_GetTileData_m89CF29041CCFBC42FD50A241A0403819E2492241,
	PipelineTile_UpdateTile_mC0AE2A8F9C2B73539996FBEBD3F17C58C72B8ED0,
	PipelineTile_TileValue_m6C6B60A314F3E57D9F24D59112F660503EE93924,
	PipelineTile_GetIndex_mDACC064FBF779BAECAECEDC42E07C273DF111C45,
	PipelineTile_GetTransform_m834617C6CD253BE67EE31CE11C5012E062013B67,
	PipelineTile__ctor_m886E466F7195F7DE374FF68863324BA76FF26C3E,
	RandomTile_GetTileData_mAA02D6E857322D28436E7F4EFB23EC7C106C97A0,
	RandomTile__ctor_m756265F30DF0F9EBD5D6A59A9A6A9AB5EC7222C5,
	TerrainTile_RefreshTile_m848140DAA98343D31A5620F7E0EA2C3E84B5FE5B,
	TerrainTile_GetTileData_mD6E4EB65B6743330F23661417B3166E3F0699B4A,
	TerrainTile_UpdateTile_m6051434702FE50654875F99AB4ADCAD21BAA139E,
	TerrainTile_TileValue_m1F1B2B8553B8B2EF0CD12C6D3601FB10542DD3CE,
	TerrainTile_GetIndex_m3D963C3BBCFAA861EA358C0B35D4AA0343C8C0BF,
	TerrainTile_GetTransform_m1B4CACCADF647D47B930B07E6CFEBC4BC9B5FC08,
	TerrainTile__ctor_m02CAE0CA65CE51921F9BCAA255B13188F96D6244,
	DemoBall_Start_m055C519C0BEA3CDCD8420F274702B2E00AF4F0BD,
	DemoBall_ProgrammedDeath_m2DF4845C32A8963BAB27D7F0BFACC5784712BE92,
	DemoBall__ctor_m6204C9FCE5AF59F772854A3DD33C78507B164D86,
	U3CProgrammedDeathU3Ed__3__ctor_m594CF3FB53C5D8B5ED586479D34EE72E4943BF2F,
	U3CProgrammedDeathU3Ed__3_System_IDisposable_Dispose_m543CD3EBD9CAC69065A27E5644411AFE8B6C7B6A,
	U3CProgrammedDeathU3Ed__3_MoveNext_m5D434C9C203D4F0DFB0A9A79A44DDC2A03208F28,
	U3CProgrammedDeathU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC82FC1704348F2794A1AEB9E8C43335F3189EDEA,
	U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_Reset_m62594DC3054FFC736C7286487579485F6501901F,
	U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_get_Current_mA0F24F509E8DF2CC5413AC39750F9F5B91179303,
	DemoButton_OnEnable_mA24ED168DCA5A8707E2351E56BC167D222B9125E,
	DemoButton_HandleWebGL_mC9058210C103DFACBCC6FD0CAB270330D647FD24,
	DemoButton__ctor_mA0B3EF6D1C4B93BD9070111ABBDC8F63ADF74DAD,
	DemoGhost_OnAnimationEnd_m57D8F1F7423CF533C405120AA5E2D7CA40C58775,
	DemoGhost__ctor_m7BA344F22206526F8FBDE367F2EBE4D6A515B865,
	DemoPackageTester_Awake_m76D03BF81A53F2DB72982AFA35A01365D0F9BF07,
	DemoPackageTester_TestForDependencies_m4F479E0A244A626276ED55AC0ACDF9EC1B32A254,
	DemoPackageTester__ctor_m1A8560B283E5D5D002ED1F8498BCD3BD294B6472,
};
static const int32_t s_InvokerIndices[449] = 
{
	3445,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	5460,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	2815,
	2815,
	3506,
	2815,
	3506,
	3506,
	3506,
	2815,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	5460,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	2815,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	2815,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	2214,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3424,
	3445,
	3506,
	5460,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	1072,
	4483,
	927,
	1745,
	576,
	4134,
	577,
	259,
	2073,
	1405,
	951,
	3506,
	3506,
	3445,
	3506,
	3506,
	-1,
	930,
	931,
	932,
	929,
	931,
	928,
	589,
	-1,
	745,
	831,
	942,
	729,
	727,
	1333,
	3506,
	2209,
	3506,
	3506,
	2502,
	5460,
	3506,
	2614,
	1072,
	927,
	3506,
	1745,
	1072,
	1072,
	1304,
	2050,
	2182,
	3506,
	1072,
	3506,
	1745,
	1072,
	1072,
	1304,
	2050,
	2182,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600018F, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 173 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	449,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

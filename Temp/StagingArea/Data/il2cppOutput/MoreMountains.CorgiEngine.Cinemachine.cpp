﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,System.Object>
struct Dictionary_2_tA56D153C1EABC9FB802D4F354B4E536F249FF4FE;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>>
struct List_1_t5128DCBC80975324FF27985AB2A9F3D29DACA894;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character>
struct List_1_t5DB922CEE6338D263668FE75E45C8566802F493E;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.CheckPoint>
struct List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716;
// System.Collections.Generic.List`1<Cinemachine.CinemachineExtension>
struct List_1_t13875FE3163CFA961C09134FFAA2E0903229691A;
// System.Collections.Generic.List`1<UnityEngine.Collider2D>
struct List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<MoreMountains.Feedbacks.MMFeedback>
struct List_1_t65DB3FDD9079C29778530442558DE4285EB4A794;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointOfEntry>
struct List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3;
// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>
struct List_1_t3926283FA9AE49778D95220056CEBFB01D034379;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// System.Collections.Generic.List`1<Cinemachine.CameraState/CustomBlendable>
struct List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009;
// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain/BrainFrame>
struct List_1_t7047292520C88A40E765EA61E967062F583F6C2A;
// MoreMountains.Tools.MMEventListener`1<MoreMountains.CorgiEngine.CorgiEngineEvent>
struct MMEventListener_1_tD96F1B385B36A340EAA8B78CAF693A7323D138D5;
// MoreMountains.Tools.MMEventListener`1<MoreMountains.CorgiEngine.MMCameraEvent>
struct MMEventListener_1_tC56767DCF77F1917DF42070A976059AD5F26AEE3;
// MoreMountains.Tools.MMEventListener`1<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>
struct MMEventListener_1_t6BCACE980BB3CB24927B40E945987FCE72916CBF;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions>
struct MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/MovementStates>
struct MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0;
// UnityEngine.AnimationCurve[]
struct AnimationCurveU5BU5D_tE3C6891AFD2EF0188200F790D3120A09202E544A;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// MoreMountains.CorgiEngine.Character[]
struct CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED;
// MoreMountains.CorgiEngine.CharacterAbility[]
struct CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E;
// Cinemachine.CinemachineComponentBase[]
struct CinemachineComponentBaseU5BU5D_t103C5C1C3257AA526385CAD3B1FD18E5CC1446EC;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// Cinemachine.CinemachineCore/Stage[]
struct StageU5BU5D_t605193BD653BA57911AFE8A2D01B1D799F7BB949;
// MoreMountains.Tools.AIBrain
struct AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.BoxCollider
struct BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9;
// MoreMountains.CorgiEngine.ButtonActivated
struct ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5;
// MoreMountains.CorgiEngine.ButtonPrompt
struct ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// MoreMountains.CorgiEngine.CameraController
struct CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6;
// MoreMountains.CorgiEngine.Character
struct Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612;
// MoreMountains.CorgiEngine.CharacterButtonActivation
struct CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B;
// MoreMountains.CorgiEngine.CharacterGravity
struct CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4;
// MoreMountains.CorgiEngine.CharacterPersistence
struct CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC;
// MoreMountains.CorgiEngine.CharacterStates
struct CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F;
// MoreMountains.CorgiEngine.CheckPoint
struct CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4;
// MoreMountains.CorgiEngine.CinemachineAxisLocker
struct CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB;
// Cinemachine.CinemachineBlend
struct CinemachineBlend_tA1423010B6E480A1066D469A77F1CB4E2BC6925C;
// Cinemachine.CinemachineBlenderSettings
struct CinemachineBlenderSettings_tCF3367AA417C05816117E15FE608928CAF40554F;
// Cinemachine.CinemachineBrain
struct CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83;
// MoreMountains.CorgiEngine.CinemachineBrainController
struct CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00;
// MoreMountains.CorgiEngine.CinemachineCameraController
struct CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1;
// Cinemachine.CinemachineConfiner
struct CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D;
// Cinemachine.CinemachineExtension
struct CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422;
// Cinemachine.CinemachineFramingTransposer
struct CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1;
// Cinemachine.CinemachineVirtualCamera
struct CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C;
// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5;
// UnityEngine.CircleCollider2D
struct CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.CompositeCollider2D
struct CompositeCollider2D_tFEE36FCBB44A7A893697E113323B78808D86AF35;
// MoreMountains.CorgiEngine.CorgiCinemachineZone
struct CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4;
// MoreMountains.CorgiEngine.CorgiController
struct CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8;
// MoreMountains.CorgiEngine.CorgiControllerParameters
struct CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8;
// MoreMountains.CorgiEngine.CorgiControllerState
struct CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// MoreMountains.CorgiEngine.DamageOnTouch
struct DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// MoreMountains.CorgiEngine.Health
struct Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51;
// Cinemachine.ICinemachineCamera
struct ICinemachineCamera_t6F02F958011535010196E7CDD88578BEDB9AE685;
// Cinemachine.ICinemachineTargetGroup
struct ICinemachineTargetGroup_t1A8D13688535CFE851EE667877C439AC8D1D38CC;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// MoreMountains.CorgiEngine.InputManager
struct InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// MoreMountains.CorgiEngine.LevelManager
struct LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E;
// MoreMountains.Tools.MMAdditiveSceneLoadingManagerSettings
struct MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB;
// MoreMountains.Tools.MMCinemachineZone
struct MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5;
// MoreMountains.Tools.MMCinemachineZone2D
struct MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606;
// MoreMountains.Feedbacks.MMFeedbacks
struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914;
// MoreMountains.Feedbacks.MMFeedbacksEvents
struct MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A;
// MoreMountains.Tools.MMPathMovement
struct MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B;
// MoreMountains.Tools.MMTweenType
struct MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D;
// Cinemachine.Utility.PositionPredictor
struct PositionPredictor_tAC12D8D878524DFBAF37CBBF695B5D18B877F56A;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// MoreMountains.CorgiEngine.Room
struct Room_t392D52D203A7F630C190D47A07148A57F4020B42;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// MoreMountains.CorgiEngine.SurfaceModifier
struct SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211;
// MoreMountains.CorgiEngine.Teleporter
struct Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// Cinemachine.CinemachineBrain/BrainEvent
struct BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E;
// Cinemachine.CinemachineBrain/VcamActivatedEvent
struct VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296;
// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate
struct CreatePipelineDelegate_tB2027A3AD25F49EFC2BE7B2872BB15C1B7856C19;
// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate
struct DestroyPipelineDelegate_tA69D0FA40EDBF1EF43F75E7FE8A33D85F8DD1B28;
// MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2
struct U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F;
// MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19
struct U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3;
// MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47
struct U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6;
// MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52
struct U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B;

IL2CPP_EXTERN_C RuntimeClass* BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMDebug_tD934F35BAB52AB49F049893A676764762BF27019_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMEventManager_tB7745D6C891FB1E2397DE32D8209E3E2B5BC54A4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1_mAC4EB763DB66C85C1998F64A22FF144B5C587E2C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D_mC97A95766A1AF55DFD6A83919B545B8CF30AF8C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_m1CD1808D474C06AE9FFA2DE1BE67E8D681311E0E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObjectExtensions_MMGetComponentAroundOrAdd_TisCinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_m0B72B1326D70ABA9819B735C42FBB0F6F44E68DF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObjectExtensions_MMGetComponentNoAlloc_TisCorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8_m0F37741609CF11EE74C0EEC69597FE9F0E1525E8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponentInParent_TisRoom_t392D52D203A7F630C190D47A07148A57F4020B42_m66E446560DA3F59C8F60C6F850D6C5DE7D7C6610_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_m2A1B17232E3845E1A0E19D3B3A62281373C4A2DA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_HasInstance_mD391A08D92462F740297996852235491E938BCDD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_Reset_m30A091C8111BCAE444372D8EFF6E6C1674AEE9B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_Reset_m62D2D3B24FC3BA71ED40F889D1B05FD0D02F4A05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_Reset_m53A758DC63962FA755B19A04E75DF4A29BC4A292_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_Reset_mF9D98A4E2B68907316E1373F44042532309211F6_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBEA1D57FF2070631549B79133AD77E1AC963A08F 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character>
struct List_1_t5DB922CEE6338D263668FE75E45C8566802F493E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E, ____items_1)); }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* get__items_1() const { return ____items_1; }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t5DB922CEE6338D263668FE75E45C8566802F493E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E_StaticFields, ____emptyArray_5)); }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____items_1)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__items_1() const { return ____items_1; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5_StaticFields, ____emptyArray_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____items_1)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__items_1() const { return ____items_1; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_StaticFields, ____emptyArray_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7  : public RuntimeObject
{
public:

public:
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2
struct U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MoreMountains.CorgiEngine.CorgiCinemachineZone MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::<>4__this
	CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * ___U3CU3E4__this_2;
	// System.Boolean MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::state
	bool ___state_3;
	// System.Int32 MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::frames
	int32_t ___frames_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F, ___U3CU3E4__this_2)); }
	inline CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F, ___state_3)); }
	inline bool get_state_3() const { return ___state_3; }
	inline bool* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(bool value)
	{
		___state_3 = value;
	}

	inline static int32_t get_offset_of_frames_4() { return static_cast<int32_t>(offsetof(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F, ___frames_4)); }
	inline int32_t get_frames_4() const { return ___frames_4; }
	inline int32_t* get_address_of_frames_4() { return &___frames_4; }
	inline void set_frames_4(int32_t value)
	{
		___frames_4 = value;
	}
};


// MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19
struct U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MoreMountains.CorgiEngine.Room MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::<>4__this
	Room_t392D52D203A7F630C190D47A07148A57F4020B42 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3, ___U3CU3E4__this_2)); }
	inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Room_t392D52D203A7F630C190D47A07148A57F4020B42 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47
struct U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MoreMountains.CorgiEngine.Teleporter MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::<>4__this
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * ___U3CU3E4__this_2;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::collider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6, ___U3CU3E4__this_2)); }
	inline Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_collider_3() { return static_cast<int32_t>(offsetof(U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6, ___collider_3)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_collider_3() const { return ___collider_3; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_collider_3() { return &___collider_3; }
	inline void set_collider_3(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___collider_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collider_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>
struct Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___list_0)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_list_0() const { return ___list_0; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14, ___current_3)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_current_3() const { return ___current_3; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.DateTime
struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth365_29), (void*)value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DaysToMonth366_30), (void*)value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MinValue_31)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405_StaticFields, ___MaxValue_32)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		___MaxValue_32 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Matrix4x4
struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___identityMatrix_17 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// System.Single UnityEngine.WaitForSecondsRealtime::<waitTime>k__BackingField
	float ___U3CwaitTimeU3Ek__BackingField_0;
	// System.Single UnityEngine.WaitForSecondsRealtime::m_WaitUntilTime
	float ___m_WaitUntilTime_1;

public:
	inline static int32_t get_offset_of_U3CwaitTimeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40, ___U3CwaitTimeU3Ek__BackingField_0)); }
	inline float get_U3CwaitTimeU3Ek__BackingField_0() const { return ___U3CwaitTimeU3Ek__BackingField_0; }
	inline float* get_address_of_U3CwaitTimeU3Ek__BackingField_0() { return &___U3CwaitTimeU3Ek__BackingField_0; }
	inline void set_U3CwaitTimeU3Ek__BackingField_0(float value)
	{
		___U3CwaitTimeU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_m_WaitUntilTime_1() { return static_cast<int32_t>(offsetof(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40, ___m_WaitUntilTime_1)); }
	inline float get_m_WaitUntilTime_1() const { return ___m_WaitUntilTime_1; }
	inline float* get_address_of_m_WaitUntilTime_1() { return &___m_WaitUntilTime_1; }
	inline void set_m_WaitUntilTime_1(float value)
	{
		___m_WaitUntilTime_1 = value;
	}
};


// Cinemachine.CameraState/CustomBlendable
struct CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B 
{
public:
	// UnityEngine.Object Cinemachine.CameraState/CustomBlendable::m_Custom
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___m_Custom_0;
	// System.Single Cinemachine.CameraState/CustomBlendable::m_Weight
	float ___m_Weight_1;

public:
	inline static int32_t get_offset_of_m_Custom_0() { return static_cast<int32_t>(offsetof(CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B, ___m_Custom_0)); }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * get_m_Custom_0() const { return ___m_Custom_0; }
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** get_address_of_m_Custom_0() { return &___m_Custom_0; }
	inline void set_m_Custom_0(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		___m_Custom_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Custom_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Weight_1() { return static_cast<int32_t>(offsetof(CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B, ___m_Weight_1)); }
	inline float get_m_Weight_1() const { return ___m_Weight_1; }
	inline float* get_address_of_m_Weight_1() { return &___m_Weight_1; }
	inline void set_m_Weight_1(float value)
	{
		___m_Weight_1 = value;
	}
};


// UnityEngine.Bounds
struct Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 
{
public:
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Center
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Center_0;
	// UnityEngine.Vector3 UnityEngine.Bounds::m_Extents
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Extents_1;

public:
	inline static int32_t get_offset_of_m_Center_0() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Center_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Center_0() const { return ___m_Center_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Center_0() { return &___m_Center_0; }
	inline void set_m_Center_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Center_0 = value;
	}

	inline static int32_t get_offset_of_m_Extents_1() { return static_cast<int32_t>(offsetof(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37, ___m_Extents_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Extents_1() const { return ___m_Extents_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Extents_1() { return &___m_Extents_1; }
	inline void set_m_Extents_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Extents_1 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiEngineEventTypes
struct CorgiEngineEventTypes_tB10F6D08B2061073EA7004B64B437F75B232F7A5 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiEngineEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CorgiEngineEventTypes_tB10F6D08B2061073EA7004B64B437F75B232F7A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.MMCameraEventTypes
struct MMCameraEventTypes_t6C6D25454A45DDFA989ED15B52AB6AA55888B317 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.MMCameraEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMCameraEventTypes_t6C6D25454A45DDFA989ED15B52AB6AA55888B317, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes
struct MMCinemachineBrainEventTypes_t5E985F20D64F50E204C2FD1F684CFA95FDB1624B 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMCinemachineBrainEventTypes_t5E985F20D64F50E204C2FD1F684CFA95FDB1624B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMColors
struct MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2  : public RuntimeObject
{
public:

public:
};

struct MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields
{
public:
	// UnityEngine.Color MoreMountains.Tools.MMColors::ReunoYellow
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ReunoYellow_0;
	// UnityEngine.Color MoreMountains.Tools.MMColors::BestRed
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___BestRed_1;
	// UnityEngine.Color MoreMountains.Tools.MMColors::AliceBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___AliceBlue_2;
	// UnityEngine.Color MoreMountains.Tools.MMColors::AntiqueWhite
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___AntiqueWhite_3;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Aqua
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Aqua_4;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Aquamarine
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Aquamarine_5;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Azure
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Azure_6;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Beige
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Beige_7;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Bisque
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Bisque_8;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Black
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Black_9;
	// UnityEngine.Color MoreMountains.Tools.MMColors::BlanchedAlmond
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___BlanchedAlmond_10;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Blue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Blue_11;
	// UnityEngine.Color MoreMountains.Tools.MMColors::BlueViolet
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___BlueViolet_12;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Brown
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Brown_13;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Burlywood
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Burlywood_14;
	// UnityEngine.Color MoreMountains.Tools.MMColors::CadetBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___CadetBlue_15;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Chartreuse
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Chartreuse_16;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Chocolate
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Chocolate_17;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Coral
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Coral_18;
	// UnityEngine.Color MoreMountains.Tools.MMColors::CornflowerBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___CornflowerBlue_19;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Cornsilk
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Cornsilk_20;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Crimson
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Crimson_21;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Cyan
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Cyan_22;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkBlue_23;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkCyan
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkCyan_24;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkGoldenrod
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkGoldenrod_25;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkGray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkGray_26;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkGreen_27;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkKhaki
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkKhaki_28;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkMagenta
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkMagenta_29;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkOliveGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkOliveGreen_30;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkOrange
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkOrange_31;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkOrchid
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkOrchid_32;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkRed
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkRed_33;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkSalmon
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkSalmon_34;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkSeaGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkSeaGreen_35;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkSlateBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkSlateBlue_36;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkSlateGray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkSlateGray_37;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkTurquoise
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkTurquoise_38;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DarkViolet
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DarkViolet_39;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DeepPink
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DeepPink_40;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DeepSkyBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DeepSkyBlue_41;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DimGray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DimGray_42;
	// UnityEngine.Color MoreMountains.Tools.MMColors::DodgerBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___DodgerBlue_43;
	// UnityEngine.Color MoreMountains.Tools.MMColors::FireBrick
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___FireBrick_44;
	// UnityEngine.Color MoreMountains.Tools.MMColors::FloralWhite
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___FloralWhite_45;
	// UnityEngine.Color MoreMountains.Tools.MMColors::ForestGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ForestGreen_46;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Fuchsia
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Fuchsia_47;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Gainsboro
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Gainsboro_48;
	// UnityEngine.Color MoreMountains.Tools.MMColors::GhostWhite
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___GhostWhite_49;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Gold
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Gold_50;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Goldenrod
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Goldenrod_51;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Gray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Gray_52;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Green
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Green_53;
	// UnityEngine.Color MoreMountains.Tools.MMColors::GreenYellow
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___GreenYellow_54;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Honeydew
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Honeydew_55;
	// UnityEngine.Color MoreMountains.Tools.MMColors::HotPink
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___HotPink_56;
	// UnityEngine.Color MoreMountains.Tools.MMColors::IndianRed
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___IndianRed_57;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Indigo
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Indigo_58;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Ivory
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Ivory_59;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Khaki
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Khaki_60;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Lavender
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Lavender_61;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Lavenderblush
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Lavenderblush_62;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LawnGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LawnGreen_63;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LemonChiffon
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LemonChiffon_64;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightBlue_65;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightCoral
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightCoral_66;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightCyan
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightCyan_67;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightGoldenodYellow
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightGoldenodYellow_68;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightGray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightGray_69;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightGreen_70;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightPink
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightPink_71;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightSalmon
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightSalmon_72;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightSeaGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightSeaGreen_73;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightSkyBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightSkyBlue_74;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightSlateGray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightSlateGray_75;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightSteelBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightSteelBlue_76;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LightYellow
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LightYellow_77;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Lime
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Lime_78;
	// UnityEngine.Color MoreMountains.Tools.MMColors::LimeGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___LimeGreen_79;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Linen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Linen_80;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Magenta
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Magenta_81;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Maroon
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Maroon_82;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumAquamarine
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumAquamarine_83;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumBlue_84;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumOrchid
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumOrchid_85;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumPurple
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumPurple_86;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumSeaGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumSeaGreen_87;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumSlateBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumSlateBlue_88;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumSpringGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumSpringGreen_89;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumTurquoise
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumTurquoise_90;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MediumVioletRed
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MediumVioletRed_91;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MidnightBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MidnightBlue_92;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Mintcream
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Mintcream_93;
	// UnityEngine.Color MoreMountains.Tools.MMColors::MistyRose
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___MistyRose_94;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Moccasin
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Moccasin_95;
	// UnityEngine.Color MoreMountains.Tools.MMColors::NavajoWhite
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___NavajoWhite_96;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Navy
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Navy_97;
	// UnityEngine.Color MoreMountains.Tools.MMColors::OldLace
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___OldLace_98;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Olive
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Olive_99;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Olivedrab
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Olivedrab_100;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Orange
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Orange_101;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Orangered
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Orangered_102;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Orchid
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Orchid_103;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PaleGoldenrod
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PaleGoldenrod_104;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PaleGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PaleGreen_105;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PaleTurquoise
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PaleTurquoise_106;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PaleVioletred
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PaleVioletred_107;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PapayaWhip
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PapayaWhip_108;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PeachPuff
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PeachPuff_109;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Peru
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Peru_110;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Pink
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Pink_111;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Plum
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Plum_112;
	// UnityEngine.Color MoreMountains.Tools.MMColors::PowderBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___PowderBlue_113;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Purple
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Purple_114;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Red
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Red_115;
	// UnityEngine.Color MoreMountains.Tools.MMColors::RosyBrown
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___RosyBrown_116;
	// UnityEngine.Color MoreMountains.Tools.MMColors::RoyalBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___RoyalBlue_117;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SaddleBrown
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SaddleBrown_118;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Salmon
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Salmon_119;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SandyBrown
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SandyBrown_120;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SeaGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SeaGreen_121;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Seashell
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Seashell_122;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Sienna
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Sienna_123;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Silver
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Silver_124;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SkyBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SkyBlue_125;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SlateBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SlateBlue_126;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SlateGray
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SlateGray_127;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Snow
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Snow_128;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SpringGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SpringGreen_129;
	// UnityEngine.Color MoreMountains.Tools.MMColors::SteelBlue
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___SteelBlue_130;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Tan
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Tan_131;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Teal
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Teal_132;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Thistle
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Thistle_133;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Tomato
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Tomato_134;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Turquoise
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Turquoise_135;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Violet
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Violet_136;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Wheat
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Wheat_137;
	// UnityEngine.Color MoreMountains.Tools.MMColors::White
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___White_138;
	// UnityEngine.Color MoreMountains.Tools.MMColors::WhiteSmoke
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___WhiteSmoke_139;
	// UnityEngine.Color MoreMountains.Tools.MMColors::Yellow
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___Yellow_140;
	// UnityEngine.Color MoreMountains.Tools.MMColors::YellowGreen
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___YellowGreen_141;

public:
	inline static int32_t get_offset_of_ReunoYellow_0() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___ReunoYellow_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ReunoYellow_0() const { return ___ReunoYellow_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ReunoYellow_0() { return &___ReunoYellow_0; }
	inline void set_ReunoYellow_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ReunoYellow_0 = value;
	}

	inline static int32_t get_offset_of_BestRed_1() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___BestRed_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_BestRed_1() const { return ___BestRed_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_BestRed_1() { return &___BestRed_1; }
	inline void set_BestRed_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___BestRed_1 = value;
	}

	inline static int32_t get_offset_of_AliceBlue_2() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___AliceBlue_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_AliceBlue_2() const { return ___AliceBlue_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_AliceBlue_2() { return &___AliceBlue_2; }
	inline void set_AliceBlue_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___AliceBlue_2 = value;
	}

	inline static int32_t get_offset_of_AntiqueWhite_3() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___AntiqueWhite_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_AntiqueWhite_3() const { return ___AntiqueWhite_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_AntiqueWhite_3() { return &___AntiqueWhite_3; }
	inline void set_AntiqueWhite_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___AntiqueWhite_3 = value;
	}

	inline static int32_t get_offset_of_Aqua_4() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Aqua_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Aqua_4() const { return ___Aqua_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Aqua_4() { return &___Aqua_4; }
	inline void set_Aqua_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Aqua_4 = value;
	}

	inline static int32_t get_offset_of_Aquamarine_5() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Aquamarine_5)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Aquamarine_5() const { return ___Aquamarine_5; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Aquamarine_5() { return &___Aquamarine_5; }
	inline void set_Aquamarine_5(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Aquamarine_5 = value;
	}

	inline static int32_t get_offset_of_Azure_6() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Azure_6)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Azure_6() const { return ___Azure_6; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Azure_6() { return &___Azure_6; }
	inline void set_Azure_6(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Azure_6 = value;
	}

	inline static int32_t get_offset_of_Beige_7() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Beige_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Beige_7() const { return ___Beige_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Beige_7() { return &___Beige_7; }
	inline void set_Beige_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Beige_7 = value;
	}

	inline static int32_t get_offset_of_Bisque_8() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Bisque_8)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Bisque_8() const { return ___Bisque_8; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Bisque_8() { return &___Bisque_8; }
	inline void set_Bisque_8(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Bisque_8 = value;
	}

	inline static int32_t get_offset_of_Black_9() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Black_9)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Black_9() const { return ___Black_9; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Black_9() { return &___Black_9; }
	inline void set_Black_9(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Black_9 = value;
	}

	inline static int32_t get_offset_of_BlanchedAlmond_10() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___BlanchedAlmond_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_BlanchedAlmond_10() const { return ___BlanchedAlmond_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_BlanchedAlmond_10() { return &___BlanchedAlmond_10; }
	inline void set_BlanchedAlmond_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___BlanchedAlmond_10 = value;
	}

	inline static int32_t get_offset_of_Blue_11() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Blue_11)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Blue_11() const { return ___Blue_11; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Blue_11() { return &___Blue_11; }
	inline void set_Blue_11(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Blue_11 = value;
	}

	inline static int32_t get_offset_of_BlueViolet_12() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___BlueViolet_12)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_BlueViolet_12() const { return ___BlueViolet_12; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_BlueViolet_12() { return &___BlueViolet_12; }
	inline void set_BlueViolet_12(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___BlueViolet_12 = value;
	}

	inline static int32_t get_offset_of_Brown_13() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Brown_13)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Brown_13() const { return ___Brown_13; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Brown_13() { return &___Brown_13; }
	inline void set_Brown_13(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Brown_13 = value;
	}

	inline static int32_t get_offset_of_Burlywood_14() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Burlywood_14)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Burlywood_14() const { return ___Burlywood_14; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Burlywood_14() { return &___Burlywood_14; }
	inline void set_Burlywood_14(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Burlywood_14 = value;
	}

	inline static int32_t get_offset_of_CadetBlue_15() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___CadetBlue_15)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_CadetBlue_15() const { return ___CadetBlue_15; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_CadetBlue_15() { return &___CadetBlue_15; }
	inline void set_CadetBlue_15(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___CadetBlue_15 = value;
	}

	inline static int32_t get_offset_of_Chartreuse_16() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Chartreuse_16)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Chartreuse_16() const { return ___Chartreuse_16; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Chartreuse_16() { return &___Chartreuse_16; }
	inline void set_Chartreuse_16(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Chartreuse_16 = value;
	}

	inline static int32_t get_offset_of_Chocolate_17() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Chocolate_17)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Chocolate_17() const { return ___Chocolate_17; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Chocolate_17() { return &___Chocolate_17; }
	inline void set_Chocolate_17(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Chocolate_17 = value;
	}

	inline static int32_t get_offset_of_Coral_18() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Coral_18)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Coral_18() const { return ___Coral_18; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Coral_18() { return &___Coral_18; }
	inline void set_Coral_18(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Coral_18 = value;
	}

	inline static int32_t get_offset_of_CornflowerBlue_19() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___CornflowerBlue_19)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_CornflowerBlue_19() const { return ___CornflowerBlue_19; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_CornflowerBlue_19() { return &___CornflowerBlue_19; }
	inline void set_CornflowerBlue_19(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___CornflowerBlue_19 = value;
	}

	inline static int32_t get_offset_of_Cornsilk_20() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Cornsilk_20)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Cornsilk_20() const { return ___Cornsilk_20; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Cornsilk_20() { return &___Cornsilk_20; }
	inline void set_Cornsilk_20(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Cornsilk_20 = value;
	}

	inline static int32_t get_offset_of_Crimson_21() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Crimson_21)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Crimson_21() const { return ___Crimson_21; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Crimson_21() { return &___Crimson_21; }
	inline void set_Crimson_21(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Crimson_21 = value;
	}

	inline static int32_t get_offset_of_Cyan_22() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Cyan_22)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Cyan_22() const { return ___Cyan_22; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Cyan_22() { return &___Cyan_22; }
	inline void set_Cyan_22(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Cyan_22 = value;
	}

	inline static int32_t get_offset_of_DarkBlue_23() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkBlue_23)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkBlue_23() const { return ___DarkBlue_23; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkBlue_23() { return &___DarkBlue_23; }
	inline void set_DarkBlue_23(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkBlue_23 = value;
	}

	inline static int32_t get_offset_of_DarkCyan_24() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkCyan_24)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkCyan_24() const { return ___DarkCyan_24; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkCyan_24() { return &___DarkCyan_24; }
	inline void set_DarkCyan_24(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkCyan_24 = value;
	}

	inline static int32_t get_offset_of_DarkGoldenrod_25() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkGoldenrod_25)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkGoldenrod_25() const { return ___DarkGoldenrod_25; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkGoldenrod_25() { return &___DarkGoldenrod_25; }
	inline void set_DarkGoldenrod_25(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkGoldenrod_25 = value;
	}

	inline static int32_t get_offset_of_DarkGray_26() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkGray_26)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkGray_26() const { return ___DarkGray_26; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkGray_26() { return &___DarkGray_26; }
	inline void set_DarkGray_26(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkGray_26 = value;
	}

	inline static int32_t get_offset_of_DarkGreen_27() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkGreen_27)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkGreen_27() const { return ___DarkGreen_27; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkGreen_27() { return &___DarkGreen_27; }
	inline void set_DarkGreen_27(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkGreen_27 = value;
	}

	inline static int32_t get_offset_of_DarkKhaki_28() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkKhaki_28)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkKhaki_28() const { return ___DarkKhaki_28; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkKhaki_28() { return &___DarkKhaki_28; }
	inline void set_DarkKhaki_28(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkKhaki_28 = value;
	}

	inline static int32_t get_offset_of_DarkMagenta_29() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkMagenta_29)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkMagenta_29() const { return ___DarkMagenta_29; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkMagenta_29() { return &___DarkMagenta_29; }
	inline void set_DarkMagenta_29(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkMagenta_29 = value;
	}

	inline static int32_t get_offset_of_DarkOliveGreen_30() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkOliveGreen_30)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkOliveGreen_30() const { return ___DarkOliveGreen_30; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkOliveGreen_30() { return &___DarkOliveGreen_30; }
	inline void set_DarkOliveGreen_30(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkOliveGreen_30 = value;
	}

	inline static int32_t get_offset_of_DarkOrange_31() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkOrange_31)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkOrange_31() const { return ___DarkOrange_31; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkOrange_31() { return &___DarkOrange_31; }
	inline void set_DarkOrange_31(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkOrange_31 = value;
	}

	inline static int32_t get_offset_of_DarkOrchid_32() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkOrchid_32)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkOrchid_32() const { return ___DarkOrchid_32; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkOrchid_32() { return &___DarkOrchid_32; }
	inline void set_DarkOrchid_32(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkOrchid_32 = value;
	}

	inline static int32_t get_offset_of_DarkRed_33() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkRed_33)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkRed_33() const { return ___DarkRed_33; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkRed_33() { return &___DarkRed_33; }
	inline void set_DarkRed_33(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkRed_33 = value;
	}

	inline static int32_t get_offset_of_DarkSalmon_34() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkSalmon_34)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkSalmon_34() const { return ___DarkSalmon_34; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkSalmon_34() { return &___DarkSalmon_34; }
	inline void set_DarkSalmon_34(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkSalmon_34 = value;
	}

	inline static int32_t get_offset_of_DarkSeaGreen_35() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkSeaGreen_35)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkSeaGreen_35() const { return ___DarkSeaGreen_35; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkSeaGreen_35() { return &___DarkSeaGreen_35; }
	inline void set_DarkSeaGreen_35(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkSeaGreen_35 = value;
	}

	inline static int32_t get_offset_of_DarkSlateBlue_36() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkSlateBlue_36)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkSlateBlue_36() const { return ___DarkSlateBlue_36; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkSlateBlue_36() { return &___DarkSlateBlue_36; }
	inline void set_DarkSlateBlue_36(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkSlateBlue_36 = value;
	}

	inline static int32_t get_offset_of_DarkSlateGray_37() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkSlateGray_37)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkSlateGray_37() const { return ___DarkSlateGray_37; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkSlateGray_37() { return &___DarkSlateGray_37; }
	inline void set_DarkSlateGray_37(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkSlateGray_37 = value;
	}

	inline static int32_t get_offset_of_DarkTurquoise_38() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkTurquoise_38)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkTurquoise_38() const { return ___DarkTurquoise_38; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkTurquoise_38() { return &___DarkTurquoise_38; }
	inline void set_DarkTurquoise_38(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkTurquoise_38 = value;
	}

	inline static int32_t get_offset_of_DarkViolet_39() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DarkViolet_39)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DarkViolet_39() const { return ___DarkViolet_39; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DarkViolet_39() { return &___DarkViolet_39; }
	inline void set_DarkViolet_39(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DarkViolet_39 = value;
	}

	inline static int32_t get_offset_of_DeepPink_40() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DeepPink_40)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DeepPink_40() const { return ___DeepPink_40; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DeepPink_40() { return &___DeepPink_40; }
	inline void set_DeepPink_40(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DeepPink_40 = value;
	}

	inline static int32_t get_offset_of_DeepSkyBlue_41() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DeepSkyBlue_41)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DeepSkyBlue_41() const { return ___DeepSkyBlue_41; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DeepSkyBlue_41() { return &___DeepSkyBlue_41; }
	inline void set_DeepSkyBlue_41(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DeepSkyBlue_41 = value;
	}

	inline static int32_t get_offset_of_DimGray_42() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DimGray_42)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DimGray_42() const { return ___DimGray_42; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DimGray_42() { return &___DimGray_42; }
	inline void set_DimGray_42(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DimGray_42 = value;
	}

	inline static int32_t get_offset_of_DodgerBlue_43() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___DodgerBlue_43)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_DodgerBlue_43() const { return ___DodgerBlue_43; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_DodgerBlue_43() { return &___DodgerBlue_43; }
	inline void set_DodgerBlue_43(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___DodgerBlue_43 = value;
	}

	inline static int32_t get_offset_of_FireBrick_44() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___FireBrick_44)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_FireBrick_44() const { return ___FireBrick_44; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_FireBrick_44() { return &___FireBrick_44; }
	inline void set_FireBrick_44(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___FireBrick_44 = value;
	}

	inline static int32_t get_offset_of_FloralWhite_45() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___FloralWhite_45)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_FloralWhite_45() const { return ___FloralWhite_45; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_FloralWhite_45() { return &___FloralWhite_45; }
	inline void set_FloralWhite_45(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___FloralWhite_45 = value;
	}

	inline static int32_t get_offset_of_ForestGreen_46() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___ForestGreen_46)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ForestGreen_46() const { return ___ForestGreen_46; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ForestGreen_46() { return &___ForestGreen_46; }
	inline void set_ForestGreen_46(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ForestGreen_46 = value;
	}

	inline static int32_t get_offset_of_Fuchsia_47() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Fuchsia_47)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Fuchsia_47() const { return ___Fuchsia_47; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Fuchsia_47() { return &___Fuchsia_47; }
	inline void set_Fuchsia_47(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Fuchsia_47 = value;
	}

	inline static int32_t get_offset_of_Gainsboro_48() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Gainsboro_48)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Gainsboro_48() const { return ___Gainsboro_48; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Gainsboro_48() { return &___Gainsboro_48; }
	inline void set_Gainsboro_48(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Gainsboro_48 = value;
	}

	inline static int32_t get_offset_of_GhostWhite_49() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___GhostWhite_49)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_GhostWhite_49() const { return ___GhostWhite_49; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_GhostWhite_49() { return &___GhostWhite_49; }
	inline void set_GhostWhite_49(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___GhostWhite_49 = value;
	}

	inline static int32_t get_offset_of_Gold_50() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Gold_50)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Gold_50() const { return ___Gold_50; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Gold_50() { return &___Gold_50; }
	inline void set_Gold_50(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Gold_50 = value;
	}

	inline static int32_t get_offset_of_Goldenrod_51() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Goldenrod_51)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Goldenrod_51() const { return ___Goldenrod_51; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Goldenrod_51() { return &___Goldenrod_51; }
	inline void set_Goldenrod_51(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Goldenrod_51 = value;
	}

	inline static int32_t get_offset_of_Gray_52() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Gray_52)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Gray_52() const { return ___Gray_52; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Gray_52() { return &___Gray_52; }
	inline void set_Gray_52(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Gray_52 = value;
	}

	inline static int32_t get_offset_of_Green_53() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Green_53)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Green_53() const { return ___Green_53; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Green_53() { return &___Green_53; }
	inline void set_Green_53(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Green_53 = value;
	}

	inline static int32_t get_offset_of_GreenYellow_54() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___GreenYellow_54)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_GreenYellow_54() const { return ___GreenYellow_54; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_GreenYellow_54() { return &___GreenYellow_54; }
	inline void set_GreenYellow_54(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___GreenYellow_54 = value;
	}

	inline static int32_t get_offset_of_Honeydew_55() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Honeydew_55)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Honeydew_55() const { return ___Honeydew_55; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Honeydew_55() { return &___Honeydew_55; }
	inline void set_Honeydew_55(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Honeydew_55 = value;
	}

	inline static int32_t get_offset_of_HotPink_56() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___HotPink_56)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_HotPink_56() const { return ___HotPink_56; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_HotPink_56() { return &___HotPink_56; }
	inline void set_HotPink_56(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___HotPink_56 = value;
	}

	inline static int32_t get_offset_of_IndianRed_57() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___IndianRed_57)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_IndianRed_57() const { return ___IndianRed_57; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_IndianRed_57() { return &___IndianRed_57; }
	inline void set_IndianRed_57(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___IndianRed_57 = value;
	}

	inline static int32_t get_offset_of_Indigo_58() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Indigo_58)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Indigo_58() const { return ___Indigo_58; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Indigo_58() { return &___Indigo_58; }
	inline void set_Indigo_58(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Indigo_58 = value;
	}

	inline static int32_t get_offset_of_Ivory_59() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Ivory_59)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Ivory_59() const { return ___Ivory_59; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Ivory_59() { return &___Ivory_59; }
	inline void set_Ivory_59(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Ivory_59 = value;
	}

	inline static int32_t get_offset_of_Khaki_60() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Khaki_60)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Khaki_60() const { return ___Khaki_60; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Khaki_60() { return &___Khaki_60; }
	inline void set_Khaki_60(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Khaki_60 = value;
	}

	inline static int32_t get_offset_of_Lavender_61() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Lavender_61)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Lavender_61() const { return ___Lavender_61; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Lavender_61() { return &___Lavender_61; }
	inline void set_Lavender_61(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Lavender_61 = value;
	}

	inline static int32_t get_offset_of_Lavenderblush_62() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Lavenderblush_62)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Lavenderblush_62() const { return ___Lavenderblush_62; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Lavenderblush_62() { return &___Lavenderblush_62; }
	inline void set_Lavenderblush_62(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Lavenderblush_62 = value;
	}

	inline static int32_t get_offset_of_LawnGreen_63() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LawnGreen_63)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LawnGreen_63() const { return ___LawnGreen_63; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LawnGreen_63() { return &___LawnGreen_63; }
	inline void set_LawnGreen_63(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LawnGreen_63 = value;
	}

	inline static int32_t get_offset_of_LemonChiffon_64() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LemonChiffon_64)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LemonChiffon_64() const { return ___LemonChiffon_64; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LemonChiffon_64() { return &___LemonChiffon_64; }
	inline void set_LemonChiffon_64(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LemonChiffon_64 = value;
	}

	inline static int32_t get_offset_of_LightBlue_65() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightBlue_65)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightBlue_65() const { return ___LightBlue_65; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightBlue_65() { return &___LightBlue_65; }
	inline void set_LightBlue_65(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightBlue_65 = value;
	}

	inline static int32_t get_offset_of_LightCoral_66() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightCoral_66)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightCoral_66() const { return ___LightCoral_66; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightCoral_66() { return &___LightCoral_66; }
	inline void set_LightCoral_66(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightCoral_66 = value;
	}

	inline static int32_t get_offset_of_LightCyan_67() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightCyan_67)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightCyan_67() const { return ___LightCyan_67; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightCyan_67() { return &___LightCyan_67; }
	inline void set_LightCyan_67(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightCyan_67 = value;
	}

	inline static int32_t get_offset_of_LightGoldenodYellow_68() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightGoldenodYellow_68)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightGoldenodYellow_68() const { return ___LightGoldenodYellow_68; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightGoldenodYellow_68() { return &___LightGoldenodYellow_68; }
	inline void set_LightGoldenodYellow_68(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightGoldenodYellow_68 = value;
	}

	inline static int32_t get_offset_of_LightGray_69() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightGray_69)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightGray_69() const { return ___LightGray_69; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightGray_69() { return &___LightGray_69; }
	inline void set_LightGray_69(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightGray_69 = value;
	}

	inline static int32_t get_offset_of_LightGreen_70() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightGreen_70)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightGreen_70() const { return ___LightGreen_70; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightGreen_70() { return &___LightGreen_70; }
	inline void set_LightGreen_70(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightGreen_70 = value;
	}

	inline static int32_t get_offset_of_LightPink_71() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightPink_71)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightPink_71() const { return ___LightPink_71; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightPink_71() { return &___LightPink_71; }
	inline void set_LightPink_71(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightPink_71 = value;
	}

	inline static int32_t get_offset_of_LightSalmon_72() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightSalmon_72)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightSalmon_72() const { return ___LightSalmon_72; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightSalmon_72() { return &___LightSalmon_72; }
	inline void set_LightSalmon_72(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightSalmon_72 = value;
	}

	inline static int32_t get_offset_of_LightSeaGreen_73() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightSeaGreen_73)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightSeaGreen_73() const { return ___LightSeaGreen_73; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightSeaGreen_73() { return &___LightSeaGreen_73; }
	inline void set_LightSeaGreen_73(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightSeaGreen_73 = value;
	}

	inline static int32_t get_offset_of_LightSkyBlue_74() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightSkyBlue_74)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightSkyBlue_74() const { return ___LightSkyBlue_74; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightSkyBlue_74() { return &___LightSkyBlue_74; }
	inline void set_LightSkyBlue_74(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightSkyBlue_74 = value;
	}

	inline static int32_t get_offset_of_LightSlateGray_75() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightSlateGray_75)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightSlateGray_75() const { return ___LightSlateGray_75; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightSlateGray_75() { return &___LightSlateGray_75; }
	inline void set_LightSlateGray_75(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightSlateGray_75 = value;
	}

	inline static int32_t get_offset_of_LightSteelBlue_76() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightSteelBlue_76)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightSteelBlue_76() const { return ___LightSteelBlue_76; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightSteelBlue_76() { return &___LightSteelBlue_76; }
	inline void set_LightSteelBlue_76(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightSteelBlue_76 = value;
	}

	inline static int32_t get_offset_of_LightYellow_77() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LightYellow_77)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LightYellow_77() const { return ___LightYellow_77; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LightYellow_77() { return &___LightYellow_77; }
	inline void set_LightYellow_77(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LightYellow_77 = value;
	}

	inline static int32_t get_offset_of_Lime_78() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Lime_78)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Lime_78() const { return ___Lime_78; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Lime_78() { return &___Lime_78; }
	inline void set_Lime_78(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Lime_78 = value;
	}

	inline static int32_t get_offset_of_LimeGreen_79() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___LimeGreen_79)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_LimeGreen_79() const { return ___LimeGreen_79; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_LimeGreen_79() { return &___LimeGreen_79; }
	inline void set_LimeGreen_79(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___LimeGreen_79 = value;
	}

	inline static int32_t get_offset_of_Linen_80() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Linen_80)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Linen_80() const { return ___Linen_80; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Linen_80() { return &___Linen_80; }
	inline void set_Linen_80(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Linen_80 = value;
	}

	inline static int32_t get_offset_of_Magenta_81() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Magenta_81)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Magenta_81() const { return ___Magenta_81; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Magenta_81() { return &___Magenta_81; }
	inline void set_Magenta_81(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Magenta_81 = value;
	}

	inline static int32_t get_offset_of_Maroon_82() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Maroon_82)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Maroon_82() const { return ___Maroon_82; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Maroon_82() { return &___Maroon_82; }
	inline void set_Maroon_82(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Maroon_82 = value;
	}

	inline static int32_t get_offset_of_MediumAquamarine_83() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumAquamarine_83)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumAquamarine_83() const { return ___MediumAquamarine_83; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumAquamarine_83() { return &___MediumAquamarine_83; }
	inline void set_MediumAquamarine_83(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumAquamarine_83 = value;
	}

	inline static int32_t get_offset_of_MediumBlue_84() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumBlue_84)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumBlue_84() const { return ___MediumBlue_84; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumBlue_84() { return &___MediumBlue_84; }
	inline void set_MediumBlue_84(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumBlue_84 = value;
	}

	inline static int32_t get_offset_of_MediumOrchid_85() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumOrchid_85)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumOrchid_85() const { return ___MediumOrchid_85; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumOrchid_85() { return &___MediumOrchid_85; }
	inline void set_MediumOrchid_85(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumOrchid_85 = value;
	}

	inline static int32_t get_offset_of_MediumPurple_86() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumPurple_86)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumPurple_86() const { return ___MediumPurple_86; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumPurple_86() { return &___MediumPurple_86; }
	inline void set_MediumPurple_86(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumPurple_86 = value;
	}

	inline static int32_t get_offset_of_MediumSeaGreen_87() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumSeaGreen_87)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumSeaGreen_87() const { return ___MediumSeaGreen_87; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumSeaGreen_87() { return &___MediumSeaGreen_87; }
	inline void set_MediumSeaGreen_87(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumSeaGreen_87 = value;
	}

	inline static int32_t get_offset_of_MediumSlateBlue_88() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumSlateBlue_88)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumSlateBlue_88() const { return ___MediumSlateBlue_88; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumSlateBlue_88() { return &___MediumSlateBlue_88; }
	inline void set_MediumSlateBlue_88(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumSlateBlue_88 = value;
	}

	inline static int32_t get_offset_of_MediumSpringGreen_89() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumSpringGreen_89)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumSpringGreen_89() const { return ___MediumSpringGreen_89; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumSpringGreen_89() { return &___MediumSpringGreen_89; }
	inline void set_MediumSpringGreen_89(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumSpringGreen_89 = value;
	}

	inline static int32_t get_offset_of_MediumTurquoise_90() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumTurquoise_90)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumTurquoise_90() const { return ___MediumTurquoise_90; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumTurquoise_90() { return &___MediumTurquoise_90; }
	inline void set_MediumTurquoise_90(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumTurquoise_90 = value;
	}

	inline static int32_t get_offset_of_MediumVioletRed_91() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MediumVioletRed_91)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MediumVioletRed_91() const { return ___MediumVioletRed_91; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MediumVioletRed_91() { return &___MediumVioletRed_91; }
	inline void set_MediumVioletRed_91(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MediumVioletRed_91 = value;
	}

	inline static int32_t get_offset_of_MidnightBlue_92() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MidnightBlue_92)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MidnightBlue_92() const { return ___MidnightBlue_92; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MidnightBlue_92() { return &___MidnightBlue_92; }
	inline void set_MidnightBlue_92(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MidnightBlue_92 = value;
	}

	inline static int32_t get_offset_of_Mintcream_93() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Mintcream_93)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Mintcream_93() const { return ___Mintcream_93; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Mintcream_93() { return &___Mintcream_93; }
	inline void set_Mintcream_93(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Mintcream_93 = value;
	}

	inline static int32_t get_offset_of_MistyRose_94() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___MistyRose_94)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_MistyRose_94() const { return ___MistyRose_94; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_MistyRose_94() { return &___MistyRose_94; }
	inline void set_MistyRose_94(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___MistyRose_94 = value;
	}

	inline static int32_t get_offset_of_Moccasin_95() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Moccasin_95)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Moccasin_95() const { return ___Moccasin_95; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Moccasin_95() { return &___Moccasin_95; }
	inline void set_Moccasin_95(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Moccasin_95 = value;
	}

	inline static int32_t get_offset_of_NavajoWhite_96() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___NavajoWhite_96)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_NavajoWhite_96() const { return ___NavajoWhite_96; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_NavajoWhite_96() { return &___NavajoWhite_96; }
	inline void set_NavajoWhite_96(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___NavajoWhite_96 = value;
	}

	inline static int32_t get_offset_of_Navy_97() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Navy_97)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Navy_97() const { return ___Navy_97; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Navy_97() { return &___Navy_97; }
	inline void set_Navy_97(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Navy_97 = value;
	}

	inline static int32_t get_offset_of_OldLace_98() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___OldLace_98)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_OldLace_98() const { return ___OldLace_98; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_OldLace_98() { return &___OldLace_98; }
	inline void set_OldLace_98(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___OldLace_98 = value;
	}

	inline static int32_t get_offset_of_Olive_99() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Olive_99)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Olive_99() const { return ___Olive_99; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Olive_99() { return &___Olive_99; }
	inline void set_Olive_99(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Olive_99 = value;
	}

	inline static int32_t get_offset_of_Olivedrab_100() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Olivedrab_100)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Olivedrab_100() const { return ___Olivedrab_100; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Olivedrab_100() { return &___Olivedrab_100; }
	inline void set_Olivedrab_100(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Olivedrab_100 = value;
	}

	inline static int32_t get_offset_of_Orange_101() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Orange_101)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Orange_101() const { return ___Orange_101; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Orange_101() { return &___Orange_101; }
	inline void set_Orange_101(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Orange_101 = value;
	}

	inline static int32_t get_offset_of_Orangered_102() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Orangered_102)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Orangered_102() const { return ___Orangered_102; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Orangered_102() { return &___Orangered_102; }
	inline void set_Orangered_102(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Orangered_102 = value;
	}

	inline static int32_t get_offset_of_Orchid_103() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Orchid_103)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Orchid_103() const { return ___Orchid_103; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Orchid_103() { return &___Orchid_103; }
	inline void set_Orchid_103(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Orchid_103 = value;
	}

	inline static int32_t get_offset_of_PaleGoldenrod_104() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PaleGoldenrod_104)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PaleGoldenrod_104() const { return ___PaleGoldenrod_104; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PaleGoldenrod_104() { return &___PaleGoldenrod_104; }
	inline void set_PaleGoldenrod_104(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PaleGoldenrod_104 = value;
	}

	inline static int32_t get_offset_of_PaleGreen_105() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PaleGreen_105)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PaleGreen_105() const { return ___PaleGreen_105; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PaleGreen_105() { return &___PaleGreen_105; }
	inline void set_PaleGreen_105(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PaleGreen_105 = value;
	}

	inline static int32_t get_offset_of_PaleTurquoise_106() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PaleTurquoise_106)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PaleTurquoise_106() const { return ___PaleTurquoise_106; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PaleTurquoise_106() { return &___PaleTurquoise_106; }
	inline void set_PaleTurquoise_106(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PaleTurquoise_106 = value;
	}

	inline static int32_t get_offset_of_PaleVioletred_107() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PaleVioletred_107)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PaleVioletred_107() const { return ___PaleVioletred_107; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PaleVioletred_107() { return &___PaleVioletred_107; }
	inline void set_PaleVioletred_107(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PaleVioletred_107 = value;
	}

	inline static int32_t get_offset_of_PapayaWhip_108() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PapayaWhip_108)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PapayaWhip_108() const { return ___PapayaWhip_108; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PapayaWhip_108() { return &___PapayaWhip_108; }
	inline void set_PapayaWhip_108(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PapayaWhip_108 = value;
	}

	inline static int32_t get_offset_of_PeachPuff_109() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PeachPuff_109)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PeachPuff_109() const { return ___PeachPuff_109; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PeachPuff_109() { return &___PeachPuff_109; }
	inline void set_PeachPuff_109(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PeachPuff_109 = value;
	}

	inline static int32_t get_offset_of_Peru_110() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Peru_110)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Peru_110() const { return ___Peru_110; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Peru_110() { return &___Peru_110; }
	inline void set_Peru_110(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Peru_110 = value;
	}

	inline static int32_t get_offset_of_Pink_111() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Pink_111)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Pink_111() const { return ___Pink_111; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Pink_111() { return &___Pink_111; }
	inline void set_Pink_111(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Pink_111 = value;
	}

	inline static int32_t get_offset_of_Plum_112() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Plum_112)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Plum_112() const { return ___Plum_112; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Plum_112() { return &___Plum_112; }
	inline void set_Plum_112(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Plum_112 = value;
	}

	inline static int32_t get_offset_of_PowderBlue_113() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___PowderBlue_113)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_PowderBlue_113() const { return ___PowderBlue_113; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_PowderBlue_113() { return &___PowderBlue_113; }
	inline void set_PowderBlue_113(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___PowderBlue_113 = value;
	}

	inline static int32_t get_offset_of_Purple_114() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Purple_114)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Purple_114() const { return ___Purple_114; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Purple_114() { return &___Purple_114; }
	inline void set_Purple_114(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Purple_114 = value;
	}

	inline static int32_t get_offset_of_Red_115() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Red_115)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Red_115() const { return ___Red_115; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Red_115() { return &___Red_115; }
	inline void set_Red_115(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Red_115 = value;
	}

	inline static int32_t get_offset_of_RosyBrown_116() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___RosyBrown_116)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_RosyBrown_116() const { return ___RosyBrown_116; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_RosyBrown_116() { return &___RosyBrown_116; }
	inline void set_RosyBrown_116(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___RosyBrown_116 = value;
	}

	inline static int32_t get_offset_of_RoyalBlue_117() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___RoyalBlue_117)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_RoyalBlue_117() const { return ___RoyalBlue_117; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_RoyalBlue_117() { return &___RoyalBlue_117; }
	inline void set_RoyalBlue_117(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___RoyalBlue_117 = value;
	}

	inline static int32_t get_offset_of_SaddleBrown_118() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SaddleBrown_118)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SaddleBrown_118() const { return ___SaddleBrown_118; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SaddleBrown_118() { return &___SaddleBrown_118; }
	inline void set_SaddleBrown_118(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SaddleBrown_118 = value;
	}

	inline static int32_t get_offset_of_Salmon_119() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Salmon_119)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Salmon_119() const { return ___Salmon_119; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Salmon_119() { return &___Salmon_119; }
	inline void set_Salmon_119(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Salmon_119 = value;
	}

	inline static int32_t get_offset_of_SandyBrown_120() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SandyBrown_120)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SandyBrown_120() const { return ___SandyBrown_120; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SandyBrown_120() { return &___SandyBrown_120; }
	inline void set_SandyBrown_120(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SandyBrown_120 = value;
	}

	inline static int32_t get_offset_of_SeaGreen_121() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SeaGreen_121)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SeaGreen_121() const { return ___SeaGreen_121; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SeaGreen_121() { return &___SeaGreen_121; }
	inline void set_SeaGreen_121(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SeaGreen_121 = value;
	}

	inline static int32_t get_offset_of_Seashell_122() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Seashell_122)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Seashell_122() const { return ___Seashell_122; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Seashell_122() { return &___Seashell_122; }
	inline void set_Seashell_122(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Seashell_122 = value;
	}

	inline static int32_t get_offset_of_Sienna_123() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Sienna_123)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Sienna_123() const { return ___Sienna_123; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Sienna_123() { return &___Sienna_123; }
	inline void set_Sienna_123(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Sienna_123 = value;
	}

	inline static int32_t get_offset_of_Silver_124() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Silver_124)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Silver_124() const { return ___Silver_124; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Silver_124() { return &___Silver_124; }
	inline void set_Silver_124(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Silver_124 = value;
	}

	inline static int32_t get_offset_of_SkyBlue_125() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SkyBlue_125)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SkyBlue_125() const { return ___SkyBlue_125; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SkyBlue_125() { return &___SkyBlue_125; }
	inline void set_SkyBlue_125(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SkyBlue_125 = value;
	}

	inline static int32_t get_offset_of_SlateBlue_126() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SlateBlue_126)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SlateBlue_126() const { return ___SlateBlue_126; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SlateBlue_126() { return &___SlateBlue_126; }
	inline void set_SlateBlue_126(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SlateBlue_126 = value;
	}

	inline static int32_t get_offset_of_SlateGray_127() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SlateGray_127)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SlateGray_127() const { return ___SlateGray_127; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SlateGray_127() { return &___SlateGray_127; }
	inline void set_SlateGray_127(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SlateGray_127 = value;
	}

	inline static int32_t get_offset_of_Snow_128() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Snow_128)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Snow_128() const { return ___Snow_128; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Snow_128() { return &___Snow_128; }
	inline void set_Snow_128(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Snow_128 = value;
	}

	inline static int32_t get_offset_of_SpringGreen_129() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SpringGreen_129)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SpringGreen_129() const { return ___SpringGreen_129; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SpringGreen_129() { return &___SpringGreen_129; }
	inline void set_SpringGreen_129(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SpringGreen_129 = value;
	}

	inline static int32_t get_offset_of_SteelBlue_130() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___SteelBlue_130)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_SteelBlue_130() const { return ___SteelBlue_130; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_SteelBlue_130() { return &___SteelBlue_130; }
	inline void set_SteelBlue_130(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___SteelBlue_130 = value;
	}

	inline static int32_t get_offset_of_Tan_131() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Tan_131)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Tan_131() const { return ___Tan_131; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Tan_131() { return &___Tan_131; }
	inline void set_Tan_131(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Tan_131 = value;
	}

	inline static int32_t get_offset_of_Teal_132() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Teal_132)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Teal_132() const { return ___Teal_132; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Teal_132() { return &___Teal_132; }
	inline void set_Teal_132(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Teal_132 = value;
	}

	inline static int32_t get_offset_of_Thistle_133() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Thistle_133)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Thistle_133() const { return ___Thistle_133; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Thistle_133() { return &___Thistle_133; }
	inline void set_Thistle_133(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Thistle_133 = value;
	}

	inline static int32_t get_offset_of_Tomato_134() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Tomato_134)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Tomato_134() const { return ___Tomato_134; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Tomato_134() { return &___Tomato_134; }
	inline void set_Tomato_134(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Tomato_134 = value;
	}

	inline static int32_t get_offset_of_Turquoise_135() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Turquoise_135)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Turquoise_135() const { return ___Turquoise_135; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Turquoise_135() { return &___Turquoise_135; }
	inline void set_Turquoise_135(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Turquoise_135 = value;
	}

	inline static int32_t get_offset_of_Violet_136() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Violet_136)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Violet_136() const { return ___Violet_136; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Violet_136() { return &___Violet_136; }
	inline void set_Violet_136(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Violet_136 = value;
	}

	inline static int32_t get_offset_of_Wheat_137() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Wheat_137)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Wheat_137() const { return ___Wheat_137; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Wheat_137() { return &___Wheat_137; }
	inline void set_Wheat_137(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Wheat_137 = value;
	}

	inline static int32_t get_offset_of_White_138() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___White_138)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_White_138() const { return ___White_138; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_White_138() { return &___White_138; }
	inline void set_White_138(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___White_138 = value;
	}

	inline static int32_t get_offset_of_WhiteSmoke_139() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___WhiteSmoke_139)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_WhiteSmoke_139() const { return ___WhiteSmoke_139; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_WhiteSmoke_139() { return &___WhiteSmoke_139; }
	inline void set_WhiteSmoke_139(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___WhiteSmoke_139 = value;
	}

	inline static int32_t get_offset_of_Yellow_140() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___Yellow_140)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_Yellow_140() const { return ___Yellow_140; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_Yellow_140() { return &___Yellow_140; }
	inline void set_Yellow_140(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___Yellow_140 = value;
	}

	inline static int32_t get_offset_of_YellowGreen_141() { return static_cast<int32_t>(offsetof(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields, ___YellowGreen_141)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_YellowGreen_141() const { return ___YellowGreen_141; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_YellowGreen_141() { return &___YellowGreen_141; }
	inline void set_YellowGreen_141(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___YellowGreen_141 = value;
	}
};


// MoreMountains.Feedbacks.MMTimeScaleMethods
struct MMTimeScaleMethods_tD88E28609762D4DEA61E293671C7020D238052AF 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMTimeScaleMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMTimeScaleMethods_tD88E28609762D4DEA61E293671C7020D238052AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMTweenDefinitionTypes
struct MMTweenDefinitionTypes_tD5323C434ABBB30864F8B8DFEFC4756B41B92F41 
{
public:
	// System.Int32 MoreMountains.Tools.MMTweenDefinitionTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMTweenDefinitionTypes_tD5323C434ABBB30864F8B8DFEFC4756B41B92F41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RaycastHit2D
struct RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// System.Int32 UnityEngine.RaycastHit2D::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Centroid_0)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Point_1)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Normal_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// MoreMountains.CorgiEngine.ButtonActivated/ButtonActivatedRequirements
struct ButtonActivatedRequirements_t50772F687A7EA08BA060844877EC546DD0257B97 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated/ButtonActivatedRequirements::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonActivatedRequirements_t50772F687A7EA08BA060844877EC546DD0257B97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.ButtonActivated/InputTypes
struct InputTypes_t7B39F8DC5451B97BCAB5F6472EF98900886BED85 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated/InputTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputTypes_t7B39F8DC5451B97BCAB5F6472EF98900886BED85, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Camera/GateFitMode
struct GateFitMode_tDBD4389C530FAD67ED78789C02808A1D2DB4A525 
{
public:
	// System.Int32 UnityEngine.Camera/GateFitMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GateFitMode_tDBD4389C530FAD67ED78789C02808A1D2DB4A525, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CameraState/BlendHintValue
struct BlendHintValue_t7E87A2F98F21271FE108514B78A38754BEBE94F4 
{
public:
	// System.Int32 Cinemachine.CameraState/BlendHintValue::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendHintValue_t7E87A2F98F21271FE108514B78A38754BEBE94F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Character/CharacterTypes
struct CharacterTypes_t9A5C5F8A30B288ECE56E91FE85D030DCEFCCFA1C 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Character/CharacterTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterTypes_t9A5C5F8A30B288ECE56E91FE85D030DCEFCCFA1C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Character/FacingDirections
struct FacingDirections_t0EB9AC4553B3988723F93C6BBF8E28FDAA96FB0F 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Character/FacingDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FacingDirections_t0EB9AC4553B3988723F93C6BBF8E28FDAA96FB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Character/SpawnFacingDirections
struct SpawnFacingDirections_tCC32C8E497FCD0F662292ECF906CC88C1EDD469C 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Character/SpawnFacingDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpawnFacingDirections_tCC32C8E497FCD0F662292ECF906CC88C1EDD469C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CharacterStates/CharacterConditions
struct CharacterConditions_t9AF9C636D95BE10970493822C7C0F2D08E6ECFBC 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CharacterStates/CharacterConditions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CharacterConditions_t9AF9C636D95BE10970493822C7C0F2D08E6ECFBC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CinemachineAxisLocker/Methods
struct Methods_t1EB212F1B84A992FEDDC93F3CE484070C76F2859 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CinemachineAxisLocker/Methods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Methods_t1EB212F1B84A992FEDDC93F3CE484070C76F2859, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBlendDefinition/Style
struct Style_t710EA49EB59E246B2C5DC218BDC1352DABAC757E 
{
public:
	// System.Int32 Cinemachine.CinemachineBlendDefinition/Style::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Style_t710EA49EB59E246B2C5DC218BDC1352DABAC757E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBrain/BrainUpdateMethod
struct BrainUpdateMethod_t38DCF33A6BB4E3829652F644BDC79617BBBE6AEF 
{
public:
	// System.Int32 Cinemachine.CinemachineBrain/BrainUpdateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BrainUpdateMethod_t38DCF33A6BB4E3829652F644BDC79617BBBE6AEF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBrain/UpdateMethod
struct UpdateMethod_t2BC8740E83444F42609FCB121D59B2B147B7E2EA 
{
public:
	// System.Int32 Cinemachine.CinemachineBrain/UpdateMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateMethod_t2BC8740E83444F42609FCB121D59B2B147B7E2EA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CinemachineCameraController/PerspectiveZoomMethods
struct PerspectiveZoomMethods_t496A2DB69445EC63B7424CF1ECC384ADE37F160D 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CinemachineCameraController/PerspectiveZoomMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PerspectiveZoomMethods_t496A2DB69445EC63B7424CF1ECC384ADE37F160D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineConfiner/Mode
struct Mode_t5CB3A7C5755C6E61B62817791C7354B07ABAF3F7 
{
public:
	// System.Int32 Cinemachine.CinemachineConfiner/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t5CB3A7C5755C6E61B62817791C7354B07ABAF3F7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineCore/Stage
struct Stage_t9554C4B330D6E1FC747961284AC5B84778D8FC0D 
{
public:
	// System.Int32 Cinemachine.CinemachineCore/Stage::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Stage_t9554C4B330D6E1FC747961284AC5B84778D8FC0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineFramingTransposer/AdjustmentMode
struct AdjustmentMode_t77C399D5B0175861AB36EC4669E6840209240BDD 
{
public:
	// System.Int32 Cinemachine.CinemachineFramingTransposer/AdjustmentMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdjustmentMode_t77C399D5B0175861AB36EC4669E6840209240BDD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineFramingTransposer/FramingMode
struct FramingMode_tD3B894A01E8B950B2112655A4488344ABC45D48F 
{
public:
	// System.Int32 Cinemachine.CinemachineFramingTransposer/FramingMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FramingMode_tD3B894A01E8B950B2112655A4488344ABC45D48F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineVirtualCameraBase/BlendHint
struct BlendHint_t9B1FFA41B35EC74367AB4D7A1AE2A4D1FE29B7CE 
{
public:
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase/BlendHint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendHint_t9B1FFA41B35EC74367AB4D7A1AE2A4D1FE29B7CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineVirtualCameraBase/StandbyUpdateMode
struct StandbyUpdateMode_t60D677E92B072D6B19C7F69C3DD753EB3457F3A5 
{
public:
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase/StandbyUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StandbyUpdateMode_t60D677E92B072D6B19C7F69C3DD753EB3457F3A5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiController/DetachmentMethods
struct DetachmentMethods_tB1BEBFED5C211907CA24D7D7A54098431911C245 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiController/DetachmentMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DetachmentMethods_tB1BEBFED5C211907CA24D7D7A54098431911C245, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiController/UpdateModes
struct UpdateModes_t49B91119A738485082808C58875A22F0ADFC1463 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.CorgiController/UpdateModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateModes_t49B91119A738485082808C58875A22F0ADFC1463, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.LensSettings/OverrideModes
struct OverrideModes_t9F50F7DC95547FC2670F467631715B79E53095C4 
{
public:
	// System.Int32 Cinemachine.LensSettings/OverrideModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OverrideModes_t9F50F7DC95547FC2670F467631715B79E53095C4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.LevelManager/BoundsModes
struct BoundsModes_t1D11BB4CB88820B8543DDFFC0488700BBF914B52 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.LevelManager/BoundsModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BoundsModes_t1D11BB4CB88820B8543DDFFC0488700BBF914B52, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.LevelManager/CheckpointDirections
struct CheckpointDirections_t669AD9746BA4D6BC94AEA84726131FD24875AD40 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.LevelManager/CheckpointDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckpointDirections_t669AD9746BA4D6BC94AEA84726131FD24875AD40, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.LevelManager/CheckpointsAxis
struct CheckpointsAxis_t85B8EB1F27266F18E0BFFD55D1B1C67B49D44464 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.LevelManager/CheckpointsAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CheckpointsAxis_t85B8EB1F27266F18E0BFFD55D1B1C67B49D44464, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMCinemachineZone/Modes
struct Modes_tE861C1573D431B7678430C10D633F29BA5A2E4AF 
{
public:
	// System.Int32 MoreMountains.Tools.MMCinemachineZone/Modes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Modes_tE861C1573D431B7678430C10D633F29BA5A2E4AF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks/Directions
struct Directions_t6734A23FBABD110399D0F300811824A2515BC3EB 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbacks/Directions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Directions_t6734A23FBABD110399D0F300811824A2515BC3EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks/InitializationModes
struct InitializationModes_t2C4B197E025D0230877655D43D5636300E479888 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbacks/InitializationModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitializationModes_t2C4B197E025D0230877655D43D5636300E479888, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks/SafeModes
struct SafeModes_t27DE69744FFB89E58A263B7318C1A1DA32EBE30E 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbacks/SafeModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SafeModes_t27DE69744FFB89E58A263B7318C1A1DA32EBE30E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMLoadScene/LoadingSceneModes
struct LoadingSceneModes_tE9CEF7BA6532CAAD9216A634C1862F6B5494C77C 
{
public:
	// System.Int32 MoreMountains.Tools.MMLoadScene/LoadingSceneModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoadingSceneModes_tE9CEF7BA6532CAAD9216A634C1862F6B5494C77C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMSpriteMaskEvent/MMSpriteMaskEventTypes
struct MMSpriteMaskEventTypes_t4E9A67C82C1935768513EB8B917817050D9F7A4F 
{
public:
	// System.Int32 MoreMountains.Tools.MMSpriteMaskEvent/MMSpriteMaskEventTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMSpriteMaskEventTypes_t4E9A67C82C1935768513EB8B917817050D9F7A4F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMTween/MMTweenCurve
struct MMTweenCurve_tC880A885C5B5976B67879343E813427B2CF5DD24 
{
public:
	// System.Int32 MoreMountains.Tools.MMTween/MMTweenCurve::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMTweenCurve_tC880A885C5B5976B67879343E813427B2CF5DD24, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52
struct U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B  : public RuntimeObject
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::collider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider_2;
	// MoreMountains.CorgiEngine.Teleporter MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::<>4__this
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * ___U3CU3E4__this_3;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::origin
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin_4;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::destination
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___destination_5;
	// System.Single MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::<startedAt>5__2
	float ___U3CstartedAtU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_collider_2() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___collider_2)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_collider_2() const { return ___collider_2; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_collider_2() { return &___collider_2; }
	inline void set_collider_2(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___collider_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___collider_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___U3CU3E4__this_3)); }
	inline Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_3), (void*)value);
	}

	inline static int32_t get_offset_of_origin_4() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___origin_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_origin_4() const { return ___origin_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_origin_4() { return &___origin_4; }
	inline void set_origin_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___origin_4 = value;
	}

	inline static int32_t get_offset_of_destination_5() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___destination_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_destination_5() const { return ___destination_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_destination_5() { return &___destination_5; }
	inline void set_destination_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___destination_5 = value;
	}

	inline static int32_t get_offset_of_U3CstartedAtU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B, ___U3CstartedAtU3E5__2_6)); }
	inline float get_U3CstartedAtU3E5__2_6() const { return ___U3CstartedAtU3E5__2_6; }
	inline float* get_address_of_U3CstartedAtU3E5__2_6() { return &___U3CstartedAtU3E5__2_6; }
	inline void set_U3CstartedAtU3E5__2_6(float value)
	{
		___U3CstartedAtU3E5__2_6 = value;
	}
};


// MoreMountains.CorgiEngine.Teleporter/CameraModes
struct CameraModes_tC83894721F496667721C2E6C2B126FAA362A840B 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Teleporter/CameraModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CameraModes_tC83894721F496667721C2E6C2B126FAA362A840B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.Teleporter/TeleportationModes
struct TeleportationModes_t4A48F6444DAEC6197DB88983E75B1A530FA4BF7C 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.Teleporter/TeleportationModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TeleportationModes_t4A48F6444DAEC6197DB88983E75B1A530FA4BF7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Cinemachine.CinemachineBlendDefinition
struct CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE 
{
public:
	// Cinemachine.CinemachineBlendDefinition/Style Cinemachine.CinemachineBlendDefinition::m_Style
	int32_t ___m_Style_0;
	// System.Single Cinemachine.CinemachineBlendDefinition::m_Time
	float ___m_Time_1;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineBlendDefinition::m_CustomCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___m_CustomCurve_2;

public:
	inline static int32_t get_offset_of_m_Style_0() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE, ___m_Style_0)); }
	inline int32_t get_m_Style_0() const { return ___m_Style_0; }
	inline int32_t* get_address_of_m_Style_0() { return &___m_Style_0; }
	inline void set_m_Style_0(int32_t value)
	{
		___m_Style_0 = value;
	}

	inline static int32_t get_offset_of_m_Time_1() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE, ___m_Time_1)); }
	inline float get_m_Time_1() const { return ___m_Time_1; }
	inline float* get_address_of_m_Time_1() { return &___m_Time_1; }
	inline void set_m_Time_1(float value)
	{
		___m_Time_1 = value;
	}

	inline static int32_t get_offset_of_m_CustomCurve_2() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE, ___m_CustomCurve_2)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_m_CustomCurve_2() const { return ___m_CustomCurve_2; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_m_CustomCurve_2() { return &___m_CustomCurve_2; }
	inline void set_m_CustomCurve_2(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___m_CustomCurve_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomCurve_2), (void*)value);
	}
};

struct CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE_StaticFields
{
public:
	// UnityEngine.AnimationCurve[] Cinemachine.CinemachineBlendDefinition::sStandardCurves
	AnimationCurveU5BU5D_tE3C6891AFD2EF0188200F790D3120A09202E544A* ___sStandardCurves_3;

public:
	inline static int32_t get_offset_of_sStandardCurves_3() { return static_cast<int32_t>(offsetof(CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE_StaticFields, ___sStandardCurves_3)); }
	inline AnimationCurveU5BU5D_tE3C6891AFD2EF0188200F790D3120A09202E544A* get_sStandardCurves_3() const { return ___sStandardCurves_3; }
	inline AnimationCurveU5BU5D_tE3C6891AFD2EF0188200F790D3120A09202E544A** get_address_of_sStandardCurves_3() { return &___sStandardCurves_3; }
	inline void set_sStandardCurves_3(AnimationCurveU5BU5D_tE3C6891AFD2EF0188200F790D3120A09202E544A* value)
	{
		___sStandardCurves_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sStandardCurves_3), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// MoreMountains.CorgiEngine.CorgiEngineEvent
struct CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B 
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineEventTypes MoreMountains.CorgiEngine.CorgiEngineEvent::EventType
	int32_t ___EventType_0;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}
};

struct CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_StaticFields
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineEvent MoreMountains.CorgiEngine.CorgiEngineEvent::e
	CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  ___e_1;

public:
	inline static int32_t get_offset_of_e_1() { return static_cast<int32_t>(offsetof(CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_StaticFields, ___e_1)); }
	inline CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  get_e_1() const { return ___e_1; }
	inline CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B * get_address_of_e_1() { return &___e_1; }
	inline void set_e_1(CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  value)
	{
		___e_1 = value;
	}
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// Cinemachine.LensSettings
struct LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA 
{
public:
	// System.Single Cinemachine.LensSettings::FieldOfView
	float ___FieldOfView_1;
	// System.Single Cinemachine.LensSettings::OrthographicSize
	float ___OrthographicSize_2;
	// System.Single Cinemachine.LensSettings::NearClipPlane
	float ___NearClipPlane_3;
	// System.Single Cinemachine.LensSettings::FarClipPlane
	float ___FarClipPlane_4;
	// System.Single Cinemachine.LensSettings::Dutch
	float ___Dutch_5;
	// Cinemachine.LensSettings/OverrideModes Cinemachine.LensSettings::ModeOverride
	int32_t ___ModeOverride_6;
	// UnityEngine.Vector2 Cinemachine.LensSettings::LensShift
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___LensShift_7;
	// UnityEngine.Camera/GateFitMode Cinemachine.LensSettings::GateFit
	int32_t ___GateFit_8;
	// UnityEngine.Vector2 Cinemachine.LensSettings::m_SensorSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_SensorSize_9;
	// System.Boolean Cinemachine.LensSettings::m_OrthoFromCamera
	bool ___m_OrthoFromCamera_10;
	// System.Boolean Cinemachine.LensSettings::m_PhysicalFromCamera
	bool ___m_PhysicalFromCamera_11;

public:
	inline static int32_t get_offset_of_FieldOfView_1() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___FieldOfView_1)); }
	inline float get_FieldOfView_1() const { return ___FieldOfView_1; }
	inline float* get_address_of_FieldOfView_1() { return &___FieldOfView_1; }
	inline void set_FieldOfView_1(float value)
	{
		___FieldOfView_1 = value;
	}

	inline static int32_t get_offset_of_OrthographicSize_2() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___OrthographicSize_2)); }
	inline float get_OrthographicSize_2() const { return ___OrthographicSize_2; }
	inline float* get_address_of_OrthographicSize_2() { return &___OrthographicSize_2; }
	inline void set_OrthographicSize_2(float value)
	{
		___OrthographicSize_2 = value;
	}

	inline static int32_t get_offset_of_NearClipPlane_3() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___NearClipPlane_3)); }
	inline float get_NearClipPlane_3() const { return ___NearClipPlane_3; }
	inline float* get_address_of_NearClipPlane_3() { return &___NearClipPlane_3; }
	inline void set_NearClipPlane_3(float value)
	{
		___NearClipPlane_3 = value;
	}

	inline static int32_t get_offset_of_FarClipPlane_4() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___FarClipPlane_4)); }
	inline float get_FarClipPlane_4() const { return ___FarClipPlane_4; }
	inline float* get_address_of_FarClipPlane_4() { return &___FarClipPlane_4; }
	inline void set_FarClipPlane_4(float value)
	{
		___FarClipPlane_4 = value;
	}

	inline static int32_t get_offset_of_Dutch_5() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___Dutch_5)); }
	inline float get_Dutch_5() const { return ___Dutch_5; }
	inline float* get_address_of_Dutch_5() { return &___Dutch_5; }
	inline void set_Dutch_5(float value)
	{
		___Dutch_5 = value;
	}

	inline static int32_t get_offset_of_ModeOverride_6() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___ModeOverride_6)); }
	inline int32_t get_ModeOverride_6() const { return ___ModeOverride_6; }
	inline int32_t* get_address_of_ModeOverride_6() { return &___ModeOverride_6; }
	inline void set_ModeOverride_6(int32_t value)
	{
		___ModeOverride_6 = value;
	}

	inline static int32_t get_offset_of_LensShift_7() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___LensShift_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_LensShift_7() const { return ___LensShift_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_LensShift_7() { return &___LensShift_7; }
	inline void set_LensShift_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___LensShift_7 = value;
	}

	inline static int32_t get_offset_of_GateFit_8() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___GateFit_8)); }
	inline int32_t get_GateFit_8() const { return ___GateFit_8; }
	inline int32_t* get_address_of_GateFit_8() { return &___GateFit_8; }
	inline void set_GateFit_8(int32_t value)
	{
		___GateFit_8 = value;
	}

	inline static int32_t get_offset_of_m_SensorSize_9() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___m_SensorSize_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_SensorSize_9() const { return ___m_SensorSize_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_SensorSize_9() { return &___m_SensorSize_9; }
	inline void set_m_SensorSize_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_SensorSize_9 = value;
	}

	inline static int32_t get_offset_of_m_OrthoFromCamera_10() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___m_OrthoFromCamera_10)); }
	inline bool get_m_OrthoFromCamera_10() const { return ___m_OrthoFromCamera_10; }
	inline bool* get_address_of_m_OrthoFromCamera_10() { return &___m_OrthoFromCamera_10; }
	inline void set_m_OrthoFromCamera_10(bool value)
	{
		___m_OrthoFromCamera_10 = value;
	}

	inline static int32_t get_offset_of_m_PhysicalFromCamera_11() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA, ___m_PhysicalFromCamera_11)); }
	inline bool get_m_PhysicalFromCamera_11() const { return ___m_PhysicalFromCamera_11; }
	inline bool* get_address_of_m_PhysicalFromCamera_11() { return &___m_PhysicalFromCamera_11; }
	inline void set_m_PhysicalFromCamera_11(bool value)
	{
		___m_PhysicalFromCamera_11 = value;
	}
};

struct LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA_StaticFields
{
public:
	// Cinemachine.LensSettings Cinemachine.LensSettings::Default
	LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  ___Default_0;

public:
	inline static int32_t get_offset_of_Default_0() { return static_cast<int32_t>(offsetof(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA_StaticFields, ___Default_0)); }
	inline LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  get_Default_0() const { return ___Default_0; }
	inline LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * get_address_of_Default_0() { return &___Default_0; }
	inline void set_Default_0(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  value)
	{
		___Default_0 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.LensSettings
struct LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA_marshaled_pinvoke
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___ModeOverride_6;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___LensShift_7;
	int32_t ___GateFit_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_SensorSize_9;
	int32_t ___m_OrthoFromCamera_10;
	int32_t ___m_PhysicalFromCamera_11;
};
// Native definition for COM marshalling of Cinemachine.LensSettings
struct LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA_marshaled_com
{
	float ___FieldOfView_1;
	float ___OrthographicSize_2;
	float ___NearClipPlane_3;
	float ___FarClipPlane_4;
	float ___Dutch_5;
	int32_t ___ModeOverride_6;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___LensShift_7;
	int32_t ___GateFit_8;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_SensorSize_9;
	int32_t ___m_OrthoFromCamera_10;
	int32_t ___m_PhysicalFromCamera_11;
};

// MoreMountains.CorgiEngine.MMCameraEvent
struct MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3 
{
public:
	// MoreMountains.CorgiEngine.MMCameraEventTypes MoreMountains.CorgiEngine.MMCameraEvent::EventType
	int32_t ___EventType_0;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.MMCameraEvent::TargetCharacter
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___TargetCharacter_1;
	// UnityEngine.Collider MoreMountains.CorgiEngine.MMCameraEvent::Bounds
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___Bounds_2;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.MMCameraEvent::Bounds2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___Bounds2D_3;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_TargetCharacter_1() { return static_cast<int32_t>(offsetof(MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3, ___TargetCharacter_1)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get_TargetCharacter_1() const { return ___TargetCharacter_1; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of_TargetCharacter_1() { return &___TargetCharacter_1; }
	inline void set_TargetCharacter_1(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		___TargetCharacter_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetCharacter_1), (void*)value);
	}

	inline static int32_t get_offset_of_Bounds_2() { return static_cast<int32_t>(offsetof(MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3, ___Bounds_2)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_Bounds_2() const { return ___Bounds_2; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_Bounds_2() { return &___Bounds_2; }
	inline void set_Bounds_2(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___Bounds_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bounds_2), (void*)value);
	}

	inline static int32_t get_offset_of_Bounds2D_3() { return static_cast<int32_t>(offsetof(MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3, ___Bounds2D_3)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_Bounds2D_3() const { return ___Bounds2D_3; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_Bounds2D_3() { return &___Bounds2D_3; }
	inline void set_Bounds2D_3(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___Bounds2D_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bounds2D_3), (void*)value);
	}
};

struct MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_StaticFields
{
public:
	// MoreMountains.CorgiEngine.MMCameraEvent MoreMountains.CorgiEngine.MMCameraEvent::e
	MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  ___e_4;

public:
	inline static int32_t get_offset_of_e_4() { return static_cast<int32_t>(offsetof(MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_StaticFields, ___e_4)); }
	inline MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  get_e_4() const { return ___e_4; }
	inline MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3 * get_address_of_e_4() { return &___e_4; }
	inline void set_e_4(MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  value)
	{
		___e_4 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_4))->___TargetCharacter_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_4))->___Bounds_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_4))->___Bounds2D_3), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of MoreMountains.CorgiEngine.MMCameraEvent
struct MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_marshaled_pinvoke
{
	int32_t ___EventType_0;
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___TargetCharacter_1;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___Bounds_2;
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___Bounds2D_3;
};
// Native definition for COM marshalling of MoreMountains.CorgiEngine.MMCameraEvent
struct MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_marshaled_com
{
	int32_t ___EventType_0;
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___TargetCharacter_1;
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___Bounds_2;
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___Bounds2D_3;
};

// MoreMountains.CorgiEngine.MMCinemachineBrainEvent
struct MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF 
{
public:
	// MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes MoreMountains.CorgiEngine.MMCinemachineBrainEvent::EventType
	int32_t ___EventType_0;
	// System.Single MoreMountains.CorgiEngine.MMCinemachineBrainEvent::Duration
	float ___Duration_1;

public:
	inline static int32_t get_offset_of_EventType_0() { return static_cast<int32_t>(offsetof(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF, ___EventType_0)); }
	inline int32_t get_EventType_0() const { return ___EventType_0; }
	inline int32_t* get_address_of_EventType_0() { return &___EventType_0; }
	inline void set_EventType_0(int32_t value)
	{
		___EventType_0 = value;
	}

	inline static int32_t get_offset_of_Duration_1() { return static_cast<int32_t>(offsetof(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF, ___Duration_1)); }
	inline float get_Duration_1() const { return ___Duration_1; }
	inline float* get_address_of_Duration_1() { return &___Duration_1; }
	inline void set_Duration_1(float value)
	{
		___Duration_1 = value;
	}
};

struct MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_StaticFields
{
public:
	// MoreMountains.CorgiEngine.MMCinemachineBrainEvent MoreMountains.CorgiEngine.MMCinemachineBrainEvent::e
	MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  ___e_2;

public:
	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_StaticFields, ___e_2)); }
	inline MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  get_e_2() const { return ___e_2; }
	inline MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF * get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  value)
	{
		___e_2 = value;
	}
};


// MoreMountains.Tools.MMTweenType
struct MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175  : public RuntimeObject
{
public:
	// MoreMountains.Tools.MMTweenDefinitionTypes MoreMountains.Tools.MMTweenType::MMTweenDefinitionType
	int32_t ___MMTweenDefinitionType_0;
	// MoreMountains.Tools.MMTween/MMTweenCurve MoreMountains.Tools.MMTweenType::MMTweenCurve
	int32_t ___MMTweenCurve_1;
	// UnityEngine.AnimationCurve MoreMountains.Tools.MMTweenType::Curve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___Curve_2;

public:
	inline static int32_t get_offset_of_MMTweenDefinitionType_0() { return static_cast<int32_t>(offsetof(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175, ___MMTweenDefinitionType_0)); }
	inline int32_t get_MMTweenDefinitionType_0() const { return ___MMTweenDefinitionType_0; }
	inline int32_t* get_address_of_MMTweenDefinitionType_0() { return &___MMTweenDefinitionType_0; }
	inline void set_MMTweenDefinitionType_0(int32_t value)
	{
		___MMTweenDefinitionType_0 = value;
	}

	inline static int32_t get_offset_of_MMTweenCurve_1() { return static_cast<int32_t>(offsetof(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175, ___MMTweenCurve_1)); }
	inline int32_t get_MMTweenCurve_1() const { return ___MMTweenCurve_1; }
	inline int32_t* get_address_of_MMTweenCurve_1() { return &___MMTweenCurve_1; }
	inline void set_MMTweenCurve_1(int32_t value)
	{
		___MMTweenCurve_1 = value;
	}

	inline static int32_t get_offset_of_Curve_2() { return static_cast<int32_t>(offsetof(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175, ___Curve_2)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_Curve_2() const { return ___Curve_2; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_Curve_2() { return &___Curve_2; }
	inline void set_Curve_2(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___Curve_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Curve_2), (void*)value);
	}
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C 
{
public:
	// Cinemachine.CinemachineVirtualCameraBase/BlendHint Cinemachine.CinemachineVirtualCameraBase/TransitionParams::m_BlendHint
	int32_t ___m_BlendHint_0;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase/TransitionParams::m_InheritPosition
	bool ___m_InheritPosition_1;
	// Cinemachine.CinemachineBrain/VcamActivatedEvent Cinemachine.CinemachineVirtualCameraBase/TransitionParams::m_OnCameraLive
	VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * ___m_OnCameraLive_2;

public:
	inline static int32_t get_offset_of_m_BlendHint_0() { return static_cast<int32_t>(offsetof(TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C, ___m_BlendHint_0)); }
	inline int32_t get_m_BlendHint_0() const { return ___m_BlendHint_0; }
	inline int32_t* get_address_of_m_BlendHint_0() { return &___m_BlendHint_0; }
	inline void set_m_BlendHint_0(int32_t value)
	{
		___m_BlendHint_0 = value;
	}

	inline static int32_t get_offset_of_m_InheritPosition_1() { return static_cast<int32_t>(offsetof(TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C, ___m_InheritPosition_1)); }
	inline bool get_m_InheritPosition_1() const { return ___m_InheritPosition_1; }
	inline bool* get_address_of_m_InheritPosition_1() { return &___m_InheritPosition_1; }
	inline void set_m_InheritPosition_1(bool value)
	{
		___m_InheritPosition_1 = value;
	}

	inline static int32_t get_offset_of_m_OnCameraLive_2() { return static_cast<int32_t>(offsetof(TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C, ___m_OnCameraLive_2)); }
	inline VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * get_m_OnCameraLive_2() const { return ___m_OnCameraLive_2; }
	inline VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 ** get_address_of_m_OnCameraLive_2() { return &___m_OnCameraLive_2; }
	inline void set_m_OnCameraLive_2(VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * value)
	{
		___m_OnCameraLive_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCameraLive_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C_marshaled_pinvoke
{
	int32_t ___m_BlendHint_0;
	int32_t ___m_InheritPosition_1;
	VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * ___m_OnCameraLive_2;
};
// Native definition for COM marshalling of Cinemachine.CinemachineVirtualCameraBase/TransitionParams
struct TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C_marshaled_com
{
	int32_t ___m_BlendHint_0;
	int32_t ___m_InheritPosition_1;
	VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * ___m_OnCameraLive_2;
};

// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// Cinemachine.CameraState
struct CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 
{
public:
	// Cinemachine.LensSettings Cinemachine.CameraState::<Lens>k__BackingField
	LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  ___U3CLensU3Ek__BackingField_0;
	// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceUp>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CReferenceUpU3Ek__BackingField_1;
	// UnityEngine.Vector3 Cinemachine.CameraState::<ReferenceLookAt>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CReferenceLookAtU3Ek__BackingField_2;
	// UnityEngine.Vector3 Cinemachine.CameraState::<RawPosition>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CRawPositionU3Ek__BackingField_4;
	// UnityEngine.Quaternion Cinemachine.CameraState::<RawOrientation>k__BackingField
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3CRawOrientationU3Ek__BackingField_5;
	// UnityEngine.Vector3 Cinemachine.CameraState::<PositionDampingBypass>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	// System.Single Cinemachine.CameraState::<ShotQuality>k__BackingField
	float ___U3CShotQualityU3Ek__BackingField_7;
	// UnityEngine.Vector3 Cinemachine.CameraState::<PositionCorrection>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionCorrectionU3Ek__BackingField_8;
	// UnityEngine.Quaternion Cinemachine.CameraState::<OrientationCorrection>k__BackingField
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3COrientationCorrectionU3Ek__BackingField_9;
	// Cinemachine.CameraState/BlendHintValue Cinemachine.CameraState::<BlendHint>k__BackingField
	int32_t ___U3CBlendHintU3Ek__BackingField_10;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom0
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom0_11;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom1
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom1_12;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom2
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom2_13;
	// Cinemachine.CameraState/CustomBlendable Cinemachine.CameraState::mCustom3
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom3_14;
	// System.Collections.Generic.List`1<Cinemachine.CameraState/CustomBlendable> Cinemachine.CameraState::m_CustomOverflow
	List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009 * ___m_CustomOverflow_15;
	// System.Int32 Cinemachine.CameraState::<NumCustomBlendables>k__BackingField
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CLensU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CLensU3Ek__BackingField_0)); }
	inline LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  get_U3CLensU3Ek__BackingField_0() const { return ___U3CLensU3Ek__BackingField_0; }
	inline LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * get_address_of_U3CLensU3Ek__BackingField_0() { return &___U3CLensU3Ek__BackingField_0; }
	inline void set_U3CLensU3Ek__BackingField_0(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  value)
	{
		___U3CLensU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceUpU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CReferenceUpU3Ek__BackingField_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CReferenceUpU3Ek__BackingField_1() const { return ___U3CReferenceUpU3Ek__BackingField_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CReferenceUpU3Ek__BackingField_1() { return &___U3CReferenceUpU3Ek__BackingField_1; }
	inline void set_U3CReferenceUpU3Ek__BackingField_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CReferenceUpU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLookAtU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CReferenceLookAtU3Ek__BackingField_2)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CReferenceLookAtU3Ek__BackingField_2() const { return ___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CReferenceLookAtU3Ek__BackingField_2() { return &___U3CReferenceLookAtU3Ek__BackingField_2; }
	inline void set_U3CReferenceLookAtU3Ek__BackingField_2(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CReferenceLookAtU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRawPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CRawPositionU3Ek__BackingField_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CRawPositionU3Ek__BackingField_4() const { return ___U3CRawPositionU3Ek__BackingField_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CRawPositionU3Ek__BackingField_4() { return &___U3CRawPositionU3Ek__BackingField_4; }
	inline void set_U3CRawPositionU3Ek__BackingField_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CRawPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRawOrientationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CRawOrientationU3Ek__BackingField_5)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_U3CRawOrientationU3Ek__BackingField_5() const { return ___U3CRawOrientationU3Ek__BackingField_5; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_U3CRawOrientationU3Ek__BackingField_5() { return &___U3CRawOrientationU3Ek__BackingField_5; }
	inline void set_U3CRawOrientationU3Ek__BackingField_5(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___U3CRawOrientationU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CPositionDampingBypassU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CPositionDampingBypassU3Ek__BackingField_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CPositionDampingBypassU3Ek__BackingField_6() const { return ___U3CPositionDampingBypassU3Ek__BackingField_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CPositionDampingBypassU3Ek__BackingField_6() { return &___U3CPositionDampingBypassU3Ek__BackingField_6; }
	inline void set_U3CPositionDampingBypassU3Ek__BackingField_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CPositionDampingBypassU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CShotQualityU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CShotQualityU3Ek__BackingField_7)); }
	inline float get_U3CShotQualityU3Ek__BackingField_7() const { return ___U3CShotQualityU3Ek__BackingField_7; }
	inline float* get_address_of_U3CShotQualityU3Ek__BackingField_7() { return &___U3CShotQualityU3Ek__BackingField_7; }
	inline void set_U3CShotQualityU3Ek__BackingField_7(float value)
	{
		___U3CShotQualityU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CPositionCorrectionU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CPositionCorrectionU3Ek__BackingField_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CPositionCorrectionU3Ek__BackingField_8() const { return ___U3CPositionCorrectionU3Ek__BackingField_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CPositionCorrectionU3Ek__BackingField_8() { return &___U3CPositionCorrectionU3Ek__BackingField_8; }
	inline void set_U3CPositionCorrectionU3Ek__BackingField_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CPositionCorrectionU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3COrientationCorrectionU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3COrientationCorrectionU3Ek__BackingField_9)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_U3COrientationCorrectionU3Ek__BackingField_9() const { return ___U3COrientationCorrectionU3Ek__BackingField_9; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_U3COrientationCorrectionU3Ek__BackingField_9() { return &___U3COrientationCorrectionU3Ek__BackingField_9; }
	inline void set_U3COrientationCorrectionU3Ek__BackingField_9(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___U3COrientationCorrectionU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CBlendHintU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CBlendHintU3Ek__BackingField_10)); }
	inline int32_t get_U3CBlendHintU3Ek__BackingField_10() const { return ___U3CBlendHintU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CBlendHintU3Ek__BackingField_10() { return &___U3CBlendHintU3Ek__BackingField_10; }
	inline void set_U3CBlendHintU3Ek__BackingField_10(int32_t value)
	{
		___U3CBlendHintU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_mCustom0_11() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___mCustom0_11)); }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  get_mCustom0_11() const { return ___mCustom0_11; }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B * get_address_of_mCustom0_11() { return &___mCustom0_11; }
	inline void set_mCustom0_11(CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  value)
	{
		___mCustom0_11 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom0_11))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_mCustom1_12() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___mCustom1_12)); }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  get_mCustom1_12() const { return ___mCustom1_12; }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B * get_address_of_mCustom1_12() { return &___mCustom1_12; }
	inline void set_mCustom1_12(CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  value)
	{
		___mCustom1_12 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom1_12))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_mCustom2_13() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___mCustom2_13)); }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  get_mCustom2_13() const { return ___mCustom2_13; }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B * get_address_of_mCustom2_13() { return &___mCustom2_13; }
	inline void set_mCustom2_13(CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  value)
	{
		___mCustom2_13 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom2_13))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_mCustom3_14() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___mCustom3_14)); }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  get_mCustom3_14() const { return ___mCustom3_14; }
	inline CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B * get_address_of_mCustom3_14() { return &___mCustom3_14; }
	inline void set_mCustom3_14(CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  value)
	{
		___mCustom3_14 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___mCustom3_14))->___m_Custom_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_CustomOverflow_15() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___m_CustomOverflow_15)); }
	inline List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009 * get_m_CustomOverflow_15() const { return ___m_CustomOverflow_15; }
	inline List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009 ** get_address_of_m_CustomOverflow_15() { return &___m_CustomOverflow_15; }
	inline void set_m_CustomOverflow_15(List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009 * value)
	{
		___m_CustomOverflow_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomOverflow_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CNumCustomBlendablesU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601, ___U3CNumCustomBlendablesU3Ek__BackingField_16)); }
	inline int32_t get_U3CNumCustomBlendablesU3Ek__BackingField_16() const { return ___U3CNumCustomBlendablesU3Ek__BackingField_16; }
	inline int32_t* get_address_of_U3CNumCustomBlendablesU3Ek__BackingField_16() { return &___U3CNumCustomBlendablesU3Ek__BackingField_16; }
	inline void set_U3CNumCustomBlendablesU3Ek__BackingField_16(int32_t value)
	{
		___U3CNumCustomBlendablesU3Ek__BackingField_16 = value;
	}
};

struct CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601_StaticFields
{
public:
	// UnityEngine.Vector3 Cinemachine.CameraState::kNoPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___kNoPoint_3;

public:
	inline static int32_t get_offset_of_kNoPoint_3() { return static_cast<int32_t>(offsetof(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601_StaticFields, ___kNoPoint_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_kNoPoint_3() const { return ___kNoPoint_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_kNoPoint_3() { return &___kNoPoint_3; }
	inline void set_kNoPoint_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___kNoPoint_3 = value;
	}
};

// Native definition for P/Invoke marshalling of Cinemachine.CameraState
struct CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601_marshaled_pinvoke
{
	LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA_marshaled_pinvoke ___U3CLensU3Ek__BackingField_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CReferenceUpU3Ek__BackingField_1;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CReferenceLookAtU3Ek__BackingField_2;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CRawPositionU3Ek__BackingField_4;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3CRawOrientationU3Ek__BackingField_5;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	float ___U3CShotQualityU3Ek__BackingField_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionCorrectionU3Ek__BackingField_8;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3COrientationCorrectionU3Ek__BackingField_9;
	int32_t ___U3CBlendHintU3Ek__BackingField_10;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom0_11;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom1_12;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom2_13;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom3_14;
	List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009 * ___m_CustomOverflow_15;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_16;
};
// Native definition for COM marshalling of Cinemachine.CameraState
struct CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601_marshaled_com
{
	LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA_marshaled_com ___U3CLensU3Ek__BackingField_0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CReferenceUpU3Ek__BackingField_1;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CReferenceLookAtU3Ek__BackingField_2;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CRawPositionU3Ek__BackingField_4;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3CRawOrientationU3Ek__BackingField_5;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionDampingBypassU3Ek__BackingField_6;
	float ___U3CShotQualityU3Ek__BackingField_7;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CPositionCorrectionU3Ek__BackingField_8;
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___U3COrientationCorrectionU3Ek__BackingField_9;
	int32_t ___U3CBlendHintU3Ek__BackingField_10;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom0_11;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom1_12;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom2_13;
	CustomBlendable_t8E179439D0939CA1F23354DD785283354767EA6B  ___mCustom3_14;
	List_1_tA61023C4AB12456AD9E3F6DFCAEC43B9536C6009 * ___m_CustomOverflow_15;
	int32_t ___U3CNumCustomBlendablesU3Ek__BackingField_16;
};

// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.LevelManager>
struct MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB_StaticFields, ____instance_4)); }
	inline LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * get__instance_4() const { return ____instance_4; }
	inline LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// UnityEngine.BoxCollider2D
struct BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9  : public Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722
{
public:

public:
};


// MoreMountains.CorgiEngine.CameraController
struct CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MoreMountains.CorgiEngine.CameraController::<FollowsPlayer>k__BackingField
	bool ___U3CFollowsPlayerU3Ek__BackingField_4;
	// System.Single MoreMountains.CorgiEngine.CameraController::HorizontalLookDistance
	float ___HorizontalLookDistance_5;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CameraController::CameraOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___CameraOffset_6;
	// System.Single MoreMountains.CorgiEngine.CameraController::LookAheadTrigger
	float ___LookAheadTrigger_7;
	// System.Single MoreMountains.CorgiEngine.CameraController::ManualUpDownLookDistance
	float ___ManualUpDownLookDistance_8;
	// System.Single MoreMountains.CorgiEngine.CameraController::ResetSpeed
	float ___ResetSpeed_9;
	// System.Single MoreMountains.CorgiEngine.CameraController::CameraSpeed
	float ___CameraSpeed_10;
	// System.Single MoreMountains.CorgiEngine.CameraController::MinimumZoom
	float ___MinimumZoom_11;
	// System.Single MoreMountains.CorgiEngine.CameraController::MaximumZoom
	float ___MaximumZoom_12;
	// System.Single MoreMountains.CorgiEngine.CameraController::ZoomSpeed
	float ___ZoomSpeed_13;
	// System.Boolean MoreMountains.CorgiEngine.CameraController::PixelPerfect
	bool ___PixelPerfect_14;
	// System.Int32 MoreMountains.CorgiEngine.CameraController::ReferenceVerticalResolution
	int32_t ___ReferenceVerticalResolution_15;
	// System.Single MoreMountains.CorgiEngine.CameraController::ReferencePixelsPerUnit
	float ___ReferencePixelsPerUnit_16;
	// System.Boolean MoreMountains.CorgiEngine.CameraController::EnableEffectsOnMobile
	bool ___EnableEffectsOnMobile_17;
	// System.Boolean MoreMountains.CorgiEngine.CameraController::InstantRepositionCameraOnRespawn
	bool ___InstantRepositionCameraOnRespawn_18;
	// System.Boolean MoreMountains.CorgiEngine.CameraController::AssignCameraOnSpawn
	bool ___AssignCameraOnSpawn_19;
	// UnityEngine.Transform MoreMountains.CorgiEngine.CameraController::Target
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___Target_20;
	// MoreMountains.CorgiEngine.CorgiController MoreMountains.CorgiEngine.CameraController::TargetController
	CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * ___TargetController_21;
	// System.Boolean MoreMountains.CorgiEngine.CameraController::StartFollowingBtn
	bool ___StartFollowingBtn_22;
	// System.Boolean MoreMountains.CorgiEngine.CameraController::StopFollowingBtn
	bool ___StopFollowingBtn_23;
	// UnityEngine.Bounds MoreMountains.CorgiEngine.CameraController::_levelBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ____levelBounds_24;
	// System.Single MoreMountains.CorgiEngine.CameraController::_xMin
	float ____xMin_25;
	// System.Single MoreMountains.CorgiEngine.CameraController::_xMax
	float ____xMax_26;
	// System.Single MoreMountains.CorgiEngine.CameraController::_yMin
	float ____yMin_27;
	// System.Single MoreMountains.CorgiEngine.CameraController::_yMax
	float ____yMax_28;
	// System.Single MoreMountains.CorgiEngine.CameraController::_offsetZ
	float ____offsetZ_29;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CameraController::_lastTargetPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____lastTargetPosition_30;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CameraController::_currentVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____currentVelocity_31;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CameraController::_lookAheadPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____lookAheadPos_32;
	// System.Single MoreMountains.CorgiEngine.CameraController::_shakeIntensity
	float ____shakeIntensity_33;
	// System.Single MoreMountains.CorgiEngine.CameraController::_shakeDecay
	float ____shakeDecay_34;
	// System.Single MoreMountains.CorgiEngine.CameraController::_shakeDuration
	float ____shakeDuration_35;
	// System.Single MoreMountains.CorgiEngine.CameraController::_currentZoom
	float ____currentZoom_36;
	// UnityEngine.Camera MoreMountains.CorgiEngine.CameraController::_camera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ____camera_37;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CameraController::_lookDirectionModifier
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____lookDirectionModifier_38;

public:
	inline static int32_t get_offset_of_U3CFollowsPlayerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___U3CFollowsPlayerU3Ek__BackingField_4)); }
	inline bool get_U3CFollowsPlayerU3Ek__BackingField_4() const { return ___U3CFollowsPlayerU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CFollowsPlayerU3Ek__BackingField_4() { return &___U3CFollowsPlayerU3Ek__BackingField_4; }
	inline void set_U3CFollowsPlayerU3Ek__BackingField_4(bool value)
	{
		___U3CFollowsPlayerU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_HorizontalLookDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___HorizontalLookDistance_5)); }
	inline float get_HorizontalLookDistance_5() const { return ___HorizontalLookDistance_5; }
	inline float* get_address_of_HorizontalLookDistance_5() { return &___HorizontalLookDistance_5; }
	inline void set_HorizontalLookDistance_5(float value)
	{
		___HorizontalLookDistance_5 = value;
	}

	inline static int32_t get_offset_of_CameraOffset_6() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___CameraOffset_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_CameraOffset_6() const { return ___CameraOffset_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_CameraOffset_6() { return &___CameraOffset_6; }
	inline void set_CameraOffset_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___CameraOffset_6 = value;
	}

	inline static int32_t get_offset_of_LookAheadTrigger_7() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___LookAheadTrigger_7)); }
	inline float get_LookAheadTrigger_7() const { return ___LookAheadTrigger_7; }
	inline float* get_address_of_LookAheadTrigger_7() { return &___LookAheadTrigger_7; }
	inline void set_LookAheadTrigger_7(float value)
	{
		___LookAheadTrigger_7 = value;
	}

	inline static int32_t get_offset_of_ManualUpDownLookDistance_8() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___ManualUpDownLookDistance_8)); }
	inline float get_ManualUpDownLookDistance_8() const { return ___ManualUpDownLookDistance_8; }
	inline float* get_address_of_ManualUpDownLookDistance_8() { return &___ManualUpDownLookDistance_8; }
	inline void set_ManualUpDownLookDistance_8(float value)
	{
		___ManualUpDownLookDistance_8 = value;
	}

	inline static int32_t get_offset_of_ResetSpeed_9() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___ResetSpeed_9)); }
	inline float get_ResetSpeed_9() const { return ___ResetSpeed_9; }
	inline float* get_address_of_ResetSpeed_9() { return &___ResetSpeed_9; }
	inline void set_ResetSpeed_9(float value)
	{
		___ResetSpeed_9 = value;
	}

	inline static int32_t get_offset_of_CameraSpeed_10() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___CameraSpeed_10)); }
	inline float get_CameraSpeed_10() const { return ___CameraSpeed_10; }
	inline float* get_address_of_CameraSpeed_10() { return &___CameraSpeed_10; }
	inline void set_CameraSpeed_10(float value)
	{
		___CameraSpeed_10 = value;
	}

	inline static int32_t get_offset_of_MinimumZoom_11() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___MinimumZoom_11)); }
	inline float get_MinimumZoom_11() const { return ___MinimumZoom_11; }
	inline float* get_address_of_MinimumZoom_11() { return &___MinimumZoom_11; }
	inline void set_MinimumZoom_11(float value)
	{
		___MinimumZoom_11 = value;
	}

	inline static int32_t get_offset_of_MaximumZoom_12() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___MaximumZoom_12)); }
	inline float get_MaximumZoom_12() const { return ___MaximumZoom_12; }
	inline float* get_address_of_MaximumZoom_12() { return &___MaximumZoom_12; }
	inline void set_MaximumZoom_12(float value)
	{
		___MaximumZoom_12 = value;
	}

	inline static int32_t get_offset_of_ZoomSpeed_13() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___ZoomSpeed_13)); }
	inline float get_ZoomSpeed_13() const { return ___ZoomSpeed_13; }
	inline float* get_address_of_ZoomSpeed_13() { return &___ZoomSpeed_13; }
	inline void set_ZoomSpeed_13(float value)
	{
		___ZoomSpeed_13 = value;
	}

	inline static int32_t get_offset_of_PixelPerfect_14() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___PixelPerfect_14)); }
	inline bool get_PixelPerfect_14() const { return ___PixelPerfect_14; }
	inline bool* get_address_of_PixelPerfect_14() { return &___PixelPerfect_14; }
	inline void set_PixelPerfect_14(bool value)
	{
		___PixelPerfect_14 = value;
	}

	inline static int32_t get_offset_of_ReferenceVerticalResolution_15() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___ReferenceVerticalResolution_15)); }
	inline int32_t get_ReferenceVerticalResolution_15() const { return ___ReferenceVerticalResolution_15; }
	inline int32_t* get_address_of_ReferenceVerticalResolution_15() { return &___ReferenceVerticalResolution_15; }
	inline void set_ReferenceVerticalResolution_15(int32_t value)
	{
		___ReferenceVerticalResolution_15 = value;
	}

	inline static int32_t get_offset_of_ReferencePixelsPerUnit_16() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___ReferencePixelsPerUnit_16)); }
	inline float get_ReferencePixelsPerUnit_16() const { return ___ReferencePixelsPerUnit_16; }
	inline float* get_address_of_ReferencePixelsPerUnit_16() { return &___ReferencePixelsPerUnit_16; }
	inline void set_ReferencePixelsPerUnit_16(float value)
	{
		___ReferencePixelsPerUnit_16 = value;
	}

	inline static int32_t get_offset_of_EnableEffectsOnMobile_17() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___EnableEffectsOnMobile_17)); }
	inline bool get_EnableEffectsOnMobile_17() const { return ___EnableEffectsOnMobile_17; }
	inline bool* get_address_of_EnableEffectsOnMobile_17() { return &___EnableEffectsOnMobile_17; }
	inline void set_EnableEffectsOnMobile_17(bool value)
	{
		___EnableEffectsOnMobile_17 = value;
	}

	inline static int32_t get_offset_of_InstantRepositionCameraOnRespawn_18() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___InstantRepositionCameraOnRespawn_18)); }
	inline bool get_InstantRepositionCameraOnRespawn_18() const { return ___InstantRepositionCameraOnRespawn_18; }
	inline bool* get_address_of_InstantRepositionCameraOnRespawn_18() { return &___InstantRepositionCameraOnRespawn_18; }
	inline void set_InstantRepositionCameraOnRespawn_18(bool value)
	{
		___InstantRepositionCameraOnRespawn_18 = value;
	}

	inline static int32_t get_offset_of_AssignCameraOnSpawn_19() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___AssignCameraOnSpawn_19)); }
	inline bool get_AssignCameraOnSpawn_19() const { return ___AssignCameraOnSpawn_19; }
	inline bool* get_address_of_AssignCameraOnSpawn_19() { return &___AssignCameraOnSpawn_19; }
	inline void set_AssignCameraOnSpawn_19(bool value)
	{
		___AssignCameraOnSpawn_19 = value;
	}

	inline static int32_t get_offset_of_Target_20() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___Target_20)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_Target_20() const { return ___Target_20; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_Target_20() { return &___Target_20; }
	inline void set_Target_20(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___Target_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_20), (void*)value);
	}

	inline static int32_t get_offset_of_TargetController_21() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___TargetController_21)); }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * get_TargetController_21() const { return ___TargetController_21; }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 ** get_address_of_TargetController_21() { return &___TargetController_21; }
	inline void set_TargetController_21(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * value)
	{
		___TargetController_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetController_21), (void*)value);
	}

	inline static int32_t get_offset_of_StartFollowingBtn_22() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___StartFollowingBtn_22)); }
	inline bool get_StartFollowingBtn_22() const { return ___StartFollowingBtn_22; }
	inline bool* get_address_of_StartFollowingBtn_22() { return &___StartFollowingBtn_22; }
	inline void set_StartFollowingBtn_22(bool value)
	{
		___StartFollowingBtn_22 = value;
	}

	inline static int32_t get_offset_of_StopFollowingBtn_23() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ___StopFollowingBtn_23)); }
	inline bool get_StopFollowingBtn_23() const { return ___StopFollowingBtn_23; }
	inline bool* get_address_of_StopFollowingBtn_23() { return &___StopFollowingBtn_23; }
	inline void set_StopFollowingBtn_23(bool value)
	{
		___StopFollowingBtn_23 = value;
	}

	inline static int32_t get_offset_of__levelBounds_24() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____levelBounds_24)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get__levelBounds_24() const { return ____levelBounds_24; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of__levelBounds_24() { return &____levelBounds_24; }
	inline void set__levelBounds_24(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		____levelBounds_24 = value;
	}

	inline static int32_t get_offset_of__xMin_25() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____xMin_25)); }
	inline float get__xMin_25() const { return ____xMin_25; }
	inline float* get_address_of__xMin_25() { return &____xMin_25; }
	inline void set__xMin_25(float value)
	{
		____xMin_25 = value;
	}

	inline static int32_t get_offset_of__xMax_26() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____xMax_26)); }
	inline float get__xMax_26() const { return ____xMax_26; }
	inline float* get_address_of__xMax_26() { return &____xMax_26; }
	inline void set__xMax_26(float value)
	{
		____xMax_26 = value;
	}

	inline static int32_t get_offset_of__yMin_27() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____yMin_27)); }
	inline float get__yMin_27() const { return ____yMin_27; }
	inline float* get_address_of__yMin_27() { return &____yMin_27; }
	inline void set__yMin_27(float value)
	{
		____yMin_27 = value;
	}

	inline static int32_t get_offset_of__yMax_28() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____yMax_28)); }
	inline float get__yMax_28() const { return ____yMax_28; }
	inline float* get_address_of__yMax_28() { return &____yMax_28; }
	inline void set__yMax_28(float value)
	{
		____yMax_28 = value;
	}

	inline static int32_t get_offset_of__offsetZ_29() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____offsetZ_29)); }
	inline float get__offsetZ_29() const { return ____offsetZ_29; }
	inline float* get_address_of__offsetZ_29() { return &____offsetZ_29; }
	inline void set__offsetZ_29(float value)
	{
		____offsetZ_29 = value;
	}

	inline static int32_t get_offset_of__lastTargetPosition_30() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____lastTargetPosition_30)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__lastTargetPosition_30() const { return ____lastTargetPosition_30; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__lastTargetPosition_30() { return &____lastTargetPosition_30; }
	inline void set__lastTargetPosition_30(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____lastTargetPosition_30 = value;
	}

	inline static int32_t get_offset_of__currentVelocity_31() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____currentVelocity_31)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__currentVelocity_31() const { return ____currentVelocity_31; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__currentVelocity_31() { return &____currentVelocity_31; }
	inline void set__currentVelocity_31(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____currentVelocity_31 = value;
	}

	inline static int32_t get_offset_of__lookAheadPos_32() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____lookAheadPos_32)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__lookAheadPos_32() const { return ____lookAheadPos_32; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__lookAheadPos_32() { return &____lookAheadPos_32; }
	inline void set__lookAheadPos_32(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____lookAheadPos_32 = value;
	}

	inline static int32_t get_offset_of__shakeIntensity_33() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____shakeIntensity_33)); }
	inline float get__shakeIntensity_33() const { return ____shakeIntensity_33; }
	inline float* get_address_of__shakeIntensity_33() { return &____shakeIntensity_33; }
	inline void set__shakeIntensity_33(float value)
	{
		____shakeIntensity_33 = value;
	}

	inline static int32_t get_offset_of__shakeDecay_34() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____shakeDecay_34)); }
	inline float get__shakeDecay_34() const { return ____shakeDecay_34; }
	inline float* get_address_of__shakeDecay_34() { return &____shakeDecay_34; }
	inline void set__shakeDecay_34(float value)
	{
		____shakeDecay_34 = value;
	}

	inline static int32_t get_offset_of__shakeDuration_35() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____shakeDuration_35)); }
	inline float get__shakeDuration_35() const { return ____shakeDuration_35; }
	inline float* get_address_of__shakeDuration_35() { return &____shakeDuration_35; }
	inline void set__shakeDuration_35(float value)
	{
		____shakeDuration_35 = value;
	}

	inline static int32_t get_offset_of__currentZoom_36() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____currentZoom_36)); }
	inline float get__currentZoom_36() const { return ____currentZoom_36; }
	inline float* get_address_of__currentZoom_36() { return &____currentZoom_36; }
	inline void set__currentZoom_36(float value)
	{
		____currentZoom_36 = value;
	}

	inline static int32_t get_offset_of__camera_37() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____camera_37)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get__camera_37() const { return ____camera_37; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of__camera_37() { return &____camera_37; }
	inline void set__camera_37(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		____camera_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____camera_37), (void*)value);
	}

	inline static int32_t get_offset_of__lookDirectionModifier_38() { return static_cast<int32_t>(offsetof(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6, ____lookDirectionModifier_38)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__lookDirectionModifier_38() const { return ____lookDirectionModifier_38; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__lookDirectionModifier_38() { return &____lookDirectionModifier_38; }
	inline void set__lookDirectionModifier_38(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____lookDirectionModifier_38 = value;
	}
};


// MoreMountains.CorgiEngine.Character
struct Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.CorgiEngine.Character/CharacterTypes MoreMountains.CorgiEngine.Character::CharacterType
	int32_t ___CharacterType_4;
	// System.String MoreMountains.CorgiEngine.Character::PlayerID
	String_t* ___PlayerID_5;
	// MoreMountains.CorgiEngine.CharacterStates MoreMountains.CorgiEngine.Character::<CharacterState>k__BackingField
	CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * ___U3CCharacterStateU3Ek__BackingField_6;
	// MoreMountains.CorgiEngine.Character/FacingDirections MoreMountains.CorgiEngine.Character::InitialFacingDirection
	int32_t ___InitialFacingDirection_7;
	// MoreMountains.CorgiEngine.Character/SpawnFacingDirections MoreMountains.CorgiEngine.Character::DirectionOnSpawn
	int32_t ___DirectionOnSpawn_8;
	// System.Boolean MoreMountains.CorgiEngine.Character::<IsFacingRight>k__BackingField
	bool ___U3CIsFacingRightU3Ek__BackingField_9;
	// UnityEngine.Animator MoreMountains.CorgiEngine.Character::CharacterAnimator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___CharacterAnimator_10;
	// System.Boolean MoreMountains.CorgiEngine.Character::UseDefaultMecanim
	bool ___UseDefaultMecanim_11;
	// System.Boolean MoreMountains.CorgiEngine.Character::PerformAnimatorSanityChecks
	bool ___PerformAnimatorSanityChecks_12;
	// System.Boolean MoreMountains.CorgiEngine.Character::DisableAnimatorLogs
	bool ___DisableAnimatorLogs_13;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.Character::CharacterModel
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CharacterModel_14;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.Character::CameraTarget
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___CameraTarget_15;
	// System.Single MoreMountains.CorgiEngine.Character::CameraTargetSpeed
	float ___CameraTargetSpeed_16;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.Character::AdditionalAbilityNodes
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___AdditionalAbilityNodes_17;
	// System.Boolean MoreMountains.CorgiEngine.Character::FlipModelOnDirectionChange
	bool ___FlipModelOnDirectionChange_18;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::ModelFlipValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___ModelFlipValue_19;
	// System.Boolean MoreMountains.CorgiEngine.Character::RotateModelOnDirectionChange
	bool ___RotateModelOnDirectionChange_20;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::ModelRotationValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___ModelRotationValue_21;
	// System.Single MoreMountains.CorgiEngine.Character::ModelRotationSpeed
	float ___ModelRotationSpeed_22;
	// MoreMountains.CorgiEngine.Health MoreMountains.CorgiEngine.Character::CharacterHealth
	Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * ___CharacterHealth_23;
	// System.Boolean MoreMountains.CorgiEngine.Character::SendStateChangeEvents
	bool ___SendStateChangeEvents_24;
	// System.Boolean MoreMountains.CorgiEngine.Character::SendStateUpdateEvents
	bool ___SendStateUpdateEvents_25;
	// System.Single MoreMountains.CorgiEngine.Character::AirborneDistance
	float ___AirborneDistance_26;
	// System.Single MoreMountains.CorgiEngine.Character::AirborneMinimumTime
	float ___AirborneMinimumTime_27;
	// MoreMountains.Tools.AIBrain MoreMountains.CorgiEngine.Character::CharacterBrain
	AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE * ___CharacterBrain_28;
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/MovementStates> MoreMountains.CorgiEngine.Character::MovementState
	MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * ___MovementState_29;
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions> MoreMountains.CorgiEngine.Character::ConditionState
	MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * ___ConditionState_30;
	// MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.Character::<SceneCamera>k__BackingField
	CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * ___U3CSceneCameraU3Ek__BackingField_31;
	// MoreMountains.CorgiEngine.InputManager MoreMountains.CorgiEngine.Character::<LinkedInputManager>k__BackingField
	InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * ___U3CLinkedInputManagerU3Ek__BackingField_32;
	// UnityEngine.Animator MoreMountains.CorgiEngine.Character::<_animator>k__BackingField
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___U3C_animatorU3Ek__BackingField_33;
	// System.Collections.Generic.HashSet`1<System.Int32> MoreMountains.CorgiEngine.Character::<_animatorParameters>k__BackingField
	HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 * ___U3C_animatorParametersU3Ek__BackingField_34;
	// System.Boolean MoreMountains.CorgiEngine.Character::<CanFlip>k__BackingField
	bool ___U3CCanFlipU3Ek__BackingField_35;
	// System.Int32 MoreMountains.CorgiEngine.Character::_groundedAnimationParameter
	int32_t ____groundedAnimationParameter_52;
	// System.Int32 MoreMountains.CorgiEngine.Character::_airborneSpeedAnimationParameter
	int32_t ____airborneSpeedAnimationParameter_53;
	// System.Int32 MoreMountains.CorgiEngine.Character::_xSpeedSpeedAnimationParameter
	int32_t ____xSpeedSpeedAnimationParameter_54;
	// System.Int32 MoreMountains.CorgiEngine.Character::_ySpeedSpeedAnimationParameter
	int32_t ____ySpeedSpeedAnimationParameter_55;
	// System.Int32 MoreMountains.CorgiEngine.Character::_worldXSpeedSpeedAnimationParameter
	int32_t ____worldXSpeedSpeedAnimationParameter_56;
	// System.Int32 MoreMountains.CorgiEngine.Character::_worldYSpeedSpeedAnimationParameter
	int32_t ____worldYSpeedSpeedAnimationParameter_57;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingLeftAnimationParameter
	int32_t ____collidingLeftAnimationParameter_58;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingRightAnimationParameter
	int32_t ____collidingRightAnimationParameter_59;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingBelowAnimationParameter
	int32_t ____collidingBelowAnimationParameter_60;
	// System.Int32 MoreMountains.CorgiEngine.Character::_collidingAboveAnimationParameter
	int32_t ____collidingAboveAnimationParameter_61;
	// System.Int32 MoreMountains.CorgiEngine.Character::_idleSpeedAnimationParameter
	int32_t ____idleSpeedAnimationParameter_62;
	// System.Int32 MoreMountains.CorgiEngine.Character::_aliveAnimationParameter
	int32_t ____aliveAnimationParameter_63;
	// System.Int32 MoreMountains.CorgiEngine.Character::_facingRightAnimationParameter
	int32_t ____facingRightAnimationParameter_64;
	// System.Int32 MoreMountains.CorgiEngine.Character::_randomAnimationParameter
	int32_t ____randomAnimationParameter_65;
	// System.Int32 MoreMountains.CorgiEngine.Character::_randomConstantAnimationParameter
	int32_t ____randomConstantAnimationParameter_66;
	// System.Int32 MoreMountains.CorgiEngine.Character::_flipAnimationParameter
	int32_t ____flipAnimationParameter_67;
	// MoreMountains.CorgiEngine.CorgiController MoreMountains.CorgiEngine.Character::_controller
	CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * ____controller_68;
	// UnityEngine.SpriteRenderer MoreMountains.CorgiEngine.Character::_spriteRenderer
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ____spriteRenderer_69;
	// UnityEngine.Color MoreMountains.CorgiEngine.Character::_initialColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ____initialColor_70;
	// MoreMountains.CorgiEngine.CharacterAbility[] MoreMountains.CorgiEngine.Character::_characterAbilities
	CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E* ____characterAbilities_71;
	// System.Single MoreMountains.CorgiEngine.Character::_originalGravity
	float ____originalGravity_72;
	// System.Boolean MoreMountains.CorgiEngine.Character::_spawnDirectionForced
	bool ____spawnDirectionForced_73;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::_targetModelRotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____targetModelRotation_74;
	// MoreMountains.CorgiEngine.DamageOnTouch MoreMountains.CorgiEngine.Character::_damageOnTouch
	DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 * ____damageOnTouch_75;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::_cameraTargetInitialPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____cameraTargetInitialPosition_76;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Character::_cameraOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____cameraOffset_77;
	// System.Boolean MoreMountains.CorgiEngine.Character::_abilitiesCachedOnce
	bool ____abilitiesCachedOnce_78;
	// System.Single MoreMountains.CorgiEngine.Character::_animatorRandomNumber
	float ____animatorRandomNumber_79;
	// MoreMountains.CorgiEngine.CharacterPersistence MoreMountains.CorgiEngine.Character::_characterPersistence
	CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * ____characterPersistence_80;
	// MoreMountains.CorgiEngine.CharacterStates/CharacterConditions MoreMountains.CorgiEngine.Character::_conditionStateBeforeFreeze
	int32_t ____conditionStateBeforeFreeze_81;

public:
	inline static int32_t get_offset_of_CharacterType_4() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterType_4)); }
	inline int32_t get_CharacterType_4() const { return ___CharacterType_4; }
	inline int32_t* get_address_of_CharacterType_4() { return &___CharacterType_4; }
	inline void set_CharacterType_4(int32_t value)
	{
		___CharacterType_4 = value;
	}

	inline static int32_t get_offset_of_PlayerID_5() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___PlayerID_5)); }
	inline String_t* get_PlayerID_5() const { return ___PlayerID_5; }
	inline String_t** get_address_of_PlayerID_5() { return &___PlayerID_5; }
	inline void set_PlayerID_5(String_t* value)
	{
		___PlayerID_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCharacterStateU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CCharacterStateU3Ek__BackingField_6)); }
	inline CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * get_U3CCharacterStateU3Ek__BackingField_6() const { return ___U3CCharacterStateU3Ek__BackingField_6; }
	inline CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F ** get_address_of_U3CCharacterStateU3Ek__BackingField_6() { return &___U3CCharacterStateU3Ek__BackingField_6; }
	inline void set_U3CCharacterStateU3Ek__BackingField_6(CharacterStates_tFB7F6A0DE3F227264096E732A3910406D76E558F * value)
	{
		___U3CCharacterStateU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCharacterStateU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_InitialFacingDirection_7() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___InitialFacingDirection_7)); }
	inline int32_t get_InitialFacingDirection_7() const { return ___InitialFacingDirection_7; }
	inline int32_t* get_address_of_InitialFacingDirection_7() { return &___InitialFacingDirection_7; }
	inline void set_InitialFacingDirection_7(int32_t value)
	{
		___InitialFacingDirection_7 = value;
	}

	inline static int32_t get_offset_of_DirectionOnSpawn_8() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___DirectionOnSpawn_8)); }
	inline int32_t get_DirectionOnSpawn_8() const { return ___DirectionOnSpawn_8; }
	inline int32_t* get_address_of_DirectionOnSpawn_8() { return &___DirectionOnSpawn_8; }
	inline void set_DirectionOnSpawn_8(int32_t value)
	{
		___DirectionOnSpawn_8 = value;
	}

	inline static int32_t get_offset_of_U3CIsFacingRightU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CIsFacingRightU3Ek__BackingField_9)); }
	inline bool get_U3CIsFacingRightU3Ek__BackingField_9() const { return ___U3CIsFacingRightU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIsFacingRightU3Ek__BackingField_9() { return &___U3CIsFacingRightU3Ek__BackingField_9; }
	inline void set_U3CIsFacingRightU3Ek__BackingField_9(bool value)
	{
		___U3CIsFacingRightU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_CharacterAnimator_10() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterAnimator_10)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_CharacterAnimator_10() const { return ___CharacterAnimator_10; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_CharacterAnimator_10() { return &___CharacterAnimator_10; }
	inline void set_CharacterAnimator_10(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___CharacterAnimator_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterAnimator_10), (void*)value);
	}

	inline static int32_t get_offset_of_UseDefaultMecanim_11() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___UseDefaultMecanim_11)); }
	inline bool get_UseDefaultMecanim_11() const { return ___UseDefaultMecanim_11; }
	inline bool* get_address_of_UseDefaultMecanim_11() { return &___UseDefaultMecanim_11; }
	inline void set_UseDefaultMecanim_11(bool value)
	{
		___UseDefaultMecanim_11 = value;
	}

	inline static int32_t get_offset_of_PerformAnimatorSanityChecks_12() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___PerformAnimatorSanityChecks_12)); }
	inline bool get_PerformAnimatorSanityChecks_12() const { return ___PerformAnimatorSanityChecks_12; }
	inline bool* get_address_of_PerformAnimatorSanityChecks_12() { return &___PerformAnimatorSanityChecks_12; }
	inline void set_PerformAnimatorSanityChecks_12(bool value)
	{
		___PerformAnimatorSanityChecks_12 = value;
	}

	inline static int32_t get_offset_of_DisableAnimatorLogs_13() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___DisableAnimatorLogs_13)); }
	inline bool get_DisableAnimatorLogs_13() const { return ___DisableAnimatorLogs_13; }
	inline bool* get_address_of_DisableAnimatorLogs_13() { return &___DisableAnimatorLogs_13; }
	inline void set_DisableAnimatorLogs_13(bool value)
	{
		___DisableAnimatorLogs_13 = value;
	}

	inline static int32_t get_offset_of_CharacterModel_14() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterModel_14)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CharacterModel_14() const { return ___CharacterModel_14; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CharacterModel_14() { return &___CharacterModel_14; }
	inline void set_CharacterModel_14(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CharacterModel_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterModel_14), (void*)value);
	}

	inline static int32_t get_offset_of_CameraTarget_15() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CameraTarget_15)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_CameraTarget_15() const { return ___CameraTarget_15; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_CameraTarget_15() { return &___CameraTarget_15; }
	inline void set_CameraTarget_15(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___CameraTarget_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CameraTarget_15), (void*)value);
	}

	inline static int32_t get_offset_of_CameraTargetSpeed_16() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CameraTargetSpeed_16)); }
	inline float get_CameraTargetSpeed_16() const { return ___CameraTargetSpeed_16; }
	inline float* get_address_of_CameraTargetSpeed_16() { return &___CameraTargetSpeed_16; }
	inline void set_CameraTargetSpeed_16(float value)
	{
		___CameraTargetSpeed_16 = value;
	}

	inline static int32_t get_offset_of_AdditionalAbilityNodes_17() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___AdditionalAbilityNodes_17)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_AdditionalAbilityNodes_17() const { return ___AdditionalAbilityNodes_17; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_AdditionalAbilityNodes_17() { return &___AdditionalAbilityNodes_17; }
	inline void set_AdditionalAbilityNodes_17(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___AdditionalAbilityNodes_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AdditionalAbilityNodes_17), (void*)value);
	}

	inline static int32_t get_offset_of_FlipModelOnDirectionChange_18() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___FlipModelOnDirectionChange_18)); }
	inline bool get_FlipModelOnDirectionChange_18() const { return ___FlipModelOnDirectionChange_18; }
	inline bool* get_address_of_FlipModelOnDirectionChange_18() { return &___FlipModelOnDirectionChange_18; }
	inline void set_FlipModelOnDirectionChange_18(bool value)
	{
		___FlipModelOnDirectionChange_18 = value;
	}

	inline static int32_t get_offset_of_ModelFlipValue_19() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ModelFlipValue_19)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_ModelFlipValue_19() const { return ___ModelFlipValue_19; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_ModelFlipValue_19() { return &___ModelFlipValue_19; }
	inline void set_ModelFlipValue_19(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___ModelFlipValue_19 = value;
	}

	inline static int32_t get_offset_of_RotateModelOnDirectionChange_20() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___RotateModelOnDirectionChange_20)); }
	inline bool get_RotateModelOnDirectionChange_20() const { return ___RotateModelOnDirectionChange_20; }
	inline bool* get_address_of_RotateModelOnDirectionChange_20() { return &___RotateModelOnDirectionChange_20; }
	inline void set_RotateModelOnDirectionChange_20(bool value)
	{
		___RotateModelOnDirectionChange_20 = value;
	}

	inline static int32_t get_offset_of_ModelRotationValue_21() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ModelRotationValue_21)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_ModelRotationValue_21() const { return ___ModelRotationValue_21; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_ModelRotationValue_21() { return &___ModelRotationValue_21; }
	inline void set_ModelRotationValue_21(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___ModelRotationValue_21 = value;
	}

	inline static int32_t get_offset_of_ModelRotationSpeed_22() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ModelRotationSpeed_22)); }
	inline float get_ModelRotationSpeed_22() const { return ___ModelRotationSpeed_22; }
	inline float* get_address_of_ModelRotationSpeed_22() { return &___ModelRotationSpeed_22; }
	inline void set_ModelRotationSpeed_22(float value)
	{
		___ModelRotationSpeed_22 = value;
	}

	inline static int32_t get_offset_of_CharacterHealth_23() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterHealth_23)); }
	inline Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * get_CharacterHealth_23() const { return ___CharacterHealth_23; }
	inline Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 ** get_address_of_CharacterHealth_23() { return &___CharacterHealth_23; }
	inline void set_CharacterHealth_23(Health_t2AA6C7EC8E736E692A26230D97BBD4983BFB4E51 * value)
	{
		___CharacterHealth_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterHealth_23), (void*)value);
	}

	inline static int32_t get_offset_of_SendStateChangeEvents_24() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___SendStateChangeEvents_24)); }
	inline bool get_SendStateChangeEvents_24() const { return ___SendStateChangeEvents_24; }
	inline bool* get_address_of_SendStateChangeEvents_24() { return &___SendStateChangeEvents_24; }
	inline void set_SendStateChangeEvents_24(bool value)
	{
		___SendStateChangeEvents_24 = value;
	}

	inline static int32_t get_offset_of_SendStateUpdateEvents_25() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___SendStateUpdateEvents_25)); }
	inline bool get_SendStateUpdateEvents_25() const { return ___SendStateUpdateEvents_25; }
	inline bool* get_address_of_SendStateUpdateEvents_25() { return &___SendStateUpdateEvents_25; }
	inline void set_SendStateUpdateEvents_25(bool value)
	{
		___SendStateUpdateEvents_25 = value;
	}

	inline static int32_t get_offset_of_AirborneDistance_26() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___AirborneDistance_26)); }
	inline float get_AirborneDistance_26() const { return ___AirborneDistance_26; }
	inline float* get_address_of_AirborneDistance_26() { return &___AirborneDistance_26; }
	inline void set_AirborneDistance_26(float value)
	{
		___AirborneDistance_26 = value;
	}

	inline static int32_t get_offset_of_AirborneMinimumTime_27() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___AirborneMinimumTime_27)); }
	inline float get_AirborneMinimumTime_27() const { return ___AirborneMinimumTime_27; }
	inline float* get_address_of_AirborneMinimumTime_27() { return &___AirborneMinimumTime_27; }
	inline void set_AirborneMinimumTime_27(float value)
	{
		___AirborneMinimumTime_27 = value;
	}

	inline static int32_t get_offset_of_CharacterBrain_28() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___CharacterBrain_28)); }
	inline AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE * get_CharacterBrain_28() const { return ___CharacterBrain_28; }
	inline AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE ** get_address_of_CharacterBrain_28() { return &___CharacterBrain_28; }
	inline void set_CharacterBrain_28(AIBrain_t6A6377BCDEB092D5425AACEAE3EF265B2AB6C5DE * value)
	{
		___CharacterBrain_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CharacterBrain_28), (void*)value);
	}

	inline static int32_t get_offset_of_MovementState_29() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___MovementState_29)); }
	inline MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * get_MovementState_29() const { return ___MovementState_29; }
	inline MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 ** get_address_of_MovementState_29() { return &___MovementState_29; }
	inline void set_MovementState_29(MMStateMachine_1_t2DA04C4B154E7DCF8C0F96693DDE3B91243E0EC0 * value)
	{
		___MovementState_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MovementState_29), (void*)value);
	}

	inline static int32_t get_offset_of_ConditionState_30() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___ConditionState_30)); }
	inline MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * get_ConditionState_30() const { return ___ConditionState_30; }
	inline MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 ** get_address_of_ConditionState_30() { return &___ConditionState_30; }
	inline void set_ConditionState_30(MMStateMachine_1_tE2E25EE1D552DA9283661F89B231149447D4C638 * value)
	{
		___ConditionState_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionState_30), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSceneCameraU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CSceneCameraU3Ek__BackingField_31)); }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * get_U3CSceneCameraU3Ek__BackingField_31() const { return ___U3CSceneCameraU3Ek__BackingField_31; }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 ** get_address_of_U3CSceneCameraU3Ek__BackingField_31() { return &___U3CSceneCameraU3Ek__BackingField_31; }
	inline void set_U3CSceneCameraU3Ek__BackingField_31(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * value)
	{
		___U3CSceneCameraU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSceneCameraU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLinkedInputManagerU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CLinkedInputManagerU3Ek__BackingField_32)); }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * get_U3CLinkedInputManagerU3Ek__BackingField_32() const { return ___U3CLinkedInputManagerU3Ek__BackingField_32; }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA ** get_address_of_U3CLinkedInputManagerU3Ek__BackingField_32() { return &___U3CLinkedInputManagerU3Ek__BackingField_32; }
	inline void set_U3CLinkedInputManagerU3Ek__BackingField_32(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * value)
	{
		___U3CLinkedInputManagerU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLinkedInputManagerU3Ek__BackingField_32), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_animatorU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3C_animatorU3Ek__BackingField_33)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_U3C_animatorU3Ek__BackingField_33() const { return ___U3C_animatorU3Ek__BackingField_33; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_U3C_animatorU3Ek__BackingField_33() { return &___U3C_animatorU3Ek__BackingField_33; }
	inline void set_U3C_animatorU3Ek__BackingField_33(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___U3C_animatorU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_animatorU3Ek__BackingField_33), (void*)value);
	}

	inline static int32_t get_offset_of_U3C_animatorParametersU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3C_animatorParametersU3Ek__BackingField_34)); }
	inline HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 * get_U3C_animatorParametersU3Ek__BackingField_34() const { return ___U3C_animatorParametersU3Ek__BackingField_34; }
	inline HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 ** get_address_of_U3C_animatorParametersU3Ek__BackingField_34() { return &___U3C_animatorParametersU3Ek__BackingField_34; }
	inline void set_U3C_animatorParametersU3Ek__BackingField_34(HashSet_1_tF187707BD5564B6808CE30721FBC083F00B385E5 * value)
	{
		___U3C_animatorParametersU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3C_animatorParametersU3Ek__BackingField_34), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCanFlipU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ___U3CCanFlipU3Ek__BackingField_35)); }
	inline bool get_U3CCanFlipU3Ek__BackingField_35() const { return ___U3CCanFlipU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CCanFlipU3Ek__BackingField_35() { return &___U3CCanFlipU3Ek__BackingField_35; }
	inline void set_U3CCanFlipU3Ek__BackingField_35(bool value)
	{
		___U3CCanFlipU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__groundedAnimationParameter_52() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____groundedAnimationParameter_52)); }
	inline int32_t get__groundedAnimationParameter_52() const { return ____groundedAnimationParameter_52; }
	inline int32_t* get_address_of__groundedAnimationParameter_52() { return &____groundedAnimationParameter_52; }
	inline void set__groundedAnimationParameter_52(int32_t value)
	{
		____groundedAnimationParameter_52 = value;
	}

	inline static int32_t get_offset_of__airborneSpeedAnimationParameter_53() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____airborneSpeedAnimationParameter_53)); }
	inline int32_t get__airborneSpeedAnimationParameter_53() const { return ____airborneSpeedAnimationParameter_53; }
	inline int32_t* get_address_of__airborneSpeedAnimationParameter_53() { return &____airborneSpeedAnimationParameter_53; }
	inline void set__airborneSpeedAnimationParameter_53(int32_t value)
	{
		____airborneSpeedAnimationParameter_53 = value;
	}

	inline static int32_t get_offset_of__xSpeedSpeedAnimationParameter_54() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____xSpeedSpeedAnimationParameter_54)); }
	inline int32_t get__xSpeedSpeedAnimationParameter_54() const { return ____xSpeedSpeedAnimationParameter_54; }
	inline int32_t* get_address_of__xSpeedSpeedAnimationParameter_54() { return &____xSpeedSpeedAnimationParameter_54; }
	inline void set__xSpeedSpeedAnimationParameter_54(int32_t value)
	{
		____xSpeedSpeedAnimationParameter_54 = value;
	}

	inline static int32_t get_offset_of__ySpeedSpeedAnimationParameter_55() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____ySpeedSpeedAnimationParameter_55)); }
	inline int32_t get__ySpeedSpeedAnimationParameter_55() const { return ____ySpeedSpeedAnimationParameter_55; }
	inline int32_t* get_address_of__ySpeedSpeedAnimationParameter_55() { return &____ySpeedSpeedAnimationParameter_55; }
	inline void set__ySpeedSpeedAnimationParameter_55(int32_t value)
	{
		____ySpeedSpeedAnimationParameter_55 = value;
	}

	inline static int32_t get_offset_of__worldXSpeedSpeedAnimationParameter_56() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____worldXSpeedSpeedAnimationParameter_56)); }
	inline int32_t get__worldXSpeedSpeedAnimationParameter_56() const { return ____worldXSpeedSpeedAnimationParameter_56; }
	inline int32_t* get_address_of__worldXSpeedSpeedAnimationParameter_56() { return &____worldXSpeedSpeedAnimationParameter_56; }
	inline void set__worldXSpeedSpeedAnimationParameter_56(int32_t value)
	{
		____worldXSpeedSpeedAnimationParameter_56 = value;
	}

	inline static int32_t get_offset_of__worldYSpeedSpeedAnimationParameter_57() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____worldYSpeedSpeedAnimationParameter_57)); }
	inline int32_t get__worldYSpeedSpeedAnimationParameter_57() const { return ____worldYSpeedSpeedAnimationParameter_57; }
	inline int32_t* get_address_of__worldYSpeedSpeedAnimationParameter_57() { return &____worldYSpeedSpeedAnimationParameter_57; }
	inline void set__worldYSpeedSpeedAnimationParameter_57(int32_t value)
	{
		____worldYSpeedSpeedAnimationParameter_57 = value;
	}

	inline static int32_t get_offset_of__collidingLeftAnimationParameter_58() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingLeftAnimationParameter_58)); }
	inline int32_t get__collidingLeftAnimationParameter_58() const { return ____collidingLeftAnimationParameter_58; }
	inline int32_t* get_address_of__collidingLeftAnimationParameter_58() { return &____collidingLeftAnimationParameter_58; }
	inline void set__collidingLeftAnimationParameter_58(int32_t value)
	{
		____collidingLeftAnimationParameter_58 = value;
	}

	inline static int32_t get_offset_of__collidingRightAnimationParameter_59() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingRightAnimationParameter_59)); }
	inline int32_t get__collidingRightAnimationParameter_59() const { return ____collidingRightAnimationParameter_59; }
	inline int32_t* get_address_of__collidingRightAnimationParameter_59() { return &____collidingRightAnimationParameter_59; }
	inline void set__collidingRightAnimationParameter_59(int32_t value)
	{
		____collidingRightAnimationParameter_59 = value;
	}

	inline static int32_t get_offset_of__collidingBelowAnimationParameter_60() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingBelowAnimationParameter_60)); }
	inline int32_t get__collidingBelowAnimationParameter_60() const { return ____collidingBelowAnimationParameter_60; }
	inline int32_t* get_address_of__collidingBelowAnimationParameter_60() { return &____collidingBelowAnimationParameter_60; }
	inline void set__collidingBelowAnimationParameter_60(int32_t value)
	{
		____collidingBelowAnimationParameter_60 = value;
	}

	inline static int32_t get_offset_of__collidingAboveAnimationParameter_61() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____collidingAboveAnimationParameter_61)); }
	inline int32_t get__collidingAboveAnimationParameter_61() const { return ____collidingAboveAnimationParameter_61; }
	inline int32_t* get_address_of__collidingAboveAnimationParameter_61() { return &____collidingAboveAnimationParameter_61; }
	inline void set__collidingAboveAnimationParameter_61(int32_t value)
	{
		____collidingAboveAnimationParameter_61 = value;
	}

	inline static int32_t get_offset_of__idleSpeedAnimationParameter_62() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____idleSpeedAnimationParameter_62)); }
	inline int32_t get__idleSpeedAnimationParameter_62() const { return ____idleSpeedAnimationParameter_62; }
	inline int32_t* get_address_of__idleSpeedAnimationParameter_62() { return &____idleSpeedAnimationParameter_62; }
	inline void set__idleSpeedAnimationParameter_62(int32_t value)
	{
		____idleSpeedAnimationParameter_62 = value;
	}

	inline static int32_t get_offset_of__aliveAnimationParameter_63() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____aliveAnimationParameter_63)); }
	inline int32_t get__aliveAnimationParameter_63() const { return ____aliveAnimationParameter_63; }
	inline int32_t* get_address_of__aliveAnimationParameter_63() { return &____aliveAnimationParameter_63; }
	inline void set__aliveAnimationParameter_63(int32_t value)
	{
		____aliveAnimationParameter_63 = value;
	}

	inline static int32_t get_offset_of__facingRightAnimationParameter_64() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____facingRightAnimationParameter_64)); }
	inline int32_t get__facingRightAnimationParameter_64() const { return ____facingRightAnimationParameter_64; }
	inline int32_t* get_address_of__facingRightAnimationParameter_64() { return &____facingRightAnimationParameter_64; }
	inline void set__facingRightAnimationParameter_64(int32_t value)
	{
		____facingRightAnimationParameter_64 = value;
	}

	inline static int32_t get_offset_of__randomAnimationParameter_65() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____randomAnimationParameter_65)); }
	inline int32_t get__randomAnimationParameter_65() const { return ____randomAnimationParameter_65; }
	inline int32_t* get_address_of__randomAnimationParameter_65() { return &____randomAnimationParameter_65; }
	inline void set__randomAnimationParameter_65(int32_t value)
	{
		____randomAnimationParameter_65 = value;
	}

	inline static int32_t get_offset_of__randomConstantAnimationParameter_66() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____randomConstantAnimationParameter_66)); }
	inline int32_t get__randomConstantAnimationParameter_66() const { return ____randomConstantAnimationParameter_66; }
	inline int32_t* get_address_of__randomConstantAnimationParameter_66() { return &____randomConstantAnimationParameter_66; }
	inline void set__randomConstantAnimationParameter_66(int32_t value)
	{
		____randomConstantAnimationParameter_66 = value;
	}

	inline static int32_t get_offset_of__flipAnimationParameter_67() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____flipAnimationParameter_67)); }
	inline int32_t get__flipAnimationParameter_67() const { return ____flipAnimationParameter_67; }
	inline int32_t* get_address_of__flipAnimationParameter_67() { return &____flipAnimationParameter_67; }
	inline void set__flipAnimationParameter_67(int32_t value)
	{
		____flipAnimationParameter_67 = value;
	}

	inline static int32_t get_offset_of__controller_68() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____controller_68)); }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * get__controller_68() const { return ____controller_68; }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 ** get_address_of__controller_68() { return &____controller_68; }
	inline void set__controller_68(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * value)
	{
		____controller_68 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____controller_68), (void*)value);
	}

	inline static int32_t get_offset_of__spriteRenderer_69() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____spriteRenderer_69)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get__spriteRenderer_69() const { return ____spriteRenderer_69; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of__spriteRenderer_69() { return &____spriteRenderer_69; }
	inline void set__spriteRenderer_69(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		____spriteRenderer_69 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____spriteRenderer_69), (void*)value);
	}

	inline static int32_t get_offset_of__initialColor_70() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____initialColor_70)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get__initialColor_70() const { return ____initialColor_70; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of__initialColor_70() { return &____initialColor_70; }
	inline void set__initialColor_70(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		____initialColor_70 = value;
	}

	inline static int32_t get_offset_of__characterAbilities_71() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____characterAbilities_71)); }
	inline CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E* get__characterAbilities_71() const { return ____characterAbilities_71; }
	inline CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E** get_address_of__characterAbilities_71() { return &____characterAbilities_71; }
	inline void set__characterAbilities_71(CharacterAbilityU5BU5D_tF35CF112B0A661D8851CBF423E19F6DDF7F5DD8E* value)
	{
		____characterAbilities_71 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterAbilities_71), (void*)value);
	}

	inline static int32_t get_offset_of__originalGravity_72() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____originalGravity_72)); }
	inline float get__originalGravity_72() const { return ____originalGravity_72; }
	inline float* get_address_of__originalGravity_72() { return &____originalGravity_72; }
	inline void set__originalGravity_72(float value)
	{
		____originalGravity_72 = value;
	}

	inline static int32_t get_offset_of__spawnDirectionForced_73() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____spawnDirectionForced_73)); }
	inline bool get__spawnDirectionForced_73() const { return ____spawnDirectionForced_73; }
	inline bool* get_address_of__spawnDirectionForced_73() { return &____spawnDirectionForced_73; }
	inline void set__spawnDirectionForced_73(bool value)
	{
		____spawnDirectionForced_73 = value;
	}

	inline static int32_t get_offset_of__targetModelRotation_74() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____targetModelRotation_74)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__targetModelRotation_74() const { return ____targetModelRotation_74; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__targetModelRotation_74() { return &____targetModelRotation_74; }
	inline void set__targetModelRotation_74(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____targetModelRotation_74 = value;
	}

	inline static int32_t get_offset_of__damageOnTouch_75() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____damageOnTouch_75)); }
	inline DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 * get__damageOnTouch_75() const { return ____damageOnTouch_75; }
	inline DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 ** get_address_of__damageOnTouch_75() { return &____damageOnTouch_75; }
	inline void set__damageOnTouch_75(DamageOnTouch_tE1D5B8D962163AE98688C3ABAE76EA29BA6732B1 * value)
	{
		____damageOnTouch_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____damageOnTouch_75), (void*)value);
	}

	inline static int32_t get_offset_of__cameraTargetInitialPosition_76() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____cameraTargetInitialPosition_76)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__cameraTargetInitialPosition_76() const { return ____cameraTargetInitialPosition_76; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__cameraTargetInitialPosition_76() { return &____cameraTargetInitialPosition_76; }
	inline void set__cameraTargetInitialPosition_76(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____cameraTargetInitialPosition_76 = value;
	}

	inline static int32_t get_offset_of__cameraOffset_77() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____cameraOffset_77)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__cameraOffset_77() const { return ____cameraOffset_77; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__cameraOffset_77() { return &____cameraOffset_77; }
	inline void set__cameraOffset_77(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____cameraOffset_77 = value;
	}

	inline static int32_t get_offset_of__abilitiesCachedOnce_78() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____abilitiesCachedOnce_78)); }
	inline bool get__abilitiesCachedOnce_78() const { return ____abilitiesCachedOnce_78; }
	inline bool* get_address_of__abilitiesCachedOnce_78() { return &____abilitiesCachedOnce_78; }
	inline void set__abilitiesCachedOnce_78(bool value)
	{
		____abilitiesCachedOnce_78 = value;
	}

	inline static int32_t get_offset_of__animatorRandomNumber_79() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____animatorRandomNumber_79)); }
	inline float get__animatorRandomNumber_79() const { return ____animatorRandomNumber_79; }
	inline float* get_address_of__animatorRandomNumber_79() { return &____animatorRandomNumber_79; }
	inline void set__animatorRandomNumber_79(float value)
	{
		____animatorRandomNumber_79 = value;
	}

	inline static int32_t get_offset_of__characterPersistence_80() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____characterPersistence_80)); }
	inline CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * get__characterPersistence_80() const { return ____characterPersistence_80; }
	inline CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC ** get_address_of__characterPersistence_80() { return &____characterPersistence_80; }
	inline void set__characterPersistence_80(CharacterPersistence_t6A90CF8CEA44217FBECFF4F5B1A5C76FA583D2BC * value)
	{
		____characterPersistence_80 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterPersistence_80), (void*)value);
	}

	inline static int32_t get_offset_of__conditionStateBeforeFreeze_81() { return static_cast<int32_t>(offsetof(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612, ____conditionStateBeforeFreeze_81)); }
	inline int32_t get__conditionStateBeforeFreeze_81() const { return ____conditionStateBeforeFreeze_81; }
	inline int32_t* get_address_of__conditionStateBeforeFreeze_81() { return &____conditionStateBeforeFreeze_81; }
	inline void set__conditionStateBeforeFreeze_81(int32_t value)
	{
		____conditionStateBeforeFreeze_81 = value;
	}
};


// Cinemachine.CinemachineBrain
struct CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Cinemachine.CinemachineBrain::m_ShowDebugText
	bool ___m_ShowDebugText_4;
	// System.Boolean Cinemachine.CinemachineBrain::m_ShowCameraFrustum
	bool ___m_ShowCameraFrustum_5;
	// System.Boolean Cinemachine.CinemachineBrain::m_IgnoreTimeScale
	bool ___m_IgnoreTimeScale_6;
	// UnityEngine.Transform Cinemachine.CinemachineBrain::m_WorldUpOverride
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_WorldUpOverride_7;
	// Cinemachine.CinemachineBrain/UpdateMethod Cinemachine.CinemachineBrain::m_UpdateMethod
	int32_t ___m_UpdateMethod_8;
	// Cinemachine.CinemachineBrain/BrainUpdateMethod Cinemachine.CinemachineBrain::m_BlendUpdateMethod
	int32_t ___m_BlendUpdateMethod_9;
	// Cinemachine.CinemachineBlendDefinition Cinemachine.CinemachineBrain::m_DefaultBlend
	CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE  ___m_DefaultBlend_10;
	// Cinemachine.CinemachineBlenderSettings Cinemachine.CinemachineBrain::m_CustomBlends
	CinemachineBlenderSettings_tCF3367AA417C05816117E15FE608928CAF40554F * ___m_CustomBlends_11;
	// UnityEngine.Camera Cinemachine.CinemachineBrain::m_OutputCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ___m_OutputCamera_12;
	// Cinemachine.CinemachineBrain/BrainEvent Cinemachine.CinemachineBrain::m_CameraCutEvent
	BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * ___m_CameraCutEvent_13;
	// Cinemachine.CinemachineBrain/VcamActivatedEvent Cinemachine.CinemachineBrain::m_CameraActivatedEvent
	VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * ___m_CameraActivatedEvent_14;
	// UnityEngine.Coroutine Cinemachine.CinemachineBrain::mPhysicsCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ___mPhysicsCoroutine_16;
	// UnityEngine.WaitForFixedUpdate Cinemachine.CinemachineBrain::mWaitForFixedUpdate
	WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 * ___mWaitForFixedUpdate_17;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineBrain/BrainFrame> Cinemachine.CinemachineBrain::mFrameStack
	List_1_t7047292520C88A40E765EA61E967062F583F6C2A * ___mFrameStack_18;
	// System.Int32 Cinemachine.CinemachineBrain::mNextFrameId
	int32_t ___mNextFrameId_19;
	// Cinemachine.CinemachineBlend Cinemachine.CinemachineBrain::mCurrentLiveCameras
	CinemachineBlend_tA1423010B6E480A1066D469A77F1CB4E2BC6925C * ___mCurrentLiveCameras_20;
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::mActiveCameraPreviousFrame
	RuntimeObject* ___mActiveCameraPreviousFrame_22;
	// UnityEngine.GameObject Cinemachine.CinemachineBrain::mActiveCameraPreviousFrameGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___mActiveCameraPreviousFrameGameObject_23;
	// Cinemachine.CameraState Cinemachine.CinemachineBrain::<CurrentCameraState>k__BackingField
	CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601  ___U3CCurrentCameraStateU3Ek__BackingField_24;

public:
	inline static int32_t get_offset_of_m_ShowDebugText_4() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_ShowDebugText_4)); }
	inline bool get_m_ShowDebugText_4() const { return ___m_ShowDebugText_4; }
	inline bool* get_address_of_m_ShowDebugText_4() { return &___m_ShowDebugText_4; }
	inline void set_m_ShowDebugText_4(bool value)
	{
		___m_ShowDebugText_4 = value;
	}

	inline static int32_t get_offset_of_m_ShowCameraFrustum_5() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_ShowCameraFrustum_5)); }
	inline bool get_m_ShowCameraFrustum_5() const { return ___m_ShowCameraFrustum_5; }
	inline bool* get_address_of_m_ShowCameraFrustum_5() { return &___m_ShowCameraFrustum_5; }
	inline void set_m_ShowCameraFrustum_5(bool value)
	{
		___m_ShowCameraFrustum_5 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreTimeScale_6() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_IgnoreTimeScale_6)); }
	inline bool get_m_IgnoreTimeScale_6() const { return ___m_IgnoreTimeScale_6; }
	inline bool* get_address_of_m_IgnoreTimeScale_6() { return &___m_IgnoreTimeScale_6; }
	inline void set_m_IgnoreTimeScale_6(bool value)
	{
		___m_IgnoreTimeScale_6 = value;
	}

	inline static int32_t get_offset_of_m_WorldUpOverride_7() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_WorldUpOverride_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_WorldUpOverride_7() const { return ___m_WorldUpOverride_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_WorldUpOverride_7() { return &___m_WorldUpOverride_7; }
	inline void set_m_WorldUpOverride_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_WorldUpOverride_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_WorldUpOverride_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_UpdateMethod_8() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_UpdateMethod_8)); }
	inline int32_t get_m_UpdateMethod_8() const { return ___m_UpdateMethod_8; }
	inline int32_t* get_address_of_m_UpdateMethod_8() { return &___m_UpdateMethod_8; }
	inline void set_m_UpdateMethod_8(int32_t value)
	{
		___m_UpdateMethod_8 = value;
	}

	inline static int32_t get_offset_of_m_BlendUpdateMethod_9() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_BlendUpdateMethod_9)); }
	inline int32_t get_m_BlendUpdateMethod_9() const { return ___m_BlendUpdateMethod_9; }
	inline int32_t* get_address_of_m_BlendUpdateMethod_9() { return &___m_BlendUpdateMethod_9; }
	inline void set_m_BlendUpdateMethod_9(int32_t value)
	{
		___m_BlendUpdateMethod_9 = value;
	}

	inline static int32_t get_offset_of_m_DefaultBlend_10() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_DefaultBlend_10)); }
	inline CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE  get_m_DefaultBlend_10() const { return ___m_DefaultBlend_10; }
	inline CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE * get_address_of_m_DefaultBlend_10() { return &___m_DefaultBlend_10; }
	inline void set_m_DefaultBlend_10(CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE  value)
	{
		___m_DefaultBlend_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_DefaultBlend_10))->___m_CustomCurve_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_CustomBlends_11() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_CustomBlends_11)); }
	inline CinemachineBlenderSettings_tCF3367AA417C05816117E15FE608928CAF40554F * get_m_CustomBlends_11() const { return ___m_CustomBlends_11; }
	inline CinemachineBlenderSettings_tCF3367AA417C05816117E15FE608928CAF40554F ** get_address_of_m_CustomBlends_11() { return &___m_CustomBlends_11; }
	inline void set_m_CustomBlends_11(CinemachineBlenderSettings_tCF3367AA417C05816117E15FE608928CAF40554F * value)
	{
		___m_CustomBlends_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CustomBlends_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_OutputCamera_12() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_OutputCamera_12)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get_m_OutputCamera_12() const { return ___m_OutputCamera_12; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of_m_OutputCamera_12() { return &___m_OutputCamera_12; }
	inline void set_m_OutputCamera_12(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		___m_OutputCamera_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OutputCamera_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraCutEvent_13() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_CameraCutEvent_13)); }
	inline BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * get_m_CameraCutEvent_13() const { return ___m_CameraCutEvent_13; }
	inline BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E ** get_address_of_m_CameraCutEvent_13() { return &___m_CameraCutEvent_13; }
	inline void set_m_CameraCutEvent_13(BrainEvent_t7DDC9AFC269C95A82A9348F1F7120BA402DCFE5E * value)
	{
		___m_CameraCutEvent_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraCutEvent_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_CameraActivatedEvent_14() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___m_CameraActivatedEvent_14)); }
	inline VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * get_m_CameraActivatedEvent_14() const { return ___m_CameraActivatedEvent_14; }
	inline VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 ** get_address_of_m_CameraActivatedEvent_14() { return &___m_CameraActivatedEvent_14; }
	inline void set_m_CameraActivatedEvent_14(VcamActivatedEvent_tFC99CBCF86077996502503BCD7019E23C7787296 * value)
	{
		___m_CameraActivatedEvent_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CameraActivatedEvent_14), (void*)value);
	}

	inline static int32_t get_offset_of_mPhysicsCoroutine_16() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mPhysicsCoroutine_16)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get_mPhysicsCoroutine_16() const { return ___mPhysicsCoroutine_16; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of_mPhysicsCoroutine_16() { return &___mPhysicsCoroutine_16; }
	inline void set_mPhysicsCoroutine_16(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		___mPhysicsCoroutine_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mPhysicsCoroutine_16), (void*)value);
	}

	inline static int32_t get_offset_of_mWaitForFixedUpdate_17() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mWaitForFixedUpdate_17)); }
	inline WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 * get_mWaitForFixedUpdate_17() const { return ___mWaitForFixedUpdate_17; }
	inline WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 ** get_address_of_mWaitForFixedUpdate_17() { return &___mWaitForFixedUpdate_17; }
	inline void set_mWaitForFixedUpdate_17(WaitForFixedUpdate_t675FCE2AEFAC5C924A4020474C997FF2CDD3F4C5 * value)
	{
		___mWaitForFixedUpdate_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mWaitForFixedUpdate_17), (void*)value);
	}

	inline static int32_t get_offset_of_mFrameStack_18() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mFrameStack_18)); }
	inline List_1_t7047292520C88A40E765EA61E967062F583F6C2A * get_mFrameStack_18() const { return ___mFrameStack_18; }
	inline List_1_t7047292520C88A40E765EA61E967062F583F6C2A ** get_address_of_mFrameStack_18() { return &___mFrameStack_18; }
	inline void set_mFrameStack_18(List_1_t7047292520C88A40E765EA61E967062F583F6C2A * value)
	{
		___mFrameStack_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mFrameStack_18), (void*)value);
	}

	inline static int32_t get_offset_of_mNextFrameId_19() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mNextFrameId_19)); }
	inline int32_t get_mNextFrameId_19() const { return ___mNextFrameId_19; }
	inline int32_t* get_address_of_mNextFrameId_19() { return &___mNextFrameId_19; }
	inline void set_mNextFrameId_19(int32_t value)
	{
		___mNextFrameId_19 = value;
	}

	inline static int32_t get_offset_of_mCurrentLiveCameras_20() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mCurrentLiveCameras_20)); }
	inline CinemachineBlend_tA1423010B6E480A1066D469A77F1CB4E2BC6925C * get_mCurrentLiveCameras_20() const { return ___mCurrentLiveCameras_20; }
	inline CinemachineBlend_tA1423010B6E480A1066D469A77F1CB4E2BC6925C ** get_address_of_mCurrentLiveCameras_20() { return &___mCurrentLiveCameras_20; }
	inline void set_mCurrentLiveCameras_20(CinemachineBlend_tA1423010B6E480A1066D469A77F1CB4E2BC6925C * value)
	{
		___mCurrentLiveCameras_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCurrentLiveCameras_20), (void*)value);
	}

	inline static int32_t get_offset_of_mActiveCameraPreviousFrame_22() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mActiveCameraPreviousFrame_22)); }
	inline RuntimeObject* get_mActiveCameraPreviousFrame_22() const { return ___mActiveCameraPreviousFrame_22; }
	inline RuntimeObject** get_address_of_mActiveCameraPreviousFrame_22() { return &___mActiveCameraPreviousFrame_22; }
	inline void set_mActiveCameraPreviousFrame_22(RuntimeObject* value)
	{
		___mActiveCameraPreviousFrame_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mActiveCameraPreviousFrame_22), (void*)value);
	}

	inline static int32_t get_offset_of_mActiveCameraPreviousFrameGameObject_23() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___mActiveCameraPreviousFrameGameObject_23)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_mActiveCameraPreviousFrameGameObject_23() const { return ___mActiveCameraPreviousFrameGameObject_23; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_mActiveCameraPreviousFrameGameObject_23() { return &___mActiveCameraPreviousFrameGameObject_23; }
	inline void set_mActiveCameraPreviousFrameGameObject_23(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___mActiveCameraPreviousFrameGameObject_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mActiveCameraPreviousFrameGameObject_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentCameraStateU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83, ___U3CCurrentCameraStateU3Ek__BackingField_24)); }
	inline CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601  get_U3CCurrentCameraStateU3Ek__BackingField_24() const { return ___U3CCurrentCameraStateU3Ek__BackingField_24; }
	inline CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * get_address_of_U3CCurrentCameraStateU3Ek__BackingField_24() { return &___U3CCurrentCameraStateU3Ek__BackingField_24; }
	inline void set_U3CCurrentCameraStateU3Ek__BackingField_24(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601  value)
	{
		___U3CCurrentCameraStateU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_24))->___mCustom0_11))->___m_Custom_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_24))->___mCustom1_12))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_24))->___mCustom2_13))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CCurrentCameraStateU3Ek__BackingField_24))->___mCustom3_14))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___U3CCurrentCameraStateU3Ek__BackingField_24))->___m_CustomOverflow_15), (void*)NULL);
		#endif
	}
};

struct CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_StaticFields
{
public:
	// Cinemachine.ICinemachineCamera Cinemachine.CinemachineBrain::mSoloCamera
	RuntimeObject* ___mSoloCamera_15;
	// UnityEngine.AnimationCurve Cinemachine.CinemachineBrain::mDefaultLinearAnimationCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___mDefaultLinearAnimationCurve_21;

public:
	inline static int32_t get_offset_of_mSoloCamera_15() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_StaticFields, ___mSoloCamera_15)); }
	inline RuntimeObject* get_mSoloCamera_15() const { return ___mSoloCamera_15; }
	inline RuntimeObject** get_address_of_mSoloCamera_15() { return &___mSoloCamera_15; }
	inline void set_mSoloCamera_15(RuntimeObject* value)
	{
		___mSoloCamera_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mSoloCamera_15), (void*)value);
	}

	inline static int32_t get_offset_of_mDefaultLinearAnimationCurve_21() { return static_cast<int32_t>(offsetof(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_StaticFields, ___mDefaultLinearAnimationCurve_21)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_mDefaultLinearAnimationCurve_21() const { return ___mDefaultLinearAnimationCurve_21; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_mDefaultLinearAnimationCurve_21() { return &___mDefaultLinearAnimationCurve_21; }
	inline void set_mDefaultLinearAnimationCurve_21(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___mDefaultLinearAnimationCurve_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mDefaultLinearAnimationCurve_21), (void*)value);
	}
};


// MoreMountains.CorgiEngine.CinemachineBrainController
struct CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Cinemachine.CinemachineBrain MoreMountains.CorgiEngine.CinemachineBrainController::_brain
	CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * ____brain_4;

public:
	inline static int32_t get_offset_of__brain_4() { return static_cast<int32_t>(offsetof(CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00, ____brain_4)); }
	inline CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * get__brain_4() const { return ____brain_4; }
	inline CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 ** get_address_of__brain_4() { return &____brain_4; }
	inline void set__brain_4(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * value)
	{
		____brain_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____brain_4), (void*)value);
	}
};


// MoreMountains.CorgiEngine.CinemachineCameraController
struct CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::<FollowsPlayer>k__BackingField
	bool ___U3CFollowsPlayerU3Ek__BackingField_4;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::FollowsAPlayer
	bool ___FollowsAPlayer_5;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::ConfineCameraToLevelBounds
	bool ___ConfineCameraToLevelBounds_6;
	// System.Single MoreMountains.CorgiEngine.CinemachineCameraController::ManualUpDownLookDistance
	float ___ManualUpDownLookDistance_7;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CinemachineCameraController::CharacterSpeed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___CharacterSpeed_8;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.CinemachineCameraController::TargetCharacter
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___TargetCharacter_9;
	// MoreMountains.CorgiEngine.CorgiController MoreMountains.CorgiEngine.CinemachineCameraController::TargetController
	CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * ___TargetController_10;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::UseOrthographicZoom
	bool ___UseOrthographicZoom_11;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CinemachineCameraController::OrthographicZoom
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___OrthographicZoom_12;
	// System.Single MoreMountains.CorgiEngine.CinemachineCameraController::InitialOrthographicZoom
	float ___InitialOrthographicZoom_13;
	// System.Single MoreMountains.CorgiEngine.CinemachineCameraController::OrthographicZoomSpeed
	float ___OrthographicZoomSpeed_14;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::UsePerspectiveZoom
	bool ___UsePerspectiveZoom_15;
	// MoreMountains.CorgiEngine.CinemachineCameraController/PerspectiveZoomMethods MoreMountains.CorgiEngine.CinemachineCameraController::PerspectiveZoomMethod
	int32_t ___PerspectiveZoomMethod_16;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CinemachineCameraController::PerspectiveZoom
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___PerspectiveZoom_17;
	// System.Single MoreMountains.CorgiEngine.CinemachineCameraController::InitialPerspectiveZoom
	float ___InitialPerspectiveZoom_18;
	// System.Single MoreMountains.CorgiEngine.CinemachineCameraController::PerspectiveZoomSpeed
	float ___PerspectiveZoomSpeed_19;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::InstantRepositionCameraOnRespawn
	bool ___InstantRepositionCameraOnRespawn_20;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::StartFollowingBtn
	bool ___StartFollowingBtn_21;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::StopFollowingBtn
	bool ___StopFollowingBtn_22;
	// Cinemachine.CinemachineVirtualCamera MoreMountains.CorgiEngine.CinemachineCameraController::_virtualCamera
	CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * ____virtualCamera_23;
	// Cinemachine.CinemachineConfiner MoreMountains.CorgiEngine.CinemachineCameraController::_confiner
	CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * ____confiner_24;
	// Cinemachine.CinemachineFramingTransposer MoreMountains.CorgiEngine.CinemachineCameraController::_framingTransposer
	CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * ____framingTransposer_25;
	// System.Single MoreMountains.CorgiEngine.CinemachineCameraController::_currentZoom
	float ____currentZoom_26;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::_initialized
	bool ____initialized_27;

public:
	inline static int32_t get_offset_of_U3CFollowsPlayerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___U3CFollowsPlayerU3Ek__BackingField_4)); }
	inline bool get_U3CFollowsPlayerU3Ek__BackingField_4() const { return ___U3CFollowsPlayerU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CFollowsPlayerU3Ek__BackingField_4() { return &___U3CFollowsPlayerU3Ek__BackingField_4; }
	inline void set_U3CFollowsPlayerU3Ek__BackingField_4(bool value)
	{
		___U3CFollowsPlayerU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_FollowsAPlayer_5() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___FollowsAPlayer_5)); }
	inline bool get_FollowsAPlayer_5() const { return ___FollowsAPlayer_5; }
	inline bool* get_address_of_FollowsAPlayer_5() { return &___FollowsAPlayer_5; }
	inline void set_FollowsAPlayer_5(bool value)
	{
		___FollowsAPlayer_5 = value;
	}

	inline static int32_t get_offset_of_ConfineCameraToLevelBounds_6() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___ConfineCameraToLevelBounds_6)); }
	inline bool get_ConfineCameraToLevelBounds_6() const { return ___ConfineCameraToLevelBounds_6; }
	inline bool* get_address_of_ConfineCameraToLevelBounds_6() { return &___ConfineCameraToLevelBounds_6; }
	inline void set_ConfineCameraToLevelBounds_6(bool value)
	{
		___ConfineCameraToLevelBounds_6 = value;
	}

	inline static int32_t get_offset_of_ManualUpDownLookDistance_7() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___ManualUpDownLookDistance_7)); }
	inline float get_ManualUpDownLookDistance_7() const { return ___ManualUpDownLookDistance_7; }
	inline float* get_address_of_ManualUpDownLookDistance_7() { return &___ManualUpDownLookDistance_7; }
	inline void set_ManualUpDownLookDistance_7(float value)
	{
		___ManualUpDownLookDistance_7 = value;
	}

	inline static int32_t get_offset_of_CharacterSpeed_8() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___CharacterSpeed_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_CharacterSpeed_8() const { return ___CharacterSpeed_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_CharacterSpeed_8() { return &___CharacterSpeed_8; }
	inline void set_CharacterSpeed_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___CharacterSpeed_8 = value;
	}

	inline static int32_t get_offset_of_TargetCharacter_9() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___TargetCharacter_9)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get_TargetCharacter_9() const { return ___TargetCharacter_9; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of_TargetCharacter_9() { return &___TargetCharacter_9; }
	inline void set_TargetCharacter_9(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		___TargetCharacter_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetCharacter_9), (void*)value);
	}

	inline static int32_t get_offset_of_TargetController_10() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___TargetController_10)); }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * get_TargetController_10() const { return ___TargetController_10; }
	inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 ** get_address_of_TargetController_10() { return &___TargetController_10; }
	inline void set_TargetController_10(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * value)
	{
		___TargetController_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetController_10), (void*)value);
	}

	inline static int32_t get_offset_of_UseOrthographicZoom_11() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___UseOrthographicZoom_11)); }
	inline bool get_UseOrthographicZoom_11() const { return ___UseOrthographicZoom_11; }
	inline bool* get_address_of_UseOrthographicZoom_11() { return &___UseOrthographicZoom_11; }
	inline void set_UseOrthographicZoom_11(bool value)
	{
		___UseOrthographicZoom_11 = value;
	}

	inline static int32_t get_offset_of_OrthographicZoom_12() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___OrthographicZoom_12)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_OrthographicZoom_12() const { return ___OrthographicZoom_12; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_OrthographicZoom_12() { return &___OrthographicZoom_12; }
	inline void set_OrthographicZoom_12(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___OrthographicZoom_12 = value;
	}

	inline static int32_t get_offset_of_InitialOrthographicZoom_13() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___InitialOrthographicZoom_13)); }
	inline float get_InitialOrthographicZoom_13() const { return ___InitialOrthographicZoom_13; }
	inline float* get_address_of_InitialOrthographicZoom_13() { return &___InitialOrthographicZoom_13; }
	inline void set_InitialOrthographicZoom_13(float value)
	{
		___InitialOrthographicZoom_13 = value;
	}

	inline static int32_t get_offset_of_OrthographicZoomSpeed_14() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___OrthographicZoomSpeed_14)); }
	inline float get_OrthographicZoomSpeed_14() const { return ___OrthographicZoomSpeed_14; }
	inline float* get_address_of_OrthographicZoomSpeed_14() { return &___OrthographicZoomSpeed_14; }
	inline void set_OrthographicZoomSpeed_14(float value)
	{
		___OrthographicZoomSpeed_14 = value;
	}

	inline static int32_t get_offset_of_UsePerspectiveZoom_15() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___UsePerspectiveZoom_15)); }
	inline bool get_UsePerspectiveZoom_15() const { return ___UsePerspectiveZoom_15; }
	inline bool* get_address_of_UsePerspectiveZoom_15() { return &___UsePerspectiveZoom_15; }
	inline void set_UsePerspectiveZoom_15(bool value)
	{
		___UsePerspectiveZoom_15 = value;
	}

	inline static int32_t get_offset_of_PerspectiveZoomMethod_16() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___PerspectiveZoomMethod_16)); }
	inline int32_t get_PerspectiveZoomMethod_16() const { return ___PerspectiveZoomMethod_16; }
	inline int32_t* get_address_of_PerspectiveZoomMethod_16() { return &___PerspectiveZoomMethod_16; }
	inline void set_PerspectiveZoomMethod_16(int32_t value)
	{
		___PerspectiveZoomMethod_16 = value;
	}

	inline static int32_t get_offset_of_PerspectiveZoom_17() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___PerspectiveZoom_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_PerspectiveZoom_17() const { return ___PerspectiveZoom_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_PerspectiveZoom_17() { return &___PerspectiveZoom_17; }
	inline void set_PerspectiveZoom_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___PerspectiveZoom_17 = value;
	}

	inline static int32_t get_offset_of_InitialPerspectiveZoom_18() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___InitialPerspectiveZoom_18)); }
	inline float get_InitialPerspectiveZoom_18() const { return ___InitialPerspectiveZoom_18; }
	inline float* get_address_of_InitialPerspectiveZoom_18() { return &___InitialPerspectiveZoom_18; }
	inline void set_InitialPerspectiveZoom_18(float value)
	{
		___InitialPerspectiveZoom_18 = value;
	}

	inline static int32_t get_offset_of_PerspectiveZoomSpeed_19() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___PerspectiveZoomSpeed_19)); }
	inline float get_PerspectiveZoomSpeed_19() const { return ___PerspectiveZoomSpeed_19; }
	inline float* get_address_of_PerspectiveZoomSpeed_19() { return &___PerspectiveZoomSpeed_19; }
	inline void set_PerspectiveZoomSpeed_19(float value)
	{
		___PerspectiveZoomSpeed_19 = value;
	}

	inline static int32_t get_offset_of_InstantRepositionCameraOnRespawn_20() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___InstantRepositionCameraOnRespawn_20)); }
	inline bool get_InstantRepositionCameraOnRespawn_20() const { return ___InstantRepositionCameraOnRespawn_20; }
	inline bool* get_address_of_InstantRepositionCameraOnRespawn_20() { return &___InstantRepositionCameraOnRespawn_20; }
	inline void set_InstantRepositionCameraOnRespawn_20(bool value)
	{
		___InstantRepositionCameraOnRespawn_20 = value;
	}

	inline static int32_t get_offset_of_StartFollowingBtn_21() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___StartFollowingBtn_21)); }
	inline bool get_StartFollowingBtn_21() const { return ___StartFollowingBtn_21; }
	inline bool* get_address_of_StartFollowingBtn_21() { return &___StartFollowingBtn_21; }
	inline void set_StartFollowingBtn_21(bool value)
	{
		___StartFollowingBtn_21 = value;
	}

	inline static int32_t get_offset_of_StopFollowingBtn_22() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ___StopFollowingBtn_22)); }
	inline bool get_StopFollowingBtn_22() const { return ___StopFollowingBtn_22; }
	inline bool* get_address_of_StopFollowingBtn_22() { return &___StopFollowingBtn_22; }
	inline void set_StopFollowingBtn_22(bool value)
	{
		___StopFollowingBtn_22 = value;
	}

	inline static int32_t get_offset_of__virtualCamera_23() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ____virtualCamera_23)); }
	inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * get__virtualCamera_23() const { return ____virtualCamera_23; }
	inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C ** get_address_of__virtualCamera_23() { return &____virtualCamera_23; }
	inline void set__virtualCamera_23(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * value)
	{
		____virtualCamera_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____virtualCamera_23), (void*)value);
	}

	inline static int32_t get_offset_of__confiner_24() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ____confiner_24)); }
	inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * get__confiner_24() const { return ____confiner_24; }
	inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D ** get_address_of__confiner_24() { return &____confiner_24; }
	inline void set__confiner_24(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * value)
	{
		____confiner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____confiner_24), (void*)value);
	}

	inline static int32_t get_offset_of__framingTransposer_25() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ____framingTransposer_25)); }
	inline CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * get__framingTransposer_25() const { return ____framingTransposer_25; }
	inline CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 ** get_address_of__framingTransposer_25() { return &____framingTransposer_25; }
	inline void set__framingTransposer_25(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * value)
	{
		____framingTransposer_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____framingTransposer_25), (void*)value);
	}

	inline static int32_t get_offset_of__currentZoom_26() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ____currentZoom_26)); }
	inline float get__currentZoom_26() const { return ____currentZoom_26; }
	inline float* get_address_of__currentZoom_26() { return &____currentZoom_26; }
	inline void set__currentZoom_26(float value)
	{
		____currentZoom_26 = value;
	}

	inline static int32_t get_offset_of__initialized_27() { return static_cast<int32_t>(offsetof(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1, ____initialized_27)); }
	inline bool get__initialized_27() const { return ____initialized_27; }
	inline bool* get_address_of__initialized_27() { return &____initialized_27; }
	inline void set__initialized_27(bool value)
	{
		____initialized_27 = value;
	}
};


// Cinemachine.CinemachineComponentBase
struct CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::m_vcamOwner
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___m_vcamOwner_5;
	// UnityEngine.Transform Cinemachine.CinemachineComponentBase::mCachedFollowTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___mCachedFollowTarget_6;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::mCachedFollowTargetVcam
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___mCachedFollowTargetVcam_7;
	// Cinemachine.ICinemachineTargetGroup Cinemachine.CinemachineComponentBase::mCachedFollowTargetGroup
	RuntimeObject* ___mCachedFollowTargetGroup_8;
	// UnityEngine.Transform Cinemachine.CinemachineComponentBase::mCachedLookAtTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___mCachedLookAtTarget_9;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineComponentBase::mCachedLookAtTargetVcam
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___mCachedLookAtTargetVcam_10;
	// Cinemachine.ICinemachineTargetGroup Cinemachine.CinemachineComponentBase::mCachedLookAtTargetGroup
	RuntimeObject* ___mCachedLookAtTargetGroup_11;

public:
	inline static int32_t get_offset_of_m_vcamOwner_5() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___m_vcamOwner_5)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_m_vcamOwner_5() const { return ___m_vcamOwner_5; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_m_vcamOwner_5() { return &___m_vcamOwner_5; }
	inline void set_m_vcamOwner_5(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___m_vcamOwner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vcamOwner_5), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedFollowTarget_6() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___mCachedFollowTarget_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_mCachedFollowTarget_6() const { return ___mCachedFollowTarget_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_mCachedFollowTarget_6() { return &___mCachedFollowTarget_6; }
	inline void set_mCachedFollowTarget_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___mCachedFollowTarget_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedFollowTarget_6), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedFollowTargetVcam_7() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___mCachedFollowTargetVcam_7)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_mCachedFollowTargetVcam_7() const { return ___mCachedFollowTargetVcam_7; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_mCachedFollowTargetVcam_7() { return &___mCachedFollowTargetVcam_7; }
	inline void set_mCachedFollowTargetVcam_7(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___mCachedFollowTargetVcam_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedFollowTargetVcam_7), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedFollowTargetGroup_8() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___mCachedFollowTargetGroup_8)); }
	inline RuntimeObject* get_mCachedFollowTargetGroup_8() const { return ___mCachedFollowTargetGroup_8; }
	inline RuntimeObject** get_address_of_mCachedFollowTargetGroup_8() { return &___mCachedFollowTargetGroup_8; }
	inline void set_mCachedFollowTargetGroup_8(RuntimeObject* value)
	{
		___mCachedFollowTargetGroup_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedFollowTargetGroup_8), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTarget_9() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___mCachedLookAtTarget_9)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_mCachedLookAtTarget_9() const { return ___mCachedLookAtTarget_9; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_mCachedLookAtTarget_9() { return &___mCachedLookAtTarget_9; }
	inline void set_mCachedLookAtTarget_9(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___mCachedLookAtTarget_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTarget_9), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTargetVcam_10() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___mCachedLookAtTargetVcam_10)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_mCachedLookAtTargetVcam_10() const { return ___mCachedLookAtTargetVcam_10; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_mCachedLookAtTargetVcam_10() { return &___mCachedLookAtTargetVcam_10; }
	inline void set_mCachedLookAtTargetVcam_10(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___mCachedLookAtTargetVcam_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTargetVcam_10), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTargetGroup_11() { return static_cast<int32_t>(offsetof(CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D, ___mCachedLookAtTargetGroup_11)); }
	inline RuntimeObject* get_mCachedLookAtTargetGroup_11() const { return ___mCachedLookAtTargetGroup_11; }
	inline RuntimeObject** get_address_of_mCachedLookAtTargetGroup_11() { return &___mCachedLookAtTargetGroup_11; }
	inline void set_mCachedLookAtTargetGroup_11(RuntimeObject* value)
	{
		___mCachedLookAtTargetGroup_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTargetGroup_11), (void*)value);
	}
};


// Cinemachine.CinemachineExtension
struct CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineExtension::m_vcamOwner
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___m_vcamOwner_5;
	// System.Collections.Generic.Dictionary`2<Cinemachine.ICinemachineCamera,System.Object> Cinemachine.CinemachineExtension::mExtraState
	Dictionary_2_tA56D153C1EABC9FB802D4F354B4E536F249FF4FE * ___mExtraState_6;

public:
	inline static int32_t get_offset_of_m_vcamOwner_5() { return static_cast<int32_t>(offsetof(CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422, ___m_vcamOwner_5)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_m_vcamOwner_5() const { return ___m_vcamOwner_5; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_m_vcamOwner_5() { return &___m_vcamOwner_5; }
	inline void set_m_vcamOwner_5(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___m_vcamOwner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_vcamOwner_5), (void*)value);
	}

	inline static int32_t get_offset_of_mExtraState_6() { return static_cast<int32_t>(offsetof(CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422, ___mExtraState_6)); }
	inline Dictionary_2_tA56D153C1EABC9FB802D4F354B4E536F249FF4FE * get_mExtraState_6() const { return ___mExtraState_6; }
	inline Dictionary_2_tA56D153C1EABC9FB802D4F354B4E536F249FF4FE ** get_address_of_mExtraState_6() { return &___mExtraState_6; }
	inline void set_mExtraState_6(Dictionary_2_tA56D153C1EABC9FB802D4F354B4E536F249FF4FE * value)
	{
		___mExtraState_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mExtraState_6), (void*)value);
	}
};


// Cinemachine.CinemachineVirtualCameraBase
struct CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String[] Cinemachine.CinemachineVirtualCameraBase::m_ExcludedPropertiesInInspector
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___m_ExcludedPropertiesInInspector_4;
	// Cinemachine.CinemachineCore/Stage[] Cinemachine.CinemachineVirtualCameraBase::m_LockStageInInspector
	StageU5BU5D_t605193BD653BA57911AFE8A2D01B1D799F7BB949* ___m_LockStageInInspector_5;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_ValidatingStreamVersion
	int32_t ___m_ValidatingStreamVersion_6;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_OnValidateCalled
	bool ___m_OnValidateCalled_7;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_StreamingVersion
	int32_t ___m_StreamingVersion_8;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_Priority
	int32_t ___m_Priority_9;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_ActivationId
	int32_t ___m_ActivationId_10;
	// System.Single Cinemachine.CinemachineVirtualCameraBase::<FollowTargetAttachment>k__BackingField
	float ___U3CFollowTargetAttachmentU3Ek__BackingField_11;
	// System.Single Cinemachine.CinemachineVirtualCameraBase::<LookAtTargetAttachment>k__BackingField
	float ___U3CLookAtTargetAttachmentU3Ek__BackingField_12;
	// Cinemachine.CinemachineVirtualCameraBase/StandbyUpdateMode Cinemachine.CinemachineVirtualCameraBase::m_StandbyUpdate
	int32_t ___m_StandbyUpdate_13;
	// System.Collections.Generic.List`1<Cinemachine.CinemachineExtension> Cinemachine.CinemachineVirtualCameraBase::<mExtensions>k__BackingField
	List_1_t13875FE3163CFA961C09134FFAA2E0903229691A * ___U3CmExtensionsU3Ek__BackingField_14;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::<PreviousStateIsValid>k__BackingField
	bool ___U3CPreviousStateIsValidU3Ek__BackingField_15;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::m_WasStarted
	bool ___m_WasStarted_16;
	// System.Boolean Cinemachine.CinemachineVirtualCameraBase::mSlaveStatusUpdated
	bool ___mSlaveStatusUpdated_17;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCameraBase::m_parentVcam
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___m_parentVcam_18;
	// System.Int32 Cinemachine.CinemachineVirtualCameraBase::m_QueuePriority
	int32_t ___m_QueuePriority_19;

public:
	inline static int32_t get_offset_of_m_ExcludedPropertiesInInspector_4() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_ExcludedPropertiesInInspector_4)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_m_ExcludedPropertiesInInspector_4() const { return ___m_ExcludedPropertiesInInspector_4; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_m_ExcludedPropertiesInInspector_4() { return &___m_ExcludedPropertiesInInspector_4; }
	inline void set_m_ExcludedPropertiesInInspector_4(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___m_ExcludedPropertiesInInspector_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExcludedPropertiesInInspector_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_LockStageInInspector_5() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_LockStageInInspector_5)); }
	inline StageU5BU5D_t605193BD653BA57911AFE8A2D01B1D799F7BB949* get_m_LockStageInInspector_5() const { return ___m_LockStageInInspector_5; }
	inline StageU5BU5D_t605193BD653BA57911AFE8A2D01B1D799F7BB949** get_address_of_m_LockStageInInspector_5() { return &___m_LockStageInInspector_5; }
	inline void set_m_LockStageInInspector_5(StageU5BU5D_t605193BD653BA57911AFE8A2D01B1D799F7BB949* value)
	{
		___m_LockStageInInspector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LockStageInInspector_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ValidatingStreamVersion_6() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_ValidatingStreamVersion_6)); }
	inline int32_t get_m_ValidatingStreamVersion_6() const { return ___m_ValidatingStreamVersion_6; }
	inline int32_t* get_address_of_m_ValidatingStreamVersion_6() { return &___m_ValidatingStreamVersion_6; }
	inline void set_m_ValidatingStreamVersion_6(int32_t value)
	{
		___m_ValidatingStreamVersion_6 = value;
	}

	inline static int32_t get_offset_of_m_OnValidateCalled_7() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_OnValidateCalled_7)); }
	inline bool get_m_OnValidateCalled_7() const { return ___m_OnValidateCalled_7; }
	inline bool* get_address_of_m_OnValidateCalled_7() { return &___m_OnValidateCalled_7; }
	inline void set_m_OnValidateCalled_7(bool value)
	{
		___m_OnValidateCalled_7 = value;
	}

	inline static int32_t get_offset_of_m_StreamingVersion_8() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_StreamingVersion_8)); }
	inline int32_t get_m_StreamingVersion_8() const { return ___m_StreamingVersion_8; }
	inline int32_t* get_address_of_m_StreamingVersion_8() { return &___m_StreamingVersion_8; }
	inline void set_m_StreamingVersion_8(int32_t value)
	{
		___m_StreamingVersion_8 = value;
	}

	inline static int32_t get_offset_of_m_Priority_9() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_Priority_9)); }
	inline int32_t get_m_Priority_9() const { return ___m_Priority_9; }
	inline int32_t* get_address_of_m_Priority_9() { return &___m_Priority_9; }
	inline void set_m_Priority_9(int32_t value)
	{
		___m_Priority_9 = value;
	}

	inline static int32_t get_offset_of_m_ActivationId_10() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_ActivationId_10)); }
	inline int32_t get_m_ActivationId_10() const { return ___m_ActivationId_10; }
	inline int32_t* get_address_of_m_ActivationId_10() { return &___m_ActivationId_10; }
	inline void set_m_ActivationId_10(int32_t value)
	{
		___m_ActivationId_10 = value;
	}

	inline static int32_t get_offset_of_U3CFollowTargetAttachmentU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___U3CFollowTargetAttachmentU3Ek__BackingField_11)); }
	inline float get_U3CFollowTargetAttachmentU3Ek__BackingField_11() const { return ___U3CFollowTargetAttachmentU3Ek__BackingField_11; }
	inline float* get_address_of_U3CFollowTargetAttachmentU3Ek__BackingField_11() { return &___U3CFollowTargetAttachmentU3Ek__BackingField_11; }
	inline void set_U3CFollowTargetAttachmentU3Ek__BackingField_11(float value)
	{
		___U3CFollowTargetAttachmentU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CLookAtTargetAttachmentU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___U3CLookAtTargetAttachmentU3Ek__BackingField_12)); }
	inline float get_U3CLookAtTargetAttachmentU3Ek__BackingField_12() const { return ___U3CLookAtTargetAttachmentU3Ek__BackingField_12; }
	inline float* get_address_of_U3CLookAtTargetAttachmentU3Ek__BackingField_12() { return &___U3CLookAtTargetAttachmentU3Ek__BackingField_12; }
	inline void set_U3CLookAtTargetAttachmentU3Ek__BackingField_12(float value)
	{
		___U3CLookAtTargetAttachmentU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_m_StandbyUpdate_13() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_StandbyUpdate_13)); }
	inline int32_t get_m_StandbyUpdate_13() const { return ___m_StandbyUpdate_13; }
	inline int32_t* get_address_of_m_StandbyUpdate_13() { return &___m_StandbyUpdate_13; }
	inline void set_m_StandbyUpdate_13(int32_t value)
	{
		___m_StandbyUpdate_13 = value;
	}

	inline static int32_t get_offset_of_U3CmExtensionsU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___U3CmExtensionsU3Ek__BackingField_14)); }
	inline List_1_t13875FE3163CFA961C09134FFAA2E0903229691A * get_U3CmExtensionsU3Ek__BackingField_14() const { return ___U3CmExtensionsU3Ek__BackingField_14; }
	inline List_1_t13875FE3163CFA961C09134FFAA2E0903229691A ** get_address_of_U3CmExtensionsU3Ek__BackingField_14() { return &___U3CmExtensionsU3Ek__BackingField_14; }
	inline void set_U3CmExtensionsU3Ek__BackingField_14(List_1_t13875FE3163CFA961C09134FFAA2E0903229691A * value)
	{
		___U3CmExtensionsU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmExtensionsU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPreviousStateIsValidU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___U3CPreviousStateIsValidU3Ek__BackingField_15)); }
	inline bool get_U3CPreviousStateIsValidU3Ek__BackingField_15() const { return ___U3CPreviousStateIsValidU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CPreviousStateIsValidU3Ek__BackingField_15() { return &___U3CPreviousStateIsValidU3Ek__BackingField_15; }
	inline void set_U3CPreviousStateIsValidU3Ek__BackingField_15(bool value)
	{
		___U3CPreviousStateIsValidU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_m_WasStarted_16() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_WasStarted_16)); }
	inline bool get_m_WasStarted_16() const { return ___m_WasStarted_16; }
	inline bool* get_address_of_m_WasStarted_16() { return &___m_WasStarted_16; }
	inline void set_m_WasStarted_16(bool value)
	{
		___m_WasStarted_16 = value;
	}

	inline static int32_t get_offset_of_mSlaveStatusUpdated_17() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___mSlaveStatusUpdated_17)); }
	inline bool get_mSlaveStatusUpdated_17() const { return ___mSlaveStatusUpdated_17; }
	inline bool* get_address_of_mSlaveStatusUpdated_17() { return &___mSlaveStatusUpdated_17; }
	inline void set_mSlaveStatusUpdated_17(bool value)
	{
		___mSlaveStatusUpdated_17 = value;
	}

	inline static int32_t get_offset_of_m_parentVcam_18() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_parentVcam_18)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_m_parentVcam_18() const { return ___m_parentVcam_18; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_m_parentVcam_18() { return &___m_parentVcam_18; }
	inline void set_m_parentVcam_18(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___m_parentVcam_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_parentVcam_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_QueuePriority_19() { return static_cast<int32_t>(offsetof(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5, ___m_QueuePriority_19)); }
	inline int32_t get_m_QueuePriority_19() const { return ___m_QueuePriority_19; }
	inline int32_t* get_address_of_m_QueuePriority_19() { return &___m_QueuePriority_19; }
	inline void set_m_QueuePriority_19(int32_t value)
	{
		___m_QueuePriority_19 = value;
	}
};


// MoreMountains.CorgiEngine.CorgiController
struct CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.CorgiEngine.CorgiControllerState MoreMountains.CorgiEngine.CorgiController::<State>k__BackingField
	CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 * ___U3CStateU3Ek__BackingField_4;
	// MoreMountains.CorgiEngine.CorgiControllerParameters MoreMountains.CorgiEngine.CorgiController::DefaultParameters
	CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * ___DefaultParameters_5;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::PlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___PlatformMask_6;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::MovingPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___MovingPlatformMask_7;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::OneWayPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___OneWayPlatformMask_8;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::MovingOneWayPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___MovingOneWayPlatformMask_9;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::MidHeightOneWayPlatformMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___MidHeightOneWayPlatformMask_10;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::StairsMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___StairsMask_11;
	// MoreMountains.CorgiEngine.CorgiController/DetachmentMethods MoreMountains.CorgiEngine.CorgiController::DetachmentMethod
	int32_t ___DetachmentMethod_12;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::SafeSetTransform
	bool ___SafeSetTransform_13;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::AutomaticallySetPhysicsSettings
	bool ___AutomaticallySetPhysicsSettings_14;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::StandingOn
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___StandingOn_15;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::<StandingOnLastFrame>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CStandingOnLastFrameU3Ek__BackingField_16;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.CorgiController::<StandingOnCollider>k__BackingField
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___U3CStandingOnColliderU3Ek__BackingField_17;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::<ForcesApplied>k__BackingField
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___U3CForcesAppliedU3Ek__BackingField_18;
	// UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::<CurrentWallCollider>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3CCurrentWallColliderU3Ek__BackingField_19;
	// MoreMountains.CorgiEngine.CorgiController/UpdateModes MoreMountains.CorgiEngine.CorgiController::UpdateMode
	int32_t ___UpdateMode_20;
	// System.Int32 MoreMountains.CorgiEngine.CorgiController::NumberOfHorizontalRays
	int32_t ___NumberOfHorizontalRays_21;
	// System.Int32 MoreMountains.CorgiEngine.CorgiController::NumberOfVerticalRays
	int32_t ___NumberOfVerticalRays_22;
	// System.Single MoreMountains.CorgiEngine.CorgiController::RayOffsetHorizontal
	float ___RayOffsetHorizontal_23;
	// System.Single MoreMountains.CorgiEngine.CorgiController::RayOffsetVertical
	float ___RayOffsetVertical_24;
	// System.Single MoreMountains.CorgiEngine.CorgiController::CrouchedRaycastLengthMultiplier
	float ___CrouchedRaycastLengthMultiplier_25;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::CastRaysOnBothSides
	bool ___CastRaysOnBothSides_26;
	// System.Single MoreMountains.CorgiEngine.CorgiController::DistanceToTheGroundRayMaximumLength
	float ___DistanceToTheGroundRayMaximumLength_27;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::PerformSafetyBoxcast
	bool ___PerformSafetyBoxcast_28;
	// System.Single MoreMountains.CorgiEngine.CorgiController::OnMovingPlatformRaycastLengthMultiplier
	float ___OnMovingPlatformRaycastLengthMultiplier_29;
	// System.Single MoreMountains.CorgiEngine.CorgiController::ObstacleHeightTolerance
	float ___ObstacleHeightTolerance_30;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::StickToSlopes
	bool ___StickToSlopes_31;
	// System.Single MoreMountains.CorgiEngine.CorgiController::StickyRaycastLength
	float ___StickyRaycastLength_32;
	// System.Single MoreMountains.CorgiEngine.CorgiController::StickToSlopesOffsetY
	float ___StickToSlopesOffsetY_33;
	// System.Single MoreMountains.CorgiEngine.CorgiController::TimeAirborne
	float ___TimeAirborne_34;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::AutomaticGravitySettings
	bool ___AutomaticGravitySettings_35;
	// MoreMountains.CorgiEngine.CorgiControllerParameters MoreMountains.CorgiEngine.CorgiController::_overrideParameters
	CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * ____overrideParameters_36;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_speed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____speed_37;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_friction
	float ____friction_38;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_fallSlowFactor
	float ____fallSlowFactor_39;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_currentGravity
	float ____currentGravity_40;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_externalForce
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____externalForce_41;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_newPosition
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____newPosition_42;
	// UnityEngine.Transform MoreMountains.CorgiEngine.CorgiController::_transform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____transform_43;
	// UnityEngine.BoxCollider2D MoreMountains.CorgiEngine.CorgiController::_boxCollider
	BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * ____boxCollider_44;
	// MoreMountains.CorgiEngine.CharacterGravity MoreMountains.CorgiEngine.CorgiController::_characterGravity
	CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * ____characterGravity_45;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_platformMaskSave
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____platformMaskSave_46;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_raysBelowLayerMaskPlatforms
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____raysBelowLayerMaskPlatforms_47;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_raysBelowLayerMaskPlatformsWithoutOneWay
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____raysBelowLayerMaskPlatformsWithoutOneWay_48;
	// UnityEngine.LayerMask MoreMountains.CorgiEngine.CorgiController::_raysBelowLayerMaskPlatformsWithoutMidHeight
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ____raysBelowLayerMaskPlatformsWithoutMidHeight_49;
	// System.Int32 MoreMountains.CorgiEngine.CorgiController::_savedBelowLayer
	int32_t ____savedBelowLayer_50;
	// MoreMountains.Tools.MMPathMovement MoreMountains.CorgiEngine.CorgiController::_movingPlatform
	MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * ____movingPlatform_51;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_movingPlatformCurrentGravity
	float ____movingPlatformCurrentGravity_52;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_gravityActive
	bool ____gravityActive_53;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.CorgiController::_ignoredCollider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____ignoredCollider_54;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_collisionsOnWithStairs
	bool ____collisionsOnWithStairs_55;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_originalColliderSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____originalColliderSize_58;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_originalColliderOffset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____originalColliderOffset_59;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_originalSizeRaycastOrigin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____originalSizeRaycastOrigin_60;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_crossBelowSlopeAngle
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____crossBelowSlopeAngle_61;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_sideHitsStorage
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____sideHitsStorage_62;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_belowHitsStorage
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____belowHitsStorage_63;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_aboveHitsStorage
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____aboveHitsStorage_64;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_stickRaycastLeft
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____stickRaycastLeft_65;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_stickRaycastRight
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____stickRaycastRight_66;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_stickRaycast
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____stickRaycast_67;
	// UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.CorgiController::_distanceToTheGroundRaycast
	RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  ____distanceToTheGroundRaycast_68;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_movementDirection
	float ____movementDirection_69;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_storedMovementDirection
	float ____storedMovementDirection_70;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_horizontalRayCastFromBottom
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____horizontalRayCastFromBottom_72;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_horizontalRayCastToTop
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____horizontalRayCastToTop_73;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_verticalRayCastFromLeft
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____verticalRayCastFromLeft_74;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_verticalRayCastToRight
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____verticalRayCastToRight_75;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_aboveRayCastStart
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____aboveRayCastStart_76;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_aboveRayCastEnd
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____aboveRayCastEnd_77;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_rayCastOrigin
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____rayCastOrigin_78;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderBottomCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderBottomCenterPosition_79;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderLeftCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderLeftCenterPosition_80;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderRightCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderRightCenterPosition_81;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::_colliderTopCenterPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____colliderTopCenterPosition_82;
	// MoreMountains.Tools.MMPathMovement MoreMountains.CorgiEngine.CorgiController::_movingPlatformTest
	MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * ____movingPlatformTest_83;
	// MoreMountains.CorgiEngine.SurfaceModifier MoreMountains.CorgiEngine.CorgiController::_frictionTest
	SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 * ____frictionTest_84;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_update
	bool ____update_85;
	// UnityEngine.RaycastHit2D[] MoreMountains.CorgiEngine.CorgiController::_raycastNonAlloc
	RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* ____raycastNonAlloc_86;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsTopLeftCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsTopLeftCorner_87;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsBottomLeftCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsBottomLeftCorner_88;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsTopRightCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsTopRightCorner_89;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsBottomRightCorner
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsBottomRightCorner_90;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_boundsCenter
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____boundsCenter_91;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_bounds
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____bounds_92;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_boundsWidth
	float ____boundsWidth_93;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_boundsHeight
	float ____boundsHeight_94;
	// System.Single MoreMountains.CorgiEngine.CorgiController::_distanceToTheGround
	float ____distanceToTheGround_95;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::_worldSpeed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____worldSpeed_96;
	// System.Collections.Generic.List`1<UnityEngine.RaycastHit2D> MoreMountains.CorgiEngine.CorgiController::_contactList
	List_1_t3926283FA9AE49778D95220056CEBFB01D034379 * ____contactList_97;
	// System.Boolean MoreMountains.CorgiEngine.CorgiController::_shouldComputeNewSpeed
	bool ____shouldComputeNewSpeed_98;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CStateU3Ek__BackingField_4)); }
	inline CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 * get_U3CStateU3Ek__BackingField_4() const { return ___U3CStateU3Ek__BackingField_4; }
	inline CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 ** get_address_of_U3CStateU3Ek__BackingField_4() { return &___U3CStateU3Ek__BackingField_4; }
	inline void set_U3CStateU3Ek__BackingField_4(CorgiControllerState_t5DA9852351129BEF2E14F5279797A03D4CB24364 * value)
	{
		___U3CStateU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultParameters_5() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___DefaultParameters_5)); }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * get_DefaultParameters_5() const { return ___DefaultParameters_5; }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 ** get_address_of_DefaultParameters_5() { return &___DefaultParameters_5; }
	inline void set_DefaultParameters_5(CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * value)
	{
		___DefaultParameters_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultParameters_5), (void*)value);
	}

	inline static int32_t get_offset_of_PlatformMask_6() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___PlatformMask_6)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_PlatformMask_6() const { return ___PlatformMask_6; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_PlatformMask_6() { return &___PlatformMask_6; }
	inline void set_PlatformMask_6(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___PlatformMask_6 = value;
	}

	inline static int32_t get_offset_of_MovingPlatformMask_7() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___MovingPlatformMask_7)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_MovingPlatformMask_7() const { return ___MovingPlatformMask_7; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_MovingPlatformMask_7() { return &___MovingPlatformMask_7; }
	inline void set_MovingPlatformMask_7(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___MovingPlatformMask_7 = value;
	}

	inline static int32_t get_offset_of_OneWayPlatformMask_8() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___OneWayPlatformMask_8)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_OneWayPlatformMask_8() const { return ___OneWayPlatformMask_8; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_OneWayPlatformMask_8() { return &___OneWayPlatformMask_8; }
	inline void set_OneWayPlatformMask_8(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___OneWayPlatformMask_8 = value;
	}

	inline static int32_t get_offset_of_MovingOneWayPlatformMask_9() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___MovingOneWayPlatformMask_9)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_MovingOneWayPlatformMask_9() const { return ___MovingOneWayPlatformMask_9; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_MovingOneWayPlatformMask_9() { return &___MovingOneWayPlatformMask_9; }
	inline void set_MovingOneWayPlatformMask_9(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___MovingOneWayPlatformMask_9 = value;
	}

	inline static int32_t get_offset_of_MidHeightOneWayPlatformMask_10() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___MidHeightOneWayPlatformMask_10)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_MidHeightOneWayPlatformMask_10() const { return ___MidHeightOneWayPlatformMask_10; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_MidHeightOneWayPlatformMask_10() { return &___MidHeightOneWayPlatformMask_10; }
	inline void set_MidHeightOneWayPlatformMask_10(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___MidHeightOneWayPlatformMask_10 = value;
	}

	inline static int32_t get_offset_of_StairsMask_11() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StairsMask_11)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_StairsMask_11() const { return ___StairsMask_11; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_StairsMask_11() { return &___StairsMask_11; }
	inline void set_StairsMask_11(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___StairsMask_11 = value;
	}

	inline static int32_t get_offset_of_DetachmentMethod_12() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___DetachmentMethod_12)); }
	inline int32_t get_DetachmentMethod_12() const { return ___DetachmentMethod_12; }
	inline int32_t* get_address_of_DetachmentMethod_12() { return &___DetachmentMethod_12; }
	inline void set_DetachmentMethod_12(int32_t value)
	{
		___DetachmentMethod_12 = value;
	}

	inline static int32_t get_offset_of_SafeSetTransform_13() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___SafeSetTransform_13)); }
	inline bool get_SafeSetTransform_13() const { return ___SafeSetTransform_13; }
	inline bool* get_address_of_SafeSetTransform_13() { return &___SafeSetTransform_13; }
	inline void set_SafeSetTransform_13(bool value)
	{
		___SafeSetTransform_13 = value;
	}

	inline static int32_t get_offset_of_AutomaticallySetPhysicsSettings_14() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___AutomaticallySetPhysicsSettings_14)); }
	inline bool get_AutomaticallySetPhysicsSettings_14() const { return ___AutomaticallySetPhysicsSettings_14; }
	inline bool* get_address_of_AutomaticallySetPhysicsSettings_14() { return &___AutomaticallySetPhysicsSettings_14; }
	inline void set_AutomaticallySetPhysicsSettings_14(bool value)
	{
		___AutomaticallySetPhysicsSettings_14 = value;
	}

	inline static int32_t get_offset_of_StandingOn_15() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StandingOn_15)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_StandingOn_15() const { return ___StandingOn_15; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_StandingOn_15() { return &___StandingOn_15; }
	inline void set_StandingOn_15(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___StandingOn_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___StandingOn_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStandingOnLastFrameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CStandingOnLastFrameU3Ek__BackingField_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CStandingOnLastFrameU3Ek__BackingField_16() const { return ___U3CStandingOnLastFrameU3Ek__BackingField_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CStandingOnLastFrameU3Ek__BackingField_16() { return &___U3CStandingOnLastFrameU3Ek__BackingField_16; }
	inline void set_U3CStandingOnLastFrameU3Ek__BackingField_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CStandingOnLastFrameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStandingOnLastFrameU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStandingOnColliderU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CStandingOnColliderU3Ek__BackingField_17)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_U3CStandingOnColliderU3Ek__BackingField_17() const { return ___U3CStandingOnColliderU3Ek__BackingField_17; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_U3CStandingOnColliderU3Ek__BackingField_17() { return &___U3CStandingOnColliderU3Ek__BackingField_17; }
	inline void set_U3CStandingOnColliderU3Ek__BackingField_17(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___U3CStandingOnColliderU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStandingOnColliderU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CForcesAppliedU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CForcesAppliedU3Ek__BackingField_18)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_U3CForcesAppliedU3Ek__BackingField_18() const { return ___U3CForcesAppliedU3Ek__BackingField_18; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_U3CForcesAppliedU3Ek__BackingField_18() { return &___U3CForcesAppliedU3Ek__BackingField_18; }
	inline void set_U3CForcesAppliedU3Ek__BackingField_18(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___U3CForcesAppliedU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentWallColliderU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___U3CCurrentWallColliderU3Ek__BackingField_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3CCurrentWallColliderU3Ek__BackingField_19() const { return ___U3CCurrentWallColliderU3Ek__BackingField_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3CCurrentWallColliderU3Ek__BackingField_19() { return &___U3CCurrentWallColliderU3Ek__BackingField_19; }
	inline void set_U3CCurrentWallColliderU3Ek__BackingField_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3CCurrentWallColliderU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentWallColliderU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_UpdateMode_20() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___UpdateMode_20)); }
	inline int32_t get_UpdateMode_20() const { return ___UpdateMode_20; }
	inline int32_t* get_address_of_UpdateMode_20() { return &___UpdateMode_20; }
	inline void set_UpdateMode_20(int32_t value)
	{
		___UpdateMode_20 = value;
	}

	inline static int32_t get_offset_of_NumberOfHorizontalRays_21() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___NumberOfHorizontalRays_21)); }
	inline int32_t get_NumberOfHorizontalRays_21() const { return ___NumberOfHorizontalRays_21; }
	inline int32_t* get_address_of_NumberOfHorizontalRays_21() { return &___NumberOfHorizontalRays_21; }
	inline void set_NumberOfHorizontalRays_21(int32_t value)
	{
		___NumberOfHorizontalRays_21 = value;
	}

	inline static int32_t get_offset_of_NumberOfVerticalRays_22() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___NumberOfVerticalRays_22)); }
	inline int32_t get_NumberOfVerticalRays_22() const { return ___NumberOfVerticalRays_22; }
	inline int32_t* get_address_of_NumberOfVerticalRays_22() { return &___NumberOfVerticalRays_22; }
	inline void set_NumberOfVerticalRays_22(int32_t value)
	{
		___NumberOfVerticalRays_22 = value;
	}

	inline static int32_t get_offset_of_RayOffsetHorizontal_23() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___RayOffsetHorizontal_23)); }
	inline float get_RayOffsetHorizontal_23() const { return ___RayOffsetHorizontal_23; }
	inline float* get_address_of_RayOffsetHorizontal_23() { return &___RayOffsetHorizontal_23; }
	inline void set_RayOffsetHorizontal_23(float value)
	{
		___RayOffsetHorizontal_23 = value;
	}

	inline static int32_t get_offset_of_RayOffsetVertical_24() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___RayOffsetVertical_24)); }
	inline float get_RayOffsetVertical_24() const { return ___RayOffsetVertical_24; }
	inline float* get_address_of_RayOffsetVertical_24() { return &___RayOffsetVertical_24; }
	inline void set_RayOffsetVertical_24(float value)
	{
		___RayOffsetVertical_24 = value;
	}

	inline static int32_t get_offset_of_CrouchedRaycastLengthMultiplier_25() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___CrouchedRaycastLengthMultiplier_25)); }
	inline float get_CrouchedRaycastLengthMultiplier_25() const { return ___CrouchedRaycastLengthMultiplier_25; }
	inline float* get_address_of_CrouchedRaycastLengthMultiplier_25() { return &___CrouchedRaycastLengthMultiplier_25; }
	inline void set_CrouchedRaycastLengthMultiplier_25(float value)
	{
		___CrouchedRaycastLengthMultiplier_25 = value;
	}

	inline static int32_t get_offset_of_CastRaysOnBothSides_26() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___CastRaysOnBothSides_26)); }
	inline bool get_CastRaysOnBothSides_26() const { return ___CastRaysOnBothSides_26; }
	inline bool* get_address_of_CastRaysOnBothSides_26() { return &___CastRaysOnBothSides_26; }
	inline void set_CastRaysOnBothSides_26(bool value)
	{
		___CastRaysOnBothSides_26 = value;
	}

	inline static int32_t get_offset_of_DistanceToTheGroundRayMaximumLength_27() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___DistanceToTheGroundRayMaximumLength_27)); }
	inline float get_DistanceToTheGroundRayMaximumLength_27() const { return ___DistanceToTheGroundRayMaximumLength_27; }
	inline float* get_address_of_DistanceToTheGroundRayMaximumLength_27() { return &___DistanceToTheGroundRayMaximumLength_27; }
	inline void set_DistanceToTheGroundRayMaximumLength_27(float value)
	{
		___DistanceToTheGroundRayMaximumLength_27 = value;
	}

	inline static int32_t get_offset_of_PerformSafetyBoxcast_28() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___PerformSafetyBoxcast_28)); }
	inline bool get_PerformSafetyBoxcast_28() const { return ___PerformSafetyBoxcast_28; }
	inline bool* get_address_of_PerformSafetyBoxcast_28() { return &___PerformSafetyBoxcast_28; }
	inline void set_PerformSafetyBoxcast_28(bool value)
	{
		___PerformSafetyBoxcast_28 = value;
	}

	inline static int32_t get_offset_of_OnMovingPlatformRaycastLengthMultiplier_29() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___OnMovingPlatformRaycastLengthMultiplier_29)); }
	inline float get_OnMovingPlatformRaycastLengthMultiplier_29() const { return ___OnMovingPlatformRaycastLengthMultiplier_29; }
	inline float* get_address_of_OnMovingPlatformRaycastLengthMultiplier_29() { return &___OnMovingPlatformRaycastLengthMultiplier_29; }
	inline void set_OnMovingPlatformRaycastLengthMultiplier_29(float value)
	{
		___OnMovingPlatformRaycastLengthMultiplier_29 = value;
	}

	inline static int32_t get_offset_of_ObstacleHeightTolerance_30() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___ObstacleHeightTolerance_30)); }
	inline float get_ObstacleHeightTolerance_30() const { return ___ObstacleHeightTolerance_30; }
	inline float* get_address_of_ObstacleHeightTolerance_30() { return &___ObstacleHeightTolerance_30; }
	inline void set_ObstacleHeightTolerance_30(float value)
	{
		___ObstacleHeightTolerance_30 = value;
	}

	inline static int32_t get_offset_of_StickToSlopes_31() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StickToSlopes_31)); }
	inline bool get_StickToSlopes_31() const { return ___StickToSlopes_31; }
	inline bool* get_address_of_StickToSlopes_31() { return &___StickToSlopes_31; }
	inline void set_StickToSlopes_31(bool value)
	{
		___StickToSlopes_31 = value;
	}

	inline static int32_t get_offset_of_StickyRaycastLength_32() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StickyRaycastLength_32)); }
	inline float get_StickyRaycastLength_32() const { return ___StickyRaycastLength_32; }
	inline float* get_address_of_StickyRaycastLength_32() { return &___StickyRaycastLength_32; }
	inline void set_StickyRaycastLength_32(float value)
	{
		___StickyRaycastLength_32 = value;
	}

	inline static int32_t get_offset_of_StickToSlopesOffsetY_33() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___StickToSlopesOffsetY_33)); }
	inline float get_StickToSlopesOffsetY_33() const { return ___StickToSlopesOffsetY_33; }
	inline float* get_address_of_StickToSlopesOffsetY_33() { return &___StickToSlopesOffsetY_33; }
	inline void set_StickToSlopesOffsetY_33(float value)
	{
		___StickToSlopesOffsetY_33 = value;
	}

	inline static int32_t get_offset_of_TimeAirborne_34() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___TimeAirborne_34)); }
	inline float get_TimeAirborne_34() const { return ___TimeAirborne_34; }
	inline float* get_address_of_TimeAirborne_34() { return &___TimeAirborne_34; }
	inline void set_TimeAirborne_34(float value)
	{
		___TimeAirborne_34 = value;
	}

	inline static int32_t get_offset_of_AutomaticGravitySettings_35() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ___AutomaticGravitySettings_35)); }
	inline bool get_AutomaticGravitySettings_35() const { return ___AutomaticGravitySettings_35; }
	inline bool* get_address_of_AutomaticGravitySettings_35() { return &___AutomaticGravitySettings_35; }
	inline void set_AutomaticGravitySettings_35(bool value)
	{
		___AutomaticGravitySettings_35 = value;
	}

	inline static int32_t get_offset_of__overrideParameters_36() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____overrideParameters_36)); }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * get__overrideParameters_36() const { return ____overrideParameters_36; }
	inline CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 ** get_address_of__overrideParameters_36() { return &____overrideParameters_36; }
	inline void set__overrideParameters_36(CorgiControllerParameters_t8C908EE880B00520825DA4FC5E9395B221F099A8 * value)
	{
		____overrideParameters_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____overrideParameters_36), (void*)value);
	}

	inline static int32_t get_offset_of__speed_37() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____speed_37)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__speed_37() const { return ____speed_37; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__speed_37() { return &____speed_37; }
	inline void set__speed_37(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____speed_37 = value;
	}

	inline static int32_t get_offset_of__friction_38() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____friction_38)); }
	inline float get__friction_38() const { return ____friction_38; }
	inline float* get_address_of__friction_38() { return &____friction_38; }
	inline void set__friction_38(float value)
	{
		____friction_38 = value;
	}

	inline static int32_t get_offset_of__fallSlowFactor_39() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____fallSlowFactor_39)); }
	inline float get__fallSlowFactor_39() const { return ____fallSlowFactor_39; }
	inline float* get_address_of__fallSlowFactor_39() { return &____fallSlowFactor_39; }
	inline void set__fallSlowFactor_39(float value)
	{
		____fallSlowFactor_39 = value;
	}

	inline static int32_t get_offset_of__currentGravity_40() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____currentGravity_40)); }
	inline float get__currentGravity_40() const { return ____currentGravity_40; }
	inline float* get_address_of__currentGravity_40() { return &____currentGravity_40; }
	inline void set__currentGravity_40(float value)
	{
		____currentGravity_40 = value;
	}

	inline static int32_t get_offset_of__externalForce_41() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____externalForce_41)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__externalForce_41() const { return ____externalForce_41; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__externalForce_41() { return &____externalForce_41; }
	inline void set__externalForce_41(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____externalForce_41 = value;
	}

	inline static int32_t get_offset_of__newPosition_42() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____newPosition_42)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__newPosition_42() const { return ____newPosition_42; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__newPosition_42() { return &____newPosition_42; }
	inline void set__newPosition_42(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____newPosition_42 = value;
	}

	inline static int32_t get_offset_of__transform_43() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____transform_43)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__transform_43() const { return ____transform_43; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__transform_43() { return &____transform_43; }
	inline void set__transform_43(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____transform_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transform_43), (void*)value);
	}

	inline static int32_t get_offset_of__boxCollider_44() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boxCollider_44)); }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * get__boxCollider_44() const { return ____boxCollider_44; }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 ** get_address_of__boxCollider_44() { return &____boxCollider_44; }
	inline void set__boxCollider_44(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * value)
	{
		____boxCollider_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____boxCollider_44), (void*)value);
	}

	inline static int32_t get_offset_of__characterGravity_45() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____characterGravity_45)); }
	inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * get__characterGravity_45() const { return ____characterGravity_45; }
	inline CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 ** get_address_of__characterGravity_45() { return &____characterGravity_45; }
	inline void set__characterGravity_45(CharacterGravity_tF75C38A55C288F2F30D7181FDE4222F40EF6A2E4 * value)
	{
		____characterGravity_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterGravity_45), (void*)value);
	}

	inline static int32_t get_offset_of__platformMaskSave_46() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____platformMaskSave_46)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__platformMaskSave_46() const { return ____platformMaskSave_46; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__platformMaskSave_46() { return &____platformMaskSave_46; }
	inline void set__platformMaskSave_46(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____platformMaskSave_46 = value;
	}

	inline static int32_t get_offset_of__raysBelowLayerMaskPlatforms_47() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raysBelowLayerMaskPlatforms_47)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__raysBelowLayerMaskPlatforms_47() const { return ____raysBelowLayerMaskPlatforms_47; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__raysBelowLayerMaskPlatforms_47() { return &____raysBelowLayerMaskPlatforms_47; }
	inline void set__raysBelowLayerMaskPlatforms_47(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____raysBelowLayerMaskPlatforms_47 = value;
	}

	inline static int32_t get_offset_of__raysBelowLayerMaskPlatformsWithoutOneWay_48() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raysBelowLayerMaskPlatformsWithoutOneWay_48)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__raysBelowLayerMaskPlatformsWithoutOneWay_48() const { return ____raysBelowLayerMaskPlatformsWithoutOneWay_48; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__raysBelowLayerMaskPlatformsWithoutOneWay_48() { return &____raysBelowLayerMaskPlatformsWithoutOneWay_48; }
	inline void set__raysBelowLayerMaskPlatformsWithoutOneWay_48(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____raysBelowLayerMaskPlatformsWithoutOneWay_48 = value;
	}

	inline static int32_t get_offset_of__raysBelowLayerMaskPlatformsWithoutMidHeight_49() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raysBelowLayerMaskPlatformsWithoutMidHeight_49)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get__raysBelowLayerMaskPlatformsWithoutMidHeight_49() const { return ____raysBelowLayerMaskPlatformsWithoutMidHeight_49; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of__raysBelowLayerMaskPlatformsWithoutMidHeight_49() { return &____raysBelowLayerMaskPlatformsWithoutMidHeight_49; }
	inline void set__raysBelowLayerMaskPlatformsWithoutMidHeight_49(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		____raysBelowLayerMaskPlatformsWithoutMidHeight_49 = value;
	}

	inline static int32_t get_offset_of__savedBelowLayer_50() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____savedBelowLayer_50)); }
	inline int32_t get__savedBelowLayer_50() const { return ____savedBelowLayer_50; }
	inline int32_t* get_address_of__savedBelowLayer_50() { return &____savedBelowLayer_50; }
	inline void set__savedBelowLayer_50(int32_t value)
	{
		____savedBelowLayer_50 = value;
	}

	inline static int32_t get_offset_of__movingPlatform_51() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movingPlatform_51)); }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * get__movingPlatform_51() const { return ____movingPlatform_51; }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B ** get_address_of__movingPlatform_51() { return &____movingPlatform_51; }
	inline void set__movingPlatform_51(MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * value)
	{
		____movingPlatform_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movingPlatform_51), (void*)value);
	}

	inline static int32_t get_offset_of__movingPlatformCurrentGravity_52() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movingPlatformCurrentGravity_52)); }
	inline float get__movingPlatformCurrentGravity_52() const { return ____movingPlatformCurrentGravity_52; }
	inline float* get_address_of__movingPlatformCurrentGravity_52() { return &____movingPlatformCurrentGravity_52; }
	inline void set__movingPlatformCurrentGravity_52(float value)
	{
		____movingPlatformCurrentGravity_52 = value;
	}

	inline static int32_t get_offset_of__gravityActive_53() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____gravityActive_53)); }
	inline bool get__gravityActive_53() const { return ____gravityActive_53; }
	inline bool* get_address_of__gravityActive_53() { return &____gravityActive_53; }
	inline void set__gravityActive_53(bool value)
	{
		____gravityActive_53 = value;
	}

	inline static int32_t get_offset_of__ignoredCollider_54() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____ignoredCollider_54)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__ignoredCollider_54() const { return ____ignoredCollider_54; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__ignoredCollider_54() { return &____ignoredCollider_54; }
	inline void set__ignoredCollider_54(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____ignoredCollider_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ignoredCollider_54), (void*)value);
	}

	inline static int32_t get_offset_of__collisionsOnWithStairs_55() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____collisionsOnWithStairs_55)); }
	inline bool get__collisionsOnWithStairs_55() const { return ____collisionsOnWithStairs_55; }
	inline bool* get_address_of__collisionsOnWithStairs_55() { return &____collisionsOnWithStairs_55; }
	inline void set__collisionsOnWithStairs_55(bool value)
	{
		____collisionsOnWithStairs_55 = value;
	}

	inline static int32_t get_offset_of__originalColliderSize_58() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____originalColliderSize_58)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__originalColliderSize_58() const { return ____originalColliderSize_58; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__originalColliderSize_58() { return &____originalColliderSize_58; }
	inline void set__originalColliderSize_58(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____originalColliderSize_58 = value;
	}

	inline static int32_t get_offset_of__originalColliderOffset_59() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____originalColliderOffset_59)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__originalColliderOffset_59() const { return ____originalColliderOffset_59; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__originalColliderOffset_59() { return &____originalColliderOffset_59; }
	inline void set__originalColliderOffset_59(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____originalColliderOffset_59 = value;
	}

	inline static int32_t get_offset_of__originalSizeRaycastOrigin_60() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____originalSizeRaycastOrigin_60)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__originalSizeRaycastOrigin_60() const { return ____originalSizeRaycastOrigin_60; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__originalSizeRaycastOrigin_60() { return &____originalSizeRaycastOrigin_60; }
	inline void set__originalSizeRaycastOrigin_60(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____originalSizeRaycastOrigin_60 = value;
	}

	inline static int32_t get_offset_of__crossBelowSlopeAngle_61() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____crossBelowSlopeAngle_61)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__crossBelowSlopeAngle_61() const { return ____crossBelowSlopeAngle_61; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__crossBelowSlopeAngle_61() { return &____crossBelowSlopeAngle_61; }
	inline void set__crossBelowSlopeAngle_61(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____crossBelowSlopeAngle_61 = value;
	}

	inline static int32_t get_offset_of__sideHitsStorage_62() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____sideHitsStorage_62)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__sideHitsStorage_62() const { return ____sideHitsStorage_62; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__sideHitsStorage_62() { return &____sideHitsStorage_62; }
	inline void set__sideHitsStorage_62(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____sideHitsStorage_62 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sideHitsStorage_62), (void*)value);
	}

	inline static int32_t get_offset_of__belowHitsStorage_63() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____belowHitsStorage_63)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__belowHitsStorage_63() const { return ____belowHitsStorage_63; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__belowHitsStorage_63() { return &____belowHitsStorage_63; }
	inline void set__belowHitsStorage_63(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____belowHitsStorage_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____belowHitsStorage_63), (void*)value);
	}

	inline static int32_t get_offset_of__aboveHitsStorage_64() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____aboveHitsStorage_64)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__aboveHitsStorage_64() const { return ____aboveHitsStorage_64; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__aboveHitsStorage_64() { return &____aboveHitsStorage_64; }
	inline void set__aboveHitsStorage_64(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____aboveHitsStorage_64 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____aboveHitsStorage_64), (void*)value);
	}

	inline static int32_t get_offset_of__stickRaycastLeft_65() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____stickRaycastLeft_65)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__stickRaycastLeft_65() const { return ____stickRaycastLeft_65; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__stickRaycastLeft_65() { return &____stickRaycastLeft_65; }
	inline void set__stickRaycastLeft_65(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____stickRaycastLeft_65 = value;
	}

	inline static int32_t get_offset_of__stickRaycastRight_66() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____stickRaycastRight_66)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__stickRaycastRight_66() const { return ____stickRaycastRight_66; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__stickRaycastRight_66() { return &____stickRaycastRight_66; }
	inline void set__stickRaycastRight_66(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____stickRaycastRight_66 = value;
	}

	inline static int32_t get_offset_of__stickRaycast_67() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____stickRaycast_67)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__stickRaycast_67() const { return ____stickRaycast_67; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__stickRaycast_67() { return &____stickRaycast_67; }
	inline void set__stickRaycast_67(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____stickRaycast_67 = value;
	}

	inline static int32_t get_offset_of__distanceToTheGroundRaycast_68() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____distanceToTheGroundRaycast_68)); }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  get__distanceToTheGroundRaycast_68() const { return ____distanceToTheGroundRaycast_68; }
	inline RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4 * get_address_of__distanceToTheGroundRaycast_68() { return &____distanceToTheGroundRaycast_68; }
	inline void set__distanceToTheGroundRaycast_68(RaycastHit2D_t210878DAEBC96C1F69DF9883C454758724A106A4  value)
	{
		____distanceToTheGroundRaycast_68 = value;
	}

	inline static int32_t get_offset_of__movementDirection_69() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movementDirection_69)); }
	inline float get__movementDirection_69() const { return ____movementDirection_69; }
	inline float* get_address_of__movementDirection_69() { return &____movementDirection_69; }
	inline void set__movementDirection_69(float value)
	{
		____movementDirection_69 = value;
	}

	inline static int32_t get_offset_of__storedMovementDirection_70() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____storedMovementDirection_70)); }
	inline float get__storedMovementDirection_70() const { return ____storedMovementDirection_70; }
	inline float* get_address_of__storedMovementDirection_70() { return &____storedMovementDirection_70; }
	inline void set__storedMovementDirection_70(float value)
	{
		____storedMovementDirection_70 = value;
	}

	inline static int32_t get_offset_of__horizontalRayCastFromBottom_72() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____horizontalRayCastFromBottom_72)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__horizontalRayCastFromBottom_72() const { return ____horizontalRayCastFromBottom_72; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__horizontalRayCastFromBottom_72() { return &____horizontalRayCastFromBottom_72; }
	inline void set__horizontalRayCastFromBottom_72(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____horizontalRayCastFromBottom_72 = value;
	}

	inline static int32_t get_offset_of__horizontalRayCastToTop_73() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____horizontalRayCastToTop_73)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__horizontalRayCastToTop_73() const { return ____horizontalRayCastToTop_73; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__horizontalRayCastToTop_73() { return &____horizontalRayCastToTop_73; }
	inline void set__horizontalRayCastToTop_73(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____horizontalRayCastToTop_73 = value;
	}

	inline static int32_t get_offset_of__verticalRayCastFromLeft_74() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____verticalRayCastFromLeft_74)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__verticalRayCastFromLeft_74() const { return ____verticalRayCastFromLeft_74; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__verticalRayCastFromLeft_74() { return &____verticalRayCastFromLeft_74; }
	inline void set__verticalRayCastFromLeft_74(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____verticalRayCastFromLeft_74 = value;
	}

	inline static int32_t get_offset_of__verticalRayCastToRight_75() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____verticalRayCastToRight_75)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__verticalRayCastToRight_75() const { return ____verticalRayCastToRight_75; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__verticalRayCastToRight_75() { return &____verticalRayCastToRight_75; }
	inline void set__verticalRayCastToRight_75(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____verticalRayCastToRight_75 = value;
	}

	inline static int32_t get_offset_of__aboveRayCastStart_76() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____aboveRayCastStart_76)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__aboveRayCastStart_76() const { return ____aboveRayCastStart_76; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__aboveRayCastStart_76() { return &____aboveRayCastStart_76; }
	inline void set__aboveRayCastStart_76(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____aboveRayCastStart_76 = value;
	}

	inline static int32_t get_offset_of__aboveRayCastEnd_77() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____aboveRayCastEnd_77)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__aboveRayCastEnd_77() const { return ____aboveRayCastEnd_77; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__aboveRayCastEnd_77() { return &____aboveRayCastEnd_77; }
	inline void set__aboveRayCastEnd_77(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____aboveRayCastEnd_77 = value;
	}

	inline static int32_t get_offset_of__rayCastOrigin_78() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____rayCastOrigin_78)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__rayCastOrigin_78() const { return ____rayCastOrigin_78; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__rayCastOrigin_78() { return &____rayCastOrigin_78; }
	inline void set__rayCastOrigin_78(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____rayCastOrigin_78 = value;
	}

	inline static int32_t get_offset_of__colliderBottomCenterPosition_79() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderBottomCenterPosition_79)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderBottomCenterPosition_79() const { return ____colliderBottomCenterPosition_79; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderBottomCenterPosition_79() { return &____colliderBottomCenterPosition_79; }
	inline void set__colliderBottomCenterPosition_79(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderBottomCenterPosition_79 = value;
	}

	inline static int32_t get_offset_of__colliderLeftCenterPosition_80() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderLeftCenterPosition_80)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderLeftCenterPosition_80() const { return ____colliderLeftCenterPosition_80; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderLeftCenterPosition_80() { return &____colliderLeftCenterPosition_80; }
	inline void set__colliderLeftCenterPosition_80(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderLeftCenterPosition_80 = value;
	}

	inline static int32_t get_offset_of__colliderRightCenterPosition_81() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderRightCenterPosition_81)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderRightCenterPosition_81() const { return ____colliderRightCenterPosition_81; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderRightCenterPosition_81() { return &____colliderRightCenterPosition_81; }
	inline void set__colliderRightCenterPosition_81(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderRightCenterPosition_81 = value;
	}

	inline static int32_t get_offset_of__colliderTopCenterPosition_82() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____colliderTopCenterPosition_82)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__colliderTopCenterPosition_82() const { return ____colliderTopCenterPosition_82; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__colliderTopCenterPosition_82() { return &____colliderTopCenterPosition_82; }
	inline void set__colliderTopCenterPosition_82(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____colliderTopCenterPosition_82 = value;
	}

	inline static int32_t get_offset_of__movingPlatformTest_83() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____movingPlatformTest_83)); }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * get__movingPlatformTest_83() const { return ____movingPlatformTest_83; }
	inline MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B ** get_address_of__movingPlatformTest_83() { return &____movingPlatformTest_83; }
	inline void set__movingPlatformTest_83(MMPathMovement_t399E825DA5246A5F493DD5AE2D86B48C3E41056B * value)
	{
		____movingPlatformTest_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____movingPlatformTest_83), (void*)value);
	}

	inline static int32_t get_offset_of__frictionTest_84() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____frictionTest_84)); }
	inline SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 * get__frictionTest_84() const { return ____frictionTest_84; }
	inline SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 ** get_address_of__frictionTest_84() { return &____frictionTest_84; }
	inline void set__frictionTest_84(SurfaceModifier_t5292679D455E930D43237D9B245DD7432C1CB211 * value)
	{
		____frictionTest_84 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____frictionTest_84), (void*)value);
	}

	inline static int32_t get_offset_of__update_85() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____update_85)); }
	inline bool get__update_85() const { return ____update_85; }
	inline bool* get_address_of__update_85() { return &____update_85; }
	inline void set__update_85(bool value)
	{
		____update_85 = value;
	}

	inline static int32_t get_offset_of__raycastNonAlloc_86() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____raycastNonAlloc_86)); }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* get__raycastNonAlloc_86() const { return ____raycastNonAlloc_86; }
	inline RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09** get_address_of__raycastNonAlloc_86() { return &____raycastNonAlloc_86; }
	inline void set__raycastNonAlloc_86(RaycastHit2DU5BU5D_tDEABD9FBBA32C695C932A32A1B8FB9C06A496F09* value)
	{
		____raycastNonAlloc_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____raycastNonAlloc_86), (void*)value);
	}

	inline static int32_t get_offset_of__boundsTopLeftCorner_87() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsTopLeftCorner_87)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsTopLeftCorner_87() const { return ____boundsTopLeftCorner_87; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsTopLeftCorner_87() { return &____boundsTopLeftCorner_87; }
	inline void set__boundsTopLeftCorner_87(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsTopLeftCorner_87 = value;
	}

	inline static int32_t get_offset_of__boundsBottomLeftCorner_88() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsBottomLeftCorner_88)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsBottomLeftCorner_88() const { return ____boundsBottomLeftCorner_88; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsBottomLeftCorner_88() { return &____boundsBottomLeftCorner_88; }
	inline void set__boundsBottomLeftCorner_88(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsBottomLeftCorner_88 = value;
	}

	inline static int32_t get_offset_of__boundsTopRightCorner_89() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsTopRightCorner_89)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsTopRightCorner_89() const { return ____boundsTopRightCorner_89; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsTopRightCorner_89() { return &____boundsTopRightCorner_89; }
	inline void set__boundsTopRightCorner_89(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsTopRightCorner_89 = value;
	}

	inline static int32_t get_offset_of__boundsBottomRightCorner_90() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsBottomRightCorner_90)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsBottomRightCorner_90() const { return ____boundsBottomRightCorner_90; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsBottomRightCorner_90() { return &____boundsBottomRightCorner_90; }
	inline void set__boundsBottomRightCorner_90(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsBottomRightCorner_90 = value;
	}

	inline static int32_t get_offset_of__boundsCenter_91() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsCenter_91)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__boundsCenter_91() const { return ____boundsCenter_91; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__boundsCenter_91() { return &____boundsCenter_91; }
	inline void set__boundsCenter_91(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____boundsCenter_91 = value;
	}

	inline static int32_t get_offset_of__bounds_92() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____bounds_92)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__bounds_92() const { return ____bounds_92; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__bounds_92() { return &____bounds_92; }
	inline void set__bounds_92(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____bounds_92 = value;
	}

	inline static int32_t get_offset_of__boundsWidth_93() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsWidth_93)); }
	inline float get__boundsWidth_93() const { return ____boundsWidth_93; }
	inline float* get_address_of__boundsWidth_93() { return &____boundsWidth_93; }
	inline void set__boundsWidth_93(float value)
	{
		____boundsWidth_93 = value;
	}

	inline static int32_t get_offset_of__boundsHeight_94() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____boundsHeight_94)); }
	inline float get__boundsHeight_94() const { return ____boundsHeight_94; }
	inline float* get_address_of__boundsHeight_94() { return &____boundsHeight_94; }
	inline void set__boundsHeight_94(float value)
	{
		____boundsHeight_94 = value;
	}

	inline static int32_t get_offset_of__distanceToTheGround_95() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____distanceToTheGround_95)); }
	inline float get__distanceToTheGround_95() const { return ____distanceToTheGround_95; }
	inline float* get_address_of__distanceToTheGround_95() { return &____distanceToTheGround_95; }
	inline void set__distanceToTheGround_95(float value)
	{
		____distanceToTheGround_95 = value;
	}

	inline static int32_t get_offset_of__worldSpeed_96() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____worldSpeed_96)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__worldSpeed_96() const { return ____worldSpeed_96; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__worldSpeed_96() { return &____worldSpeed_96; }
	inline void set__worldSpeed_96(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____worldSpeed_96 = value;
	}

	inline static int32_t get_offset_of__contactList_97() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____contactList_97)); }
	inline List_1_t3926283FA9AE49778D95220056CEBFB01D034379 * get__contactList_97() const { return ____contactList_97; }
	inline List_1_t3926283FA9AE49778D95220056CEBFB01D034379 ** get_address_of__contactList_97() { return &____contactList_97; }
	inline void set__contactList_97(List_1_t3926283FA9AE49778D95220056CEBFB01D034379 * value)
	{
		____contactList_97 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____contactList_97), (void*)value);
	}

	inline static int32_t get_offset_of__shouldComputeNewSpeed_98() { return static_cast<int32_t>(offsetof(CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8, ____shouldComputeNewSpeed_98)); }
	inline bool get__shouldComputeNewSpeed_98() const { return ____shouldComputeNewSpeed_98; }
	inline bool* get_address_of__shouldComputeNewSpeed_98() { return &____shouldComputeNewSpeed_98; }
	inline void set__shouldComputeNewSpeed_98(bool value)
	{
		____shouldComputeNewSpeed_98 = value;
	}
};


// MoreMountains.Tools.MMCinemachineZone
struct MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Cinemachine.CinemachineVirtualCamera MoreMountains.Tools.MMCinemachineZone::VirtualCamera
	CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * ___VirtualCamera_4;
	// MoreMountains.Tools.MMCinemachineZone/Modes MoreMountains.Tools.MMCinemachineZone::Mode
	int32_t ___Mode_5;
	// System.Boolean MoreMountains.Tools.MMCinemachineZone::CameraStartsActive
	bool ___CameraStartsActive_6;
	// System.Int32 MoreMountains.Tools.MMCinemachineZone::EnabledPriority
	int32_t ___EnabledPriority_7;
	// System.Int32 MoreMountains.Tools.MMCinemachineZone::DisabledPriority
	int32_t ___DisabledPriority_8;
	// UnityEngine.LayerMask MoreMountains.Tools.MMCinemachineZone::TriggerMask
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___TriggerMask_9;
	// System.Boolean MoreMountains.Tools.MMCinemachineZone::SetupConfinerOnStart
	bool ___SetupConfinerOnStart_10;
	// System.Boolean MoreMountains.Tools.MMCinemachineZone::GenerateConfinerSetup
	bool ___GenerateConfinerSetup_11;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMCinemachineZone::OnEnterZoneEvent
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnEnterZoneEvent_12;
	// UnityEngine.Events.UnityEvent MoreMountains.Tools.MMCinemachineZone::OnExitZoneEvent
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnExitZoneEvent_13;
	// System.Boolean MoreMountains.Tools.MMCinemachineZone::DrawGizmos
	bool ___DrawGizmos_14;
	// UnityEngine.Color MoreMountains.Tools.MMCinemachineZone::GizmosColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___GizmosColor_15;
	// UnityEngine.GameObject MoreMountains.Tools.MMCinemachineZone::_confinerGameObject
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____confinerGameObject_16;
	// UnityEngine.Vector3 MoreMountains.Tools.MMCinemachineZone::_gizmoSize
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____gizmoSize_17;

public:
	inline static int32_t get_offset_of_VirtualCamera_4() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___VirtualCamera_4)); }
	inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * get_VirtualCamera_4() const { return ___VirtualCamera_4; }
	inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C ** get_address_of_VirtualCamera_4() { return &___VirtualCamera_4; }
	inline void set_VirtualCamera_4(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * value)
	{
		___VirtualCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VirtualCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_Mode_5() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___Mode_5)); }
	inline int32_t get_Mode_5() const { return ___Mode_5; }
	inline int32_t* get_address_of_Mode_5() { return &___Mode_5; }
	inline void set_Mode_5(int32_t value)
	{
		___Mode_5 = value;
	}

	inline static int32_t get_offset_of_CameraStartsActive_6() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___CameraStartsActive_6)); }
	inline bool get_CameraStartsActive_6() const { return ___CameraStartsActive_6; }
	inline bool* get_address_of_CameraStartsActive_6() { return &___CameraStartsActive_6; }
	inline void set_CameraStartsActive_6(bool value)
	{
		___CameraStartsActive_6 = value;
	}

	inline static int32_t get_offset_of_EnabledPriority_7() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___EnabledPriority_7)); }
	inline int32_t get_EnabledPriority_7() const { return ___EnabledPriority_7; }
	inline int32_t* get_address_of_EnabledPriority_7() { return &___EnabledPriority_7; }
	inline void set_EnabledPriority_7(int32_t value)
	{
		___EnabledPriority_7 = value;
	}

	inline static int32_t get_offset_of_DisabledPriority_8() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___DisabledPriority_8)); }
	inline int32_t get_DisabledPriority_8() const { return ___DisabledPriority_8; }
	inline int32_t* get_address_of_DisabledPriority_8() { return &___DisabledPriority_8; }
	inline void set_DisabledPriority_8(int32_t value)
	{
		___DisabledPriority_8 = value;
	}

	inline static int32_t get_offset_of_TriggerMask_9() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___TriggerMask_9)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_TriggerMask_9() const { return ___TriggerMask_9; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_TriggerMask_9() { return &___TriggerMask_9; }
	inline void set_TriggerMask_9(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___TriggerMask_9 = value;
	}

	inline static int32_t get_offset_of_SetupConfinerOnStart_10() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___SetupConfinerOnStart_10)); }
	inline bool get_SetupConfinerOnStart_10() const { return ___SetupConfinerOnStart_10; }
	inline bool* get_address_of_SetupConfinerOnStart_10() { return &___SetupConfinerOnStart_10; }
	inline void set_SetupConfinerOnStart_10(bool value)
	{
		___SetupConfinerOnStart_10 = value;
	}

	inline static int32_t get_offset_of_GenerateConfinerSetup_11() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___GenerateConfinerSetup_11)); }
	inline bool get_GenerateConfinerSetup_11() const { return ___GenerateConfinerSetup_11; }
	inline bool* get_address_of_GenerateConfinerSetup_11() { return &___GenerateConfinerSetup_11; }
	inline void set_GenerateConfinerSetup_11(bool value)
	{
		___GenerateConfinerSetup_11 = value;
	}

	inline static int32_t get_offset_of_OnEnterZoneEvent_12() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___OnEnterZoneEvent_12)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnEnterZoneEvent_12() const { return ___OnEnterZoneEvent_12; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnEnterZoneEvent_12() { return &___OnEnterZoneEvent_12; }
	inline void set_OnEnterZoneEvent_12(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnEnterZoneEvent_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEnterZoneEvent_12), (void*)value);
	}

	inline static int32_t get_offset_of_OnExitZoneEvent_13() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___OnExitZoneEvent_13)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnExitZoneEvent_13() const { return ___OnExitZoneEvent_13; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnExitZoneEvent_13() { return &___OnExitZoneEvent_13; }
	inline void set_OnExitZoneEvent_13(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnExitZoneEvent_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnExitZoneEvent_13), (void*)value);
	}

	inline static int32_t get_offset_of_DrawGizmos_14() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___DrawGizmos_14)); }
	inline bool get_DrawGizmos_14() const { return ___DrawGizmos_14; }
	inline bool* get_address_of_DrawGizmos_14() { return &___DrawGizmos_14; }
	inline void set_DrawGizmos_14(bool value)
	{
		___DrawGizmos_14 = value;
	}

	inline static int32_t get_offset_of_GizmosColor_15() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ___GizmosColor_15)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_GizmosColor_15() const { return ___GizmosColor_15; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_GizmosColor_15() { return &___GizmosColor_15; }
	inline void set_GizmosColor_15(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___GizmosColor_15 = value;
	}

	inline static int32_t get_offset_of__confinerGameObject_16() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ____confinerGameObject_16)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__confinerGameObject_16() const { return ____confinerGameObject_16; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__confinerGameObject_16() { return &____confinerGameObject_16; }
	inline void set__confinerGameObject_16(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____confinerGameObject_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____confinerGameObject_16), (void*)value);
	}

	inline static int32_t get_offset_of__gizmoSize_17() { return static_cast<int32_t>(offsetof(MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5, ____gizmoSize_17)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__gizmoSize_17() const { return ____gizmoSize_17; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__gizmoSize_17() { return &____gizmoSize_17; }
	inline void set__gizmoSize_17(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____gizmoSize_17 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbacks
struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Collections.Generic.List`1<MoreMountains.Feedbacks.MMFeedback> MoreMountains.Feedbacks.MMFeedbacks::Feedbacks
	List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 * ___Feedbacks_4;
	// MoreMountains.Feedbacks.MMFeedbacks/InitializationModes MoreMountains.Feedbacks.MMFeedbacks::InitializationMode
	int32_t ___InitializationMode_5;
	// MoreMountains.Feedbacks.MMFeedbacks/SafeModes MoreMountains.Feedbacks.MMFeedbacks::SafeMode
	int32_t ___SafeMode_6;
	// MoreMountains.Feedbacks.MMFeedbacks/Directions MoreMountains.Feedbacks.MMFeedbacks::Direction
	int32_t ___Direction_7;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::AutoChangeDirectionOnEnd
	bool ___AutoChangeDirectionOnEnd_8;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::AutoPlayOnStart
	bool ___AutoPlayOnStart_9;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::AutoPlayOnEnable
	bool ___AutoPlayOnEnable_10;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::DurationMultiplier
	float ___DurationMultiplier_11;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::DisplayFullDurationDetails
	bool ___DisplayFullDurationDetails_12;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::CooldownDuration
	float ___CooldownDuration_13;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::InitialDelay
	float ___InitialDelay_14;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::CanPlayWhileAlreadyPlaying
	bool ___CanPlayWhileAlreadyPlaying_15;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::FeedbacksIntensity
	float ___FeedbacksIntensity_16;
	// MoreMountains.Feedbacks.MMFeedbacksEvents MoreMountains.Feedbacks.MMFeedbacks::Events
	MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A * ___Events_17;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::DebugActive
	bool ___DebugActive_19;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_20;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<InScriptDrivenPause>k__BackingField
	bool ___U3CInScriptDrivenPauseU3Ek__BackingField_21;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<ContainsLoop>k__BackingField
	bool ___U3CContainsLoopU3Ek__BackingField_22;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::<ShouldRevertOnNextPlay>k__BackingField
	bool ___U3CShouldRevertOnNextPlayU3Ek__BackingField_23;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_startTime
	float ____startTime_24;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_holdingMax
	float ____holdingMax_25;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_lastStartAt
	float ____lastStartAt_26;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::_pauseFound
	bool ____pauseFound_27;
	// System.Single MoreMountains.Feedbacks.MMFeedbacks::_totalDuration
	float ____totalDuration_28;

public:
	inline static int32_t get_offset_of_Feedbacks_4() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___Feedbacks_4)); }
	inline List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 * get_Feedbacks_4() const { return ___Feedbacks_4; }
	inline List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 ** get_address_of_Feedbacks_4() { return &___Feedbacks_4; }
	inline void set_Feedbacks_4(List_1_t65DB3FDD9079C29778530442558DE4285EB4A794 * value)
	{
		___Feedbacks_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Feedbacks_4), (void*)value);
	}

	inline static int32_t get_offset_of_InitializationMode_5() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___InitializationMode_5)); }
	inline int32_t get_InitializationMode_5() const { return ___InitializationMode_5; }
	inline int32_t* get_address_of_InitializationMode_5() { return &___InitializationMode_5; }
	inline void set_InitializationMode_5(int32_t value)
	{
		___InitializationMode_5 = value;
	}

	inline static int32_t get_offset_of_SafeMode_6() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___SafeMode_6)); }
	inline int32_t get_SafeMode_6() const { return ___SafeMode_6; }
	inline int32_t* get_address_of_SafeMode_6() { return &___SafeMode_6; }
	inline void set_SafeMode_6(int32_t value)
	{
		___SafeMode_6 = value;
	}

	inline static int32_t get_offset_of_Direction_7() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___Direction_7)); }
	inline int32_t get_Direction_7() const { return ___Direction_7; }
	inline int32_t* get_address_of_Direction_7() { return &___Direction_7; }
	inline void set_Direction_7(int32_t value)
	{
		___Direction_7 = value;
	}

	inline static int32_t get_offset_of_AutoChangeDirectionOnEnd_8() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___AutoChangeDirectionOnEnd_8)); }
	inline bool get_AutoChangeDirectionOnEnd_8() const { return ___AutoChangeDirectionOnEnd_8; }
	inline bool* get_address_of_AutoChangeDirectionOnEnd_8() { return &___AutoChangeDirectionOnEnd_8; }
	inline void set_AutoChangeDirectionOnEnd_8(bool value)
	{
		___AutoChangeDirectionOnEnd_8 = value;
	}

	inline static int32_t get_offset_of_AutoPlayOnStart_9() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___AutoPlayOnStart_9)); }
	inline bool get_AutoPlayOnStart_9() const { return ___AutoPlayOnStart_9; }
	inline bool* get_address_of_AutoPlayOnStart_9() { return &___AutoPlayOnStart_9; }
	inline void set_AutoPlayOnStart_9(bool value)
	{
		___AutoPlayOnStart_9 = value;
	}

	inline static int32_t get_offset_of_AutoPlayOnEnable_10() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___AutoPlayOnEnable_10)); }
	inline bool get_AutoPlayOnEnable_10() const { return ___AutoPlayOnEnable_10; }
	inline bool* get_address_of_AutoPlayOnEnable_10() { return &___AutoPlayOnEnable_10; }
	inline void set_AutoPlayOnEnable_10(bool value)
	{
		___AutoPlayOnEnable_10 = value;
	}

	inline static int32_t get_offset_of_DurationMultiplier_11() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___DurationMultiplier_11)); }
	inline float get_DurationMultiplier_11() const { return ___DurationMultiplier_11; }
	inline float* get_address_of_DurationMultiplier_11() { return &___DurationMultiplier_11; }
	inline void set_DurationMultiplier_11(float value)
	{
		___DurationMultiplier_11 = value;
	}

	inline static int32_t get_offset_of_DisplayFullDurationDetails_12() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___DisplayFullDurationDetails_12)); }
	inline bool get_DisplayFullDurationDetails_12() const { return ___DisplayFullDurationDetails_12; }
	inline bool* get_address_of_DisplayFullDurationDetails_12() { return &___DisplayFullDurationDetails_12; }
	inline void set_DisplayFullDurationDetails_12(bool value)
	{
		___DisplayFullDurationDetails_12 = value;
	}

	inline static int32_t get_offset_of_CooldownDuration_13() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___CooldownDuration_13)); }
	inline float get_CooldownDuration_13() const { return ___CooldownDuration_13; }
	inline float* get_address_of_CooldownDuration_13() { return &___CooldownDuration_13; }
	inline void set_CooldownDuration_13(float value)
	{
		___CooldownDuration_13 = value;
	}

	inline static int32_t get_offset_of_InitialDelay_14() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___InitialDelay_14)); }
	inline float get_InitialDelay_14() const { return ___InitialDelay_14; }
	inline float* get_address_of_InitialDelay_14() { return &___InitialDelay_14; }
	inline void set_InitialDelay_14(float value)
	{
		___InitialDelay_14 = value;
	}

	inline static int32_t get_offset_of_CanPlayWhileAlreadyPlaying_15() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___CanPlayWhileAlreadyPlaying_15)); }
	inline bool get_CanPlayWhileAlreadyPlaying_15() const { return ___CanPlayWhileAlreadyPlaying_15; }
	inline bool* get_address_of_CanPlayWhileAlreadyPlaying_15() { return &___CanPlayWhileAlreadyPlaying_15; }
	inline void set_CanPlayWhileAlreadyPlaying_15(bool value)
	{
		___CanPlayWhileAlreadyPlaying_15 = value;
	}

	inline static int32_t get_offset_of_FeedbacksIntensity_16() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___FeedbacksIntensity_16)); }
	inline float get_FeedbacksIntensity_16() const { return ___FeedbacksIntensity_16; }
	inline float* get_address_of_FeedbacksIntensity_16() { return &___FeedbacksIntensity_16; }
	inline void set_FeedbacksIntensity_16(float value)
	{
		___FeedbacksIntensity_16 = value;
	}

	inline static int32_t get_offset_of_Events_17() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___Events_17)); }
	inline MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A * get_Events_17() const { return ___Events_17; }
	inline MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A ** get_address_of_Events_17() { return &___Events_17; }
	inline void set_Events_17(MMFeedbacksEvents_tD97C44EABE3B4C421BC47194E569A5333306807A * value)
	{
		___Events_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Events_17), (void*)value);
	}

	inline static int32_t get_offset_of_DebugActive_19() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___DebugActive_19)); }
	inline bool get_DebugActive_19() const { return ___DebugActive_19; }
	inline bool* get_address_of_DebugActive_19() { return &___DebugActive_19; }
	inline void set_DebugActive_19(bool value)
	{
		___DebugActive_19 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CIsPlayingU3Ek__BackingField_20)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_20() const { return ___U3CIsPlayingU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_20() { return &___U3CIsPlayingU3Ek__BackingField_20; }
	inline void set_U3CIsPlayingU3Ek__BackingField_20(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CInScriptDrivenPauseU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CInScriptDrivenPauseU3Ek__BackingField_21)); }
	inline bool get_U3CInScriptDrivenPauseU3Ek__BackingField_21() const { return ___U3CInScriptDrivenPauseU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CInScriptDrivenPauseU3Ek__BackingField_21() { return &___U3CInScriptDrivenPauseU3Ek__BackingField_21; }
	inline void set_U3CInScriptDrivenPauseU3Ek__BackingField_21(bool value)
	{
		___U3CInScriptDrivenPauseU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CContainsLoopU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CContainsLoopU3Ek__BackingField_22)); }
	inline bool get_U3CContainsLoopU3Ek__BackingField_22() const { return ___U3CContainsLoopU3Ek__BackingField_22; }
	inline bool* get_address_of_U3CContainsLoopU3Ek__BackingField_22() { return &___U3CContainsLoopU3Ek__BackingField_22; }
	inline void set_U3CContainsLoopU3Ek__BackingField_22(bool value)
	{
		___U3CContainsLoopU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CShouldRevertOnNextPlayU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ___U3CShouldRevertOnNextPlayU3Ek__BackingField_23)); }
	inline bool get_U3CShouldRevertOnNextPlayU3Ek__BackingField_23() const { return ___U3CShouldRevertOnNextPlayU3Ek__BackingField_23; }
	inline bool* get_address_of_U3CShouldRevertOnNextPlayU3Ek__BackingField_23() { return &___U3CShouldRevertOnNextPlayU3Ek__BackingField_23; }
	inline void set_U3CShouldRevertOnNextPlayU3Ek__BackingField_23(bool value)
	{
		___U3CShouldRevertOnNextPlayU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of__startTime_24() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____startTime_24)); }
	inline float get__startTime_24() const { return ____startTime_24; }
	inline float* get_address_of__startTime_24() { return &____startTime_24; }
	inline void set__startTime_24(float value)
	{
		____startTime_24 = value;
	}

	inline static int32_t get_offset_of__holdingMax_25() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____holdingMax_25)); }
	inline float get__holdingMax_25() const { return ____holdingMax_25; }
	inline float* get_address_of__holdingMax_25() { return &____holdingMax_25; }
	inline void set__holdingMax_25(float value)
	{
		____holdingMax_25 = value;
	}

	inline static int32_t get_offset_of__lastStartAt_26() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____lastStartAt_26)); }
	inline float get__lastStartAt_26() const { return ____lastStartAt_26; }
	inline float* get_address_of__lastStartAt_26() { return &____lastStartAt_26; }
	inline void set__lastStartAt_26(float value)
	{
		____lastStartAt_26 = value;
	}

	inline static int32_t get_offset_of__pauseFound_27() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____pauseFound_27)); }
	inline bool get__pauseFound_27() const { return ____pauseFound_27; }
	inline bool* get_address_of__pauseFound_27() { return &____pauseFound_27; }
	inline void set__pauseFound_27(bool value)
	{
		____pauseFound_27 = value;
	}

	inline static int32_t get_offset_of__totalDuration_28() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914, ____totalDuration_28)); }
	inline float get__totalDuration_28() const { return ____totalDuration_28; }
	inline float* get_address_of__totalDuration_28() { return &____totalDuration_28; }
	inline void set__totalDuration_28(float value)
	{
		____totalDuration_28 = value;
	}
};

struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914_StaticFields
{
public:
	// System.Boolean MoreMountains.Feedbacks.MMFeedbacks::GlobalMMFeedbacksActive
	bool ___GlobalMMFeedbacksActive_18;

public:
	inline static int32_t get_offset_of_GlobalMMFeedbacksActive_18() { return static_cast<int32_t>(offsetof(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914_StaticFields, ___GlobalMMFeedbacksActive_18)); }
	inline bool get_GlobalMMFeedbacksActive_18() const { return ___GlobalMMFeedbacksActive_18; }
	inline bool* get_address_of_GlobalMMFeedbacksActive_18() { return &___GlobalMMFeedbacksActive_18; }
	inline void set_GlobalMMFeedbacksActive_18(bool value)
	{
		___GlobalMMFeedbacksActive_18 = value;
	}
};


// MoreMountains.Tools.MMMonoBehaviour
struct MMMonoBehaviour_tCF4A374C5050AB4B331002CA4B9CF6ACA984B03C  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// MoreMountains.CorgiEngine.Room
struct Room_t392D52D203A7F630C190D47A07148A57F4020B42  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Cinemachine.CinemachineVirtualCamera MoreMountains.CorgiEngine.Room::VirtualCamera
	CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * ___VirtualCamera_4;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.Room::Confiner
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___Confiner_5;
	// Cinemachine.CinemachineConfiner MoreMountains.CorgiEngine.Room::CinemachineCameraConfiner
	CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * ___CinemachineCameraConfiner_6;
	// System.Boolean MoreMountains.CorgiEngine.Room::ResizeConfinerAutomatically
	bool ___ResizeConfinerAutomatically_7;
	// System.Boolean MoreMountains.CorgiEngine.Room::AutoDetectFirstRoomOnStart
	bool ___AutoDetectFirstRoomOnStart_8;
	// System.Boolean MoreMountains.CorgiEngine.Room::CurrentRoom
	bool ___CurrentRoom_9;
	// System.Boolean MoreMountains.CorgiEngine.Room::RoomVisited
	bool ___RoomVisited_10;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.Room::OnPlayerEntersRoomForTheFirstTime
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnPlayerEntersRoomForTheFirstTime_11;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.Room::OnPlayerEntersRoom
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnPlayerEntersRoom_12;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.Room::OnPlayerExitsRoom
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnPlayerExitsRoom_13;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.Room::ActivationList
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___ActivationList_14;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.Room::_roomCollider2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____roomCollider2D_15;
	// UnityEngine.Camera MoreMountains.CorgiEngine.Room::_mainCamera
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * ____mainCamera_16;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.Room::_cameraSize
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____cameraSize_17;
	// System.Boolean MoreMountains.CorgiEngine.Room::_initialized
	bool ____initialized_18;

public:
	inline static int32_t get_offset_of_VirtualCamera_4() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___VirtualCamera_4)); }
	inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * get_VirtualCamera_4() const { return ___VirtualCamera_4; }
	inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C ** get_address_of_VirtualCamera_4() { return &___VirtualCamera_4; }
	inline void set_VirtualCamera_4(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * value)
	{
		___VirtualCamera_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VirtualCamera_4), (void*)value);
	}

	inline static int32_t get_offset_of_Confiner_5() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___Confiner_5)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_Confiner_5() const { return ___Confiner_5; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_Confiner_5() { return &___Confiner_5; }
	inline void set_Confiner_5(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___Confiner_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Confiner_5), (void*)value);
	}

	inline static int32_t get_offset_of_CinemachineCameraConfiner_6() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___CinemachineCameraConfiner_6)); }
	inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * get_CinemachineCameraConfiner_6() const { return ___CinemachineCameraConfiner_6; }
	inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D ** get_address_of_CinemachineCameraConfiner_6() { return &___CinemachineCameraConfiner_6; }
	inline void set_CinemachineCameraConfiner_6(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * value)
	{
		___CinemachineCameraConfiner_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CinemachineCameraConfiner_6), (void*)value);
	}

	inline static int32_t get_offset_of_ResizeConfinerAutomatically_7() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___ResizeConfinerAutomatically_7)); }
	inline bool get_ResizeConfinerAutomatically_7() const { return ___ResizeConfinerAutomatically_7; }
	inline bool* get_address_of_ResizeConfinerAutomatically_7() { return &___ResizeConfinerAutomatically_7; }
	inline void set_ResizeConfinerAutomatically_7(bool value)
	{
		___ResizeConfinerAutomatically_7 = value;
	}

	inline static int32_t get_offset_of_AutoDetectFirstRoomOnStart_8() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___AutoDetectFirstRoomOnStart_8)); }
	inline bool get_AutoDetectFirstRoomOnStart_8() const { return ___AutoDetectFirstRoomOnStart_8; }
	inline bool* get_address_of_AutoDetectFirstRoomOnStart_8() { return &___AutoDetectFirstRoomOnStart_8; }
	inline void set_AutoDetectFirstRoomOnStart_8(bool value)
	{
		___AutoDetectFirstRoomOnStart_8 = value;
	}

	inline static int32_t get_offset_of_CurrentRoom_9() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___CurrentRoom_9)); }
	inline bool get_CurrentRoom_9() const { return ___CurrentRoom_9; }
	inline bool* get_address_of_CurrentRoom_9() { return &___CurrentRoom_9; }
	inline void set_CurrentRoom_9(bool value)
	{
		___CurrentRoom_9 = value;
	}

	inline static int32_t get_offset_of_RoomVisited_10() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___RoomVisited_10)); }
	inline bool get_RoomVisited_10() const { return ___RoomVisited_10; }
	inline bool* get_address_of_RoomVisited_10() { return &___RoomVisited_10; }
	inline void set_RoomVisited_10(bool value)
	{
		___RoomVisited_10 = value;
	}

	inline static int32_t get_offset_of_OnPlayerEntersRoomForTheFirstTime_11() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___OnPlayerEntersRoomForTheFirstTime_11)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnPlayerEntersRoomForTheFirstTime_11() const { return ___OnPlayerEntersRoomForTheFirstTime_11; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnPlayerEntersRoomForTheFirstTime_11() { return &___OnPlayerEntersRoomForTheFirstTime_11; }
	inline void set_OnPlayerEntersRoomForTheFirstTime_11(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnPlayerEntersRoomForTheFirstTime_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPlayerEntersRoomForTheFirstTime_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnPlayerEntersRoom_12() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___OnPlayerEntersRoom_12)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnPlayerEntersRoom_12() const { return ___OnPlayerEntersRoom_12; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnPlayerEntersRoom_12() { return &___OnPlayerEntersRoom_12; }
	inline void set_OnPlayerEntersRoom_12(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnPlayerEntersRoom_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPlayerEntersRoom_12), (void*)value);
	}

	inline static int32_t get_offset_of_OnPlayerExitsRoom_13() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___OnPlayerExitsRoom_13)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnPlayerExitsRoom_13() const { return ___OnPlayerExitsRoom_13; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnPlayerExitsRoom_13() { return &___OnPlayerExitsRoom_13; }
	inline void set_OnPlayerExitsRoom_13(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnPlayerExitsRoom_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnPlayerExitsRoom_13), (void*)value);
	}

	inline static int32_t get_offset_of_ActivationList_14() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ___ActivationList_14)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_ActivationList_14() const { return ___ActivationList_14; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_ActivationList_14() { return &___ActivationList_14; }
	inline void set_ActivationList_14(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___ActivationList_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ActivationList_14), (void*)value);
	}

	inline static int32_t get_offset_of__roomCollider2D_15() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ____roomCollider2D_15)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__roomCollider2D_15() const { return ____roomCollider2D_15; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__roomCollider2D_15() { return &____roomCollider2D_15; }
	inline void set__roomCollider2D_15(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____roomCollider2D_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____roomCollider2D_15), (void*)value);
	}

	inline static int32_t get_offset_of__mainCamera_16() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ____mainCamera_16)); }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * get__mainCamera_16() const { return ____mainCamera_16; }
	inline Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C ** get_address_of__mainCamera_16() { return &____mainCamera_16; }
	inline void set__mainCamera_16(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * value)
	{
		____mainCamera_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mainCamera_16), (void*)value);
	}

	inline static int32_t get_offset_of__cameraSize_17() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ____cameraSize_17)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__cameraSize_17() const { return ____cameraSize_17; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__cameraSize_17() { return &____cameraSize_17; }
	inline void set__cameraSize_17(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____cameraSize_17 = value;
	}

	inline static int32_t get_offset_of__initialized_18() { return static_cast<int32_t>(offsetof(Room_t392D52D203A7F630C190D47A07148A57F4020B42, ____initialized_18)); }
	inline bool get__initialized_18() const { return ____initialized_18; }
	inline bool* get_address_of__initialized_18() { return &____initialized_18; }
	inline void set__initialized_18(bool value)
	{
		____initialized_18 = value;
	}
};


// MoreMountains.CorgiEngine.ButtonActivated
struct ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5  : public MMMonoBehaviour_tCF4A374C5050AB4B331002CA4B9CF6ACA984B03C
{
public:
	// MoreMountains.CorgiEngine.ButtonActivated/ButtonActivatedRequirements MoreMountains.CorgiEngine.ButtonActivated::ButtonActivatedRequirement
	int32_t ___ButtonActivatedRequirement_4;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::RequiresPlayerType
	bool ___RequiresPlayerType_5;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::RequiresButtonActivationAbility
	bool ___RequiresButtonActivationAbility_6;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::PreventJumpsWhileInThisZone
	bool ___PreventJumpsWhileInThisZone_7;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::Activable
	bool ___Activable_8;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AutoActivation
	bool ___AutoActivation_9;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AutoActivationAndButtonInteraction
	bool ___AutoActivationAndButtonInteraction_10;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::CanOnlyActivateIfGrounded
	bool ___CanOnlyActivateIfGrounded_11;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::ShouldUpdateState
	bool ___ShouldUpdateState_12;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::OnlyOneActivationAtOnce
	bool ___OnlyOneActivationAtOnce_13;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AlsoPerformChecksOnStay
	bool ___AlsoPerformChecksOnStay_14;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::UnlimitedActivations
	bool ___UnlimitedActivations_15;
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated::MaxNumberOfActivations
	int32_t ___MaxNumberOfActivations_16;
	// System.Single MoreMountains.CorgiEngine.ButtonActivated::DelayBetweenUses
	float ___DelayBetweenUses_17;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::DisableAfterUse
	bool ___DisableAfterUse_18;
	// MoreMountains.CorgiEngine.ButtonActivated/InputTypes MoreMountains.CorgiEngine.ButtonActivated::InputType
	int32_t ___InputType_19;
	// System.String MoreMountains.CorgiEngine.ButtonActivated::InputButton
	String_t* ___InputButton_20;
	// UnityEngine.KeyCode MoreMountains.CorgiEngine.ButtonActivated::InputKey
	int32_t ___InputKey_21;
	// System.String MoreMountains.CorgiEngine.ButtonActivated::AnimationTriggerParameterName
	String_t* ___AnimationTriggerParameterName_22;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::UseVisualPrompt
	bool ___UseVisualPrompt_23;
	// MoreMountains.CorgiEngine.ButtonPrompt MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptPrefab
	ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * ___ButtonPromptPrefab_24;
	// System.String MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptText
	String_t* ___ButtonPromptText_25;
	// UnityEngine.Color MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ButtonPromptColor_26;
	// UnityEngine.Color MoreMountains.CorgiEngine.ButtonActivated::ButtonPromptTextColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___ButtonPromptTextColor_27;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::AlwaysShowPrompt
	bool ___AlwaysShowPrompt_28;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::ShowPromptWhenColliding
	bool ___ShowPromptWhenColliding_29;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::HidePromptAfterUse
	bool ___HidePromptAfterUse_30;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.ButtonActivated::PromptRelativePosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___PromptRelativePosition_31;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::ActivationFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___ActivationFeedback_32;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::DeniedFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___DeniedFeedback_33;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::EnterFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___EnterFeedback_34;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.ButtonActivated::ExitFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___ExitFeedback_35;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.ButtonActivated::OnActivation
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnActivation_36;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.ButtonActivated::OnExit
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnExit_37;
	// UnityEngine.Events.UnityEvent MoreMountains.CorgiEngine.ButtonActivated::OnStay
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___OnStay_38;
	// UnityEngine.Animator MoreMountains.CorgiEngine.ButtonActivated::_buttonPromptAnimator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____buttonPromptAnimator_39;
	// MoreMountains.CorgiEngine.ButtonPrompt MoreMountains.CorgiEngine.ButtonActivated::_buttonPrompt
	ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * ____buttonPrompt_40;
	// System.Boolean MoreMountains.CorgiEngine.ButtonActivated::_promptHiddenForever
	bool ____promptHiddenForever_41;
	// System.Int32 MoreMountains.CorgiEngine.ButtonActivated::_numberOfActivationsLeft
	int32_t ____numberOfActivationsLeft_42;
	// System.Single MoreMountains.CorgiEngine.ButtonActivated::_lastActivationTimestamp
	float ____lastActivationTimestamp_43;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.ButtonActivated::_buttonActivatedZoneCollider
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____buttonActivatedZoneCollider_44;
	// MoreMountains.CorgiEngine.CharacterButtonActivation MoreMountains.CorgiEngine.ButtonActivated::_characterButtonActivation
	CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B * ____characterButtonActivation_45;
	// MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.ButtonActivated::_currentCharacter
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ____currentCharacter_46;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.ButtonActivated::_collidingObjects
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ____collidingObjects_47;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MoreMountains.CorgiEngine.ButtonActivated::_stayingGameObjects
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ____stayingGameObjects_48;
	// System.Collections.Generic.List`1<UnityEngine.Collider2D> MoreMountains.CorgiEngine.ButtonActivated::_enteredColliders
	List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 * ____enteredColliders_49;

public:
	inline static int32_t get_offset_of_ButtonActivatedRequirement_4() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonActivatedRequirement_4)); }
	inline int32_t get_ButtonActivatedRequirement_4() const { return ___ButtonActivatedRequirement_4; }
	inline int32_t* get_address_of_ButtonActivatedRequirement_4() { return &___ButtonActivatedRequirement_4; }
	inline void set_ButtonActivatedRequirement_4(int32_t value)
	{
		___ButtonActivatedRequirement_4 = value;
	}

	inline static int32_t get_offset_of_RequiresPlayerType_5() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___RequiresPlayerType_5)); }
	inline bool get_RequiresPlayerType_5() const { return ___RequiresPlayerType_5; }
	inline bool* get_address_of_RequiresPlayerType_5() { return &___RequiresPlayerType_5; }
	inline void set_RequiresPlayerType_5(bool value)
	{
		___RequiresPlayerType_5 = value;
	}

	inline static int32_t get_offset_of_RequiresButtonActivationAbility_6() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___RequiresButtonActivationAbility_6)); }
	inline bool get_RequiresButtonActivationAbility_6() const { return ___RequiresButtonActivationAbility_6; }
	inline bool* get_address_of_RequiresButtonActivationAbility_6() { return &___RequiresButtonActivationAbility_6; }
	inline void set_RequiresButtonActivationAbility_6(bool value)
	{
		___RequiresButtonActivationAbility_6 = value;
	}

	inline static int32_t get_offset_of_PreventJumpsWhileInThisZone_7() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___PreventJumpsWhileInThisZone_7)); }
	inline bool get_PreventJumpsWhileInThisZone_7() const { return ___PreventJumpsWhileInThisZone_7; }
	inline bool* get_address_of_PreventJumpsWhileInThisZone_7() { return &___PreventJumpsWhileInThisZone_7; }
	inline void set_PreventJumpsWhileInThisZone_7(bool value)
	{
		___PreventJumpsWhileInThisZone_7 = value;
	}

	inline static int32_t get_offset_of_Activable_8() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___Activable_8)); }
	inline bool get_Activable_8() const { return ___Activable_8; }
	inline bool* get_address_of_Activable_8() { return &___Activable_8; }
	inline void set_Activable_8(bool value)
	{
		___Activable_8 = value;
	}

	inline static int32_t get_offset_of_AutoActivation_9() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AutoActivation_9)); }
	inline bool get_AutoActivation_9() const { return ___AutoActivation_9; }
	inline bool* get_address_of_AutoActivation_9() { return &___AutoActivation_9; }
	inline void set_AutoActivation_9(bool value)
	{
		___AutoActivation_9 = value;
	}

	inline static int32_t get_offset_of_AutoActivationAndButtonInteraction_10() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AutoActivationAndButtonInteraction_10)); }
	inline bool get_AutoActivationAndButtonInteraction_10() const { return ___AutoActivationAndButtonInteraction_10; }
	inline bool* get_address_of_AutoActivationAndButtonInteraction_10() { return &___AutoActivationAndButtonInteraction_10; }
	inline void set_AutoActivationAndButtonInteraction_10(bool value)
	{
		___AutoActivationAndButtonInteraction_10 = value;
	}

	inline static int32_t get_offset_of_CanOnlyActivateIfGrounded_11() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___CanOnlyActivateIfGrounded_11)); }
	inline bool get_CanOnlyActivateIfGrounded_11() const { return ___CanOnlyActivateIfGrounded_11; }
	inline bool* get_address_of_CanOnlyActivateIfGrounded_11() { return &___CanOnlyActivateIfGrounded_11; }
	inline void set_CanOnlyActivateIfGrounded_11(bool value)
	{
		___CanOnlyActivateIfGrounded_11 = value;
	}

	inline static int32_t get_offset_of_ShouldUpdateState_12() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ShouldUpdateState_12)); }
	inline bool get_ShouldUpdateState_12() const { return ___ShouldUpdateState_12; }
	inline bool* get_address_of_ShouldUpdateState_12() { return &___ShouldUpdateState_12; }
	inline void set_ShouldUpdateState_12(bool value)
	{
		___ShouldUpdateState_12 = value;
	}

	inline static int32_t get_offset_of_OnlyOneActivationAtOnce_13() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnlyOneActivationAtOnce_13)); }
	inline bool get_OnlyOneActivationAtOnce_13() const { return ___OnlyOneActivationAtOnce_13; }
	inline bool* get_address_of_OnlyOneActivationAtOnce_13() { return &___OnlyOneActivationAtOnce_13; }
	inline void set_OnlyOneActivationAtOnce_13(bool value)
	{
		___OnlyOneActivationAtOnce_13 = value;
	}

	inline static int32_t get_offset_of_AlsoPerformChecksOnStay_14() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AlsoPerformChecksOnStay_14)); }
	inline bool get_AlsoPerformChecksOnStay_14() const { return ___AlsoPerformChecksOnStay_14; }
	inline bool* get_address_of_AlsoPerformChecksOnStay_14() { return &___AlsoPerformChecksOnStay_14; }
	inline void set_AlsoPerformChecksOnStay_14(bool value)
	{
		___AlsoPerformChecksOnStay_14 = value;
	}

	inline static int32_t get_offset_of_UnlimitedActivations_15() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___UnlimitedActivations_15)); }
	inline bool get_UnlimitedActivations_15() const { return ___UnlimitedActivations_15; }
	inline bool* get_address_of_UnlimitedActivations_15() { return &___UnlimitedActivations_15; }
	inline void set_UnlimitedActivations_15(bool value)
	{
		___UnlimitedActivations_15 = value;
	}

	inline static int32_t get_offset_of_MaxNumberOfActivations_16() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___MaxNumberOfActivations_16)); }
	inline int32_t get_MaxNumberOfActivations_16() const { return ___MaxNumberOfActivations_16; }
	inline int32_t* get_address_of_MaxNumberOfActivations_16() { return &___MaxNumberOfActivations_16; }
	inline void set_MaxNumberOfActivations_16(int32_t value)
	{
		___MaxNumberOfActivations_16 = value;
	}

	inline static int32_t get_offset_of_DelayBetweenUses_17() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___DelayBetweenUses_17)); }
	inline float get_DelayBetweenUses_17() const { return ___DelayBetweenUses_17; }
	inline float* get_address_of_DelayBetweenUses_17() { return &___DelayBetweenUses_17; }
	inline void set_DelayBetweenUses_17(float value)
	{
		___DelayBetweenUses_17 = value;
	}

	inline static int32_t get_offset_of_DisableAfterUse_18() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___DisableAfterUse_18)); }
	inline bool get_DisableAfterUse_18() const { return ___DisableAfterUse_18; }
	inline bool* get_address_of_DisableAfterUse_18() { return &___DisableAfterUse_18; }
	inline void set_DisableAfterUse_18(bool value)
	{
		___DisableAfterUse_18 = value;
	}

	inline static int32_t get_offset_of_InputType_19() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___InputType_19)); }
	inline int32_t get_InputType_19() const { return ___InputType_19; }
	inline int32_t* get_address_of_InputType_19() { return &___InputType_19; }
	inline void set_InputType_19(int32_t value)
	{
		___InputType_19 = value;
	}

	inline static int32_t get_offset_of_InputButton_20() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___InputButton_20)); }
	inline String_t* get_InputButton_20() const { return ___InputButton_20; }
	inline String_t** get_address_of_InputButton_20() { return &___InputButton_20; }
	inline void set_InputButton_20(String_t* value)
	{
		___InputButton_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputButton_20), (void*)value);
	}

	inline static int32_t get_offset_of_InputKey_21() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___InputKey_21)); }
	inline int32_t get_InputKey_21() const { return ___InputKey_21; }
	inline int32_t* get_address_of_InputKey_21() { return &___InputKey_21; }
	inline void set_InputKey_21(int32_t value)
	{
		___InputKey_21 = value;
	}

	inline static int32_t get_offset_of_AnimationTriggerParameterName_22() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AnimationTriggerParameterName_22)); }
	inline String_t* get_AnimationTriggerParameterName_22() const { return ___AnimationTriggerParameterName_22; }
	inline String_t** get_address_of_AnimationTriggerParameterName_22() { return &___AnimationTriggerParameterName_22; }
	inline void set_AnimationTriggerParameterName_22(String_t* value)
	{
		___AnimationTriggerParameterName_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AnimationTriggerParameterName_22), (void*)value);
	}

	inline static int32_t get_offset_of_UseVisualPrompt_23() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___UseVisualPrompt_23)); }
	inline bool get_UseVisualPrompt_23() const { return ___UseVisualPrompt_23; }
	inline bool* get_address_of_UseVisualPrompt_23() { return &___UseVisualPrompt_23; }
	inline void set_UseVisualPrompt_23(bool value)
	{
		___UseVisualPrompt_23 = value;
	}

	inline static int32_t get_offset_of_ButtonPromptPrefab_24() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptPrefab_24)); }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * get_ButtonPromptPrefab_24() const { return ___ButtonPromptPrefab_24; }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 ** get_address_of_ButtonPromptPrefab_24() { return &___ButtonPromptPrefab_24; }
	inline void set_ButtonPromptPrefab_24(ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * value)
	{
		___ButtonPromptPrefab_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPromptPrefab_24), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPromptText_25() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptText_25)); }
	inline String_t* get_ButtonPromptText_25() const { return ___ButtonPromptText_25; }
	inline String_t** get_address_of_ButtonPromptText_25() { return &___ButtonPromptText_25; }
	inline void set_ButtonPromptText_25(String_t* value)
	{
		___ButtonPromptText_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPromptText_25), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPromptColor_26() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptColor_26)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ButtonPromptColor_26() const { return ___ButtonPromptColor_26; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ButtonPromptColor_26() { return &___ButtonPromptColor_26; }
	inline void set_ButtonPromptColor_26(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ButtonPromptColor_26 = value;
	}

	inline static int32_t get_offset_of_ButtonPromptTextColor_27() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ButtonPromptTextColor_27)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_ButtonPromptTextColor_27() const { return ___ButtonPromptTextColor_27; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_ButtonPromptTextColor_27() { return &___ButtonPromptTextColor_27; }
	inline void set_ButtonPromptTextColor_27(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___ButtonPromptTextColor_27 = value;
	}

	inline static int32_t get_offset_of_AlwaysShowPrompt_28() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___AlwaysShowPrompt_28)); }
	inline bool get_AlwaysShowPrompt_28() const { return ___AlwaysShowPrompt_28; }
	inline bool* get_address_of_AlwaysShowPrompt_28() { return &___AlwaysShowPrompt_28; }
	inline void set_AlwaysShowPrompt_28(bool value)
	{
		___AlwaysShowPrompt_28 = value;
	}

	inline static int32_t get_offset_of_ShowPromptWhenColliding_29() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ShowPromptWhenColliding_29)); }
	inline bool get_ShowPromptWhenColliding_29() const { return ___ShowPromptWhenColliding_29; }
	inline bool* get_address_of_ShowPromptWhenColliding_29() { return &___ShowPromptWhenColliding_29; }
	inline void set_ShowPromptWhenColliding_29(bool value)
	{
		___ShowPromptWhenColliding_29 = value;
	}

	inline static int32_t get_offset_of_HidePromptAfterUse_30() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___HidePromptAfterUse_30)); }
	inline bool get_HidePromptAfterUse_30() const { return ___HidePromptAfterUse_30; }
	inline bool* get_address_of_HidePromptAfterUse_30() { return &___HidePromptAfterUse_30; }
	inline void set_HidePromptAfterUse_30(bool value)
	{
		___HidePromptAfterUse_30 = value;
	}

	inline static int32_t get_offset_of_PromptRelativePosition_31() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___PromptRelativePosition_31)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_PromptRelativePosition_31() const { return ___PromptRelativePosition_31; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_PromptRelativePosition_31() { return &___PromptRelativePosition_31; }
	inline void set_PromptRelativePosition_31(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___PromptRelativePosition_31 = value;
	}

	inline static int32_t get_offset_of_ActivationFeedback_32() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ActivationFeedback_32)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_ActivationFeedback_32() const { return ___ActivationFeedback_32; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_ActivationFeedback_32() { return &___ActivationFeedback_32; }
	inline void set_ActivationFeedback_32(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___ActivationFeedback_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ActivationFeedback_32), (void*)value);
	}

	inline static int32_t get_offset_of_DeniedFeedback_33() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___DeniedFeedback_33)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_DeniedFeedback_33() const { return ___DeniedFeedback_33; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_DeniedFeedback_33() { return &___DeniedFeedback_33; }
	inline void set_DeniedFeedback_33(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___DeniedFeedback_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DeniedFeedback_33), (void*)value);
	}

	inline static int32_t get_offset_of_EnterFeedback_34() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___EnterFeedback_34)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_EnterFeedback_34() const { return ___EnterFeedback_34; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_EnterFeedback_34() { return &___EnterFeedback_34; }
	inline void set_EnterFeedback_34(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___EnterFeedback_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnterFeedback_34), (void*)value);
	}

	inline static int32_t get_offset_of_ExitFeedback_35() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___ExitFeedback_35)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_ExitFeedback_35() const { return ___ExitFeedback_35; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_ExitFeedback_35() { return &___ExitFeedback_35; }
	inline void set_ExitFeedback_35(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___ExitFeedback_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ExitFeedback_35), (void*)value);
	}

	inline static int32_t get_offset_of_OnActivation_36() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnActivation_36)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnActivation_36() const { return ___OnActivation_36; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnActivation_36() { return &___OnActivation_36; }
	inline void set_OnActivation_36(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnActivation_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnActivation_36), (void*)value);
	}

	inline static int32_t get_offset_of_OnExit_37() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnExit_37)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnExit_37() const { return ___OnExit_37; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnExit_37() { return &___OnExit_37; }
	inline void set_OnExit_37(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnExit_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnExit_37), (void*)value);
	}

	inline static int32_t get_offset_of_OnStay_38() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ___OnStay_38)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_OnStay_38() const { return ___OnStay_38; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_OnStay_38() { return &___OnStay_38; }
	inline void set_OnStay_38(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___OnStay_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStay_38), (void*)value);
	}

	inline static int32_t get_offset_of__buttonPromptAnimator_39() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____buttonPromptAnimator_39)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__buttonPromptAnimator_39() const { return ____buttonPromptAnimator_39; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__buttonPromptAnimator_39() { return &____buttonPromptAnimator_39; }
	inline void set__buttonPromptAnimator_39(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____buttonPromptAnimator_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonPromptAnimator_39), (void*)value);
	}

	inline static int32_t get_offset_of__buttonPrompt_40() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____buttonPrompt_40)); }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * get__buttonPrompt_40() const { return ____buttonPrompt_40; }
	inline ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 ** get_address_of__buttonPrompt_40() { return &____buttonPrompt_40; }
	inline void set__buttonPrompt_40(ButtonPrompt_tE3F5BABE8645442D14A8B6A1B4A56EE3D550DB03 * value)
	{
		____buttonPrompt_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonPrompt_40), (void*)value);
	}

	inline static int32_t get_offset_of__promptHiddenForever_41() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____promptHiddenForever_41)); }
	inline bool get__promptHiddenForever_41() const { return ____promptHiddenForever_41; }
	inline bool* get_address_of__promptHiddenForever_41() { return &____promptHiddenForever_41; }
	inline void set__promptHiddenForever_41(bool value)
	{
		____promptHiddenForever_41 = value;
	}

	inline static int32_t get_offset_of__numberOfActivationsLeft_42() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____numberOfActivationsLeft_42)); }
	inline int32_t get__numberOfActivationsLeft_42() const { return ____numberOfActivationsLeft_42; }
	inline int32_t* get_address_of__numberOfActivationsLeft_42() { return &____numberOfActivationsLeft_42; }
	inline void set__numberOfActivationsLeft_42(int32_t value)
	{
		____numberOfActivationsLeft_42 = value;
	}

	inline static int32_t get_offset_of__lastActivationTimestamp_43() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____lastActivationTimestamp_43)); }
	inline float get__lastActivationTimestamp_43() const { return ____lastActivationTimestamp_43; }
	inline float* get_address_of__lastActivationTimestamp_43() { return &____lastActivationTimestamp_43; }
	inline void set__lastActivationTimestamp_43(float value)
	{
		____lastActivationTimestamp_43 = value;
	}

	inline static int32_t get_offset_of__buttonActivatedZoneCollider_44() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____buttonActivatedZoneCollider_44)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__buttonActivatedZoneCollider_44() const { return ____buttonActivatedZoneCollider_44; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__buttonActivatedZoneCollider_44() { return &____buttonActivatedZoneCollider_44; }
	inline void set__buttonActivatedZoneCollider_44(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____buttonActivatedZoneCollider_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____buttonActivatedZoneCollider_44), (void*)value);
	}

	inline static int32_t get_offset_of__characterButtonActivation_45() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____characterButtonActivation_45)); }
	inline CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B * get__characterButtonActivation_45() const { return ____characterButtonActivation_45; }
	inline CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B ** get_address_of__characterButtonActivation_45() { return &____characterButtonActivation_45; }
	inline void set__characterButtonActivation_45(CharacterButtonActivation_tBF675D2472BAE090AA64F7F57EF9C8814063C06B * value)
	{
		____characterButtonActivation_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____characterButtonActivation_45), (void*)value);
	}

	inline static int32_t get_offset_of__currentCharacter_46() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____currentCharacter_46)); }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * get__currentCharacter_46() const { return ____currentCharacter_46; }
	inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 ** get_address_of__currentCharacter_46() { return &____currentCharacter_46; }
	inline void set__currentCharacter_46(Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * value)
	{
		____currentCharacter_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentCharacter_46), (void*)value);
	}

	inline static int32_t get_offset_of__collidingObjects_47() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____collidingObjects_47)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get__collidingObjects_47() const { return ____collidingObjects_47; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of__collidingObjects_47() { return &____collidingObjects_47; }
	inline void set__collidingObjects_47(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		____collidingObjects_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collidingObjects_47), (void*)value);
	}

	inline static int32_t get_offset_of__stayingGameObjects_48() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____stayingGameObjects_48)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get__stayingGameObjects_48() const { return ____stayingGameObjects_48; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of__stayingGameObjects_48() { return &____stayingGameObjects_48; }
	inline void set__stayingGameObjects_48(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		____stayingGameObjects_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stayingGameObjects_48), (void*)value);
	}

	inline static int32_t get_offset_of__enteredColliders_49() { return static_cast<int32_t>(offsetof(ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5, ____enteredColliders_49)); }
	inline List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 * get__enteredColliders_49() const { return ____enteredColliders_49; }
	inline List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 ** get_address_of__enteredColliders_49() { return &____enteredColliders_49; }
	inline void set__enteredColliders_49(List_1_t103D91044D260C2ED8E0166A083D8989CD4AD998 * value)
	{
		____enteredColliders_49 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____enteredColliders_49), (void*)value);
	}
};


// MoreMountains.CorgiEngine.CinemachineAxisLocker
struct CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB  : public CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422
{
public:
	// System.Boolean MoreMountains.CorgiEngine.CinemachineAxisLocker::LockXAxis
	bool ___LockXAxis_7;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineAxisLocker::LockYAxis
	bool ___LockYAxis_8;
	// System.Boolean MoreMountains.CorgiEngine.CinemachineAxisLocker::LockZAxis
	bool ___LockZAxis_9;
	// MoreMountains.CorgiEngine.CinemachineAxisLocker/Methods MoreMountains.CorgiEngine.CinemachineAxisLocker::Method
	int32_t ___Method_10;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CinemachineAxisLocker::ForcedPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___ForcedPosition_11;
	// UnityEngine.Collider MoreMountains.CorgiEngine.CinemachineAxisLocker::TargetCollider
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___TargetCollider_12;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.CinemachineAxisLocker::TargetCollider2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___TargetCollider2D_13;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.CinemachineAxisLocker::_forcedPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____forcedPosition_14;

public:
	inline static int32_t get_offset_of_LockXAxis_7() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___LockXAxis_7)); }
	inline bool get_LockXAxis_7() const { return ___LockXAxis_7; }
	inline bool* get_address_of_LockXAxis_7() { return &___LockXAxis_7; }
	inline void set_LockXAxis_7(bool value)
	{
		___LockXAxis_7 = value;
	}

	inline static int32_t get_offset_of_LockYAxis_8() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___LockYAxis_8)); }
	inline bool get_LockYAxis_8() const { return ___LockYAxis_8; }
	inline bool* get_address_of_LockYAxis_8() { return &___LockYAxis_8; }
	inline void set_LockYAxis_8(bool value)
	{
		___LockYAxis_8 = value;
	}

	inline static int32_t get_offset_of_LockZAxis_9() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___LockZAxis_9)); }
	inline bool get_LockZAxis_9() const { return ___LockZAxis_9; }
	inline bool* get_address_of_LockZAxis_9() { return &___LockZAxis_9; }
	inline void set_LockZAxis_9(bool value)
	{
		___LockZAxis_9 = value;
	}

	inline static int32_t get_offset_of_Method_10() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___Method_10)); }
	inline int32_t get_Method_10() const { return ___Method_10; }
	inline int32_t* get_address_of_Method_10() { return &___Method_10; }
	inline void set_Method_10(int32_t value)
	{
		___Method_10 = value;
	}

	inline static int32_t get_offset_of_ForcedPosition_11() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___ForcedPosition_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_ForcedPosition_11() const { return ___ForcedPosition_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_ForcedPosition_11() { return &___ForcedPosition_11; }
	inline void set_ForcedPosition_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___ForcedPosition_11 = value;
	}

	inline static int32_t get_offset_of_TargetCollider_12() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___TargetCollider_12)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_TargetCollider_12() const { return ___TargetCollider_12; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_TargetCollider_12() { return &___TargetCollider_12; }
	inline void set_TargetCollider_12(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___TargetCollider_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetCollider_12), (void*)value);
	}

	inline static int32_t get_offset_of_TargetCollider2D_13() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ___TargetCollider2D_13)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_TargetCollider2D_13() const { return ___TargetCollider2D_13; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_TargetCollider2D_13() { return &___TargetCollider2D_13; }
	inline void set_TargetCollider2D_13(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___TargetCollider2D_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetCollider2D_13), (void*)value);
	}

	inline static int32_t get_offset_of__forcedPosition_14() { return static_cast<int32_t>(offsetof(CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB, ____forcedPosition_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__forcedPosition_14() const { return ____forcedPosition_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__forcedPosition_14() { return &____forcedPosition_14; }
	inline void set__forcedPosition_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____forcedPosition_14 = value;
	}
};


// Cinemachine.CinemachineConfiner
struct CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D  : public CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422
{
public:
	// Cinemachine.CinemachineConfiner/Mode Cinemachine.CinemachineConfiner::m_ConfineMode
	int32_t ___m_ConfineMode_7;
	// UnityEngine.Collider Cinemachine.CinemachineConfiner::m_BoundingVolume
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___m_BoundingVolume_8;
	// UnityEngine.Collider2D Cinemachine.CinemachineConfiner::m_BoundingShape2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___m_BoundingShape2D_9;
	// UnityEngine.Collider2D Cinemachine.CinemachineConfiner::m_BoundingShape2DCache
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___m_BoundingShape2DCache_10;
	// System.Boolean Cinemachine.CinemachineConfiner::m_ConfineScreenEdges
	bool ___m_ConfineScreenEdges_11;
	// System.Single Cinemachine.CinemachineConfiner::m_Damping
	float ___m_Damping_12;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<UnityEngine.Vector2>> Cinemachine.CinemachineConfiner::m_pathCache
	List_1_t5128DCBC80975324FF27985AB2A9F3D29DACA894 * ___m_pathCache_13;
	// System.Int32 Cinemachine.CinemachineConfiner::m_pathTotalPointCount
	int32_t ___m_pathTotalPointCount_14;

public:
	inline static int32_t get_offset_of_m_ConfineMode_7() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_ConfineMode_7)); }
	inline int32_t get_m_ConfineMode_7() const { return ___m_ConfineMode_7; }
	inline int32_t* get_address_of_m_ConfineMode_7() { return &___m_ConfineMode_7; }
	inline void set_m_ConfineMode_7(int32_t value)
	{
		___m_ConfineMode_7 = value;
	}

	inline static int32_t get_offset_of_m_BoundingVolume_8() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_BoundingVolume_8)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_m_BoundingVolume_8() const { return ___m_BoundingVolume_8; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_m_BoundingVolume_8() { return &___m_BoundingVolume_8; }
	inline void set_m_BoundingVolume_8(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___m_BoundingVolume_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BoundingVolume_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_BoundingShape2D_9() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_BoundingShape2D_9)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_m_BoundingShape2D_9() const { return ___m_BoundingShape2D_9; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_m_BoundingShape2D_9() { return &___m_BoundingShape2D_9; }
	inline void set_m_BoundingShape2D_9(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___m_BoundingShape2D_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BoundingShape2D_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_BoundingShape2DCache_10() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_BoundingShape2DCache_10)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_m_BoundingShape2DCache_10() const { return ___m_BoundingShape2DCache_10; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_m_BoundingShape2DCache_10() { return &___m_BoundingShape2DCache_10; }
	inline void set_m_BoundingShape2DCache_10(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___m_BoundingShape2DCache_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BoundingShape2DCache_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_ConfineScreenEdges_11() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_ConfineScreenEdges_11)); }
	inline bool get_m_ConfineScreenEdges_11() const { return ___m_ConfineScreenEdges_11; }
	inline bool* get_address_of_m_ConfineScreenEdges_11() { return &___m_ConfineScreenEdges_11; }
	inline void set_m_ConfineScreenEdges_11(bool value)
	{
		___m_ConfineScreenEdges_11 = value;
	}

	inline static int32_t get_offset_of_m_Damping_12() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_Damping_12)); }
	inline float get_m_Damping_12() const { return ___m_Damping_12; }
	inline float* get_address_of_m_Damping_12() { return &___m_Damping_12; }
	inline void set_m_Damping_12(float value)
	{
		___m_Damping_12 = value;
	}

	inline static int32_t get_offset_of_m_pathCache_13() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_pathCache_13)); }
	inline List_1_t5128DCBC80975324FF27985AB2A9F3D29DACA894 * get_m_pathCache_13() const { return ___m_pathCache_13; }
	inline List_1_t5128DCBC80975324FF27985AB2A9F3D29DACA894 ** get_address_of_m_pathCache_13() { return &___m_pathCache_13; }
	inline void set_m_pathCache_13(List_1_t5128DCBC80975324FF27985AB2A9F3D29DACA894 * value)
	{
		___m_pathCache_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_pathCache_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_pathTotalPointCount_14() { return static_cast<int32_t>(offsetof(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D, ___m_pathTotalPointCount_14)); }
	inline int32_t get_m_pathTotalPointCount_14() const { return ___m_pathTotalPointCount_14; }
	inline int32_t* get_address_of_m_pathTotalPointCount_14() { return &___m_pathTotalPointCount_14; }
	inline void set_m_pathTotalPointCount_14(int32_t value)
	{
		___m_pathTotalPointCount_14 = value;
	}
};


// Cinemachine.CinemachineFramingTransposer
struct CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1  : public CinemachineComponentBase_tCB9029D861525FDAF243B8F6F5B910145DB63B7D
{
public:
	// UnityEngine.Vector3 Cinemachine.CinemachineFramingTransposer::m_TrackedObjectOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_TrackedObjectOffset_12;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_LookaheadTime
	float ___m_LookaheadTime_13;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_LookaheadSmoothing
	float ___m_LookaheadSmoothing_14;
	// System.Boolean Cinemachine.CinemachineFramingTransposer::m_LookaheadIgnoreY
	bool ___m_LookaheadIgnoreY_15;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_XDamping
	float ___m_XDamping_16;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_YDamping
	float ___m_YDamping_17;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_ZDamping
	float ___m_ZDamping_18;
	// System.Boolean Cinemachine.CinemachineFramingTransposer::m_TargetMovementOnly
	bool ___m_TargetMovementOnly_19;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_ScreenX
	float ___m_ScreenX_20;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_ScreenY
	float ___m_ScreenY_21;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_CameraDistance
	float ___m_CameraDistance_22;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_DeadZoneWidth
	float ___m_DeadZoneWidth_23;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_DeadZoneHeight
	float ___m_DeadZoneHeight_24;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_DeadZoneDepth
	float ___m_DeadZoneDepth_25;
	// System.Boolean Cinemachine.CinemachineFramingTransposer::m_UnlimitedSoftZone
	bool ___m_UnlimitedSoftZone_26;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_SoftZoneWidth
	float ___m_SoftZoneWidth_27;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_SoftZoneHeight
	float ___m_SoftZoneHeight_28;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_BiasX
	float ___m_BiasX_29;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_BiasY
	float ___m_BiasY_30;
	// System.Boolean Cinemachine.CinemachineFramingTransposer::m_CenterOnActivate
	bool ___m_CenterOnActivate_31;
	// Cinemachine.CinemachineFramingTransposer/FramingMode Cinemachine.CinemachineFramingTransposer::m_GroupFramingMode
	int32_t ___m_GroupFramingMode_32;
	// Cinemachine.CinemachineFramingTransposer/AdjustmentMode Cinemachine.CinemachineFramingTransposer::m_AdjustmentMode
	int32_t ___m_AdjustmentMode_33;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_GroupFramingSize
	float ___m_GroupFramingSize_34;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaxDollyIn
	float ___m_MaxDollyIn_35;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaxDollyOut
	float ___m_MaxDollyOut_36;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MinimumDistance
	float ___m_MinimumDistance_37;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaximumDistance
	float ___m_MaximumDistance_38;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MinimumFOV
	float ___m_MinimumFOV_39;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaximumFOV
	float ___m_MaximumFOV_40;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MinimumOrthoSize
	float ___m_MinimumOrthoSize_41;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_MaximumOrthoSize
	float ___m_MaximumOrthoSize_42;
	// UnityEngine.Vector3 Cinemachine.CinemachineFramingTransposer::m_PreviousCameraPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_PreviousCameraPosition_45;
	// Cinemachine.Utility.PositionPredictor Cinemachine.CinemachineFramingTransposer::m_Predictor
	PositionPredictor_tAC12D8D878524DFBAF37CBBF695B5D18B877F56A * ___m_Predictor_46;
	// UnityEngine.Vector3 Cinemachine.CinemachineFramingTransposer::<TrackedPoint>k__BackingField
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CTrackedPointU3Ek__BackingField_47;
	// System.Boolean Cinemachine.CinemachineFramingTransposer::<InheritingPosition>k__BackingField
	bool ___U3CInheritingPositionU3Ek__BackingField_48;
	// System.Single Cinemachine.CinemachineFramingTransposer::m_prevFOV
	float ___m_prevFOV_49;
	// UnityEngine.Quaternion Cinemachine.CinemachineFramingTransposer::m_prevRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___m_prevRotation_50;
	// UnityEngine.Bounds Cinemachine.CinemachineFramingTransposer::<LastBounds>k__BackingField
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___U3CLastBoundsU3Ek__BackingField_51;
	// UnityEngine.Matrix4x4 Cinemachine.CinemachineFramingTransposer::<LastBoundsMatrix>k__BackingField
	Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  ___U3CLastBoundsMatrixU3Ek__BackingField_52;

public:
	inline static int32_t get_offset_of_m_TrackedObjectOffset_12() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_TrackedObjectOffset_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_TrackedObjectOffset_12() const { return ___m_TrackedObjectOffset_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_TrackedObjectOffset_12() { return &___m_TrackedObjectOffset_12; }
	inline void set_m_TrackedObjectOffset_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_TrackedObjectOffset_12 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadTime_13() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_LookaheadTime_13)); }
	inline float get_m_LookaheadTime_13() const { return ___m_LookaheadTime_13; }
	inline float* get_address_of_m_LookaheadTime_13() { return &___m_LookaheadTime_13; }
	inline void set_m_LookaheadTime_13(float value)
	{
		___m_LookaheadTime_13 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadSmoothing_14() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_LookaheadSmoothing_14)); }
	inline float get_m_LookaheadSmoothing_14() const { return ___m_LookaheadSmoothing_14; }
	inline float* get_address_of_m_LookaheadSmoothing_14() { return &___m_LookaheadSmoothing_14; }
	inline void set_m_LookaheadSmoothing_14(float value)
	{
		___m_LookaheadSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_m_LookaheadIgnoreY_15() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_LookaheadIgnoreY_15)); }
	inline bool get_m_LookaheadIgnoreY_15() const { return ___m_LookaheadIgnoreY_15; }
	inline bool* get_address_of_m_LookaheadIgnoreY_15() { return &___m_LookaheadIgnoreY_15; }
	inline void set_m_LookaheadIgnoreY_15(bool value)
	{
		___m_LookaheadIgnoreY_15 = value;
	}

	inline static int32_t get_offset_of_m_XDamping_16() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_XDamping_16)); }
	inline float get_m_XDamping_16() const { return ___m_XDamping_16; }
	inline float* get_address_of_m_XDamping_16() { return &___m_XDamping_16; }
	inline void set_m_XDamping_16(float value)
	{
		___m_XDamping_16 = value;
	}

	inline static int32_t get_offset_of_m_YDamping_17() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_YDamping_17)); }
	inline float get_m_YDamping_17() const { return ___m_YDamping_17; }
	inline float* get_address_of_m_YDamping_17() { return &___m_YDamping_17; }
	inline void set_m_YDamping_17(float value)
	{
		___m_YDamping_17 = value;
	}

	inline static int32_t get_offset_of_m_ZDamping_18() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_ZDamping_18)); }
	inline float get_m_ZDamping_18() const { return ___m_ZDamping_18; }
	inline float* get_address_of_m_ZDamping_18() { return &___m_ZDamping_18; }
	inline void set_m_ZDamping_18(float value)
	{
		___m_ZDamping_18 = value;
	}

	inline static int32_t get_offset_of_m_TargetMovementOnly_19() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_TargetMovementOnly_19)); }
	inline bool get_m_TargetMovementOnly_19() const { return ___m_TargetMovementOnly_19; }
	inline bool* get_address_of_m_TargetMovementOnly_19() { return &___m_TargetMovementOnly_19; }
	inline void set_m_TargetMovementOnly_19(bool value)
	{
		___m_TargetMovementOnly_19 = value;
	}

	inline static int32_t get_offset_of_m_ScreenX_20() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_ScreenX_20)); }
	inline float get_m_ScreenX_20() const { return ___m_ScreenX_20; }
	inline float* get_address_of_m_ScreenX_20() { return &___m_ScreenX_20; }
	inline void set_m_ScreenX_20(float value)
	{
		___m_ScreenX_20 = value;
	}

	inline static int32_t get_offset_of_m_ScreenY_21() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_ScreenY_21)); }
	inline float get_m_ScreenY_21() const { return ___m_ScreenY_21; }
	inline float* get_address_of_m_ScreenY_21() { return &___m_ScreenY_21; }
	inline void set_m_ScreenY_21(float value)
	{
		___m_ScreenY_21 = value;
	}

	inline static int32_t get_offset_of_m_CameraDistance_22() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_CameraDistance_22)); }
	inline float get_m_CameraDistance_22() const { return ___m_CameraDistance_22; }
	inline float* get_address_of_m_CameraDistance_22() { return &___m_CameraDistance_22; }
	inline void set_m_CameraDistance_22(float value)
	{
		___m_CameraDistance_22 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneWidth_23() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_DeadZoneWidth_23)); }
	inline float get_m_DeadZoneWidth_23() const { return ___m_DeadZoneWidth_23; }
	inline float* get_address_of_m_DeadZoneWidth_23() { return &___m_DeadZoneWidth_23; }
	inline void set_m_DeadZoneWidth_23(float value)
	{
		___m_DeadZoneWidth_23 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneHeight_24() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_DeadZoneHeight_24)); }
	inline float get_m_DeadZoneHeight_24() const { return ___m_DeadZoneHeight_24; }
	inline float* get_address_of_m_DeadZoneHeight_24() { return &___m_DeadZoneHeight_24; }
	inline void set_m_DeadZoneHeight_24(float value)
	{
		___m_DeadZoneHeight_24 = value;
	}

	inline static int32_t get_offset_of_m_DeadZoneDepth_25() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_DeadZoneDepth_25)); }
	inline float get_m_DeadZoneDepth_25() const { return ___m_DeadZoneDepth_25; }
	inline float* get_address_of_m_DeadZoneDepth_25() { return &___m_DeadZoneDepth_25; }
	inline void set_m_DeadZoneDepth_25(float value)
	{
		___m_DeadZoneDepth_25 = value;
	}

	inline static int32_t get_offset_of_m_UnlimitedSoftZone_26() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_UnlimitedSoftZone_26)); }
	inline bool get_m_UnlimitedSoftZone_26() const { return ___m_UnlimitedSoftZone_26; }
	inline bool* get_address_of_m_UnlimitedSoftZone_26() { return &___m_UnlimitedSoftZone_26; }
	inline void set_m_UnlimitedSoftZone_26(bool value)
	{
		___m_UnlimitedSoftZone_26 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneWidth_27() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_SoftZoneWidth_27)); }
	inline float get_m_SoftZoneWidth_27() const { return ___m_SoftZoneWidth_27; }
	inline float* get_address_of_m_SoftZoneWidth_27() { return &___m_SoftZoneWidth_27; }
	inline void set_m_SoftZoneWidth_27(float value)
	{
		___m_SoftZoneWidth_27 = value;
	}

	inline static int32_t get_offset_of_m_SoftZoneHeight_28() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_SoftZoneHeight_28)); }
	inline float get_m_SoftZoneHeight_28() const { return ___m_SoftZoneHeight_28; }
	inline float* get_address_of_m_SoftZoneHeight_28() { return &___m_SoftZoneHeight_28; }
	inline void set_m_SoftZoneHeight_28(float value)
	{
		___m_SoftZoneHeight_28 = value;
	}

	inline static int32_t get_offset_of_m_BiasX_29() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_BiasX_29)); }
	inline float get_m_BiasX_29() const { return ___m_BiasX_29; }
	inline float* get_address_of_m_BiasX_29() { return &___m_BiasX_29; }
	inline void set_m_BiasX_29(float value)
	{
		___m_BiasX_29 = value;
	}

	inline static int32_t get_offset_of_m_BiasY_30() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_BiasY_30)); }
	inline float get_m_BiasY_30() const { return ___m_BiasY_30; }
	inline float* get_address_of_m_BiasY_30() { return &___m_BiasY_30; }
	inline void set_m_BiasY_30(float value)
	{
		___m_BiasY_30 = value;
	}

	inline static int32_t get_offset_of_m_CenterOnActivate_31() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_CenterOnActivate_31)); }
	inline bool get_m_CenterOnActivate_31() const { return ___m_CenterOnActivate_31; }
	inline bool* get_address_of_m_CenterOnActivate_31() { return &___m_CenterOnActivate_31; }
	inline void set_m_CenterOnActivate_31(bool value)
	{
		___m_CenterOnActivate_31 = value;
	}

	inline static int32_t get_offset_of_m_GroupFramingMode_32() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_GroupFramingMode_32)); }
	inline int32_t get_m_GroupFramingMode_32() const { return ___m_GroupFramingMode_32; }
	inline int32_t* get_address_of_m_GroupFramingMode_32() { return &___m_GroupFramingMode_32; }
	inline void set_m_GroupFramingMode_32(int32_t value)
	{
		___m_GroupFramingMode_32 = value;
	}

	inline static int32_t get_offset_of_m_AdjustmentMode_33() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_AdjustmentMode_33)); }
	inline int32_t get_m_AdjustmentMode_33() const { return ___m_AdjustmentMode_33; }
	inline int32_t* get_address_of_m_AdjustmentMode_33() { return &___m_AdjustmentMode_33; }
	inline void set_m_AdjustmentMode_33(int32_t value)
	{
		___m_AdjustmentMode_33 = value;
	}

	inline static int32_t get_offset_of_m_GroupFramingSize_34() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_GroupFramingSize_34)); }
	inline float get_m_GroupFramingSize_34() const { return ___m_GroupFramingSize_34; }
	inline float* get_address_of_m_GroupFramingSize_34() { return &___m_GroupFramingSize_34; }
	inline void set_m_GroupFramingSize_34(float value)
	{
		___m_GroupFramingSize_34 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyIn_35() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MaxDollyIn_35)); }
	inline float get_m_MaxDollyIn_35() const { return ___m_MaxDollyIn_35; }
	inline float* get_address_of_m_MaxDollyIn_35() { return &___m_MaxDollyIn_35; }
	inline void set_m_MaxDollyIn_35(float value)
	{
		___m_MaxDollyIn_35 = value;
	}

	inline static int32_t get_offset_of_m_MaxDollyOut_36() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MaxDollyOut_36)); }
	inline float get_m_MaxDollyOut_36() const { return ___m_MaxDollyOut_36; }
	inline float* get_address_of_m_MaxDollyOut_36() { return &___m_MaxDollyOut_36; }
	inline void set_m_MaxDollyOut_36(float value)
	{
		___m_MaxDollyOut_36 = value;
	}

	inline static int32_t get_offset_of_m_MinimumDistance_37() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MinimumDistance_37)); }
	inline float get_m_MinimumDistance_37() const { return ___m_MinimumDistance_37; }
	inline float* get_address_of_m_MinimumDistance_37() { return &___m_MinimumDistance_37; }
	inline void set_m_MinimumDistance_37(float value)
	{
		___m_MinimumDistance_37 = value;
	}

	inline static int32_t get_offset_of_m_MaximumDistance_38() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MaximumDistance_38)); }
	inline float get_m_MaximumDistance_38() const { return ___m_MaximumDistance_38; }
	inline float* get_address_of_m_MaximumDistance_38() { return &___m_MaximumDistance_38; }
	inline void set_m_MaximumDistance_38(float value)
	{
		___m_MaximumDistance_38 = value;
	}

	inline static int32_t get_offset_of_m_MinimumFOV_39() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MinimumFOV_39)); }
	inline float get_m_MinimumFOV_39() const { return ___m_MinimumFOV_39; }
	inline float* get_address_of_m_MinimumFOV_39() { return &___m_MinimumFOV_39; }
	inline void set_m_MinimumFOV_39(float value)
	{
		___m_MinimumFOV_39 = value;
	}

	inline static int32_t get_offset_of_m_MaximumFOV_40() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MaximumFOV_40)); }
	inline float get_m_MaximumFOV_40() const { return ___m_MaximumFOV_40; }
	inline float* get_address_of_m_MaximumFOV_40() { return &___m_MaximumFOV_40; }
	inline void set_m_MaximumFOV_40(float value)
	{
		___m_MaximumFOV_40 = value;
	}

	inline static int32_t get_offset_of_m_MinimumOrthoSize_41() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MinimumOrthoSize_41)); }
	inline float get_m_MinimumOrthoSize_41() const { return ___m_MinimumOrthoSize_41; }
	inline float* get_address_of_m_MinimumOrthoSize_41() { return &___m_MinimumOrthoSize_41; }
	inline void set_m_MinimumOrthoSize_41(float value)
	{
		___m_MinimumOrthoSize_41 = value;
	}

	inline static int32_t get_offset_of_m_MaximumOrthoSize_42() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_MaximumOrthoSize_42)); }
	inline float get_m_MaximumOrthoSize_42() const { return ___m_MaximumOrthoSize_42; }
	inline float* get_address_of_m_MaximumOrthoSize_42() { return &___m_MaximumOrthoSize_42; }
	inline void set_m_MaximumOrthoSize_42(float value)
	{
		___m_MaximumOrthoSize_42 = value;
	}

	inline static int32_t get_offset_of_m_PreviousCameraPosition_45() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_PreviousCameraPosition_45)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_PreviousCameraPosition_45() const { return ___m_PreviousCameraPosition_45; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_PreviousCameraPosition_45() { return &___m_PreviousCameraPosition_45; }
	inline void set_m_PreviousCameraPosition_45(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_PreviousCameraPosition_45 = value;
	}

	inline static int32_t get_offset_of_m_Predictor_46() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_Predictor_46)); }
	inline PositionPredictor_tAC12D8D878524DFBAF37CBBF695B5D18B877F56A * get_m_Predictor_46() const { return ___m_Predictor_46; }
	inline PositionPredictor_tAC12D8D878524DFBAF37CBBF695B5D18B877F56A ** get_address_of_m_Predictor_46() { return &___m_Predictor_46; }
	inline void set_m_Predictor_46(PositionPredictor_tAC12D8D878524DFBAF37CBBF695B5D18B877F56A * value)
	{
		___m_Predictor_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Predictor_46), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTrackedPointU3Ek__BackingField_47() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___U3CTrackedPointU3Ek__BackingField_47)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CTrackedPointU3Ek__BackingField_47() const { return ___U3CTrackedPointU3Ek__BackingField_47; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CTrackedPointU3Ek__BackingField_47() { return &___U3CTrackedPointU3Ek__BackingField_47; }
	inline void set_U3CTrackedPointU3Ek__BackingField_47(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CTrackedPointU3Ek__BackingField_47 = value;
	}

	inline static int32_t get_offset_of_U3CInheritingPositionU3Ek__BackingField_48() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___U3CInheritingPositionU3Ek__BackingField_48)); }
	inline bool get_U3CInheritingPositionU3Ek__BackingField_48() const { return ___U3CInheritingPositionU3Ek__BackingField_48; }
	inline bool* get_address_of_U3CInheritingPositionU3Ek__BackingField_48() { return &___U3CInheritingPositionU3Ek__BackingField_48; }
	inline void set_U3CInheritingPositionU3Ek__BackingField_48(bool value)
	{
		___U3CInheritingPositionU3Ek__BackingField_48 = value;
	}

	inline static int32_t get_offset_of_m_prevFOV_49() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_prevFOV_49)); }
	inline float get_m_prevFOV_49() const { return ___m_prevFOV_49; }
	inline float* get_address_of_m_prevFOV_49() { return &___m_prevFOV_49; }
	inline void set_m_prevFOV_49(float value)
	{
		___m_prevFOV_49 = value;
	}

	inline static int32_t get_offset_of_m_prevRotation_50() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___m_prevRotation_50)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_m_prevRotation_50() const { return ___m_prevRotation_50; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_m_prevRotation_50() { return &___m_prevRotation_50; }
	inline void set_m_prevRotation_50(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___m_prevRotation_50 = value;
	}

	inline static int32_t get_offset_of_U3CLastBoundsU3Ek__BackingField_51() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___U3CLastBoundsU3Ek__BackingField_51)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_U3CLastBoundsU3Ek__BackingField_51() const { return ___U3CLastBoundsU3Ek__BackingField_51; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_U3CLastBoundsU3Ek__BackingField_51() { return &___U3CLastBoundsU3Ek__BackingField_51; }
	inline void set_U3CLastBoundsU3Ek__BackingField_51(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___U3CLastBoundsU3Ek__BackingField_51 = value;
	}

	inline static int32_t get_offset_of_U3CLastBoundsMatrixU3Ek__BackingField_52() { return static_cast<int32_t>(offsetof(CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1, ___U3CLastBoundsMatrixU3Ek__BackingField_52)); }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  get_U3CLastBoundsMatrixU3Ek__BackingField_52() const { return ___U3CLastBoundsMatrixU3Ek__BackingField_52; }
	inline Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461 * get_address_of_U3CLastBoundsMatrixU3Ek__BackingField_52() { return &___U3CLastBoundsMatrixU3Ek__BackingField_52; }
	inline void set_U3CLastBoundsMatrixU3Ek__BackingField_52(Matrix4x4_tDE7FF4F2E2EA284F6EFE00D627789D0E5B8B4461  value)
	{
		___U3CLastBoundsMatrixU3Ek__BackingField_52 = value;
	}
};


// Cinemachine.CinemachineVirtualCamera
struct CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C  : public CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5
{
public:
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_LookAt
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_LookAt_20;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_Follow
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_Follow_21;
	// Cinemachine.LensSettings Cinemachine.CinemachineVirtualCamera::m_Lens
	LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  ___m_Lens_22;
	// Cinemachine.CinemachineVirtualCameraBase/TransitionParams Cinemachine.CinemachineVirtualCamera::m_Transitions
	TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C  ___m_Transitions_23;
	// Cinemachine.CinemachineVirtualCameraBase/BlendHint Cinemachine.CinemachineVirtualCamera::m_LegacyBlendHint
	int32_t ___m_LegacyBlendHint_24;
	// System.Boolean Cinemachine.CinemachineVirtualCamera::<UserIsDragging>k__BackingField
	bool ___U3CUserIsDraggingU3Ek__BackingField_28;
	// Cinemachine.CameraState Cinemachine.CinemachineVirtualCamera::m_State
	CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601  ___m_State_29;
	// Cinemachine.CinemachineComponentBase[] Cinemachine.CinemachineVirtualCamera::m_ComponentPipeline
	CinemachineComponentBaseU5BU5D_t103C5C1C3257AA526385CAD3B1FD18E5CC1446EC* ___m_ComponentPipeline_30;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::m_ComponentOwner
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_ComponentOwner_31;
	// UnityEngine.Transform Cinemachine.CinemachineVirtualCamera::mCachedLookAtTarget
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___mCachedLookAtTarget_32;
	// Cinemachine.CinemachineVirtualCameraBase Cinemachine.CinemachineVirtualCamera::mCachedLookAtTargetVcam
	CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___mCachedLookAtTargetVcam_33;

public:
	inline static int32_t get_offset_of_m_LookAt_20() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_LookAt_20)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_LookAt_20() const { return ___m_LookAt_20; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_LookAt_20() { return &___m_LookAt_20; }
	inline void set_m_LookAt_20(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_LookAt_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_LookAt_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_Follow_21() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_Follow_21)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_Follow_21() const { return ___m_Follow_21; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_Follow_21() { return &___m_Follow_21; }
	inline void set_m_Follow_21(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_Follow_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Follow_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Lens_22() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_Lens_22)); }
	inline LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  get_m_Lens_22() const { return ___m_Lens_22; }
	inline LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * get_address_of_m_Lens_22() { return &___m_Lens_22; }
	inline void set_m_Lens_22(LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA  value)
	{
		___m_Lens_22 = value;
	}

	inline static int32_t get_offset_of_m_Transitions_23() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_Transitions_23)); }
	inline TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C  get_m_Transitions_23() const { return ___m_Transitions_23; }
	inline TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C * get_address_of_m_Transitions_23() { return &___m_Transitions_23; }
	inline void set_m_Transitions_23(TransitionParams_t2EE11D8A937A8C60D8C27C6D24C37B0D096AA11C  value)
	{
		___m_Transitions_23 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Transitions_23))->___m_OnCameraLive_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_LegacyBlendHint_24() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_LegacyBlendHint_24)); }
	inline int32_t get_m_LegacyBlendHint_24() const { return ___m_LegacyBlendHint_24; }
	inline int32_t* get_address_of_m_LegacyBlendHint_24() { return &___m_LegacyBlendHint_24; }
	inline void set_m_LegacyBlendHint_24(int32_t value)
	{
		___m_LegacyBlendHint_24 = value;
	}

	inline static int32_t get_offset_of_U3CUserIsDraggingU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___U3CUserIsDraggingU3Ek__BackingField_28)); }
	inline bool get_U3CUserIsDraggingU3Ek__BackingField_28() const { return ___U3CUserIsDraggingU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CUserIsDraggingU3Ek__BackingField_28() { return &___U3CUserIsDraggingU3Ek__BackingField_28; }
	inline void set_U3CUserIsDraggingU3Ek__BackingField_28(bool value)
	{
		___U3CUserIsDraggingU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_m_State_29() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_State_29)); }
	inline CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601  get_m_State_29() const { return ___m_State_29; }
	inline CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * get_address_of_m_State_29() { return &___m_State_29; }
	inline void set_m_State_29(CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601  value)
	{
		___m_State_29 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_29))->___mCustom0_11))->___m_Custom_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_29))->___mCustom1_12))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_29))->___mCustom2_13))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_State_29))->___mCustom3_14))->___m_Custom_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_State_29))->___m_CustomOverflow_15), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_ComponentPipeline_30() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_ComponentPipeline_30)); }
	inline CinemachineComponentBaseU5BU5D_t103C5C1C3257AA526385CAD3B1FD18E5CC1446EC* get_m_ComponentPipeline_30() const { return ___m_ComponentPipeline_30; }
	inline CinemachineComponentBaseU5BU5D_t103C5C1C3257AA526385CAD3B1FD18E5CC1446EC** get_address_of_m_ComponentPipeline_30() { return &___m_ComponentPipeline_30; }
	inline void set_m_ComponentPipeline_30(CinemachineComponentBaseU5BU5D_t103C5C1C3257AA526385CAD3B1FD18E5CC1446EC* value)
	{
		___m_ComponentPipeline_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentPipeline_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_ComponentOwner_31() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___m_ComponentOwner_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_ComponentOwner_31() const { return ___m_ComponentOwner_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_ComponentOwner_31() { return &___m_ComponentOwner_31; }
	inline void set_m_ComponentOwner_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_ComponentOwner_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ComponentOwner_31), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTarget_32() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___mCachedLookAtTarget_32)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_mCachedLookAtTarget_32() const { return ___mCachedLookAtTarget_32; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_mCachedLookAtTarget_32() { return &___mCachedLookAtTarget_32; }
	inline void set_mCachedLookAtTarget_32(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___mCachedLookAtTarget_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTarget_32), (void*)value);
	}

	inline static int32_t get_offset_of_mCachedLookAtTargetVcam_33() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C, ___mCachedLookAtTargetVcam_33)); }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * get_mCachedLookAtTargetVcam_33() const { return ___mCachedLookAtTargetVcam_33; }
	inline CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 ** get_address_of_mCachedLookAtTargetVcam_33() { return &___mCachedLookAtTargetVcam_33; }
	inline void set_mCachedLookAtTargetVcam_33(CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * value)
	{
		___mCachedLookAtTargetVcam_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mCachedLookAtTargetVcam_33), (void*)value);
	}
};

struct CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_StaticFields
{
public:
	// Cinemachine.CinemachineVirtualCamera/CreatePipelineDelegate Cinemachine.CinemachineVirtualCamera::CreatePipelineOverride
	CreatePipelineDelegate_tB2027A3AD25F49EFC2BE7B2872BB15C1B7856C19 * ___CreatePipelineOverride_26;
	// Cinemachine.CinemachineVirtualCamera/DestroyPipelineDelegate Cinemachine.CinemachineVirtualCamera::DestroyPipelineOverride
	DestroyPipelineDelegate_tA69D0FA40EDBF1EF43F75E7FE8A33D85F8DD1B28 * ___DestroyPipelineOverride_27;

public:
	inline static int32_t get_offset_of_CreatePipelineOverride_26() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_StaticFields, ___CreatePipelineOverride_26)); }
	inline CreatePipelineDelegate_tB2027A3AD25F49EFC2BE7B2872BB15C1B7856C19 * get_CreatePipelineOverride_26() const { return ___CreatePipelineOverride_26; }
	inline CreatePipelineDelegate_tB2027A3AD25F49EFC2BE7B2872BB15C1B7856C19 ** get_address_of_CreatePipelineOverride_26() { return &___CreatePipelineOverride_26; }
	inline void set_CreatePipelineOverride_26(CreatePipelineDelegate_tB2027A3AD25F49EFC2BE7B2872BB15C1B7856C19 * value)
	{
		___CreatePipelineOverride_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CreatePipelineOverride_26), (void*)value);
	}

	inline static int32_t get_offset_of_DestroyPipelineOverride_27() { return static_cast<int32_t>(offsetof(CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_StaticFields, ___DestroyPipelineOverride_27)); }
	inline DestroyPipelineDelegate_tA69D0FA40EDBF1EF43F75E7FE8A33D85F8DD1B28 * get_DestroyPipelineOverride_27() const { return ___DestroyPipelineOverride_27; }
	inline DestroyPipelineDelegate_tA69D0FA40EDBF1EF43F75E7FE8A33D85F8DD1B28 ** get_address_of_DestroyPipelineOverride_27() { return &___DestroyPipelineOverride_27; }
	inline void set_DestroyPipelineOverride_27(DestroyPipelineDelegate_tA69D0FA40EDBF1EF43F75E7FE8A33D85F8DD1B28 * value)
	{
		___DestroyPipelineOverride_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DestroyPipelineOverride_27), (void*)value);
	}
};


// MoreMountains.CorgiEngine.LevelManager
struct LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E  : public MMSingleton_1_tA763EC03A0B0496FE271C15347A7280C7380FBDB
{
public:
	// MoreMountains.CorgiEngine.Character[] MoreMountains.CorgiEngine.LevelManager::PlayerPrefabs
	CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* ___PlayerPrefabs_5;
	// System.Boolean MoreMountains.CorgiEngine.LevelManager::AutoAttributePlayerIDs
	bool ___AutoAttributePlayerIDs_6;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character> MoreMountains.CorgiEngine.LevelManager::SceneCharacters
	List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * ___SceneCharacters_7;
	// MoreMountains.CorgiEngine.CheckPoint MoreMountains.CorgiEngine.LevelManager::DebugSpawn
	CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * ___DebugSpawn_8;
	// MoreMountains.CorgiEngine.LevelManager/CheckpointsAxis MoreMountains.CorgiEngine.LevelManager::CheckpointAttributionAxis
	int32_t ___CheckpointAttributionAxis_9;
	// MoreMountains.CorgiEngine.LevelManager/CheckpointDirections MoreMountains.CorgiEngine.LevelManager::CheckpointAttributionDirection
	int32_t ___CheckpointAttributionDirection_10;
	// MoreMountains.CorgiEngine.CheckPoint MoreMountains.CorgiEngine.LevelManager::CurrentCheckPoint
	CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * ___CurrentCheckPoint_11;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointOfEntry> MoreMountains.CorgiEngine.LevelManager::PointsOfEntry
	List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 * ___PointsOfEntry_12;
	// System.Single MoreMountains.CorgiEngine.LevelManager::IntroFadeDuration
	float ___IntroFadeDuration_13;
	// System.Single MoreMountains.CorgiEngine.LevelManager::OutroFadeDuration
	float ___OutroFadeDuration_14;
	// System.Int32 MoreMountains.CorgiEngine.LevelManager::FaderID
	int32_t ___FaderID_15;
	// MoreMountains.Tools.MMTweenType MoreMountains.CorgiEngine.LevelManager::FadeTween
	MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * ___FadeTween_16;
	// System.Single MoreMountains.CorgiEngine.LevelManager::RespawnDelay
	float ___RespawnDelay_17;
	// MoreMountains.CorgiEngine.LevelManager/BoundsModes MoreMountains.CorgiEngine.LevelManager::BoundsMode
	int32_t ___BoundsMode_18;
	// UnityEngine.Bounds MoreMountains.CorgiEngine.LevelManager::LevelBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ___LevelBounds_19;
	// System.Boolean MoreMountains.CorgiEngine.LevelManager::ConvertToColliderBoundsButton
	bool ___ConvertToColliderBoundsButton_20;
	// UnityEngine.Collider MoreMountains.CorgiEngine.LevelManager::<BoundsCollider>k__BackingField
	Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___U3CBoundsColliderU3Ek__BackingField_21;
	// UnityEngine.Collider2D MoreMountains.CorgiEngine.LevelManager::<BoundsCollider2D>k__BackingField
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___U3CBoundsCollider2DU3Ek__BackingField_22;
	// MoreMountains.Tools.MMLoadScene/LoadingSceneModes MoreMountains.CorgiEngine.LevelManager::LoadingSceneMode
	int32_t ___LoadingSceneMode_23;
	// System.String MoreMountains.CorgiEngine.LevelManager::LoadingSceneName
	String_t* ___LoadingSceneName_24;
	// MoreMountains.Tools.MMAdditiveSceneLoadingManagerSettings MoreMountains.CorgiEngine.LevelManager::AdditiveLoadingSettings
	MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB * ___AdditiveLoadingSettings_25;
	// MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.LevelManager::<LevelCameraController>k__BackingField
	CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * ___U3CLevelCameraControllerU3Ek__BackingField_26;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character> MoreMountains.CorgiEngine.LevelManager::<Players>k__BackingField
	List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * ___U3CPlayersU3Ek__BackingField_27;
	// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.CheckPoint> MoreMountains.CorgiEngine.LevelManager::<Checkpoints>k__BackingField
	List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 * ___U3CCheckpointsU3Ek__BackingField_28;
	// System.DateTime MoreMountains.CorgiEngine.LevelManager::_started
	DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  ____started_29;
	// System.Int32 MoreMountains.CorgiEngine.LevelManager::_savedPoints
	int32_t ____savedPoints_30;
	// System.String MoreMountains.CorgiEngine.LevelManager::_nextLevel
	String_t* ____nextLevel_31;
	// UnityEngine.BoxCollider MoreMountains.CorgiEngine.LevelManager::_collider
	BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * ____collider_32;
	// UnityEngine.BoxCollider2D MoreMountains.CorgiEngine.LevelManager::_collider2D
	BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * ____collider2D_33;
	// UnityEngine.Bounds MoreMountains.CorgiEngine.LevelManager::_originalBounds
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  ____originalBounds_34;

public:
	inline static int32_t get_offset_of_PlayerPrefabs_5() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___PlayerPrefabs_5)); }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* get_PlayerPrefabs_5() const { return ___PlayerPrefabs_5; }
	inline CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED** get_address_of_PlayerPrefabs_5() { return &___PlayerPrefabs_5; }
	inline void set_PlayerPrefabs_5(CharacterU5BU5D_t2F6C215E584D8E6EC8723BDD00F86D9E9BC5E5ED* value)
	{
		___PlayerPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerPrefabs_5), (void*)value);
	}

	inline static int32_t get_offset_of_AutoAttributePlayerIDs_6() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___AutoAttributePlayerIDs_6)); }
	inline bool get_AutoAttributePlayerIDs_6() const { return ___AutoAttributePlayerIDs_6; }
	inline bool* get_address_of_AutoAttributePlayerIDs_6() { return &___AutoAttributePlayerIDs_6; }
	inline void set_AutoAttributePlayerIDs_6(bool value)
	{
		___AutoAttributePlayerIDs_6 = value;
	}

	inline static int32_t get_offset_of_SceneCharacters_7() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___SceneCharacters_7)); }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * get_SceneCharacters_7() const { return ___SceneCharacters_7; }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E ** get_address_of_SceneCharacters_7() { return &___SceneCharacters_7; }
	inline void set_SceneCharacters_7(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * value)
	{
		___SceneCharacters_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SceneCharacters_7), (void*)value);
	}

	inline static int32_t get_offset_of_DebugSpawn_8() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___DebugSpawn_8)); }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * get_DebugSpawn_8() const { return ___DebugSpawn_8; }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 ** get_address_of_DebugSpawn_8() { return &___DebugSpawn_8; }
	inline void set_DebugSpawn_8(CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * value)
	{
		___DebugSpawn_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DebugSpawn_8), (void*)value);
	}

	inline static int32_t get_offset_of_CheckpointAttributionAxis_9() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___CheckpointAttributionAxis_9)); }
	inline int32_t get_CheckpointAttributionAxis_9() const { return ___CheckpointAttributionAxis_9; }
	inline int32_t* get_address_of_CheckpointAttributionAxis_9() { return &___CheckpointAttributionAxis_9; }
	inline void set_CheckpointAttributionAxis_9(int32_t value)
	{
		___CheckpointAttributionAxis_9 = value;
	}

	inline static int32_t get_offset_of_CheckpointAttributionDirection_10() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___CheckpointAttributionDirection_10)); }
	inline int32_t get_CheckpointAttributionDirection_10() const { return ___CheckpointAttributionDirection_10; }
	inline int32_t* get_address_of_CheckpointAttributionDirection_10() { return &___CheckpointAttributionDirection_10; }
	inline void set_CheckpointAttributionDirection_10(int32_t value)
	{
		___CheckpointAttributionDirection_10 = value;
	}

	inline static int32_t get_offset_of_CurrentCheckPoint_11() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___CurrentCheckPoint_11)); }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * get_CurrentCheckPoint_11() const { return ___CurrentCheckPoint_11; }
	inline CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 ** get_address_of_CurrentCheckPoint_11() { return &___CurrentCheckPoint_11; }
	inline void set_CurrentCheckPoint_11(CheckPoint_t158E28973A68B04BA7852CA56ADE158F50BD98B4 * value)
	{
		___CurrentCheckPoint_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentCheckPoint_11), (void*)value);
	}

	inline static int32_t get_offset_of_PointsOfEntry_12() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___PointsOfEntry_12)); }
	inline List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 * get_PointsOfEntry_12() const { return ___PointsOfEntry_12; }
	inline List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 ** get_address_of_PointsOfEntry_12() { return &___PointsOfEntry_12; }
	inline void set_PointsOfEntry_12(List_1_tFDB98DAFB815ED3AC7A48FF032F082FE272AFFA3 * value)
	{
		___PointsOfEntry_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PointsOfEntry_12), (void*)value);
	}

	inline static int32_t get_offset_of_IntroFadeDuration_13() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___IntroFadeDuration_13)); }
	inline float get_IntroFadeDuration_13() const { return ___IntroFadeDuration_13; }
	inline float* get_address_of_IntroFadeDuration_13() { return &___IntroFadeDuration_13; }
	inline void set_IntroFadeDuration_13(float value)
	{
		___IntroFadeDuration_13 = value;
	}

	inline static int32_t get_offset_of_OutroFadeDuration_14() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___OutroFadeDuration_14)); }
	inline float get_OutroFadeDuration_14() const { return ___OutroFadeDuration_14; }
	inline float* get_address_of_OutroFadeDuration_14() { return &___OutroFadeDuration_14; }
	inline void set_OutroFadeDuration_14(float value)
	{
		___OutroFadeDuration_14 = value;
	}

	inline static int32_t get_offset_of_FaderID_15() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___FaderID_15)); }
	inline int32_t get_FaderID_15() const { return ___FaderID_15; }
	inline int32_t* get_address_of_FaderID_15() { return &___FaderID_15; }
	inline void set_FaderID_15(int32_t value)
	{
		___FaderID_15 = value;
	}

	inline static int32_t get_offset_of_FadeTween_16() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___FadeTween_16)); }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * get_FadeTween_16() const { return ___FadeTween_16; }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 ** get_address_of_FadeTween_16() { return &___FadeTween_16; }
	inline void set_FadeTween_16(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * value)
	{
		___FadeTween_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FadeTween_16), (void*)value);
	}

	inline static int32_t get_offset_of_RespawnDelay_17() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___RespawnDelay_17)); }
	inline float get_RespawnDelay_17() const { return ___RespawnDelay_17; }
	inline float* get_address_of_RespawnDelay_17() { return &___RespawnDelay_17; }
	inline void set_RespawnDelay_17(float value)
	{
		___RespawnDelay_17 = value;
	}

	inline static int32_t get_offset_of_BoundsMode_18() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___BoundsMode_18)); }
	inline int32_t get_BoundsMode_18() const { return ___BoundsMode_18; }
	inline int32_t* get_address_of_BoundsMode_18() { return &___BoundsMode_18; }
	inline void set_BoundsMode_18(int32_t value)
	{
		___BoundsMode_18 = value;
	}

	inline static int32_t get_offset_of_LevelBounds_19() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___LevelBounds_19)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get_LevelBounds_19() const { return ___LevelBounds_19; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of_LevelBounds_19() { return &___LevelBounds_19; }
	inline void set_LevelBounds_19(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		___LevelBounds_19 = value;
	}

	inline static int32_t get_offset_of_ConvertToColliderBoundsButton_20() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___ConvertToColliderBoundsButton_20)); }
	inline bool get_ConvertToColliderBoundsButton_20() const { return ___ConvertToColliderBoundsButton_20; }
	inline bool* get_address_of_ConvertToColliderBoundsButton_20() { return &___ConvertToColliderBoundsButton_20; }
	inline void set_ConvertToColliderBoundsButton_20(bool value)
	{
		___ConvertToColliderBoundsButton_20 = value;
	}

	inline static int32_t get_offset_of_U3CBoundsColliderU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CBoundsColliderU3Ek__BackingField_21)); }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * get_U3CBoundsColliderU3Ek__BackingField_21() const { return ___U3CBoundsColliderU3Ek__BackingField_21; }
	inline Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 ** get_address_of_U3CBoundsColliderU3Ek__BackingField_21() { return &___U3CBoundsColliderU3Ek__BackingField_21; }
	inline void set_U3CBoundsColliderU3Ek__BackingField_21(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * value)
	{
		___U3CBoundsColliderU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBoundsColliderU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBoundsCollider2DU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CBoundsCollider2DU3Ek__BackingField_22)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get_U3CBoundsCollider2DU3Ek__BackingField_22() const { return ___U3CBoundsCollider2DU3Ek__BackingField_22; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of_U3CBoundsCollider2DU3Ek__BackingField_22() { return &___U3CBoundsCollider2DU3Ek__BackingField_22; }
	inline void set_U3CBoundsCollider2DU3Ek__BackingField_22(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		___U3CBoundsCollider2DU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBoundsCollider2DU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_LoadingSceneMode_23() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___LoadingSceneMode_23)); }
	inline int32_t get_LoadingSceneMode_23() const { return ___LoadingSceneMode_23; }
	inline int32_t* get_address_of_LoadingSceneMode_23() { return &___LoadingSceneMode_23; }
	inline void set_LoadingSceneMode_23(int32_t value)
	{
		___LoadingSceneMode_23 = value;
	}

	inline static int32_t get_offset_of_LoadingSceneName_24() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___LoadingSceneName_24)); }
	inline String_t* get_LoadingSceneName_24() const { return ___LoadingSceneName_24; }
	inline String_t** get_address_of_LoadingSceneName_24() { return &___LoadingSceneName_24; }
	inline void set_LoadingSceneName_24(String_t* value)
	{
		___LoadingSceneName_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LoadingSceneName_24), (void*)value);
	}

	inline static int32_t get_offset_of_AdditiveLoadingSettings_25() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___AdditiveLoadingSettings_25)); }
	inline MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB * get_AdditiveLoadingSettings_25() const { return ___AdditiveLoadingSettings_25; }
	inline MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB ** get_address_of_AdditiveLoadingSettings_25() { return &___AdditiveLoadingSettings_25; }
	inline void set_AdditiveLoadingSettings_25(MMAdditiveSceneLoadingManagerSettings_tA9D422A7CA28BC490AB895A4AB07DF4DBB8881BB * value)
	{
		___AdditiveLoadingSettings_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AdditiveLoadingSettings_25), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLevelCameraControllerU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CLevelCameraControllerU3Ek__BackingField_26)); }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * get_U3CLevelCameraControllerU3Ek__BackingField_26() const { return ___U3CLevelCameraControllerU3Ek__BackingField_26; }
	inline CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 ** get_address_of_U3CLevelCameraControllerU3Ek__BackingField_26() { return &___U3CLevelCameraControllerU3Ek__BackingField_26; }
	inline void set_U3CLevelCameraControllerU3Ek__BackingField_26(CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * value)
	{
		___U3CLevelCameraControllerU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLevelCameraControllerU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPlayersU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CPlayersU3Ek__BackingField_27)); }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * get_U3CPlayersU3Ek__BackingField_27() const { return ___U3CPlayersU3Ek__BackingField_27; }
	inline List_1_t5DB922CEE6338D263668FE75E45C8566802F493E ** get_address_of_U3CPlayersU3Ek__BackingField_27() { return &___U3CPlayersU3Ek__BackingField_27; }
	inline void set_U3CPlayersU3Ek__BackingField_27(List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * value)
	{
		___U3CPlayersU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPlayersU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCheckpointsU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ___U3CCheckpointsU3Ek__BackingField_28)); }
	inline List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 * get_U3CCheckpointsU3Ek__BackingField_28() const { return ___U3CCheckpointsU3Ek__BackingField_28; }
	inline List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 ** get_address_of_U3CCheckpointsU3Ek__BackingField_28() { return &___U3CCheckpointsU3Ek__BackingField_28; }
	inline void set_U3CCheckpointsU3Ek__BackingField_28(List_1_t3639B780938F0B4D9C2A1C827B7905A395EA9716 * value)
	{
		___U3CCheckpointsU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCheckpointsU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of__started_29() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____started_29)); }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  get__started_29() const { return ____started_29; }
	inline DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405 * get_address_of__started_29() { return &____started_29; }
	inline void set__started_29(DateTime_tEAF2CD16E071DF5441F40822E4CFE880E5245405  value)
	{
		____started_29 = value;
	}

	inline static int32_t get_offset_of__savedPoints_30() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____savedPoints_30)); }
	inline int32_t get__savedPoints_30() const { return ____savedPoints_30; }
	inline int32_t* get_address_of__savedPoints_30() { return &____savedPoints_30; }
	inline void set__savedPoints_30(int32_t value)
	{
		____savedPoints_30 = value;
	}

	inline static int32_t get_offset_of__nextLevel_31() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____nextLevel_31)); }
	inline String_t* get__nextLevel_31() const { return ____nextLevel_31; }
	inline String_t** get_address_of__nextLevel_31() { return &____nextLevel_31; }
	inline void set__nextLevel_31(String_t* value)
	{
		____nextLevel_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nextLevel_31), (void*)value);
	}

	inline static int32_t get_offset_of__collider_32() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____collider_32)); }
	inline BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * get__collider_32() const { return ____collider_32; }
	inline BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 ** get_address_of__collider_32() { return &____collider_32; }
	inline void set__collider_32(BoxCollider_tA530691AC1A3C9FE6428F68F98588FCB1BF9AAA5 * value)
	{
		____collider_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider_32), (void*)value);
	}

	inline static int32_t get_offset_of__collider2D_33() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____collider2D_33)); }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * get__collider2D_33() const { return ____collider2D_33; }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 ** get_address_of__collider2D_33() { return &____collider2D_33; }
	inline void set__collider2D_33(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * value)
	{
		____collider2D_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider2D_33), (void*)value);
	}

	inline static int32_t get_offset_of__originalBounds_34() { return static_cast<int32_t>(offsetof(LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E, ____originalBounds_34)); }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  get__originalBounds_34() const { return ____originalBounds_34; }
	inline Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * get_address_of__originalBounds_34() { return &____originalBounds_34; }
	inline void set__originalBounds_34(Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  value)
	{
		____originalBounds_34 = value;
	}
};


// MoreMountains.Tools.MMCinemachineZone2D
struct MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606  : public MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5
{
public:
	// UnityEngine.Collider2D MoreMountains.Tools.MMCinemachineZone2D::_collider2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____collider2D_18;
	// UnityEngine.Collider2D MoreMountains.Tools.MMCinemachineZone2D::_confinerCollider2D
	Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ____confinerCollider2D_19;
	// UnityEngine.Rigidbody2D MoreMountains.Tools.MMCinemachineZone2D::_confinerRigidbody2D
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ____confinerRigidbody2D_20;
	// UnityEngine.CompositeCollider2D MoreMountains.Tools.MMCinemachineZone2D::_confinerCompositeCollider2D
	CompositeCollider2D_tFEE36FCBB44A7A893697E113323B78808D86AF35 * ____confinerCompositeCollider2D_21;
	// Cinemachine.CinemachineConfiner MoreMountains.Tools.MMCinemachineZone2D::_cinemachineConfiner
	CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * ____cinemachineConfiner_22;
	// UnityEngine.BoxCollider2D MoreMountains.Tools.MMCinemachineZone2D::_boxCollider2D
	BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * ____boxCollider2D_23;
	// UnityEngine.CircleCollider2D MoreMountains.Tools.MMCinemachineZone2D::_circleCollider2D
	CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * ____circleCollider2D_24;
	// UnityEngine.PolygonCollider2D MoreMountains.Tools.MMCinemachineZone2D::_polygonCollider2D
	PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D * ____polygonCollider2D_25;

public:
	inline static int32_t get_offset_of__collider2D_18() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____collider2D_18)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__collider2D_18() const { return ____collider2D_18; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__collider2D_18() { return &____collider2D_18; }
	inline void set__collider2D_18(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____collider2D_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____collider2D_18), (void*)value);
	}

	inline static int32_t get_offset_of__confinerCollider2D_19() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____confinerCollider2D_19)); }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * get__confinerCollider2D_19() const { return ____confinerCollider2D_19; }
	inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 ** get_address_of__confinerCollider2D_19() { return &____confinerCollider2D_19; }
	inline void set__confinerCollider2D_19(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * value)
	{
		____confinerCollider2D_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____confinerCollider2D_19), (void*)value);
	}

	inline static int32_t get_offset_of__confinerRigidbody2D_20() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____confinerRigidbody2D_20)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get__confinerRigidbody2D_20() const { return ____confinerRigidbody2D_20; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of__confinerRigidbody2D_20() { return &____confinerRigidbody2D_20; }
	inline void set__confinerRigidbody2D_20(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		____confinerRigidbody2D_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____confinerRigidbody2D_20), (void*)value);
	}

	inline static int32_t get_offset_of__confinerCompositeCollider2D_21() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____confinerCompositeCollider2D_21)); }
	inline CompositeCollider2D_tFEE36FCBB44A7A893697E113323B78808D86AF35 * get__confinerCompositeCollider2D_21() const { return ____confinerCompositeCollider2D_21; }
	inline CompositeCollider2D_tFEE36FCBB44A7A893697E113323B78808D86AF35 ** get_address_of__confinerCompositeCollider2D_21() { return &____confinerCompositeCollider2D_21; }
	inline void set__confinerCompositeCollider2D_21(CompositeCollider2D_tFEE36FCBB44A7A893697E113323B78808D86AF35 * value)
	{
		____confinerCompositeCollider2D_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____confinerCompositeCollider2D_21), (void*)value);
	}

	inline static int32_t get_offset_of__cinemachineConfiner_22() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____cinemachineConfiner_22)); }
	inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * get__cinemachineConfiner_22() const { return ____cinemachineConfiner_22; }
	inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D ** get_address_of__cinemachineConfiner_22() { return &____cinemachineConfiner_22; }
	inline void set__cinemachineConfiner_22(CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * value)
	{
		____cinemachineConfiner_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cinemachineConfiner_22), (void*)value);
	}

	inline static int32_t get_offset_of__boxCollider2D_23() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____boxCollider2D_23)); }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * get__boxCollider2D_23() const { return ____boxCollider2D_23; }
	inline BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 ** get_address_of__boxCollider2D_23() { return &____boxCollider2D_23; }
	inline void set__boxCollider2D_23(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * value)
	{
		____boxCollider2D_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____boxCollider2D_23), (void*)value);
	}

	inline static int32_t get_offset_of__circleCollider2D_24() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____circleCollider2D_24)); }
	inline CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * get__circleCollider2D_24() const { return ____circleCollider2D_24; }
	inline CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 ** get_address_of__circleCollider2D_24() { return &____circleCollider2D_24; }
	inline void set__circleCollider2D_24(CircleCollider2D_tD909965F1FE89EA6CAF32E86E3675F16A79EB913 * value)
	{
		____circleCollider2D_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____circleCollider2D_24), (void*)value);
	}

	inline static int32_t get_offset_of__polygonCollider2D_25() { return static_cast<int32_t>(offsetof(MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606, ____polygonCollider2D_25)); }
	inline PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D * get__polygonCollider2D_25() const { return ____polygonCollider2D_25; }
	inline PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D ** get_address_of__polygonCollider2D_25() { return &____polygonCollider2D_25; }
	inline void set__polygonCollider2D_25(PolygonCollider2D_t0DE3E0562D6B75598DFDB71D7605BD8A1761835D * value)
	{
		____polygonCollider2D_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____polygonCollider2D_25), (void*)value);
	}
};


// MoreMountains.CorgiEngine.CorgiCinemachineZone
struct CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4  : public MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606
{
public:
	// MoreMountains.CorgiEngine.CinemachineCameraController MoreMountains.CorgiEngine.CorgiCinemachineZone::_cinemachineCameraController
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * ____cinemachineCameraController_26;

public:
	inline static int32_t get_offset_of__cinemachineCameraController_26() { return static_cast<int32_t>(offsetof(CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4, ____cinemachineCameraController_26)); }
	inline CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * get__cinemachineCameraController_26() const { return ____cinemachineCameraController_26; }
	inline CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 ** get_address_of__cinemachineCameraController_26() { return &____cinemachineCameraController_26; }
	inline void set__cinemachineCameraController_26(CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * value)
	{
		____cinemachineCameraController_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cinemachineCameraController_26), (void*)value);
	}
};


// MoreMountains.CorgiEngine.Teleporter
struct Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870  : public ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5
{
public:
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::OnlyAffectsPlayer
	bool ___OnlyAffectsPlayer_50;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Teleporter::ExitOffset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___ExitOffset_51;
	// MoreMountains.CorgiEngine.Teleporter/TeleportationModes MoreMountains.CorgiEngine.Teleporter::TeleportationMode
	int32_t ___TeleportationMode_52;
	// MoreMountains.Tools.MMTween/MMTweenCurve MoreMountains.CorgiEngine.Teleporter::TweenCurve
	int32_t ___TweenCurve_53;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::MaintainXEntryPositionOnExit
	bool ___MaintainXEntryPositionOnExit_54;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::MaintainYEntryPositionOnExit
	bool ___MaintainYEntryPositionOnExit_55;
	// MoreMountains.CorgiEngine.Teleporter MoreMountains.CorgiEngine.Teleporter::Destination
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * ___Destination_56;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::AddToDestinationIgnoreList
	bool ___AddToDestinationIgnoreList_57;
	// MoreMountains.CorgiEngine.Teleporter/CameraModes MoreMountains.CorgiEngine.Teleporter::CameraMode
	int32_t ___CameraMode_58;
	// MoreMountains.CorgiEngine.Room MoreMountains.CorgiEngine.Teleporter::CurrentRoom
	Room_t392D52D203A7F630C190D47A07148A57F4020B42 * ___CurrentRoom_59;
	// MoreMountains.CorgiEngine.Room MoreMountains.CorgiEngine.Teleporter::TargetRoom
	Room_t392D52D203A7F630C190D47A07148A57F4020B42 * ___TargetRoom_60;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::TriggerFade
	bool ___TriggerFade_61;
	// System.Int32 MoreMountains.CorgiEngine.Teleporter::FaderID
	int32_t ___FaderID_62;
	// MoreMountains.Tools.MMTweenType MoreMountains.CorgiEngine.Teleporter::FadeTween
	MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * ___FadeTween_63;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::MoveMask
	bool ___MoveMask_64;
	// MoreMountains.Tools.MMTween/MMTweenCurve MoreMountains.CorgiEngine.Teleporter::MoveMaskCurve
	int32_t ___MoveMaskCurve_65;
	// MoreMountains.Tools.MMSpriteMaskEvent/MMSpriteMaskEventTypes MoreMountains.CorgiEngine.Teleporter::MoveMaskMethod
	int32_t ___MoveMaskMethod_66;
	// System.Single MoreMountains.CorgiEngine.Teleporter::MoveMaskDuration
	float ___MoveMaskDuration_67;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::FreezeTime
	bool ___FreezeTime_68;
	// System.Boolean MoreMountains.CorgiEngine.Teleporter::FreezeCharacter
	bool ___FreezeCharacter_69;
	// System.Single MoreMountains.CorgiEngine.Teleporter::InitialDelay
	float ___InitialDelay_70;
	// System.Single MoreMountains.CorgiEngine.Teleporter::FadeOutDuration
	float ___FadeOutDuration_71;
	// System.Single MoreMountains.CorgiEngine.Teleporter::DelayBetweenFades
	float ___DelayBetweenFades_72;
	// System.Single MoreMountains.CorgiEngine.Teleporter::FadeInDuration
	float ___FadeInDuration_73;
	// System.Single MoreMountains.CorgiEngine.Teleporter::FinalDelay
	float ___FinalDelay_74;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Teleporter::SequenceStartFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___SequenceStartFeedback_75;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Teleporter::AfterInitialDelayFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___AfterInitialDelayFeedback_76;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Teleporter::AfterFadeOutFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___AfterFadeOutFeedback_77;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Teleporter::AfterDelayBetweenFadesFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___AfterDelayBetweenFadesFeedback_78;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Teleporter::SequenceEndFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___SequenceEndFeedback_79;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.CorgiEngine.Teleporter::ArrivedHereFeedback
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ___ArrivedHereFeedback_80;
	// System.Collections.Generic.List`1<UnityEngine.Transform> MoreMountains.CorgiEngine.Teleporter::_ignoreList
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ____ignoreList_81;
	// UnityEngine.WaitForSecondsRealtime MoreMountains.CorgiEngine.Teleporter::_initialDelayWaitForSeconds
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ____initialDelayWaitForSeconds_82;
	// UnityEngine.WaitForSecondsRealtime MoreMountains.CorgiEngine.Teleporter::_fadeOutDurationWaitForSeconds
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ____fadeOutDurationWaitForSeconds_83;
	// UnityEngine.WaitForSecondsRealtime MoreMountains.CorgiEngine.Teleporter::_halfDelayBetweenFadesWaitForSeconds
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ____halfDelayBetweenFadesWaitForSeconds_84;
	// UnityEngine.WaitForSecondsRealtime MoreMountains.CorgiEngine.Teleporter::_fadeInDurationWaitForSeconds
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ____fadeInDurationWaitForSeconds_85;
	// UnityEngine.WaitForSecondsRealtime MoreMountains.CorgiEngine.Teleporter::_finalDelayyWaitForSeconds
	WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * ____finalDelayyWaitForSeconds_86;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Teleporter::_entryPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____entryPosition_87;
	// UnityEngine.Vector3 MoreMountains.CorgiEngine.Teleporter::_newPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ____newPosition_88;

public:
	inline static int32_t get_offset_of_OnlyAffectsPlayer_50() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___OnlyAffectsPlayer_50)); }
	inline bool get_OnlyAffectsPlayer_50() const { return ___OnlyAffectsPlayer_50; }
	inline bool* get_address_of_OnlyAffectsPlayer_50() { return &___OnlyAffectsPlayer_50; }
	inline void set_OnlyAffectsPlayer_50(bool value)
	{
		___OnlyAffectsPlayer_50 = value;
	}

	inline static int32_t get_offset_of_ExitOffset_51() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___ExitOffset_51)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_ExitOffset_51() const { return ___ExitOffset_51; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_ExitOffset_51() { return &___ExitOffset_51; }
	inline void set_ExitOffset_51(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___ExitOffset_51 = value;
	}

	inline static int32_t get_offset_of_TeleportationMode_52() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___TeleportationMode_52)); }
	inline int32_t get_TeleportationMode_52() const { return ___TeleportationMode_52; }
	inline int32_t* get_address_of_TeleportationMode_52() { return &___TeleportationMode_52; }
	inline void set_TeleportationMode_52(int32_t value)
	{
		___TeleportationMode_52 = value;
	}

	inline static int32_t get_offset_of_TweenCurve_53() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___TweenCurve_53)); }
	inline int32_t get_TweenCurve_53() const { return ___TweenCurve_53; }
	inline int32_t* get_address_of_TweenCurve_53() { return &___TweenCurve_53; }
	inline void set_TweenCurve_53(int32_t value)
	{
		___TweenCurve_53 = value;
	}

	inline static int32_t get_offset_of_MaintainXEntryPositionOnExit_54() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___MaintainXEntryPositionOnExit_54)); }
	inline bool get_MaintainXEntryPositionOnExit_54() const { return ___MaintainXEntryPositionOnExit_54; }
	inline bool* get_address_of_MaintainXEntryPositionOnExit_54() { return &___MaintainXEntryPositionOnExit_54; }
	inline void set_MaintainXEntryPositionOnExit_54(bool value)
	{
		___MaintainXEntryPositionOnExit_54 = value;
	}

	inline static int32_t get_offset_of_MaintainYEntryPositionOnExit_55() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___MaintainYEntryPositionOnExit_55)); }
	inline bool get_MaintainYEntryPositionOnExit_55() const { return ___MaintainYEntryPositionOnExit_55; }
	inline bool* get_address_of_MaintainYEntryPositionOnExit_55() { return &___MaintainYEntryPositionOnExit_55; }
	inline void set_MaintainYEntryPositionOnExit_55(bool value)
	{
		___MaintainYEntryPositionOnExit_55 = value;
	}

	inline static int32_t get_offset_of_Destination_56() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___Destination_56)); }
	inline Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * get_Destination_56() const { return ___Destination_56; }
	inline Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 ** get_address_of_Destination_56() { return &___Destination_56; }
	inline void set_Destination_56(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * value)
	{
		___Destination_56 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Destination_56), (void*)value);
	}

	inline static int32_t get_offset_of_AddToDestinationIgnoreList_57() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___AddToDestinationIgnoreList_57)); }
	inline bool get_AddToDestinationIgnoreList_57() const { return ___AddToDestinationIgnoreList_57; }
	inline bool* get_address_of_AddToDestinationIgnoreList_57() { return &___AddToDestinationIgnoreList_57; }
	inline void set_AddToDestinationIgnoreList_57(bool value)
	{
		___AddToDestinationIgnoreList_57 = value;
	}

	inline static int32_t get_offset_of_CameraMode_58() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___CameraMode_58)); }
	inline int32_t get_CameraMode_58() const { return ___CameraMode_58; }
	inline int32_t* get_address_of_CameraMode_58() { return &___CameraMode_58; }
	inline void set_CameraMode_58(int32_t value)
	{
		___CameraMode_58 = value;
	}

	inline static int32_t get_offset_of_CurrentRoom_59() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___CurrentRoom_59)); }
	inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 * get_CurrentRoom_59() const { return ___CurrentRoom_59; }
	inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 ** get_address_of_CurrentRoom_59() { return &___CurrentRoom_59; }
	inline void set_CurrentRoom_59(Room_t392D52D203A7F630C190D47A07148A57F4020B42 * value)
	{
		___CurrentRoom_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentRoom_59), (void*)value);
	}

	inline static int32_t get_offset_of_TargetRoom_60() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___TargetRoom_60)); }
	inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 * get_TargetRoom_60() const { return ___TargetRoom_60; }
	inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 ** get_address_of_TargetRoom_60() { return &___TargetRoom_60; }
	inline void set_TargetRoom_60(Room_t392D52D203A7F630C190D47A07148A57F4020B42 * value)
	{
		___TargetRoom_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetRoom_60), (void*)value);
	}

	inline static int32_t get_offset_of_TriggerFade_61() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___TriggerFade_61)); }
	inline bool get_TriggerFade_61() const { return ___TriggerFade_61; }
	inline bool* get_address_of_TriggerFade_61() { return &___TriggerFade_61; }
	inline void set_TriggerFade_61(bool value)
	{
		___TriggerFade_61 = value;
	}

	inline static int32_t get_offset_of_FaderID_62() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FaderID_62)); }
	inline int32_t get_FaderID_62() const { return ___FaderID_62; }
	inline int32_t* get_address_of_FaderID_62() { return &___FaderID_62; }
	inline void set_FaderID_62(int32_t value)
	{
		___FaderID_62 = value;
	}

	inline static int32_t get_offset_of_FadeTween_63() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FadeTween_63)); }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * get_FadeTween_63() const { return ___FadeTween_63; }
	inline MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 ** get_address_of_FadeTween_63() { return &___FadeTween_63; }
	inline void set_FadeTween_63(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * value)
	{
		___FadeTween_63 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FadeTween_63), (void*)value);
	}

	inline static int32_t get_offset_of_MoveMask_64() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___MoveMask_64)); }
	inline bool get_MoveMask_64() const { return ___MoveMask_64; }
	inline bool* get_address_of_MoveMask_64() { return &___MoveMask_64; }
	inline void set_MoveMask_64(bool value)
	{
		___MoveMask_64 = value;
	}

	inline static int32_t get_offset_of_MoveMaskCurve_65() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___MoveMaskCurve_65)); }
	inline int32_t get_MoveMaskCurve_65() const { return ___MoveMaskCurve_65; }
	inline int32_t* get_address_of_MoveMaskCurve_65() { return &___MoveMaskCurve_65; }
	inline void set_MoveMaskCurve_65(int32_t value)
	{
		___MoveMaskCurve_65 = value;
	}

	inline static int32_t get_offset_of_MoveMaskMethod_66() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___MoveMaskMethod_66)); }
	inline int32_t get_MoveMaskMethod_66() const { return ___MoveMaskMethod_66; }
	inline int32_t* get_address_of_MoveMaskMethod_66() { return &___MoveMaskMethod_66; }
	inline void set_MoveMaskMethod_66(int32_t value)
	{
		___MoveMaskMethod_66 = value;
	}

	inline static int32_t get_offset_of_MoveMaskDuration_67() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___MoveMaskDuration_67)); }
	inline float get_MoveMaskDuration_67() const { return ___MoveMaskDuration_67; }
	inline float* get_address_of_MoveMaskDuration_67() { return &___MoveMaskDuration_67; }
	inline void set_MoveMaskDuration_67(float value)
	{
		___MoveMaskDuration_67 = value;
	}

	inline static int32_t get_offset_of_FreezeTime_68() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FreezeTime_68)); }
	inline bool get_FreezeTime_68() const { return ___FreezeTime_68; }
	inline bool* get_address_of_FreezeTime_68() { return &___FreezeTime_68; }
	inline void set_FreezeTime_68(bool value)
	{
		___FreezeTime_68 = value;
	}

	inline static int32_t get_offset_of_FreezeCharacter_69() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FreezeCharacter_69)); }
	inline bool get_FreezeCharacter_69() const { return ___FreezeCharacter_69; }
	inline bool* get_address_of_FreezeCharacter_69() { return &___FreezeCharacter_69; }
	inline void set_FreezeCharacter_69(bool value)
	{
		___FreezeCharacter_69 = value;
	}

	inline static int32_t get_offset_of_InitialDelay_70() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___InitialDelay_70)); }
	inline float get_InitialDelay_70() const { return ___InitialDelay_70; }
	inline float* get_address_of_InitialDelay_70() { return &___InitialDelay_70; }
	inline void set_InitialDelay_70(float value)
	{
		___InitialDelay_70 = value;
	}

	inline static int32_t get_offset_of_FadeOutDuration_71() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FadeOutDuration_71)); }
	inline float get_FadeOutDuration_71() const { return ___FadeOutDuration_71; }
	inline float* get_address_of_FadeOutDuration_71() { return &___FadeOutDuration_71; }
	inline void set_FadeOutDuration_71(float value)
	{
		___FadeOutDuration_71 = value;
	}

	inline static int32_t get_offset_of_DelayBetweenFades_72() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___DelayBetweenFades_72)); }
	inline float get_DelayBetweenFades_72() const { return ___DelayBetweenFades_72; }
	inline float* get_address_of_DelayBetweenFades_72() { return &___DelayBetweenFades_72; }
	inline void set_DelayBetweenFades_72(float value)
	{
		___DelayBetweenFades_72 = value;
	}

	inline static int32_t get_offset_of_FadeInDuration_73() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FadeInDuration_73)); }
	inline float get_FadeInDuration_73() const { return ___FadeInDuration_73; }
	inline float* get_address_of_FadeInDuration_73() { return &___FadeInDuration_73; }
	inline void set_FadeInDuration_73(float value)
	{
		___FadeInDuration_73 = value;
	}

	inline static int32_t get_offset_of_FinalDelay_74() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___FinalDelay_74)); }
	inline float get_FinalDelay_74() const { return ___FinalDelay_74; }
	inline float* get_address_of_FinalDelay_74() { return &___FinalDelay_74; }
	inline void set_FinalDelay_74(float value)
	{
		___FinalDelay_74 = value;
	}

	inline static int32_t get_offset_of_SequenceStartFeedback_75() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___SequenceStartFeedback_75)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_SequenceStartFeedback_75() const { return ___SequenceStartFeedback_75; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_SequenceStartFeedback_75() { return &___SequenceStartFeedback_75; }
	inline void set_SequenceStartFeedback_75(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___SequenceStartFeedback_75 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SequenceStartFeedback_75), (void*)value);
	}

	inline static int32_t get_offset_of_AfterInitialDelayFeedback_76() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___AfterInitialDelayFeedback_76)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_AfterInitialDelayFeedback_76() const { return ___AfterInitialDelayFeedback_76; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_AfterInitialDelayFeedback_76() { return &___AfterInitialDelayFeedback_76; }
	inline void set_AfterInitialDelayFeedback_76(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___AfterInitialDelayFeedback_76 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AfterInitialDelayFeedback_76), (void*)value);
	}

	inline static int32_t get_offset_of_AfterFadeOutFeedback_77() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___AfterFadeOutFeedback_77)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_AfterFadeOutFeedback_77() const { return ___AfterFadeOutFeedback_77; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_AfterFadeOutFeedback_77() { return &___AfterFadeOutFeedback_77; }
	inline void set_AfterFadeOutFeedback_77(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___AfterFadeOutFeedback_77 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AfterFadeOutFeedback_77), (void*)value);
	}

	inline static int32_t get_offset_of_AfterDelayBetweenFadesFeedback_78() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___AfterDelayBetweenFadesFeedback_78)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_AfterDelayBetweenFadesFeedback_78() const { return ___AfterDelayBetweenFadesFeedback_78; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_AfterDelayBetweenFadesFeedback_78() { return &___AfterDelayBetweenFadesFeedback_78; }
	inline void set_AfterDelayBetweenFadesFeedback_78(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___AfterDelayBetweenFadesFeedback_78 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AfterDelayBetweenFadesFeedback_78), (void*)value);
	}

	inline static int32_t get_offset_of_SequenceEndFeedback_79() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___SequenceEndFeedback_79)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_SequenceEndFeedback_79() const { return ___SequenceEndFeedback_79; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_SequenceEndFeedback_79() { return &___SequenceEndFeedback_79; }
	inline void set_SequenceEndFeedback_79(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___SequenceEndFeedback_79 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SequenceEndFeedback_79), (void*)value);
	}

	inline static int32_t get_offset_of_ArrivedHereFeedback_80() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ___ArrivedHereFeedback_80)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get_ArrivedHereFeedback_80() const { return ___ArrivedHereFeedback_80; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of_ArrivedHereFeedback_80() { return &___ArrivedHereFeedback_80; }
	inline void set_ArrivedHereFeedback_80(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		___ArrivedHereFeedback_80 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ArrivedHereFeedback_80), (void*)value);
	}

	inline static int32_t get_offset_of__ignoreList_81() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____ignoreList_81)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get__ignoreList_81() const { return ____ignoreList_81; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of__ignoreList_81() { return &____ignoreList_81; }
	inline void set__ignoreList_81(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		____ignoreList_81 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____ignoreList_81), (void*)value);
	}

	inline static int32_t get_offset_of__initialDelayWaitForSeconds_82() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____initialDelayWaitForSeconds_82)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get__initialDelayWaitForSeconds_82() const { return ____initialDelayWaitForSeconds_82; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of__initialDelayWaitForSeconds_82() { return &____initialDelayWaitForSeconds_82; }
	inline void set__initialDelayWaitForSeconds_82(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		____initialDelayWaitForSeconds_82 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____initialDelayWaitForSeconds_82), (void*)value);
	}

	inline static int32_t get_offset_of__fadeOutDurationWaitForSeconds_83() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____fadeOutDurationWaitForSeconds_83)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get__fadeOutDurationWaitForSeconds_83() const { return ____fadeOutDurationWaitForSeconds_83; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of__fadeOutDurationWaitForSeconds_83() { return &____fadeOutDurationWaitForSeconds_83; }
	inline void set__fadeOutDurationWaitForSeconds_83(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		____fadeOutDurationWaitForSeconds_83 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fadeOutDurationWaitForSeconds_83), (void*)value);
	}

	inline static int32_t get_offset_of__halfDelayBetweenFadesWaitForSeconds_84() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____halfDelayBetweenFadesWaitForSeconds_84)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get__halfDelayBetweenFadesWaitForSeconds_84() const { return ____halfDelayBetweenFadesWaitForSeconds_84; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of__halfDelayBetweenFadesWaitForSeconds_84() { return &____halfDelayBetweenFadesWaitForSeconds_84; }
	inline void set__halfDelayBetweenFadesWaitForSeconds_84(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		____halfDelayBetweenFadesWaitForSeconds_84 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____halfDelayBetweenFadesWaitForSeconds_84), (void*)value);
	}

	inline static int32_t get_offset_of__fadeInDurationWaitForSeconds_85() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____fadeInDurationWaitForSeconds_85)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get__fadeInDurationWaitForSeconds_85() const { return ____fadeInDurationWaitForSeconds_85; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of__fadeInDurationWaitForSeconds_85() { return &____fadeInDurationWaitForSeconds_85; }
	inline void set__fadeInDurationWaitForSeconds_85(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		____fadeInDurationWaitForSeconds_85 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____fadeInDurationWaitForSeconds_85), (void*)value);
	}

	inline static int32_t get_offset_of__finalDelayyWaitForSeconds_86() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____finalDelayyWaitForSeconds_86)); }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * get__finalDelayyWaitForSeconds_86() const { return ____finalDelayyWaitForSeconds_86; }
	inline WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 ** get_address_of__finalDelayyWaitForSeconds_86() { return &____finalDelayyWaitForSeconds_86; }
	inline void set__finalDelayyWaitForSeconds_86(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * value)
	{
		____finalDelayyWaitForSeconds_86 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____finalDelayyWaitForSeconds_86), (void*)value);
	}

	inline static int32_t get_offset_of__entryPosition_87() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____entryPosition_87)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__entryPosition_87() const { return ____entryPosition_87; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__entryPosition_87() { return &____entryPosition_87; }
	inline void set__entryPosition_87(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____entryPosition_87 = value;
	}

	inline static int32_t get_offset_of__newPosition_88() { return static_cast<int32_t>(offsetof(Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870, ____newPosition_88)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get__newPosition_88() const { return ____newPosition_88; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of__newPosition_88() { return &____newPosition_88; }
	inline void set__newPosition_88(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		____newPosition_88 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 Cinemachine.CinemachineVirtualCamera::GetCinemachineComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * CinemachineVirtualCamera_GetCinemachineComponent_TisRuntimeObject_m057362AEC21C83F60E2A6F055FCAC147F961D6EB_gshared (CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * __this, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMSingleton`1<System.Object>::get_Instance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared (const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentNoAlloc<System.Object>(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObjectExtensions_MMGetComponentNoAlloc_TisRuntimeObject_m4490C39D71D5B192FDC4A00FBCA384ADF9C5F70D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.MMCameraEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.MMCameraEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentAroundOrAdd<System.Object>(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObjectExtensions_MMGetComponentAroundOrAdd_TisRuntimeObject_m24AB316404DDFC727D3D62154DEB86D480FA6BC5_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMEventManager::TriggerEvent<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>(!!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0_gshared (MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  ___newEvent0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean MoreMountains.Tools.MMSingleton`1<System.Object>::get_HasInstance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool MMSingleton_1_get_HasInstance_mB78637D86379B68203F768178DBCECD3F9BC0184_gshared (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponentInParent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponentInParent_TisRuntimeObject_m1CAE1DA88E1147164A44737BD03D5CB12DC73673_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);

// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Collider::get_bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  Collider_get_bounds_mE341D29E1DA184ADD53A474D57D9082A3550EACB (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_center()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8 (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Collider2D::get_offset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Collider2D_get_offset_m63144560DFF782608BA6627DE192D6337821995C (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Behaviour::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 Cinemachine.CameraState::get_RawPosition()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CameraState_get_RawPosition_mA01BC7F842C837D49F0542A0475AE0EA9C88539D_inline (CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * __this, const RuntimeMethod* method);
// System.Void Cinemachine.CameraState::set_RawPosition(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CameraState_set_RawPosition_m0E1142A79939C7AD7156986F91CD0F1D60499A31_inline (CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Void Cinemachine.CinemachineExtension::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineExtension__ctor_m7331AF4105FCAFF48E3CC9A412B1953647E0BEBA (CinemachineExtension_t8FBFE85287359DD26462EE5C24031D8433801422 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<Cinemachine.CinemachineBrain>()
inline CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * GameObject_GetComponent_TisCinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_m2A1B17232E3845E1A0E19D3B3A62281373C4A2DA (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2_gshared)(___caller0, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Cinemachine.CinemachineVirtualCamera>()
inline CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * Component_GetComponent_TisCinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_m1CD1808D474C06AE9FFA2DE1BE67E8D681311E0E (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<Cinemachine.CinemachineConfiner>()
inline CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * Component_GetComponent_TisCinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D_mC97A95766A1AF55DFD6A83919B545B8CF30AF8C6 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean Cinemachine.LensSettings::get_Orthographic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool LensSettings_get_Orthographic_mD89E6548BD05054F1ADFF7CB767ADCBA147970A0 (LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * __this, const RuntimeMethod* method);
// !!0 Cinemachine.CinemachineVirtualCamera::GetCinemachineComponent<Cinemachine.CinemachineFramingTransposer>()
inline CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1_mAC4EB763DB66C85C1998F64A22FF144B5C587E2C (CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * __this, const RuntimeMethod* method)
{
	return ((  CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * (*) (CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C *, const RuntimeMethod*))CinemachineVirtualCamera_GetCinemachineComponent_TisRuntimeObject_m057362AEC21C83F60E2A6F055FCAC147F961D6EB_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.LevelManager>::get_Instance()
inline LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B (const RuntimeMethod* method)
{
	return ((  LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * (*) (const RuntimeMethod*))MMSingleton_1_get_Instance_mC267244DAA054383A8A9060924FB7B27B8E9B392_gshared)(method);
}
// UnityEngine.Collider2D MoreMountains.CorgiEngine.LevelManager::get_BoundsCollider2D()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * LevelManager_get_BoundsCollider2D_m38E36A905EB6DF0E0B21758DDA955453641339B0_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method);
// UnityEngine.Collider MoreMountains.CorgiEngine.LevelManager::get_BoundsCollider()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * LevelManager_get_BoundsCollider_mF941E9DDAD3B216DE55E1A7ECD29F1A3D349E922_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentNoAlloc<MoreMountains.CorgiEngine.CorgiController>(UnityEngine.GameObject)
inline CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * GameObjectExtensions_MMGetComponentNoAlloc_TisCorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8_m0F37741609CF11EE74C0EEC69597FE9F0E1525E8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method)
{
	return ((  CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObjectExtensions_MMGetComponentNoAlloc_TisRuntimeObject_m4490C39D71D5B192FDC4A00FBCA384ADF9C5F70D_gshared)(___this0, method);
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::set_FollowsPlayer(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD_inline (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_Speed()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline (CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Single MoreMountains.Tools.MMMaths::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMMaths_Remap_mBEAF91CCEABC9ACD132FC4FF072A8A463B2F6C60 (float ___x0, float ___A1, float ___B2, float ___C3, float ___D4, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Void Cinemachine.CinemachineVirtualCameraBase::set_Priority(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CinemachineVirtualCameraBase_set_Priority_m6C180B742F19E669D648B6D1BE4D9D9C5824962B_inline (CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character> MoreMountains.CorgiEngine.LevelManager::get_Players()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character>::get_Item(System.Int32)
inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_inline (List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * (*) (List_1_t5DB922CEE6338D263668FE75E45C8566802F493E *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.MMCameraEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.MMCameraEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.CorgiEngine.CorgiEngineEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17 (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_gshared)(___caller0, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMCinemachineZone::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCinemachineZone_Awake_m9B042DE73AD8085DB8528B7158BC183568BDA5CE (MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentAroundOrAdd<MoreMountains.CorgiEngine.CinemachineCameraController>(UnityEngine.GameObject)
inline CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * GameObjectExtensions_MMGetComponentAroundOrAdd_TisCinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_m0B72B1326D70ABA9819B735C42FBB0F6F44E68DF (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method)
{
	return ((  CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObjectExtensions_MMGetComponentAroundOrAdd_TisRuntimeObject_m24AB316404DDFC727D3D62154DEB86D480FA6BC5_gshared)(___this0, method);
}
// System.Void MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEnableCameraU3Ed__2__ctor_m1D9DDDCC28E121A6A0C27F904A2A8F29F7E70CCD (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMCinemachineZone2D::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCinemachineZone2D__ctor_mE6617D271D06E0855AADE639A48BEBCD50626542 (MMCinemachineZone2D_t9B54814925C52FF899FB8347DE5EBD254BA90606 * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator MoreMountains.Tools.MMCinemachineZone::EnableCamera(System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MMCinemachineZone_EnableCamera_m00AEFAEF3455A2462F4433F0F6E1863555400D52 (MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5 * __this, bool ___state0, int32_t ___frames1, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.MMCinemachineBrainEvent::.ctor(MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCinemachineBrainEvent__ctor_m84DE9FBB871D80C3824E0AD21BF19B7B4446FE18 (MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF * __this, int32_t ___eventType0, float ___duration1, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMEventManager::TriggerEvent<MoreMountains.CorgiEngine.MMCinemachineBrainEvent>(!!0)
inline void MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0 (MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  ___newEvent0, const RuntimeMethod* method)
{
	((  void (*) (MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF , const RuntimeMethod*))MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0_gshared)(___newEvent0, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Collider2D>()
inline Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<UnityEngine.GameObject>::GetEnumerator()
inline Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6 (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  (*) (List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::get_Current()
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::MoveNext()
inline bool Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GameObject>::Dispose()
inline void Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9 (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResizeConfinerU3Ed__19__ctor_mFD9E4619E6D3E4ABD7AE146801D22E1C2F5F2A59 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Boolean MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.LevelManager>::get_HasInstance()
inline bool MMSingleton_1_get_HasInstance_mD391A08D92462F740297996852235491E938BCDD (const RuntimeMethod* method)
{
	return ((  bool (*) (const RuntimeMethod*))MMSingleton_1_get_HasInstance_mB78637D86379B68203F768178DBCECD3F9BC0184_gshared)(method);
}
// System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Bounds_Contains_m24154F7F564711846389EF6AEDABB4092A72FFA1 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.MMCameraEvent::Trigger(MoreMountains.CorgiEngine.MMCameraEventTypes,MoreMountains.CorgiEngine.Character,UnityEngine.Collider,UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8 (int32_t ___eventType0, Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___targetCharacter1, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___bounds2, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___bounds2D3, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.MMCinemachineBrainEvent::Trigger(MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCinemachineBrainEvent_Trigger_m3393F1966039CFA331A3171293F8E3726A432F4E (int32_t ___eventType0, float ___duration1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Bounds::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14 (Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSpriteMaskEvent::Trigger(MoreMountains.Tools.MMSpriteMaskEvent/MMSpriteMaskEventTypes,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSpriteMaskEvent_Trigger_m94383E73C7E743D4ED07BFBB86ABE48F62A9833D (int32_t ___eventType0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newPosition1, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___newSize2, float ___duration3, int32_t ___curve4, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::.ctor()
inline void List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// !!0 UnityEngine.GameObject::GetComponentInParent<MoreMountains.CorgiEngine.Room>()
inline Room_t392D52D203A7F630C190D47A07148A57F4020B42 * GameObject_GetComponentInParent_TisRoom_t392D52D203A7F630C190D47A07148A57F4020B42_m66E446560DA3F59C8F60C6F850D6C5DE7D7C6610 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Room_t392D52D203A7F630C190D47A07148A57F4020B42 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponentInParent_TisRuntimeObject_m1CAE1DA88E1147164A44737BD03D5CB12DC73673_gshared)(__this, method);
}
// System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225 (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * __this, float ___time0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Contains(!0)
inline bool List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))List_1_Contains_m99C700668AC6D272188471D2D6B784A2B5636C8E_gshared)(__this, ___item0, method);
}
// System.Void MoreMountains.CorgiEngine.ButtonActivated::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonActivated_OnTriggerEnter2D_m658128E15BA4BDCCF88ABA9970E0BFB9A6C305BB (ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collidingObject0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.ButtonActivated::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonActivated_OnTriggerStay2D_m9C2A689D96D707EC3B7AA81744CCF933F087BB13 (ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collidingObject0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.ButtonActivated::TriggerButtonAction(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonActivated_TriggerButtonAction_m90ABA5F03388FD3B7B2DC788838C3D638F02ED6F (ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___instigator0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportSequenceU3Ed__47__ctor_m8AB83EFFF24D4A556C827B654ED7B8915B8DF816 (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMTimeScaleEvent::Trigger(MoreMountains.Feedbacks.MMTimeScaleMethods,System.Single,System.Single,System.Boolean,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMTimeScaleEvent_Trigger_mF54D807E19DF44F9EB1E07B9A76C8E294632741E (int32_t ___timeScaleMethod0, float ___timeScale1, float ___duration2, bool ___lerp3, float ___lerpSpeed4, bool ___infinite5, const RuntimeMethod* method);
// !!0 MoreMountains.Tools.GameObjectExtensions::MMGetComponentNoAlloc<MoreMountains.CorgiEngine.Character>(UnityEngine.GameObject)
inline Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___this0, const RuntimeMethod* method)
{
	return ((  Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObjectExtensions_MMGetComponentNoAlloc_TisRuntimeObject_m4490C39D71D5B192FDC4A00FBCA384ADF9C5F70D_gshared)(___this0, method);
}
// System.Void MoreMountains.Tools.MMFadeInEvent::Trigger(System.Single,MoreMountains.Tools.MMTweenType,System.Int32,System.Boolean,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFadeInEvent_Trigger_m090C697AF081E64E75802956A665E2DA9FF5C666 (float ___duration0, MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * ___tween1, int32_t ___id2, bool ___ignoreTimeScale3, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition4, const RuntimeMethod* method);
// UnityEngine.Collider2D MoreMountains.CorgiEngine.Room::get_RoomCollider()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * Room_get_RoomCollider_mE452329CF0C1B9ACBB4CC515C9DD4ADF511ACC6F_inline (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Transform>::Remove(!0)
inline bool List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportTweenCoU3Ed__52__ctor_m1644936594C6AC11FF6F80E342A05642C083BF68 (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.LevelManager::get_LevelCameraController()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * LevelManager_get_LevelCameraController_m81BFB44DD7452381F90CE3F622A7FF33A5953152_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMFadeOutEvent::Trigger(System.Single,MoreMountains.Tools.MMTweenType,System.Int32,System.Boolean,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFadeOutEvent_Trigger_m0AECC19A24868A953A20E499EF61A676555E8EA5 (float ___duration0, MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * ___tween1, int32_t ___id2, bool ___ignoreTimeScale3, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition4, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.ButtonActivated::TriggerExitAction(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonActivated_TriggerExitAction_m97D1EAB36E83B9D79288723D747EB956CFAE66D8 (ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___collider0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Transform>::Add(!0)
inline void List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982 (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_cyan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_cyan_m0C608BC083FD98C45C1F4F15AE803D288C647686 (const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMDebug::DrawGizmoArrow(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDebug_DrawGizmoArrow_m6B5F0DF9A12597B04EBE240E7CBA67F721A49AE1 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, float ___arrowHeadLength3, float ___arrowHeadAngle4, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1 (const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMDebug::DebugDrawCross(UnityEngine.Vector3,System.Single,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDebug_DebugDrawCross_mACB3343EF705188007D0CB3841BC1AEE194CCF0D (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___spot0, float ___crossSize1, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color2, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMDebug::DrawPoint(UnityEngine.Vector3,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDebug_DrawPoint_m3D487A68FDEB4E5EF7F4976D70445A10A0A0D38A (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color1, float ___size2, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMTweenType::.ctor(MoreMountains.Tools.MMTween/MMTweenCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMTweenType__ctor_mE4203911CD62AF1A455460DC0674447C2CBE0D8D (MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * __this, int32_t ___newCurve0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.ButtonActivated::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonActivated__ctor_m6FEA8A488C4C38817E424F66B6956EB282CFEE81 (ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiCinemachineZone::<>n__0(System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiCinemachineZone_U3CU3En__0_m2B0BD1A14B71AD52EE8B2105DE29729579DE90A9 (CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * __this, bool ___state0, int32_t ___frames1, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Collider2D::set_offset(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Collider2D_set_offset_m496DDE5487C59E9E8D6B468E8918EE1EFC625E38 (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  BoxCollider2D_get_size_m011E7AA7861BF58898A64D986A4235C1E2061BF9 (BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.BoxCollider2D::set_size(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760 (BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_orthographicSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_orthographicSize_m970DC87D428A71EDF30F9ED7D0E76E08B1BE4EFE (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Camera::get_aspect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987 (Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * __this, const RuntimeMethod* method);
// System.Void Cinemachine.CinemachineConfiner::InvalidatePathCache()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineConfiner_InvalidatePathCache_m1C66E34112C65B4CDACC628AD8D7F8C90F45706B (CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258 (const RuntimeMethod* method);
// UnityEngine.Vector3 MoreMountains.Tools.MMTween::Tween(System.Single,System.Single,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,MoreMountains.Tools.MMTween/MMTweenCurve)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  MMTween_Tween_m2BDC8328BE236482F235949CE01A9CE2118CA21E (float ___currentTime0, float ___initialTime1, float ___endTime2, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startValue3, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue4, int32_t ___curve5, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.CinemachineAxisLocker::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineAxisLocker_Start_m233B89B27D99B45E63B415C8B7543791FE630E54 (CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// switch (Method)
		int32_t L_0 = __this->get_Method_10();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_001e;
			}
			case 1:
			{
				goto IL_002b;
			}
			case 2:
			{
				goto IL_003d;
			}
			case 3:
			{
				goto IL_0057;
			}
		}
	}
	{
		return;
	}

IL_001e:
	{
		// _forcedPosition = ForcedPosition;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = __this->get_ForcedPosition_11();
		__this->set__forcedPosition_14(L_2);
		// break;
		return;
	}

IL_002b:
	{
		// _forcedPosition = this.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		__this->set__forcedPosition_14(L_4);
		// break;
		return;
	}

IL_003d:
	{
		// _forcedPosition = TargetCollider.bounds.center;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_5 = __this->get_TargetCollider_12();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_6;
		L_6 = Collider_get_bounds_mE341D29E1DA184ADD53A474D57D9082A3550EACB(L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		__this->set__forcedPosition_14(L_7);
		// break;
		return;
	}

IL_0057:
	{
		// _forcedPosition = TargetCollider2D.bounds.center + (Vector3)TargetCollider2D.offset;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_8 = __this->get_TargetCollider2D_13();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_9;
		L_9 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_1), /*hidden argument*/NULL);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_11 = __this->get_TargetCollider2D_13();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12;
		L_12 = Collider2D_get_offset_m63144560DFF782608BA6627DE192D6337821995C(L_11, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline(L_12, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_10, L_13, /*hidden argument*/NULL);
		__this->set__forcedPosition_14(L_14);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineAxisLocker::PostPipelineStageCallback(Cinemachine.CinemachineVirtualCameraBase,Cinemachine.CinemachineCore/Stage,Cinemachine.CameraState&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineAxisLocker_PostPipelineStageCallback_mDA354CDC653DE0BECFFA324532EBFB8C54CF233E (CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB * __this, CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * ___vcam0, int32_t ___stage1, CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * ___state2, float ___deltaTime3, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (enabled && stage == CinemachineCore.Stage.Body)
		bool L_0;
		L_0 = Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_1 = ___stage1;
		if (L_1)
		{
			goto IL_0067;
		}
	}
	{
		// Vector3 pos = state.RawPosition;
		CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * L_2 = ___state2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = CameraState_get_RawPosition_mA01BC7F842C837D49F0542A0475AE0EA9C88539D_inline((CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 *)L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		// if (LockXAxis)
		bool L_4 = __this->get_LockXAxis_7();
		if (!L_4)
		{
			goto IL_002c;
		}
	}
	{
		// pos.x = _forcedPosition.x;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_5 = __this->get_address_of__forcedPosition_14();
		float L_6 = L_5->get_x_2();
		(&V_0)->set_x_2(L_6);
	}

IL_002c:
	{
		// if (LockYAxis)
		bool L_7 = __this->get_LockYAxis_8();
		if (!L_7)
		{
			goto IL_0046;
		}
	}
	{
		// pos.y = _forcedPosition.y;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_8 = __this->get_address_of__forcedPosition_14();
		float L_9 = L_8->get_y_3();
		(&V_0)->set_y_3(L_9);
	}

IL_0046:
	{
		// if (LockZAxis)
		bool L_10 = __this->get_LockZAxis_9();
		if (!L_10)
		{
			goto IL_0060;
		}
	}
	{
		// pos.z = _forcedPosition.z;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of__forcedPosition_14();
		float L_12 = L_11->get_z_4();
		(&V_0)->set_z_4(L_12);
	}

IL_0060:
	{
		// state.RawPosition = pos;
		CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * L_13 = ___state2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = V_0;
		CameraState_set_RawPosition_m0E1142A79939C7AD7156986F91CD0F1D60499A31_inline((CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 *)L_13, L_14, /*hidden argument*/NULL);
	}

IL_0067:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineAxisLocker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineAxisLocker__ctor_mAEAB13553DC51B67783A00031BCDBD32D485DE35 (CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB * __this, const RuntimeMethod* method)
{
	{
		// public Methods Method = Methods.InitialPosition;
		__this->set_Method_10(1);
		CinemachineExtension__ctor_m7331AF4105FCAFF48E3CC9A412B1953647E0BEBA(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.CinemachineBrainController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineBrainController_Awake_m9CB38AC20EE89A4503546D357ECE3F51433FAF48 (CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_m2A1B17232E3845E1A0E19D3B3A62281373C4A2DA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _brain = this.gameObject.GetComponent<CinemachineBrain>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * L_1;
		L_1 = GameObject_GetComponent_TisCinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_m2A1B17232E3845E1A0E19D3B3A62281373C4A2DA(L_0, /*hidden argument*/GameObject_GetComponent_TisCinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_m2A1B17232E3845E1A0E19D3B3A62281373C4A2DA_RuntimeMethod_var);
		__this->set__brain_4(L_1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineBrainController::SetDefaultBlendDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineBrainController_SetDefaultBlendDuration_m660DE1772082A4DBC0C9DDCA4CC5828A038AE138 (CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00 * __this, float ___newDuration0, const RuntimeMethod* method)
{
	{
		// _brain.m_DefaultBlend.m_Time = newDuration;
		CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83 * L_0 = __this->get__brain_4();
		CinemachineBlendDefinition_tFCB9356176B08582D6AB0B036E25729EBEDBFAEE * L_1 = L_0->get_address_of_m_DefaultBlend_10();
		float L_2 = ___newDuration0;
		L_1->set_m_Time_1(L_2);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineBrainController::OnMMEvent(MoreMountains.CorgiEngine.MMCinemachineBrainEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineBrainController_OnMMEvent_mE87D1FDD34615CA6B0479F57522C4FF921C4BDA1 (CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00 * __this, MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  ___cinemachineBrainEvent0, const RuntimeMethod* method)
{
	{
		// switch (cinemachineBrainEvent.EventType)
		MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  L_0 = ___cinemachineBrainEvent0;
		int32_t L_1 = L_0.get_EventType_0();
		if (L_1)
		{
			goto IL_0014;
		}
	}
	{
		// SetDefaultBlendDuration(cinemachineBrainEvent.Duration);
		MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  L_2 = ___cinemachineBrainEvent0;
		float L_3 = L_2.get_Duration_1();
		VirtActionInvoker1< float >::Invoke(6 /* System.Void MoreMountains.CorgiEngine.CinemachineBrainController::SetDefaultBlendDuration(System.Single) */, __this, L_3);
	}

IL_0014:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineBrainController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineBrainController_OnEnable_mA8E36426A0226FAE49D1EA7AF949D6A5A7117DDE (CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStartListening<MMCinemachineBrainEvent>();
		EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF7CCA68743C86ECBA3DB14CF9F8809E06CD2DBE0_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineBrainController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineBrainController_OnDisable_m7F71A3668681DF8306CBE0769154A3593405644B (CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStopListening<MMCinemachineBrainEvent>();
		EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_m6C8312BF76AB4B503B28976BE6DAD2F99C374EF2_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineBrainController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineBrainController__ctor_m37DCD33E3B92CF88BFE6002B7D1AF582E91FB8AB (CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.CorgiEngine.CinemachineCameraController::get_FollowsPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CinemachineCameraController_get_FollowsPlayer_m062EA52273780A614BB2705A35A3852DF542C05A (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// public bool FollowsPlayer { get; set; }
		bool L_0 = __this->get_U3CFollowsPlayerU3Ek__BackingField_4();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::set_FollowsPlayer(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool FollowsPlayer { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CFollowsPlayerU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_Awake_m141F1FD186BF0533B0E775F0BBB07A5B7F4BB6F1 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// Initialization();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::Initialization() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_Initialization_m14A01A14674088F49182D3B31243F07FA3359DE6 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1_mAC4EB763DB66C85C1998F64A22FF144B5C587E2C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D_mC97A95766A1AF55DFD6A83919B545B8CF30AF8C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisCinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_m1CD1808D474C06AE9FFA2DE1BE67E8D681311E0E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * G_B4_0 = NULL;
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * G_B3_0 = NULL;
	float G_B5_0 = 0.0f;
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * G_B5_1 = NULL;
	{
		// if (_initialized)
		bool L_0 = __this->get__initialized_27();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// _virtualCamera = GetComponent<CinemachineVirtualCamera>();
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_1;
		L_1 = Component_GetComponent_TisCinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_m1CD1808D474C06AE9FFA2DE1BE67E8D681311E0E(__this, /*hidden argument*/Component_GetComponent_TisCinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C_m1CD1808D474C06AE9FFA2DE1BE67E8D681311E0E_RuntimeMethod_var);
		__this->set__virtualCamera_23(L_1);
		// _confiner = GetComponent<CinemachineConfiner>();
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_2;
		L_2 = Component_GetComponent_TisCinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D_mC97A95766A1AF55DFD6A83919B545B8CF30AF8C6(__this, /*hidden argument*/Component_GetComponent_TisCinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D_mC97A95766A1AF55DFD6A83919B545B8CF30AF8C6_RuntimeMethod_var);
		__this->set__confiner_24(L_2);
		// _currentZoom = _virtualCamera.m_Lens.Orthographic ? InitialOrthographicZoom : InitialPerspectiveZoom;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_3 = __this->get__virtualCamera_23();
		LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * L_4 = L_3->get_address_of_m_Lens_22();
		bool L_5;
		L_5 = LensSettings_get_Orthographic_mD89E6548BD05054F1ADFF7CB767ADCBA147970A0((LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA *)L_4, /*hidden argument*/NULL);
		G_B3_0 = __this;
		if (L_5)
		{
			G_B4_0 = __this;
			goto IL_003c;
		}
	}
	{
		float L_6 = __this->get_InitialPerspectiveZoom_18();
		G_B5_0 = L_6;
		G_B5_1 = G_B3_0;
		goto IL_0042;
	}

IL_003c:
	{
		float L_7 = __this->get_InitialOrthographicZoom_13();
		G_B5_0 = L_7;
		G_B5_1 = G_B4_0;
	}

IL_0042:
	{
		G_B5_1->set__currentZoom_26(G_B5_0);
		// _framingTransposer = _virtualCamera.GetCinemachineComponent<CinemachineFramingTransposer>();
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_8 = __this->get__virtualCamera_23();
		CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * L_9;
		L_9 = CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1_mAC4EB763DB66C85C1998F64A22FF144B5C587E2C(L_8, /*hidden argument*/CinemachineVirtualCamera_GetCinemachineComponent_TisCinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1_mAC4EB763DB66C85C1998F64A22FF144B5C587E2C_RuntimeMethod_var);
		__this->set__framingTransposer_25(L_9);
		// _initialized = true;
		__this->set__initialized_27((bool)1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_Start_m42930153C4D21D98C000FD556918E7382FA6C935 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// InitializeConfiner();
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::InitializeConfiner() */, __this);
		// if (UseOrthographicZoom)
		bool L_0 = __this->get_UseOrthographicZoom_11();
		if (!L_0)
		{
			goto IL_0024;
		}
	}
	{
		// _virtualCamera.m_Lens.OrthographicSize = InitialOrthographicZoom;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_1 = __this->get__virtualCamera_23();
		LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * L_2 = L_1->get_address_of_m_Lens_22();
		float L_3 = __this->get_InitialOrthographicZoom_13();
		L_2->set_OrthographicSize_2(L_3);
	}

IL_0024:
	{
		// if (UsePerspectiveZoom)
		bool L_4 = __this->get_UsePerspectiveZoom_15();
		if (!L_4)
		{
			goto IL_0038;
		}
	}
	{
		// SetPerspectiveZoom(InitialPerspectiveZoom);
		float L_5 = __this->get_InitialPerspectiveZoom_18();
		VirtActionInvoker1< float >::Invoke(17 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetPerspectiveZoom(System.Single) */, __this, L_5);
	}

IL_0038:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::InitializeConfiner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_InitializeConfiner_m8CD6C9FB993479C5D093080F49904CFCEA9E6F5B (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((_confiner != null) && ConfineCameraToLevelBounds)
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_0 = __this->get__confiner_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_004e;
		}
	}
	{
		bool L_2 = __this->get_ConfineCameraToLevelBounds_6();
		if (!L_2)
		{
			goto IL_004e;
		}
	}
	{
		// if (_confiner.m_ConfineMode == CinemachineConfiner.Mode.Confine2D)
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_3 = __this->get__confiner_24();
		int32_t L_4 = L_3->get_m_ConfineMode_7();
		if (L_4)
		{
			goto IL_0039;
		}
	}
	{
		// _confiner.m_BoundingShape2D = LevelManager.Instance.BoundsCollider2D;
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_5 = __this->get__confiner_24();
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_6;
		L_6 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_7;
		L_7 = LevelManager_get_BoundsCollider2D_m38E36A905EB6DF0E0B21758DDA955453641339B0_inline(L_6, /*hidden argument*/NULL);
		L_5->set_m_BoundingShape2D_9(L_7);
		// }
		return;
	}

IL_0039:
	{
		// _confiner.m_BoundingVolume = LevelManager.Instance.BoundsCollider;
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_8 = __this->get__confiner_24();
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_9;
		L_9 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_10;
		L_10 = LevelManager_get_BoundsCollider_mF941E9DDAD3B216DE55E1A7ECD29F1A3D349E922_inline(L_9, /*hidden argument*/NULL);
		L_8->set_m_BoundingVolume_8(L_10);
	}

IL_004e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetTarget(MoreMountains.CorgiEngine.Character)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_SetTarget_mFA0C40836834B957AF622BCEBEEA5FBE7F1BA9A7 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * ___character0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_MMGetComponentNoAlloc_TisCorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8_m0F37741609CF11EE74C0EEC69597FE9F0E1525E8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// TargetCharacter = character;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_0 = ___character0;
		__this->set_TargetCharacter_9(L_0);
		// TargetController = character.gameObject.MMGetComponentNoAlloc<CorgiController>();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_1 = ___character0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_3;
		L_3 = GameObjectExtensions_MMGetComponentNoAlloc_TisCorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8_m0F37741609CF11EE74C0EEC69597FE9F0E1525E8(L_2, /*hidden argument*/GameObjectExtensions_MMGetComponentNoAlloc_TisCorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8_m0F37741609CF11EE74C0EEC69597FE9F0E1525E8_RuntimeMethod_var);
		__this->set_TargetController_10(L_3);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StartFollowing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_StartFollowing_m3CC528B40722BD3160F83BD10083D7F64F07E01D (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// Initialization();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::Initialization() */, __this);
		// if (!FollowsAPlayer) { return; }
		bool L_0 = __this->get_FollowsAPlayer_5();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// if (!FollowsAPlayer) { return; }
		return;
	}

IL_000f:
	{
		// FollowsPlayer = true;
		CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD_inline(__this, (bool)1, /*hidden argument*/NULL);
		// _virtualCamera.Follow = TargetCharacter.CameraTarget.transform;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_1 = __this->get__virtualCamera_23();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_2 = __this->get_TargetCharacter_9();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = L_2->get_CameraTarget_15();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_3, /*hidden argument*/NULL);
		VirtActionInvoker1< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(30 /* System.Void Cinemachine.CinemachineVirtualCameraBase::set_Follow(UnityEngine.Transform) */, L_1, L_4);
		// _virtualCamera.enabled = true;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_5 = __this->get__virtualCamera_23();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_5, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StopFollowing()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_StopFollowing_mB188B7280467613FE1FD09AE961031202AF8C810 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// Initialization();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::Initialization() */, __this);
		// if (!FollowsAPlayer) { return; }
		bool L_0 = __this->get_FollowsAPlayer_5();
		if (L_0)
		{
			goto IL_000f;
		}
	}
	{
		// if (!FollowsAPlayer) { return; }
		return;
	}

IL_000f:
	{
		// FollowsPlayer = false;
		CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD_inline(__this, (bool)0, /*hidden argument*/NULL);
		// _virtualCamera.Follow = null;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_1 = __this->get__virtualCamera_23();
		VirtActionInvoker1< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(30 /* System.Void Cinemachine.CinemachineVirtualCameraBase::set_Follow(UnityEngine.Transform) */, L_1, (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL);
		// _virtualCamera.enabled = false;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_2 = __this->get__virtualCamera_23();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_2, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_LateUpdate_m4C4945A3F0A12080DB6C553BF858B0DC37F11D22 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// HandleZoom();
		VirtActionInvoker0::Invoke(14 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::HandleZoom() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::HandleZoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_HandleZoom_m3DD6876F21AC410A27E7DE8689AA387800A2B77C (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// if (_virtualCamera.m_Lens.Orthographic)
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_0 = __this->get__virtualCamera_23();
		LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * L_1 = L_0->get_address_of_m_Lens_22();
		bool L_2;
		L_2 = LensSettings_get_Orthographic_mD89E6548BD05054F1ADFF7CB767ADCBA147970A0((LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA *)L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// PerformOrthographicZoom();
		VirtActionInvoker0::Invoke(15 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::PerformOrthographicZoom() */, __this);
		// }
		return;
	}

IL_0019:
	{
		// PerformPerspectiveZoom();
		VirtActionInvoker0::Invoke(16 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::PerformPerspectiveZoom() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::PerformOrthographicZoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_PerformOrthographicZoom_m71115161B6B7CB274D52D5C5A9C3E3A7B89A89DE (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (!UseOrthographicZoom || (TargetController == null))
		bool L_0 = __this->get_UseOrthographicZoom_11();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_1 = __this->get_TargetController_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		// return;
		return;
	}

IL_0017:
	{
		// float characterSpeed = Mathf.Abs(TargetController.Speed.x);
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_3 = __this->get_TargetController_10();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_x_0();
		float L_6;
		L_6 = fabsf(L_5);
		// float currentVelocity = Mathf.Max(characterSpeed, CharacterSpeed.x);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_7 = __this->get_address_of_CharacterSpeed_8();
		float L_8 = L_7->get_x_0();
		float L_9;
		L_9 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_6, L_8, /*hidden argument*/NULL);
		// float targetZoom = MMMaths.Remap(currentVelocity, CharacterSpeed.x, CharacterSpeed.y, OrthographicZoom.x, OrthographicZoom.y);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_10 = __this->get_address_of_CharacterSpeed_8();
		float L_11 = L_10->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_12 = __this->get_address_of_CharacterSpeed_8();
		float L_13 = L_12->get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_14 = __this->get_address_of_OrthographicZoom_12();
		float L_15 = L_14->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_16 = __this->get_address_of_OrthographicZoom_12();
		float L_17 = L_16->get_y_1();
		float L_18;
		L_18 = MMMaths_Remap_mBEAF91CCEABC9ACD132FC4FF072A8A463B2F6C60(L_9, L_11, L_13, L_15, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		// _currentZoom = Mathf.Lerp(_currentZoom, targetZoom, Time.deltaTime * OrthographicZoomSpeed);
		float L_19 = __this->get__currentZoom_26();
		float L_20 = V_0;
		float L_21;
		L_21 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_22 = __this->get_OrthographicZoomSpeed_14();
		float L_23;
		L_23 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_19, L_20, ((float)il2cpp_codegen_multiply((float)L_21, (float)L_22)), /*hidden argument*/NULL);
		__this->set__currentZoom_26(L_23);
		// _virtualCamera.m_Lens.OrthographicSize = _currentZoom;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_24 = __this->get__virtualCamera_23();
		LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * L_25 = L_24->get_address_of_m_Lens_22();
		float L_26 = __this->get__currentZoom_26();
		L_25->set_OrthographicSize_2(L_26);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::PerformPerspectiveZoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_PerformPerspectiveZoom_mD2B8CA4633593E0A8891B288CB18173402F145A2 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// if (!UsePerspectiveZoom || (TargetController == null))
		bool L_0 = __this->get_UsePerspectiveZoom_15();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_1 = __this->get_TargetController_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		// return;
		return;
	}

IL_0017:
	{
		// float characterSpeed = Mathf.Abs(TargetController.Speed.x);
		CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * L_3 = __this->get_TargetController_10();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_x_0();
		float L_6;
		L_6 = fabsf(L_5);
		// float currentVelocity = Mathf.Max(characterSpeed, CharacterSpeed.x);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_7 = __this->get_address_of_CharacterSpeed_8();
		float L_8 = L_7->get_x_0();
		float L_9;
		L_9 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_6, L_8, /*hidden argument*/NULL);
		// float targetZoom = MMMaths.Remap(currentVelocity, CharacterSpeed.x, CharacterSpeed.y, PerspectiveZoom.x, PerspectiveZoom.y);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_10 = __this->get_address_of_CharacterSpeed_8();
		float L_11 = L_10->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_12 = __this->get_address_of_CharacterSpeed_8();
		float L_13 = L_12->get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_14 = __this->get_address_of_PerspectiveZoom_17();
		float L_15 = L_14->get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_16 = __this->get_address_of_PerspectiveZoom_17();
		float L_17 = L_16->get_y_1();
		float L_18;
		L_18 = MMMaths_Remap_mBEAF91CCEABC9ACD132FC4FF072A8A463B2F6C60(L_9, L_11, L_13, L_15, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		// _currentZoom = Mathf.Lerp(_currentZoom, targetZoom, Time.deltaTime * PerspectiveZoomSpeed);
		float L_19 = __this->get__currentZoom_26();
		float L_20 = V_0;
		float L_21;
		L_21 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_22 = __this->get_PerspectiveZoomSpeed_19();
		float L_23;
		L_23 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_19, L_20, ((float)il2cpp_codegen_multiply((float)L_21, (float)L_22)), /*hidden argument*/NULL);
		__this->set__currentZoom_26(L_23);
		// SetPerspectiveZoom(_currentZoom);
		float L_24 = __this->get__currentZoom_26();
		VirtActionInvoker1< float >::Invoke(17 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetPerspectiveZoom(System.Single) */, __this, L_24);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetPerspectiveZoom(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_SetPerspectiveZoom_m0F36A1D13BC57D7B7188CB1BA91B0D653B084E87 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, float ___newZoom0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (PerspectiveZoomMethod)
		int32_t L_0 = __this->get_PerspectiveZoomMethod_16();
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0021;
		}
	}
	{
		return;
	}

IL_000f:
	{
		// _virtualCamera.m_Lens.FieldOfView = newZoom;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_3 = __this->get__virtualCamera_23();
		LensSettings_tDBD2B66E4841DEDB5E221053F9B74AE2625014CA * L_4 = L_3->get_address_of_m_Lens_22();
		float L_5 = ___newZoom0;
		L_4->set_FieldOfView_1(L_5);
		// break;
		return;
	}

IL_0021:
	{
		// if (_framingTransposer != null)
		CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * L_6 = __this->get__framingTransposer_25();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		// _framingTransposer.m_CameraDistance = newZoom;
		CinemachineFramingTransposer_t2CDD350721EC85D1DE05E23615BBA0DCC9FD37B1 * L_8 = __this->get__framingTransposer_25();
		float L_9 = ___newZoom0;
		L_8->set_m_CameraDistance_22(L_9);
	}

IL_003b:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::OnMMEvent(MoreMountains.CorgiEngine.MMCameraEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_OnMMEvent_mCA41FD96DC3ACC6415969678A48EEF3074953891 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  ___cameraEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// switch (cameraEvent.EventType)
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_0 = ___cameraEvent0;
		int32_t L_1 = L_0.get_EventType_0();
		V_0 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_002f;
			}
			case 2:
			{
				goto IL_007c;
			}
			case 3:
			{
				goto IL_00a5;
			}
			case 4:
			{
				goto IL_00ce;
			}
		}
	}
	{
		return;
	}

IL_0022:
	{
		// SetTarget(cameraEvent.TargetCharacter);
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_3 = ___cameraEvent0;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_4 = L_3.get_TargetCharacter_1();
		VirtActionInvoker1< Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetTarget(MoreMountains.CorgiEngine.Character) */, __this, L_4);
		// break;
		return;
	}

IL_002f:
	{
		// if ((_confiner != null) && (ConfineCameraToLevelBounds))
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_5 = __this->get__confiner_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00da;
		}
	}
	{
		bool L_7 = __this->get_ConfineCameraToLevelBounds_6();
		if (!L_7)
		{
			goto IL_00da;
		}
	}
	{
		// if (_confiner.m_ConfineMode == CinemachineConfiner.Mode.Confine2D)
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_8 = __this->get__confiner_24();
		int32_t L_9 = L_8->get_m_ConfineMode_7();
		if (L_9)
		{
			goto IL_006a;
		}
	}
	{
		// _confiner.m_BoundingShape2D = cameraEvent.Bounds2D;
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_10 = __this->get__confiner_24();
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_11 = ___cameraEvent0;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_12 = L_11.get_Bounds2D_3();
		L_10->set_m_BoundingShape2D_9(L_12);
		// }
		return;
	}

IL_006a:
	{
		// _confiner.m_BoundingVolume = cameraEvent.Bounds;
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_13 = __this->get__confiner_24();
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_14 = ___cameraEvent0;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_15 = L_14.get_Bounds_2();
		L_13->set_m_BoundingVolume_8(L_15);
		// break;
		return;
	}

IL_007c:
	{
		// if (cameraEvent.TargetCharacter != null)
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_16 = ___cameraEvent0;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_17 = L_16.get_TargetCharacter_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_18;
		L_18 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_17, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_009e;
		}
	}
	{
		// if (cameraEvent.TargetCharacter != TargetCharacter)
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_19 = ___cameraEvent0;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_20 = L_19.get_TargetCharacter_1();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_21 = __this->get_TargetCharacter_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_22;
		L_22 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_009e;
		}
	}
	{
		// return;
		return;
	}

IL_009e:
	{
		// StartFollowing();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StartFollowing() */, __this);
		// break;
		return;
	}

IL_00a5:
	{
		// if (cameraEvent.TargetCharacter != null)
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_23 = ___cameraEvent0;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_24 = L_23.get_TargetCharacter_1();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_24, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00c7;
		}
	}
	{
		// if (cameraEvent.TargetCharacter != TargetCharacter)
		MMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3  L_26 = ___cameraEvent0;
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_27 = L_26.get_TargetCharacter_1();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_28 = __this->get_TargetCharacter_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_29;
		L_29 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_27, L_28, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_00c7;
		}
	}
	{
		// return;
		return;
	}

IL_00c7:
	{
		// StopFollowing();
		VirtActionInvoker0::Invoke(12 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StopFollowing() */, __this);
		// break;
		return;
	}

IL_00ce:
	{
		// _virtualCamera.Priority = 0;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_30 = __this->get__virtualCamera_23();
		CinemachineVirtualCameraBase_set_Priority_m6C180B742F19E669D648B6D1BE4D9D9C5824962B_inline(L_30, 0, /*hidden argument*/NULL);
	}

IL_00da:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::TeleportCameraToTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_TeleportCameraToTarget_mEB0F810436B357D26CB93BC125216A514D41B376 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// this.transform.position = TargetCharacter.transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_1 = __this->get_TargetCharacter_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_1, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetPriority(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_SetPriority_mE2BAB08A0B2C55EF62BECEE812A5DD03F3C36663 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, int32_t ___priority0, const RuntimeMethod* method)
{
	{
		// _virtualCamera.Priority = priority;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_0 = __this->get__virtualCamera_23();
		int32_t L_1 = ___priority0;
		CinemachineVirtualCameraBase_set_Priority_m6C180B742F19E669D648B6D1BE4D9D9C5824962B_inline(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_OnMMEvent_m13E3DAAE8805DE1BFD952019073966AC51EC412E (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  ___corgiEngineEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (corgiEngineEvent.EventType == CorgiEngineEventTypes.Respawn)
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_0 = ___corgiEngineEvent0;
		int32_t L_1 = L_0.get_EventType_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)7))))
		{
			goto IL_0017;
		}
	}
	{
		// if (InstantRepositionCameraOnRespawn)
		bool L_2 = __this->get_InstantRepositionCameraOnRespawn_20();
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		// TeleportCameraToTarget();
		VirtActionInvoker0::Invoke(19 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::TeleportCameraToTarget() */, __this);
	}

IL_0017:
	{
		// if (corgiEngineEvent.EventType == CorgiEngineEventTypes.CharacterSwitch)
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_3 = ___corgiEngineEvent0;
		int32_t L_4 = L_3.get_EventType_0();
		if ((!(((uint32_t)L_4) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_003d;
		}
	}
	{
		// SetTarget(LevelManager.Instance.Players[0]);
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_5;
		L_5 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * L_6;
		L_6 = LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline(L_5, /*hidden argument*/NULL);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_7;
		L_7 = List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_inline(L_6, 0, /*hidden argument*/List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		VirtActionInvoker1< Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetTarget(MoreMountains.CorgiEngine.Character) */, __this, L_7);
		// StartFollowing();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StartFollowing() */, __this);
	}

IL_003d:
	{
		// if (corgiEngineEvent.EventType == CorgiEngineEventTypes.CharacterSwap)
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_8 = ___corgiEngineEvent0;
		int32_t L_9 = L_8.get_EventType_0();
		if ((!(((uint32_t)L_9) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_0063;
		}
	}
	{
		// SetTarget(LevelManager.Instance.Players[0]);
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_10;
		L_10 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * L_11;
		L_11 = LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline(L_10, /*hidden argument*/NULL);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_12;
		L_12 = List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_inline(L_11, 0, /*hidden argument*/List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		VirtActionInvoker1< Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * >::Invoke(10 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::SetTarget(MoreMountains.CorgiEngine.Character) */, __this, L_12);
		// StartFollowing();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StartFollowing() */, __this);
	}

IL_0063:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_OnEnable_mF1709444C4995E41C9A1C4F0A6C76FD4D567DA75 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStartListening<MMCameraEvent>();
		EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mC193493C05E43E9AC505A0939656A93B192D1876_RuntimeMethod_var);
		// this.MMEventStartListening<CorgiEngineEvent>();
		EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController_OnDisable_m3A0F69E703D0F528FAACE1EBCFD162E62D0094E7 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStopListening<MMCameraEvent>();
		EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisMMCameraEvent_tD993E0072CBB833F87886BA607D4F75C5077AFF3_mE67380663D36EB348845BAD145A9738F125FB525_RuntimeMethod_var);
		// this.MMEventStopListening<CorgiEngineEvent>();
		EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CinemachineCameraController::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CinemachineCameraController__ctor_mD27353C114B937E2271772C6F2570FE01E89FD94 (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, const RuntimeMethod* method)
{
	{
		// public bool FollowsAPlayer = true;
		__this->set_FollowsAPlayer_5((bool)1);
		// public bool ConfineCameraToLevelBounds = true;
		__this->set_ConfineCameraToLevelBounds_6((bool)1);
		// public float ManualUpDownLookDistance = 3;
		__this->set_ManualUpDownLookDistance_7((3.0f));
		// public Vector2 CharacterSpeed = new Vector2(0f, 16f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_0), (0.0f), (16.0f), /*hidden argument*/NULL);
		__this->set_CharacterSpeed_8(L_0);
		// public Vector2 OrthographicZoom = new Vector2(5f, 9f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_1), (5.0f), (9.0f), /*hidden argument*/NULL);
		__this->set_OrthographicZoom_12(L_1);
		// public float InitialOrthographicZoom = 5f;
		__this->set_InitialOrthographicZoom_13((5.0f));
		// public float OrthographicZoomSpeed = 0.4f;
		__this->set_OrthographicZoomSpeed_14((0.400000006f));
		// public PerspectiveZoomMethods PerspectiveZoomMethod = PerspectiveZoomMethods.FramingTransposerDistance;
		__this->set_PerspectiveZoomMethod_16(1);
		// public Vector2 PerspectiveZoom = new Vector2(10f, 15f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), (10.0f), (15.0f), /*hidden argument*/NULL);
		__this->set_PerspectiveZoom_17(L_2);
		// public float InitialPerspectiveZoom = 5f;
		__this->set_InitialPerspectiveZoom_18((5.0f));
		// public float PerspectiveZoomSpeed = 0.4f;
		__this->set_PerspectiveZoomSpeed_19((0.400000006f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.CorgiCinemachineZone::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiCinemachineZone_Awake_m55FB68E8DB32173B956E5EA3D8DF1422E3B8EB34 (CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_MMGetComponentAroundOrAdd_TisCinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_m0B72B1326D70ABA9819B735C42FBB0F6F44E68DF_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Awake();
		MMCinemachineZone_Awake_m9B042DE73AD8085DB8528B7158BC183568BDA5CE(__this, /*hidden argument*/NULL);
		// if (Application.isPlaying)
		bool L_0;
		L_0 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_002f;
		}
	}
	{
		// _cinemachineCameraController = VirtualCamera.gameObject.MMGetComponentAroundOrAdd<CinemachineCameraController>();
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_1 = ((MMCinemachineZone_t7C8CF8307E3A2A86E53E93F50753A2340DC69CB5 *)__this)->get_VirtualCamera_4();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * L_3;
		L_3 = GameObjectExtensions_MMGetComponentAroundOrAdd_TisCinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_m0B72B1326D70ABA9819B735C42FBB0F6F44E68DF(L_2, /*hidden argument*/GameObjectExtensions_MMGetComponentAroundOrAdd_TisCinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_m0B72B1326D70ABA9819B735C42FBB0F6F44E68DF_RuntimeMethod_var);
		__this->set__cinemachineCameraController_26(L_3);
		// _cinemachineCameraController.ConfineCameraToLevelBounds = false;
		CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * L_4 = __this->get__cinemachineCameraController_26();
		L_4->set_ConfineCameraToLevelBounds_6((bool)0);
	}

IL_002f:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiCinemachineZone::EnableCamera(System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiCinemachineZone_EnableCamera_m3F8A8F2E8050881F79E584578098B52A0CC87F22 (CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * __this, bool ___state0, int32_t ___frames1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * L_0 = (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F *)il2cpp_codegen_object_new(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_il2cpp_TypeInfo_var);
		U3CEnableCameraU3Ed__2__ctor_m1D9DDDCC28E121A6A0C27F904A2A8F29F7E70CCD(L_0, 0, /*hidden argument*/NULL);
		U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * L_2 = L_1;
		bool L_3 = ___state0;
		L_2->set_state_3(L_3);
		U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * L_4 = L_2;
		int32_t L_5 = ___frames1;
		L_4->set_frames_4(L_5);
		return L_4;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiCinemachineZone::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiCinemachineZone__ctor_mD0E6208A13E5492A258F5405561D60B099A2A6AB (CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * __this, const RuntimeMethod* method)
{
	{
		MMCinemachineZone2D__ctor_mE6617D271D06E0855AADE639A48BEBCD50626542(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiCinemachineZone::<>n__0(System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiCinemachineZone_U3CU3En__0_m2B0BD1A14B71AD52EE8B2105DE29729579DE90A9 (CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * __this, bool ___state0, int32_t ___frames1, const RuntimeMethod* method)
{
	{
		bool L_0 = ___state0;
		int32_t L_1 = ___frames1;
		RuntimeObject* L_2;
		L_2 = MMCinemachineZone_EnableCamera_m00AEFAEF3455A2462F4433F0F6E1863555400D52(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.MMCinemachineBrainEvent::.ctor(MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCinemachineBrainEvent__ctor_m84DE9FBB871D80C3824E0AD21BF19B7B4446FE18 (MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF * __this, int32_t ___eventType0, float ___duration1, const RuntimeMethod* method)
{
	{
		// EventType = eventType;
		int32_t L_0 = ___eventType0;
		__this->set_EventType_0(L_0);
		// Duration = duration;
		float L_1 = ___duration1;
		__this->set_Duration_1(L_1);
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void MMCinemachineBrainEvent__ctor_m84DE9FBB871D80C3824E0AD21BF19B7B4446FE18_AdjustorThunk (RuntimeObject * __this, int32_t ___eventType0, float ___duration1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF * _thisAdjusted = reinterpret_cast<MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF *>(__this + _offset);
	MMCinemachineBrainEvent__ctor_m84DE9FBB871D80C3824E0AD21BF19B7B4446FE18(_thisAdjusted, ___eventType0, ___duration1, method);
}
// System.Void MoreMountains.CorgiEngine.MMCinemachineBrainEvent::Trigger(MoreMountains.CorgiEngine.MMCinemachineBrainEventTypes,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMCinemachineBrainEvent_Trigger_m3393F1966039CFA331A3171293F8E3726A432F4E (int32_t ___eventType0, float ___duration1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMEventManager_tB7745D6C891FB1E2397DE32D8209E3E2B5BC54A4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// e.EventType = eventType;
		int32_t L_0 = ___eventType0;
		(((MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_StaticFields*)il2cpp_codegen_static_fields_for(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_il2cpp_TypeInfo_var))->get_address_of_e_2())->set_EventType_0(L_0);
		// e.Duration = duration;
		float L_1 = ___duration1;
		(((MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_StaticFields*)il2cpp_codegen_static_fields_for(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_il2cpp_TypeInfo_var))->get_address_of_e_2())->set_Duration_1(L_1);
		// MMEventManager.TriggerEvent(e);
		MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF  L_2 = ((MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_StaticFields*)il2cpp_codegen_static_fields_for(MMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_il2cpp_TypeInfo_var))->get_e_2();
		IL2CPP_RUNTIME_CLASS_INIT(MMEventManager_tB7745D6C891FB1E2397DE32D8209E3E2B5BC54A4_il2cpp_TypeInfo_var);
		MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0(L_2, /*hidden argument*/MMEventManager_TriggerEvent_TisMMCinemachineBrainEvent_tCF64BC002C8FBFFA53723C7784FF149126E823CF_mF037B0000CF69D9037A0A359A34DD8B347E2E1B0_RuntimeMethod_var);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Collider2D MoreMountains.CorgiEngine.Room::get_RoomCollider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * Room_get_RoomCollider_mE452329CF0C1B9ACBB4CC515C9DD4ADF511ACC6F (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	{
		// public Collider2D RoomCollider { get { return _roomCollider2D; } }
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = __this->get__roomCollider2D_15();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_Start_m46B4EA1A6D2ED9E9457CB308A145DF4352D25600 (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	{
		// Initialization();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.Room::Initialization() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_Initialization_mC05A31B45240A5EEEA574BBC8F011DEB2D92DEAD (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// if (_initialized)
		bool L_0 = __this->get__initialized_18();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// _roomCollider2D = this.gameObject.GetComponent<Collider2D>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_2;
		L_2 = GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E(L_1, /*hidden argument*/GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E_RuntimeMethod_var);
		__this->set__roomCollider2D_15(L_2);
		// _mainCamera = Camera.main;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_3;
		L_3 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		__this->set__mainCamera_16(L_3);
		// StartCoroutine(ResizeConfiner());
		RuntimeObject* L_4;
		L_4 = VirtFuncInvoker0< RuntimeObject* >::Invoke(7 /* System.Collections.IEnumerator MoreMountains.CorgiEngine.Room::ResizeConfiner() */, __this);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_5;
		L_5 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_4, /*hidden argument*/NULL);
		// _initialized = true;
		__this->set__initialized_18((bool)1);
		// if (VirtualCamera != null)
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_6 = __this->get_VirtualCamera_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0053;
		}
	}
	{
		// VirtualCamera.enabled = false;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_8 = __this->get_VirtualCamera_4();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_8, (bool)0, /*hidden argument*/NULL);
	}

IL_0053:
	{
		// foreach (GameObject go in ActivationList)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_9 = __this->get_ActivationList_14();
		Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  L_10;
		L_10 = List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6(L_9, /*hidden argument*/List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		V_0 = L_10;
	}

IL_005f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0061:
		{
			// foreach (GameObject go in ActivationList)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_11;
			L_11 = Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
			// go.SetActive(false);
			GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_11, (bool)0, /*hidden argument*/NULL);
		}

IL_006e:
		{
			// foreach (GameObject go in ActivationList)
			bool L_12;
			L_12 = Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
			if (L_12)
			{
				goto IL_0061;
			}
		}

IL_0077:
		{
			IL2CPP_LEAVE(0x87, FINALLY_0079);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0079;
	}

FINALLY_0079:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(121)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(121)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x87, IL_0087)
	}

IL_0087:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator MoreMountains.CorgiEngine.Room::ResizeConfiner()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Room_ResizeConfiner_mD64E0BE92ACDD5A2FE6F058A857016D3D5102D2F (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * L_0 = (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 *)il2cpp_codegen_object_new(U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_il2cpp_TypeInfo_var);
		U3CResizeConfinerU3Ed__19__ctor_mFD9E4619E6D3E4ABD7AE146801D22E1C2F5F2A59(L_0, 0, /*hidden argument*/NULL);
		U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		return L_1;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::HandleLevelStartDetection()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_HandleLevelStartDetection_m42CDED3F38E4321FCC828218D35D0C4759CA58DE (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_HasInstance_mD391A08D92462F740297996852235491E938BCDD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!_initialized)
		bool L_0 = __this->get__initialized_18();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Initialization();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.Room::Initialization() */, __this);
	}

IL_000e:
	{
		// if (AutoDetectFirstRoomOnStart)
		bool L_1 = __this->get_AutoDetectFirstRoomOnStart_8();
		if (!L_1)
		{
			goto IL_00b5;
		}
	}
	{
		// if (LevelManager.HasInstance)
		bool L_2;
		L_2 = MMSingleton_1_get_HasInstance_mD391A08D92462F740297996852235491E938BCDD(/*hidden argument*/MMSingleton_1_get_HasInstance_mD391A08D92462F740297996852235491E938BCDD_RuntimeMethod_var);
		if (!L_2)
		{
			goto IL_00b5;
		}
	}
	{
		// if (_roomCollider2D.bounds.Contains(LevelManager.Instance.Players[0].transform.position))
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_3 = __this->get__roomCollider2D_15();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_4;
		L_4 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_5;
		L_5 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * L_6;
		L_6 = LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline(L_5, /*hidden argument*/NULL);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_7;
		L_7 = List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_inline(L_6, 0, /*hidden argument*/List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_8, /*hidden argument*/NULL);
		bool L_10;
		L_10 = Bounds_Contains_m24154F7F564711846389EF6AEDABB4092A72FFA1((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00b5;
		}
	}
	{
		// MMCameraEvent.Trigger(MMCameraEventTypes.ResetPriorities);
		MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8(4, (Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 *)NULL, (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 *)NULL, (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 *)NULL, /*hidden argument*/NULL);
		// MMCinemachineBrainEvent.Trigger(MMCinemachineBrainEventTypes.ChangeBlendDuration, 0f);
		MMCinemachineBrainEvent_Trigger_m3393F1966039CFA331A3171293F8E3726A432F4E(0, (0.0f), /*hidden argument*/NULL);
		// VirtualCamera.Priority = 10;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_11 = __this->get_VirtualCamera_4();
		CinemachineVirtualCameraBase_set_Priority_m6C180B742F19E669D648B6D1BE4D9D9C5824962B_inline(L_11, ((int32_t)10), /*hidden argument*/NULL);
		// MMSpriteMaskEvent.Trigger(MMSpriteMaskEvent.MMSpriteMaskEventTypes.MoveToNewPosition,
		//     (Vector2)_roomCollider2D.bounds.center,
		//     _roomCollider2D.bounds.size,
		//     0f, MMTween.MMTweenCurve.LinearTween);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_12 = __this->get__roomCollider2D_15();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_13;
		L_13 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_15;
		L_15 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_14, /*hidden argument*/NULL);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_16 = __this->get__roomCollider2D_15();
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_17;
		L_17 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_19;
		L_19 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_18, /*hidden argument*/NULL);
		MMSpriteMaskEvent_Trigger_m94383E73C7E743D4ED07BFBB86ABE48F62A9833D(0, L_15, L_19, (0.0f), 0, /*hidden argument*/NULL);
		// PlayerEntersRoom();
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.CorgiEngine.Room::PlayerEntersRoom() */, __this);
	}

IL_00b5:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::PlayerEntersRoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_PlayerEntersRoom_m44A46339668ADB3034DD16DEAF475F5D74515B98 (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B5_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B4_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B8_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B7_0 = NULL;
	{
		// CurrentRoom = true;
		__this->set_CurrentRoom_9((bool)1);
		// if (VirtualCamera != null)
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_0 = __this->get_VirtualCamera_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		// VirtualCamera.enabled = true;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_2 = __this->get_VirtualCamera_4();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_0021:
	{
		// if (RoomVisited)
		bool L_3 = __this->get_RoomVisited_10();
		if (!L_3)
		{
			goto IL_003c;
		}
	}
	{
		// OnPlayerEntersRoom?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = __this->get_OnPlayerEntersRoom_12();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_5 = L_4;
		G_B4_0 = L_5;
		if (L_5)
		{
			G_B5_0 = L_5;
			goto IL_0035;
		}
	}
	{
		goto IL_0054;
	}

IL_0035:
	{
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B5_0, /*hidden argument*/NULL);
		// }
		goto IL_0054;
	}

IL_003c:
	{
		// RoomVisited = true;
		__this->set_RoomVisited_10((bool)1);
		// OnPlayerEntersRoomForTheFirstTime?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_6 = __this->get_OnPlayerEntersRoomForTheFirstTime_11();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_7 = L_6;
		G_B7_0 = L_7;
		if (L_7)
		{
			G_B8_0 = L_7;
			goto IL_004f;
		}
	}
	{
		goto IL_0054;
	}

IL_004f:
	{
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B8_0, /*hidden argument*/NULL);
	}

IL_0054:
	{
		// foreach(GameObject go in ActivationList)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_8 = __this->get_ActivationList_14();
		Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  L_9;
		L_9 = List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6(L_8, /*hidden argument*/List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		V_0 = L_9;
	}

IL_0060:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006f;
		}

IL_0062:
		{
			// foreach(GameObject go in ActivationList)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10;
			L_10 = Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
			// go.SetActive(true);
			GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_10, (bool)1, /*hidden argument*/NULL);
		}

IL_006f:
		{
			// foreach(GameObject go in ActivationList)
			bool L_11;
			L_11 = Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
			if (L_11)
			{
				goto IL_0062;
			}
		}

IL_0078:
		{
			IL2CPP_LEAVE(0x88, FINALLY_007a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007a;
	}

FINALLY_007a:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(122)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(122)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x88, IL_0088)
	}

IL_0088:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::PlayerExitsRoom()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_PlayerExitsRoom_m0343E42A15E2E108F5677C6A71BBF6A3EC6C361F (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B4_0 = NULL;
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * G_B3_0 = NULL;
	{
		// if (VirtualCamera != null)
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_0 = __this->get_VirtualCamera_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		// VirtualCamera.enabled = false;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_2 = __this->get_VirtualCamera_4();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// CurrentRoom = false;
		__this->set_CurrentRoom_9((bool)0);
		// OnPlayerExitsRoom?.Invoke();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_3 = __this->get_OnPlayerExitsRoom_13();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = L_3;
		G_B3_0 = L_4;
		if (L_4)
		{
			G_B4_0 = L_4;
			goto IL_002d;
		}
	}
	{
		goto IL_0032;
	}

IL_002d:
	{
		UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5(G_B4_0, /*hidden argument*/NULL);
	}

IL_0032:
	{
		// foreach (GameObject go in ActivationList)
		List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * L_5 = __this->get_ActivationList_14();
		Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14  L_6;
		L_6 = List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6(L_5, /*hidden argument*/List_1_GetEnumerator_m3616D04A85546C8251A6C376656CDB5358D893F6_RuntimeMethod_var);
		V_0 = L_6;
	}

IL_003e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0040:
		{
			// foreach (GameObject go in ActivationList)
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
			L_7 = Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_inline((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_get_Current_mB38DBEFCD264B4682A190F8592464C0658F702B7_RuntimeMethod_var);
			// go.SetActive(false);
			GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_7, (bool)0, /*hidden argument*/NULL);
		}

IL_004d:
		{
			// foreach (GameObject go in ActivationList)
			bool L_8;
			L_8 = Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mF39107B3A55F66C83EBCA798CBC93AC4C990DBD7_RuntimeMethod_var);
			if (L_8)
			{
				goto IL_0040;
			}
		}

IL_0056:
		{
			IL2CPP_LEAVE(0x66, FINALLY_0058);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0058;
	}

FINALLY_0058:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9((Enumerator_tFF7242F2EA0307D809676E9B45A3AF1F8BB52A14 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m4B68F0A4E0441A036D7E39BC7E639536164196D9_RuntimeMethod_var);
		IL2CPP_END_FINALLY(88)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(88)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x66, IL_0066)
	}

IL_0066:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_OnMMEvent_m40AC339CC09A004EAA154C3202045B3024D6401C (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  ___corgiEngineEvent0, const RuntimeMethod* method)
{
	{
		// if ((corgiEngineEvent.EventType == CorgiEngineEventTypes.Respawn)
		//     || (corgiEngineEvent.EventType == CorgiEngineEventTypes.LevelStart))
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_0 = ___corgiEngineEvent0;
		int32_t L_1 = L_0.get_EventType_0();
		if ((((int32_t)L_1) == ((int32_t)7)))
		{
			goto IL_0012;
		}
	}
	{
		CorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B  L_2 = ___corgiEngineEvent0;
		int32_t L_3 = L_2.get_EventType_0();
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_001e;
		}
	}

IL_0012:
	{
		// PlayerExitsRoom();
		VirtActionInvoker0::Invoke(10 /* System.Void MoreMountains.CorgiEngine.Room::PlayerExitsRoom() */, __this);
		// HandleLevelStartDetection();
		VirtActionInvoker0::Invoke(8 /* System.Void MoreMountains.CorgiEngine.Room::HandleLevelStartDetection() */, __this);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_OnEnable_m7BB72D5D749B06F95B50823CE6A5462458393DB6 (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStartListening<CorgiEngineEvent>();
		EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_m04E9C45DE862DD7D7EBD163A5A04078592E53306_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room_OnDisable_mE34742B767148FD9B9523F0F4DE5510C1A878969 (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStopListening<CorgiEngineEvent>();
		EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisCorgiEngineEvent_t5EC009C4C928E540098373D6B471391352C7AB7B_mC8F665AC85522C2AB0A0D8B5CCB0972A8C51AA17_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Room__ctor_m2DAC7D52C34D412D24F99B97EB53AF7B3B986180 (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	{
		// public bool ResizeConfinerAutomatically = true;
		__this->set_ResizeConfinerAutomatically_7((bool)1);
		// public bool AutoDetectFirstRoomOnStart = true;
		__this->set_AutoDetectFirstRoomOnStart_8((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.Teleporter::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_Awake_m4157D8AB603943C626E6E283BB81D63C895FE51B (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, const RuntimeMethod* method)
{
	{
		// InitializeTeleporter();
		VirtActionInvoker0::Invoke(27 /* System.Void MoreMountains.CorgiEngine.Teleporter::InitializeTeleporter() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::InitializeTeleporter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_InitializeTeleporter_m2FF7237639D25637D5F6064393F1C12EDCDF50C6 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponentInParent_TisRoom_t392D52D203A7F630C190D47A07148A57F4020B42_m66E446560DA3F59C8F60C6F850D6C5DE7D7C6610_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _ignoreList = new List<Transform>();
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = (List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 *)il2cpp_codegen_object_new(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0_il2cpp_TypeInfo_var);
		List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F(L_0, /*hidden argument*/List_1__ctor_mF1D464BA700E6389AEA8AF2F197270F387D9A41F_RuntimeMethod_var);
		__this->set__ignoreList_81(L_0);
		// if (CurrentRoom == null)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_1 = __this->get_CurrentRoom_59();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002a;
		}
	}
	{
		// CurrentRoom = this.gameObject.GetComponentInParent<Room>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3;
		L_3 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_4;
		L_4 = GameObject_GetComponentInParent_TisRoom_t392D52D203A7F630C190D47A07148A57F4020B42_m66E446560DA3F59C8F60C6F850D6C5DE7D7C6610(L_3, /*hidden argument*/GameObject_GetComponentInParent_TisRoom_t392D52D203A7F630C190D47A07148A57F4020B42_m66E446560DA3F59C8F60C6F850D6C5DE7D7C6610_RuntimeMethod_var);
		__this->set_CurrentRoom_59(L_4);
	}

IL_002a:
	{
		// _initialDelayWaitForSeconds = new WaitForSecondsRealtime(InitialDelay);
		float L_5 = __this->get_InitialDelay_70();
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_6 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_6, L_5, /*hidden argument*/NULL);
		__this->set__initialDelayWaitForSeconds_82(L_6);
		// _fadeOutDurationWaitForSeconds = new WaitForSecondsRealtime(FadeOutDuration);
		float L_7 = __this->get_FadeOutDuration_71();
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_8 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_8, L_7, /*hidden argument*/NULL);
		__this->set__fadeOutDurationWaitForSeconds_83(L_8);
		// _halfDelayBetweenFadesWaitForSeconds = new WaitForSecondsRealtime(DelayBetweenFades/2f);
		float L_9 = __this->get_DelayBetweenFades_72();
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_10 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_10, ((float)((float)L_9/(float)(2.0f))), /*hidden argument*/NULL);
		__this->set__halfDelayBetweenFadesWaitForSeconds_84(L_10);
		// _fadeInDurationWaitForSeconds = new WaitForSecondsRealtime(FadeInDuration);
		float L_11 = __this->get_FadeInDuration_73();
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_12 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_12, L_11, /*hidden argument*/NULL);
		__this->set__fadeInDurationWaitForSeconds_85(L_12);
		// _finalDelayyWaitForSeconds = new WaitForSecondsRealtime(FinalDelay);
		float L_13 = __this->get_FinalDelay_74();
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_14 = (WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 *)il2cpp_codegen_object_new(WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40_il2cpp_TypeInfo_var);
		WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225(L_14, L_13, /*hidden argument*/NULL);
		__this->set__finalDelayyWaitForSeconds_86(L_14);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_OnTriggerEnter2D_m5B5ACFCE1459C386F505D28FA15440B7DD3EE56B (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_ignoreList.Contains(collider.transform))
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = __this->get__ignoreList_81();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_1 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_1, /*hidden argument*/NULL);
		bool L_3;
		L_3 = List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD(L_0, L_2, /*hidden argument*/List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0014;
		}
	}
	{
		// return;
		return;
	}

IL_0014:
	{
		// if (OnlyAffectsPlayer || !AutoActivation)
		bool L_4 = __this->get_OnlyAffectsPlayer_50();
		if (L_4)
		{
			goto IL_0024;
		}
	}
	{
		bool L_5 = ((ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 *)__this)->get_AutoActivation_9();
		if (L_5)
		{
			goto IL_002c;
		}
	}

IL_0024:
	{
		// base.OnTriggerEnter2D(collider);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_6 = ___collider0;
		ButtonActivated_OnTriggerEnter2D_m658128E15BA4BDCCF88ABA9970E0BFB9A6C305BB(__this, L_6, /*hidden argument*/NULL);
		// }
		return;
	}

IL_002c:
	{
		// Teleport(collider);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_7 = ___collider0;
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(28 /* System.Void MoreMountains.CorgiEngine.Teleporter::Teleport(UnityEngine.Collider2D) */, __this, L_7);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::OnTriggerStay2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_OnTriggerStay2D_mCAF78DFD32104172D44C8029254D6371351E1510 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_ignoreList.Contains(collider.transform))
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = __this->get__ignoreList_81();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_1 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_1, /*hidden argument*/NULL);
		bool L_3;
		L_3 = List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD(L_0, L_2, /*hidden argument*/List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0014;
		}
	}
	{
		// return;
		return;
	}

IL_0014:
	{
		// base.OnTriggerStay2D(collider);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_4 = ___collider0;
		ButtonActivated_OnTriggerStay2D_m9C2A689D96D707EC3B7AA81744CCF933F087BB13(__this, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::TriggerButtonAction(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_TriggerButtonAction_m662F2A01A8DA6234D052B3041F5E47541ED72265 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___instigator0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!CheckNumberOfUses())
		bool L_0;
		L_0 = VirtFuncInvoker0< bool >::Invoke(24 /* System.Boolean MoreMountains.CorgiEngine.ButtonActivated::CheckNumberOfUses() */, __this);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// Collider2D coll = instigator.GetComponent<Collider2D>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = ___instigator0;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_2;
		L_2 = GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E(L_1, /*hidden argument*/GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E_RuntimeMethod_var);
		// if (coll != null)
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		// base.TriggerButtonAction (instigator);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = ___instigator0;
		ButtonActivated_TriggerButtonAction_m90ABA5F03388FD3B7B2DC788838C3D638F02ED6F(__this, L_4, /*hidden argument*/NULL);
		// Teleport(instigator.GetComponent<Collider2D>());
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___instigator0;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_6;
		L_6 = GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E(L_5, /*hidden argument*/GameObject_GetComponent_TisCollider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_m770EEE9CD21A968F23ABBBEF9BF3897DA14D085E_RuntimeMethod_var);
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(28 /* System.Void MoreMountains.CorgiEngine.Teleporter::Teleport(UnityEngine.Collider2D) */, __this, L_6);
	}

IL_002a:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::Teleport(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_Teleport_m5F65F84664A43E069968483C1895223D2920EFFC (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _entryPosition = collider.transform.position;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		__this->set__entryPosition_87(L_2);
		// if (Destination != null)
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_3 = __this->get_Destination_56();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		// StartCoroutine(TeleportSequence(collider));
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_5 = ___collider0;
		RuntimeObject* L_6;
		L_6 = VirtFuncInvoker1< RuntimeObject*, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(29 /* System.Collections.IEnumerator MoreMountains.CorgiEngine.Teleporter::TeleportSequence(UnityEngine.Collider2D) */, __this, L_5);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_7;
		L_7 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_6, /*hidden argument*/NULL);
	}

IL_002d:
	{
		// }
		return;
	}
}
// System.Collections.IEnumerator MoreMountains.CorgiEngine.Teleporter::TeleportSequence(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Teleporter_TeleportSequence_mCB4AFE4F1F8114AD5DD94DAE787742ACC50148BB (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * L_0 = (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 *)il2cpp_codegen_object_new(U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_il2cpp_TypeInfo_var);
		U3CTeleportSequenceU3Ed__47__ctor_m8AB83EFFF24D4A556C827B654ED7B8915B8DF816(L_0, 0, /*hidden argument*/NULL);
		U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * L_2 = L_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_3 = ___collider0;
		L_2->set_collider_3(L_3);
		return L_2;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::SequenceStart(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_SequenceStart_m585E7A8E76385322C9D40EDE2E5FC0991427D90C (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * V_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B11_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B10_0 = NULL;
	{
		// if (AutoActivation)
		bool L_0 = ((ButtonActivated_t31F4EE2949E93B6789E56D26C3EC964A694472C5 *)__this)->get_AutoActivation_9();
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// ActivateZone();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.ButtonActivated::ActivateZone() */, __this);
	}

IL_000e:
	{
		// if (CameraMode == CameraModes.TeleportCamera)
		int32_t L_1 = __this->get_CameraMode_58();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0020;
		}
	}
	{
		// MMCameraEvent.Trigger(MMCameraEventTypes.StopFollowing);
		MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8(3, (Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 *)NULL, (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 *)NULL, (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 *)NULL, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// if (FreezeTime)
		bool L_2 = __this->get_FreezeTime_68();
		if (!L_2)
		{
			goto IL_003f;
		}
	}
	{
		// MMTimeScaleEvent.Trigger(MMTimeScaleMethods.For, 0f, 0f, false, 0f, true);
		MMTimeScaleEvent_Trigger_mF54D807E19DF44F9EB1E07B9A76C8E294632741E(0, (0.0f), (0.0f), (bool)0, (0.0f), (bool)1, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// Character player = collider.gameObject.MMGetComponentNoAlloc<Character>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_3 = ___collider0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_5;
		L_5 = GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73(L_4, /*hidden argument*/GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73_RuntimeMethod_var);
		V_0 = L_5;
		// if (FreezeCharacter && (player != null))
		bool L_6 = __this->get_FreezeCharacter_69();
		if (!L_6)
		{
			goto IL_0062;
		}
	}
	{
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_7, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0062;
		}
	}
	{
		// player.Freeze();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_9 = V_0;
		VirtActionInvoker0::Invoke(26 /* System.Void MoreMountains.CorgiEngine.Character::Freeze() */, L_9);
	}

IL_0062:
	{
		// SequenceStartFeedback?.PlayFeedbacks();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_10 = __this->get_SequenceStartFeedback_75();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_11 = L_10;
		G_B10_0 = L_11;
		if (L_11)
		{
			G_B11_0 = L_11;
			goto IL_006d;
		}
	}
	{
		return;
	}

IL_006d:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B11_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::AfterInitialDelay(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_AfterInitialDelay_m6AF0DBDEAB26209A8A6736AB9D4DB9E900CAB751 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B4_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B3_0 = NULL;
	{
		// if (TriggerFade)
		bool L_0 = __this->get_TriggerFade_61();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		// MMFadeInEvent.Trigger(FadeOutDuration, FadeTween, FaderID, false, LevelManager.Instance.Players[0].transform.position);
		float L_1 = __this->get_FadeOutDuration_71();
		MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * L_2 = __this->get_FadeTween_63();
		int32_t L_3 = __this->get_FaderID_62();
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_4;
		L_4 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * L_5;
		L_5 = LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline(L_4, /*hidden argument*/NULL);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_6;
		L_6 = List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_inline(L_5, 0, /*hidden argument*/List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		MMFadeInEvent_Trigger_m090C697AF081E64E75802956A665E2DA9FF5C666(L_1, L_2, L_3, (bool)0, L_8, /*hidden argument*/NULL);
	}

IL_003a:
	{
		// AfterInitialDelayFeedback?.PlayFeedbacks();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_9 = __this->get_AfterInitialDelayFeedback_76();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_10 = L_9;
		G_B3_0 = L_10;
		if (L_10)
		{
			G_B4_0 = L_10;
			goto IL_0045;
		}
	}
	{
		return;
	}

IL_0045:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B4_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::AfterFadeOut(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_AfterFadeOut_mD6EFED9AE20A072B82790CB3879AD76EC4AAEBEB (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  V_0;
	memset((&V_0), 0, sizeof(V_0));
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B10_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B9_0 = NULL;
	{
		// if (AddToDestinationIgnoreList)
		bool L_0 = __this->get_AddToDestinationIgnoreList_57();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		// Destination.AddToIgnoreList(collider.transform);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_1 = __this->get_Destination_56();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_2 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_2, /*hidden argument*/NULL);
		VirtActionInvoker1< Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * >::Invoke(39 /* System.Void MoreMountains.CorgiEngine.Teleporter::AddToIgnoreList(UnityEngine.Transform) */, L_1, L_3);
	}

IL_0019:
	{
		// if (CameraMode == CameraModes.CinemachinePriority)
		int32_t L_4 = __this->get_CameraMode_58();
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_0037;
		}
	}
	{
		// MMCameraEvent.Trigger(MMCameraEventTypes.ResetPriorities);
		MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8(4, (Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 *)NULL, (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 *)NULL, (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 *)NULL, /*hidden argument*/NULL);
		// MMCinemachineBrainEvent.Trigger(MMCinemachineBrainEventTypes.ChangeBlendDuration, DelayBetweenFades);
		float L_5 = __this->get_DelayBetweenFades_72();
		MMCinemachineBrainEvent_Trigger_m3393F1966039CFA331A3171293F8E3726A432F4E(0, L_5, /*hidden argument*/NULL);
	}

IL_0037:
	{
		// if (CurrentRoom != null)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_6 = __this->get_CurrentRoom_59();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_7;
		L_7 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_6, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		// CurrentRoom.PlayerExitsRoom();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_8 = __this->get_CurrentRoom_59();
		VirtActionInvoker0::Invoke(10 /* System.Void MoreMountains.CorgiEngine.Room::PlayerExitsRoom() */, L_8);
	}

IL_0050:
	{
		// if (TargetRoom != null)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_9 = __this->get_TargetRoom_60();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_9, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00cc;
		}
	}
	{
		// TargetRoom.PlayerEntersRoom();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_11 = __this->get_TargetRoom_60();
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.CorgiEngine.Room::PlayerEntersRoom() */, L_11);
		// TargetRoom.VirtualCamera.Priority = 10;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_12 = __this->get_TargetRoom_60();
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_13 = L_12->get_VirtualCamera_4();
		CinemachineVirtualCameraBase_set_Priority_m6C180B742F19E669D648B6D1BE4D9D9C5824962B_inline(L_13, ((int32_t)10), /*hidden argument*/NULL);
		// MMSpriteMaskEvent.Trigger(MoveMaskMethod, (Vector2)TargetRoom.RoomCollider.bounds.center, TargetRoom.RoomCollider.bounds.size, MoveMaskDuration, MoveMaskCurve);
		int32_t L_14 = __this->get_MoveMaskMethod_66();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_15 = __this->get_TargetRoom_60();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_16;
		L_16 = Room_get_RoomCollider_mE452329CF0C1B9ACBB4CC515C9DD4ADF511ACC6F_inline(L_15, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_17;
		L_17 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_19;
		L_19 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_18, /*hidden argument*/NULL);
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_20 = __this->get_TargetRoom_60();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_21;
		L_21 = Room_get_RoomCollider_mE452329CF0C1B9ACBB4CC515C9DD4ADF511ACC6F_inline(L_20, /*hidden argument*/NULL);
		Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37  L_22;
		L_22 = Collider2D_get_bounds_mAC9477EF790D42A796B09CD1E946129B3054ACA8(L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14((Bounds_t0F1F36D4F7AF49524B3C2A2259594412A3D3AE37 *)(&V_0), /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_24;
		L_24 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_23, /*hidden argument*/NULL);
		float L_25 = __this->get_MoveMaskDuration_67();
		int32_t L_26 = __this->get_MoveMaskCurve_65();
		MMSpriteMaskEvent_Trigger_m94383E73C7E743D4ED07BFBB86ABE48F62A9833D(L_14, L_19, L_24, L_25, L_26, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		// AfterFadeOutFeedback?.PlayFeedbacks();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_27 = __this->get_AfterFadeOutFeedback_77();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_28 = L_27;
		G_B9_0 = L_28;
		if (L_28)
		{
			G_B10_0 = L_28;
			goto IL_00d7;
		}
	}
	{
		return;
	}

IL_00d7:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B10_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::TeleportCollider(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_TeleportCollider_m677BB81867B658893F962014D25B71BD876B3669 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B6_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B5_0 = NULL;
	{
		// _newPosition = Destination.transform.position + Destination.ExitOffset;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_0 = __this->get_Destination_56();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_3 = __this->get_Destination_56();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = L_3->get_ExitOffset_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_2, L_4, /*hidden argument*/NULL);
		__this->set__newPosition_88(L_5);
		// if (MaintainXEntryPositionOnExit)
		bool L_6 = __this->get_MaintainXEntryPositionOnExit_54();
		if (!L_6)
		{
			goto IL_0044;
		}
	}
	{
		// _newPosition.x = _entryPosition.x;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_7 = __this->get_address_of__newPosition_88();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_8 = __this->get_address_of__entryPosition_87();
		float L_9 = L_8->get_x_2();
		L_7->set_x_2(L_9);
	}

IL_0044:
	{
		// if (MaintainYEntryPositionOnExit)
		bool L_10 = __this->get_MaintainYEntryPositionOnExit_55();
		if (!L_10)
		{
			goto IL_0062;
		}
	}
	{
		// _newPosition.y = _entryPosition.y;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_11 = __this->get_address_of__newPosition_88();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_12 = __this->get_address_of__entryPosition_87();
		float L_13 = L_12->get_y_3();
		L_11->set_y_3(L_13);
	}

IL_0062:
	{
		// _newPosition.z = collider.transform.position.z;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_14 = __this->get_address_of__newPosition_88();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_15 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_15, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_16, /*hidden argument*/NULL);
		float L_18 = L_17.get_z_4();
		L_14->set_z_4(L_18);
		// Destination.ArrivedHereFeedback?.PlayFeedbacks();
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_19 = __this->get_Destination_56();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_20 = L_19->get_ArrivedHereFeedback_80();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_21 = L_20;
		G_B5_0 = L_21;
		if (L_21)
		{
			G_B6_0 = L_21;
			goto IL_008e;
		}
	}
	{
		goto IL_0093;
	}

IL_008e:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B6_0);
	}

IL_0093:
	{
		// switch (TeleportationMode)
		int32_t L_22 = __this->get_TeleportationMode_52();
		V_0 = L_22;
		int32_t L_23 = V_0;
		if (!L_23)
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) == ((int32_t)1)))
		{
			goto IL_00c6;
		}
	}
	{
		return;
	}

IL_00a2:
	{
		// collider.transform.position = _newPosition;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_25 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26;
		L_26 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_25, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = __this->get__newPosition_88();
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_26, L_27, /*hidden argument*/NULL);
		// _ignoreList.Remove(collider.transform);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_28 = __this->get__ignoreList_81();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_29 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_30;
		L_30 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_29, /*hidden argument*/NULL);
		bool L_31;
		L_31 = List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A(L_28, L_30, /*hidden argument*/List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		// break;
		return;
	}

IL_00c6:
	{
		// StartCoroutine(TeleportTweenCo(collider, collider.transform.position, _newPosition));
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_32 = ___collider0;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_33 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34;
		L_34 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_33, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35;
		L_35 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_34, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_36 = __this->get__newPosition_88();
		RuntimeObject* L_37;
		L_37 = VirtFuncInvoker3< RuntimeObject*, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(34 /* System.Collections.IEnumerator MoreMountains.CorgiEngine.Teleporter::TeleportTweenCo(UnityEngine.Collider2D,UnityEngine.Vector3,UnityEngine.Vector3) */, __this, L_32, L_35, L_36);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_38;
		L_38 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_37, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator MoreMountains.CorgiEngine.Teleporter::TeleportTweenCo(UnityEngine.Collider2D,UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Teleporter_TeleportTweenCo_mD805D9A909935BC3E4D7EAA71B0B7D9FE1905F13 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___destination2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * L_0 = (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B *)il2cpp_codegen_object_new(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_il2cpp_TypeInfo_var);
		U3CTeleportTweenCoU3Ed__52__ctor_m1644936594C6AC11FF6F80E342A05642C083BF68(L_0, 0, /*hidden argument*/NULL);
		U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * L_1 = L_0;
		L_1->set_U3CU3E4__this_3(__this);
		U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * L_2 = L_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_3 = ___collider0;
		L_2->set_collider_2(L_3);
		U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * L_4 = L_2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = ___origin1;
		L_4->set_origin_4(L_5);
		U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * L_6 = L_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = ___destination2;
		L_6->set_destination_5(L_7);
		return L_6;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::BetweenFades(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_BetweenFades_m802E750E353293E6982C119C8C9C118F8F09F7CC (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	{
		// TeleportCollider(collider);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(33 /* System.Void MoreMountains.CorgiEngine.Teleporter::TeleportCollider(UnityEngine.Collider2D) */, __this, L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::AfterDelayBetweenFades(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_AfterDelayBetweenFades_m6869BDA61BB6F4B4388238BB344448C3D98400B0 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B6_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B5_0 = NULL;
	{
		// if (CameraMode == CameraModes.TeleportCamera)
		int32_t L_0 = __this->get_CameraMode_58();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0033;
		}
	}
	{
		// if (LevelManager.Instance.LevelCameraController != null)
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_1;
		L_1 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * L_2;
		L_2 = LevelManager_get_LevelCameraController_m81BFB44DD7452381F90CE3F622A7FF33A5953152_inline(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002a;
		}
	}
	{
		// LevelManager.Instance.LevelCameraController.TeleportCameraToTarget();
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_4;
		L_4 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * L_5;
		L_5 = LevelManager_get_LevelCameraController_m81BFB44DD7452381F90CE3F622A7FF33A5953152_inline(L_4, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(23 /* System.Void MoreMountains.CorgiEngine.CameraController::TeleportCameraToTarget() */, L_5);
	}

IL_002a:
	{
		// MMCameraEvent.Trigger(MMCameraEventTypes.StartFollowing);
		MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8(2, (Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 *)NULL, (Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 *)NULL, (Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 *)NULL, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// AfterDelayBetweenFadesFeedback?.PlayFeedbacks();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_6 = __this->get_AfterDelayBetweenFadesFeedback_78();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_7 = L_6;
		G_B5_0 = L_7;
		if (L_7)
		{
			G_B6_0 = L_7;
			goto IL_003f;
		}
	}
	{
		goto IL_0044;
	}

IL_003f:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B6_0);
	}

IL_0044:
	{
		// if (TriggerFade)
		bool L_8 = __this->get_TriggerFade_61();
		if (!L_8)
		{
			goto IL_007e;
		}
	}
	{
		// MMFadeOutEvent.Trigger(FadeInDuration, FadeTween, FaderID, false, LevelManager.Instance.Players[0].transform.position);
		float L_9 = __this->get_FadeInDuration_73();
		MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * L_10 = __this->get_FadeTween_63();
		int32_t L_11 = __this->get_FaderID_62();
		LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * L_12;
		L_12 = MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B(/*hidden argument*/MMSingleton_1_get_Instance_mD44B769F2E9F4C66B629A84780EC7F41F33F871B_RuntimeMethod_var);
		List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * L_13;
		L_13 = LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline(L_12, /*hidden argument*/NULL);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_14;
		L_14 = List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_inline(L_13, 0, /*hidden argument*/List_1_get_Item_m325810FB025DF79766FB66ABF3573FE50E1D73C9_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_14, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_15, /*hidden argument*/NULL);
		MMFadeOutEvent_Trigger_m0AECC19A24868A953A20E499EF61A676555E8EA5(L_9, L_10, L_11, (bool)0, L_16, /*hidden argument*/NULL);
	}

IL_007e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::AfterFadeIn(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_AfterFadeIn_m11511CB32C4632508DA7B32B68D164636701DB4C (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::SequenceEnd(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_SequenceEnd_mF1F3D8FF7478D0DB18CFF8CDF154A4D198ACBDC9 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * V_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B7_0 = NULL;
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * G_B6_0 = NULL;
	{
		// Character player = collider.gameObject.MMGetComponentNoAlloc<Character>();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = ___collider0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1;
		L_1 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameObjectExtensions_t1DDCEA11B7703EEA3E6C90E23ED4261AE39C49A1_il2cpp_TypeInfo_var);
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_2;
		L_2 = GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73(L_1, /*hidden argument*/GameObjectExtensions_MMGetComponentNoAlloc_TisCharacter_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612_m7B872A174289A461159887DA276091450EC5CA73_RuntimeMethod_var);
		V_0 = L_2;
		// if (FreezeCharacter && (player != null))
		bool L_3 = __this->get_FreezeCharacter_69();
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0023;
		}
	}
	{
		// player.UnFreeze();
		Character_tB3F7717D876D5CA5537405D23A3DAA5AF95C3612 * L_6 = V_0;
		VirtActionInvoker0::Invoke(27 /* System.Void MoreMountains.CorgiEngine.Character::UnFreeze() */, L_6);
	}

IL_0023:
	{
		// if (FreezeTime)
		bool L_7 = __this->get_FreezeTime_68();
		if (!L_7)
		{
			goto IL_0042;
		}
	}
	{
		// MMTimeScaleEvent.Trigger(MMTimeScaleMethods.Unfreeze, 1f, 0f, false, 0f, false);
		MMTimeScaleEvent_Trigger_mF54D807E19DF44F9EB1E07B9A76C8E294632741E(2, (1.0f), (0.0f), (bool)0, (0.0f), (bool)0, /*hidden argument*/NULL);
	}

IL_0042:
	{
		// SequenceEndFeedback?.PlayFeedbacks();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_8 = __this->get_SequenceEndFeedback_79();
		MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * L_9 = L_8;
		G_B6_0 = L_9;
		if (L_9)
		{
			G_B7_0 = L_9;
			goto IL_004d;
		}
	}
	{
		return;
	}

IL_004d:
	{
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.Feedbacks.MMFeedbacks::PlayFeedbacks() */, G_B7_0);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::TriggerExitAction(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_TriggerExitAction_m50723E067B489C598F88FB2F851CFBE1820B17FB (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_ignoreList.Contains(collider.transform))
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = __this->get__ignoreList_81();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_1, /*hidden argument*/NULL);
		bool L_3;
		L_3 = List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD(L_0, L_2, /*hidden argument*/List_1_Contains_m37E02ADF41F0AFB1B9BF6BE8E4BC09D48405EFAD_RuntimeMethod_var);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		// _ignoreList.Remove(collider.transform);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_4 = __this->get__ignoreList_81();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = ___collider0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_5, /*hidden argument*/NULL);
		bool L_7;
		L_7 = List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A(L_4, L_6, /*hidden argument*/List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
	}

IL_0025:
	{
		// base.TriggerExitAction(collider);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_8 = ___collider0;
		ButtonActivated_TriggerExitAction_m97D1EAB36E83B9D79288723D747EB956CFAE66D8(__this, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::AddToIgnoreList(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_AddToIgnoreList_mFC4CA451080B806D40B6C25D5FF69BEDA5AB1114 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___objectToIgnore0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _ignoreList.Add(objectToIgnore);
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_0 = __this->get__ignoreList_81();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = ___objectToIgnore0;
		List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982(L_0, L_1, /*hidden argument*/List_1_Add_m7AB707ADE023585729593334A399B3FF485A7982_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::OnDrawGizmos()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter_OnDrawGizmos_m91C31E92F566D4B41646704FF6F8A18AEC681C42 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMDebug_tD934F35BAB52AB49F049893A676764762BF27019_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Destination != null)
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_0 = __this->get_Destination_56();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00aa;
		}
	}
	{
		// MMDebug.DrawGizmoArrow(this.transform.position, (Destination.transform.position + Destination.ExitOffset) - this.transform.position, Color.cyan, 1f, 25f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_4 = __this->get_Destination_56();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_5, /*hidden argument*/NULL);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_7 = __this->get_Destination_56();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = L_7->get_ExitOffset_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_6, L_8, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_10, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_9, L_11, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_13;
		L_13 = Color_get_cyan_m0C608BC083FD98C45C1F4F15AE803D288C647686(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MMDebug_tD934F35BAB52AB49F049893A676764762BF27019_il2cpp_TypeInfo_var);
		MMDebug_DrawGizmoArrow_m6B5F0DF9A12597B04EBE240E7CBA67F721A49AE1(L_3, L_12, L_13, (1.0f), (25.0f), /*hidden argument*/NULL);
		// MMDebug.DebugDrawCross(this.transform.position + ExitOffset, 0.5f, Color.yellow);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14;
		L_14 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_14, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = __this->get_ExitOffset_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_15, L_16, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_18;
		L_18 = Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1(/*hidden argument*/NULL);
		MMDebug_DebugDrawCross_mACB3343EF705188007D0CB3841BC1AEE194CCF0D(L_17, (0.5f), L_18, /*hidden argument*/NULL);
		// MMDebug.DrawPoint(this.transform.position + ExitOffset, Color.yellow, 0.5f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_19;
		L_19 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_19, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21 = __this->get_ExitOffset_51();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_22;
		L_22 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_20, L_21, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_23;
		L_23 = Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1(/*hidden argument*/NULL);
		MMDebug_DrawPoint_m3D487A68FDEB4E5EF7F4976D70445A10A0A0D38A(L_22, L_23, (0.5f), /*hidden argument*/NULL);
	}

IL_00aa:
	{
		// if (TargetRoom != null)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_24 = __this->get_TargetRoom_60();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_25;
		L_25 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_24, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_00f7;
		}
	}
	{
		// MMDebug.DrawGizmoArrow(this.transform.position, TargetRoom.transform.position - this.transform.position, MMColors.Pink, 1f, 25f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_26;
		L_26 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_26, /*hidden argument*/NULL);
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_28 = __this->get_TargetRoom_60();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29;
		L_29 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_28, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_29, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_31;
		L_31 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_31, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_30, L_32, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_il2cpp_TypeInfo_var);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_34 = ((MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_StaticFields*)il2cpp_codegen_static_fields_for(MMColors_t30851F281A7EE3F182BB7BD88C8B42B4FB34D0B2_il2cpp_TypeInfo_var))->get_Pink_111();
		IL2CPP_RUNTIME_CLASS_INIT(MMDebug_tD934F35BAB52AB49F049893A676764762BF27019_il2cpp_TypeInfo_var);
		MMDebug_DrawGizmoArrow_m6B5F0DF9A12597B04EBE240E7CBA67F721A49AE1(L_27, L_33, L_34, (1.0f), (25.0f), /*hidden argument*/NULL);
	}

IL_00f7:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Teleporter__ctor_mBE564F38B9FA5BA13449AF9283AC535B871A9DB4 (Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool OnlyAffectsPlayer = true;
		__this->set_OnlyAffectsPlayer_50((bool)1);
		// public MMTween.MMTweenCurve TweenCurve = MMTween.MMTweenCurve.EaseInCubic;
		__this->set_TweenCurve_53(4);
		// public bool AddToDestinationIgnoreList = true;
		__this->set_AddToDestinationIgnoreList_57((bool)1);
		// public CameraModes CameraMode = CameraModes.TeleportCamera;
		__this->set_CameraMode_58(1);
		// public MMTweenType FadeTween = new MMTweenType(MMTween.MMTweenCurve.EaseInCubic);
		MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 * L_0 = (MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175 *)il2cpp_codegen_object_new(MMTweenType_tFC19C698DA9BA94FF1935880AE64857EE31C1175_il2cpp_TypeInfo_var);
		MMTweenType__ctor_mE4203911CD62AF1A455460DC0674447C2CBE0D8D(L_0, 4, /*hidden argument*/NULL);
		__this->set_FadeTween_63(L_0);
		// public bool MoveMask = true;
		__this->set_MoveMask_64((bool)1);
		// public MMTween.MMTweenCurve MoveMaskCurve = MMTween.MMTweenCurve.EaseInCubic;
		__this->set_MoveMaskCurve_65(4);
		// public MMSpriteMaskEvent.MMSpriteMaskEventTypes MoveMaskMethod = MMSpriteMaskEvent.MMSpriteMaskEventTypes.ExpandAndMoveToNewPosition;
		__this->set_MoveMaskMethod_66(1);
		// public float MoveMaskDuration = 0.2f;
		__this->set_MoveMaskDuration_67((0.200000003f));
		// public bool FreezeCharacter = true;
		__this->set_FreezeCharacter_69((bool)1);
		// public float InitialDelay = 0.1f;
		__this->set_InitialDelay_70((0.100000001f));
		// public float FadeOutDuration = 0.2f;
		__this->set_FadeOutDuration_71((0.200000003f));
		// public float DelayBetweenFades = 0.3f;
		__this->set_DelayBetweenFades_72((0.300000012f));
		// public float FadeInDuration = 0.2f;
		__this->set_FadeInDuration_73((0.200000003f));
		// public float FinalDelay = 0.1f;
		__this->set_FinalDelay_74((0.100000001f));
		ButtonActivated__ctor_m6FEA8A488C4C38817E424F66B6956EB282CFEE81(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEnableCameraU3Ed__2__ctor_m1D9DDDCC28E121A6A0C27F904A2A8F29F7E70CCD (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEnableCameraU3Ed__2_System_IDisposable_Dispose_m4AE158A18B8F6565EC11D412F07D3C9F47055B83 (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CEnableCameraU3Ed__2_MoveNext_m5D25253181AE42BD1A6D5875606D5E60A1543B1C (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_003f;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return base.EnableCamera(state, frames);
		CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * L_4 = V_1;
		bool L_5 = __this->get_state_3();
		int32_t L_6 = __this->get_frames_4();
		RuntimeObject* L_7;
		L_7 = CorgiCinemachineZone_U3CU3En__0_m2B0BD1A14B71AD52EE8B2105DE29729579DE90A9(L_4, L_5, L_6, /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_7);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_003f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if (state)
		bool L_8 = __this->get_state_3();
		if (!L_8)
		{
			goto IL_0067;
		}
	}
	{
		// _cinemachineCameraController.FollowsAPlayer = true;
		CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * L_9 = V_1;
		CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * L_10 = L_9->get__cinemachineCameraController_26();
		L_10->set_FollowsAPlayer_5((bool)1);
		// _cinemachineCameraController.StartFollowing();
		CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * L_11 = V_1;
		CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * L_12 = L_11->get__cinemachineCameraController_26();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StartFollowing() */, L_12);
		// }
		goto IL_007e;
	}

IL_0067:
	{
		// _cinemachineCameraController.StopFollowing();
		CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * L_13 = V_1;
		CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * L_14 = L_13->get__cinemachineCameraController_26();
		VirtActionInvoker0::Invoke(12 /* System.Void MoreMountains.CorgiEngine.CinemachineCameraController::StopFollowing() */, L_14);
		// _cinemachineCameraController.FollowsAPlayer = false;
		CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4 * L_15 = V_1;
		CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * L_16 = L_15->get__cinemachineCameraController_26();
		L_16->set_FollowsAPlayer_5((bool)0);
	}

IL_007e:
	{
		// }
		return (bool)0;
	}
}
// System.Object MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CEnableCameraU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03610361267D7AFEEDBA116769EAA9B1C43FAB4 (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_Reset_m30A091C8111BCAE444372D8EFF6E6C1674AEE9B5 (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_Reset_m30A091C8111BCAE444372D8EFF6E6C1674AEE9B5_RuntimeMethod_var)));
	}
}
// System.Object MoreMountains.CorgiEngine.CorgiCinemachineZone/<EnableCamera>d__2::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_get_Current_m0080986FA7BEECF8586B5083D26C228D4F7D6A8A (U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResizeConfinerU3Ed__19__ctor_mFD9E4619E6D3E4ABD7AE146801D22E1C2F5F2A59 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResizeConfinerU3Ed__19_System_IDisposable_Dispose_m4962D021C26E0B0EE67F6CB3955EF04785AE9CE3 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CResizeConfinerU3Ed__19_MoveNext_m295467BE7795C49B83733DE8177BD6961B25AFE4 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Room_t392D52D203A7F630C190D47A07148A57F4020B42 * V_1 = NULL;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_005f;
			}
			case 2:
			{
				goto IL_0076;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0022:
	{
		__this->set_U3CU3E1__state_0((-1));
		// if ((VirtualCamera == null) || (Confiner == null) || !ResizeConfinerAutomatically)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_3 = V_1;
		CinemachineVirtualCamera_t5BD4629093D8B75CE9F4A382AD28E6F96938C43C * L_4 = L_3->get_VirtualCamera_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_004d;
		}
	}
	{
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_6 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_7 = L_6->get_Confiner_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_7, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_004d;
		}
	}
	{
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_9 = V_1;
		bool L_10 = L_9->get_ResizeConfinerAutomatically_7();
		if (L_10)
		{
			goto IL_004f;
		}
	}

IL_004d:
	{
		// yield break;
		return (bool)0;
	}

IL_004f:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005f:
	{
		__this->set_U3CU3E1__state_0((-1));
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0076:
	{
		__this->set_U3CU3E1__state_0((-1));
		// (Confiner as BoxCollider2D).offset = _roomCollider2D.offset;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_11 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_12 = L_11->get_Confiner_5();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_13 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_14 = L_13->get__roomCollider2D_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_15;
		L_15 = Collider2D_get_offset_m63144560DFF782608BA6627DE192D6337821995C(L_14, /*hidden argument*/NULL);
		Collider2D_set_offset_m496DDE5487C59E9E8D6B468E8918EE1EFC625E38(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_12, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), L_15, /*hidden argument*/NULL);
		// (Confiner as BoxCollider2D).size = (_roomCollider2D as BoxCollider2D).size;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_16 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_17 = L_16->get_Confiner_5();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_18 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_19 = L_18->get__roomCollider2D_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_20;
		L_20 = BoxCollider2D_get_size_m011E7AA7861BF58898A64D986A4235C1E2061BF9(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_19, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_17, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), L_20, /*hidden argument*/NULL);
		// _cameraSize.y = 2 * _mainCamera.orthographicSize;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_21 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_22 = L_21->get_address_of__cameraSize_17();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_23 = V_1;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_24 = L_23->get__mainCamera_16();
		float L_25;
		L_25 = Camera_get_orthographicSize_m970DC87D428A71EDF30F9ED7D0E76E08B1BE4EFE(L_24, /*hidden argument*/NULL);
		L_22->set_y_1(((float)il2cpp_codegen_multiply((float)(2.0f), (float)L_25)));
		// _cameraSize.x = _cameraSize.y * _mainCamera.aspect;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_26 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_27 = L_26->get_address_of__cameraSize_17();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_28 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_29 = L_28->get_address_of__cameraSize_17();
		float L_30 = L_29->get_y_1();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_31 = V_1;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_32 = L_31->get__mainCamera_16();
		float L_33;
		L_33 = Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987(L_32, /*hidden argument*/NULL);
		L_27->set_x_0(((float)il2cpp_codegen_multiply((float)L_30, (float)L_33)));
		// Vector2 newSize = (Confiner as BoxCollider2D).size;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_34 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_35 = L_34->get_Confiner_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_36;
		L_36 = BoxCollider2D_get_size_m011E7AA7861BF58898A64D986A4235C1E2061BF9(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_35, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		V_2 = L_36;
		// if ((Confiner as BoxCollider2D).size.x < _cameraSize.x)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_37 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_38 = L_37->get_Confiner_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_39;
		L_39 = BoxCollider2D_get_size_m011E7AA7861BF58898A64D986A4235C1E2061BF9(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_38, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		float L_40 = L_39.get_x_0();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_41 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_42 = L_41->get_address_of__cameraSize_17();
		float L_43 = L_42->get_x_0();
		if ((!(((float)L_40) < ((float)L_43))))
		{
			goto IL_013b;
		}
	}
	{
		// newSize.x = _cameraSize.x;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_44 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_45 = L_44->get_address_of__cameraSize_17();
		float L_46 = L_45->get_x_0();
		(&V_2)->set_x_0(L_46);
	}

IL_013b:
	{
		// if ((Confiner as BoxCollider2D).size.y < _cameraSize.y)
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_47 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_48 = L_47->get_Confiner_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_49;
		L_49 = BoxCollider2D_get_size_m011E7AA7861BF58898A64D986A4235C1E2061BF9(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_48, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		float L_50 = L_49.get_y_1();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_51 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_52 = L_51->get_address_of__cameraSize_17();
		float L_53 = L_52->get_y_1();
		if ((!(((float)L_50) < ((float)L_53))))
		{
			goto IL_016f;
		}
	}
	{
		// newSize.y = _cameraSize.y;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_54 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_55 = L_54->get_address_of__cameraSize_17();
		float L_56 = L_55->get_y_1();
		(&V_2)->set_y_1(L_56);
	}

IL_016f:
	{
		// (Confiner as BoxCollider2D).size = newSize;
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_57 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_58 = L_57->get_Confiner_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_59 = V_2;
		BoxCollider2D_set_size_m8460A38ADDD4BE82BE1F416DE3D7AFB87EBA6760(((BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9 *)IsInstSealed((RuntimeObject*)L_58, BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_il2cpp_TypeInfo_var)), L_59, /*hidden argument*/NULL);
		// CinemachineCameraConfiner.InvalidatePathCache();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_60 = V_1;
		CinemachineConfiner_tB85C2863E800596353E4E8F6BA19492837F7C45D * L_61 = L_60->get_CinemachineCameraConfiner_6();
		CinemachineConfiner_InvalidatePathCache_m1C66E34112C65B4CDACC628AD8D7F8C90F45706B(L_61, /*hidden argument*/NULL);
		// HandleLevelStartDetection();
		Room_t392D52D203A7F630C190D47A07148A57F4020B42 * L_62 = V_1;
		VirtActionInvoker0::Invoke(8 /* System.Void MoreMountains.CorgiEngine.Room::HandleLevelStartDetection() */, L_62);
		// }
		return (bool)0;
	}
}
// System.Object MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CResizeConfinerU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FBA07F0972F7340AB48E3C344E3E589BB92DAF3 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_Reset_m62D2D3B24FC3BA71ED40F889D1B05FD0D02F4A05 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_Reset_m62D2D3B24FC3BA71ED40F889D1B05FD0D02F4A05_RuntimeMethod_var)));
	}
}
// System.Object MoreMountains.CorgiEngine.Room/<ResizeConfiner>d__19::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_get_Current_m4526C7997761778F7232684D72638DA47D6CAC86 (U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportSequenceU3Ed__47__ctor_m8AB83EFFF24D4A556C827B654ED7B8915B8DF816 (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportSequenceU3Ed__47_System_IDisposable_Dispose_m810BF66E18978CB0ED0DF9AF0BA37584610CFF1E (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTeleportSequenceU3Ed__47_MoveNext_mA202004968F84A3C212402FA753BF4E660705EB8 (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * V_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0032;
			}
			case 1:
			{
				goto IL_005a;
			}
			case 2:
			{
				goto IL_0082;
			}
			case 3:
			{
				goto IL_00aa;
			}
			case 4:
			{
				goto IL_00d2;
			}
			case 5:
			{
				goto IL_00fa;
			}
			case 6:
			{
				goto IL_0122;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0032:
	{
		__this->set_U3CU3E1__state_0((-1));
		// SequenceStart(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_3 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_4 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(30 /* System.Void MoreMountains.CorgiEngine.Teleporter::SequenceStart(UnityEngine.Collider2D) */, L_3, L_4);
		// yield return _initialDelayWaitForSeconds;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_5 = V_1;
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_6 = L_5->get__initialDelayWaitForSeconds_82();
		__this->set_U3CU3E2__current_1(L_6);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_005a:
	{
		__this->set_U3CU3E1__state_0((-1));
		// AfterInitialDelay(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_7 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_8 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(31 /* System.Void MoreMountains.CorgiEngine.Teleporter::AfterInitialDelay(UnityEngine.Collider2D) */, L_7, L_8);
		// yield return _fadeOutDurationWaitForSeconds;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_9 = V_1;
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_10 = L_9->get__fadeOutDurationWaitForSeconds_83();
		__this->set_U3CU3E2__current_1(L_10);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0082:
	{
		__this->set_U3CU3E1__state_0((-1));
		// AfterFadeOut(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_11 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_12 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(32 /* System.Void MoreMountains.CorgiEngine.Teleporter::AfterFadeOut(UnityEngine.Collider2D) */, L_11, L_12);
		// yield return _halfDelayBetweenFadesWaitForSeconds;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_13 = V_1;
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_14 = L_13->get__halfDelayBetweenFadesWaitForSeconds_84();
		__this->set_U3CU3E2__current_1(L_14);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_00aa:
	{
		__this->set_U3CU3E1__state_0((-1));
		// BetweenFades(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_15 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_16 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(35 /* System.Void MoreMountains.CorgiEngine.Teleporter::BetweenFades(UnityEngine.Collider2D) */, L_15, L_16);
		// yield return _halfDelayBetweenFadesWaitForSeconds;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_17 = V_1;
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_18 = L_17->get__halfDelayBetweenFadesWaitForSeconds_84();
		__this->set_U3CU3E2__current_1(L_18);
		__this->set_U3CU3E1__state_0(4);
		return (bool)1;
	}

IL_00d2:
	{
		__this->set_U3CU3E1__state_0((-1));
		// AfterDelayBetweenFades(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_19 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_20 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(36 /* System.Void MoreMountains.CorgiEngine.Teleporter::AfterDelayBetweenFades(UnityEngine.Collider2D) */, L_19, L_20);
		// yield return _fadeInDurationWaitForSeconds;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_21 = V_1;
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_22 = L_21->get__fadeInDurationWaitForSeconds_85();
		__this->set_U3CU3E2__current_1(L_22);
		__this->set_U3CU3E1__state_0(5);
		return (bool)1;
	}

IL_00fa:
	{
		__this->set_U3CU3E1__state_0((-1));
		// AfterFadeIn(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_23 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_24 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(37 /* System.Void MoreMountains.CorgiEngine.Teleporter::AfterFadeIn(UnityEngine.Collider2D) */, L_23, L_24);
		// yield return _finalDelayyWaitForSeconds;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_25 = V_1;
		WaitForSecondsRealtime_t04F2884A9814C3E4E415E788AFE56B5928577C40 * L_26 = L_25->get__finalDelayyWaitForSeconds_86();
		__this->set_U3CU3E2__current_1(L_26);
		__this->set_U3CU3E1__state_0(6);
		return (bool)1;
	}

IL_0122:
	{
		__this->set_U3CU3E1__state_0((-1));
		// SequenceEnd(collider);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_27 = V_1;
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_28 = __this->get_collider_3();
		VirtActionInvoker1< Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * >::Invoke(38 /* System.Void MoreMountains.CorgiEngine.Teleporter::SequenceEnd(UnityEngine.Collider2D) */, L_27, L_28);
		// }
		return (bool)0;
	}
}
// System.Object MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportSequenceU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3DB96977E9170F1361CAC32DF7C6A7251C6BB0D (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_Reset_m53A758DC63962FA755B19A04E75DF4A29BC4A292 (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_Reset_m53A758DC63962FA755B19A04E75DF4A29BC4A292_RuntimeMethod_var)));
	}
}
// System.Object MoreMountains.CorgiEngine.Teleporter/<TeleportSequence>d__47::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_get_Current_m75E54E7AB41FB6E6C76D783563F038E14CF86B75 (U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportTweenCoU3Ed__52__ctor_m1644936594C6AC11FF6F80E342A05642C083BF68 (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportTweenCoU3Ed__52_System_IDisposable_Dispose_m27BD8E4476BA2FE7469F6BE440B747D6F9758C46 (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CTeleportTweenCoU3Ed__52_MoveNext_m69F3E5441AF7554FC4FE45FFCC3065EB79CDDB83 (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * V_1 = NULL;
	float V_2 = 0.0f;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_1 = __this->get_U3CU3E4__this_3();
		V_1 = L_1;
		int32_t L_2 = V_0;
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)1)))
		{
			goto IL_0081;
		}
	}
	{
		return (bool)0;
	}

IL_0017:
	{
		__this->set_U3CU3E1__state_0((-1));
		// float startedAt = Time.unscaledTime;
		float L_4;
		L_4 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		__this->set_U3CstartedAtU3E5__2_6(L_4);
		goto IL_0088;
	}

IL_002b:
	{
		// float elapsedTime = Time.unscaledTime - startedAt;
		float L_5;
		L_5 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		float L_6 = __this->get_U3CstartedAtU3E5__2_6();
		V_2 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_6));
		// collider.transform.position = MMTween.Tween(elapsedTime, 0f, DelayBetweenFades/2f, origin, destination, TweenCurve);
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_7 = __this->get_collider_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_7, /*hidden argument*/NULL);
		float L_9 = V_2;
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_10 = V_1;
		float L_11 = L_10->get_DelayBetweenFades_72();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = __this->get_origin_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = __this->get_destination_5();
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_14 = V_1;
		int32_t L_15 = L_14->get_TweenCurve_53();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = MMTween_Tween_m2BDC8328BE236482F235949CE01A9CE2118CA21E(L_9, (0.0f), ((float)((float)L_11/(float)(2.0f))), L_12, L_13, L_15, /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_8, L_16, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_0081:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_0088:
	{
		// while (Time.unscaledTime - startedAt < DelayBetweenFades/2f)
		float L_17;
		L_17 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		float L_18 = __this->get_U3CstartedAtU3E5__2_6();
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_19 = V_1;
		float L_20 = L_19->get_DelayBetweenFades_72();
		if ((((float)((float)il2cpp_codegen_subtract((float)L_17, (float)L_18))) < ((float)((float)((float)L_20/(float)(2.0f))))))
		{
			goto IL_002b;
		}
	}
	{
		// _ignoreList.Remove(collider.transform);
		Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870 * L_21 = V_1;
		List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * L_22 = L_21->get__ignoreList_81();
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_23 = __this->get_collider_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_23, /*hidden argument*/NULL);
		bool L_25;
		L_25 = List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A(L_22, L_24, /*hidden argument*/List_1_Remove_m4FBB736F89C431F0D19B655E23269EAB321B863A_RuntimeMethod_var);
		// }
		return (bool)0;
	}
}
// System.Object MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportTweenCoU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CF0CB64F7ADDDEF90530D50755562A5EB7F418F (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_Reset_mF9D98A4E2B68907316E1373F44042532309211F6 (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_Reset_mF9D98A4E2B68907316E1373F44042532309211F6_RuntimeMethod_var)));
	}
}
// System.Object MoreMountains.CorgiEngine.Teleporter/<TeleportTweenCo>d__52::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_get_Current_m5EA2400320AFC0C2F6E5F002CA071D3221C83E9A (U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___v0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v0;
		float L_3 = L_2.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), L_1, L_3, (0.0f), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001a;
	}

IL_001a:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  CameraState_get_RawPosition_mA01BC7F842C837D49F0542A0475AE0EA9C88539D_inline (CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * __this, const RuntimeMethod* method)
{
	{
		// public Vector3 RawPosition { get; set; }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = __this->get_U3CRawPositionU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CameraState_set_RawPosition_m0E1142A79939C7AD7156986F91CD0F1D60499A31_inline (CameraState_t013E2FD0AB221E45E189CCBE69E25F79E2446601 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method)
{
	{
		// public Vector3 RawPosition { get; set; }
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___value0;
		__this->set_U3CRawPositionU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * LevelManager_get_BoundsCollider2D_m38E36A905EB6DF0E0B21758DDA955453641339B0_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method)
{
	{
		// public Collider2D BoundsCollider2D { get; protected set; }
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = __this->get_U3CBoundsCollider2DU3Ek__BackingField_22();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * LevelManager_get_BoundsCollider_mF941E9DDAD3B216DE55E1A7ECD29F1A3D349E922_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method)
{
	{
		// public Collider BoundsCollider { get; protected set; }
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = __this->get_U3CBoundsColliderU3Ek__BackingField_21();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD_inline (CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// public bool FollowsPlayer { get; set; }
		bool L_0 = ___value0;
		__this->set_U3CFollowsPlayerU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE_inline (CorgiController_t7079FC8AE5B9F509AC9B02D90E34EBC68654BCB8 * __this, const RuntimeMethod* method)
{
	{
		// public Vector2 Speed { get{ return _speed; } }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get__speed_37();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CinemachineVirtualCameraBase_set_Priority_m6C180B742F19E669D648B6D1BE4D9D9C5824962B_inline (CinemachineVirtualCameraBase_tFF7E3B9D8C53B0FFEBF8969BED5854D1EEB76AD5 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int Priority { get { return m_Priority; } set { m_Priority = value; } }
		int32_t L_0 = ___value0;
		__this->set_m_Priority_9(L_0);
		// public int Priority { get { return m_Priority; } set { m_Priority = value; } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method)
{
	{
		// public List<Character> Players { get; protected set; }
		List_1_t5DB922CEE6338D263668FE75E45C8566802F493E * L_0 = __this->get_U3CPlayersU3Ek__BackingField_27();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * Room_get_RoomCollider_mE452329CF0C1B9ACBB4CC515C9DD4ADF511ACC6F_inline (Room_t392D52D203A7F630C190D47A07148A57F4020B42 * __this, const RuntimeMethod* method)
{
	{
		// public Collider2D RoomCollider { get { return _roomCollider2D; } }
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_0 = __this->get__roomCollider2D_15();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * LevelManager_get_LevelCameraController_m81BFB44DD7452381F90CE3F622A7FF33A5953152_inline (LevelManager_t722A10F3B3AF477EBF2CB8C8DA469C511126B91E * __this, const RuntimeMethod* method)
{
	{
		// public CameraController LevelCameraController { get; set; }
		CameraController_t99EB13C0C52CDC4C2D775D8FA44CD520AFC930C6 * L_0 = __this->get_U3CLevelCameraControllerU3Ek__BackingField_26();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}

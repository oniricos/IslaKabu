﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MoreMountains.CorgiEngine.BackgroundTree::Start()
extern void BackgroundTree_Start_m898A0B450B7B456E5A74AA54E44E951C4C35E076 (void);
// 0x00000002 System.Void MoreMountains.CorgiEngine.BackgroundTree::Update()
extern void BackgroundTree_Update_mB290C176CD97B64E083B3EEFCBDB6D5DE96F14DE (void);
// 0x00000003 UnityEngine.Vector3 MoreMountains.CorgiEngine.BackgroundTree::WiggleScale()
extern void BackgroundTree_WiggleScale_m88A20BAE284AA98537CADE6E2C2D4504B6ACA87A (void);
// 0x00000004 UnityEngine.Quaternion MoreMountains.CorgiEngine.BackgroundTree::WiggleRotate()
extern void BackgroundTree_WiggleRotate_m1D0E88C06C72CDE0809A6DA6B3704CF1CF9355C8 (void);
// 0x00000005 System.Void MoreMountains.CorgiEngine.BackgroundTree::.ctor()
extern void BackgroundTree__ctor_m2BD03FF226E6859560729C172C9C4EB9B289A884 (void);
// 0x00000006 System.Boolean MoreMountains.CorgiEngine.LevelMapCharacter::get_CollidingWithAPathElement()
extern void LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2 (void);
// 0x00000007 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::set_CollidingWithAPathElement(System.Boolean)
extern void LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899 (void);
// 0x00000008 MoreMountains.CorgiEngine.LevelMapPathElement MoreMountains.CorgiEngine.LevelMapCharacter::get_LastVisitedPathElement()
extern void LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583 (void);
// 0x00000009 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::set_LastVisitedPathElement(MoreMountains.CorgiEngine.LevelMapPathElement)
extern void LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2 (void);
// 0x0000000A System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Start()
extern void LevelMapCharacter_Start_m926EC44D3202BF153F681E4048311DDB1A5E8CFA (void);
// 0x0000000B System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Update()
extern void LevelMapCharacter_Update_mE43EF1D69903EFBF6571A30CD2E0A9AD96EC5067 (void);
// 0x0000000C System.Void MoreMountains.CorgiEngine.LevelMapCharacter::InputMovement()
extern void LevelMapCharacter_InputMovement_m7683BFCCD876AF2572300C9C89FEF114441D0E27 (void);
// 0x0000000D System.Void MoreMountains.CorgiEngine.LevelMapCharacter::MoveCharacter()
extern void LevelMapCharacter_MoveCharacter_mA687E9B0FCA8DD8302D9C093E7F9A893C479B8B7 (void);
// 0x0000000E System.Void MoreMountains.CorgiEngine.LevelMapCharacter::AnimateCharacter()
extern void LevelMapCharacter_AnimateCharacter_mAFBB1072005CB90FA0EF66C66D3716D0DB547648 (void);
// 0x0000000F System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetCurrentPathElement(MoreMountains.CorgiEngine.LevelMapPathElement)
extern void LevelMapCharacter_SetCurrentPathElement_mB60CFD5D0371B7DCF20D67BB8894545A8EB4991A (void);
// 0x00000010 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetDestination(MoreMountains.CorgiEngine.LevelMapPathElement)
extern void LevelMapCharacter_SetDestination_m9DA1462F26BB41CD9617379754985D880A36B85C (void);
// 0x00000011 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::Flip()
extern void LevelMapCharacter_Flip_m73C6581590C0D1FA2EDA378C1F0E832660A40ACB (void);
// 0x00000012 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::ButtonPressed()
extern void LevelMapCharacter_ButtonPressed_m3A3752479794487EB647F9928AFC09110FDBA758 (void);
// 0x00000013 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetHorizontalMove(System.Single)
extern void LevelMapCharacter_SetHorizontalMove_mFE3466F76D6D632C24780058459422258EC9E312 (void);
// 0x00000014 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::SetVerticalMove(System.Single)
extern void LevelMapCharacter_SetVerticalMove_m3CF33D93147CA1639C7C8E6356D44D664DC0CE20 (void);
// 0x00000015 System.Void MoreMountains.CorgiEngine.LevelMapCharacter::.ctor()
extern void LevelMapCharacter__ctor_m53FE1564EB41AA1CE960C7571CBFEDA9E14B6735 (void);
// 0x00000016 System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoUp()
extern void LevelMapPathElement_CanGoUp_m1ADE0670DC5696D0CF682AF6358F448A8D230C56 (void);
// 0x00000017 System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoRight()
extern void LevelMapPathElement_CanGoRight_mE141BCE2495ED21BB77B1D1A6E89BBAB0C5A6407 (void);
// 0x00000018 System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoDown()
extern void LevelMapPathElement_CanGoDown_m918CEF6BA0991A242D98CCDC94451BD54DEF1099 (void);
// 0x00000019 System.Boolean MoreMountains.CorgiEngine.LevelMapPathElement::CanGoLeft()
extern void LevelMapPathElement_CanGoLeft_m3FB80BE75E0C5A5AF592B72D749976C08338C6C7 (void);
// 0x0000001A System.Void MoreMountains.CorgiEngine.LevelMapPathElement::Start()
extern void LevelMapPathElement_Start_m8ABEC4640D4F01094DFDC31EEB2E85366D8B4BEF (void);
// 0x0000001B System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnTriggerStay2D(UnityEngine.Collider2D)
extern void LevelMapPathElement_OnTriggerStay2D_m6F7EA546A8DFB866A95F32C3E335F41CFA185C0D (void);
// 0x0000001C System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnTriggerExit2D(UnityEngine.Collider2D)
extern void LevelMapPathElement_OnTriggerExit2D_mDF3BB11711CD5EC59BD7450356027ABD8E0D8DF9 (void);
// 0x0000001D System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void LevelMapPathElement_OnTriggerEnter2D_m4B2DA6ED322FF90AC270B46FA890442A5BD8DCFB (void);
// 0x0000001E System.Void MoreMountains.CorgiEngine.LevelMapPathElement::OnDrawGizmos()
extern void LevelMapPathElement_OnDrawGizmos_m170BE035B712DF066E8C2EB5936A3CA6998BA673 (void);
// 0x0000001F System.Void MoreMountains.CorgiEngine.LevelMapPathElement::.ctor()
extern void LevelMapPathElement__ctor_mAE02AF04C3FB0204EF09814117DA5D05789DF8BA (void);
// 0x00000020 System.Void MoreMountains.CorgiEngine.UncheckReverseGravityInput::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void UncheckReverseGravityInput_OnTriggerEnter2D_m963D8CB6D83DCE51E0A9B2B06D6A2DF8D84DF48D (void);
// 0x00000021 System.Void MoreMountains.CorgiEngine.UncheckReverseGravityInput::.ctor()
extern void UncheckReverseGravityInput__ctor_m157A9AA2942E17B6BD1EF0559297EBAC84188477 (void);
// 0x00000022 System.Void MoreMountains.CorgiEngine.RetroCopter::Initialization()
extern void RetroCopter_Initialization_mCB539802F71E0140B7068037F583B388981836DC (void);
// 0x00000023 System.Void MoreMountains.CorgiEngine.RetroCopter::ProcessAbility()
extern void RetroCopter_ProcessAbility_mD5EBA674023BA4A28339893E4EC5D20687D4299D (void);
// 0x00000024 System.Void MoreMountains.CorgiEngine.RetroCopter::.ctor()
extern void RetroCopter__ctor_mFB3A9D8C6C5369BDD999CF9A4FF89E65C0A733CA (void);
// 0x00000025 System.Void MoreMountains.CorgiEngine.RetroDoor::Awake()
extern void RetroDoor_Awake_m0AE22E765B0DF59C15D5A2FE260D24F29228FE16 (void);
// 0x00000026 System.Void MoreMountains.CorgiEngine.RetroDoor::.ctor()
extern void RetroDoor__ctor_mBD851611EE3F48A13A7C792B731C55751B7FFAF6 (void);
// 0x00000027 System.Void MoreMountains.CorgiEngine.RetroAdventureFinishLevel::GoToNextLevel()
extern void RetroAdventureFinishLevel_GoToNextLevel_m4E372B402D42CB81BF5C2AC233802060A5A8F6A5 (void);
// 0x00000028 System.Void MoreMountains.CorgiEngine.RetroAdventureFinishLevel::.ctor()
extern void RetroAdventureFinishLevel__ctor_mE00FE3EDFD027B723B602F9DBE5C8E5408D2E61B (void);
// 0x00000029 System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::Update()
extern void RetroAdventureGUIManager_Update_m535B6E047596ED9365DE4C0807B54585FD094781 (void);
// 0x0000002A System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::UpdateStars()
extern void RetroAdventureGUIManager_UpdateStars_mCCFD3A83212DF75B13A96F08F42E0C760E08960B (void);
// 0x0000002B System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::LevelComplete()
extern void RetroAdventureGUIManager_LevelComplete_m33DFE5D7582F17210D62C4C8313D55EF0769A255 (void);
// 0x0000002C System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void RetroAdventureGUIManager_OnMMEvent_m5813C52BDB5C6426DF724D4626A485E2921BB0E7 (void);
// 0x0000002D System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::OnEnable()
extern void RetroAdventureGUIManager_OnEnable_mA69C59DD6FC22949285D4A6D543E9CEB4FBB0C00 (void);
// 0x0000002E System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::OnDisable()
extern void RetroAdventureGUIManager_OnDisable_m7E4A376D7074CA96CAF9646EF3C89071812448CA (void);
// 0x0000002F System.Void MoreMountains.CorgiEngine.RetroAdventureGUIManager::.ctor()
extern void RetroAdventureGUIManager__ctor_mCEB46DF00172D3B7A2013E4D3B017F24EB5AA252 (void);
// 0x00000030 System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::GoToLevel()
extern void RetroAdventureLevel_GoToLevel_m0F5B54EA595A6D047F7298741688A5EA42EC8277 (void);
// 0x00000031 System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::Start()
extern void RetroAdventureLevel_Start_mD38C63C4C3098B5D161FD6DFB064301955D6FEF0 (void);
// 0x00000032 System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::InitialSetup()
extern void RetroAdventureLevel_InitialSetup_m0DC4B4FA1E2E13AE6CE25C4A75D903578C1B7DC4 (void);
// 0x00000033 System.Void MoreMountains.CorgiEngine.RetroAdventureLevel::.ctor()
extern void RetroAdventureLevel__ctor_mD3D9CE8D5D720F3D4DD7A672F21ECAB2E4090950 (void);
// 0x00000034 System.Void MoreMountains.CorgiEngine.RetroAdventureScene::.ctor()
extern void RetroAdventureScene__ctor_mFFA7165F7D3FC78DD571B766BD87533FB8B4285F (void);
// 0x00000035 System.Void MoreMountains.CorgiEngine.Progress::.ctor()
extern void Progress__ctor_m328101047C95644D26CFCC3D5B11384D08734D4F (void);
// 0x00000036 System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_InitialMaximumLives()
extern void RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB (void);
// 0x00000037 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_InitialMaximumLives(System.Int32)
extern void RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223 (void);
// 0x00000038 System.Int32 MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_InitialCurrentLives()
extern void RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52 (void);
// 0x00000039 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_InitialCurrentLives(System.Int32)
extern void RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C (void);
// 0x0000003A System.Single MoreMountains.CorgiEngine.RetroAdventureProgressManager::get_CurrentStars()
extern void RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC (void);
// 0x0000003B System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::set_CurrentStars(System.Single)
extern void RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56 (void);
// 0x0000003C System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::Awake()
extern void RetroAdventureProgressManager_Awake_m297B452272540A6B67E70D2965D335BE73F95738 (void);
// 0x0000003D System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::LevelComplete()
extern void RetroAdventureProgressManager_LevelComplete_m269033BC6F6CB023B473ADDC31AFA23350E44362 (void);
// 0x0000003E System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::InitializeStars()
extern void RetroAdventureProgressManager_InitializeStars_m0AA36F967AC3A29EDECF74A195167FE1F35CF14A (void);
// 0x0000003F System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::SaveProgress()
extern void RetroAdventureProgressManager_SaveProgress_mD5271DD17C484FB3A916D1C1426E98FC37431649 (void);
// 0x00000040 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::CreateSaveGame()
extern void RetroAdventureProgressManager_CreateSaveGame_mB8949DA72B6E82FF2563B16D0C9412040B106242 (void);
// 0x00000041 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::LoadSavedProgress()
extern void RetroAdventureProgressManager_LoadSavedProgress_m0794BAACFEAC07D49704DA9A148F9B662B13EA0D (void);
// 0x00000042 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineStarEvent)
extern void RetroAdventureProgressManager_OnMMEvent_mBD4A63105FFBB77336C3CA91494BE56AEEDCFC52 (void);
// 0x00000043 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void RetroAdventureProgressManager_OnMMEvent_mFD2CFF7F7D5A54D5977775A9F1D0B151EC2EE6E4 (void);
// 0x00000044 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::GameOver()
extern void RetroAdventureProgressManager_GameOver_m0F44C908A39FB4A9B91FE9699E1DA563E27B8F11 (void);
// 0x00000045 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::ResetLives()
extern void RetroAdventureProgressManager_ResetLives_m5216E3EA0AF1A655F740AF028D0E121DB8C7ACE1 (void);
// 0x00000046 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::ResetProgress()
extern void RetroAdventureProgressManager_ResetProgress_m4307B34F9DD80AF725E7C3AD51D9002364D15928 (void);
// 0x00000047 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnEnable()
extern void RetroAdventureProgressManager_OnEnable_mC654DC829247D18D50F7034358F5F0BE9E7F7B0A (void);
// 0x00000048 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::OnDisable()
extern void RetroAdventureProgressManager_OnDisable_m1378CA6151546C02FC41D13F65BAD9BA383B0C4A (void);
// 0x00000049 System.Void MoreMountains.CorgiEngine.RetroAdventureProgressManager::.ctor()
extern void RetroAdventureProgressManager__ctor_mB3F495CE08D74760707ED4C5BE97746B96BF2B32 (void);
// 0x0000004A System.Void MoreMountains.CorgiEngine.RetroNextLevel::NextLevel()
extern void RetroNextLevel_NextLevel_m69D42A79000BD005B73C938B6C559BE114065C55 (void);
// 0x0000004B System.Void MoreMountains.CorgiEngine.RetroNextLevel::.ctor()
extern void RetroNextLevel__ctor_m87098EEA866DE6507D4C779B02D15DDC8DA04D9F (void);
// 0x0000004C System.Void MoreMountains.CorgiEngine.RetroStar::Start()
extern void RetroStar_Start_mF1EB43419CFC05D037CBEEB30E83533495EB727E (void);
// 0x0000004D System.Void MoreMountains.CorgiEngine.RetroStar::DisableIfAlreadyCollected()
extern void RetroStar_DisableIfAlreadyCollected_mB70C9C58119152E68436AEF45D4D329498DE89C2 (void);
// 0x0000004E System.Void MoreMountains.CorgiEngine.RetroStar::Disable()
extern void RetroStar_Disable_m1FA46910EF55FCF7F6217F0FECBA338D561CFBFA (void);
// 0x0000004F System.Void MoreMountains.CorgiEngine.RetroStar::.ctor()
extern void RetroStar__ctor_m51FCC9FB2918C70D1997D3B5F9F82331D9611305 (void);
// 0x00000050 System.Boolean MoreMountains.CorgiEngine.Mushroom::CheckIfPickable()
extern void Mushroom_CheckIfPickable_m4B5C49A62BD2D4D937959ADABF99C1ADA0C021E2 (void);
// 0x00000051 System.Void MoreMountains.CorgiEngine.Mushroom::Pick()
extern void Mushroom_Pick_m00F6FA0FF0A52045F46377126EE885E7B9AA9279 (void);
// 0x00000052 System.Void MoreMountains.CorgiEngine.Mushroom::.ctor()
extern void Mushroom__ctor_m41FF2F1C0AACCC9AE564060D7F1820D7357054F6 (void);
// 0x00000053 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Initialization()
extern void SuperHipsterBrosHealth_Initialization_m6AC48ED265AD32EA71256AE63A00737508593C8E (void);
// 0x00000054 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Damage(System.Int32,UnityEngine.GameObject,System.Single,System.Single,UnityEngine.Vector3)
extern void SuperHipsterBrosHealth_Damage_mA9E773BA8F793D9E897E13D44234702BFB3208DA (void);
// 0x00000055 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Grow(System.Single)
extern void SuperHipsterBrosHealth_Grow_m42039F01DB484C347D26BF1F562672F0D11B1C9F (void);
// 0x00000056 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Shrink(System.Single)
extern void SuperHipsterBrosHealth_Shrink_m83E2B699753362A96EB8D0991A635630E0731C48 (void);
// 0x00000057 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::ResetScale(System.Single)
extern void SuperHipsterBrosHealth_ResetScale_m61B52A7BDBDB04396508823157B6207559B77188 (void);
// 0x00000058 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::Kill()
extern void SuperHipsterBrosHealth_Kill_m1B5B0969B2DDEF808EE1C7EAA52D16D9FB2291E7 (void);
// 0x00000059 System.Void MoreMountains.CorgiEngine.SuperHipsterBrosHealth::.ctor()
extern void SuperHipsterBrosHealth__ctor_m7F0074239E8783F7E7A70F5BFA71289CE0ECD12A (void);
static Il2CppMethodPointer s_methodPointers[89] = 
{
	BackgroundTree_Start_m898A0B450B7B456E5A74AA54E44E951C4C35E076,
	BackgroundTree_Update_mB290C176CD97B64E083B3EEFCBDB6D5DE96F14DE,
	BackgroundTree_WiggleScale_m88A20BAE284AA98537CADE6E2C2D4504B6ACA87A,
	BackgroundTree_WiggleRotate_m1D0E88C06C72CDE0809A6DA6B3704CF1CF9355C8,
	BackgroundTree__ctor_m2BD03FF226E6859560729C172C9C4EB9B289A884,
	LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2,
	LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899,
	LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583,
	LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2,
	LevelMapCharacter_Start_m926EC44D3202BF153F681E4048311DDB1A5E8CFA,
	LevelMapCharacter_Update_mE43EF1D69903EFBF6571A30CD2E0A9AD96EC5067,
	LevelMapCharacter_InputMovement_m7683BFCCD876AF2572300C9C89FEF114441D0E27,
	LevelMapCharacter_MoveCharacter_mA687E9B0FCA8DD8302D9C093E7F9A893C479B8B7,
	LevelMapCharacter_AnimateCharacter_mAFBB1072005CB90FA0EF66C66D3716D0DB547648,
	LevelMapCharacter_SetCurrentPathElement_mB60CFD5D0371B7DCF20D67BB8894545A8EB4991A,
	LevelMapCharacter_SetDestination_m9DA1462F26BB41CD9617379754985D880A36B85C,
	LevelMapCharacter_Flip_m73C6581590C0D1FA2EDA378C1F0E832660A40ACB,
	LevelMapCharacter_ButtonPressed_m3A3752479794487EB647F9928AFC09110FDBA758,
	LevelMapCharacter_SetHorizontalMove_mFE3466F76D6D632C24780058459422258EC9E312,
	LevelMapCharacter_SetVerticalMove_m3CF33D93147CA1639C7C8E6356D44D664DC0CE20,
	LevelMapCharacter__ctor_m53FE1564EB41AA1CE960C7571CBFEDA9E14B6735,
	LevelMapPathElement_CanGoUp_m1ADE0670DC5696D0CF682AF6358F448A8D230C56,
	LevelMapPathElement_CanGoRight_mE141BCE2495ED21BB77B1D1A6E89BBAB0C5A6407,
	LevelMapPathElement_CanGoDown_m918CEF6BA0991A242D98CCDC94451BD54DEF1099,
	LevelMapPathElement_CanGoLeft_m3FB80BE75E0C5A5AF592B72D749976C08338C6C7,
	LevelMapPathElement_Start_m8ABEC4640D4F01094DFDC31EEB2E85366D8B4BEF,
	LevelMapPathElement_OnTriggerStay2D_m6F7EA546A8DFB866A95F32C3E335F41CFA185C0D,
	LevelMapPathElement_OnTriggerExit2D_mDF3BB11711CD5EC59BD7450356027ABD8E0D8DF9,
	LevelMapPathElement_OnTriggerEnter2D_m4B2DA6ED322FF90AC270B46FA890442A5BD8DCFB,
	LevelMapPathElement_OnDrawGizmos_m170BE035B712DF066E8C2EB5936A3CA6998BA673,
	LevelMapPathElement__ctor_mAE02AF04C3FB0204EF09814117DA5D05789DF8BA,
	UncheckReverseGravityInput_OnTriggerEnter2D_m963D8CB6D83DCE51E0A9B2B06D6A2DF8D84DF48D,
	UncheckReverseGravityInput__ctor_m157A9AA2942E17B6BD1EF0559297EBAC84188477,
	RetroCopter_Initialization_mCB539802F71E0140B7068037F583B388981836DC,
	RetroCopter_ProcessAbility_mD5EBA674023BA4A28339893E4EC5D20687D4299D,
	RetroCopter__ctor_mFB3A9D8C6C5369BDD999CF9A4FF89E65C0A733CA,
	RetroDoor_Awake_m0AE22E765B0DF59C15D5A2FE260D24F29228FE16,
	RetroDoor__ctor_mBD851611EE3F48A13A7C792B731C55751B7FFAF6,
	RetroAdventureFinishLevel_GoToNextLevel_m4E372B402D42CB81BF5C2AC233802060A5A8F6A5,
	RetroAdventureFinishLevel__ctor_mE00FE3EDFD027B723B602F9DBE5C8E5408D2E61B,
	RetroAdventureGUIManager_Update_m535B6E047596ED9365DE4C0807B54585FD094781,
	RetroAdventureGUIManager_UpdateStars_mCCFD3A83212DF75B13A96F08F42E0C760E08960B,
	RetroAdventureGUIManager_LevelComplete_m33DFE5D7582F17210D62C4C8313D55EF0769A255,
	RetroAdventureGUIManager_OnMMEvent_m5813C52BDB5C6426DF724D4626A485E2921BB0E7,
	RetroAdventureGUIManager_OnEnable_mA69C59DD6FC22949285D4A6D543E9CEB4FBB0C00,
	RetroAdventureGUIManager_OnDisable_m7E4A376D7074CA96CAF9646EF3C89071812448CA,
	RetroAdventureGUIManager__ctor_mCEB46DF00172D3B7A2013E4D3B017F24EB5AA252,
	RetroAdventureLevel_GoToLevel_m0F5B54EA595A6D047F7298741688A5EA42EC8277,
	RetroAdventureLevel_Start_mD38C63C4C3098B5D161FD6DFB064301955D6FEF0,
	RetroAdventureLevel_InitialSetup_m0DC4B4FA1E2E13AE6CE25C4A75D903578C1B7DC4,
	RetroAdventureLevel__ctor_mD3D9CE8D5D720F3D4DD7A672F21ECAB2E4090950,
	RetroAdventureScene__ctor_mFFA7165F7D3FC78DD571B766BD87533FB8B4285F,
	Progress__ctor_m328101047C95644D26CFCC3D5B11384D08734D4F,
	RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB,
	RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223,
	RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52,
	RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C,
	RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC,
	RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56,
	RetroAdventureProgressManager_Awake_m297B452272540A6B67E70D2965D335BE73F95738,
	RetroAdventureProgressManager_LevelComplete_m269033BC6F6CB023B473ADDC31AFA23350E44362,
	RetroAdventureProgressManager_InitializeStars_m0AA36F967AC3A29EDECF74A195167FE1F35CF14A,
	RetroAdventureProgressManager_SaveProgress_mD5271DD17C484FB3A916D1C1426E98FC37431649,
	RetroAdventureProgressManager_CreateSaveGame_mB8949DA72B6E82FF2563B16D0C9412040B106242,
	RetroAdventureProgressManager_LoadSavedProgress_m0794BAACFEAC07D49704DA9A148F9B662B13EA0D,
	RetroAdventureProgressManager_OnMMEvent_mBD4A63105FFBB77336C3CA91494BE56AEEDCFC52,
	RetroAdventureProgressManager_OnMMEvent_mFD2CFF7F7D5A54D5977775A9F1D0B151EC2EE6E4,
	RetroAdventureProgressManager_GameOver_m0F44C908A39FB4A9B91FE9699E1DA563E27B8F11,
	RetroAdventureProgressManager_ResetLives_m5216E3EA0AF1A655F740AF028D0E121DB8C7ACE1,
	RetroAdventureProgressManager_ResetProgress_m4307B34F9DD80AF725E7C3AD51D9002364D15928,
	RetroAdventureProgressManager_OnEnable_mC654DC829247D18D50F7034358F5F0BE9E7F7B0A,
	RetroAdventureProgressManager_OnDisable_m1378CA6151546C02FC41D13F65BAD9BA383B0C4A,
	RetroAdventureProgressManager__ctor_mB3F495CE08D74760707ED4C5BE97746B96BF2B32,
	RetroNextLevel_NextLevel_m69D42A79000BD005B73C938B6C559BE114065C55,
	RetroNextLevel__ctor_m87098EEA866DE6507D4C779B02D15DDC8DA04D9F,
	RetroStar_Start_mF1EB43419CFC05D037CBEEB30E83533495EB727E,
	RetroStar_DisableIfAlreadyCollected_mB70C9C58119152E68436AEF45D4D329498DE89C2,
	RetroStar_Disable_m1FA46910EF55FCF7F6217F0FECBA338D561CFBFA,
	RetroStar__ctor_m51FCC9FB2918C70D1997D3B5F9F82331D9611305,
	Mushroom_CheckIfPickable_m4B5C49A62BD2D4D937959ADABF99C1ADA0C021E2,
	Mushroom_Pick_m00F6FA0FF0A52045F46377126EE885E7B9AA9279,
	Mushroom__ctor_m41FF2F1C0AACCC9AE564060D7F1820D7357054F6,
	SuperHipsterBrosHealth_Initialization_m6AC48ED265AD32EA71256AE63A00737508593C8E,
	SuperHipsterBrosHealth_Damage_mA9E773BA8F793D9E897E13D44234702BFB3208DA,
	SuperHipsterBrosHealth_Grow_m42039F01DB484C347D26BF1F562672F0D11B1C9F,
	SuperHipsterBrosHealth_Shrink_m83E2B699753362A96EB8D0991A635630E0731C48,
	SuperHipsterBrosHealth_ResetScale_m61B52A7BDBDB04396508823157B6207559B77188,
	SuperHipsterBrosHealth_Kill_m1B5B0969B2DDEF808EE1C7EAA52D16D9FB2291E7,
	SuperHipsterBrosHealth__ctor_m7F0074239E8783F7E7A70F5BFA71289CE0ECD12A,
};
static const int32_t s_InvokerIndices[89] = 
{
	3506,
	3506,
	3502,
	3458,
	3506,
	3473,
	2837,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	2839,
	2839,
	3506,
	3473,
	3473,
	3473,
	3473,
	3506,
	2815,
	2815,
	2815,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3424,
	2775,
	3424,
	2775,
	3476,
	2839,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2734,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	291,
	2839,
	2839,
	2839,
	3506,
	3506,
};
extern const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_Demos_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_CorgiEngine_Demos_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_CorgiEngine_Demos_CodeGenModule = 
{
	"MoreMountains.CorgiEngine.Demos.dll",
	89,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_MoreMountains_CorgiEngine_Demos_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

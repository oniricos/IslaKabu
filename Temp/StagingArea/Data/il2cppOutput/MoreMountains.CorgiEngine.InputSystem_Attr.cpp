﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
static void MoreMountains_CorgiEngine_InputSystem_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_CustomAttributesCacheGenerator_CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_0_m46ACF796CE6662C47F5F6F7DF3F93D66458546C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_1_m2F8C9BF720B21AFFEA59C51DAB9AFDB00F1784FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_2_m446C76D83D4632E0FF67A9592B6847E671D5080B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_3_m0FC1B7B16A1FA07223B493FFBC7EBA28B4AD20EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_4_m8BE231372655FFED47110BA4F1FD2C669F2F5269(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_5_m13CCF14FE346DE32BA377629591B961298E42B9A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_6_m7CDD3C382E16BDAA0879403900170E3A843BFB72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_7_mFEAC9CC38469CFB33F91332564BD32D2E6C698E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_8_m70C3E7F0E5D68F4B1FE4BE9C596A17D9B8A57CB7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_9_m0E5555C114C1B5F7E716B1C78E185CBC626491AB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_10_mEFDA6A719B33D59A1768E47E6B3D7E618BD56F40(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_11_m6F724C37CE09DD1C52D6D5636CCA616F99F74246(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_12_m0212D2D2FA4CBB4E27D20BCBE20F389F94C0E52B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_13_m00B1CE3F5790A3F641BED8A7254629F943AC4E05(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_14_m4DD059256DE4FED79F39CF0E3A0EE07672534B01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_15_m708B678A008CC12A1FE7A62E3133F528BCC5CEBE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_16_m0EF285474B25D28254204813684E18243A720237(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_17_m960D219A1C16790E3519F35094204360452DEAC9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_18_mDB44A20E9422F4F04200E018DD1AC60474420D29(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_19_m90E4403D1EE32D1A0991ABFDC802E3144BCC6958(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_InputSystem_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_InputSystem_AttributeGenerators[23] = 
{
	CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField,
	CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_CustomAttributesCacheGenerator_CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_0_m46ACF796CE6662C47F5F6F7DF3F93D66458546C7,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_1_m2F8C9BF720B21AFFEA59C51DAB9AFDB00F1784FC,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_2_m446C76D83D4632E0FF67A9592B6847E671D5080B,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_3_m0FC1B7B16A1FA07223B493FFBC7EBA28B4AD20EE,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_4_m8BE231372655FFED47110BA4F1FD2C669F2F5269,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_5_m13CCF14FE346DE32BA377629591B961298E42B9A,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_6_m7CDD3C382E16BDAA0879403900170E3A843BFB72,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_7_mFEAC9CC38469CFB33F91332564BD32D2E6C698E6,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_8_m70C3E7F0E5D68F4B1FE4BE9C596A17D9B8A57CB7,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_9_m0E5555C114C1B5F7E716B1C78E185CBC626491AB,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_10_mEFDA6A719B33D59A1768E47E6B3D7E618BD56F40,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_11_m6F724C37CE09DD1C52D6D5636CCA616F99F74246,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_12_m0212D2D2FA4CBB4E27D20BCBE20F389F94C0E52B,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_13_m00B1CE3F5790A3F641BED8A7254629F943AC4E05,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_14_m4DD059256DE4FED79F39CF0E3A0EE07672534B01,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_15_m708B678A008CC12A1FE7A62E3133F528BCC5CEBE,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_16_m0EF285474B25D28254204813684E18243A720237,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_17_m960D219A1C16790E3519F35094204360452DEAC9,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_18_mDB44A20E9422F4F04200E018DD1AC60474420D29,
	InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80_CustomAttributesCacheGenerator_InputSystemManager_U3CInitializationU3Eb__4_19_m90E4403D1EE32D1A0991ABFDC802E3144BCC6958,
	MoreMountains_CorgiEngine_InputSystem_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

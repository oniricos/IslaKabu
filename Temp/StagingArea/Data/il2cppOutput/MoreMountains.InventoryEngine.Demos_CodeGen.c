﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MoreMountains.InventoryEngine.ChangeLevel::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ChangeLevel_OnTriggerEnter2D_m67AD2E1AF1C24CC5F96E31388E422D3FF1500E3F (void);
// 0x00000002 System.Void MoreMountains.InventoryEngine.ChangeLevel::.ctor()
extern void ChangeLevel__ctor_m4ADEB4E4961811FAC5883AD7C6AE2A0D4325074B (void);
// 0x00000003 System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::Update()
extern void DemoCharacterInputManager_Update_m9BB79F4E358FED7BF871484794DA912919E3C186 (void);
// 0x00000004 System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::HandleDemoCharacterInput()
extern void DemoCharacterInputManager_HandleDemoCharacterInput_m33C8E9D8B77500B2B5AF5B40908DA48C4A106D79 (void);
// 0x00000005 System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void DemoCharacterInputManager_OnMMEvent_mA6245AA2C7033D3D7053A7A3ABB2A6AFDC522A1A (void);
// 0x00000006 System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::OnEnable()
extern void DemoCharacterInputManager_OnEnable_m49CB6CE44C92083F529FB22006A4A1273468153C (void);
// 0x00000007 System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::OnDisable()
extern void DemoCharacterInputManager_OnDisable_mA463380A74F5BB73F4D3EC6036441860B6D4B0EB (void);
// 0x00000008 System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::.ctor()
extern void DemoCharacterInputManager__ctor_m422D91BAA00864DB8AA8FF122F294880C678FE98 (void);
// 0x00000009 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Start()
extern void InventoryDemoCharacter_Start_mCD951E8673D555FDA81E30118106A6BFE5543C01 (void);
// 0x0000000A System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::FixedUpdate()
extern void InventoryDemoCharacter_FixedUpdate_m5AE4850AD5EEF88CC0D5D7FA991E28A70030F1F8 (void);
// 0x0000000B System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetMovement(System.Single,System.Single)
extern void InventoryDemoCharacter_SetMovement_m5D034E7B67BA41233F9D0CE5EEC04A09849B025E (void);
// 0x0000000C System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetHorizontalMove(System.Single)
extern void InventoryDemoCharacter_SetHorizontalMove_mF17CFCCA4581E038AF17A5D4CF385EA639C6D28B (void);
// 0x0000000D System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetVerticalMove(System.Single)
extern void InventoryDemoCharacter_SetVerticalMove_m04F2DC5D937813625162B83FBA99726F4606FFFD (void);
// 0x0000000E System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Movement()
extern void InventoryDemoCharacter_Movement_m4E55F8CC0A027B57E492FFEB43CC35736317A95B (void);
// 0x0000000F System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Flip()
extern void InventoryDemoCharacter_Flip_m115687C165D98FC93FFAB48F0812F6E3A17E4FCB (void);
// 0x00000010 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::UpdateAnimator()
extern void InventoryDemoCharacter_UpdateAnimator_m37AC18CEFEA8CF069D30560996216F45B356FE4E (void);
// 0x00000011 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetArmor(System.Int32)
extern void InventoryDemoCharacter_SetArmor_mE711C29C7211485F09C612978D8B254E80402342 (void);
// 0x00000012 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetWeapon(UnityEngine.Sprite,MoreMountains.InventoryEngine.InventoryItem)
extern void InventoryDemoCharacter_SetWeapon_m6C37DC70CF14F594880E251665A89C2682BF7CB7 (void);
// 0x00000013 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void InventoryDemoCharacter_OnMMEvent_m52139D753486835929148161DD14F74EDB8A3F4F (void);
// 0x00000014 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::OnEnable()
extern void InventoryDemoCharacter_OnEnable_m5FBF346FFF0DBA988296045D35E37E34BB4D49CF (void);
// 0x00000015 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::OnDisable()
extern void InventoryDemoCharacter_OnDisable_m6AA9CE64698F9A665D853E227FB0B8A8CDC01104 (void);
// 0x00000016 System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::.ctor()
extern void InventoryDemoCharacter__ctor_m2FDF6D2D9EBC89159EFDD2AF62B77CD00C808148 (void);
// 0x00000017 MoreMountains.InventoryEngine.InventoryDemoCharacter MoreMountains.InventoryEngine.InventoryDemoGameManager::get_Player()
extern void InventoryDemoGameManager_get_Player_m4F66F3841D163EABDB6D9A8D4B477BDE2D9B4664 (void);
// 0x00000018 System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::set_Player(MoreMountains.InventoryEngine.InventoryDemoCharacter)
extern void InventoryDemoGameManager_set_Player_m2C1B04A843CA953D8411C86ED48D110379ECA60C (void);
// 0x00000019 System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::Awake()
extern void InventoryDemoGameManager_Awake_m3BF5D7015E550E90C8F4344CEFCA4A76D49CB3CE (void);
// 0x0000001A System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::Start()
extern void InventoryDemoGameManager_Start_m5D699173F5EEA66D2173570F303F2949049F3877 (void);
// 0x0000001B System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::.ctor()
extern void InventoryDemoGameManager__ctor_m45D0B6413D1B48DC274E293293908631DD800842 (void);
// 0x0000001C System.Boolean MoreMountains.InventoryEngine.ArmorItem::Equip(System.String)
extern void ArmorItem_Equip_m93C78320601810881EBF57208FD6BD136D1625CF (void);
// 0x0000001D System.Boolean MoreMountains.InventoryEngine.ArmorItem::UnEquip(System.String)
extern void ArmorItem_UnEquip_m224E73B2F972313547D04B053EE67774AEB14C2F (void);
// 0x0000001E System.Void MoreMountains.InventoryEngine.ArmorItem::.ctor()
extern void ArmorItem__ctor_m2FFCA33D1749790CB8AFC9D0BB37E2354783BD55 (void);
// 0x0000001F System.Boolean MoreMountains.InventoryEngine.BombItem::Use(System.String)
extern void BombItem_Use_mD109113C34D83AFB0257290FC77D6E60B7328FB4 (void);
// 0x00000020 System.Void MoreMountains.InventoryEngine.BombItem::.ctor()
extern void BombItem__ctor_mB5903CEFC944A5BB52F61C07FC188ADF331D6846 (void);
// 0x00000021 System.Boolean MoreMountains.InventoryEngine.HealthBonusItem::Use(System.String)
extern void HealthBonusItem_Use_m1E665B43824EEFD3D34A834AF6F649AA90E17104 (void);
// 0x00000022 System.Void MoreMountains.InventoryEngine.HealthBonusItem::.ctor()
extern void HealthBonusItem__ctor_m97B12494BB16B98D4FD81E34D392121D42FB1DCD (void);
// 0x00000023 System.Boolean MoreMountains.InventoryEngine.WeaponItem::Equip(System.String)
extern void WeaponItem_Equip_m66786C93CEE9F0D9CE47C44391B83975BA9C3001 (void);
// 0x00000024 System.Boolean MoreMountains.InventoryEngine.WeaponItem::UnEquip(System.String)
extern void WeaponItem_UnEquip_m0DDD81BC4FC8A40C1A91CA5689E504C8653B7630 (void);
// 0x00000025 System.Void MoreMountains.InventoryEngine.WeaponItem::.ctor()
extern void WeaponItem__ctor_m860D6399E7572F7CD8932094A939F96224842873 (void);
static Il2CppMethodPointer s_methodPointers[37] = 
{
	ChangeLevel_OnTriggerEnter2D_m67AD2E1AF1C24CC5F96E31388E422D3FF1500E3F,
	ChangeLevel__ctor_m4ADEB4E4961811FAC5883AD7C6AE2A0D4325074B,
	DemoCharacterInputManager_Update_m9BB79F4E358FED7BF871484794DA912919E3C186,
	DemoCharacterInputManager_HandleDemoCharacterInput_m33C8E9D8B77500B2B5AF5B40908DA48C4A106D79,
	DemoCharacterInputManager_OnMMEvent_mA6245AA2C7033D3D7053A7A3ABB2A6AFDC522A1A,
	DemoCharacterInputManager_OnEnable_m49CB6CE44C92083F529FB22006A4A1273468153C,
	DemoCharacterInputManager_OnDisable_mA463380A74F5BB73F4D3EC6036441860B6D4B0EB,
	DemoCharacterInputManager__ctor_m422D91BAA00864DB8AA8FF122F294880C678FE98,
	InventoryDemoCharacter_Start_mCD951E8673D555FDA81E30118106A6BFE5543C01,
	InventoryDemoCharacter_FixedUpdate_m5AE4850AD5EEF88CC0D5D7FA991E28A70030F1F8,
	InventoryDemoCharacter_SetMovement_m5D034E7B67BA41233F9D0CE5EEC04A09849B025E,
	InventoryDemoCharacter_SetHorizontalMove_mF17CFCCA4581E038AF17A5D4CF385EA639C6D28B,
	InventoryDemoCharacter_SetVerticalMove_m04F2DC5D937813625162B83FBA99726F4606FFFD,
	InventoryDemoCharacter_Movement_m4E55F8CC0A027B57E492FFEB43CC35736317A95B,
	InventoryDemoCharacter_Flip_m115687C165D98FC93FFAB48F0812F6E3A17E4FCB,
	InventoryDemoCharacter_UpdateAnimator_m37AC18CEFEA8CF069D30560996216F45B356FE4E,
	InventoryDemoCharacter_SetArmor_mE711C29C7211485F09C612978D8B254E80402342,
	InventoryDemoCharacter_SetWeapon_m6C37DC70CF14F594880E251665A89C2682BF7CB7,
	InventoryDemoCharacter_OnMMEvent_m52139D753486835929148161DD14F74EDB8A3F4F,
	InventoryDemoCharacter_OnEnable_m5FBF346FFF0DBA988296045D35E37E34BB4D49CF,
	InventoryDemoCharacter_OnDisable_m6AA9CE64698F9A665D853E227FB0B8A8CDC01104,
	InventoryDemoCharacter__ctor_m2FDF6D2D9EBC89159EFDD2AF62B77CD00C808148,
	InventoryDemoGameManager_get_Player_m4F66F3841D163EABDB6D9A8D4B477BDE2D9B4664,
	InventoryDemoGameManager_set_Player_m2C1B04A843CA953D8411C86ED48D110379ECA60C,
	InventoryDemoGameManager_Awake_m3BF5D7015E550E90C8F4344CEFCA4A76D49CB3CE,
	InventoryDemoGameManager_Start_m5D699173F5EEA66D2173570F303F2949049F3877,
	InventoryDemoGameManager__ctor_m45D0B6413D1B48DC274E293293908631DD800842,
	ArmorItem_Equip_m93C78320601810881EBF57208FD6BD136D1625CF,
	ArmorItem_UnEquip_m224E73B2F972313547D04B053EE67774AEB14C2F,
	ArmorItem__ctor_m2FFCA33D1749790CB8AFC9D0BB37E2354783BD55,
	BombItem_Use_mD109113C34D83AFB0257290FC77D6E60B7328FB4,
	BombItem__ctor_mB5903CEFC944A5BB52F61C07FC188ADF331D6846,
	HealthBonusItem_Use_m1E665B43824EEFD3D34A834AF6F649AA90E17104,
	HealthBonusItem__ctor_m97B12494BB16B98D4FD81E34D392121D42FB1DCD,
	WeaponItem_Equip_m66786C93CEE9F0D9CE47C44391B83975BA9C3001,
	WeaponItem_UnEquip_m0DDD81BC4FC8A40C1A91CA5689E504C8653B7630,
	WeaponItem__ctor_m860D6399E7572F7CD8932094A939F96224842873,
};
static const int32_t s_InvokerIndices[37] = 
{
	2815,
	3506,
	3506,
	3506,
	2798,
	3506,
	3506,
	3506,
	3506,
	3506,
	1728,
	2839,
	2839,
	3506,
	3506,
	3506,
	2775,
	1695,
	2798,
	3506,
	3506,
	3506,
	3445,
	2815,
	3506,
	3506,
	3506,
	2427,
	2427,
	3506,
	2427,
	3506,
	2427,
	3506,
	2427,
	2427,
	3506,
};
extern const CustomAttributesCacheGenerator g_MoreMountains_InventoryEngine_Demos_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_InventoryEngine_Demos_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_InventoryEngine_Demos_CodeGenModule = 
{
	"MoreMountains.InventoryEngine.Demos.dll",
	37,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_MoreMountains_InventoryEngine_Demos_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emision2 : MonoBehaviour
{
    public GameObject player;
    Rigidbody2D rb2d;
    Animator anim;
    Collider2D coll;

    bool hit = false;

    float contador;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        rb2d = this.GetComponent<Rigidbody2D>();
        coll = this.GetComponent<Collider2D>();
    }
    private void Update()
    {
        if (!hit)
        {
            rb2d.velocity = Vector2.left * 5;
        }
        contador += Time.deltaTime;

        if (contador > 2.5f)
        {
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            hit = true;
            rb2d.velocity = Vector2.zero;
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
        if (collision.gameObject.tag == "Bala")
        {
            hit = true;
            rb2d.velocity = Vector2.zero;
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
        if (collision.gameObject.tag == "Platforms")
        {
            hit = true;
            try
            {
                rb2d.velocity = Vector2.zero;
            }
            catch
            {
                Debug.Log("No rb");
            }
            rb2d.velocity = Vector2.zero;
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
    }
    public void Explosion()
    {
        Destroy(this.gameObject);
    }
}

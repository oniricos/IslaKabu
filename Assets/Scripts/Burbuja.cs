using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burbuja : MonoBehaviour
{
    Rigidbody2D rb2d;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

        StartCoroutine(MoverIzqDr());
    }

    IEnumerator MoverIzqDr()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            rb2d.velocity = Vector2.right * 0.5f;
            yield return new WaitForSeconds(1f);
            rb2d.velocity = Vector2.left * 0.5f;
        }
    }
}

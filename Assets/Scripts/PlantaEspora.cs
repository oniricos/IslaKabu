using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantaEspora : MonoBehaviour
{
    public GameObject plataformaEspora;
    GameObject spawn;
    public float tiempoSpawn;
    Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        spawn = this.transform.GetChild(0).gameObject;
        StartCoroutine(InstanciaEsporas());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator InstanciaEsporas()
    {
        while (true)
        {
            yield return new WaitForSeconds(tiempoSpawn);
            anim.SetTrigger("Dispara");
        }
    }
    public void Instancia()
    {
        Instantiate(plataformaEspora, spawn.transform.position, Quaternion.identity);
    }
}

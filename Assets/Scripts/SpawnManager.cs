using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] puntoSpawn;

    GameObject player;

    bool spawneado;

    private void Start()
    {
        spawneado = false;
    }

    void Update()
    {
        if (!player)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            catch
            {
                Debug.Log("No hay player");
            }
        }
        if (player && !spawneado)
        {
            if (PlayerPrefs.GetInt("Spawn") == 0)
            {
                spawneado = true;
                return;
            }
            else if (PlayerPrefs.GetInt("Spawn") == 1)
            {
                player.transform.position = puntoSpawn[0].transform.position;
                spawneado = true;
                return;
            }
            else if (PlayerPrefs.GetInt("Spawn") == 2)
            {
                player.transform.position = puntoSpawn[1].transform.position;
                spawneado = true;
                return;
            }
            else if (PlayerPrefs.GetInt("Spawn") == 3)
            {
                player.transform.position = puntoSpawn[2].transform.position;
                spawneado = true;
                return;
            }
        }
    }
    public void RestablecerSpawn()
    {
        PlayerPrefs.SetInt("Spawn", 0);
    }
}

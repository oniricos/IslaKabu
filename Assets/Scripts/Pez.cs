using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pez : MonoBehaviour
{
    Rigidbody2D rb2D;

    public float fuerza;

    public float tiempoSalto;

    GameObject pezFuego;


    bool enBucle;

    Collider2D coll;

    SpriteRenderer sr;

    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

        sr = GetComponent<SpriteRenderer>();

        coll = GetComponent<Collider2D>();

        anim = GetComponent<Animator>();

        pezFuego = transform.GetChild(0).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (coll.isTrigger == true)
        {
            coll.isTrigger = false;
        }

        if (!enBucle)
        {
            StartCoroutine(Salta());
        }

        if (rb2D.velocity.y > 0)
        {
            sr.flipY = false;
            pezFuego.GetComponent<SpriteRenderer>().flipY = true;
        }
        if (rb2D.velocity.y < 0)
        {
            sr.flipY = true;
            pezFuego.GetComponent<SpriteRenderer>().flipY = false;
        }
    }
    IEnumerator Salta()
    {
        enBucle = true;

        rb2D.AddForce(Vector2.up * fuerza, ForceMode2D.Impulse);
        yield return new WaitForSeconds(tiempoSalto);

        enBucle = false;
    }
}

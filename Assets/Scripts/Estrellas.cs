using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Estrellas : MonoBehaviour
{
    public GameObject star1, star2, star3;
    public int Estrellas1, Estrellas3;
    bool anim1, anim2, anim3;
    public bool ejecutar = false;
    bool activarboton = false;
    AudioSource audioS;
    [SerializeField] AudioClip sonidoEstrella;
    [SerializeField] GameObject texto;
    [SerializeField] string sceneToLoad;
    // Start is called before the first frame update
    void Start()
    {
        audioS = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        //audioS.clip = sonidoEstrella;
    }

    // Update is called once per frame
    void Update()
    {
        contadorAerena contador = GameObject.FindGameObjectWithTag("Contador").GetComponent<contadorAerena>();
        //activar animacion estrellas
        if (contador.VerPuntos() >= Estrellas3)
        {
            anim3 = true;
        }

        else if (contador.VerPuntos() <= Estrellas1)
        {
            //Animacion 1 Estrella
            anim1 = true;
        }

        else if (contador.VerPuntos() > Estrellas1 && contador.VerPuntos() < Estrellas3)
        {
            anim2 = true;
        }


        if (ejecutar)
        {
            StartCoroutine(Star());
            //activarboton = true;
        }


        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Joystick1Button0)) && activarboton)
        {
            PlayerPrefs.SetInt("Spawn", 0);
            SceneManager.LoadScene(sceneToLoad);
        }
    }
    public IEnumerator Star()
    {
        ejecutar = false;
        audioS.clip = sonidoEstrella;
        if (anim1 == true)
        {
            yield return new WaitForSeconds(0.2f);
            if (!audioS.isPlaying)
            {
                audioS.PlayOneShot(sonidoEstrella);
            }
            star1.SetActive(true);
            star2.SetActive(false);
            star3.SetActive(false);
        }
        else if (anim2 == true)
        {
            yield return new WaitForSeconds(0.2f);
            if (!audioS.isPlaying)
            {
                audioS.PlayOneShot(sonidoEstrella);
            }
            //audioS.Play();
            //audioS.clip = null;
            star1.SetActive(true);
            yield return new WaitForSeconds(1f);
            if (!audioS.isPlaying)
            {
                audioS.PlayOneShot(sonidoEstrella);
            }
            //audioS.Play();
            //audioS.clip = null;
            star2.SetActive(true);
            star3.SetActive(false);
        }
        else if (anim3 == true)
        {
            yield return new WaitForSeconds(0.2f);
            if (!audioS.isPlaying)
            {
                audioS.PlayOneShot(sonidoEstrella);
            }
            //audioS.PlayOneShot(sonidoEstrella);
            star1.SetActive(true);
            yield return new WaitForSeconds(1f);
            if (!audioS.isPlaying)
            {
                audioS.PlayOneShot(sonidoEstrella);
            }
            //audioS.PlayOneShot(sonidoEstrella);
            star2.SetActive(true);
            yield return new WaitForSeconds(1f);
            if (!audioS.isPlaying)
            {
                audioS.PlayOneShot(sonidoEstrella);
            }
            //audioS.PlayOneShot(sonidoEstrella);
            star3.SetActive(true);
        }
        //ejecutar = false;
        yield return new WaitForSeconds(0.5f);
        texto.SetActive(true);
        activarboton = true;
        yield break;
    }
}

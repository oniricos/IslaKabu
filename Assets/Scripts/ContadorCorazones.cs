using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class ContadorCorazones : MonoBehaviour
{
    public GameObject corazon1, corazon2, corazon3;
    Health player;
    int corazones;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
            }
            catch
            {
                Debug.Log("no player");
            }
        }
        corazones = player.CurrentHealth;

        if (corazones == 3)
        {
            corazon1.SetActive(true);
            corazon2.SetActive(true);
            corazon3.SetActive(true);
        }
        else if (corazones == 2)
        {
            corazon1.SetActive(true);
            corazon2.SetActive(true);
            corazon3.SetActive(false);
        }
        else if (corazones == 1)
        {
            corazon1.SetActive(true);
            corazon2.SetActive(false);
            corazon3.SetActive(false);
        }
        else
        {
            corazon1.SetActive(false);
            corazon2.SetActive(false);
            corazon3.SetActive(false);
        }
    }
}

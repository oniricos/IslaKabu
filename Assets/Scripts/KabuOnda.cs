using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KabuOnda : MonoBehaviour
{
    Rigidbody2D rb2d;
    AudioSource Source;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

        Source = GetComponent<AudioSource>();

        StartCoroutine(MoverIzqDr());
    }

    IEnumerator MoverIzqDr()
    {
        while (true)
        {
            yield return new WaitForSeconds(2);
            rb2d.velocity = Vector2.down * 1f;
            yield return new WaitForSeconds(2);
            rb2d.velocity = Vector2.up * 1f;
        }
    }

    public void Sonido()
    {
        //Source.Play();
    }
}

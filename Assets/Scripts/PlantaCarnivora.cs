using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantaCarnivora : MonoBehaviour
{
    float tiempoInactivo = 3f;
    float tiempoAfuera = 5f;

    Animator anim;

    IEnumerator Plantacarnivora()
    {
        while (true)
        {
            yield return new WaitForSeconds(tiempoInactivo);
            anim.SetBool("Salir", true);
            yield return new WaitForSeconds(tiempoAfuera);
            anim.SetBool("Salir", false);
        }
    }

    private void Start()
    {
        anim = this.gameObject.GetComponent<Animator>();
        StartCoroutine(Plantacarnivora());
    }
}

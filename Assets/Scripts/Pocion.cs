using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class Pocion : MonoBehaviour
{
    Collider2D coll;
    SpriteRenderer sR;
    AudioSource source;
    public AudioClip clip;
    private void Awake()
    {
        sR = GetComponent<SpriteRenderer>();
        coll = GetComponent<Collider2D>();
        source = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            source.PlayOneShot(clip);
            Health playerHealth = collision.gameObject.GetComponent<Health>();
            if (playerHealth.CurrentHealth <= 2)
            {
                coll.enabled = false;
                sR.enabled = false;
                collision.gameObject.GetComponent<Health>().CurrentHealth++;
                Destroy(this.gameObject, 3);
            }
            else
            {
                coll.enabled = false;
                sR.enabled = false;
                //Debug.Log("3 de vida");
                Destroy(this.gameObject, 3);
            }
        }
    }
}

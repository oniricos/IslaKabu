using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBurbujas : MonoBehaviour
{
    public GameObject burbuja;
    public float min, max;
    public float tiempoSpawn;
    private void Start()
    {
        Instantiate(burbuja, new Vector3(Random.Range(min, max), transform.position.y, 0), Quaternion.identity);

        StartCoroutine(InstanciaVoladores());
    }
    IEnumerator InstanciaVoladores()
    {
        while (true)
        {
            yield return new WaitForSeconds(tiempoSpawn);

            Instantiate(burbuja, new Vector3(Random.Range(min, max), transform.position.y, transform.position.z), Quaternion.identity);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoKabu : MonoBehaviour
{
    public Vector3 direccion;
    public float velocidad;

    void Update()
    {
        transform.Translate(direccion * velocidad * Time.deltaTime, Space.World);
    }
}

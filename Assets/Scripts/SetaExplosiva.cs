using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class SetaExplosiva : MonoBehaviour
{
    public float distDeteccion;
    public float velocidad;
    public float tiempoCaminar;

    public LayerMask layer;
    public GameObject explosionEsporas;

    bool playerDetectado = false;
    bool primeraDeteccion;
    bool bucle = false;

    bool moverDr;
    bool moverIzq;
    bool noMover;
    bool flip;

    Rigidbody2D rb2D;
    SpriteRenderer sr;
    Animator anim;
    Health health;
    AudioSource audio;

    [SerializeField] AudioClip[] clip;

    private void Awake()
    {
        rb2D = this.gameObject.GetComponent<Rigidbody2D>();
        sr = this.gameObject.GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        health = GetComponent<Health>();
        audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        RaycastHit2D hitL = Physics2D.Raycast(transform.position, Vector2.left, distDeteccion, layer);
        RaycastHit2D hitR = Physics2D.Raycast(transform.position, Vector2.right, distDeteccion, layer);
        //Collider2D hit = Physics2D.OverlapCircle(transform.position, radioDeteccion, layer);

        if ((hitL || hitR) && !playerDetectado)
        {
            try
            {
                GetComponent<MovimientoSeta>().enabled = false;
            }
            catch
            {
                Debug.Log("Continuar");
            }
            playerDetectado = true;
        }

        if (playerDetectado)
        {
            if ((hitL || hitR) && !primeraDeteccion)
            {
                float aux;
                try
                {
                    Vector2 dist = hitL.transform.position - this.transform.position;
                    aux = dist.x;
                }
                catch (System.Exception)
                {
                    Vector2 dist = hitR.transform.position - this.transform.position;
                    aux = dist.x;
                }
                //Vector2 dist = hitL.transform.position - this.transform.position;
                //aux = dist.x;

                if (aux < 0)
                {
                    moverIzq = true;
                }
                else if (aux > 0)
                {
                    moverDr = true;
                }
                else
                {
                    noMover = true;
                }

                primeraDeteccion = true;
            }
        }

        if (moverDr && !bucle)
        {
            flip = true;
            StartCoroutine(MoveExpl(Vector2.right));
        }
        else if (moverIzq && !bucle)
        {
            flip = false;
            StartCoroutine(MoveExpl(Vector2.left));
        }
        else if (noMover && !bucle)
        {
            StartCoroutine(MoveExpl(Vector2.zero));
        }

        if (!flip)
        {
            sr.flipX = false;
        }
        else
        {
            sr.flipX = true;
        }
    }

    IEnumerator MoveExpl(Vector2 dir)
    {
        bucle = true;

        if (health.CurrentHealth <= 0)
        {
            yield break;
        }

        audio.PlayOneShot(clip[0]);
        anim.SetBool("Correr", true);
        rb2D.velocity = dir * velocidad;
        yield return new WaitForSeconds(1f);
        rb2D.velocity = Vector2.zero;

        yield return new WaitForSeconds(0.01f);

        anim.SetBool("Correr", false);
        anim.SetBool("Explotar", true);

        //animacion explosion
    }

    public void Explosion()
    {
        audio.PlayOneShot(clip[1]);
        Instantiate(explosionEsporas, transform.position, Quaternion.identity);
        Destroy(this.gameObject, 1.5f);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawRay(this.transform.position, Vector2.left * distDeteccion);
        Gizmos.DrawRay(this.transform.position, Vector2.right * distDeteccion);
    }
}

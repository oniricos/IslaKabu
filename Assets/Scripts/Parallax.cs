using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    Vector3 positionP;
    public float velocityImg;
    public float anchoImg; //20 el tama�o de la imagen
    // Start is called before the first frame update
    void Start()
    {
        positionP = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        float resto = Mathf.Repeat(velocityImg * Time.time, anchoImg);
        transform.position = positionP + Vector3.right * resto; 
    }
}

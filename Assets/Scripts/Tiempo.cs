using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Feedbacks;

public class Tiempo : MonoBehaviour
{
    //GameObject pausa;
    MMTimeManager time;
    private void Awake()
    {
        //pausa = GameObject.FindGameObjectWithTag("Pausa");

        //pausa.SetActive(false);

        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
        time = GetComponent<MMTimeManager>();
        time.CurrentTimeScale = 1;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (time.CurrentTimeScale == 0)
        {
            time.CurrentTimeScale = 1; //boton de salir

        }
        time.CurrentTimeScale = 1;
        //time.CurrentTimeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Escape))
        //{
        //    try
        //    {
        //        pausa.SetActive(true);
        //    }
        //    catch
        //    {
        //        Debug.Log("No se puede");
        //    }
        //}
    }
}

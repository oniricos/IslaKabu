using MoreMountains.CorgiEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlanta : MonoBehaviour
{
    [field: SerializeField] private GameObject puntoSpawn;

    private GameObject carnivora;

    public GameObject prefabCarnivora;

    bool spawneando;

    private void Start()
    {
        carnivora = Instantiate(prefabCarnivora, puntoSpawn.transform.position, Quaternion.identity);
    }

    private void Update()
    {
        if (carnivora)
        {
            if (carnivora.GetComponent<Health>().CurrentHealth <= 0)
            {
                carnivora = null;

                return;
            }
        }

        CheckIfIsNull();
    }

    private void CheckIfIsNull()
    {
        //Debug.Log(carnivora);

        if (carnivora == null && !spawneando)
        {
            spawneando = true;
            StartCoroutine(Spawn());
        }
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(4f);

        carnivora = Instantiate(prefabCarnivora, puntoSpawn.transform.position, Quaternion.identity);

        spawneando = false;
    }
}

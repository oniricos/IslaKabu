using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class Victoria : MonoBehaviour
{
    public GameObject canvasVictoria;
    GameObject canvasDerrota;
    GameObject huevo;
    ContadorFinal contadorFinal;
    AudioSource audioS;
    AudioSource playerAudio;
    public AudioClip[] sonidos;
    Health saludPlayer;
    Animator anim;
    bool enElAire = false;
    public GameObject animacionFinal;

    // Start is called before the first frame update
    void Awake()
    {
        audioS = GameObject.FindGameObjectWithTag("MainCamera").gameObject.GetComponent<AudioSource>();
        playerAudio = GetComponent<AudioSource>();
        contadorFinal = GameObject.FindGameObjectWithTag("CFinal").GetComponent<ContadorFinal>();
        canvasVictoria = GameObject.FindGameObjectWithTag("Victoria");
        canvasDerrota = GameObject.FindGameObjectWithTag("Derrota");
        huevo = GameObject.Find("Huevo");
        canvasVictoria.SetActive(false);
        canvasDerrota.SetActive(false);
        saludPlayer = GetComponent<Health>();
        anim = GetComponent<Animator>();


        animacionFinal = GameObject.FindGameObjectWithTag("AnimFinal");
        animacionFinal.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (saludPlayer.CurrentHealth <= 0 && !enElAire)
        {
            Camera.main.GetComponent<CameraController>().StopFollowing();

            gameObject.GetComponent<Character>().enabled = false;
            gameObject.GetComponent<CorgiController>().enabled = false;
            gameObject.GetComponent<CharacterHorizontalMovement>().enabled = false;
            gameObject.GetComponent<CharacterJump>().enabled = false;
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            anim.SetBool("Death", true);

        }
        if (!canvasVictoria)
        {
            try
            {
                canvasVictoria = GameObject.FindGameObjectWithTag("Victoria");
            }
            catch
            {
                Debug.Log("No hay canvas");
            }
        }

        if (!animacionFinal)
        {
            try
            {
                animacionFinal = GameObject.FindGameObjectWithTag("AnimFinal");
            }
            catch
            {
                Debug.Log("No hay animacion Final");
            }
        }
    }
    public void Huevo()
    {
        huevo.SetActive(false);
    }
    public void ActivarAudio()
    {
        try
        {
            audioS.loop = false;
            audioS.clip = sonidos[0];
            audioS.PlayOneShot(sonidos[0]);
        }
        catch
        {
            return;
        }

        audioS = null;
    }
    public void Victory()
    {
        Camera.main.GetComponent<CameraController>().StopFollowing();

        //this.gameObject.SetActive(false);

        gameObject.GetComponent<Character>().enabled = false;
        gameObject.GetComponent<CorgiController>().enabled = false;
        gameObject.GetComponent<CharacterHorizontalMovement>().enabled = false;
        gameObject.GetComponent<CharacterJump>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;

        animacionFinal.SetActive(true);

        //canvasVictoria.SetActive(true);

        //contadorFinal.StartCoroutine(contadorFinal.EjecutarSuma());
    }

    void ActivarCanvas()
    {
        canvasVictoria.SetActive(true);
        contadorFinal.StartCoroutine(contadorFinal.EjecutarSuma());
    }
    public void DeathCanvas()
    {
        Camera.main.GetComponent<CameraController>().StopFollowing();

        this.gameObject.SetActive(false);

        gameObject.GetComponent<Character>().enabled = false;
        gameObject.GetComponent<CorgiController>().enabled = false;
        gameObject.GetComponent<CharacterHorizontalMovement>().enabled = false;
        gameObject.GetComponent<CharacterJump>().enabled = false;
        gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

        try
        {
            audioS.loop = false;
            audioS.clip = sonidos[1];
            audioS.PlayOneShot(sonidos[1]);
        }
        catch
        {
            return;
        }
        
        audioS = null;
        canvasDerrota.SetActive(true);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            //Debug.Log("En el suelo");
            enElAire = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Suelo")
        {
            //Debug.Log("En el aire");
            enElAire = true;
        }
    }
    public void EjecutarSonidoRecoger()
    {
        playerAudio.clip = sonidos[2];
        playerAudio.PlayOneShot(sonidos[2]);
    }
    public void EjecutarSonidoExplosion()
    {
        playerAudio.clip = sonidos[3];
        playerAudio.PlayOneShot(sonidos[3]);
    }
}

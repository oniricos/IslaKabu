using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class Enredadera : MonoBehaviour
{
    public CharacterHorizontalMovement horizontalMovement;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            horizontalMovement = collision.gameObject.GetComponent<CharacterHorizontalMovement>();
            horizontalMovement.MovementSpeed = 1;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            horizontalMovement = collision.gameObject.GetComponent<CharacterHorizontalMovement>();
            horizontalMovement.MovementSpeed = 6;
        }
    }
}

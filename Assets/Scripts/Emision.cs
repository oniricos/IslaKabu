using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emision : MonoBehaviour
{
    public GameObject player;
    Rigidbody2D rb2d;
    Animator anim;
    Vector3 dir;
    Collider2D coll;

    bool hit = false;

    float contador;
    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        //player = GameObject.FindGameObjectWithTag("Kabu");
        rb2d = this.GetComponent<Rigidbody2D>();
        coll = this.GetComponent<Collider2D>();
        //dir = transform.position - new Vector3 (player.transform.position.x , transform.position.y, transform.position.z);
    }
    private void Update()
    {

        if (!hit)
        {
            rb2d.velocity = Vector2.right * 5;
        }
        contador += Time.deltaTime;
        //rb2d.velocity = dir.normalized * 5;

        if (contador > 2.5f)
        {
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            hit = true;
            rb2d.velocity = Vector2.zero;
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
        if (collision.gameObject.tag == "Bala")
        {
            hit = true;
            rb2d.velocity = Vector2.zero;
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
        if (collision.gameObject.tag == "Platforms")
        {
            hit = true;
            try
            {
                rb2d.velocity = Vector2.zero;
            }
            catch
            {
                Debug.Log("No rb");
            }
            rb2d.velocity = Vector2.zero;
            coll.enabled = false;
            rb2d.velocity = Vector2.zero;
            anim.SetBool("Explosion", true);
        }
    }
    public void Explosion()
    {
        Destroy(this.gameObject);
    }
}

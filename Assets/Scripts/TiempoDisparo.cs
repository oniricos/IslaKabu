using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Tools;

public class TiempoDisparo : MonoBehaviour
{
    CharacterHandleWeapon handleWeapon;
    ProjectileWeapon weapon;
    MMSimpleObjectPooler objectPooler;
    GameObject feedback;

    float timer;
    float timer2;
    float timeToShoot = 1.2f;
    float tiempoDisparo = 1f;
    bool canShoot = true;
    
    bool iniciarTimer = false;
    bool tiempoEntreDisparo;
    bool timerIniciado;
    bool dentro;

    private void Awake()
    {
        //feedback = GameObject.FindGameObjectWithTag("Feedback");
        //weapon = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<ProjectileWeapon>();
        //handleWeapon = this.gameObject.GetComponent<CharacterHandleWeapon>();
    }
    // Start is called before the first frame update
    void Start()
    {
        weapon = null;
        objectPooler = null;
        feedback = null;
        //feedback = GameObject.FindGameObjectWithTag("Feedback");
        //weapon = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<ProjectileWeapon>();
        //objectPooler = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<MMSimpleObjectPooler>();
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(weapon);
        if (weapon == null)
        {
            try
            {
                weapon = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<ProjectileWeapon>();
            }
            catch
            {
                Debug.Log("No hay arma");
            }
            //weapon = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<ProjectileWeapon>();
        }
        if (objectPooler == null)
        {
            try
            {
                objectPooler = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<MMSimpleObjectPooler>();
            }
            catch
            {
                Debug.Log("No hay pooler");
            }
            //objectPooler = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<MMSimpleObjectPooler>();
        }
        if (feedback == null)
        {
            try
            {
                feedback = GameObject.FindGameObjectWithTag("Feedback");
            }
            catch
            {
                Debug.Log("No hay arma");
            }
        }
        
        if (Input.GetMouseButtonDown(0) && canShoot && weapon != null && objectPooler != null && feedback != null && !dentro)
        {
            dentro = true;
            Debug.Log("disparo");
            if (!timerIniciado)
            {
                timerIniciado = true;
                iniciarTimer = true;
            }
        }

        if (iniciarTimer)
        {
            timer += Time.deltaTime;
        }
        else if (!iniciarTimer)
        {
            timer = 0;
        }
     
        if (tiempoEntreDisparo)
        {
            timer2 += Time.deltaTime;
        }
        else if (!tiempoEntreDisparo)
        {
            timer2 = 0;
        }

        if (timer > timeToShoot)
        {
            canShoot = false;
            tiempoEntreDisparo = true;
        }

        if (timer2 > tiempoDisparo)
        {
            tiempoEntreDisparo = false;
            iniciarTimer = false;
            canShoot = true;
            timerIniciado = false;
            dentro = false;
        }


        if (!canShoot && feedback)
        {
            feedback.SetActive(false);
            objectPooler.enabled = false;
            weapon.enabled = false;
        }
        else if(canShoot && feedback)
        {
            feedback.SetActive(true);
            objectPooler.enabled = true;
            weapon.enabled = true;
        }
    }
    private void LateUpdate()
    {
        /*
        if (weapon == null)
        {
            try
            {
                weapon = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<ProjectileWeapon>();
            }
            catch
            {
                Debug.Log("No hay arma");
            }
            //weapon = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<ProjectileWeapon>();
        }
        if (objectPooler == null)
        {
            try
            {
                objectPooler = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<MMSimpleObjectPooler>();
            }
            catch
            {
                Debug.Log("No hay pooler");
            }
            //objectPooler = GameObject.FindGameObjectWithTag("Arma").gameObject.GetComponent<MMSimpleObjectPooler>();
        }
        if (feedback == null)
        {
            try
            {
                feedback = GameObject.FindGameObjectWithTag("Feedback");
            }
            catch
            {
                Debug.Log("No hay arma");
            }
        }*/
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorFinal : MonoBehaviour
{
    static int puntos = 0;
    int puntosContador;
    Text text;
    Estrellas stars;
    AudioSource audioS;
    public AudioClip clip;
    private void Awake()
    {
        stars = GameObject.FindGameObjectWithTag("Estrellas").GetComponent<Estrellas>();
        audioS = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        text = this.gameObject.GetComponent<Text>();
        puntos = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(puntosContador);
        puntosContador = GameObject.FindGameObjectWithTag("Contador").GetComponent<contadorAerena>().VerPuntos();
    }
    public IEnumerator EjecutarSuma()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < puntosContador + 1; i++)
        {
            audioS.PlayOneShot(clip);
            puntos = i;
            text.text = "" + puntos;
            yield return new WaitForSeconds(0.03f);
        }
        GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().SetBool("Huevo", false);
        GameObject.FindGameObjectWithTag("Player").SetActive(false);
        stars.ejecutar = true;
        yield break;
    }
}

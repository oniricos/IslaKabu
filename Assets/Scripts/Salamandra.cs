using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salamandra : MonoBehaviour
{
    GameObject player;

    public GameObject llamaIzq;
    public GameObject llamaDrc;

    Rigidbody2D rb2D;

    public float fuerza;

    bool enBucle;

    Collider2D coll;
    SpriteRenderer sr;

    Animator anim;

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();

        sr = GetComponent<SpriteRenderer>();

        coll = GetComponent<Collider2D>();

        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (coll.isTrigger == true)
        {
            coll.isTrigger = false;
        }

        if (!enBucle)
        {
            StartCoroutine(DisparaSalta());
        }
    }
    IEnumerator DisparaSalta()
    {
        enBucle = true;

        Debug.Log("salta izq");   // IZQUIERDa---------------------------------
        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", true);
        sr.flipX = false;
        rb2D.AddForce(Vector2.up * fuerza * 1.2f + Vector2.left * fuerza, ForceMode2D.Impulse);
        yield return new WaitForSeconds(4f);
        Debug.Log("Dispara izq");

        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", false);
        anim.SetBool("Shoot", true);

        yield return new WaitForSeconds(0.5f);
        llamaIzq.SetActive(true);
        rb2D.isKinematic = false;
        yield return new WaitForSeconds(3);


        anim.SetBool("Quieto", true);
        anim.SetBool("Shoot", false);
        anim.SetBool("Jump", false);
        llamaIzq.SetActive(false);
        Debug.Log("Para");
        rb2D.velocity = Vector2.zero;
        yield return new WaitForSeconds(1f);

        Debug.Log("salta izq");   // IZQUIERDa---------------------------------
        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", true);
        sr.flipX = false;
        rb2D.AddForce(Vector2.up * fuerza * 1.2f + Vector2.left * fuerza, ForceMode2D.Impulse);
        yield return new WaitForSeconds(4f);
        Debug.Log("Dispara izq");

        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", false);
        anim.SetBool("Shoot", true);

        yield return new WaitForSeconds(0.5f);
        llamaIzq.SetActive(true);
        rb2D.isKinematic = false;
        yield return new WaitForSeconds(3);


        anim.SetBool("Quieto", true);
        anim.SetBool("Shoot", false);
        anim.SetBool("Jump", false);
        llamaIzq.SetActive(false);
        Debug.Log("Para");
        rb2D.velocity = Vector2.zero;
        yield return new WaitForSeconds(1f);

        Debug.Log("Salta dr");            //DERECHA--------------------------
        anim.SetBool("Jump", true);
        anim.SetBool("Quieto", false);
        sr.flipX = true;
        rb2D.AddForce(Vector2.up * fuerza * 1.2f + Vector2.right * fuerza, ForceMode2D.Impulse);
        yield return new WaitForSeconds(4f);
        Debug.Log("Dispara derecha");

        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", false);
        anim.SetBool("Shoot", true);

        yield return new WaitForSeconds(0.5f);
        llamaDrc.SetActive(true);
        rb2D.isKinematic = false;
        yield return new WaitForSeconds(3);


        anim.SetBool("Quieto", true);
        anim.SetBool("Shoot", false);
        anim.SetBool("Jump", true);
        llamaDrc.SetActive(false);
        Debug.Log("Para");
        rb2D.velocity = Vector2.zero;
        yield return new WaitForSeconds(1f);

        Debug.Log("Salta dr");            //DERECHA--------------------------
        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", true);
        sr.flipX = true;
        rb2D.AddForce(Vector2.up * fuerza * 1.2f + Vector2.right * fuerza, ForceMode2D.Impulse);
        yield return new WaitForSeconds(4f);
        Debug.Log("Dispara derecha");

        anim.SetBool("Quieto", false);
        anim.SetBool("Jump", false);
        anim.SetBool("Shoot", true);

        yield return new WaitForSeconds(0.5f);
        llamaDrc.SetActive(true);
        rb2D.isKinematic = false;
        yield return new WaitForSeconds(3);

        anim.SetBool("Quieto", true);
        anim.SetBool("Shoot", false);
        anim.SetBool("Jump", true);
        llamaDrc.SetActive(false);
        Debug.Log("Para");
        rb2D.velocity = Vector2.zero;
        yield return new WaitForSeconds(1f);

        enBucle = false;
    }
    public void OnFinishAnim()
    {
        anim.SetBool("Jump", false);

        anim.SetBool("Shoot", false);

        anim.SetBool("Quieto", true);
    }
}

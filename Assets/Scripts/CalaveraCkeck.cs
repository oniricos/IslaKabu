using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalaveraCkeck : MonoBehaviour
{
    public GameObject fire;
    AudioSource audio;
    [SerializeField] int nSpawn;
    [SerializeField] AudioClip clipaudio;
    bool audioEjecutado;
    private void Update()
    {
        Debug.Log(PlayerPrefs.GetInt("Spawn"));
        audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {

            audio.PlayOneShot(clipaudio);
            PlayerPrefs.SetInt("Spawn", nSpawn);
            fire.SetActive(true);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFinal : MonoBehaviour
{
    GameObject spawnFinal;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnFinal == null)
        {
            spawnFinal = GameObject.FindGameObjectWithTag("Finalizar");
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            this.transform.position = spawnFinal.transform.position;
        }
    }
}

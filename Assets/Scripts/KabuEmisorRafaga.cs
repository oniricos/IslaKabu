using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class KabuEmisorRafaga : MonoBehaviour
{
    public GameObject bala;
    public GameObject bala2;
    Rigidbody2D rb2d;
    SpriteRenderer sr;
    Animator anim;

    GameObject spawnIz, spawnDr;

    Health health;

    public float tiempoDePausaDisparo;
    public float tiempoMovimiento;
    void Start()
    {
        anim = this.GetComponent<Animator>();
        spawnDr = this.transform.GetChild(0).gameObject;
        spawnIz = this.transform.GetChild(1).gameObject;
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();

        health = GetComponent<Health>();

        StartCoroutine(PararYDisparar());
    }
    void Update()
    {
        if (health.CurrentHealth <= 0)
        {
            anim.SetBool("Muerte", true);
        }
    }
    IEnumerator PararYDisparar()
    {
        while (true)
        {
            sr.flipX = false;
            rb2d.velocity = Vector2.left;
            yield return new WaitForSeconds(tiempoMovimiento);
            rb2d.velocity = Vector2.zero;
            yield return new WaitForSeconds(0.15f);
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            anim.SetTrigger("Disparo");
            Instantiate(bala2, spawnIz.transform.position, Quaternion.identity);
            bala.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            yield return new WaitForSeconds(tiempoDePausaDisparo);
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            anim.SetTrigger("Disparo");
            Instantiate(bala2, spawnIz.transform.position, Quaternion.identity);
            bala.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            yield return new WaitForSeconds(tiempoDePausaDisparo);
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            anim.SetTrigger("Disparo");
            Instantiate(bala2, spawnIz.transform.position, Quaternion.identity);
            bala.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            yield return new WaitForSeconds(tiempoDePausaDisparo);
            sr.flipX = enabled;
            rb2d.velocity = Vector2.right;
            yield return new WaitForSeconds(tiempoMovimiento);
            rb2d.velocity = Vector2.zero;
            yield return new WaitForSeconds(0.15f);
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            anim.SetTrigger("Disparo");
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            Instantiate(bala, spawnDr.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(tiempoDePausaDisparo);
            anim.SetTrigger("Disparo");
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            Instantiate(bala, spawnDr.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(tiempoDePausaDisparo);
            anim.SetTrigger("Disparo");
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            Instantiate(bala, spawnDr.transform.position, Quaternion.identity);
            yield return new WaitForSeconds(tiempoDePausaDisparo);
        }
    }
    private void OnDestroy()
    {
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;

public class ComprobarVida : MonoBehaviour
{
    Health health;

    Animator anima;
    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<Health>();
        anima = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //ebug.Log(health.CurrentHealth);
        if (health.CurrentHealth <= 0)
        {
            this.GetComponent<KabuEmisor>().enabled = false;

            anima.SetBool("Muerte", true);
        }
    }
}

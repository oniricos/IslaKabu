using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KabuBasico : MonoBehaviour
{
    Rigidbody2D rb2d;
    SpriteRenderer sr;
    [SerializeField] float distancia;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        StartCoroutine(PararYDisparar());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator PararYDisparar()
    {
        while (true)
        {
            sr.flipX = false;
            rb2d.velocity = Vector2.left;
            yield return new WaitForSeconds(distancia);
            rb2d.velocity = Vector2.zero;
            yield return new WaitForSeconds(0.05f);
            sr.flipX = enabled;
            rb2d.velocity = Vector2.right;
            yield return new WaitForSeconds(distancia);
            rb2d.velocity = Vector2.zero;
            yield return new WaitForSeconds(0.05f);
        }
    }
}

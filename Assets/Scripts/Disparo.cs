using MoreMountains.CorgiEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    //public GameObject bala;
    //public float tiempoEntreBalas;
    //public int disparosInstanciados;

    //GameObject objectAux = null;

    //bool puedoDisparar = true;
    //bool heDisparado = false;

    Weapon projectileWeapon;

    public List<GameObject> gameObjects;

    bool puedoDisparar;

    public LayerMask enemigos;

    void Start()
    {
        projectileWeapon = gameObject.GetComponentInChildren<ProjectileWeapon>();
    }
    private void FixedUpdate()
    {
        Debug.Log(projectileWeapon);

        if (!projectileWeapon)
        {
            projectileWeapon = gameObject.GetComponentInChildren<ProjectileWeapon>();
        }

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right, 5f, enemigos);

        RaycastHit2D hit2D = Physics2D.CircleCast(transform.position, 1, Vector2.zero, enemigos);

        if (hit.collider != null)
        {
            Debug.Log("Algo");
            projectileWeapon.TimeBetweenUsesReleaseInterruption = true;
        }
        else if (true)
        {
            Debug.Log("Nada");
            projectileWeapon.TimeBetweenUsesReleaseInterruption = false;
        }

    }
    void Update()
    {
        
        //if ((Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.KeypadEnter)))
        //{
        //    if (objectAux && tiempoEntreBalas > 0.2f)
        //    {
        //        Debug.Log("Disparo");
        //        puedoDisparar = true;
        //        Disparar();
        //        return;
        //    }
        //    if (objectAux && tiempoEntreBalas <= 0.2f)
        //    {
        //        puedoDisparar = false;
        //        return;
        //    }
        //    if (!objectAux && tiempoEntreBalas > 0.2f)
        //    {
        //        Debug.Log("Disparo");
        //        Disparar();
        //        return;
        //    }
        //    if (!objectAux && tiempoEntreBalas <= 0.2f)
        //    {
        //        Debug.Log("Disparo");
        //        Disparar();
        //        return;
        //    }
        //}

        //ComprobarSiDisparamos();
    }
    //void Disparar()
    //{
    //    heDisparado = true;
    //    GameObject instancia = Instantiate(bala, transform.position + new Vector3(2f, transform.position.y, transform.position.z), Quaternion.identity);

    //    objectAux = instancia;

    //    return;
    //}
    //void ComprobarSiDisparamos()
    //{
    //    if (heDisparado)
    //    {
    //        puedoDisparar = false;
    //        if (tiempoEntreBalas <= 0)
    //        {
    //            tiempoEntreBalas += Time.deltaTime;

    //            if (tiempoEntreBalas > 0.2f || objectAux == null)
    //            {
    //                puedoDisparar = true;
    //                heDisparado = false;
    //                tiempoEntreBalas = 0f;
    //                objectAux = null;
    //                return;
    //            }
    //        }
    //    }
    //    else if(objectAux && tiempoEntreBalas > 0.2f)
    //    {
    //        puedoDisparar = true;
    //    }
    //}
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, 5);
    }
}

using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MenuDePausa : MonoBehaviour
{
    public static bool IsPaused = false;
    public GameObject pausa;
    public GameObject primerBoton;
    MMTimeManager tiempo;
    GameObject player;
    private void Awake()
    {
        //player = GameObject.FindGameObjectWithTag("Player");
        //tiempo = GetComponent<MMTimeManager>();
        //pausa = GameObject.FindGameObjectWithTag("Pausa");
        //pausa.SetActive(false);
    }
    private void Start()
    {
        tiempo = GetComponent<MMTimeManager>();

        player = GameObject.FindGameObjectWithTag("Player");


        pausa = GameObject.FindGameObjectWithTag("Pausa");
        pausa.SetActive(false);

        //player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if (pausa == null)
        {
            try
            {
                pausa = GameObject.Find("PauseSplash");
                //player = GameObject.FindGameObjectWithTag("Player");
                pausa.SetActive(false);
            }
            catch 
            {
                Debug.Log("error");
            }
            IsPaused = false;
        }

        //Debug.Log(pausa);
        if ((Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Joystick1Button7)))
        {
            if (IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

    }

    public void Pause()
    {
        try
        {
            player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<CharacterJump>().enabled = false;
            player.GetComponent<CharacterHandleWeapon>().enabled = false;
        }
        catch
        {

            Debug.Log("Falta player");
            return;
        }
        //player = GameObject.FindGameObjectWithTag("Player");
        //player.GetComponent<CharacterJump>().enabled = false;
        //player.GetComponent<CharacterHandleWeapon>().enabled = false;
        pausa.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(primerBoton);
        //tiempo.CurrentTimeScale = 0f;
        Time.timeScale = 0f;
        IsPaused = true;
    }

    public void Resume()
    {
        try
        {
            player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<CharacterJump>().enabled = true;
            player.GetComponent<CharacterHandleWeapon>().enabled = true;
        }
        catch
        {

            Debug.Log("Falta player");
            return;
        }
        EventSystem.current.SetSelectedGameObject(null);
        pausa.SetActive(false);
        //tiempo.CurrentTimeScale = 1f;
        Time.timeScale = 1f;
        IsPaused = false;
    }
    public void QuitarSeleccion()
    {
        //EventSystem.current.SetSelectedGameObject(null);
    }
}

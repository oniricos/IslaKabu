using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemigos : MonoBehaviour
{
    public GameObject[] enemigos;
    GameObject spawnPoint;
    GameObject kabuAux;
    Animator anim;
    AudioSource audioS;
    [SerializeField] AudioClip clip;
    // Start is called before the first frame update
    void Start()
    {
        spawnPoint = transform.GetChild(0).transform.gameObject;
        kabuAux = transform.GetChild(1).transform.gameObject;
        kabuAux.SetActive(false);

        anim = GetComponent<Animator>();
        audioS = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            anim.SetBool("Spawn", true);
        }
    }

    public void KabuAnimacion()
    {
        kabuAux.SetActive(true);
    }

    public void EliminarKabu()
    {
        kabuAux.SetActive(false);
    }

    public void SonidoTronco()
    {
        audioS.PlayOneShot(clip, 0.3f);
    }
    public void InstanciarKabu()
    {
        Instantiate(enemigos[0], spawnPoint.transform.position, Quaternion.identity);
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public void DestruirSpawn()
    {
        Destroy(this.gameObject);
    }
}
using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using MoreMountains.Tools;
using System;
using MoreMountains.CorgiEngine;

public class Huevo : MonoBehaviour
{
    //public GameObject canvasVictoria;
    GameObject huevo;
    AudioSource audioS;
    private void Awake()
    {
        huevo = transform.GetChild(0).gameObject;
        audioS = GetComponent<AudioSource>();
    }
    private void Start()
    {
        //canvasVictoria.SetActive(false);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Animator anim = collision.gameObject.GetComponent<Animator>();
            //huevo.SetActive(false);
            anim.SetBool("Huevo", true);
            collision.gameObject.GetComponent<Character>().enabled = false;
            collision.gameObject.GetComponent<CorgiController>().enabled = false;
            collision.gameObject.GetComponent<CharacterHorizontalMovement>().enabled = false;
            collision.gameObject.GetComponent<CharacterJump>().enabled = false;
            collision.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            //Animator anim = collision.gameObject.GetComponent<Animator>();
            //huevo.SetActive(false);
            //anim.SetBool("Huevo", true);
        }
    }
    public void Victoria()
    {
        //canvasVictoria.SetActive(true);
    }
}

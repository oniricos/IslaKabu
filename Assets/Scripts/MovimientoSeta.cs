using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoSeta : MonoBehaviour
{
    Rigidbody2D rb2d;

    SpriteRenderer sr;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();

        sr = GetComponent<SpriteRenderer>();

        StartCoroutine(MoverIzqDr());
    }

    IEnumerator MoverIzqDr()
    {
        while (true)
        {
            if (this.enabled == false)
            {
                yield break;
            }
            sr.flipX = true;
            rb2d.velocity = Vector2.right; //
            if (this.enabled == false)
            {
                yield break;
            }
            yield return new WaitForSeconds(1f);//
            if (this.enabled == false)
            {
                yield break;
            }
            sr.flipX = false;
            rb2d.velocity = Vector2.left;//
            if (this.enabled == false)
            {
                yield break;
            }
            yield return new WaitForSeconds(1f);//
            if (this.enabled == false)
            {
                yield break;
            }
        }
    }
}

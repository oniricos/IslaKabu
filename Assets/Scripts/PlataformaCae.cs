using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaCae : MonoBehaviour
{
    public float tiempoArriba;

    //Animator anim;
    Rigidbody2D rb2d;

    bool arriba;

    bool ejecucion;

    AudioSource audio;
    ParticleSystem particle;
    Animator anim;
    private void Start()
    {
        anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        particle = GetComponent<ParticleSystem>();
        audio = GetComponent<AudioSource>();
        //anim = this.gameObject.GetComponent<Animator>();
    }

    private void Update()
    {

        if (arriba && !ejecucion)
        {
            StartCoroutine(PlayerArriba());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            arriba = true;
        }
    }
    IEnumerator PlayerArriba()
    {
        ejecucion = true;
        particle.Play();
        audio.Play();
        yield return new WaitForSeconds(0.1f);
        anim.SetBool("Arriba", true);
        yield return new WaitForSeconds(tiempoArriba);
        //anim.SetBool("Arriba", false);
        //anim.SetBool("Destruir", true);
        rb2d.isKinematic = false;
        yield return new WaitForSeconds(0.5f);
        audio.Stop();
    }

    public void Destruir()
    {
        Destroy(this.gameObject);
    }
}

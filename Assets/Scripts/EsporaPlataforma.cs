using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsporaPlataforma : MonoBehaviour
{
    public float tiempoArriba;

    Animator anim;

    float time;

    bool arriba;

    bool ejecucion;
    private void Start()
    {
        anim = this.gameObject.GetComponent<Animator>();
    }

    private void Update()
    {
        time = time + Time.deltaTime;

        if (time > 10)
        {
            anim.SetBool("Destruir", true);
        }

        if (arriba && !ejecucion)
        {
            StartCoroutine(PlayerArriba());
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            arriba = true;
        }
    }
    IEnumerator PlayerArriba()
    {
        ejecucion = true;
        anim.SetBool("Arriba", true);
        yield return new WaitForSeconds(tiempoArriba);
        anim.SetBool("Arriba", false);
        anim.SetBool("Destruir", true);
    }

    public void Destruir()
    {
        Destroy(this.gameObject);
    }
}

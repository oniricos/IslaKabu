using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnVoladores : MonoBehaviour
{
    public GameObject volador;
    public float min, max;
    public float tiempoSpawn;
    private void Start()
    {
        Instantiate(volador, new Vector3(transform.position.x, Random.Range(min, max), transform.position.z), Quaternion.identity);

        StartCoroutine(InstanciaVoladores());
    }
    IEnumerator InstanciaVoladores()
    {
        while (true)
        {
            yield return new WaitForSeconds(tiempoSpawn);

            Instantiate(volador, new Vector3(transform.position.x, Random.Range(min, max), transform.position.z), Quaternion.identity);
        }
    }
}

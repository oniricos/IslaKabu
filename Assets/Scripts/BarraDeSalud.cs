using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MoreMountains.CorgiEngine;

public class BarraDeSalud : MonoBehaviour
{
    Health player;

    int vida;

    public Slider slider;

    private void Update()
    {
        if (player == null)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player").GetComponent<Health>();
            }
            catch
            {
                Debug.Log("no player");
            }
        }

        vida = player.CurrentHealth;

        slider.value = vida;
    }
}

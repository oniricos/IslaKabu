using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntradaSpawn : MonoBehaviour
{
    public GameObject Spawn;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Spawn.SetActive(true);
        }
    }
}

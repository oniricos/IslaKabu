using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destrutror : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            return;
        }
        Destroy(collision.gameObject);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            return;
        }
        Destroy(collision.gameObject);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiQuieto : MonoBehaviour
{
    public GameObject llamaIzq;
    public GameObject llamaDrc;

    GameObject player;

    Collider2D coll;
    SpriteRenderer sr;
    Animator anim;

    [SerializeField] float tiempoEntreDisparos;

    Vector3 dist;
    bool enBucle;

    void Start()
    {
        coll = GetComponent<Collider2D>();

        anim = GetComponent<Animator>();

        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (!player)
        {
            try
            {
                player = GameObject.FindGameObjectWithTag("Player");
            }
            catch
            {
                Debug.Log("No hay player");
            }
        }

        if (player)
        {
            dist = player.transform.position - transform.position;

            Debug.Log(dist.x);
        }

        if (coll.isTrigger == true)
        {
            coll.isTrigger = false;
        }

        if (dist.x < 0 && !enBucle)
        {
            sr.flipX = false;
        }
        else if(dist.x >= 0 && !enBucle)
        {
            sr.flipX = true;
        }

        if (!enBucle)
        {
            if (dist.x < 0)
            {
                StartCoroutine(DispararIzq());
            }
            else
            {
                StartCoroutine(DispararDrc());
            }
        }
    }
    IEnumerator DispararIzq()
    {
        enBucle = true;

        yield return new WaitForSeconds(tiempoEntreDisparos);

        anim.SetBool("Quieto", false);
        anim.SetBool("Shoot", true);
        yield return new WaitForSeconds(0.5f);
        llamaIzq.SetActive(true);
        yield return new WaitForSeconds(3);
        anim.SetBool("Shoot", false);
        anim.SetBool("Quieto", true);
        llamaIzq.SetActive(false);

        enBucle = false;

    }
    IEnumerator DispararDrc()
    {
        enBucle = true;

        yield return new WaitForSeconds(tiempoEntreDisparos);

        anim.SetBool("Quieto", false);
        anim.SetBool("Shoot", true);
        yield return new WaitForSeconds(0.5f);
        llamaDrc.SetActive(true);
        yield return new WaitForSeconds(3);
        anim.SetBool("Shoot", false);
        anim.SetBool("Quieto", true);
        llamaDrc.SetActive(false);

        enBucle = false;
    }
}

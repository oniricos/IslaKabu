using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;


public class KabuEmisor : MonoBehaviour
{
    public GameObject bala;
    public GameObject bala2;
    Rigidbody2D rb2d;
    SpriteRenderer sr;
    Animator anim;

    GameObject spawnIz, spawnDr;

    Health health;

    public float tiempoDePausaDisparo;
    public float tiempoMovimiento;
    void Start()
    {
        anim = this.GetComponent<Animator>();
        spawnDr = this.transform.GetChild(0).gameObject;
        spawnIz = this.transform.GetChild(1).gameObject;
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();

        health = GetComponent<Health>();

        StartCoroutine(PararYDisparar());
    }
    void Update()
    {
        //Debug.Log(health.CurrentHealth);
        //if (movimiento)
        //{
        //    contador = 0;
        //}
        //else if (!movimiento)
        //{
        //    contador += Time.deltaTime;
        //}


        //if (movimiento) //si hay movimiento me muevo hacia la izq
        //{
        //    transform.Translate(Vector3.left * Time.deltaTime, Space.Self);
        //}
        
        //if (posicionDestino == posIzq)
        //{
        //    if (transform.position.x <= posIzq.x && movimiento)
        //    {
        //        movimiento = false;
        //        //StartCoroutine(PararYDisparar());
        //        //posicionDestino = posDer;
        //    }
        //    if (movimiento == false)
        //    {
        //        if (contador > tiempoDisparo)
        //        {
        //            movimiento = true;
        //            //disparo
        //        }
        //        posicionDestino = posDer;
        //    }
        //}
        //if (posicionDestino == posDer)
        //{
        //    if (transform.position.x >= posDer.x && movimiento)
        //    {
        //        movimiento = false;
        //        //StartCoroutine(PararYDisparar());
        //        //transform.rotation = Quaternion.EulerAngles(transform.rotation.x, 0, transform.rotation.z);
        //    }
        //    if (movimiento == false)
        //    {
        //        if (contador > tiempoDisparo)
        //        {
        //            //disparo
        //        }
        //        posicionDestino = posIzq;
        //    }
        //}
        
        //if (posicionDestino == posDer)
        //{
        //    transform.Rotate(0, 180, 0);
        //}
        //else
        //{
        //    transform.Rotate(0, 0, 0);
        //}
    }
    IEnumerator PararYDisparar()
    {
        while (true)
        {
                sr.flipX = false;
                rb2d.velocity = Vector2.left;
                yield return new WaitForSeconds(tiempoMovimiento);
                rb2d.velocity = Vector2.zero;
                yield return new WaitForSeconds(0.15f);
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            anim.SetTrigger("Disparo");
                Instantiate(bala2, spawnIz.transform.position, Quaternion.identity);
                bala.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
                yield return new WaitForSeconds(tiempoDePausaDisparo);
                sr.flipX = enabled;
                rb2d.velocity = Vector2.right;
                yield return new WaitForSeconds(tiempoMovimiento);
                rb2d.velocity = Vector2.zero;
                yield return new WaitForSeconds(0.15f);
            if (health.CurrentHealth <= 0)
            {
                break;
            }
            anim.SetTrigger("Disparo");
                Instantiate(bala, spawnDr.transform.position, Quaternion.identity);
                yield return new WaitForSeconds(tiempoDePausaDisparo);
            //sr.flipX = false;
            //rb2d.velocity = Vector2.left;
            //yield return new WaitForSeconds(tiempoMovimiento);
            //rb2d.velocity = Vector2.zero;
            //yield return new WaitForSeconds(0.15f);
            //anim.SetTrigger("Disparo");
            //Instantiate(bala2, spawnIz.transform.position, Quaternion.identity);
            //bala.GetComponent<Rigidbody2D>().velocity = Vector2.left * 5;
            //yield return new WaitForSeconds(tiempoDePausaDisparo);
            //sr.flipX = enabled;
            //rb2d.velocity = Vector2.right;
            //yield return new WaitForSeconds(tiempoMovimiento);
            //rb2d.velocity = Vector2.zero;
            //yield return new WaitForSeconds(0.15f);
            //anim.SetTrigger("Disparo");
            //Instantiate(bala, spawnDr.transform.position, Quaternion.identity);
            //yield return new WaitForSeconds(tiempoDePausaDisparo);
        }
    }
    private void OnDestroy()
    {
        
    }
}

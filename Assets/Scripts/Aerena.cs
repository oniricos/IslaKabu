using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aerena : MonoBehaviour
{
    Animator anim;
    Collider2D coll;
    SpriteRenderer sR;
    Animator corazonUI;
    [SerializeField] ParticleSystem particle;

    private void Awake()
    {
        coll = GetComponent<Collider2D>();
        anim = GetComponent<Animator>();
        sR = GetComponent<SpriteRenderer>();
        corazonUI = GameObject.FindGameObjectWithTag("CorazonUI").GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameObject.FindGameObjectWithTag("Contador").GetComponent<contadorAerena>().ReloadPoints();
            corazonUI.SetTrigger("Palpita");
            particle.Play();
            coll.enabled = false;
            //sR.enabled = false;
            anim.SetBool("Coin", true);
            //Destroy(this.gameObject, 6);
        }
    }
    public void Coin()
    {
        sR.enabled = false;
    }
}

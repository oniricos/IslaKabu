using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class contadorAerena : MonoBehaviour
{
    public static int points = 0;
    int aux = 0;
    Text text;
    // Start is called before the first frame update
    private void Awake()
    {
        text = this.gameObject.GetComponent<Text>();
        points = 0;
    }
    void Start()
    {
        //text.text = "" + points;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(points);
        text.text = "" + points;
    }
    public void ReloadPoints()
    {
        points++;
        //text.text = "" + points;
    }
    public int VerPuntos()
    {
        return points;
    }
    public IEnumerator EjecutarSuma()
    {
        for (int i = 0; i < points + 1; i++)
        {
            aux = i;
            yield return new WaitForSeconds(0.01f);
        }
    }
}

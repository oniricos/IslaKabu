using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickSound : MonoBehaviour
{
    public AudioSource source { get { return GetComponent<AudioSource>(); } }
    public Button btn { get { return GetComponent<Button>(); } }
    public AudioClip clip;
    public float volumen;
    void Start()
    {
      
        gameObject.AddComponent<AudioSource>();
        source.volume = volumen;

        btn.onClick.AddListener(PlaySound);
    }

    // Update is called once per frame
  void PlaySound()
    {
       
        source.PlayOneShot(clip);
    }
}

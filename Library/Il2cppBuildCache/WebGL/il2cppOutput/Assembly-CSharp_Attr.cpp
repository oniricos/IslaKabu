﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteAlways
struct ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// MoreMountains.Feedbacks.MMFInformationAttribute
struct MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteAlways
struct ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// MoreMountains.Feedbacks.MMFInformationAttribute
struct MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFInformationAttribute/InformationType
struct InformationType_t2B47BCDF424923E78151463EC97AC1ECF6FFCA31 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFInformationAttribute/InformationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InformationType_t2B47BCDF424923E78151463EC97AC1ECF6FFCA31, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteAlways::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteAlways__ctor_mDB73D23637E65E57DE87C7BAAFE4CE694AE9BEE0 (ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFInformationAttribute::.ctor(System.String,MoreMountains.Feedbacks.MMFInformationAttribute/InformationType,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFInformationAttribute__ctor_m004E74EBE93920E7BAC370DDB10E141EDE1A073D (MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2 * __this, String_t* ___message0, int32_t ___type1, bool ___messageAfterProperty2, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void Aerena_tC6DBA0AFEF358551164E5F68D6230150D0A8B72D_CustomAttributesCacheGenerator_particle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BrujaFinal_tF1E125D636EBA3B32B60627063E9B436B97C06E1_CustomAttributesCacheGenerator_brujaHuevos(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Burbuja_t1324D3930878CA1583FB78D4D648E7A0D5B8DD7A_CustomAttributesCacheGenerator_Burbuja_MoverIzqDr_m5A8511933345071EC18815BED888F8B0E38387E1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_0_0_0_var), NULL);
	}
}
static void U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2__ctor_mFF418106ED642001DE5EB4EF9B1DA81E6FC9F95B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_IDisposable_Dispose_m6BD167F2FD4C89A730E88EE2143851AD001A3181(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87099B322323E87F8E5FA22020DD9F716F1DB421(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_Reset_m6811DBECCC2CE3550F38BA1B956DCEA51E2FB330(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_get_Current_m5773AEC31ABAB38671A73E5086BE573F14817574(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CalaveraCkeck_tB42328E027FB4FBEA90A17F5F4E0A98E3FBCA8B4_CustomAttributesCacheGenerator_nSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CalaveraCkeck_tB42328E027FB4FBEA90A17F5F4E0A98E3FBCA8B4_CustomAttributesCacheGenerator_clipaudio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ContadorFinal_tF8183130C720FBDBDB40640B454039392F224F13_CustomAttributesCacheGenerator_ContadorFinal_EjecutarSuma_mFE44F1E853276A4C77872A0E012DF9A86248EE98(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_0_0_0_var), NULL);
	}
}
static void U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9__ctor_m555B5D67B42B1BA719A4FDAC995620EB3494F48E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_IDisposable_Dispose_m51E2F857F838E5B63D1EB6C280C14F1AA38FA396(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC01B665FC29861D3CFEF4E6B5E3C38CD1E10DE5A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_Reset_mAB9D23DD640D14BFD07A18375B560886D30407F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_get_Current_mECBDCC521AAF9ADDE4EB030C11D055488FCB5E01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EfectoParallax_t8D7625E77EB39C58505238A435BFD25FC2AE0F83_CustomAttributesCacheGenerator_parallaxEffectMultiplier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EsporaPlataforma_t8ADD6531CF86D25657A3549F7B40A9360E6C0812_CustomAttributesCacheGenerator_EsporaPlataforma_PlayerArriba_mC18CA67F1ABF04F16C77688C489BDC2557BFF05F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_0_0_0_var), NULL);
	}
}
static void U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8__ctor_m299B668FEB88DA495D32B2611BEFCE92196184C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_IDisposable_Dispose_mAE4D097CFF0CD28C27C270DC6C0234C976BF65B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m190919B43D3B6C4971D2B06CD2626862F446211E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_Reset_mD517702B85C053A9320DED1EFF9909B108DD4268(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_get_Current_m7DAD340B862A764D7B16D3BDCCBEC9418CBB479D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_sonidoEstrella(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_texto(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_sceneToLoad(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_Estrellas_Star_mE2BC7E35D0F8BFD0911800B7E308C46F14C1B285(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_0_0_0_var), NULL);
	}
}
static void U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17__ctor_mAC92040ACB86FA97FAC8781D22D6CB531D259157(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_IDisposable_Dispose_m26583F744D3AFF0D05AD5D694ACBB24C816EF83D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EA8D7A57AB7E6EDD3DDD6DA65B8A5E9F6E0840F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_Collections_IEnumerator_Reset_m3A2F5A257120B3DE339BC03EAB7092076161BDBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_Collections_IEnumerator_get_Current_m01F25A7B6083895784BDE39B6C16C7D953F5451D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void KabuBasico_tFE69878CBDDE4BBD0EFD39EF830F96043CD51FB2_CustomAttributesCacheGenerator_distancia(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void KabuBasico_tFE69878CBDDE4BBD0EFD39EF830F96043CD51FB2_CustomAttributesCacheGenerator_KabuBasico_PararYDisparar_m098F975AB2D83E3736C8D78453D8997DE57DCB07(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_0_0_0_var), NULL);
	}
}
static void U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5__ctor_m28B78F6217A3C7D8E4BB4F68EA15F9D5E26F4CB6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_IDisposable_Dispose_m3494AFC9EB836374DF75AD4B274764BB53B6E894(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E2003B8D3BE6EF61331DB120E9AE8BB915C712F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_Reset_mF0BA0302C68431F2AFFB14674A875A6AF12F6884(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_get_Current_m25FE5DDBAB2FC720A89EF7D744E38A3A77E40DCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void KabuEmisor_tC18A3EE157C5D089DCF6284DD6E10E012876F7E3_CustomAttributesCacheGenerator_KabuEmisor_PararYDisparar_m28F90EA2256A32A214247BA59F0C5FF70D96E29F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_0_0_0_var), NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12__ctor_m2D700458181046A503F701ADFC8BA8DC9D3A9EB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m7B7085B9C5D6AE8BAFC79EE702AAD249E6997E9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6799E0EB5EAFE14DADFD640BB72A0A09470B96C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mF54B2836FF863E4F1CDF46F6498B9E100C6E4FE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_mF32B2BB1AD709293B55180110157472741FCCA06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void KabuEmisorRafaga_t6D55AE8D80C528082263DFB1B7F58E975776E0D5_CustomAttributesCacheGenerator_KabuEmisorRafaga_PararYDisparar_m09C14B3FB2BFFE4EB0CF8E22B11D8C83820993F0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_0_0_0_var), NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12__ctor_mF857BF4F98F66D816DFF39B81B6DF35DA228D900(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m5259FEE7B1C494B388D7E2FC7D7D9476BC4A10CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9C65E1438072C80B375B1894990FFC643263294(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mA55C1772F133A8694C145E9CE918316A12606C97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_m0B368BD51F913A2888E0C3E70EEBE509DFA14945(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void KabuOnda_t25E841418585C188F8782AAF36168B50D7260EAC_CustomAttributesCacheGenerator_KabuOnda_MoverIzqDr_m4FE6E82C5F835C766A913EEB83567A117E4E710F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_0_0_0_var), NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3__ctor_m1F2F458C54014365FD64467AEB95A749273D292B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_mEE21FEE22A97265ACCD39394E3894557F0E4942C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10A5F520A464818C6522A42F2DC28C5769C38401(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mFB3F2FF2AEF004BC485745191C954172F0C3C3FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_m104439B07E2DDEB5A7A71252CF2073525EF3EDD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MovimientoSeta_t06B121F1FADF4A6FA90C9809D19D6B1B19BA5835_CustomAttributesCacheGenerator_MovimientoSeta_MoverIzqDr_mDD6840527C7DE1E9CE743CEB3B1498BA712792C1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_0_0_0_var), NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3__ctor_m96CB5E50F03A4DA2462445C3D8C9B0C4954BFD1A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_m2C33D7423D8CF1A4186256039D15E77B6E68C72B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA534FFD4507218231C17A4A0D559A5ABB5B9ECA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mF33DE026F075998BD681BB1C01F0C891CCA9EEA7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_mBB7FCDF227060896BDC0F40A549A034C56C875B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MuerteManager_tC1ACE75ADC447C3D8CBC99113922A280CF15E1A1_CustomAttributesCacheGenerator_mismaEscena(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Pez_t5C363F201A4942DC2B18D0DA3679A7BDD0404E4E_CustomAttributesCacheGenerator_Pez_Salta_m871747B9548F5058B2B6F71E0402E979E21F1651(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_0_0_0_var), NULL);
	}
}
static void U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10__ctor_m0307177566364F8231E65778A7EFF36B440BDA85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_IDisposable_Dispose_mEACEE99BB082F725001956990EE7532B0F101703(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED10B84D5D0F45C7D0218B89912F50347CFCD0A2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_Collections_IEnumerator_Reset_m0CAB56F87D69775FDBA924C157D8BA9984D6FCA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_Collections_IEnumerator_get_Current_mD7CB6FAB5E7BD41C0EC578DE9FA4A185A778138F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlantaCarnivora_tFFC2E11DFA95D7EE3BB9C11AD588E10A7200C512_CustomAttributesCacheGenerator_PlantaCarnivora_Plantacarnivora_m615E1F73C291B71F36330C85ADEA77064B5F00EA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_0_0_0_var), NULL);
	}
}
static void U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3__ctor_mA1FD778DB89B77632E8E9FAC03AA6CADB30A6262(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_IDisposable_Dispose_mEE53C5431D11E1570FB3E2B3FD320C948929A789(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26A31B3F8FA250958606C9826236F2F2F49DE751(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_Reset_mD90975CD3BF73D486C1E5E51D671E7C066164778(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_get_Current_mE44BC670E424CB47A3392906F03C1870F5810EED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlantaEspora_t79CE382FBAAD9FDA4FB1AB353379FF2CC31DB2D6_CustomAttributesCacheGenerator_PlantaEspora_InstanciaEsporas_m206FCB2862197C6D0A89C8315150CF66AD64B9D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_0_0_0_var), NULL);
	}
}
static void U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6__ctor_mD138B51F39DA5DED79627D873CA8864FED69952A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_IDisposable_Dispose_m2A6E5460A7B2CAED82982944943BAC197A5276D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8300C6B902901FDBD298B132B2EE4F344C8BE0B4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_Reset_m88576E6176233477BD4612172744765EAE8E413B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_get_Current_mBB6C82B823E03BAC16DD428ED16B53F003CE7D37(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PlataformaCae_tB34860FEAABFFCDADD23714CEC6D5C149A228CCF_CustomAttributesCacheGenerator_PlataformaCae_PlayerArriba_m6B299C877DDF407B84EB3D2843A319570A3F7314(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_0_0_0_var), NULL);
	}
}
static void U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10__ctor_m1CC2DF67A3E6CEB1DBFE36F189607B2886A1BDD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_IDisposable_Dispose_m654D1CD2F1EF73741B21CBAE8602DD3C23D957F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m778E4A1A205EAC3F0ED1CFDBAB69A619839BDA8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_Reset_m621E3B3090BDE42B7043AE2ABA1843E7D2E75907(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_get_Current_m935DF9075C85D1D5204F3CBE076F59C6040CCD27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Salamandra_t985C8281F5C86772E4647E9B1E852F2AC2D9C05D_CustomAttributesCacheGenerator_Salamandra_DisparaSalta_m313C54FB7866FB593AB9DD00299606A15951E5FA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_0_0_0_var), NULL);
	}
}
static void U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11__ctor_mD9001ABB51207AF811B15948966A3EAE6D1730AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_IDisposable_Dispose_m5CC9EBA93013AD7E12FC6C0137613B304F6970FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03C65B1CD417703E42F52AB854B415BCEE0E547D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_Reset_mCCE91D7C239B131EB0F3E1A48DFF350E49D8B719(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_get_Current_m069AD940AD90A91ABEF88800EFD5446C7C2FB455(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SetaExplosiva_tD4D853F68CA237BC6D6F6DEF52F7A718BCA3C3A7_CustomAttributesCacheGenerator_clip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SetaExplosiva_tD4D853F68CA237BC6D6F6DEF52F7A718BCA3C3A7_CustomAttributesCacheGenerator_SetaExplosiva_MoveExpl_mB966BBEBABB361812827BB32A8B975949A978756(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_0_0_0_var), NULL);
	}
}
static void U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20__ctor_m1AD6BC85565B85E3097D42B879DECCC2ECF3EA19(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_IDisposable_Dispose_mB6769B0CE3DFAD199D3175811B6FAE67C028085B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0289ACBC55AC3361224DFCBCDC5E240F82DD30F5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_Collections_IEnumerator_Reset_m27878F4BCBD7CA8ACF397C1E2B0F76C8327E4182(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_Collections_IEnumerator_get_Current_m79B6D36E92E37A5F984F552BCA6C96B736848F12(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpawnBurbujas_t8E962568AA1B2B02F102849823690BBA8C989DB8_CustomAttributesCacheGenerator_SpawnBurbujas_InstanciaVoladores_m8E4D3E9E79075B79CC7764F778B63845ABE86DC2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_0_0_0_var), NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5__ctor_m566BC2C949479B61D744B9C4A6F22A77258986EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mF94EEE8EE391F6B50244657DB59C85F52537E58A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5F857AE3CAD2009297B73E6FF08D60551492A68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mB9573B135681505A6F5D8DE51C2BA457F86B8B39(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_mCDC3EFAE58163D472B6C186624B99AF52E770AD4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpawnEnemigos_t87E729BFC5C26DFB5A7AB1EAD88B43C9F949967F_CustomAttributesCacheGenerator_clip(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnPlanta_t2D3C351C7D837CC0CEDF8DCEA7E936549CB3B294_CustomAttributesCacheGenerator_puntoSpawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpawnPlanta_t2D3C351C7D837CC0CEDF8DCEA7E936549CB3B294_CustomAttributesCacheGenerator_SpawnPlanta_Spawn_m9A239121B02E5C48C32F6A03109EA6B985D31778(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_0_0_0_var), NULL);
	}
}
static void U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7__ctor_m8F2FCB7CD60C3C16C3684449CC19D8A62C0FB30C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_IDisposable_Dispose_mDD5B5C4073274DD53D03539305093E05A62ABB80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41D02349281E8475CBA467DAD138C068AB0B46B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_Collections_IEnumerator_Reset_m725CDFFF87F29DF99BD34EC091B742DE77E0C331(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_Collections_IEnumerator_get_Current_mE08A8F64462C87D5CA0515DEA6AD51C86A7EC342(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SpawnVoladores_t510DFFE8436CABEE8B91BD764B692FECEE8D963C_CustomAttributesCacheGenerator_SpawnVoladores_InstanciaVoladores_m5D18C26DBDBE5B44417CC3D1FC50C8F2DC89BFF7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_0_0_0_var), NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5__ctor_m6A47693939320F7DACB1B4C4CBDB197C79CA6D80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mA4EC4C5F217E444594EB675DE05504890EDDA72C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF179B25FE4C16D4D2884B3FA9667F13E64D8A246(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mA33C0021369B8C22FEE3E1FFA897E2F744C5C139(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_m717DEBC2BB5F4397E38DDBAF05883A8899CE1ECC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UiQuieto_tC9C1C9A8F3CC755BCD2DDA4D9D335C65B7EDD2A7_CustomAttributesCacheGenerator_tiempoEntreDisparos(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiQuieto_tC9C1C9A8F3CC755BCD2DDA4D9D335C65B7EDD2A7_CustomAttributesCacheGenerator_UiQuieto_DispararIzq_m161DD5C46E0D042FFFBD2CB880C084BE7A3EE7F2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_0_0_0_var), NULL);
	}
}
static void UiQuieto_tC9C1C9A8F3CC755BCD2DDA4D9D335C65B7EDD2A7_CustomAttributesCacheGenerator_UiQuieto_DispararDrc_m559535A821ABE56F02C4AC2B2CB2CD7E1158EF85(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_0_0_0_var), NULL);
	}
}
static void U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11__ctor_m3C73C4D06B254E45AEA5179164A8B11AEE17FD06(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_IDisposable_Dispose_mF422CF6AE6D3C5467F30E2A9425C88B5103DCAD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD72EB319620E3C152D71A1B166D134C606B21D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_Reset_m2169F2504BDFFD82B5EBF4CD0B1C644A50E9B14A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_get_Current_mD25026294FDF7D508B82F96AFE59540070282C6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12__ctor_m80D8E27DB0037C73FA28E09D588266AE3ECBDDBA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_IDisposable_Dispose_mFA330C43C7F50AACD3CBF37DFB682861F5B395B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D48A12FF98EBACE1C0146CFDDD151E9C80432A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_Reset_mAB2E8B6BC2CC8C357054304C212B17198FD7495C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_get_Current_mA18797AA09FFA1698A00A87A9356C39733BD70CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Volver_t5F9ACD9D47282A0E298A48DDF750D8E790AC36AB_CustomAttributesCacheGenerator_scena(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void contadorAerena_t19733C65D7D3675C9D702F54D5A6E41675993B1D_CustomAttributesCacheGenerator_contadorAerena_EjecutarSuma_mEC3153784BA9B13858B65F778A0E0A83378150ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_0_0_0_var), NULL);
	}
}
static void U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8__ctor_mB673D2B05A6FB785A327713F2319440CFC0E92C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_IDisposable_Dispose_mD1CB9B1643C7430D4E3FFCFA7DDB7A2153BC6FA8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m680481E7056FF135C717AD069C76864E13EF1E96(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_Reset_m3744EBE3720715716DC0495BC10DB972486551D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_get_Current_mD1812047E81A84A1D89D2A3CDE3FE94CCCCA954F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RuleTile_tD245F3ABA7BA1F4CEAC8D3EB694FAAD18856C41C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
	}
}
static void RuleTile_tD245F3ABA7BA1F4CEAC8D3EB694FAAD18856C41C_CustomAttributesCacheGenerator_m_TilingRules(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6C\x65\x6D\x61\x70\x2F\x47\x72\x69\x64\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionIntKeys(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionIntValues(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionStringKeys(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionStringValues(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionFloatKeys(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionFloatValues(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionDoubleKeys(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionDoubleValues(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionObjectKeys(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionObjectValues(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionColorKeys(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionColorValues(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_t3EF34025377C04F4DA01C883829E9CE1C67E7528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t97B4F0D0AFE883572C7088804613726181537F21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PipelineTile_t64C4FBC3DDED03C6B806BC75395C0040B4319285_CustomAttributesCacheGenerator_m_Sprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomTile_tC499D4E54B5A60DC7254BD583F321251B6F6CE70_CustomAttributesCacheGenerator_m_Sprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TerrainTile_tF926F13A9557D14454AC5AADC7AF2118D5E1C93C_CustomAttributesCacheGenerator_m_Sprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoBall_t2DC8C97097BDF12123C9ECFAA6E1926345A2BC28_CustomAttributesCacheGenerator_DemoBall_ProgrammedDeath_m2DF4845C32A8963BAB27D7F0BFACC5784712BE92(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_0_0_0_var), NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3__ctor_m594CF3FB53C5D8B5ED586479D34EE72E4943BF2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_IDisposable_Dispose_m543CD3EBD9CAC69065A27E5644411AFE8B6C7B6A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC82FC1704348F2794A1AEB9E8C43335F3189EDEA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_Reset_m62594DC3054FFC736C7286487579485F6501901F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_get_Current_mA0F24F509E8DF2CC5413AC39750F9F5B91179303(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B * tmp = (ExecuteAlways_tF6C3132EB025F81EAA1C682801417AE96BEBF84B *)cache->attributes[0];
		ExecuteAlways__ctor_mDB73D23637E65E57DE87C7BAAFE4CE694AE9BEE0(tmp, NULL);
	}
}
static void DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_NotSupportedInWebGL(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
}
static void DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_TargetButton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x69\x6E\x64\x69\x6E\x67\x73"), NULL);
	}
}
static void DemoPackageTester_t0907E9A97CEA5E17DFB6E5E5670F5D1E60D00DD5_CustomAttributesCacheGenerator_RequiresPostProcessing(CustomAttributesCache* cache)
{
	{
		MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2 * tmp = (MMFInformationAttribute_t0C1DA3939625AA24735CF9454EAD9AED698F3DB2 *)cache->attributes[0];
		MMFInformationAttribute__ctor_m004E74EBE93920E7BAC370DDB10E141EDE1A073D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6F\x6E\x6C\x79\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x61\x6E\x20\x65\x72\x72\x6F\x72\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x6F\x6E\x73\x6F\x6C\x65\x20\x69\x6E\x20\x63\x61\x73\x65\x20\x64\x65\x70\x65\x6E\x64\x65\x6E\x63\x69\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x64\x65\x6D\x6F\x20\x68\x61\x76\x65\x6E\x27\x74\x20\x62\x65\x65\x6E\x20\x69\x6E\x73\x74\x61\x6C\x6C\x65\x64\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x73\x61\x66\x65\x6C\x79\x20\x72\x65\x6D\x6F\x76\x65\x20\x69\x74\x20\x69\x66\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x2C\x20\x61\x6E\x64\x20\x74\x79\x70\x69\x63\x61\x6C\x6C\x79\x20\x79\x6F\x75\x20\x77\x6F\x75\x6C\x64\x6E\x27\x74\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x6B\x65\x65\x70\x20\x74\x68\x61\x74\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x6F\x77\x6E\x20\x67\x61\x6D\x65\x2E"), 3LL, false, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[194] = 
{
	U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator,
	U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator,
	U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator,
	U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator,
	U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator,
	U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator,
	U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator,
	U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator,
	U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator,
	U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator,
	U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator,
	U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator,
	U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator,
	U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator,
	U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator,
	U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator,
	U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator,
	U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator,
	U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator,
	U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator,
	U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator,
	RuleTile_tD245F3ABA7BA1F4CEAC8D3EB694FAAD18856C41C_CustomAttributesCacheGenerator,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_t3EF34025377C04F4DA01C883829E9CE1C67E7528_CustomAttributesCacheGenerator,
	U3CU3Ec_t97B4F0D0AFE883572C7088804613726181537F21_CustomAttributesCacheGenerator,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator,
	DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator,
	Aerena_tC6DBA0AFEF358551164E5F68D6230150D0A8B72D_CustomAttributesCacheGenerator_particle,
	BrujaFinal_tF1E125D636EBA3B32B60627063E9B436B97C06E1_CustomAttributesCacheGenerator_brujaHuevos,
	CalaveraCkeck_tB42328E027FB4FBEA90A17F5F4E0A98E3FBCA8B4_CustomAttributesCacheGenerator_nSpawn,
	CalaveraCkeck_tB42328E027FB4FBEA90A17F5F4E0A98E3FBCA8B4_CustomAttributesCacheGenerator_clipaudio,
	EfectoParallax_t8D7625E77EB39C58505238A435BFD25FC2AE0F83_CustomAttributesCacheGenerator_parallaxEffectMultiplier,
	Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_sonidoEstrella,
	Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_texto,
	Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_sceneToLoad,
	KabuBasico_tFE69878CBDDE4BBD0EFD39EF830F96043CD51FB2_CustomAttributesCacheGenerator_distancia,
	MuerteManager_tC1ACE75ADC447C3D8CBC99113922A280CF15E1A1_CustomAttributesCacheGenerator_mismaEscena,
	SetaExplosiva_tD4D853F68CA237BC6D6F6DEF52F7A718BCA3C3A7_CustomAttributesCacheGenerator_clip,
	SpawnEnemigos_t87E729BFC5C26DFB5A7AB1EAD88B43C9F949967F_CustomAttributesCacheGenerator_clip,
	SpawnPlanta_t2D3C351C7D837CC0CEDF8DCEA7E936549CB3B294_CustomAttributesCacheGenerator_puntoSpawn,
	UiQuieto_tC9C1C9A8F3CC755BCD2DDA4D9D335C65B7EDD2A7_CustomAttributesCacheGenerator_tiempoEntreDisparos,
	Volver_t5F9ACD9D47282A0E298A48DDF750D8E790AC36AB_CustomAttributesCacheGenerator_scena,
	RuleTile_tD245F3ABA7BA1F4CEAC8D3EB694FAAD18856C41C_CustomAttributesCacheGenerator_m_TilingRules,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionIntKeys,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionIntValues,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionStringKeys,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionStringValues,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionFloatKeys,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionFloatValues,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionDoubleKeys,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionDoubleValues,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionObjectKeys,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionObjectValues,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionColorKeys,
	GridInformation_tB66E038FEC513611EC706BB58F2EB8A62F9EB36E_CustomAttributesCacheGenerator_m_PositionColorValues,
	PipelineTile_t64C4FBC3DDED03C6B806BC75395C0040B4319285_CustomAttributesCacheGenerator_m_Sprites,
	RandomTile_tC499D4E54B5A60DC7254BD583F321251B6F6CE70_CustomAttributesCacheGenerator_m_Sprites,
	TerrainTile_tF926F13A9557D14454AC5AADC7AF2118D5E1C93C_CustomAttributesCacheGenerator_m_Sprites,
	DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_NotSupportedInWebGL,
	DemoButton_t137F4A23D860DA55E2C4A5505168231367D2FF2C_CustomAttributesCacheGenerator_TargetButton,
	DemoPackageTester_t0907E9A97CEA5E17DFB6E5E5670F5D1E60D00DD5_CustomAttributesCacheGenerator_RequiresPostProcessing,
	Burbuja_t1324D3930878CA1583FB78D4D648E7A0D5B8DD7A_CustomAttributesCacheGenerator_Burbuja_MoverIzqDr_m5A8511933345071EC18815BED888F8B0E38387E1,
	U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2__ctor_mFF418106ED642001DE5EB4EF9B1DA81E6FC9F95B,
	U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_IDisposable_Dispose_m6BD167F2FD4C89A730E88EE2143851AD001A3181,
	U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m87099B322323E87F8E5FA22020DD9F716F1DB421,
	U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_Reset_m6811DBECCC2CE3550F38BA1B956DCEA51E2FB330,
	U3CMoverIzqDrU3Ed__2_tEAA5A53032B040A3CD755CD12A540EB658790E90_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__2_System_Collections_IEnumerator_get_Current_m5773AEC31ABAB38671A73E5086BE573F14817574,
	ContadorFinal_tF8183130C720FBDBDB40640B454039392F224F13_CustomAttributesCacheGenerator_ContadorFinal_EjecutarSuma_mFE44F1E853276A4C77872A0E012DF9A86248EE98,
	U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9__ctor_m555B5D67B42B1BA719A4FDAC995620EB3494F48E,
	U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_IDisposable_Dispose_m51E2F857F838E5B63D1EB6C280C14F1AA38FA396,
	U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC01B665FC29861D3CFEF4E6B5E3C38CD1E10DE5A,
	U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_Reset_mAB9D23DD640D14BFD07A18375B560886D30407F4,
	U3CEjecutarSumaU3Ed__9_t284512F593490A534DE9534712D1EEB3EFD87002_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__9_System_Collections_IEnumerator_get_Current_mECBDCC521AAF9ADDE4EB030C11D055488FCB5E01,
	EsporaPlataforma_t8ADD6531CF86D25657A3549F7B40A9360E6C0812_CustomAttributesCacheGenerator_EsporaPlataforma_PlayerArriba_mC18CA67F1ABF04F16C77688C489BDC2557BFF05F,
	U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8__ctor_m299B668FEB88DA495D32B2611BEFCE92196184C8,
	U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_IDisposable_Dispose_mAE4D097CFF0CD28C27C270DC6C0234C976BF65B1,
	U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m190919B43D3B6C4971D2B06CD2626862F446211E,
	U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_Reset_mD517702B85C053A9320DED1EFF9909B108DD4268,
	U3CPlayerArribaU3Ed__8_tE32201D2551966462A1D8EDA76A9886E10B49DF8_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__8_System_Collections_IEnumerator_get_Current_m7DAD340B862A764D7B16D3BDCCBEC9418CBB479D,
	Estrellas_t27722F4DF54C58D1DDE3242AE4AE603A70D4DA45_CustomAttributesCacheGenerator_Estrellas_Star_mE2BC7E35D0F8BFD0911800B7E308C46F14C1B285,
	U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17__ctor_mAC92040ACB86FA97FAC8781D22D6CB531D259157,
	U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_IDisposable_Dispose_m26583F744D3AFF0D05AD5D694ACBB24C816EF83D,
	U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5EA8D7A57AB7E6EDD3DDD6DA65B8A5E9F6E0840F,
	U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_Collections_IEnumerator_Reset_m3A2F5A257120B3DE339BC03EAB7092076161BDBD,
	U3CStarU3Ed__17_tA49D811B3E8E456C47C5FDB51A430DBA5E672194_CustomAttributesCacheGenerator_U3CStarU3Ed__17_System_Collections_IEnumerator_get_Current_m01F25A7B6083895784BDE39B6C16C7D953F5451D,
	KabuBasico_tFE69878CBDDE4BBD0EFD39EF830F96043CD51FB2_CustomAttributesCacheGenerator_KabuBasico_PararYDisparar_m098F975AB2D83E3736C8D78453D8997DE57DCB07,
	U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5__ctor_m28B78F6217A3C7D8E4BB4F68EA15F9D5E26F4CB6,
	U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_IDisposable_Dispose_m3494AFC9EB836374DF75AD4B274764BB53B6E894,
	U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6E2003B8D3BE6EF61331DB120E9AE8BB915C712F,
	U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_Reset_mF0BA0302C68431F2AFFB14674A875A6AF12F6884,
	U3CPararYDispararU3Ed__5_tDE3C52BECC84B5CF78E9C0E0B738AEC6862BD2D3_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__5_System_Collections_IEnumerator_get_Current_m25FE5DDBAB2FC720A89EF7D744E38A3A77E40DCC,
	KabuEmisor_tC18A3EE157C5D089DCF6284DD6E10E012876F7E3_CustomAttributesCacheGenerator_KabuEmisor_PararYDisparar_m28F90EA2256A32A214247BA59F0C5FF70D96E29F,
	U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12__ctor_m2D700458181046A503F701ADFC8BA8DC9D3A9EB9,
	U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m7B7085B9C5D6AE8BAFC79EE702AAD249E6997E9D,
	U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6799E0EB5EAFE14DADFD640BB72A0A09470B96C,
	U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mF54B2836FF863E4F1CDF46F6498B9E100C6E4FE7,
	U3CPararYDispararU3Ed__12_tD17F609892D7CA38FE71593493F9803E6C8D4567_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_mF32B2BB1AD709293B55180110157472741FCCA06,
	KabuEmisorRafaga_t6D55AE8D80C528082263DFB1B7F58E975776E0D5_CustomAttributesCacheGenerator_KabuEmisorRafaga_PararYDisparar_m09C14B3FB2BFFE4EB0CF8E22B11D8C83820993F0,
	U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12__ctor_mF857BF4F98F66D816DFF39B81B6DF35DA228D900,
	U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_IDisposable_Dispose_m5259FEE7B1C494B388D7E2FC7D7D9476BC4A10CD,
	U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA9C65E1438072C80B375B1894990FFC643263294,
	U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_Reset_mA55C1772F133A8694C145E9CE918316A12606C97,
	U3CPararYDispararU3Ed__12_tA1745D307CE5A011895E94509B03F2C1E07ACB9F_CustomAttributesCacheGenerator_U3CPararYDispararU3Ed__12_System_Collections_IEnumerator_get_Current_m0B368BD51F913A2888E0C3E70EEBE509DFA14945,
	KabuOnda_t25E841418585C188F8782AAF36168B50D7260EAC_CustomAttributesCacheGenerator_KabuOnda_MoverIzqDr_m4FE6E82C5F835C766A913EEB83567A117E4E710F,
	U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3__ctor_m1F2F458C54014365FD64467AEB95A749273D292B,
	U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_mEE21FEE22A97265ACCD39394E3894557F0E4942C,
	U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10A5F520A464818C6522A42F2DC28C5769C38401,
	U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mFB3F2FF2AEF004BC485745191C954172F0C3C3FB,
	U3CMoverIzqDrU3Ed__3_tF34701A6B7AD64B322E9F6554A133E8E27D5E975_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_m104439B07E2DDEB5A7A71252CF2073525EF3EDD3,
	MovimientoSeta_t06B121F1FADF4A6FA90C9809D19D6B1B19BA5835_CustomAttributesCacheGenerator_MovimientoSeta_MoverIzqDr_mDD6840527C7DE1E9CE743CEB3B1498BA712792C1,
	U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3__ctor_m96CB5E50F03A4DA2462445C3D8C9B0C4954BFD1A,
	U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_IDisposable_Dispose_m2C33D7423D8CF1A4186256039D15E77B6E68C72B,
	U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA534FFD4507218231C17A4A0D559A5ABB5B9ECA3,
	U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_Reset_mF33DE026F075998BD681BB1C01F0C891CCA9EEA7,
	U3CMoverIzqDrU3Ed__3_t9DB02AB79CA73E8D77086DEBE8AE636B4E13D361_CustomAttributesCacheGenerator_U3CMoverIzqDrU3Ed__3_System_Collections_IEnumerator_get_Current_mBB7FCDF227060896BDC0F40A549A034C56C875B4,
	Pez_t5C363F201A4942DC2B18D0DA3679A7BDD0404E4E_CustomAttributesCacheGenerator_Pez_Salta_m871747B9548F5058B2B6F71E0402E979E21F1651,
	U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10__ctor_m0307177566364F8231E65778A7EFF36B440BDA85,
	U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_IDisposable_Dispose_mEACEE99BB082F725001956990EE7532B0F101703,
	U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED10B84D5D0F45C7D0218B89912F50347CFCD0A2,
	U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_Collections_IEnumerator_Reset_m0CAB56F87D69775FDBA924C157D8BA9984D6FCA0,
	U3CSaltaU3Ed__10_t48888CDA98CAF9E068067FE28282A50FC10DF9EF_CustomAttributesCacheGenerator_U3CSaltaU3Ed__10_System_Collections_IEnumerator_get_Current_mD7CB6FAB5E7BD41C0EC578DE9FA4A185A778138F,
	PlantaCarnivora_tFFC2E11DFA95D7EE3BB9C11AD588E10A7200C512_CustomAttributesCacheGenerator_PlantaCarnivora_Plantacarnivora_m615E1F73C291B71F36330C85ADEA77064B5F00EA,
	U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3__ctor_mA1FD778DB89B77632E8E9FAC03AA6CADB30A6262,
	U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_IDisposable_Dispose_mEE53C5431D11E1570FB3E2B3FD320C948929A789,
	U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26A31B3F8FA250958606C9826236F2F2F49DE751,
	U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_Reset_mD90975CD3BF73D486C1E5E51D671E7C066164778,
	U3CPlantacarnivoraU3Ed__3_t503670C6D0CD5176430045B459ED2940AF1A3A19_CustomAttributesCacheGenerator_U3CPlantacarnivoraU3Ed__3_System_Collections_IEnumerator_get_Current_mE44BC670E424CB47A3392906F03C1870F5810EED,
	PlantaEspora_t79CE382FBAAD9FDA4FB1AB353379FF2CC31DB2D6_CustomAttributesCacheGenerator_PlantaEspora_InstanciaEsporas_m206FCB2862197C6D0A89C8315150CF66AD64B9D6,
	U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6__ctor_mD138B51F39DA5DED79627D873CA8864FED69952A,
	U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_IDisposable_Dispose_m2A6E5460A7B2CAED82982944943BAC197A5276D4,
	U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8300C6B902901FDBD298B132B2EE4F344C8BE0B4,
	U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_Reset_m88576E6176233477BD4612172744765EAE8E413B,
	U3CInstanciaEsporasU3Ed__6_tA57CA644BD4E355E0C02EF4ED2483600B00EA1EB_CustomAttributesCacheGenerator_U3CInstanciaEsporasU3Ed__6_System_Collections_IEnumerator_get_Current_mBB6C82B823E03BAC16DD428ED16B53F003CE7D37,
	PlataformaCae_tB34860FEAABFFCDADD23714CEC6D5C149A228CCF_CustomAttributesCacheGenerator_PlataformaCae_PlayerArriba_m6B299C877DDF407B84EB3D2843A319570A3F7314,
	U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10__ctor_m1CC2DF67A3E6CEB1DBFE36F189607B2886A1BDD3,
	U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_IDisposable_Dispose_m654D1CD2F1EF73741B21CBAE8602DD3C23D957F9,
	U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m778E4A1A205EAC3F0ED1CFDBAB69A619839BDA8E,
	U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_Reset_m621E3B3090BDE42B7043AE2ABA1843E7D2E75907,
	U3CPlayerArribaU3Ed__10_t19B123E3F7D4B899004D05A1E46779D80858CDD4_CustomAttributesCacheGenerator_U3CPlayerArribaU3Ed__10_System_Collections_IEnumerator_get_Current_m935DF9075C85D1D5204F3CBE076F59C6040CCD27,
	Salamandra_t985C8281F5C86772E4647E9B1E852F2AC2D9C05D_CustomAttributesCacheGenerator_Salamandra_DisparaSalta_m313C54FB7866FB593AB9DD00299606A15951E5FA,
	U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11__ctor_mD9001ABB51207AF811B15948966A3EAE6D1730AC,
	U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_IDisposable_Dispose_m5CC9EBA93013AD7E12FC6C0137613B304F6970FF,
	U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03C65B1CD417703E42F52AB854B415BCEE0E547D,
	U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_Reset_mCCE91D7C239B131EB0F3E1A48DFF350E49D8B719,
	U3CDisparaSaltaU3Ed__11_t9F605A2EB269D546FC2AC95669B3598F781E7A42_CustomAttributesCacheGenerator_U3CDisparaSaltaU3Ed__11_System_Collections_IEnumerator_get_Current_m069AD940AD90A91ABEF88800EFD5446C7C2FB455,
	SetaExplosiva_tD4D853F68CA237BC6D6F6DEF52F7A718BCA3C3A7_CustomAttributesCacheGenerator_SetaExplosiva_MoveExpl_mB966BBEBABB361812827BB32A8B975949A978756,
	U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20__ctor_m1AD6BC85565B85E3097D42B879DECCC2ECF3EA19,
	U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_IDisposable_Dispose_mB6769B0CE3DFAD199D3175811B6FAE67C028085B,
	U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0289ACBC55AC3361224DFCBCDC5E240F82DD30F5,
	U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_Collections_IEnumerator_Reset_m27878F4BCBD7CA8ACF397C1E2B0F76C8327E4182,
	U3CMoveExplU3Ed__20_t58ADF0DCD646584EB90387766715D9ADA8C89241_CustomAttributesCacheGenerator_U3CMoveExplU3Ed__20_System_Collections_IEnumerator_get_Current_m79B6D36E92E37A5F984F552BCA6C96B736848F12,
	SpawnBurbujas_t8E962568AA1B2B02F102849823690BBA8C989DB8_CustomAttributesCacheGenerator_SpawnBurbujas_InstanciaVoladores_m8E4D3E9E79075B79CC7764F778B63845ABE86DC2,
	U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5__ctor_m566BC2C949479B61D744B9C4A6F22A77258986EC,
	U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mF94EEE8EE391F6B50244657DB59C85F52537E58A,
	U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5F857AE3CAD2009297B73E6FF08D60551492A68,
	U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mB9573B135681505A6F5D8DE51C2BA457F86B8B39,
	U3CInstanciaVoladoresU3Ed__5_tF96DBC35CDEABA8ED82D5F51BBE48FE35BC642DC_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_mCDC3EFAE58163D472B6C186624B99AF52E770AD4,
	SpawnPlanta_t2D3C351C7D837CC0CEDF8DCEA7E936549CB3B294_CustomAttributesCacheGenerator_SpawnPlanta_Spawn_m9A239121B02E5C48C32F6A03109EA6B985D31778,
	U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7__ctor_m8F2FCB7CD60C3C16C3684449CC19D8A62C0FB30C,
	U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_IDisposable_Dispose_mDD5B5C4073274DD53D03539305093E05A62ABB80,
	U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m41D02349281E8475CBA467DAD138C068AB0B46B2,
	U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_Collections_IEnumerator_Reset_m725CDFFF87F29DF99BD34EC091B742DE77E0C331,
	U3CSpawnU3Ed__7_t2A02056202B24068A0312475D7CCFCE010E32635_CustomAttributesCacheGenerator_U3CSpawnU3Ed__7_System_Collections_IEnumerator_get_Current_mE08A8F64462C87D5CA0515DEA6AD51C86A7EC342,
	SpawnVoladores_t510DFFE8436CABEE8B91BD764B692FECEE8D963C_CustomAttributesCacheGenerator_SpawnVoladores_InstanciaVoladores_m5D18C26DBDBE5B44417CC3D1FC50C8F2DC89BFF7,
	U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5__ctor_m6A47693939320F7DACB1B4C4CBDB197C79CA6D80,
	U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_IDisposable_Dispose_mA4EC4C5F217E444594EB675DE05504890EDDA72C,
	U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF179B25FE4C16D4D2884B3FA9667F13E64D8A246,
	U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_Reset_mA33C0021369B8C22FEE3E1FFA897E2F744C5C139,
	U3CInstanciaVoladoresU3Ed__5_t5D5853E4CE7F56728FF3890AA7D15EED6C80D7EB_CustomAttributesCacheGenerator_U3CInstanciaVoladoresU3Ed__5_System_Collections_IEnumerator_get_Current_m717DEBC2BB5F4397E38DDBAF05883A8899CE1ECC,
	UiQuieto_tC9C1C9A8F3CC755BCD2DDA4D9D335C65B7EDD2A7_CustomAttributesCacheGenerator_UiQuieto_DispararIzq_m161DD5C46E0D042FFFBD2CB880C084BE7A3EE7F2,
	UiQuieto_tC9C1C9A8F3CC755BCD2DDA4D9D335C65B7EDD2A7_CustomAttributesCacheGenerator_UiQuieto_DispararDrc_m559535A821ABE56F02C4AC2B2CB2CD7E1158EF85,
	U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11__ctor_m3C73C4D06B254E45AEA5179164A8B11AEE17FD06,
	U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_IDisposable_Dispose_mF422CF6AE6D3C5467F30E2A9425C88B5103DCAD8,
	U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEDD72EB319620E3C152D71A1B166D134C606B21D,
	U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_Reset_m2169F2504BDFFD82B5EBF4CD0B1C644A50E9B14A,
	U3CDispararIzqU3Ed__11_t6B07ACB2CB830B86ED96CCECC14A1C702059D7AF_CustomAttributesCacheGenerator_U3CDispararIzqU3Ed__11_System_Collections_IEnumerator_get_Current_mD25026294FDF7D508B82F96AFE59540070282C6F,
	U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12__ctor_m80D8E27DB0037C73FA28E09D588266AE3ECBDDBA,
	U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_IDisposable_Dispose_mFA330C43C7F50AACD3CBF37DFB682861F5B395B3,
	U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5D48A12FF98EBACE1C0146CFDDD151E9C80432A1,
	U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_Reset_mAB2E8B6BC2CC8C357054304C212B17198FD7495C,
	U3CDispararDrcU3Ed__12_tDA049AFBD90BA91102DB0AB4AE04C347AEEFD10B_CustomAttributesCacheGenerator_U3CDispararDrcU3Ed__12_System_Collections_IEnumerator_get_Current_mA18797AA09FFA1698A00A87A9356C39733BD70CB,
	contadorAerena_t19733C65D7D3675C9D702F54D5A6E41675993B1D_CustomAttributesCacheGenerator_contadorAerena_EjecutarSuma_mEC3153784BA9B13858B65F778A0E0A83378150ED,
	U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8__ctor_mB673D2B05A6FB785A327713F2319440CFC0E92C5,
	U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_IDisposable_Dispose_mD1CB9B1643C7430D4E3FFCFA7DDB7A2153BC6FA8,
	U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m680481E7056FF135C717AD069C76864E13EF1E96,
	U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_Reset_m3744EBE3720715716DC0495BC10DB972486551D8,
	U3CEjecutarSumaU3Ed__8_tF980D07F40B14BB6E902F0B5EFA9DCACCB2547D2_CustomAttributesCacheGenerator_U3CEjecutarSumaU3Ed__8_System_Collections_IEnumerator_get_Current_mD1812047E81A84A1D89D2A3CDE3FE94CCCCA954F,
	DemoBall_t2DC8C97097BDF12123C9ECFAA6E1926345A2BC28_CustomAttributesCacheGenerator_DemoBall_ProgrammedDeath_m2DF4845C32A8963BAB27D7F0BFACC5784712BE92,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3__ctor_m594CF3FB53C5D8B5ED586479D34EE72E4943BF2F,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_IDisposable_Dispose_m543CD3EBD9CAC69065A27E5644411AFE8B6C7B6A,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC82FC1704348F2794A1AEB9E8C43335F3189EDEA,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_Reset_m62594DC3054FFC736C7286487579485F6501901F,
	U3CProgrammedDeathU3Ed__3_t66AB9997CF14E4C726CFF62F618BBFC4DABC3C14_CustomAttributesCacheGenerator_U3CProgrammedDeathU3Ed__3_System_Collections_IEnumerator_get_Current_mA0F24F509E8DF2CC5413AC39750F9F5B91179303,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// MoreMountains.Tools.MMConditionAttribute
struct MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16;
// MoreMountains.Tools.MMHiddenAttribute
struct MMHiddenAttribute_tB5F49A623FDE00E5947BEF4035CE4F0E351622B6;
// MoreMountains.Tools.MMInformationAttribute
struct MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45;
// MoreMountains.Tools.MMInspectorButtonAttribute
struct MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898;
// MoreMountains.Tools.MMReadOnlyAttribute
struct MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// MoreMountains.Tools.MMConditionAttribute
struct MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMConditionAttribute::ConditionBoolean
	String_t* ___ConditionBoolean_0;
	// System.Boolean MoreMountains.Tools.MMConditionAttribute::Hidden
	bool ___Hidden_1;

public:
	inline static int32_t get_offset_of_ConditionBoolean_0() { return static_cast<int32_t>(offsetof(MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16, ___ConditionBoolean_0)); }
	inline String_t* get_ConditionBoolean_0() const { return ___ConditionBoolean_0; }
	inline String_t** get_address_of_ConditionBoolean_0() { return &___ConditionBoolean_0; }
	inline void set_ConditionBoolean_0(String_t* value)
	{
		___ConditionBoolean_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionBoolean_0), (void*)value);
	}

	inline static int32_t get_offset_of_Hidden_1() { return static_cast<int32_t>(offsetof(MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16, ___Hidden_1)); }
	inline bool get_Hidden_1() const { return ___Hidden_1; }
	inline bool* get_address_of_Hidden_1() { return &___Hidden_1; }
	inline void set_Hidden_1(bool value)
	{
		___Hidden_1 = value;
	}
};


// MoreMountains.Tools.MMHiddenAttribute
struct MMHiddenAttribute_tB5F49A623FDE00E5947BEF4035CE4F0E351622B6  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// MoreMountains.Tools.MMInformationAttribute
struct MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// MoreMountains.Tools.MMInspectorButtonAttribute
struct MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMInspectorButtonAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_0), (void*)value);
	}
};


// MoreMountains.Tools.MMReadOnlyAttribute
struct MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMInformationAttribute/InformationType
struct InformationType_t633DB4CABDA4EA9C03B284CF077738EFB3C2571A 
{
public:
	// System.Int32 MoreMountains.Tools.MMInformationAttribute/InformationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InformationType_t633DB4CABDA4EA9C03B284CF077738EFB3C2571A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMInformationAttribute::.ctor(System.String,MoreMountains.Tools.MMInformationAttribute/InformationType,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588 (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * __this, String_t* ___message0, int32_t ___type1, bool ___messageAfterProperty2, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMConditionAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * __this, String_t* ___conditionBoolean0, bool ___hideInInspector1, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMReadOnlyAttribute__ctor_m542563E5945344E9F070E568F1073E7325B59374 (MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SelectionBaseAttribute__ctor_mDCDA943585A570BA4243FEFB022DABA360910E11 (SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMHiddenAttribute__ctor_m507BA5DB8F4A54D459DBF54572723651FD8F89B6 (MMHiddenAttribute_tB5F49A623FDE00E5947BEF4035CE4F0E351622B6 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMInspectorButtonAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6 (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * __this, String_t* ___MethodName0, const RuntimeMethod* method);
static void MoreMountains_InventoryEngine_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_PlayerID(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x49\x44"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x75\x6E\x69\x71\x75\x65\x20\x49\x44\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x69\x64\x65\x6E\x74\x69\x66\x79\x20\x74\x68\x65\x20\x6F\x77\x6E\x65\x72\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Content(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x69\x73\x20\x61\x20\x72\x65\x61\x6C\x74\x69\x6D\x65\x20\x76\x69\x65\x77\x20\x6F\x66\x20\x79\x6F\x75\x72\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x27\x73\x20\x63\x6F\x6E\x74\x65\x6E\x74\x73\x2E\x20\x44\x6F\x6E\x27\x74\x20\x6D\x6F\x64\x69\x66\x79\x20\x74\x68\x69\x73\x20\x6C\x69\x73\x74\x20\x76\x69\x61\x20\x74\x68\x65\x20\x69\x6E\x73\x70\x65\x63\x74\x6F\x72\x2C\x20\x69\x74\x27\x73\x20\x76\x69\x73\x69\x62\x6C\x65\x20\x66\x6F\x72\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x70\x75\x72\x70\x6F\x73\x65\x73\x20\x6F\x6E\x6C\x79\x2E"), NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_InventoryType(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x54\x79\x70\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x27\x73\x20\x74\x79\x70\x65\x2E\x20\x4D\x61\x69\x6E\x20\x61\x72\x65\x20\x27\x72\x65\x67\x75\x6C\x61\x72\x27\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x69\x65\x73\x2E\x20\x45\x71\x75\x69\x70\x6D\x65\x6E\x74\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x69\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x62\x6F\x75\x6E\x64\x20\x74\x6F\x20\x61\x20\x63\x65\x72\x74\x61\x69\x6E\x20\x69\x74\x65\x6D\x20\x63\x6C\x61\x73\x73\x20\x61\x6E\x64\x20\x68\x61\x76\x65\x20\x64\x65\x64\x69\x63\x61\x74\x65\x64\x20\x6F\x70\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_TargetTransform(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x20\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x54\x61\x72\x67\x65\x74\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x69\x73\x20\x61\x6E\x79\x20\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x64\x72\x6F\x70\x70\x65\x64\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x77\x69\x6C\x6C\x20\x73\x70\x61\x77\x6E\x2E"), NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Persistent(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x69\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x73\x68\x6F\x75\x6C\x64\x20\x72\x65\x73\x70\x6F\x6E\x64\x20\x74\x6F\x20\x4C\x6F\x61\x64\x20\x61\x6E\x64\x20\x53\x61\x76\x65\x20\x65\x76\x65\x6E\x74\x73\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x64\x6F\x6E\x27\x74\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x68\x61\x76\x65\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x73\x61\x76\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x6B\x2C\x20\x73\x65\x74\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x66\x61\x6C\x73\x65\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x68\x61\x76\x65\x20\x69\x74\x20\x72\x65\x73\x65\x74\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x2C\x20\x74\x6F\x20\x6D\x61\x6B\x65\x20\x73\x75\x72\x65\x20\x69\x74\x27\x73\x20\x61\x6C\x77\x61\x79\x73\x20\x65\x6D\x70\x74\x79\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x6C\x65\x76\x65\x6C\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x73\x69\x73\x74\x65\x6E\x63\x79"), NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_DrawContentInInspector(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6C\x69\x6B\x65\x20\x74\x68\x65\x20\x64\x61\x74\x61\x62\x61\x73\x65\x20\x61\x6E\x64\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x20\x70\x61\x72\x74\x20\x6F\x66\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E\x20\x49\x74\x20\x77\x6F\x6E\x27\x74\x20\x73\x68\x6F\x77\x20\x61\x6E\x79\x74\x68\x69\x6E\x67\x20\x6F\x6E\x20\x73\x63\x72\x65\x65\x6E\x2C\x20\x79\x6F\x75\x27\x6C\x6C\x20\x6E\x65\x65\x64\x20\x61\x6C\x73\x6F\x20\x61\x6E\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x20\x66\x6F\x72\x20\x74\x68\x61\x74\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x63\x69\x64\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x6F\x75\x74\x70\x75\x74\x20\x61\x20\x64\x65\x62\x75\x67\x20\x63\x6F\x6E\x74\x65\x6E\x74\x20\x69\x6E\x20\x74\x68\x65\x20\x69\x6E\x73\x70\x65\x63\x74\x6F\x72\x20\x28\x75\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x64\x65\x62\x75\x67\x67\x69\x6E\x67\x29\x2E"), NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_U3COwnerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Inventory_get_Owner_mE01A17C278F484A328297B5708967E25C9795D25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Inventory_set_Owner_m676D8E71B989E750CF110AB1DE22944089FB2B39(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_TargetInventoryContainer(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x42\x69\x6E\x64\x20\x68\x65\x72\x65\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x28\x74\x68\x65\x20\x43\x61\x6E\x76\x61\x73\x47\x72\x6F\x75\x70\x20\x74\x68\x61\x74\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x74\x75\x72\x6E\x20\x6F\x6E\x2F\x6F\x66\x66\x20\x77\x68\x65\x6E\x20\x6F\x70\x65\x6E\x69\x6E\x67\x2F\x63\x6C\x6F\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x29\x2C\x20\x79\x6F\x75\x72\x20\x6D\x61\x69\x6E\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6F\x76\x65\x72\x6C\x61\x79\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x75\x6E\x64\x65\x72\x20\x74\x68\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x6F\x70\x65\x6E\x65\x64\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x73"), NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_HideContainerOnStart(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x73\x65\x74\x20\x48\x69\x64\x65\x43\x6F\x6E\x74\x61\x69\x6E\x65\x72\x4F\x6E\x53\x74\x61\x72\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x54\x61\x72\x67\x65\x74\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x43\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x64\x65\x66\x69\x6E\x65\x64\x20\x72\x69\x67\x68\x74\x20\x61\x62\x6F\x76\x65\x20\x74\x68\x69\x73\x20\x66\x69\x65\x6C\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x68\x69\x64\x64\x65\x6E\x20\x6F\x6E\x20\x53\x74\x61\x72\x74\x2C\x20\x65\x76\x65\x6E\x20\x69\x66\x20\x79\x6F\x75\x27\x76\x65\x20\x6C\x65\x66\x74\x20\x69\x74\x20\x76\x69\x73\x69\x62\x6C\x65\x20\x69\x6E\x20\x53\x63\x65\x6E\x65\x20\x76\x69\x65\x77\x2E\x20\x55\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x73\x65\x74\x75\x70\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InputOnlyWhenOpen(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x6D\x69\x73\x73\x69\x6F\x6E\x73"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x63\x69\x64\x65\x20\x74\x6F\x20\x68\x61\x76\x65\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x63\x61\x74\x63\x68\x20\x69\x6E\x70\x75\x74\x20\x6F\x6E\x6C\x79\x20\x77\x68\x65\x6E\x20\x6F\x70\x65\x6E\x2C\x20\x6F\x72\x20\x6E\x6F\x74\x2E"), 1LL, false, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_ToggleInventoryKey(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x65\x79\x20\x4D\x61\x70\x70\x69\x6E\x67"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x6E\x65\x65\x64\x20\x74\x6F\x20\x73\x65\x74\x20\x74\x68\x65\x20\x76\x61\x72\x69\x6F\x75\x73\x20\x6B\x65\x79\x20\x62\x69\x6E\x64\x69\x6E\x67\x73\x20\x79\x6F\x75\x20\x70\x72\x65\x66\x65\x72\x2E\x20\x54\x68\x65\x72\x65\x20\x61\x72\x65\x20\x73\x6F\x6D\x65\x20\x62\x79\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x62\x75\x74\x20\x66\x65\x65\x6C\x20\x66\x72\x65\x65\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x74\x68\x65\x6D\x2E"), 1LL, false, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_CloseList(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6C\x6F\x73\x65\x20\x42\x69\x6E\x64\x69\x6E\x67\x73"), NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_ManageButtons(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x74\x74\x6F\x6E\x73"), NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_ManageButtonsMode(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_EquipUseButton(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_MoveButton(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_DropButton(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_EquipButton(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_UseButton(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_UnEquipButton(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x61\x67\x65\x42\x75\x74\x74\x6F\x6E\x73"), true, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_U3CCurrentlySelectedInventorySlotU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InventoryIsOpen(CustomAttributesCache* cache)
{
	{
		MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 * tmp = (MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 *)cache->attributes[0];
		MMReadOnlyAttribute__ctor_m542563E5945344E9F070E568F1073E7325B59374(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x65"), NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InventoryInputManager_get_CurrentlySelectedInventorySlot_mE06883E42278A8B2A4DFF27879C4CFD2EF38AEC8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InventoryInputManager_set_CurrentlySelectedInventorySlot_m7651E8F8A7EF0159C3CC275FFFFA09F56DE108B4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ItemID(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x44\x20\x61\x6E\x64\x20\x54\x61\x72\x67\x65\x74"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x75\x6E\x69\x71\x75\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x79\x6F\x75\x72\x20\x6F\x62\x6A\x65\x63\x74\x2E"), 1LL, false, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_TargetIndex(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x63\x65\x53\x6C\x6F\x74\x49\x6E\x64\x65\x78"), true, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Usable(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x6D\x69\x73\x73\x69\x6F\x6E\x73"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x79\x6F\x75\x72\x20\x6F\x62\x6A\x65\x63\x74\x20\x69\x73\x20\x55\x73\x61\x62\x6C\x65\x2C\x20\x45\x71\x75\x69\x70\x70\x61\x62\x6C\x65\x2C\x20\x6F\x72\x20\x62\x6F\x74\x68\x2E\x20\x55\x73\x61\x62\x6C\x65\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x61\x72\x65\x20\x74\x79\x70\x69\x63\x61\x6C\x6C\x79\x20\x62\x6F\x6D\x62\x73\x2C\x20\x70\x6F\x74\x69\x6F\x6E\x73\x2C\x20\x73\x74\x75\x66\x66\x20\x6C\x69\x6B\x65\x20\x74\x68\x61\x74\x2E\x20\x45\x71\x75\x69\x70\x70\x61\x62\x6C\x65\x73\x20\x61\x72\x65\x20\x75\x73\x75\x61\x6C\x6C\x79\x20\x77\x65\x61\x70\x6F\x6E\x73\x20\x6F\x72\x20\x61\x72\x6D\x6F\x72\x2E"), 1LL, false, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Consumable(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x61\x62\x6C\x65"), true, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ConsumeQuantity(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x73\x75\x6D\x61\x62\x6C\x65"), true, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Quantity(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ItemName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x73\x69\x63\x20\x69\x6E\x66\x6F"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x61\x73\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x69\x74\x20\x74\x6F\x20\x61\x70\x70\x65\x61\x72\x20\x69\x6E\x20\x74\x68\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x70\x61\x6E\x65\x6C"), 1LL, false, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ShortDescription(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x53\x68\x6F\x72\x74\x20\x61\x6E\x64\x20\x27\x6C\x6F\x6E\x67\x27\x20\x64\x65\x73\x63\x72\x69\x70\x74\x69\x6F\x6E\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x69\x6E\x20\x74\x68\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x65\x74\x61\x69\x6C\x73\x20\x70\x61\x6E\x65\x6C\x2E"), 1LL, false, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Description(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Icon(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6D\x61\x67\x65\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x73\x69\x64\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x20\x70\x61\x6E\x65\x6C\x73\x20\x61\x6E\x64\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x65\x74\x61\x69\x6C\x73\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6D\x61\x67\x65"), NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Prefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x20\x44\x72\x6F\x70"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x70\x61\x77\x6E\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x62\x65\x20\x64\x72\x6F\x70\x70\x65\x64\x20\x66\x72\x6F\x6D\x20\x69\x74\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x73\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x6D\x69\x6E\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x73\x70\x61\x77\x6E\x65\x64\x2E"), 1LL, false, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_MaximumStack(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x74\x61\x63\x6B\x65\x64\x20\x28\x6D\x75\x6C\x74\x69\x70\x6C\x65\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x73\x20\x69\x6E\x20\x61\x20\x73\x69\x6E\x67\x6C\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x73\x6C\x6F\x74\x29\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x70\x65\x63\x69\x66\x79\x20\x68\x65\x72\x65\x20\x74\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x61\x74\x20\x73\x74\x61\x63\x6B\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x73\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x20\x63\x6C\x61\x73\x73\x20\x28\x75\x73\x65\x66\x75\x6C\x20\x66\x6F\x72\x20\x65\x71\x75\x69\x70\x6D\x65\x6E\x74\x20\x69\x74\x65\x6D\x73\x20\x6D\x6F\x73\x74\x6C\x79\x29"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x50\x72\x6F\x70\x65\x72\x74\x69\x65\x73"), NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_TargetEquipmentInventoryName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x71\x75\x69\x70\x70\x61\x62\x6C\x65"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x65\x71\x75\x69\x70\x70\x61\x62\x6C\x65\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x65\x74\x20\x68\x65\x72\x65\x20\x69\x74\x73\x20\x74\x61\x72\x67\x65\x74\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x6E\x61\x6D\x65\x20\x28\x66\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x20\x41\x72\x6D\x6F\x72\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x29\x2E\x20\x4F\x66\x20\x63\x6F\x75\x72\x73\x65\x20\x79\x6F\x75\x27\x6C\x6C\x20\x6E\x65\x65\x64\x20\x61\x6E\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x77\x69\x74\x68\x20\x61\x20\x6D\x61\x74\x63\x68\x69\x6E\x67\x20\x6E\x61\x6D\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x73\x70\x65\x63\x69\x66\x79\x20\x61\x20\x73\x6F\x75\x6E\x64\x20\x74\x6F\x20\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x65\x71\x75\x69\x70\x70\x65\x64\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x64\x6F\x6E\x27\x74\x2C\x20\x61\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x73\x6F\x75\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x2E"), 1LL, false, NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_UsedSound(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x69\x74\x65\x6D\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x65\x74\x20\x68\x65\x72\x65\x20\x61\x20\x73\x6F\x75\x6E\x64\x20\x74\x6F\x20\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x69\x74\x20\x67\x65\x74\x73\x20\x75\x73\x65\x64\x2C\x20\x69\x66\x20\x79\x6F\x75\x20\x64\x6F\x6E\x27\x74\x20\x61\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x73\x6F\x75\x6E\x64\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x61\x62\x6C\x65"), NULL);
	}
}
static void InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_MovedSound(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x74\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x73\x6F\x75\x6E\x64\x73\x20\x66\x6F\x72\x20\x6D\x6F\x76\x65\x20\x61\x6E\x64\x20\x64\x72\x6F\x70\x20\x65\x76\x65\x6E\x74\x73\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64\x73"), NULL);
	}
}
static void ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7_CustomAttributesCacheGenerator_Item(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x74\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x6F\x20\x61\x20\x54\x72\x69\x67\x67\x65\x72\x20\x62\x6F\x78\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x20\x32\x44\x20\x61\x6E\x64\x20\x69\x74\x27\x6C\x6C\x20\x6D\x61\x6B\x65\x20\x69\x74\x20\x70\x69\x63\x6B\x61\x62\x6C\x65\x2C\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x61\x64\x64\x20\x74\x68\x65\x20\x73\x70\x65\x63\x69\x66\x69\x65\x64\x20\x69\x74\x65\x6D\x20\x74\x6F\x20\x69\x74\x73\x20\x74\x61\x72\x67\x65\x74\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E\x20\x4A\x75\x73\x74\x20\x64\x72\x61\x67\x20\x61\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x6C\x79\x20\x63\x72\x65\x61\x74\x65\x64\x20\x69\x74\x65\x6D\x20\x69\x6E\x74\x6F\x20\x74\x68\x65\x20\x73\x6C\x6F\x74\x20\x62\x65\x6C\x6F\x77\x2E\x20\x46\x6F\x72\x20\x6D\x6F\x72\x65\x20\x61\x62\x6F\x75\x74\x20\x68\x6F\x77\x20\x74\x6F\x20\x63\x72\x65\x61\x74\x65\x20\x69\x74\x65\x6D\x73\x2C\x20\x68\x61\x76\x65\x20\x61\x20\x6C\x6F\x6F\x6B\x20\x61\x74\x20\x74\x68\x65\x20\x64\x6F\x63\x75\x6D\x65\x6E\x74\x61\x74\x69\x6F\x6E\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x73\x70\x65\x63\x69\x66\x79\x20\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x6F\x66\x20\x74\x68\x61\x74\x20\x69\x74\x65\x6D\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x70\x69\x63\x6B\x65\x64\x20\x77\x68\x65\x6E\x20\x70\x69\x63\x6B\x69\x6E\x67\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), 1LL, false, NULL);
	}
}
static void ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7_CustomAttributesCacheGenerator_Quantity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x69\x63\x6B\x20\x51\x75\x61\x6E\x74\x69\x74\x79"), NULL);
	}
}
static void ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7_CustomAttributesCacheGenerator_PickableIfInventoryIsFull(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x64\x69\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_TargetInventoryName(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x63\x69\x66\x79\x20\x68\x65\x72\x65\x20\x74\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x77\x68\x6F\x73\x65\x20\x63\x6F\x6E\x74\x65\x6E\x74\x27\x73\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x69\x6E\x20\x74\x68\x69\x73\x20\x44\x65\x74\x61\x69\x6C\x73\x20\x70\x61\x6E\x65\x6C\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x61\x6C\x73\x6F\x20\x64\x65\x63\x69\x64\x65\x20\x74\x6F\x20\x6D\x61\x6B\x65\x20\x69\x74\x20\x67\x6C\x6F\x62\x61\x6C\x2E\x20\x49\x66\x20\x79\x6F\x75\x20\x64\x6F\x20\x73\x6F\x2C\x20\x69\x74\x27\x6C\x6C\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x6F\x66\x20\x61\x6C\x6C\x20\x69\x74\x65\x6D\x73\x2C\x20\x72\x65\x67\x61\x72\x64\x6C\x65\x73\x73\x20\x6F\x66\x20\x74\x68\x65\x69\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E"), 1LL, false, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_U3CHiddenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_HideOnEmptySlot(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x42\x79\x20\x63\x68\x65\x63\x6B\x69\x6E\x67\x20\x48\x69\x64\x65\x4F\x6E\x45\x6D\x70\x74\x79\x53\x6C\x6F\x74\x2C\x20\x74\x68\x65\x20\x44\x65\x74\x61\x69\x6C\x73\x20\x70\x61\x6E\x65\x6C\x20\x77\x6F\x6E\x27\x74\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x66\x20\x79\x6F\x75\x20\x73\x65\x6C\x65\x63\x74\x20\x61\x6E\x20\x65\x6D\x70\x74\x79\x20\x73\x6C\x6F\x74\x2E"), 1LL, false, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_DefaultTitle(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x65\x74\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x76\x61\x6C\x75\x65\x73\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x66\x69\x65\x6C\x64\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x70\x61\x6E\x65\x6C\x2E\x20\x54\x68\x65\x73\x65\x20\x76\x61\x6C\x75\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x6E\x6F\x20\x69\x74\x65\x6D\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x28\x61\x6E\x64\x20\x69\x66\x20\x79\x6F\x75\x27\x76\x65\x20\x63\x68\x6F\x73\x65\x6E\x20\x6E\x6F\x74\x20\x74\x6F\x20\x68\x69\x64\x65\x20\x74\x68\x65\x20\x70\x61\x6E\x65\x6C\x20\x69\x6E\x20\x74\x68\x61\x74\x20\x63\x61\x73\x65\x29\x2E"), 1LL, false, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_HideOnStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x63\x69\x64\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x68\x69\x64\x65\x20\x74\x68\x65\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x70\x61\x6E\x65\x6C\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x2E"), 1LL, false, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_Icon(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x6E\x65\x65\x64\x20\x74\x6F\x20\x62\x69\x6E\x64\x20\x74\x68\x65\x20\x70\x61\x6E\x65\x6C\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x73\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x73"), NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_get_Hidden_mCE81253E9F4208C1F5F2BEC300BEB2B5CCA66226(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_set_Hidden_m9AD3E32E93E637B972DBB06255E347C2E9092A12(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_FillDetailFields_m28C0F7CDA206A0D61631B6B534618C56A9390BFE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_0_0_0_var), NULL);
	}
}
static void InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_FillDetailFieldsWithDefaults_m7E866BA1CC220EF12FB1DC643D40D26595FC798E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_0_0_0_var), NULL);
	}
}
static void U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23__ctor_m30FD6F764B454974959D118019025E6BDCF52937(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_IDisposable_Dispose_mB1DC5B14A9DF711E14850109BAABF4C60E420B6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m368AA5564217C2D7DE19BD1CB4C88E7D5282B76B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_Reset_m713299BF6202DADB0DA364E926981B4A8DAB8C4E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_get_Current_m4F2A03610ECA92C775228D6821CC7DFDBB601EA1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24__ctor_m1F16C86295811C4D6AAC91674C187CCC050291B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_IDisposable_Dispose_m76EA674ABC0192E325573EC331D4BFBD827C90A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8D81549F524FC8A566A13D76A26F0C2BCBBA4CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_Reset_mC5189493E7C2E1A47226CC7DBB9BDB5EA1711A24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_get_Current_m0F3E0BF9F8914A43406764CD643B3E857434AD2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB * tmp = (SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB *)cache->attributes[0];
		SelectionBaseAttribute__ctor_mDCDA943585A570BA4243FEFB022DABA360910E11(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_TargetInventoryName(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x20\x69\x73\x20\x61\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x68\x61\x6E\x64\x6C\x65\x20\x74\x68\x65\x20\x76\x69\x73\x75\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x61\x74\x61\x20\x63\x6F\x6E\x74\x61\x69\x6E\x65\x64\x20\x69\x6E\x20\x61\x6E\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E\x20\x53\x74\x61\x72\x74\x20\x62\x79\x20\x73\x70\x65\x63\x69\x66\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x69\x6E\x64\x69\x6E\x67"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_NumberOfRows(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x20\x70\x72\x65\x73\x65\x6E\x74\x73\x20\x61\x6E\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x27\x73\x20\x64\x61\x74\x61\x20\x69\x6E\x20\x73\x6C\x6F\x74\x73\x20\x63\x6F\x6E\x74\x61\x69\x6E\x69\x6E\x67\x20\x6F\x6E\x65\x20\x69\x74\x65\x6D\x20\x65\x61\x63\x68\x2C\x20\x61\x6E\x64\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x69\x6E\x20\x61\x20\x67\x72\x69\x64\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x65\x74\x20\x68\x6F\x77\x20\x6D\x61\x6E\x79\x20\x72\x6F\x77\x73\x20\x61\x6E\x64\x20\x63\x6F\x6C\x75\x6D\x6E\x73\x20\x6F\x66\x20\x73\x6C\x6F\x74\x73\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x2E\x20\x4F\x6E\x63\x65\x20\x79\x6F\x75\x27\x72\x65\x20\x68\x61\x70\x70\x79\x20\x77\x69\x74\x68\x20\x79\x6F\x75\x72\x20\x73\x65\x74\x74\x69\x6E\x67\x73\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x70\x72\x65\x73\x73\x20\x74\x68\x65\x20\x27\x61\x75\x74\x6F\x20\x73\x65\x74\x75\x70\x27\x20\x62\x75\x74\x74\x6F\x6E\x20\x61\x74\x20\x74\x68\x65\x20\x62\x6F\x74\x74\x6F\x6D\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x69\x6E\x73\x70\x65\x63\x74\x6F\x72\x20\x74\x6F\x20\x73\x65\x65\x20\x79\x6F\x75\x72\x20\x63\x68\x61\x6E\x67\x65\x73\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x69\x7A\x65"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_TargetChoiceInventory(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x64\x69\x73\x70\x6C\x61\x79\x73\x20\x74\x68\x65\x20\x63\x6F\x6E\x74\x65\x6E\x74\x73\x20\x6F\x66\x20\x61\x6E\x20\x45\x71\x75\x69\x70\x6D\x65\x6E\x74\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2C\x20\x79\x6F\x75\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x69\x6E\x64\x20\x68\x65\x72\x65\x20\x61\x20\x43\x68\x6F\x69\x63\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E\x20\x41\x20\x43\x68\x6F\x69\x63\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x69\x73\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x69\x6E\x20\x77\x68\x69\x63\x68\x20\x79\x6F\x75\x27\x6C\x6C\x20\x70\x69\x63\x6B\x20\x69\x74\x65\x6D\x73\x20\x66\x6F\x72\x20\x79\x6F\x75\x72\x20\x65\x71\x75\x69\x70\x6D\x65\x6E\x74\x2E\x20\x55\x73\x75\x61\x6C\x6C\x79\x20\x74\x68\x65\x20\x43\x68\x6F\x69\x63\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x69\x73\x20\x74\x68\x65\x20\x4D\x61\x69\x6E\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E\x20\x41\x67\x61\x69\x6E\x2C\x20\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x61\x6E\x20\x65\x71\x75\x69\x70\x6D\x65\x6E\x74\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x70\x65\x63\x69\x66\x79\x20\x77\x68\x61\x74\x20\x63\x6C\x61\x73\x73\x20\x6F\x66\x20\x69\x74\x65\x6D\x73\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x61\x75\x74\x68\x6F\x72\x69\x7A\x65\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x71\x75\x69\x70\x6D\x65\x6E\x74"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_DrawEmptySlots(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x20\x73\x65\x74\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x74\x72\x75\x65\x2C\x20\x65\x6D\x70\x74\x79\x20\x73\x6C\x6F\x74\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x72\x61\x77\x6E\x2C\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x74\x68\x65\x79\x27\x6C\x6C\x20\x62\x65\x20\x68\x69\x64\x64\x65\x6E\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_PaddingTop(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x50\x61\x64\x64\x69\x6E\x67"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x70\x61\x64\x64\x69\x6E\x67\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x68\x65\x20\x62\x6F\x72\x64\x65\x72\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x70\x61\x6E\x65\x6C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x73\x6C\x6F\x74\x73\x2E"), 1LL, false, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_SlotPrefab(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x6E\x20\x70\x72\x65\x73\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x61\x75\x74\x6F\x20\x73\x65\x74\x75\x70\x20\x62\x75\x74\x74\x6F\x6E\x20\x61\x74\x20\x74\x68\x65\x20\x62\x6F\x74\x74\x6F\x6D\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2C\x20\x74\x68\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x44\x69\x73\x70\x6C\x61\x79\x20\x77\x69\x6C\x6C\x20\x66\x69\x6C\x6C\x20\x69\x74\x73\x65\x6C\x66\x20\x77\x69\x74\x68\x20\x73\x6C\x6F\x74\x73\x20\x72\x65\x61\x64\x79\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x27\x73\x20\x63\x6F\x6E\x74\x65\x6E\x74\x73\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x73\x6C\x6F\x74\x27\x73\x20\x73\x69\x7A\x65\x2C\x20\x6D\x61\x72\x67\x69\x6E\x73\x2C\x20\x61\x6E\x64\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x69\x6D\x61\x67\x65\x73\x20\x74\x6F\x20\x75\x73\x65\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x73\x6C\x6F\x74\x20\x69\x73\x20\x65\x6D\x70\x74\x79\x2C\x20\x66\x69\x6C\x6C\x65\x64\x2C\x20\x65\x74\x63\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x6F\x74\x73"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_EnableNavigation(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x63\x69\x64\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x79\x6F\x75\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x68\x65\x20\x62\x75\x69\x6C\x74\x2D\x69\x6E\x20\x6E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E\x20\x73\x79\x73\x74\x65\x6D\x20\x28\x61\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x66\x72\x6F\x6D\x20\x73\x6C\x6F\x74\x20\x74\x6F\x20\x73\x6C\x6F\x74\x20\x75\x73\x69\x6E\x67\x20\x6B\x65\x79\x62\x6F\x61\x72\x64\x20\x61\x72\x72\x6F\x77\x73\x20\x6F\x72\x20\x61\x20\x6A\x6F\x79\x73\x74\x69\x63\x6B\x29\x2C\x20\x61\x6E\x64\x20\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x69\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x70\x61\x6E\x65\x6C\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x66\x6F\x63\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x74\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x73\x74\x61\x72\x74\x73\x2E\x20\x55\x73\x75\x61\x6C\x6C\x79\x20\x79\x6F\x75\x27\x6C\x6C\x20\x77\x61\x6E\x74\x20\x79\x6F\x75\x72\x20\x6D\x61\x69\x6E\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x74\x6F\x20\x67\x65\x74\x20\x66\x6F\x63\x75\x73\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_DisplayTitle(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x74\x6C\x65\x20\x54\x65\x78\x74"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x63\x69\x64\x65\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x28\x6F\x72\x20\x6E\x6F\x74\x29\x20\x61\x20\x74\x69\x74\x6C\x65\x20\x6E\x65\x78\x74\x20\x74\x6F\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x70\x61\x6E\x65\x6C\x2E\x20\x46\x6F\x72\x20\x69\x74\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x70\x65\x63\x69\x66\x79\x20\x74\x68\x65\x20\x74\x69\x74\x6C\x65\x2C\x20\x66\x6F\x6E\x74\x2C\x20\x66\x6F\x6E\x74\x20\x73\x69\x7A\x65\x2C\x20\x63\x6F\x6C\x6F\x72\x20\x65\x74\x63\x2E"), 1LL, false, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_QtyFont(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x79\x6F\x75\x72\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x63\x6F\x6E\x74\x61\x69\x6E\x73\x20\x73\x74\x61\x63\x6B\x65\x64\x20\x69\x74\x65\x6D\x73\x20\x28\x6D\x6F\x72\x65\x20\x74\x68\x61\x6E\x20\x6F\x6E\x65\x20\x69\x74\x65\x6D\x20\x6F\x66\x20\x61\x20\x63\x65\x72\x74\x61\x69\x6E\x20\x73\x6F\x72\x74\x20\x69\x6E\x20\x61\x20\x73\x69\x6E\x67\x6C\x65\x20\x73\x6C\x6F\x74\x2C\x20\x6C\x69\x6B\x65\x20\x63\x6F\x69\x6E\x73\x20\x6F\x72\x20\x70\x6F\x74\x69\x6F\x6E\x73\x20\x6D\x61\x79\x62\x65\x29\x20\x79\x6F\x75\x27\x6C\x6C\x20\x70\x72\x6F\x62\x61\x62\x6C\x79\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x71\x75\x61\x6E\x74\x69\x74\x79\x20\x6E\x65\x78\x74\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x74\x65\x6D\x27\x73\x20\x69\x63\x6F\x6E\x2E\x20\x46\x6F\x72\x20\x74\x68\x61\x74\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x70\x65\x63\x69\x66\x79\x20\x68\x65\x72\x65\x20\x74\x68\x65\x20\x66\x6F\x6E\x74\x20\x74\x6F\x20\x75\x73\x65\x2C\x20\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x2C\x20\x61\x6E\x64\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x61\x74\x20\x71\x75\x61\x6E\x74\x69\x74\x79\x20\x74\x65\x78\x74\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x51\x75\x61\x6E\x74\x69\x74\x79\x20\x54\x65\x78\x74"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_PreviousInventory(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x49\x6E\x70\x75\x74\x4D\x61\x6E\x61\x67\x65\x72\x20\x63\x6F\x6D\x65\x73\x20\x77\x69\x74\x68\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x73\x20\x61\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x79\x6F\x75\x20\x74\x6F\x20\x67\x6F\x20\x66\x72\x6F\x6D\x20\x6F\x6E\x65\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x70\x61\x6E\x65\x6C\x20\x74\x6F\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x77\x68\x61\x74\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x67\x6F\x20\x74\x6F\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x70\x61\x6E\x65\x6C\x20\x77\x68\x65\x6E\x20\x70\x72\x65\x73\x73\x69\x6E\x67\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x6F\x75\x73\x20\x6F\x72\x20\x6E\x65\x78\x74\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x62\x75\x74\x74\x6F\x6E\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x74\x72\x61\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x4E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CInventoryGridU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CInventoryTitleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CSlotContainerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CReturnInventoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CIsOpenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_CurrentlyBeingMovedItemIndex(CustomAttributesCache* cache)
{
	{
		MMHiddenAttribute_tB5F49A623FDE00E5947BEF4035CE4F0E351622B6 * tmp = (MMHiddenAttribute_tB5F49A623FDE00E5947BEF4035CE4F0E351622B6 *)cache->attributes[0];
		MMHiddenAttribute__ctor_m507BA5DB8F4A54D459DBF54572723651FD8F89B6(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_InventoryGrid_m4E69ECE8EF1203241368A1E3FAB2B81D92BA91F1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_InventoryGrid_mDFD36F2E09257D3CA0323B7A9F0000A828B13597(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_InventoryTitle_m5B57607A8D6E4EF0FAF5EAB02D3F08995899588F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_InventoryTitle_m6630BC853FD5B0CE7BDF9A74CF50037D201845D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_SlotContainer_mCBDA7C6985A5BBB5097809A1CE1FDC236ED44C7A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_SlotContainer_m8CC56240D696518E779B8F2E5BC36EE57D7D73E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_ReturnInventory_m6689389CB4D14E29E9CA8033F5D98BDEE29981B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_ReturnInventory_m93121DC8B33F8C67FA13434D5409621D2242E937(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_IsOpen_mE5A66A580266D5B8DDE024EBA7EDB8C5CBC385EC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_IsOpen_m83EC1F7DEAE4E995F250F127E29F310A36763017(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryHotbar_t7FDF7984DA4EC06468BE98D1796D966284CB755D_CustomAttributesCacheGenerator_HotbarKey(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x74\x62\x61\x72"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x6B\x65\x79\x73\x20\x79\x6F\x75\x72\x20\x68\x6F\x74\x62\x61\x72\x20\x77\x69\x6C\x6C\x20\x6C\x69\x73\x74\x65\x6E\x20\x74\x6F\x20\x74\x6F\x20\x61\x63\x74\x69\x76\x61\x74\x65\x20\x74\x68\x65\x20\x68\x6F\x74\x62\x61\x72\x27\x73\x20\x61\x63\x74\x69\x6F\x6E\x2E"), 1LL, false, NULL);
	}
}
static void InventorySelectionMarker_t83FB8A9DF152C13A74F334CE03749354EFC6B654_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
}
static void InventorySelectionMarker_t83FB8A9DF152C13A74F334CE03749354EFC6B654_CustomAttributesCacheGenerator_TransitionSpeed(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x69\x6F\x6E\x20\x6D\x61\x72\x6B\x65\x72\x20\x77\x69\x6C\x6C\x20\x68\x69\x67\x68\x6C\x69\x67\x68\x74\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x73\x65\x6C\x65\x63\x74\x69\x6F\x6E\x2E\x20\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x69\x74\x73\x20\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x61\x6E\x64\x20\x6D\x69\x6E\x69\x6D\x61\x6C\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x28\x69\x74\x27\x73\x20\x75\x73\x75\x61\x6C\x6C\x79\x20\x6F\x6B\x20\x74\x6F\x20\x6C\x65\x61\x76\x65\x20\x69\x74\x20\x74\x6F\x20\x64\x65\x66\x61\x75\x6C\x74\x29\x2E"), 1LL, false, NULL);
	}
}
static void InventorySoundPlayer_tE06209AD7C1659D9135E098184574F9EDE351601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_0_0_0_var), NULL);
	}
}
static void InventorySoundPlayer_tE06209AD7C1659D9135E098184574F9EDE351601_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void InventorySoundPlayer_tE06209AD7C1659D9135E098184574F9EDE351601_CustomAttributesCacheGenerator_OpenFx(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6F\x75\x6E\x64\x73"), NULL);
	}
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[1];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x65\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x64\x65\x66\x69\x6E\x65\x20\x74\x68\x65\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x73\x6F\x75\x6E\x64\x73\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x67\x65\x74\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x69\x6E\x67\x20\x77\x69\x74\x68\x20\x74\x68\x69\x73\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x2E"), 1LL, false, NULL);
	}
}
static void BaseItem_t3CA368209E6DAFB6E82938016DDB2DF2B3B0D614_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x73\x65\x49\x74\x65\x6D"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x45\x6E\x67\x69\x6E\x65\x2F\x42\x61\x73\x65\x49\x74\x65\x6D"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItem(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x69\x74\x65\x6D"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItemTestButton(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x49\x74\x65\x6D\x54\x65\x73\x74"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItemAtItem(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x20\x69\x74\x65\x6D\x20\x61\x74"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItemAtTestButton(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x49\x74\x65\x6D\x41\x74\x54\x65\x73\x74"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemOrigin(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemTestButton(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x49\x74\x65\x6D\x54\x65\x73\x74"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemToInventoryOriginIndex(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x20\x49\x74\x65\x6D\x20\x54\x6F\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemToInventoryTestButton(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x76\x65\x49\x74\x65\x6D\x54\x6F\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_RemoveItemIndex(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6D\x6F\x76\x65\x20\x49\x74\x65\x6D"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_RemoveItemTestButton(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6D\x6F\x76\x65\x49\x74\x65\x6D\x54\x65\x73\x74"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_EmptyTargetInventory(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6D\x70\x74\x79\x20\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
	}
}
static void InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_EmptyInventoryTestButton(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6D\x70\x74\x79\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x54\x65\x73\x74"), NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6E8C73801A309A7902D439062F10AC9862D455CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MoreMountains_InventoryEngine_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MoreMountains_InventoryEngine_AttributeGenerators[114] = 
{
	U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator,
	InventorySelectionMarker_t83FB8A9DF152C13A74F334CE03749354EFC6B654_CustomAttributesCacheGenerator,
	InventorySoundPlayer_tE06209AD7C1659D9135E098184574F9EDE351601_CustomAttributesCacheGenerator,
	BaseItem_t3CA368209E6DAFB6E82938016DDB2DF2B3B0D614_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6E8C73801A309A7902D439062F10AC9862D455CF_CustomAttributesCacheGenerator,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_PlayerID,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Content,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_InventoryType,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_TargetTransform,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Persistent,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_DrawContentInInspector,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_U3COwnerU3Ek__BackingField,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_TargetInventoryContainer,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_HideContainerOnStart,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InputOnlyWhenOpen,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_ToggleInventoryKey,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_CloseList,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_ManageButtons,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_ManageButtonsMode,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_EquipUseButton,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_MoveButton,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_DropButton,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_EquipButton,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_UseButton,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_UnEquipButton,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_U3CCurrentlySelectedInventorySlotU3Ek__BackingField,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InventoryIsOpen,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ItemID,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_TargetIndex,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Usable,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Consumable,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ConsumeQuantity,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Quantity,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ItemName,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_ShortDescription,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Description,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Icon,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_Prefab,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_MaximumStack,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_TargetEquipmentInventoryName,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_UsedSound,
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86_CustomAttributesCacheGenerator_MovedSound,
	ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7_CustomAttributesCacheGenerator_Item,
	ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7_CustomAttributesCacheGenerator_Quantity,
	ItemPicker_t79858C6F0CDA9F6077E88E38756D624DE96535A7_CustomAttributesCacheGenerator_PickableIfInventoryIsFull,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_TargetInventoryName,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_U3CHiddenU3Ek__BackingField,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_HideOnEmptySlot,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_DefaultTitle,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_HideOnStart,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_Icon,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_TargetInventoryName,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_NumberOfRows,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_TargetChoiceInventory,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_DrawEmptySlots,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_PaddingTop,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_SlotPrefab,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_EnableNavigation,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_DisplayTitle,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_QtyFont,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_PreviousInventory,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CInventoryGridU3Ek__BackingField,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CInventoryTitleU3Ek__BackingField,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CSlotContainerU3Ek__BackingField,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CReturnInventoryU3Ek__BackingField,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_U3CIsOpenU3Ek__BackingField,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_CurrentlyBeingMovedItemIndex,
	InventoryHotbar_t7FDF7984DA4EC06468BE98D1796D966284CB755D_CustomAttributesCacheGenerator_HotbarKey,
	InventorySelectionMarker_t83FB8A9DF152C13A74F334CE03749354EFC6B654_CustomAttributesCacheGenerator_TransitionSpeed,
	InventorySoundPlayer_tE06209AD7C1659D9135E098184574F9EDE351601_CustomAttributesCacheGenerator_Mode,
	InventorySoundPlayer_tE06209AD7C1659D9135E098184574F9EDE351601_CustomAttributesCacheGenerator_OpenFx,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItem,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItemTestButton,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItemAtItem,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_AddItemAtTestButton,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemOrigin,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemTestButton,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemToInventoryOriginIndex,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_MoveItemToInventoryTestButton,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_RemoveItemIndex,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_RemoveItemTestButton,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_EmptyTargetInventory,
	InventoryTester_t4A108AC79A4EDBBC723714D9B75073204B755A80_CustomAttributesCacheGenerator_EmptyInventoryTestButton,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Inventory_get_Owner_mE01A17C278F484A328297B5708967E25C9795D25,
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_CustomAttributesCacheGenerator_Inventory_set_Owner_m676D8E71B989E750CF110AB1DE22944089FB2B39,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InventoryInputManager_get_CurrentlySelectedInventorySlot_mE06883E42278A8B2A4DFF27879C4CFD2EF38AEC8,
	InventoryInputManager_t3AEB5D920451AF8802A18A5CE3ABBA04E0E113FC_CustomAttributesCacheGenerator_InventoryInputManager_set_CurrentlySelectedInventorySlot_m7651E8F8A7EF0159C3CC275FFFFA09F56DE108B4,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_get_Hidden_mCE81253E9F4208C1F5F2BEC300BEB2B5CCA66226,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_set_Hidden_m9AD3E32E93E637B972DBB06255E347C2E9092A12,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_FillDetailFields_m28C0F7CDA206A0D61631B6B534618C56A9390BFE,
	InventoryDetails_tC1492CB227D49FB24B03872C4180D5337D123514_CustomAttributesCacheGenerator_InventoryDetails_FillDetailFieldsWithDefaults_m7E866BA1CC220EF12FB1DC643D40D26595FC798E,
	U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23__ctor_m30FD6F764B454974959D118019025E6BDCF52937,
	U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_IDisposable_Dispose_mB1DC5B14A9DF711E14850109BAABF4C60E420B6B,
	U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m368AA5564217C2D7DE19BD1CB4C88E7D5282B76B,
	U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_Reset_m713299BF6202DADB0DA364E926981B4A8DAB8C4E,
	U3CFillDetailFieldsU3Ed__23_t89B89F1908926486A0067D3C3F7F421D6F7CE3A4_CustomAttributesCacheGenerator_U3CFillDetailFieldsU3Ed__23_System_Collections_IEnumerator_get_Current_m4F2A03610ECA92C775228D6821CC7DFDBB601EA1,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24__ctor_m1F16C86295811C4D6AAC91674C187CCC050291B1,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_IDisposable_Dispose_m76EA674ABC0192E325573EC331D4BFBD827C90A8,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8D81549F524FC8A566A13D76A26F0C2BCBBA4CD,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_Reset_mC5189493E7C2E1A47226CC7DBB9BDB5EA1711A24,
	U3CFillDetailFieldsWithDefaultsU3Ed__24_tE151785EE2247CC51C00D47DBF302AB6C8ACCD05_CustomAttributesCacheGenerator_U3CFillDetailFieldsWithDefaultsU3Ed__24_System_Collections_IEnumerator_get_Current_m0F3E0BF9F8914A43406764CD643B3E857434AD2C,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_InventoryGrid_m4E69ECE8EF1203241368A1E3FAB2B81D92BA91F1,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_InventoryGrid_mDFD36F2E09257D3CA0323B7A9F0000A828B13597,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_InventoryTitle_m5B57607A8D6E4EF0FAF5EAB02D3F08995899588F,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_InventoryTitle_m6630BC853FD5B0CE7BDF9A74CF50037D201845D8,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_SlotContainer_mCBDA7C6985A5BBB5097809A1CE1FDC236ED44C7A,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_SlotContainer_m8CC56240D696518E779B8F2E5BC36EE57D7D73E2,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_ReturnInventory_m6689389CB4D14E29E9CA8033F5D98BDEE29981B5,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_ReturnInventory_m93121DC8B33F8C67FA13434D5409621D2242E937,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_get_IsOpen_mE5A66A580266D5B8DDE024EBA7EDB8C5CBC385EC,
	InventoryDisplay_t3747BD8704644418778AB5F8606A9DE0336A2D45_CustomAttributesCacheGenerator_InventoryDisplay_set_IsOpen_m83EC1F7DEAE4E995F250F127E29F310A36763017,
	MoreMountains_InventoryEngine_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}

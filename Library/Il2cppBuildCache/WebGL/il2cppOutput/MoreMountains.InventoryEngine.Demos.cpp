﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Collections.Generic.List`1<MoreMountains.InventoryEngine.Inventory>
struct List_1_t9A33E88EA9AE1683DDF87D57FA5760C0C6BD8DBF;
// MoreMountains.Tools.MMEventListener`1<MoreMountains.InventoryEngine.MMInventoryEvent>
struct MMEventListener_1_tC12C2A95D477D76833A8842BF96D855A83C436BE;
// MoreMountains.Tools.MMSingleton`1<MoreMountains.InventoryEngine.InventoryDemoGameManager>
struct MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6;
// MoreMountains.Tools.MMSingleton`1<System.Object>
struct MMSingleton_1_tDFC31364B2B8D70B8F6DBDF995CE7F469D1A68EB;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// MoreMountains.InventoryEngine.InventoryItem[]
struct InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// MoreMountains.InventoryEngine.ArmorItem
struct ArmorItem_t118801464F0EF0CA8ABD4D2A120F3FC630EA9A7D;
// UnityEngine.AudioClip
struct AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE;
// MoreMountains.InventoryEngine.BombItem
struct BombItem_tCAA7908F1944E2058A9F55EC36344199718DF146;
// MoreMountains.InventoryEngine.ChangeLevel
struct ChangeLevel_t2A19F206E0EC0D00D31CEC0E7B301A1D0BFAEFF5;
// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// MoreMountains.InventoryEngine.DemoCharacterInputManager
struct DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// MoreMountains.InventoryEngine.HealthBonusItem
struct HealthBonusItem_t809D6D413316CE01502F7DE1628FD316BEB0F679;
// MoreMountains.InventoryEngine.Inventory
struct Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492;
// MoreMountains.InventoryEngine.InventoryDemoCharacter
struct InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8;
// MoreMountains.InventoryEngine.InventoryDemoGameManager
struct InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4;
// MoreMountains.InventoryEngine.InventoryItem
struct InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86;
// MoreMountains.InventoryEngine.InventorySlot
struct InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16;
// MoreMountains.Tools.MMSpawnAroundProperties
struct MMSpawnAroundProperties_tA295935389AAA8651ADD3179DDA2DBF5D4A5DDFA;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// MoreMountains.InventoryEngine.WeaponItem
struct WeaponItem_t2F6EAB00FDCFE01CEFD533030238A0D9CF4B7D0B;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral1BDFF999CFF057F2E088504315E99A5F03E32B31;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral2661F08F27FF609BA49F4FC672CEA2AD6E6926C3;
IL2CPP_EXTERN_C String_t* _stringLiteral3F68A4F7400B6BC83838DE47D64FC69450772D56;
IL2CPP_EXTERN_C String_t* _stringLiteral49F1997C80A5F634288B2B9FCBC9E13F35157CC9;
IL2CPP_EXTERN_C String_t* _stringLiteral4D937C48A0B3B755CCD4745CF3D32EA541D7C210;
IL2CPP_EXTERN_C String_t* _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32;
IL2CPP_EXTERN_C String_t* _stringLiteral623CBDCC47C6DB996D3C60509009A4E557A3E1EF;
IL2CPP_EXTERN_C String_t* _stringLiteral6A719B16F03E18E6ED7CE19A6F58C0FEF4C3D978;
IL2CPP_EXTERN_C String_t* _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70;
IL2CPP_EXTERN_C String_t* _stringLiteralCE863626383155D02291456632E72C0FBEC22C3C;
IL2CPP_EXTERN_C String_t* _stringLiteralEAF8AEB7AD09248217D9FE6B19DC5252E83AE7B1;
IL2CPP_EXTERN_C const RuntimeMethod* Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1_Awake_mA505CDCCF28ED524838AF58EDB6FAF888FB01325_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMSingleton_1__ctor_mDBFCCFEED93B7DA4979E27AFF81A0DD8D8F29934_RuntimeMethod_var;

struct InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tC8CB1B265FF6E734329A202B6D8BF2006AD7916F 
{
public:

public:
};


// System.Object


// System.EmptyArray`1<System.Object>
struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4  : public RuntimeObject
{
public:

public:
};

struct EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields
{
public:
	// T[] System.EmptyArray`1::Value
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___Value_0;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields, ___Value_0)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_Value_0() const { return ___Value_0; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// MoreMountains.InventoryEngine.ItemClasses
struct ItemClasses_tDC862C00691C194F27AF5C7089DD6529F083B753 
{
public:
	// System.Int32 MoreMountains.InventoryEngine.ItemClasses::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemClasses_tDC862C00691C194F27AF5C7089DD6529F083B753, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.KeyCode
struct KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.InventoryEngine.MMInventoryEventType
struct MMInventoryEventType_t86A162D51C8223DDD48185F1D31A9049FE353F48 
{
public:
	// System.Int32 MoreMountains.InventoryEngine.MMInventoryEventType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMInventoryEventType_t86A162D51C8223DDD48185F1D31A9049FE353F48, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// MoreMountains.InventoryEngine.Inventory/InventoryTypes
struct InventoryTypes_t236E6C276BBB171556964EA00B20F9337161213D 
{
public:
	// System.Int32 MoreMountains.InventoryEngine.Inventory/InventoryTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InventoryTypes_t236E6C276BBB171556964EA00B20F9337161213D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// MoreMountains.InventoryEngine.MMInventoryEvent
struct MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D 
{
public:
	// MoreMountains.InventoryEngine.MMInventoryEventType MoreMountains.InventoryEngine.MMInventoryEvent::InventoryEventType
	int32_t ___InventoryEventType_0;
	// MoreMountains.InventoryEngine.InventorySlot MoreMountains.InventoryEngine.MMInventoryEvent::Slot
	InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16 * ___Slot_1;
	// System.String MoreMountains.InventoryEngine.MMInventoryEvent::TargetInventoryName
	String_t* ___TargetInventoryName_2;
	// MoreMountains.InventoryEngine.InventoryItem MoreMountains.InventoryEngine.MMInventoryEvent::EventItem
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * ___EventItem_3;
	// System.Int32 MoreMountains.InventoryEngine.MMInventoryEvent::Quantity
	int32_t ___Quantity_4;
	// System.Int32 MoreMountains.InventoryEngine.MMInventoryEvent::Index
	int32_t ___Index_5;
	// System.String MoreMountains.InventoryEngine.MMInventoryEvent::PlayerID
	String_t* ___PlayerID_6;

public:
	inline static int32_t get_offset_of_InventoryEventType_0() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___InventoryEventType_0)); }
	inline int32_t get_InventoryEventType_0() const { return ___InventoryEventType_0; }
	inline int32_t* get_address_of_InventoryEventType_0() { return &___InventoryEventType_0; }
	inline void set_InventoryEventType_0(int32_t value)
	{
		___InventoryEventType_0 = value;
	}

	inline static int32_t get_offset_of_Slot_1() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___Slot_1)); }
	inline InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16 * get_Slot_1() const { return ___Slot_1; }
	inline InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16 ** get_address_of_Slot_1() { return &___Slot_1; }
	inline void set_Slot_1(InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16 * value)
	{
		___Slot_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Slot_1), (void*)value);
	}

	inline static int32_t get_offset_of_TargetInventoryName_2() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___TargetInventoryName_2)); }
	inline String_t* get_TargetInventoryName_2() const { return ___TargetInventoryName_2; }
	inline String_t** get_address_of_TargetInventoryName_2() { return &___TargetInventoryName_2; }
	inline void set_TargetInventoryName_2(String_t* value)
	{
		___TargetInventoryName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetInventoryName_2), (void*)value);
	}

	inline static int32_t get_offset_of_EventItem_3() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___EventItem_3)); }
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * get_EventItem_3() const { return ___EventItem_3; }
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 ** get_address_of_EventItem_3() { return &___EventItem_3; }
	inline void set_EventItem_3(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * value)
	{
		___EventItem_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EventItem_3), (void*)value);
	}

	inline static int32_t get_offset_of_Quantity_4() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___Quantity_4)); }
	inline int32_t get_Quantity_4() const { return ___Quantity_4; }
	inline int32_t* get_address_of_Quantity_4() { return &___Quantity_4; }
	inline void set_Quantity_4(int32_t value)
	{
		___Quantity_4 = value;
	}

	inline static int32_t get_offset_of_Index_5() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___Index_5)); }
	inline int32_t get_Index_5() const { return ___Index_5; }
	inline int32_t* get_address_of_Index_5() { return &___Index_5; }
	inline void set_Index_5(int32_t value)
	{
		___Index_5 = value;
	}

	inline static int32_t get_offset_of_PlayerID_6() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D, ___PlayerID_6)); }
	inline String_t* get_PlayerID_6() const { return ___PlayerID_6; }
	inline String_t** get_address_of_PlayerID_6() { return &___PlayerID_6; }
	inline void set_PlayerID_6(String_t* value)
	{
		___PlayerID_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_6), (void*)value);
	}
};

struct MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_StaticFields
{
public:
	// MoreMountains.InventoryEngine.MMInventoryEvent MoreMountains.InventoryEngine.MMInventoryEvent::e
	MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  ___e_7;

public:
	inline static int32_t get_offset_of_e_7() { return static_cast<int32_t>(offsetof(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_StaticFields, ___e_7)); }
	inline MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  get_e_7() const { return ___e_7; }
	inline MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D * get_address_of_e_7() { return &___e_7; }
	inline void set_e_7(MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  value)
	{
		___e_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_7))->___Slot_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_7))->___TargetInventoryName_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_7))->___EventItem_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___e_7))->___PlayerID_6), (void*)NULL);
		#endif
	}
};

// Native definition for P/Invoke marshalling of MoreMountains.InventoryEngine.MMInventoryEvent
struct MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_marshaled_pinvoke
{
	int32_t ___InventoryEventType_0;
	InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16 * ___Slot_1;
	char* ___TargetInventoryName_2;
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * ___EventItem_3;
	int32_t ___Quantity_4;
	int32_t ___Index_5;
	char* ___PlayerID_6;
};
// Native definition for COM marshalling of MoreMountains.InventoryEngine.MMInventoryEvent
struct MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_marshaled_com
{
	int32_t ___InventoryEventType_0;
	InventorySlot_tC5D9D2A3C65E2C232C89AA0067D0D7887C905B16 * ___Slot_1;
	Il2CppChar* ___TargetInventoryName_2;
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * ___EventItem_3;
	int32_t ___Quantity_4;
	int32_t ___Index_5;
	Il2CppChar* ___PlayerID_6;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// MoreMountains.InventoryEngine.InventoryItem
struct InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.String MoreMountains.InventoryEngine.InventoryItem::ItemID
	String_t* ___ItemID_4;
	// System.String MoreMountains.InventoryEngine.InventoryItem::TargetInventoryName
	String_t* ___TargetInventoryName_5;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::ForceSlotIndex
	bool ___ForceSlotIndex_6;
	// System.Int32 MoreMountains.InventoryEngine.InventoryItem::TargetIndex
	int32_t ___TargetIndex_7;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::Usable
	bool ___Usable_8;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::Consumable
	bool ___Consumable_9;
	// System.Int32 MoreMountains.InventoryEngine.InventoryItem::ConsumeQuantity
	int32_t ___ConsumeQuantity_10;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::Equippable
	bool ___Equippable_11;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::MoveWhenEquipped
	bool ___MoveWhenEquipped_12;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::Droppable
	bool ___Droppable_13;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::CanMoveObject
	bool ___CanMoveObject_14;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::CanSwapObject
	bool ___CanSwapObject_15;
	// System.Int32 MoreMountains.InventoryEngine.InventoryItem::Quantity
	int32_t ___Quantity_16;
	// System.String MoreMountains.InventoryEngine.InventoryItem::ItemName
	String_t* ___ItemName_17;
	// System.String MoreMountains.InventoryEngine.InventoryItem::ShortDescription
	String_t* ___ShortDescription_18;
	// System.String MoreMountains.InventoryEngine.InventoryItem::Description
	String_t* ___Description_19;
	// UnityEngine.Sprite MoreMountains.InventoryEngine.InventoryItem::Icon
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___Icon_20;
	// UnityEngine.GameObject MoreMountains.InventoryEngine.InventoryItem::Prefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Prefab_21;
	// MoreMountains.Tools.MMSpawnAroundProperties MoreMountains.InventoryEngine.InventoryItem::DropProperties
	MMSpawnAroundProperties_tA295935389AAA8651ADD3179DDA2DBF5D4A5DDFA * ___DropProperties_22;
	// System.Int32 MoreMountains.InventoryEngine.InventoryItem::MaximumStack
	int32_t ___MaximumStack_23;
	// MoreMountains.InventoryEngine.ItemClasses MoreMountains.InventoryEngine.InventoryItem::ItemClass
	int32_t ___ItemClass_24;
	// System.String MoreMountains.InventoryEngine.InventoryItem::TargetEquipmentInventoryName
	String_t* ___TargetEquipmentInventoryName_25;
	// UnityEngine.AudioClip MoreMountains.InventoryEngine.InventoryItem::EquippedSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___EquippedSound_26;
	// UnityEngine.AudioClip MoreMountains.InventoryEngine.InventoryItem::UsedSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___UsedSound_27;
	// UnityEngine.AudioClip MoreMountains.InventoryEngine.InventoryItem::MovedSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___MovedSound_28;
	// UnityEngine.AudioClip MoreMountains.InventoryEngine.InventoryItem::DroppedSound
	AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * ___DroppedSound_29;
	// System.Boolean MoreMountains.InventoryEngine.InventoryItem::UseDefaultSoundsIfNull
	bool ___UseDefaultSoundsIfNull_30;
	// MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::_targetInventory
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * ____targetInventory_31;
	// MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::_targetEquipmentInventory
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * ____targetEquipmentInventory_32;

public:
	inline static int32_t get_offset_of_ItemID_4() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___ItemID_4)); }
	inline String_t* get_ItemID_4() const { return ___ItemID_4; }
	inline String_t** get_address_of_ItemID_4() { return &___ItemID_4; }
	inline void set_ItemID_4(String_t* value)
	{
		___ItemID_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ItemID_4), (void*)value);
	}

	inline static int32_t get_offset_of_TargetInventoryName_5() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___TargetInventoryName_5)); }
	inline String_t* get_TargetInventoryName_5() const { return ___TargetInventoryName_5; }
	inline String_t** get_address_of_TargetInventoryName_5() { return &___TargetInventoryName_5; }
	inline void set_TargetInventoryName_5(String_t* value)
	{
		___TargetInventoryName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetInventoryName_5), (void*)value);
	}

	inline static int32_t get_offset_of_ForceSlotIndex_6() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___ForceSlotIndex_6)); }
	inline bool get_ForceSlotIndex_6() const { return ___ForceSlotIndex_6; }
	inline bool* get_address_of_ForceSlotIndex_6() { return &___ForceSlotIndex_6; }
	inline void set_ForceSlotIndex_6(bool value)
	{
		___ForceSlotIndex_6 = value;
	}

	inline static int32_t get_offset_of_TargetIndex_7() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___TargetIndex_7)); }
	inline int32_t get_TargetIndex_7() const { return ___TargetIndex_7; }
	inline int32_t* get_address_of_TargetIndex_7() { return &___TargetIndex_7; }
	inline void set_TargetIndex_7(int32_t value)
	{
		___TargetIndex_7 = value;
	}

	inline static int32_t get_offset_of_Usable_8() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Usable_8)); }
	inline bool get_Usable_8() const { return ___Usable_8; }
	inline bool* get_address_of_Usable_8() { return &___Usable_8; }
	inline void set_Usable_8(bool value)
	{
		___Usable_8 = value;
	}

	inline static int32_t get_offset_of_Consumable_9() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Consumable_9)); }
	inline bool get_Consumable_9() const { return ___Consumable_9; }
	inline bool* get_address_of_Consumable_9() { return &___Consumable_9; }
	inline void set_Consumable_9(bool value)
	{
		___Consumable_9 = value;
	}

	inline static int32_t get_offset_of_ConsumeQuantity_10() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___ConsumeQuantity_10)); }
	inline int32_t get_ConsumeQuantity_10() const { return ___ConsumeQuantity_10; }
	inline int32_t* get_address_of_ConsumeQuantity_10() { return &___ConsumeQuantity_10; }
	inline void set_ConsumeQuantity_10(int32_t value)
	{
		___ConsumeQuantity_10 = value;
	}

	inline static int32_t get_offset_of_Equippable_11() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Equippable_11)); }
	inline bool get_Equippable_11() const { return ___Equippable_11; }
	inline bool* get_address_of_Equippable_11() { return &___Equippable_11; }
	inline void set_Equippable_11(bool value)
	{
		___Equippable_11 = value;
	}

	inline static int32_t get_offset_of_MoveWhenEquipped_12() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___MoveWhenEquipped_12)); }
	inline bool get_MoveWhenEquipped_12() const { return ___MoveWhenEquipped_12; }
	inline bool* get_address_of_MoveWhenEquipped_12() { return &___MoveWhenEquipped_12; }
	inline void set_MoveWhenEquipped_12(bool value)
	{
		___MoveWhenEquipped_12 = value;
	}

	inline static int32_t get_offset_of_Droppable_13() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Droppable_13)); }
	inline bool get_Droppable_13() const { return ___Droppable_13; }
	inline bool* get_address_of_Droppable_13() { return &___Droppable_13; }
	inline void set_Droppable_13(bool value)
	{
		___Droppable_13 = value;
	}

	inline static int32_t get_offset_of_CanMoveObject_14() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___CanMoveObject_14)); }
	inline bool get_CanMoveObject_14() const { return ___CanMoveObject_14; }
	inline bool* get_address_of_CanMoveObject_14() { return &___CanMoveObject_14; }
	inline void set_CanMoveObject_14(bool value)
	{
		___CanMoveObject_14 = value;
	}

	inline static int32_t get_offset_of_CanSwapObject_15() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___CanSwapObject_15)); }
	inline bool get_CanSwapObject_15() const { return ___CanSwapObject_15; }
	inline bool* get_address_of_CanSwapObject_15() { return &___CanSwapObject_15; }
	inline void set_CanSwapObject_15(bool value)
	{
		___CanSwapObject_15 = value;
	}

	inline static int32_t get_offset_of_Quantity_16() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Quantity_16)); }
	inline int32_t get_Quantity_16() const { return ___Quantity_16; }
	inline int32_t* get_address_of_Quantity_16() { return &___Quantity_16; }
	inline void set_Quantity_16(int32_t value)
	{
		___Quantity_16 = value;
	}

	inline static int32_t get_offset_of_ItemName_17() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___ItemName_17)); }
	inline String_t* get_ItemName_17() const { return ___ItemName_17; }
	inline String_t** get_address_of_ItemName_17() { return &___ItemName_17; }
	inline void set_ItemName_17(String_t* value)
	{
		___ItemName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ItemName_17), (void*)value);
	}

	inline static int32_t get_offset_of_ShortDescription_18() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___ShortDescription_18)); }
	inline String_t* get_ShortDescription_18() const { return ___ShortDescription_18; }
	inline String_t** get_address_of_ShortDescription_18() { return &___ShortDescription_18; }
	inline void set_ShortDescription_18(String_t* value)
	{
		___ShortDescription_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShortDescription_18), (void*)value);
	}

	inline static int32_t get_offset_of_Description_19() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Description_19)); }
	inline String_t* get_Description_19() const { return ___Description_19; }
	inline String_t** get_address_of_Description_19() { return &___Description_19; }
	inline void set_Description_19(String_t* value)
	{
		___Description_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Description_19), (void*)value);
	}

	inline static int32_t get_offset_of_Icon_20() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Icon_20)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_Icon_20() const { return ___Icon_20; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_Icon_20() { return &___Icon_20; }
	inline void set_Icon_20(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___Icon_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Icon_20), (void*)value);
	}

	inline static int32_t get_offset_of_Prefab_21() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___Prefab_21)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Prefab_21() const { return ___Prefab_21; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Prefab_21() { return &___Prefab_21; }
	inline void set_Prefab_21(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Prefab_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Prefab_21), (void*)value);
	}

	inline static int32_t get_offset_of_DropProperties_22() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___DropProperties_22)); }
	inline MMSpawnAroundProperties_tA295935389AAA8651ADD3179DDA2DBF5D4A5DDFA * get_DropProperties_22() const { return ___DropProperties_22; }
	inline MMSpawnAroundProperties_tA295935389AAA8651ADD3179DDA2DBF5D4A5DDFA ** get_address_of_DropProperties_22() { return &___DropProperties_22; }
	inline void set_DropProperties_22(MMSpawnAroundProperties_tA295935389AAA8651ADD3179DDA2DBF5D4A5DDFA * value)
	{
		___DropProperties_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DropProperties_22), (void*)value);
	}

	inline static int32_t get_offset_of_MaximumStack_23() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___MaximumStack_23)); }
	inline int32_t get_MaximumStack_23() const { return ___MaximumStack_23; }
	inline int32_t* get_address_of_MaximumStack_23() { return &___MaximumStack_23; }
	inline void set_MaximumStack_23(int32_t value)
	{
		___MaximumStack_23 = value;
	}

	inline static int32_t get_offset_of_ItemClass_24() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___ItemClass_24)); }
	inline int32_t get_ItemClass_24() const { return ___ItemClass_24; }
	inline int32_t* get_address_of_ItemClass_24() { return &___ItemClass_24; }
	inline void set_ItemClass_24(int32_t value)
	{
		___ItemClass_24 = value;
	}

	inline static int32_t get_offset_of_TargetEquipmentInventoryName_25() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___TargetEquipmentInventoryName_25)); }
	inline String_t* get_TargetEquipmentInventoryName_25() const { return ___TargetEquipmentInventoryName_25; }
	inline String_t** get_address_of_TargetEquipmentInventoryName_25() { return &___TargetEquipmentInventoryName_25; }
	inline void set_TargetEquipmentInventoryName_25(String_t* value)
	{
		___TargetEquipmentInventoryName_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetEquipmentInventoryName_25), (void*)value);
	}

	inline static int32_t get_offset_of_EquippedSound_26() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___EquippedSound_26)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_EquippedSound_26() const { return ___EquippedSound_26; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_EquippedSound_26() { return &___EquippedSound_26; }
	inline void set_EquippedSound_26(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___EquippedSound_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EquippedSound_26), (void*)value);
	}

	inline static int32_t get_offset_of_UsedSound_27() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___UsedSound_27)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_UsedSound_27() const { return ___UsedSound_27; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_UsedSound_27() { return &___UsedSound_27; }
	inline void set_UsedSound_27(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___UsedSound_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___UsedSound_27), (void*)value);
	}

	inline static int32_t get_offset_of_MovedSound_28() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___MovedSound_28)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_MovedSound_28() const { return ___MovedSound_28; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_MovedSound_28() { return &___MovedSound_28; }
	inline void set_MovedSound_28(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___MovedSound_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MovedSound_28), (void*)value);
	}

	inline static int32_t get_offset_of_DroppedSound_29() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___DroppedSound_29)); }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * get_DroppedSound_29() const { return ___DroppedSound_29; }
	inline AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE ** get_address_of_DroppedSound_29() { return &___DroppedSound_29; }
	inline void set_DroppedSound_29(AudioClip_t16D2E573E7CC1C5118D8EE0F0692D46866A1C0EE * value)
	{
		___DroppedSound_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DroppedSound_29), (void*)value);
	}

	inline static int32_t get_offset_of_UseDefaultSoundsIfNull_30() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ___UseDefaultSoundsIfNull_30)); }
	inline bool get_UseDefaultSoundsIfNull_30() const { return ___UseDefaultSoundsIfNull_30; }
	inline bool* get_address_of_UseDefaultSoundsIfNull_30() { return &___UseDefaultSoundsIfNull_30; }
	inline void set_UseDefaultSoundsIfNull_30(bool value)
	{
		___UseDefaultSoundsIfNull_30 = value;
	}

	inline static int32_t get_offset_of__targetInventory_31() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ____targetInventory_31)); }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * get__targetInventory_31() const { return ____targetInventory_31; }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 ** get_address_of__targetInventory_31() { return &____targetInventory_31; }
	inline void set__targetInventory_31(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * value)
	{
		____targetInventory_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____targetInventory_31), (void*)value);
	}

	inline static int32_t get_offset_of__targetEquipmentInventory_32() { return static_cast<int32_t>(offsetof(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86, ____targetEquipmentInventory_32)); }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * get__targetEquipmentInventory_32() const { return ____targetEquipmentInventory_32; }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 ** get_address_of__targetEquipmentInventory_32() { return &____targetEquipmentInventory_32; }
	inline void set__targetEquipmentInventory_32(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * value)
	{
		____targetEquipmentInventory_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____targetEquipmentInventory_32), (void*)value);
	}
};


// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// MoreMountains.InventoryEngine.ArmorItem
struct ArmorItem_t118801464F0EF0CA8ABD4D2A120F3FC630EA9A7D  : public InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86
{
public:
	// System.Int32 MoreMountains.InventoryEngine.ArmorItem::ArmorIndex
	int32_t ___ArmorIndex_33;

public:
	inline static int32_t get_offset_of_ArmorIndex_33() { return static_cast<int32_t>(offsetof(ArmorItem_t118801464F0EF0CA8ABD4D2A120F3FC630EA9A7D, ___ArmorIndex_33)); }
	inline int32_t get_ArmorIndex_33() const { return ___ArmorIndex_33; }
	inline int32_t* get_address_of_ArmorIndex_33() { return &___ArmorIndex_33; }
	inline void set_ArmorIndex_33(int32_t value)
	{
		___ArmorIndex_33 = value;
	}
};


// MoreMountains.InventoryEngine.BombItem
struct BombItem_tCAA7908F1944E2058A9F55EC36344199718DF146  : public InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86
{
public:

public:
};


// UnityEngine.Collider2D
struct Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// MoreMountains.InventoryEngine.HealthBonusItem
struct HealthBonusItem_t809D6D413316CE01502F7DE1628FD316BEB0F679  : public InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86
{
public:
	// System.Int32 MoreMountains.InventoryEngine.HealthBonusItem::HealthBonus
	int32_t ___HealthBonus_33;

public:
	inline static int32_t get_offset_of_HealthBonus_33() { return static_cast<int32_t>(offsetof(HealthBonusItem_t809D6D413316CE01502F7DE1628FD316BEB0F679, ___HealthBonus_33)); }
	inline int32_t get_HealthBonus_33() const { return ___HealthBonus_33; }
	inline int32_t* get_address_of_HealthBonus_33() { return &___HealthBonus_33; }
	inline void set_HealthBonus_33(int32_t value)
	{
		___HealthBonus_33 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF  : public Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C
{
public:

public:
};


// MoreMountains.InventoryEngine.WeaponItem
struct WeaponItem_t2F6EAB00FDCFE01CEFD533030238A0D9CF4B7D0B  : public InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86
{
public:
	// UnityEngine.Sprite MoreMountains.InventoryEngine.WeaponItem::WeaponSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___WeaponSprite_33;

public:
	inline static int32_t get_offset_of_WeaponSprite_33() { return static_cast<int32_t>(offsetof(WeaponItem_t2F6EAB00FDCFE01CEFD533030238A0D9CF4B7D0B, ___WeaponSprite_33)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_WeaponSprite_33() const { return ___WeaponSprite_33; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_WeaponSprite_33() { return &___WeaponSprite_33; }
	inline void set_WeaponSprite_33(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___WeaponSprite_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WeaponSprite_33), (void*)value);
	}
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.InventoryEngine.InventoryDemoGameManager>
struct MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6_StaticFields, ____instance_4)); }
	inline InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * get__instance_4() const { return ____instance_4; }
	inline InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// MoreMountains.InventoryEngine.ChangeLevel
struct ChangeLevel_t2A19F206E0EC0D00D31CEC0E7B301A1D0BFAEFF5  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String MoreMountains.InventoryEngine.ChangeLevel::Destination
	String_t* ___Destination_4;

public:
	inline static int32_t get_offset_of_Destination_4() { return static_cast<int32_t>(offsetof(ChangeLevel_t2A19F206E0EC0D00D31CEC0E7B301A1D0BFAEFF5, ___Destination_4)); }
	inline String_t* get_Destination_4() const { return ___Destination_4; }
	inline String_t** get_address_of_Destination_4() { return &___Destination_4; }
	inline void set_Destination_4(String_t* value)
	{
		___Destination_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Destination_4), (void*)value);
	}
};


// MoreMountains.InventoryEngine.DemoCharacterInputManager
struct DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.InventoryEngine.InventoryDemoCharacter MoreMountains.InventoryEngine.DemoCharacterInputManager::DemoCharacter
	InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * ___DemoCharacter_4;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::LeftKey
	int32_t ___LeftKey_5;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::LeftKeyAlt
	int32_t ___LeftKeyAlt_6;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::RightKey
	int32_t ___RightKey_7;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::RightKeyAlt
	int32_t ___RightKeyAlt_8;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::DownKey
	int32_t ___DownKey_9;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::DownKeyAlt
	int32_t ___DownKeyAlt_10;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::UpKey
	int32_t ___UpKey_11;
	// UnityEngine.KeyCode MoreMountains.InventoryEngine.DemoCharacterInputManager::UpKeyAlt
	int32_t ___UpKeyAlt_12;
	// System.String MoreMountains.InventoryEngine.DemoCharacterInputManager::VerticalAxisName
	String_t* ___VerticalAxisName_13;
	// System.Boolean MoreMountains.InventoryEngine.DemoCharacterInputManager::_pause
	bool ____pause_14;

public:
	inline static int32_t get_offset_of_DemoCharacter_4() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___DemoCharacter_4)); }
	inline InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * get_DemoCharacter_4() const { return ___DemoCharacter_4; }
	inline InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 ** get_address_of_DemoCharacter_4() { return &___DemoCharacter_4; }
	inline void set_DemoCharacter_4(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * value)
	{
		___DemoCharacter_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DemoCharacter_4), (void*)value);
	}

	inline static int32_t get_offset_of_LeftKey_5() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___LeftKey_5)); }
	inline int32_t get_LeftKey_5() const { return ___LeftKey_5; }
	inline int32_t* get_address_of_LeftKey_5() { return &___LeftKey_5; }
	inline void set_LeftKey_5(int32_t value)
	{
		___LeftKey_5 = value;
	}

	inline static int32_t get_offset_of_LeftKeyAlt_6() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___LeftKeyAlt_6)); }
	inline int32_t get_LeftKeyAlt_6() const { return ___LeftKeyAlt_6; }
	inline int32_t* get_address_of_LeftKeyAlt_6() { return &___LeftKeyAlt_6; }
	inline void set_LeftKeyAlt_6(int32_t value)
	{
		___LeftKeyAlt_6 = value;
	}

	inline static int32_t get_offset_of_RightKey_7() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___RightKey_7)); }
	inline int32_t get_RightKey_7() const { return ___RightKey_7; }
	inline int32_t* get_address_of_RightKey_7() { return &___RightKey_7; }
	inline void set_RightKey_7(int32_t value)
	{
		___RightKey_7 = value;
	}

	inline static int32_t get_offset_of_RightKeyAlt_8() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___RightKeyAlt_8)); }
	inline int32_t get_RightKeyAlt_8() const { return ___RightKeyAlt_8; }
	inline int32_t* get_address_of_RightKeyAlt_8() { return &___RightKeyAlt_8; }
	inline void set_RightKeyAlt_8(int32_t value)
	{
		___RightKeyAlt_8 = value;
	}

	inline static int32_t get_offset_of_DownKey_9() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___DownKey_9)); }
	inline int32_t get_DownKey_9() const { return ___DownKey_9; }
	inline int32_t* get_address_of_DownKey_9() { return &___DownKey_9; }
	inline void set_DownKey_9(int32_t value)
	{
		___DownKey_9 = value;
	}

	inline static int32_t get_offset_of_DownKeyAlt_10() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___DownKeyAlt_10)); }
	inline int32_t get_DownKeyAlt_10() const { return ___DownKeyAlt_10; }
	inline int32_t* get_address_of_DownKeyAlt_10() { return &___DownKeyAlt_10; }
	inline void set_DownKeyAlt_10(int32_t value)
	{
		___DownKeyAlt_10 = value;
	}

	inline static int32_t get_offset_of_UpKey_11() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___UpKey_11)); }
	inline int32_t get_UpKey_11() const { return ___UpKey_11; }
	inline int32_t* get_address_of_UpKey_11() { return &___UpKey_11; }
	inline void set_UpKey_11(int32_t value)
	{
		___UpKey_11 = value;
	}

	inline static int32_t get_offset_of_UpKeyAlt_12() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___UpKeyAlt_12)); }
	inline int32_t get_UpKeyAlt_12() const { return ___UpKeyAlt_12; }
	inline int32_t* get_address_of_UpKeyAlt_12() { return &___UpKeyAlt_12; }
	inline void set_UpKeyAlt_12(int32_t value)
	{
		___UpKeyAlt_12 = value;
	}

	inline static int32_t get_offset_of_VerticalAxisName_13() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ___VerticalAxisName_13)); }
	inline String_t* get_VerticalAxisName_13() const { return ___VerticalAxisName_13; }
	inline String_t** get_address_of_VerticalAxisName_13() { return &___VerticalAxisName_13; }
	inline void set_VerticalAxisName_13(String_t* value)
	{
		___VerticalAxisName_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VerticalAxisName_13), (void*)value);
	}

	inline static int32_t get_offset_of__pause_14() { return static_cast<int32_t>(offsetof(DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2, ____pause_14)); }
	inline bool get__pause_14() const { return ____pause_14; }
	inline bool* get_address_of__pause_14() { return &____pause_14; }
	inline void set__pause_14(bool value)
	{
		____pause_14 = value;
	}
};


// MoreMountains.InventoryEngine.Inventory
struct Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String MoreMountains.InventoryEngine.Inventory::PlayerID
	String_t* ___PlayerID_5;
	// MoreMountains.InventoryEngine.InventoryItem[] MoreMountains.InventoryEngine.Inventory::Content
	InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* ___Content_6;
	// MoreMountains.InventoryEngine.Inventory/InventoryTypes MoreMountains.InventoryEngine.Inventory::InventoryType
	int32_t ___InventoryType_7;
	// UnityEngine.Transform MoreMountains.InventoryEngine.Inventory::TargetTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___TargetTransform_8;
	// System.Boolean MoreMountains.InventoryEngine.Inventory::Persistent
	bool ___Persistent_9;
	// System.Boolean MoreMountains.InventoryEngine.Inventory::ResetThisInventorySaveOnStart
	bool ___ResetThisInventorySaveOnStart_10;
	// System.Boolean MoreMountains.InventoryEngine.Inventory::DrawContentInInspector
	bool ___DrawContentInInspector_11;
	// UnityEngine.GameObject MoreMountains.InventoryEngine.Inventory::<Owner>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3COwnerU3Ek__BackingField_12;
	// MoreMountains.InventoryEngine.InventoryItem MoreMountains.InventoryEngine.Inventory::_loadedInventoryItem
	InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * ____loadedInventoryItem_16;

public:
	inline static int32_t get_offset_of_PlayerID_5() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___PlayerID_5)); }
	inline String_t* get_PlayerID_5() const { return ___PlayerID_5; }
	inline String_t** get_address_of_PlayerID_5() { return &___PlayerID_5; }
	inline void set_PlayerID_5(String_t* value)
	{
		___PlayerID_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_5), (void*)value);
	}

	inline static int32_t get_offset_of_Content_6() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___Content_6)); }
	inline InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* get_Content_6() const { return ___Content_6; }
	inline InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32** get_address_of_Content_6() { return &___Content_6; }
	inline void set_Content_6(InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* value)
	{
		___Content_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Content_6), (void*)value);
	}

	inline static int32_t get_offset_of_InventoryType_7() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___InventoryType_7)); }
	inline int32_t get_InventoryType_7() const { return ___InventoryType_7; }
	inline int32_t* get_address_of_InventoryType_7() { return &___InventoryType_7; }
	inline void set_InventoryType_7(int32_t value)
	{
		___InventoryType_7 = value;
	}

	inline static int32_t get_offset_of_TargetTransform_8() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___TargetTransform_8)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_TargetTransform_8() const { return ___TargetTransform_8; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_TargetTransform_8() { return &___TargetTransform_8; }
	inline void set_TargetTransform_8(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___TargetTransform_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetTransform_8), (void*)value);
	}

	inline static int32_t get_offset_of_Persistent_9() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___Persistent_9)); }
	inline bool get_Persistent_9() const { return ___Persistent_9; }
	inline bool* get_address_of_Persistent_9() { return &___Persistent_9; }
	inline void set_Persistent_9(bool value)
	{
		___Persistent_9 = value;
	}

	inline static int32_t get_offset_of_ResetThisInventorySaveOnStart_10() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___ResetThisInventorySaveOnStart_10)); }
	inline bool get_ResetThisInventorySaveOnStart_10() const { return ___ResetThisInventorySaveOnStart_10; }
	inline bool* get_address_of_ResetThisInventorySaveOnStart_10() { return &___ResetThisInventorySaveOnStart_10; }
	inline void set_ResetThisInventorySaveOnStart_10(bool value)
	{
		___ResetThisInventorySaveOnStart_10 = value;
	}

	inline static int32_t get_offset_of_DrawContentInInspector_11() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___DrawContentInInspector_11)); }
	inline bool get_DrawContentInInspector_11() const { return ___DrawContentInInspector_11; }
	inline bool* get_address_of_DrawContentInInspector_11() { return &___DrawContentInInspector_11; }
	inline void set_DrawContentInInspector_11(bool value)
	{
		___DrawContentInInspector_11 = value;
	}

	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ___U3COwnerU3Ek__BackingField_12)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3COwnerU3Ek__BackingField_12() const { return ___U3COwnerU3Ek__BackingField_12; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3COwnerU3Ek__BackingField_12() { return &___U3COwnerU3Ek__BackingField_12; }
	inline void set_U3COwnerU3Ek__BackingField_12(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3COwnerU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COwnerU3Ek__BackingField_12), (void*)value);
	}

	inline static int32_t get_offset_of__loadedInventoryItem_16() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492, ____loadedInventoryItem_16)); }
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * get__loadedInventoryItem_16() const { return ____loadedInventoryItem_16; }
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 ** get_address_of__loadedInventoryItem_16() { return &____loadedInventoryItem_16; }
	inline void set__loadedInventoryItem_16(InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * value)
	{
		____loadedInventoryItem_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____loadedInventoryItem_16), (void*)value);
	}
};

struct Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_StaticFields
{
public:
	// System.Collections.Generic.List`1<MoreMountains.InventoryEngine.Inventory> MoreMountains.InventoryEngine.Inventory::RegisteredInventories
	List_1_t9A33E88EA9AE1683DDF87D57FA5760C0C6BD8DBF * ___RegisteredInventories_4;

public:
	inline static int32_t get_offset_of_RegisteredInventories_4() { return static_cast<int32_t>(offsetof(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492_StaticFields, ___RegisteredInventories_4)); }
	inline List_1_t9A33E88EA9AE1683DDF87D57FA5760C0C6BD8DBF * get_RegisteredInventories_4() const { return ___RegisteredInventories_4; }
	inline List_1_t9A33E88EA9AE1683DDF87D57FA5760C0C6BD8DBF ** get_address_of_RegisteredInventories_4() { return &___RegisteredInventories_4; }
	inline void set_RegisteredInventories_4(List_1_t9A33E88EA9AE1683DDF87D57FA5760C0C6BD8DBF * value)
	{
		___RegisteredInventories_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___RegisteredInventories_4), (void*)value);
	}
};


// MoreMountains.InventoryEngine.InventoryDemoCharacter
struct InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String MoreMountains.InventoryEngine.InventoryDemoCharacter::PlayerID
	String_t* ___PlayerID_4;
	// System.Single MoreMountains.InventoryEngine.InventoryDemoCharacter::CharacterSpeed
	float ___CharacterSpeed_5;
	// UnityEngine.SpriteRenderer MoreMountains.InventoryEngine.InventoryDemoCharacter::WeaponSprite
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___WeaponSprite_6;
	// MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryDemoCharacter::ArmorInventory
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * ___ArmorInventory_7;
	// MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryDemoCharacter::WeaponInventory
	Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * ___WeaponInventory_8;
	// System.Int32 MoreMountains.InventoryEngine.InventoryDemoCharacter::_currentArmor
	int32_t ____currentArmor_9;
	// System.Int32 MoreMountains.InventoryEngine.InventoryDemoCharacter::_currentWeapon
	int32_t ____currentWeapon_10;
	// System.Single MoreMountains.InventoryEngine.InventoryDemoCharacter::_horizontalMove
	float ____horizontalMove_11;
	// System.Single MoreMountains.InventoryEngine.InventoryDemoCharacter::_verticalMove
	float ____verticalMove_12;
	// UnityEngine.Vector2 MoreMountains.InventoryEngine.InventoryDemoCharacter::_movement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____movement_13;
	// UnityEngine.Animator MoreMountains.InventoryEngine.InventoryDemoCharacter::_animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ____animator_14;
	// UnityEngine.Rigidbody2D MoreMountains.InventoryEngine.InventoryDemoCharacter::_rigidBody2D
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ____rigidBody2D_15;
	// System.Boolean MoreMountains.InventoryEngine.InventoryDemoCharacter::_isFacingRight
	bool ____isFacingRight_16;

public:
	inline static int32_t get_offset_of_PlayerID_4() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ___PlayerID_4)); }
	inline String_t* get_PlayerID_4() const { return ___PlayerID_4; }
	inline String_t** get_address_of_PlayerID_4() { return &___PlayerID_4; }
	inline void set_PlayerID_4(String_t* value)
	{
		___PlayerID_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_4), (void*)value);
	}

	inline static int32_t get_offset_of_CharacterSpeed_5() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ___CharacterSpeed_5)); }
	inline float get_CharacterSpeed_5() const { return ___CharacterSpeed_5; }
	inline float* get_address_of_CharacterSpeed_5() { return &___CharacterSpeed_5; }
	inline void set_CharacterSpeed_5(float value)
	{
		___CharacterSpeed_5 = value;
	}

	inline static int32_t get_offset_of_WeaponSprite_6() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ___WeaponSprite_6)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_WeaponSprite_6() const { return ___WeaponSprite_6; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_WeaponSprite_6() { return &___WeaponSprite_6; }
	inline void set_WeaponSprite_6(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___WeaponSprite_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WeaponSprite_6), (void*)value);
	}

	inline static int32_t get_offset_of_ArmorInventory_7() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ___ArmorInventory_7)); }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * get_ArmorInventory_7() const { return ___ArmorInventory_7; }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 ** get_address_of_ArmorInventory_7() { return &___ArmorInventory_7; }
	inline void set_ArmorInventory_7(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * value)
	{
		___ArmorInventory_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ArmorInventory_7), (void*)value);
	}

	inline static int32_t get_offset_of_WeaponInventory_8() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ___WeaponInventory_8)); }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * get_WeaponInventory_8() const { return ___WeaponInventory_8; }
	inline Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 ** get_address_of_WeaponInventory_8() { return &___WeaponInventory_8; }
	inline void set_WeaponInventory_8(Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * value)
	{
		___WeaponInventory_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___WeaponInventory_8), (void*)value);
	}

	inline static int32_t get_offset_of__currentArmor_9() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____currentArmor_9)); }
	inline int32_t get__currentArmor_9() const { return ____currentArmor_9; }
	inline int32_t* get_address_of__currentArmor_9() { return &____currentArmor_9; }
	inline void set__currentArmor_9(int32_t value)
	{
		____currentArmor_9 = value;
	}

	inline static int32_t get_offset_of__currentWeapon_10() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____currentWeapon_10)); }
	inline int32_t get__currentWeapon_10() const { return ____currentWeapon_10; }
	inline int32_t* get_address_of__currentWeapon_10() { return &____currentWeapon_10; }
	inline void set__currentWeapon_10(int32_t value)
	{
		____currentWeapon_10 = value;
	}

	inline static int32_t get_offset_of__horizontalMove_11() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____horizontalMove_11)); }
	inline float get__horizontalMove_11() const { return ____horizontalMove_11; }
	inline float* get_address_of__horizontalMove_11() { return &____horizontalMove_11; }
	inline void set__horizontalMove_11(float value)
	{
		____horizontalMove_11 = value;
	}

	inline static int32_t get_offset_of__verticalMove_12() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____verticalMove_12)); }
	inline float get__verticalMove_12() const { return ____verticalMove_12; }
	inline float* get_address_of__verticalMove_12() { return &____verticalMove_12; }
	inline void set__verticalMove_12(float value)
	{
		____verticalMove_12 = value;
	}

	inline static int32_t get_offset_of__movement_13() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____movement_13)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__movement_13() const { return ____movement_13; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__movement_13() { return &____movement_13; }
	inline void set__movement_13(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____movement_13 = value;
	}

	inline static int32_t get_offset_of__animator_14() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____animator_14)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get__animator_14() const { return ____animator_14; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of__animator_14() { return &____animator_14; }
	inline void set__animator_14(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		____animator_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____animator_14), (void*)value);
	}

	inline static int32_t get_offset_of__rigidBody2D_15() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____rigidBody2D_15)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get__rigidBody2D_15() const { return ____rigidBody2D_15; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of__rigidBody2D_15() { return &____rigidBody2D_15; }
	inline void set__rigidBody2D_15(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		____rigidBody2D_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rigidBody2D_15), (void*)value);
	}

	inline static int32_t get_offset_of__isFacingRight_16() { return static_cast<int32_t>(offsetof(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8, ____isFacingRight_16)); }
	inline bool get__isFacingRight_16() const { return ____isFacingRight_16; }
	inline bool* get_address_of__isFacingRight_16() { return &____isFacingRight_16; }
	inline void set__isFacingRight_16(bool value)
	{
		____isFacingRight_16 = value;
	}
};


// MoreMountains.InventoryEngine.InventoryDemoGameManager
struct InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4  : public MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6
{
public:
	// MoreMountains.InventoryEngine.InventoryDemoCharacter MoreMountains.InventoryEngine.InventoryDemoGameManager::<Player>k__BackingField
	InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * ___U3CPlayerU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CPlayerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4, ___U3CPlayerU3Ek__BackingField_5)); }
	inline InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * get_U3CPlayerU3Ek__BackingField_5() const { return ___U3CPlayerU3Ek__BackingField_5; }
	inline InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 ** get_address_of_U3CPlayerU3Ek__BackingField_5() { return &___U3CPlayerU3Ek__BackingField_5; }
	inline void set_U3CPlayerU3Ek__BackingField_5(InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * value)
	{
		___U3CPlayerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPlayerU3Ek__BackingField_5), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// MoreMountains.InventoryEngine.InventoryItem[]
struct InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * m_Items[1];

public:
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_gshared_inline (const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.InventoryEngine.MMInventoryEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.InventoryEngine.MMInventoryEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_gshared (RuntimeObject* ___caller0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<System.Object>::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSingleton_1_Awake_mEB353F0FE1AE4048D7B6C466737EE678ED8475F7_gshared (MMSingleton_1_tDFC31364B2B8D70B8F6DBDF995CE7F469D1A68EB * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMSingleton_1__ctor_m3A954A61821E2E1FB2A7C0F581642256050B2794_gshared (MMSingleton_1_tDFC31364B2B8D70B8F6DBDF995CE7F469D1A68EB * __this, const RuntimeMethod* method);

// System.Boolean MoreMountains.InventoryEngine.InventoryItem::Equip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InventoryItem_Equip_mF7910C91F206C62FAC4BAC0C06478EF0D723E37A (InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * __this, String_t* ___playerID0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<MoreMountains.InventoryEngine.InventoryDemoCharacter>()
inline InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Boolean MoreMountains.InventoryEngine.InventoryItem::UnEquip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InventoryItem_UnEquip_m074921C7579A067FDE957FDA1D6CE4FBCD61368A (InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * __this, String_t* ___playerID0, const RuntimeMethod* method);
// System.Void MoreMountains.InventoryEngine.InventoryItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01 (InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * __this, const RuntimeMethod* method);
// System.Boolean MoreMountains.InventoryEngine.InventoryItem::Use(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InventoryItem_Use_m7A779F6DA2B9B7BBC9D0674DF6F8C222615DBF20 (InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * __this, String_t* ___playerID0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_inline (const RuntimeMethod* method)
{
	return ((  ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* (*) (const RuntimeMethod*))Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_gshared_inline)(method);
}
// System.Void UnityEngine.Debug::LogFormat(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6 (String_t* ___format0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<MoreMountains.InventoryEngine.InventoryDemoCharacter>()
inline InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMGameEvent::Trigger(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGameEvent_Trigger_m6E62092C8F6B0FC44688EA24BA7CED9B73D6BF24 (String_t* ___newName0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092 (String_t* ___sceneName0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429 (int32_t ___key0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.EventRegister::MMEventStartListening<MoreMountains.InventoryEngine.MMInventoryEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_gshared)(___caller0, method);
}
// System.Void MoreMountains.Tools.EventRegister::MMEventStopListening<MoreMountains.InventoryEngine.MMInventoryEvent>(MoreMountains.Tools.MMEventListener`1<!!0>)
inline void EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE (RuntimeObject* ___caller0, const RuntimeMethod* method)
{
	((  void (*) (RuntimeObject*, const RuntimeMethod*))EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_gshared)(___caller0, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78 (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Rigidbody2D_get_velocity_m138328DCC01EB876FB5EA025BF08728030D93D66 (Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector2::get_magnitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetInteger(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetInteger_mFB04A03AF6C24978BF2BDE9161243F8F6B9762C5 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, int32_t ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A (SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Boolean MoreMountains.InventoryEngine.InventoryItem::IsNull(MoreMountains.InventoryEngine.InventoryItem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InventoryItem_IsNull_m0335EC480FD5315FDE70E3A00BD543BC857B55DA (InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * ___item0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<MoreMountains.InventoryEngine.InventoryDemoGameManager>::Awake()
inline void MMSingleton_1_Awake_mA505CDCCF28ED524838AF58EDB6FAF888FB01325 (MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6 * __this, const RuntimeMethod* method)
{
	((  void (*) (MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6 *, const RuntimeMethod*))MMSingleton_1_Awake_mEB353F0FE1AE4048D7B6C466737EE678ED8475F7_gshared)(__this, method);
}
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (String_t* ___tag0, const RuntimeMethod* method);
// System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::set_Player(MoreMountains.InventoryEngine.InventoryDemoCharacter)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InventoryDemoGameManager_set_Player_m2C1B04A843CA953D8411C86ED48D110379ECA60C_inline (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMSingleton`1<MoreMountains.InventoryEngine.InventoryDemoGameManager>::.ctor()
inline void MMSingleton_1__ctor_mDBFCCFEED93B7DA4979E27AFF81A0DD8D8F29934 (MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6 * __this, const RuntimeMethod* method)
{
	((  void (*) (MMSingleton_1_tE6E78FAE5E01DA856175A622F060598C7D7EDFF6 *, const RuntimeMethod*))MMSingleton_1__ctor_m3A954A61821E2E1FB2A7C0F581642256050B2794_gshared)(__this, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.InventoryEngine.ArmorItem::Equip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ArmorItem_Equip_m93C78320601810881EBF57208FD6BD136D1625CF (ArmorItem_t118801464F0EF0CA8ABD4D2A120F3FC630EA9A7D * __this, String_t* ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Equip(playerID);
		String_t* L_0 = ___playerID0;
		bool L_1;
		L_1 = InventoryItem_Equip_mF7910C91F206C62FAC4BAC0C06478EF0D723E37A(__this, L_0, /*hidden argument*/NULL);
		// TargetInventory(playerID).TargetTransform.GetComponent<InventoryDemoCharacter>().SetArmor(ArmorIndex);
		String_t* L_2 = ___playerID0;
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_3;
		L_3 = VirtFuncInvoker1< Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 *, String_t* >::Invoke(6 /* MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::TargetInventory(System.String) */, __this, L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = L_3->get_TargetTransform_8();
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_5;
		L_5 = Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711(L_4, /*hidden argument*/Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		int32_t L_6 = __this->get_ArmorIndex_33();
		VirtActionInvoker1< int32_t >::Invoke(13 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetArmor(System.Int32) */, L_5, L_6);
		// return true;
		return (bool)1;
	}
}
// System.Boolean MoreMountains.InventoryEngine.ArmorItem::UnEquip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ArmorItem_UnEquip_m224E73B2F972313547D04B053EE67774AEB14C2F (ArmorItem_t118801464F0EF0CA8ABD4D2A120F3FC630EA9A7D * __this, String_t* ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.UnEquip(playerID);
		String_t* L_0 = ___playerID0;
		bool L_1;
		L_1 = InventoryItem_UnEquip_m074921C7579A067FDE957FDA1D6CE4FBCD61368A(__this, L_0, /*hidden argument*/NULL);
		// TargetInventory(playerID).TargetTransform.GetComponent<InventoryDemoCharacter>().SetArmor(0);
		String_t* L_2 = ___playerID0;
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_3;
		L_3 = VirtFuncInvoker1< Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 *, String_t* >::Invoke(6 /* MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::TargetInventory(System.String) */, __this, L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = L_3->get_TargetTransform_8();
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_5;
		L_5 = Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711(L_4, /*hidden argument*/Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		VirtActionInvoker1< int32_t >::Invoke(13 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetArmor(System.Int32) */, L_5, 0);
		// return true;
		return (bool)1;
	}
}
// System.Void MoreMountains.InventoryEngine.ArmorItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArmorItem__ctor_m2FFCA33D1749790CB8AFC9D0BB37E2354783BD55 (ArmorItem_t118801464F0EF0CA8ABD4D2A120F3FC630EA9A7D * __this, const RuntimeMethod* method)
{
	{
		InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.InventoryEngine.BombItem::Use(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool BombItem_Use_mD109113C34D83AFB0257290FC77D6E60B7328FB4 (BombItem_tCAA7908F1944E2058A9F55EC36344199718DF146 * __this, String_t* ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral49F1997C80A5F634288B2B9FCBC9E13F35157CC9);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Use(playerID);
		String_t* L_0 = ___playerID0;
		bool L_1;
		L_1 = InventoryItem_Use_m7A779F6DA2B9B7BBC9D0674DF6F8C222615DBF20(__this, L_0, /*hidden argument*/NULL);
		// Debug.LogFormat("bomb explosion");
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2;
		L_2 = Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6(_stringLiteral49F1997C80A5F634288B2B9FCBC9E13F35157CC9, L_2, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}
}
// System.Void MoreMountains.InventoryEngine.BombItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BombItem__ctor_mB5903CEFC944A5BB52F61C07FC188ADF331D6846 (BombItem_tCAA7908F1944E2058A9F55EC36344199718DF146 * __this, const RuntimeMethod* method)
{
	{
		InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.InventoryEngine.ChangeLevel::OnTriggerEnter2D(UnityEngine.Collider2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeLevel_OnTriggerEnter2D_m67AD2E1AF1C24CC5F96E31388E422D3FF1500E3F (ChangeLevel_t2A19F206E0EC0D00D31CEC0E7B301A1D0BFAEFF5 * __this, Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * ___collider0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCE863626383155D02291456632E72C0FBEC22C3C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if ((Destination != null) && (collider.gameObject.GetComponent<InventoryDemoCharacter>() != null))
		String_t* L_0 = __this->get_Destination_4();
		if (!L_0)
		{
			goto IL_0030;
		}
	}
	{
		Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722 * L_1 = ___collider0;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_1, /*hidden argument*/NULL);
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_3;
		L_3 = GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F(L_2, /*hidden argument*/GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0030;
		}
	}
	{
		// MMGameEvent.Trigger("Save");
		MMGameEvent_Trigger_m6E62092C8F6B0FC44688EA24BA7CED9B73D6BF24(_stringLiteralCE863626383155D02291456632E72C0FBEC22C3C, /*hidden argument*/NULL);
		// SceneManager.LoadScene(Destination);
		String_t* L_5 = __this->get_Destination_4();
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092(L_5, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.ChangeLevel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChangeLevel__ctor_m4ADEB4E4961811FAC5883AD7C6AE2A0D4325074B (ChangeLevel_t2A19F206E0EC0D00D31CEC0E7B301A1D0BFAEFF5 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoCharacterInputManager_Update_m9BB79F4E358FED7BF871484794DA912919E3C186 (DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2 * __this, const RuntimeMethod* method)
{
	{
		// HandleDemoCharacterInput();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::HandleDemoCharacterInput() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::HandleDemoCharacterInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoCharacterInputManager_HandleDemoCharacterInput_m33C8E9D8B77500B2B5AF5B40908DA48C4A106D79 (DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// if (_pause)
		bool L_0 = __this->get__pause_14();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		// DemoCharacter.SetMovement(0,0);
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_1 = __this->get_DemoCharacter_4();
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetMovement(System.Single,System.Single) */, L_1, (0.0f), (0.0f));
		// return;
		return;
	}

IL_001e:
	{
		// float horizontalMovement = 0f;
		V_0 = (0.0f);
		// float verticalMovement = 0f;
		V_1 = (0.0f);
		// if ( (Input.GetKey(LeftKey)) || (Input.GetKey(LeftKeyAlt)) ) { horizontalMovement = -1f; }
		int32_t L_2 = __this->get_LeftKey_5();
		bool L_3;
		L_3 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		int32_t L_4 = __this->get_LeftKeyAlt_6();
		bool L_5;
		L_5 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004a;
		}
	}

IL_0044:
	{
		// if ( (Input.GetKey(LeftKey)) || (Input.GetKey(LeftKeyAlt)) ) { horizontalMovement = -1f; }
		V_0 = (-1.0f);
	}

IL_004a:
	{
		// if ( (Input.GetKey(RightKey)) || (Input.GetKey(RightKeyAlt)) ) { horizontalMovement = 1f; }
		int32_t L_6 = __this->get_RightKey_7();
		bool L_7;
		L_7 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_0064;
		}
	}
	{
		int32_t L_8 = __this->get_RightKeyAlt_8();
		bool L_9;
		L_9 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006a;
		}
	}

IL_0064:
	{
		// if ( (Input.GetKey(RightKey)) || (Input.GetKey(RightKeyAlt)) ) { horizontalMovement = 1f; }
		V_0 = (1.0f);
	}

IL_006a:
	{
		// if ( (Input.GetKey(DownKey)) || (Input.GetKey(DownKeyAlt)) ) { verticalMovement = -1f; }
		int32_t L_10 = __this->get_DownKey_9();
		bool L_11;
		L_11 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0084;
		}
	}
	{
		int32_t L_12 = __this->get_DownKeyAlt_10();
		bool L_13;
		L_13 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_008a;
		}
	}

IL_0084:
	{
		// if ( (Input.GetKey(DownKey)) || (Input.GetKey(DownKeyAlt)) ) { verticalMovement = -1f; }
		V_1 = (-1.0f);
	}

IL_008a:
	{
		// if ( (Input.GetKey(UpKey)) || (Input.GetKey(UpKeyAlt)) ) { verticalMovement = 1f; }
		int32_t L_14 = __this->get_UpKey_11();
		bool L_15;
		L_15 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00a4;
		}
	}
	{
		int32_t L_16 = __this->get_UpKeyAlt_12();
		bool L_17;
		L_17 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00aa;
		}
	}

IL_00a4:
	{
		// if ( (Input.GetKey(UpKey)) || (Input.GetKey(UpKeyAlt)) ) { verticalMovement = 1f; }
		V_1 = (1.0f);
	}

IL_00aa:
	{
		// DemoCharacter.SetMovement(horizontalMovement,verticalMovement);
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_18 = __this->get_DemoCharacter_4();
		float L_19 = V_0;
		float L_20 = V_1;
		VirtActionInvoker2< float, float >::Invoke(7 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetMovement(System.Single,System.Single) */, L_18, L_19, L_20);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoCharacterInputManager_OnMMEvent_mA6245AA2C7033D3D7053A7A3ABB2A6AFDC522A1A (DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2 * __this, MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  ___inventoryEvent0, const RuntimeMethod* method)
{
	{
		// if (inventoryEvent.InventoryEventType == MMInventoryEventType.InventoryOpens)
		MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  L_0 = ___inventoryEvent0;
		int32_t L_1 = L_0.get_InventoryEventType_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)15)))))
		{
			goto IL_0011;
		}
	}
	{
		// _pause = true;
		__this->set__pause_14((bool)1);
	}

IL_0011:
	{
		// if (inventoryEvent.InventoryEventType == MMInventoryEventType.InventoryCloses)
		MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  L_2 = ___inventoryEvent0;
		int32_t L_3 = L_2.get_InventoryEventType_0();
		if ((!(((uint32_t)L_3) == ((uint32_t)((int32_t)17)))))
		{
			goto IL_0022;
		}
	}
	{
		// _pause = false;
		__this->set__pause_14((bool)0);
	}

IL_0022:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoCharacterInputManager_OnEnable_m49CB6CE44C92083F529FB22006A4A1273468153C (DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStartListening<MMInventoryEvent>();
		EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoCharacterInputManager_OnDisable_mA463380A74F5BB73F4D3EC6036441860B6D4B0EB (DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStopListening<MMInventoryEvent>();
		EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.DemoCharacterInputManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DemoCharacterInputManager__ctor_m422D91BAA00864DB8AA8FF122F294880C678FE98 (DemoCharacterInputManager_t735E6D01FE0CAEB7634864CD7A36F563FE66ECF2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public KeyCode LeftKey = KeyCode.LeftArrow;
		__this->set_LeftKey_5(((int32_t)276));
		// public KeyCode RightKey = KeyCode.RightArrow;
		__this->set_RightKey_7(((int32_t)275));
		// public KeyCode DownKey = KeyCode.DownArrow;
		__this->set_DownKey_9(((int32_t)274));
		// public KeyCode UpKey = KeyCode.UpArrow;
		__this->set_UpKey_11(((int32_t)273));
		// public string VerticalAxisName = "Vertical";
		__this->set_VerticalAxisName_13(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.InventoryEngine.HealthBonusItem::Use(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool HealthBonusItem_Use_m1E665B43824EEFD3D34A834AF6F649AA90E17104 (HealthBonusItem_t809D6D413316CE01502F7DE1628FD316BEB0F679 * __this, String_t* ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2661F08F27FF609BA49F4FC672CEA2AD6E6926C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D937C48A0B3B755CCD4745CF3D32EA541D7C210);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Use(playerID);
		String_t* L_0 = ___playerID0;
		bool L_1;
		L_1 = InventoryItem_Use_m7A779F6DA2B9B7BBC9D0674DF6F8C222615DBF20(__this, L_0, /*hidden argument*/NULL);
		// Debug.LogFormat("increase character "+playerID+"'s health by "+HealthBonus);
		String_t* L_2 = ___playerID0;
		int32_t* L_3 = __this->get_address_of_HealthBonus_33();
		String_t* L_4;
		L_4 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_3, /*hidden argument*/NULL);
		String_t* L_5;
		L_5 = String_Concat_m37A5BF26F8F8F1892D60D727303B23FB604FEE78(_stringLiteral2661F08F27FF609BA49F4FC672CEA2AD6E6926C3, L_2, _stringLiteral4D937C48A0B3B755CCD4745CF3D32EA541D7C210, L_4, /*hidden argument*/NULL);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6;
		L_6 = Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_inline(/*hidden argument*/Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6(L_5, L_6, /*hidden argument*/NULL);
		// return true;
		return (bool)1;
	}
}
// System.Void MoreMountains.InventoryEngine.HealthBonusItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HealthBonusItem__ctor_m97B12494BB16B98D4FD81E34D392121D42FB1DCD (HealthBonusItem_t809D6D413316CE01502F7DE1628FD316BEB0F679 * __this, const RuntimeMethod* method)
{
	{
		InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_Start_mCD951E8673D555FDA81E30118106A6BFE5543C01 (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _animator = GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set__animator_14(L_0);
		// _rigidBody2D = GetComponent<Rigidbody2D>();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_1;
		L_1 = Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_m4E9E5E48D529420FAC117599819C02DB73FC7487_RuntimeMethod_var);
		__this->set__rigidBody2D_15(L_1);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_FixedUpdate_m5AE4850AD5EEF88CC0D5D7FA991E28A70030F1F8 (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	{
		// Movement();
		VirtActionInvoker0::Invoke(10 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Movement() */, __this);
		// UpdateAnimator();
		VirtActionInvoker0::Invoke(12 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::UpdateAnimator() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetMovement(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_SetMovement_m5D034E7B67BA41233F9D0CE5EEC04A09849B025E (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, float ___movementX0, float ___movementY1, const RuntimeMethod* method)
{
	{
		// _horizontalMove = movementX;
		float L_0 = ___movementX0;
		__this->set__horizontalMove_11(L_0);
		// _verticalMove = movementY;
		float L_1 = ___movementY1;
		__this->set__verticalMove_12(L_1);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetHorizontalMove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_SetHorizontalMove_mF17CFCCA4581E038AF17A5D4CF385EA639C6D28B (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// _horizontalMove = value;
		float L_0 = ___value0;
		__this->set__horizontalMove_11(L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetVerticalMove(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_SetVerticalMove_m04F2DC5D937813625162B83FBA99726F4606FFFD (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// _verticalMove = value;
		float L_0 = ___value0;
		__this->set__verticalMove_12(L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Movement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_Movement_m4E55F8CC0A027B57E492FFEB43CC35736317A95B (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	{
		// if (_horizontalMove > 0.1f)
		float L_0 = __this->get__horizontalMove_11();
		if ((!(((float)L_0) > ((float)(0.100000001f)))))
		{
			goto IL_001d;
		}
	}
	{
		// if (!_isFacingRight)
		bool L_1 = __this->get__isFacingRight_16();
		if (L_1)
		{
			goto IL_0038;
		}
	}
	{
		// Flip();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Flip() */, __this);
		// }
		goto IL_0038;
	}

IL_001d:
	{
		// else if (_horizontalMove < -0.1f)
		float L_2 = __this->get__horizontalMove_11();
		if ((!(((float)L_2) < ((float)(-0.100000001f)))))
		{
			goto IL_0038;
		}
	}
	{
		// if (_isFacingRight)
		bool L_3 = __this->get__isFacingRight_16();
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		// Flip();
		VirtActionInvoker0::Invoke(11 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Flip() */, __this);
	}

IL_0038:
	{
		// _movement = new Vector2(_horizontalMove, _verticalMove);
		float L_4 = __this->get__horizontalMove_11();
		float L_5 = __this->get__verticalMove_12();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), L_4, L_5, /*hidden argument*/NULL);
		__this->set__movement_13(L_6);
		// _movement *= CharacterSpeed * Time.deltaTime;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = __this->get__movement_13();
		float L_8 = __this->get_CharacterSpeed_5();
		float L_9;
		L_9 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10;
		L_10 = Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline(L_7, ((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), /*hidden argument*/NULL);
		__this->set__movement_13(L_10);
		// _rigidBody2D.velocity = _movement;
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_11 = __this->get__rigidBody2D_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = __this->get__movement_13();
		Rigidbody2D_set_velocity_m56B745344E78C85462843AE623BF0A40764FC2DA(L_11, L_12, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::Flip()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_Flip_m115687C165D98FC93FFAB48F0812F6E3A17E4FCB (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	{
		// transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_y_3();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_10), ((-L_3)), L_6, L_9, /*hidden argument*/NULL);
		Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A(L_0, L_10, /*hidden argument*/NULL);
		// _isFacingRight = transform.localScale.x > 0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046(L_11, /*hidden argument*/NULL);
		float L_13 = L_12.get_x_2();
		__this->set__isFacingRight_16((bool)((((float)L_13) > ((float)(0.0f)))? 1 : 0));
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::UpdateAnimator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_UpdateAnimator_m37AC18CEFEA8CF069D30560996216F45B356FE4E (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3F68A4F7400B6BC83838DE47D64FC69450772D56);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (_animator != null)
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get__animator_14();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0047;
		}
	}
	{
		// _animator.SetFloat("Speed", _rigidBody2D.velocity.magnitude);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = __this->get__animator_14();
		Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * L_3 = __this->get__rigidBody2D_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = Rigidbody2D_get_velocity_m138328DCC01EB876FB5EA025BF08728030D93D66(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5;
		L_5 = Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_0), /*hidden argument*/NULL);
		Animator_SetFloat_mD731F47ED44C2D629F8E1C6DB15629C3E1B992A0(L_2, _stringLiteral5D2E3D85D1C3D4F42FAE33FB35C01C48241E0B32, L_5, /*hidden argument*/NULL);
		// _animator.SetInteger("Armor", _currentArmor);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_6 = __this->get__animator_14();
		int32_t L_7 = __this->get__currentArmor_9();
		Animator_SetInteger_mFB04A03AF6C24978BF2BDE9161243F8F6B9762C5(L_6, _stringLiteral3F68A4F7400B6BC83838DE47D64FC69450772D56, L_7, /*hidden argument*/NULL);
	}

IL_0047:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetArmor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_SetArmor_mE711C29C7211485F09C612978D8B254E80402342 (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		// _currentArmor = index;
		int32_t L_0 = ___index0;
		__this->set__currentArmor_9(L_0);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetWeapon(UnityEngine.Sprite,MoreMountains.InventoryEngine.InventoryItem)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_SetWeapon_m6C37DC70CF14F594880E251665A89C2682BF7CB7 (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___newSprite0, InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * ___item1, const RuntimeMethod* method)
{
	{
		// WeaponSprite.sprite = newSprite;
		SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * L_0 = __this->get_WeaponSprite_6();
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_1 = ___newSprite0;
		SpriteRenderer_set_sprite_mBCFFBF3F10C068FD1174C4506DF73E204303FC1A(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_OnMMEvent_m52139D753486835929148161DD14F74EDB8A3F4F (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  ___inventoryEvent0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1BDFF999CFF057F2E088504315E99A5F03E32B31);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEAF8AEB7AD09248217D9FE6B19DC5252E83AE7B1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (inventoryEvent.InventoryEventType == MMInventoryEventType.InventoryLoaded)
		MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  L_0 = ___inventoryEvent0;
		int32_t L_1 = L_0.get_InventoryEventType_0();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)18)))))
		{
			goto IL_00a7;
		}
	}
	{
		// if (inventoryEvent.TargetInventoryName == "RogueArmorInventory")
		MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  L_2 = ___inventoryEvent0;
		String_t* L_3 = L_2.get_TargetInventoryName_2();
		bool L_4;
		L_4 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_3, _stringLiteral1BDFF999CFF057F2E088504315E99A5F03E32B31, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005a;
		}
	}
	{
		// if (ArmorInventory != null)
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_5 = __this->get_ArmorInventory_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005a;
		}
	}
	{
		// if (!InventoryItem.IsNull(ArmorInventory.Content [0]))
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_7 = __this->get_ArmorInventory_7();
		InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* L_8 = L_7->get_Content_6();
		int32_t L_9 = 0;
		InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * L_10 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
		bool L_11;
		L_11 = InventoryItem_IsNull_m0335EC480FD5315FDE70E3A00BD543BC857B55DA(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005a;
		}
	}
	{
		// ArmorInventory.Content [0].Equip (PlayerID);
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_12 = __this->get_ArmorInventory_7();
		InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* L_13 = L_12->get_Content_6();
		int32_t L_14 = 0;
		InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * L_15 = (L_13)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_14));
		String_t* L_16 = __this->get_PlayerID_4();
		bool L_17;
		L_17 = VirtFuncInvoker1< bool, String_t* >::Invoke(12 /* System.Boolean MoreMountains.InventoryEngine.InventoryItem::Equip(System.String) */, L_15, L_16);
	}

IL_005a:
	{
		// if (inventoryEvent.TargetInventoryName == "RogueWeaponInventory")
		MMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D  L_18 = ___inventoryEvent0;
		String_t* L_19 = L_18.get_TargetInventoryName_2();
		bool L_20;
		L_20 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_19, _stringLiteralEAF8AEB7AD09248217D9FE6B19DC5252E83AE7B1, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00a7;
		}
	}
	{
		// if (WeaponInventory != null)
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_21 = __this->get_WeaponInventory_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_22;
		L_22 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_21, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a7;
		}
	}
	{
		// if (!InventoryItem.IsNull (WeaponInventory.Content [0]))
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_23 = __this->get_WeaponInventory_8();
		InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* L_24 = L_23->get_Content_6();
		int32_t L_25 = 0;
		InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * L_26 = (L_24)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_25));
		bool L_27;
		L_27 = InventoryItem_IsNull_m0335EC480FD5315FDE70E3A00BD543BC857B55DA(L_26, /*hidden argument*/NULL);
		if (L_27)
		{
			goto IL_00a7;
		}
	}
	{
		// WeaponInventory.Content [0].Equip (PlayerID);
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_28 = __this->get_WeaponInventory_8();
		InventoryItemU5BU5D_tE10685AA82C529D268277E2F9A4457436AB21B32* L_29 = L_28->get_Content_6();
		int32_t L_30 = 0;
		InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * L_31 = (L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_30));
		String_t* L_32 = __this->get_PlayerID_4();
		bool L_33;
		L_33 = VirtFuncInvoker1< bool, String_t* >::Invoke(12 /* System.Boolean MoreMountains.InventoryEngine.InventoryItem::Equip(System.String) */, L_31, L_32);
	}

IL_00a7:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_OnEnable_m5FBF346FFF0DBA988296045D35E37E34BB4D49CF (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStartListening<MMInventoryEvent>();
		EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A(__this, /*hidden argument*/EventRegister_MMEventStartListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m79E16B43F6F1813A9E74E19349FCD4F7242A0E8A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter_OnDisable_m6AA9CE64698F9A665D853E227FB0B8A8CDC01104 (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.MMEventStopListening<MMInventoryEvent>();
		EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE(__this, /*hidden argument*/EventRegister_MMEventStopListening_TisMMInventoryEvent_tCE555BF9BC3F97AE5DC7A7FFC92855E841A87C2D_m862F7E99D2CE79DFB69369AE4025FE6982DF2EDE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoCharacter__ctor_m2FDF6D2D9EBC89159EFDD2AF62B77CD00C808148 (InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral623CBDCC47C6DB996D3C60509009A4E557A3E1EF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public string PlayerID = "Player1";
		__this->set_PlayerID_4(_stringLiteral623CBDCC47C6DB996D3C60509009A4E557A3E1EF);
		// public float CharacterSpeed = 300f;
		__this->set_CharacterSpeed_5((300.0f));
		// protected bool _isFacingRight = true;
		__this->set__isFacingRight_16((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// MoreMountains.InventoryEngine.InventoryDemoCharacter MoreMountains.InventoryEngine.InventoryDemoGameManager::get_Player()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * InventoryDemoGameManager_get_Player_m4F66F3841D163EABDB6D9A8D4B477BDE2D9B4664 (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, const RuntimeMethod* method)
{
	{
		// public InventoryDemoCharacter Player { get; protected set; }
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_0 = __this->get_U3CPlayerU3Ek__BackingField_5();
		return L_0;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::set_Player(MoreMountains.InventoryEngine.InventoryDemoCharacter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoGameManager_set_Player_m2C1B04A843CA953D8411C86ED48D110379ECA60C (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * ___value0, const RuntimeMethod* method)
{
	{
		// public InventoryDemoCharacter Player { get; protected set; }
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_0 = ___value0;
		__this->set_U3CPlayerU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoGameManager_Awake_m3BF5D7015E550E90C8F4344CEFCA4A76D49CB3CE (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1_Awake_mA505CDCCF28ED524838AF58EDB6FAF888FB01325_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Awake ();
		MMSingleton_1_Awake_mA505CDCCF28ED524838AF58EDB6FAF888FB01325(__this, /*hidden argument*/MMSingleton_1_Awake_mA505CDCCF28ED524838AF58EDB6FAF888FB01325_RuntimeMethod_var);
		// Player = GameObject.FindGameObjectWithTag("Player").GetComponent<InventoryDemoCharacter>()    ;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_1;
		L_1 = GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F(L_0, /*hidden argument*/GameObject_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_mA402714F380A05C57161F043D6F5C7852AFB492F_RuntimeMethod_var);
		InventoryDemoGameManager_set_Player_m2C1B04A843CA953D8411C86ED48D110379ECA60C_inline(__this, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoGameManager_Start_m5D699173F5EEA66D2173570F303F2949049F3877 (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6A719B16F03E18E6ED7CE19A6F58C0FEF4C3D978);
		s_Il2CppMethodInitialized = true;
	}
	{
		// MMGameEvent.Trigger("Load");
		MMGameEvent_Trigger_m6E62092C8F6B0FC44688EA24BA7CED9B73D6BF24(_stringLiteral6A719B16F03E18E6ED7CE19A6F58C0FEF4C3D978, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.InventoryEngine.InventoryDemoGameManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InventoryDemoGameManager__ctor_m45D0B6413D1B48DC274E293293908631DD800842 (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMSingleton_1__ctor_mDBFCCFEED93B7DA4979E27AFF81A0DD8D8F29934_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MMSingleton_1__ctor_mDBFCCFEED93B7DA4979E27AFF81A0DD8D8F29934(__this, /*hidden argument*/MMSingleton_1__ctor_mDBFCCFEED93B7DA4979E27AFF81A0DD8D8F29934_RuntimeMethod_var);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean MoreMountains.InventoryEngine.WeaponItem::Equip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WeaponItem_Equip_m66786C93CEE9F0D9CE47C44391B83975BA9C3001 (WeaponItem_t2F6EAB00FDCFE01CEFD533030238A0D9CF4B7D0B * __this, String_t* ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Equip(playerID);
		String_t* L_0 = ___playerID0;
		bool L_1;
		L_1 = InventoryItem_Equip_mF7910C91F206C62FAC4BAC0C06478EF0D723E37A(__this, L_0, /*hidden argument*/NULL);
		// TargetInventory(playerID).TargetTransform.GetComponent<InventoryDemoCharacter>().SetWeapon(WeaponSprite,this);
		String_t* L_2 = ___playerID0;
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_3;
		L_3 = VirtFuncInvoker1< Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 *, String_t* >::Invoke(6 /* MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::TargetInventory(System.String) */, __this, L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = L_3->get_TargetTransform_8();
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_5;
		L_5 = Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711(L_4, /*hidden argument*/Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_6 = __this->get_WeaponSprite_33();
		VirtActionInvoker2< Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 *, InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * >::Invoke(14 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetWeapon(UnityEngine.Sprite,MoreMountains.InventoryEngine.InventoryItem) */, L_5, L_6, __this);
		// return true;
		return (bool)1;
	}
}
// System.Boolean MoreMountains.InventoryEngine.WeaponItem::UnEquip(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WeaponItem_UnEquip_m0DDD81BC4FC8A40C1A91CA5689E504C8653B7630 (WeaponItem_t2F6EAB00FDCFE01CEFD533030238A0D9CF4B7D0B * __this, String_t* ___playerID0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.UnEquip(playerID);
		String_t* L_0 = ___playerID0;
		bool L_1;
		L_1 = InventoryItem_UnEquip_m074921C7579A067FDE957FDA1D6CE4FBCD61368A(__this, L_0, /*hidden argument*/NULL);
		// TargetInventory(playerID).TargetTransform.GetComponent<InventoryDemoCharacter>().SetWeapon(null,this);
		String_t* L_2 = ___playerID0;
		Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 * L_3;
		L_3 = VirtFuncInvoker1< Inventory_t56AFDC7E6F60608530BB0B6EF0DAC7699319F492 *, String_t* >::Invoke(6 /* MoreMountains.InventoryEngine.Inventory MoreMountains.InventoryEngine.InventoryItem::TargetInventory(System.String) */, __this, L_2);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = L_3->get_TargetTransform_8();
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_5;
		L_5 = Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711(L_4, /*hidden argument*/Component_GetComponent_TisInventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8_m7BD2EE931E881F54F42F9D6590974ED5525AE711_RuntimeMethod_var);
		VirtActionInvoker2< Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 *, InventoryItem_t66EA7810AF967522C4EC9B8A144A1D54B9C32B86 * >::Invoke(14 /* System.Void MoreMountains.InventoryEngine.InventoryDemoCharacter::SetWeapon(UnityEngine.Sprite,MoreMountains.InventoryEngine.InventoryItem) */, L_5, (Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 *)NULL, __this);
		// return true;
		return (bool)1;
	}
}
// System.Void MoreMountains.InventoryEngine.WeaponItem::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WeaponItem__ctor_m860D6399E7572F7CD8932094A939F96224842873 (WeaponItem_t2F6EAB00FDCFE01CEFD533030238A0D9CF4B7D0B * __this, const RuntimeMethod* method)
{
	{
		InventoryItem__ctor_m4E1BBF45484330A259929326AE89AB361A0D6A01(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		float L_2 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___a0;
		float L_4 = L_3.get_y_1();
		float L_5 = ___d1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_6), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0019;
	}

IL_0019:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = V_0;
		return L_7;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InventoryDemoGameManager_set_Player_m2C1B04A843CA953D8411C86ED48D110379ECA60C_inline (InventoryDemoGameManager_t29A798E724456454999C45D3BA5CE8B8E415ACF4 * __this, InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * ___value0, const RuntimeMethod* method)
{
	{
		// public InventoryDemoCharacter Player { get; protected set; }
		InventoryDemoCharacter_t54F6506710064634D04001A1ABFA52662D720ED8 * L_0 = ___value0;
		__this->set_U3CPlayerU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Array_Empty_TisRuntimeObject_m1FBC21243DF3542384C523801E8CA8A97606C747_gshared_inline (const RuntimeMethod* method)
{
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->rgctx_data, 0));
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ((EmptyArray_1_tBF73225DFA890366D579424FE8F40073BF9FBAD4_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->rgctx_data, 0)))->get_Value_0();
		return (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_0;
	}
}

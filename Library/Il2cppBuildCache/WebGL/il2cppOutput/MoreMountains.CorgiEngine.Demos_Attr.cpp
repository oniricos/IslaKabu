﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// MoreMountains.Tools.MMInspectorButtonAttribute
struct MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// MoreMountains.Tools.MMInspectorButtonAttribute
struct MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMInspectorButtonAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_0), (void*)value);
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMInspectorButtonAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6 (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * __this, String_t* ___MethodName0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
static void MoreMountains_CorgiEngine_Demos_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_scaleSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x70\x65\x65\x64\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x61\x20\x6E\x65\x77\x20\x74\x61\x72\x67\x65\x74\x20\x73\x63\x61\x6C\x65\x20\x69\x73\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x64\x2E\x20\x42\x69\x67\x67\x65\x72\x20\x73\x63\x61\x6C\x65\x53\x70\x65\x65\x64\x20\x6D\x65\x61\x6E\x73\x20\x73\x6C\x6F\x77\x65\x72\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_scaleDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6E\x65\x77\x20\x74\x61\x72\x67\x65\x74\x20\x73\x63\x61\x6C\x65\x20"), NULL);
	}
}
static void BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_rotationSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x2E\x20\x42\x69\x67\x67\x65\x72\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x6D\x65\x61\x6E\x73\x20\x66\x61\x73\x74\x65\x72\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x2E"), NULL);
	}
}
static void BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_rotationAmplitude(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x6D\x70\x6C\x69\x74\x75\x64\x65\x20\x28\x69\x6E\x20\x64\x65\x67\x72\x65\x65\x73\x29\x2E"), NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(BoxCollider2D_t929D014FDE69DCA5443296C432D640BCBE7E30B9_0_0_0_var), NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_PlayerID(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x49\x44\x20\x28\x66\x6F\x72\x20\x69\x6E\x70\x75\x74\x20\x6D\x61\x6E\x61\x67\x65\x72\x29"), NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_CharacterSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x27\x73\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x73\x70\x65\x65\x64"), NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_StartingPoint(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x6F\x69\x6E\x74\x20\x6F\x6E\x20\x74\x68\x65\x20\x6D\x61\x70\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x73\x70\x61\x77\x6E"), NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_U3CCollidingWithAPathElementU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_CharacterIsFacingRight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x72\x75\x65\x20\x69\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x69\x73\x20\x63\x75\x72\x72\x65\x6E\x74\x6C\x79\x20\x66\x61\x63\x69\x6E\x67\x20\x72\x69\x67\x68\x74"), NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_U3CLastVisitedPathElementU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_AutomaticMovement(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x73\x68\x6F\x75\x6C\x64\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x6D\x6F\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x6E\x65\x78\x74\x20\x70\x6F\x69\x6E\x74\x20\x77\x68\x65\x6E\x20\x72\x65\x61\x63\x68\x69\x6E\x67\x20\x74\x68\x69\x73\x20\x6F\x6E\x65"), NULL);
	}
}
static void LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Up(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x73\x69\x62\x6C\x65\x20\x44\x69\x72\x65\x63\x74\x69\x6F\x6E\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x61\x74\x68\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x67\x6F\x20\x74\x6F\x20\x77\x68\x65\x6E\x20\x67\x6F\x69\x6E\x67\x20\x75\x70"), NULL);
	}
}
static void LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Right(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x61\x74\x68\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x67\x6F\x20\x74\x6F\x20\x77\x68\x65\x6E\x20\x67\x6F\x69\x6E\x67\x20\x72\x69\x67\x68\x74"), NULL);
	}
}
static void LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Down(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x61\x74\x68\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x67\x6F\x20\x74\x6F\x20\x77\x68\x65\x6E\x20\x67\x6F\x69\x6E\x67\x20\x64\x6F\x77\x6E"), NULL);
	}
}
static void LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Left(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x61\x74\x68\x20\x65\x6C\x65\x6D\x65\x6E\x74\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x67\x6F\x20\x74\x6F\x20\x77\x68\x65\x6E\x20\x67\x6F\x69\x6E\x67\x20\x6C\x65\x66\x74"), NULL);
	}
}
static void RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC_CustomAttributesCacheGenerator_MaximumAllowedAngle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x6E\x67\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x70\x74\x65\x72"), NULL);
	}
}
static void RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC_CustomAttributesCacheGenerator_CharacterRotationSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x70\x65\x65\x64\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x6C\x65\x72\x70\x73"), NULL);
	}
}
static void RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985_CustomAttributesCacheGenerator_DoorColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x61\x70\x70\x6C\x79"), NULL);
	}
}
static void RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985_CustomAttributesCacheGenerator_DoorLightModel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x70\x72\x69\x74\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x20\x74\x6F\x20\x6D\x6F\x64\x69\x66\x79"), NULL);
	}
}
static void RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985_CustomAttributesCacheGenerator_DoorParticles(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x73\x79\x73\x74\x65\x6D\x20\x74\x6F\x20\x6D\x6F\x64\x69\x66\x79"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_StarDisplayText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x74\x72\x6F\x41\x64\x76\x65\x6E\x74\x75\x72\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x65\x78\x74\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x63\x6F\x6C\x6C\x65\x63\x74\x65\x64\x20\x73\x74\x61\x72\x73\x20\x6E\x75\x6D\x62\x65\x72"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_LevelCompleteSplash(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x70\x6C\x61\x73\x68\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C\x20\x69\x73\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_LevelCompleteSplashFocus(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x67\x69\x76\x65\x20\x66\x6F\x63\x75\x73\x20\x74\x6F\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x20\x73\x70\x6C\x61\x73\x68\x20\x67\x65\x74\x73\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_Inventories(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x47\x55\x49\x20\x69\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x64\x69\x73\x70\x6C\x61\x79\x73"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_Stars(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x72\x65\x70\x72\x65\x73\x65\x6E\x74\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x6F\x6C\x6C\x65\x63\x74\x65\x64\x20\x73\x74\x61\x72\x73"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_StarOnColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x61\x20\x63\x6F\x6C\x6C\x65\x63\x74\x65\x64\x20\x73\x74\x61\x72\x20\x77\x69\x74\x68"), NULL);
	}
}
static void RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_StarOffColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x61\x20\x6E\x6F\x74\x20\x63\x6F\x6C\x6C\x65\x63\x74\x65\x64\x20\x73\x74\x61\x72\x20\x77\x69\x74\x68"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_SceneName(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x74\x6F\x20\x62\x69\x6E\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x65\x6C\x65\x6D\x65\x6E\x74"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_LockedIcon(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x69\x63\x6F\x6E\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C\x20\x69\x73\x20\x6C\x6F\x63\x6B\x65\x64"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_PlayButton(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x74\x6F\x20\x70\x72\x65\x73\x73\x20\x74\x6F\x20\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_ScenePreview(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x70\x20\x49\x6D\x61\x67\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x69\x6D\x61\x67\x65\x20\x73\x68\x6F\x77\x69\x6E\x67\x20\x61\x20\x70\x72\x65\x76\x69\x65\x77\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_OffMaterial(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C\x20\x69\x73\x20\x6F\x66\x66"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_Stars(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x74\x61\x72\x73\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x69\x6E\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C\x20\x65\x6C\x65\x6D\x65\x6E\x74"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x73"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_StarOffColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x73\x74\x61\x72\x73\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x79\x27\x72\x65\x20\x6C\x6F\x63\x6B\x65\x64"), NULL);
	}
}
static void RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_StarOnColor(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x73\x74\x61\x72\x73\x20\x6F\x6E\x63\x65\x20\x74\x68\x65\x79\x27\x76\x65\x20\x62\x65\x65\x6E\x20\x75\x6E\x6C\x6F\x63\x6B\x65\x64"), NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_U3CInitialMaximumLivesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_U3CInitialCurrentLivesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_Scenes(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6C\x69\x73\x74\x20\x6F\x66\x20\x73\x63\x65\x6E\x65\x73\x20\x74\x68\x61\x74\x20\x77\x65\x27\x6C\x6C\x20\x77\x61\x6E\x74\x20\x74\x6F\x20\x63\x6F\x6E\x73\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x6F\x75\x72\x20\x67\x61\x6D\x65"), NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_CreateSaveGameBtn(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x65\x61\x74\x65\x53\x61\x76\x65\x47\x61\x6D\x65"), NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_U3CCurrentStarsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x72\x67\x69\x20\x45\x6E\x67\x69\x6E\x65\x2F\x49\x74\x65\x6D\x73\x2F\x52\x65\x74\x72\x6F\x20\x53\x74\x61\x72"), NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_Demos_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_Demos_AttributeGenerators[53] = 
{
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator,
	RetroStar_tDEB2DEB51F5B86FCCFD0DFD768F820D91741581B_CustomAttributesCacheGenerator,
	BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_scaleSpeed,
	BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_scaleDistance,
	BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_rotationSpeed,
	BackgroundTree_t393268A917CCEEF84154F54845FB5BFFF18A7C56_CustomAttributesCacheGenerator_rotationAmplitude,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_PlayerID,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_CharacterSpeed,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_StartingPoint,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_U3CCollidingWithAPathElementU3Ek__BackingField,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_CharacterIsFacingRight,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_U3CLastVisitedPathElementU3Ek__BackingField,
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_AutomaticMovement,
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Up,
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Right,
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Down,
	LevelMapPathElement_t18F8366F4AE1DC5765E24A984ED292C64F7CCB75_CustomAttributesCacheGenerator_Left,
	RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC_CustomAttributesCacheGenerator_MaximumAllowedAngle,
	RetroCopter_tB7E3F1F7B6F07D56EB5B251EDC2790627FC39BAC_CustomAttributesCacheGenerator_CharacterRotationSpeed,
	RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985_CustomAttributesCacheGenerator_DoorColor,
	RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985_CustomAttributesCacheGenerator_DoorLightModel,
	RetroDoor_t8257F68B1DE5CB6FE96FFE45EF85F3B6EE382985_CustomAttributesCacheGenerator_DoorParticles,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_StarDisplayText,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_LevelCompleteSplash,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_LevelCompleteSplashFocus,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_Inventories,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_Stars,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_StarOnColor,
	RetroAdventureGUIManager_t1615915E1516886FEA0075C53A5ED3B810A2F7D8_CustomAttributesCacheGenerator_StarOffColor,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_SceneName,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_LockedIcon,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_PlayButton,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_ScenePreview,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_OffMaterial,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_Stars,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_StarOffColor,
	RetroAdventureLevel_t5B1DCF7162C55BEA35488C113ACD37BF9DD37E28_CustomAttributesCacheGenerator_StarOnColor,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_U3CInitialMaximumLivesU3Ek__BackingField,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_U3CInitialCurrentLivesU3Ek__BackingField,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_Scenes,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_CreateSaveGameBtn,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_U3CCurrentStarsU3Ek__BackingField,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_get_CollidingWithAPathElement_mCCA15865BC72A3A19055201923CC81A6607E99E2,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_set_CollidingWithAPathElement_m0A75744024164BE1FE90302F16C723B14D759899,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_get_LastVisitedPathElement_mAEFDFFBBDC955AC070D38813A94743647CC95583,
	LevelMapCharacter_t240479B4AA3A145F7C10FF7489389F99FFD9F5AD_CustomAttributesCacheGenerator_LevelMapCharacter_set_LastVisitedPathElement_m67A556D6095A94522F78CDF44465E63475230FD2,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_get_InitialMaximumLives_m9DB619542C6EF0F889893E62BC321730A01C76EB,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_set_InitialMaximumLives_m9A5BF8F4D5854AAC9C4CBD0820F62E67F4F43223,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_get_InitialCurrentLives_m00AB5EE9A7FD58A45C7F4631C39A3BA53C2FAB52,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_set_InitialCurrentLives_mDA292ECCB6F7864E80D01DF0669ACB137696DD4C,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_get_CurrentStars_m68881ACF328FE4CACCE49C70ED2FAE9AD2A16CDC,
	RetroAdventureProgressManager_t055C6C293FFC996B92F373F5ABCD66A6FC51D260_CustomAttributesCacheGenerator_RetroAdventureProgressManager_set_CurrentStars_m9D2F76098EFEAE02E1437C46D6C64A4C26E01A56,
	MoreMountains_CorgiEngine_Demos_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

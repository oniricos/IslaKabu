﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Collections.BitArray
struct BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// MoreMountains.Feedbacks.FeedbackHelpAttribute
struct FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124;
// MoreMountains.Feedbacks.FeedbackPathAttribute
struct FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// MoreMountains.Feedbacks.MMFEnumConditionAttribute
struct MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D;
// MoreMountains.Feedbacks.MMFInspectorButtonAttribute
struct MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// MoreMountains.Feedbacks.FeedbackHelpAttribute
struct FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String MoreMountains.Feedbacks.FeedbackHelpAttribute::HelpText
	String_t* ___HelpText_0;

public:
	inline static int32_t get_offset_of_HelpText_0() { return static_cast<int32_t>(offsetof(FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124, ___HelpText_0)); }
	inline String_t* get_HelpText_0() const { return ___HelpText_0; }
	inline String_t** get_address_of_HelpText_0() { return &___HelpText_0; }
	inline void set_HelpText_0(String_t* value)
	{
		___HelpText_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___HelpText_0), (void*)value);
	}
};


// MoreMountains.Feedbacks.FeedbackPathAttribute
struct FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String MoreMountains.Feedbacks.FeedbackPathAttribute::Path
	String_t* ___Path_0;
	// System.String MoreMountains.Feedbacks.FeedbackPathAttribute::Name
	String_t* ___Name_1;

public:
	inline static int32_t get_offset_of_Path_0() { return static_cast<int32_t>(offsetof(FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9, ___Path_0)); }
	inline String_t* get_Path_0() const { return ___Path_0; }
	inline String_t** get_address_of_Path_0() { return &___Path_0; }
	inline void set_Path_0(String_t* value)
	{
		___Path_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Path_0), (void*)value);
	}

	inline static int32_t get_offset_of_Name_1() { return static_cast<int32_t>(offsetof(FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9, ___Name_1)); }
	inline String_t* get_Name_1() const { return ___Name_1; }
	inline String_t** get_address_of_Name_1() { return &___Name_1; }
	inline void set_Name_1(String_t* value)
	{
		___Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_1), (void*)value);
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// MoreMountains.Feedbacks.MMFEnumConditionAttribute
struct MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Feedbacks.MMFEnumConditionAttribute::ConditionEnum
	String_t* ___ConditionEnum_0;
	// System.Boolean MoreMountains.Feedbacks.MMFEnumConditionAttribute::Hidden
	bool ___Hidden_1;
	// System.Collections.BitArray MoreMountains.Feedbacks.MMFEnumConditionAttribute::bitArray
	BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * ___bitArray_2;

public:
	inline static int32_t get_offset_of_ConditionEnum_0() { return static_cast<int32_t>(offsetof(MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D, ___ConditionEnum_0)); }
	inline String_t* get_ConditionEnum_0() const { return ___ConditionEnum_0; }
	inline String_t** get_address_of_ConditionEnum_0() { return &___ConditionEnum_0; }
	inline void set_ConditionEnum_0(String_t* value)
	{
		___ConditionEnum_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionEnum_0), (void*)value);
	}

	inline static int32_t get_offset_of_Hidden_1() { return static_cast<int32_t>(offsetof(MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D, ___Hidden_1)); }
	inline bool get_Hidden_1() const { return ___Hidden_1; }
	inline bool* get_address_of_Hidden_1() { return &___Hidden_1; }
	inline void set_Hidden_1(bool value)
	{
		___Hidden_1 = value;
	}

	inline static int32_t get_offset_of_bitArray_2() { return static_cast<int32_t>(offsetof(MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D, ___bitArray_2)); }
	inline BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * get_bitArray_2() const { return ___bitArray_2; }
	inline BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 ** get_address_of_bitArray_2() { return &___bitArray_2; }
	inline void set_bitArray_2(BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * value)
	{
		___bitArray_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bitArray_2), (void*)value);
	}
};


// MoreMountains.Feedbacks.MMFInspectorButtonAttribute
struct MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Feedbacks.MMFInspectorButtonAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_0), (void*)value);
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.FeedbackPathAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * __this, String_t* ___path0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.FeedbackHelpAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94 (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * __this, String_t* ___helpText0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFEnumConditionAttribute::.ctor(System.String,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * __this, String_t* ___conditionBoolean0, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___enumValues1, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFInspectorButtonAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFInspectorButtonAttribute__ctor_m9F7D6C8785E6F916CBCF7844EEDFDE4E942F648D (MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616 * __this, String_t* ___MethodName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
static void MoreMountains_Feedbacks_PostProcessing_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[0];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x42\x6C\x6F\x6F\x6D"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[2];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x62\x6C\x6F\x6F\x6D\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x61\x6E\x64\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E\x20\x49\x74\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x79\x6F\x75\x20\x68\x61\x76\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x56\x6F\x6C\x75\x6D\x65\x20\x77\x69\x74\x68\x20\x42\x6C\x6F\x6F\x6D\x20\x61\x63\x74\x69\x76\x65\x2C\x20\x61\x6E\x64\x20\x61\x20\x4D\x4D\x42\x6C\x6F\x6F\x6D\x53\x68\x61\x6B\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x6F\x6F\x6D"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ShakeDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x73\x68\x61\x6B\x65\x72\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RelativeValues(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ShakeIntensity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ShakeThreshold(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapThresholdZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapThresholdOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[0];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x63\x68\x72\x6F\x6D\x61\x74\x69\x63\x20\x61\x62\x65\x72\x72\x61\x74\x69\x6F\x6E\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E\x20\x49\x74\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x79\x6F\x75\x20\x68\x61\x76\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x56\x6F\x6C\x75\x6D\x65\x20\x77\x69\x74\x68\x20\x43\x68\x72\x6F\x6D\x61\x74\x69\x63\x20\x41\x62\x65\x72\x72\x61\x74\x69\x6F\x6E\x20\x61\x63\x74\x69\x76\x65\x2C\x20\x61\x6E\x64\x20\x61\x20\x4D\x4D\x43\x68\x72\x6F\x6D\x61\x74\x69\x63\x41\x62\x65\x72\x72\x61\x74\x69\x6F\x6E\x53\x68\x61\x6B\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[1];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x43\x68\x72\x6F\x6D\x61\x74\x69\x63\x20\x41\x62\x65\x72\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x72\x6F\x6D\x61\x74\x69\x63\x20\x41\x62\x65\x72\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x68\x61\x6B\x65\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x73\x68\x61\x6B\x65\x72\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Intensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Amplitude(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x72\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x63\x75\x72\x76\x65"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[0];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x63\x6F\x6C\x6F\x72\x20\x67\x72\x61\x64\x69\x6E\x67\x20\x70\x6F\x73\x74\x20\x65\x78\x70\x6F\x73\x75\x72\x65\x2C\x20\x68\x75\x65\x20\x73\x68\x69\x66\x74\x2C\x20\x73\x61\x74\x75\x72\x61\x74\x69\x6F\x6E\x20\x61\x6E\x64\x20\x63\x6F\x6E\x74\x72\x61\x73\x74\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E\x20\x49\x74\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x79\x6F\x75\x20\x68\x61\x76\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x56\x6F\x6C\x75\x6D\x65\x20\x77\x69\x74\x68\x20\x43\x6F\x6C\x6F\x72\x20\x47\x72\x61\x64\x69\x6E\x67\x20\x61\x63\x74\x69\x76\x65\x2C\x20\x61\x6E\x64\x20\x61\x20\x4D\x4D\x43\x6F\x6C\x6F\x72\x47\x72\x61\x64\x69\x6E\x67\x53\x68\x61\x6B\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[1];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x43\x6F\x6C\x6F\x72\x20\x47\x72\x61\x64\x69\x6E\x67"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x20\x47\x72\x61\x64\x69\x6E\x67"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x68\x61\x6B\x65\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x73\x68\x61\x6B\x65\x72\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakePostExposure(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x20\x45\x78\x70\x6F\x73\x75\x72\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x75\x73\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapPostExposureZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapPostExposureOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeHueShift(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x75\x65\x20\x53\x68\x69\x66\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x61\x70\x65\x72\x74\x75\x72\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapHueShiftZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -180.0f, 180.0f, NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapHueShiftOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -180.0f, 180.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeSaturation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x74\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapSaturationZero(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapSaturationOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeContrast(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x72\x61\x73\x74"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapContrastZero(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapContrastOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x64\x65\x70\x74\x68\x20\x6F\x66\x20\x66\x69\x65\x6C\x64\x20\x66\x6F\x63\x75\x73\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x2C\x20\x61\x70\x65\x72\x74\x75\x72\x65\x20\x61\x6E\x64\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E\x20\x49\x74\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x79\x6F\x75\x20\x68\x61\x76\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x56\x6F\x6C\x75\x6D\x65\x20\x77\x69\x74\x68\x20\x44\x65\x70\x74\x68\x20\x6F\x66\x20\x46\x69\x65\x6C\x64\x20\x61\x63\x74\x69\x76\x65\x2C\x20\x61\x6E\x64\x20\x61\x20\x4D\x4D\x44\x65\x70\x74\x68\x4F\x66\x46\x69\x65\x6C\x64\x53\x68\x61\x6B\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E"), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x44\x65\x70\x74\x68\x20\x4F\x66\x20\x46\x69\x65\x6C\x64"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x70\x74\x68\x20\x4F\x66\x20\x46\x69\x65\x6C\x64"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x68\x61\x6B\x65\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RelativeValues(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x76\x61\x6C\x75\x65\x73"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x73\x68\x61\x6B\x65\x72\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeFocusDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x75\x73\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x63\x75\x73\x20\x44\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocusDistanceZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocusDistanceOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeAperture(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x70\x65\x72\x74\x75\x72\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x61\x70\x65\x72\x74\x75\x72\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapApertureZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 32.0f, NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapApertureOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 32.0f, NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeFocalLength(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x63\x61\x6C\x20\x4C\x65\x6E\x67\x74\x68"), NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocalLengthZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 300.0f, NULL);
	}
}
static void MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocalLengthOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 300.0f, NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[0];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x6C\x65\x74\x20\x79\x6F\x75\x20\x70\x69\x6C\x6F\x74\x20\x61\x20\x47\x6C\x6F\x62\x61\x6C\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x20\x56\x6F\x6C\x75\x6D\x65\x20\x41\x75\x74\x6F\x42\x6C\x65\x6E\x64\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E\x20\x41\x20\x47\x50\x50\x56\x41\x42\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x70\x6C\x61\x63\x65\x64\x20\x6F\x6E\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x20\x56\x6F\x6C\x75\x6D\x65\x2C\x20\x61\x6E\x64\x20\x77\x69\x6C\x6C\x20\x6C\x65\x74\x20\x79\x6F\x75\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x61\x6E\x64\x20\x62\x6C\x65\x6E\x64\x20\x69\x74\x73\x20\x77\x65\x69\x67\x68\x74\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x20\x6F\x6E\x20\x64\x65\x6D\x61\x6E\x64\x2E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[2];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x47\x6C\x6F\x62\x61\x6C\x20\x50\x50\x20\x56\x6F\x6C\x75\x6D\x65\x20\x41\x75\x74\x6F\x20\x42\x6C\x65\x6E\x64"), NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_TargetAutoBlend(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x20\x56\x6F\x6C\x75\x6D\x65\x20\x42\x6C\x65\x6E\x64"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x61\x75\x74\x6F\x20\x62\x6C\x65\x6E\x64\x20\x74\x6F\x20\x70\x69\x6C\x6F\x74\x20\x77\x69\x74\x68\x20\x74\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B"), NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_Mode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x6F\x73\x65\x6E\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_BlendAction(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x6F\x73\x65\x6E\x20\x61\x63\x74\x69\x6F\x6E\x20\x77\x68\x65\x6E\x20\x69\x6E\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_BlendDuration(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x77\x68\x65\x6E\x20\x69\x6E\x20\x6F\x76\x65\x72\x72\x69\x64\x65\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_BlendCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[0];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64"), NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_InitialWeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x77\x65\x69\x67\x68\x74\x20\x74\x6F\x20\x62\x6C\x65\x6E\x64\x20\x66\x72\x6F\x6D"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_FinalWeight(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x77\x65\x69\x67\x68\x74\x20\x74\x6F\x20\x62\x6C\x65\x6E\x64\x20\x74\x6F"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_ResetToInitialValueOnEnd(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x61\x74\x20\x74\x68\x65\x20\x65\x6E\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x68\x61\x6B\x65"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D * tmp = (MMFEnumConditionAttribute_t73AB3B5B09E5DAB5D66899984A2363E107998B2D *)cache->attributes[1];
		MMFEnumConditionAttribute__ctor_m26433ABA8F0B35FFC2B3ACB20EE930C6F1453B7F(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[1];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x4C\x65\x6E\x73\x20\x44\x69\x73\x74\x6F\x72\x74\x69\x6F\x6E"), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[2];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x6C\x65\x6E\x73\x20\x64\x69\x73\x74\x6F\x72\x74\x69\x6F\x6E\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E\x20\x49\x74\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x79\x6F\x75\x20\x68\x61\x76\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x56\x6F\x6C\x75\x6D\x65\x20\x77\x69\x74\x68\x20\x4C\x65\x6E\x73\x20\x44\x69\x73\x74\x6F\x72\x74\x69\x6F\x6E\x20\x61\x63\x74\x69\x76\x65\x2C\x20\x61\x6E\x64\x20\x61\x20\x4D\x4D\x4C\x65\x6E\x73\x44\x69\x73\x74\x6F\x72\x74\x69\x6F\x6E\x53\x68\x61\x6B\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x6E\x73\x20\x44\x69\x73\x74\x6F\x72\x74\x69\x6F\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x68\x61\x6B\x65\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x73\x68\x61\x6B\x65\x72\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x76\x61\x6C\x75\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_Intensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
}
static void MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 * tmp = (FeedbackPathAttribute_tFACAA17FF56B49455F5EFCFD87CD00ADC6C81DC9 *)cache->attributes[0];
		FeedbackPathAttribute__ctor_mF36AB244DDE78F2BF1F0B9285F977B1D7212E2BC(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x2F\x56\x69\x67\x6E\x65\x74\x74\x65"), NULL);
	}
	{
		FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 * tmp = (FeedbackHelpAttribute_t9755CADBEB027A0FF097C3E9550DE4A47B241124 *)cache->attributes[1];
		FeedbackHelpAttribute__ctor_mACF45ABD072B9B8871B703E67DF4E7076C9AFF94(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x6C\x6C\x6F\x77\x73\x20\x79\x6F\x75\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x76\x69\x67\x6E\x65\x74\x74\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E\x20\x49\x74\x20\x72\x65\x71\x75\x69\x72\x65\x73\x20\x79\x6F\x75\x20\x68\x61\x76\x65\x20\x69\x6E\x20\x79\x6F\x75\x72\x20\x73\x63\x65\x6E\x65\x20\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x74\x68\x20\x61\x20\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x56\x6F\x6C\x75\x6D\x65\x20\x77\x69\x74\x68\x20\x56\x69\x67\x6E\x65\x74\x74\x65\x20\x61\x63\x74\x69\x76\x65\x2C\x20\x61\x6E\x64\x20\x61\x20\x4D\x4D\x56\x69\x67\x6E\x65\x74\x74\x65\x53\x68\x61\x6B\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2E"), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[2];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_Channel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x67\x6E\x65\x74\x74\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x61\x6E\x6E\x65\x6C\x20\x74\x6F\x20\x65\x6D\x69\x74\x20\x6F\x6E"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_Duration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x68\x61\x6B\x65\x2C\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x73\x68\x61\x6B\x65\x72\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x72\x65\x73\x65\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x66\x74\x65\x72\x20\x73\x68\x61\x6B\x65"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_Intensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x27\x73\x20\x7A\x65\x72\x6F\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x27\x73\x20\x6F\x6E\x65\x20\x74\x6F"), NULL);
	}
}
static void MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x47\x6C\x6F\x62\x61\x6C\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x56\x6F\x6C\x75\x6D\x65\x41\x75\x74\x6F\x42\x6C\x65\x6E\x64"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_BlendTriggerMode(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x65\x6E\x64"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x72\x69\x67\x67\x65\x72\x20\x6D\x6F\x64\x65\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x4D\x4D\x47\x6C\x6F\x62\x61\x6C\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x56\x6F\x6C\x75\x6D\x65\x41\x75\x74\x6F\x42\x6C\x65\x6E\x64"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_BlendDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_Curve(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x6F\x20\x62\x6C\x65\x6E\x64"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_InitialWeight(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x57\x65\x69\x67\x68\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x77\x65\x69\x67\x68\x74\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_FinalWeight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x65\x73\x69\x72\x65\x64\x20\x77\x65\x69\x67\x68\x74\x20\x61\x74\x20\x74\x68\x65\x20\x65\x6E\x64\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_TimeScale(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x65\x68\x61\x76\x69\x6F\x75\x72"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x69\x6D\x65\x73\x63\x61\x6C\x65\x20\x74\x6F\x20\x6F\x70\x65\x72\x61\x74\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_DisableVolumeOnZeroWeight(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x65\x20\x61\x73\x73\x6F\x63\x69\x61\x74\x65\x64\x20\x76\x6F\x6C\x75\x6D\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x64\x69\x73\x61\x62\x6C\x65\x64\x20\x61\x74\x20\x30"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_DisableSelfAfterEnd(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x69\x73\x20\x62\x6C\x65\x6E\x64\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x69\x74\x73\x65\x6C\x66\x20\x61\x74\x20\x30"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_Interruptable(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x69\x73\x20\x62\x6C\x65\x6E\x64\x65\x72\x20\x63\x61\x6E\x20\x62\x65\x20\x69\x6E\x74\x65\x72\x72\x75\x70\x74\x65\x64"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_StartFromCurrentValue(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x69\x73\x20\x62\x6C\x65\x6E\x64\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x70\x69\x63\x6B\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x76\x61\x6C\x75\x65\x20\x61\x73\x20\x69\x74\x73\x20\x73\x74\x61\x72\x74\x69\x6E\x67\x20\x70\x6F\x69\x6E\x74"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_ResetToInitialValueOnEnd(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x72\x65\x73\x65\x74\x20\x74\x6F\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E\x20\x65\x6E\x64\x20"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_TestBlend(CustomAttributesCache* cache)
{
	{
		MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616 * tmp = (MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616 *)cache->attributes[0];
		MMFInspectorButtonAttribute__ctor_m9F7D6C8785E6F916CBCF7844EEDFDE4E942F648D(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x65\x6E\x64"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x73\x74\x20\x62\x6C\x65\x6E\x64\x20\x62\x75\x74\x74\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x73"), NULL);
	}
}
static void MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_TestBlendBackwards(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x65\x73\x74\x20\x62\x6C\x65\x6E\x64\x20\x62\x61\x63\x6B\x20\x62\x75\x74\x74\x6F\x6E"), NULL);
	}
	{
		MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616 * tmp = (MMFInspectorButtonAttribute_tF12DBEFE9665D5613DEE45420C64D6EF99C57616 *)cache->attributes[1];
		MMFInspectorButtonAttribute__ctor_m9F7D6C8785E6F916CBCF7844EEDFDE4E942F648D(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x65\x6E\x64\x42\x61\x63\x6B"), NULL);
	}
}
static void MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x41\x75\x74\x6F\x46\x6F\x63\x75\x73"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
}
static void MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_CameraTransform(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x69\x6E\x64\x69\x6E\x67\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_FocusTargets(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x6C\x69\x73\x74\x20\x6F\x66\x20\x61\x6C\x6C\x20\x70\x6F\x73\x73\x69\x62\x6C\x65\x20\x74\x61\x72\x67\x65\x74\x73"), NULL);
	}
}
static void MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_Offset(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x6E\x20\x6F\x66\x66\x73\x65\x74\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x66\x6F\x63\x75\x73\x20\x74\x61\x72\x67\x65\x74"), NULL);
	}
}
static void MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_FocusTargetID(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x75\x70"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x74\x61\x72\x67\x65\x74\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x61\x75\x74\x6F\x20\x66\x6F\x63\x75\x73"), NULL);
	}
}
static void MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_Aperture(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 20.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x61\x70\x65\x72\x74\x75\x72\x65\x20\x74\x6F\x20\x77\x6F\x72\x6B\x20\x77\x69\x74\x68"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x69\x72\x65\x64\x20\x41\x70\x65\x72\x74\x75\x72\x65"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x42\x6C\x6F\x6F\x6D\x53\x68\x61\x6B\x65\x72"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_ShakeIntensity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_ShakeThreshold(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x72\x65\x73\x68\x6F\x6C\x64"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapThresholdZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapThresholdOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_CustomAttributesCacheGenerator_OnEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_CustomAttributesCacheGenerator_MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_CustomAttributesCacheGenerator_MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x43\x68\x72\x6F\x6D\x61\x74\x69\x63\x41\x62\x65\x72\x72\x61\x74\x69\x6F\x6E\x53\x68\x61\x6B\x65\x72"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
}
static void MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x76\x61\x6C\x75\x65"), NULL);
	}
}
static void MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_ShakeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_CustomAttributesCacheGenerator_OnEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_CustomAttributesCacheGenerator_MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_CustomAttributesCacheGenerator_MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x43\x6F\x6C\x6F\x72\x47\x72\x61\x64\x69\x6E\x67\x53\x68\x61\x6B\x65\x72"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakePostExposure(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x73\x74\x20\x45\x78\x70\x6F\x73\x75\x72\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x75\x73\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapPostExposureZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapPostExposureOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakeHueShift(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x75\x65\x20\x53\x68\x69\x66\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x61\x70\x65\x72\x74\x75\x72\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapHueShiftZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -180.0f, 180.0f, NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapHueShiftOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -180.0f, 180.0f, NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakeSaturation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x74\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapSaturationZero(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapSaturationOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakeContrast(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x72\x61\x73\x74"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapContrastZero(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapContrastOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
}
static void MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_CustomAttributesCacheGenerator_OnEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_CustomAttributesCacheGenerator_MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_CustomAttributesCacheGenerator_MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x44\x65\x70\x74\x68\x4F\x66\x46\x69\x65\x6C\x64\x53\x68\x61\x6B\x65\x72"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_ShakeFocusDistance(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x63\x75\x73\x20\x44\x69\x73\x74\x61\x6E\x63\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x75\x73\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocusDistanceZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocusDistanceOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_ShakeAperture(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x61\x70\x65\x72\x74\x75\x72\x65\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x70\x65\x72\x74\x75\x72\x65"), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapApertureZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 32.0f, NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapApertureOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.100000001f, 32.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_ShakeFocalLength(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x66\x6F\x63\x61\x6C\x20\x6C\x65\x6E\x67\x74\x68\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x63\x61\x6C\x20\x4C\x65\x6E\x67\x74\x68"), NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocalLengthZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 300.0f, NULL);
	}
}
static void MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocalLengthOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 300.0f, NULL);
	}
}
static void MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_CustomAttributesCacheGenerator_OnEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_CustomAttributesCacheGenerator_MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_CustomAttributesCacheGenerator_MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x4C\x65\x6E\x73\x44\x69\x73\x74\x6F\x72\x74\x69\x6F\x6E\x53\x68\x61\x6B\x65\x72"), NULL);
	}
}
static void MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x76\x61\x6C\x75\x65"), NULL);
	}
}
static void MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_ShakeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
}
static void MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, -100.0f, 100.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
}
static void MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_CustomAttributesCacheGenerator_OnEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_CustomAttributesCacheGenerator_MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_CustomAttributesCacheGenerator_MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x72\x65\x20\x4D\x6F\x75\x6E\x74\x61\x69\x6E\x73\x2F\x46\x65\x65\x64\x62\x61\x63\x6B\x73\x2F\x53\x68\x61\x6B\x65\x72\x73\x2F\x50\x6F\x73\x74\x50\x72\x6F\x63\x65\x73\x73\x69\x6E\x67\x2F\x4D\x4D\x56\x69\x67\x6E\x65\x74\x74\x65\x53\x68\x61\x6B\x65\x72"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_0_0_0_var), NULL);
	}
}
static void MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_RelativeIntensity(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x74\x65\x6E\x73\x69\x74\x79"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x61\x64\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x76\x61\x6C\x75\x65"), NULL);
	}
}
static void MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_ShakeIntensity(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x61\x6E\x69\x6D\x61\x74\x65\x20\x74\x68\x65\x20\x69\x6E\x74\x65\x6E\x73\x69\x74\x79\x20\x76\x61\x6C\x75\x65\x20\x6F\x6E"), NULL);
	}
}
static void MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_RemapIntensityZero(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x30\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_RemapIntensityOne(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x74\x6F\x20\x72\x65\x6D\x61\x70\x20\x74\x68\x65\x20\x63\x75\x72\x76\x65\x27\x73\x20\x31\x20\x74\x6F"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_CustomAttributesCacheGenerator_OnEvent(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_CustomAttributesCacheGenerator_MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_CustomAttributesCacheGenerator_MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_PostProcessing_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_PostProcessing_AttributeGenerators[166] = 
{
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator,
	MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator,
	MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator,
	MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator,
	MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_Channel,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ShakeDuration,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RelativeValues,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ShakeIntensity,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_ShakeThreshold,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapThresholdZero,
	MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6_CustomAttributesCacheGenerator_RemapThresholdOne,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Channel,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Duration,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Intensity,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_Amplitude,
	MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6_CustomAttributesCacheGenerator_RelativeIntensity,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_Channel,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeDuration,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RelativeIntensity,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakePostExposure,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapPostExposureZero,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapPostExposureOne,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeHueShift,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapHueShiftZero,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapHueShiftOne,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeSaturation,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapSaturationZero,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapSaturationOne,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_ShakeContrast,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapContrastZero,
	MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9_CustomAttributesCacheGenerator_RemapContrastOne,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_Channel,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeDuration,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RelativeValues,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeFocusDistance,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocusDistanceZero,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocusDistanceOne,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeAperture,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapApertureZero,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapApertureOne,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_ShakeFocalLength,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocalLengthZero,
	MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2_CustomAttributesCacheGenerator_RemapFocalLengthOne,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_TargetAutoBlend,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_Mode,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_BlendAction,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_BlendDuration,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_BlendCurve,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_InitialWeight,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_FinalWeight,
	MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68_CustomAttributesCacheGenerator_ResetToInitialValueOnEnd,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_Channel,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_Duration,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_RelativeIntensity,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_Intensity,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_Channel,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_Duration,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_ResetShakerValuesAfterShake,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_ResetTargetValuesAfterShake,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_Intensity,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1_CustomAttributesCacheGenerator_RelativeIntensity,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_BlendTriggerMode,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_BlendDuration,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_Curve,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_InitialWeight,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_FinalWeight,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_TimeScale,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_DisableVolumeOnZeroWeight,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_DisableSelfAfterEnd,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_Interruptable,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_StartFromCurrentValue,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_ResetToInitialValueOnEnd,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_TestBlend,
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744_CustomAttributesCacheGenerator_TestBlendBackwards,
	MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_CameraTransform,
	MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_FocusTargets,
	MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_Offset,
	MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_FocusTargetID,
	MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA_CustomAttributesCacheGenerator_Aperture,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_ShakeIntensity,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_ShakeThreshold,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapThresholdZero,
	MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE_CustomAttributesCacheGenerator_RemapThresholdOne,
	MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_CustomAttributesCacheGenerator_OnEvent,
	MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_RelativeIntensity,
	MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_ShakeIntensity,
	MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_CustomAttributesCacheGenerator_OnEvent,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakePostExposure,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapPostExposureZero,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapPostExposureOne,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakeHueShift,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapHueShiftZero,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapHueShiftOne,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakeSaturation,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapSaturationZero,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapSaturationOne,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_ShakeContrast,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapContrastZero,
	MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9_CustomAttributesCacheGenerator_RemapContrastOne,
	MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_CustomAttributesCacheGenerator_OnEvent,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_ShakeFocusDistance,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocusDistanceZero,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocusDistanceOne,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_ShakeAperture,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapApertureZero,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapApertureOne,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_ShakeFocalLength,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocalLengthZero,
	MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645_CustomAttributesCacheGenerator_RemapFocalLengthOne,
	MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_CustomAttributesCacheGenerator_OnEvent,
	MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_RelativeIntensity,
	MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_ShakeIntensity,
	MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_CustomAttributesCacheGenerator_OnEvent,
	MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_RelativeIntensity,
	MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_ShakeIntensity,
	MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_RemapIntensityZero,
	MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F_CustomAttributesCacheGenerator_RemapIntensityOne,
	MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_CustomAttributesCacheGenerator_OnEvent,
	MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_CustomAttributesCacheGenerator_MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413,
	MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_CustomAttributesCacheGenerator_MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465,
	MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_CustomAttributesCacheGenerator_MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704,
	MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_CustomAttributesCacheGenerator_MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B,
	MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_CustomAttributesCacheGenerator_MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653,
	MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_CustomAttributesCacheGenerator_MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24,
	MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_CustomAttributesCacheGenerator_MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553,
	MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_CustomAttributesCacheGenerator_MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC,
	MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_CustomAttributesCacheGenerator_MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE,
	MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_CustomAttributesCacheGenerator_MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398,
	MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_CustomAttributesCacheGenerator_MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290,
	MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_CustomAttributesCacheGenerator_MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563,
	MoreMountains_Feedbacks_PostProcessing_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.Canvas MoreMountains.Tools.MMUIFollowMouse::get_TargetCanvas()
extern void MMUIFollowMouse_get_TargetCanvas_mC2A16A1F1E3C181333FF7C33CD6D50DAD1032B35 (void);
// 0x00000002 System.Void MoreMountains.Tools.MMUIFollowMouse::set_TargetCanvas(UnityEngine.Canvas)
extern void MMUIFollowMouse_set_TargetCanvas_m8BA066A2E73DEF42A0D6295E481F391B312C54D1 (void);
// 0x00000003 System.Void MoreMountains.Tools.MMUIFollowMouse::LateUpdate()
extern void MMUIFollowMouse_LateUpdate_mA5D7AB2CA4091CF5E612C6D569B2E8B888C5E683 (void);
// 0x00000004 System.Void MoreMountains.Tools.MMUIFollowMouse::.ctor()
extern void MMUIFollowMouse__ctor_m894816A4C231066E405B17D111F2CB528311D980 (void);
// 0x00000005 System.Void MoreMountains.MMInterface.MMCarousel::Start()
extern void MMCarousel_Start_mE4C7BB5CF90DABBCBFC446BD7EB82F43D389F270 (void);
// 0x00000006 System.Void MoreMountains.MMInterface.MMCarousel::Initialization()
extern void MMCarousel_Initialization_mDA14D60DD9EEA882005F56C45171798223BAE7FE (void);
// 0x00000007 System.Void MoreMountains.MMInterface.MMCarousel::MoveLeft()
extern void MMCarousel_MoveLeft_mE3CAD5465A91B53032DB50BF8D178AA431407D5D (void);
// 0x00000008 System.Void MoreMountains.MMInterface.MMCarousel::MoveRight()
extern void MMCarousel_MoveRight_m7BCBBBCFC158002870A857E1ADADF146911AD111 (void);
// 0x00000009 System.Void MoreMountains.MMInterface.MMCarousel::MoveToCurrentIndex()
extern void MMCarousel_MoveToCurrentIndex_m183FC7EF9B039C1E15354AD44620AACB7E2CAE68 (void);
// 0x0000000A UnityEngine.Vector2 MoreMountains.MMInterface.MMCarousel::DeterminePosition()
extern void MMCarousel_DeterminePosition_m0A5C12863533663259E47140A3D086253CA2DBF4 (void);
// 0x0000000B System.Boolean MoreMountains.MMInterface.MMCarousel::CanMoveLeft()
extern void MMCarousel_CanMoveLeft_mEECDA148343FC7BEDA95F7CD765FE8E8E6608F50 (void);
// 0x0000000C System.Boolean MoreMountains.MMInterface.MMCarousel::CanMoveRight()
extern void MMCarousel_CanMoveRight_m5626332C9BC0BB6302DEA9FF1D74691417A4F022 (void);
// 0x0000000D System.Void MoreMountains.MMInterface.MMCarousel::Update()
extern void MMCarousel_Update_m65D90AFBAD10CC3A24536A9E305F30A6044A6BEC (void);
// 0x0000000E System.Void MoreMountains.MMInterface.MMCarousel::HandleFocus()
extern void MMCarousel_HandleFocus_m4752F4B09A11CE67B13C7E506BB86E573E38B2AE (void);
// 0x0000000F System.Void MoreMountains.MMInterface.MMCarousel::HandleButtons()
extern void MMCarousel_HandleButtons_m240B6F8AC886197FD2CC7D9464A7B947A45A3D2D (void);
// 0x00000010 System.Void MoreMountains.MMInterface.MMCarousel::LerpPosition()
extern void MMCarousel_LerpPosition_mC2282F41656B7410E1F4AB984AA3B06C296193D7 (void);
// 0x00000011 System.Void MoreMountains.MMInterface.MMCarousel::.ctor()
extern void MMCarousel__ctor_mCB40B2937BB85CA644781DDD6C7A47ED6D85C0C9 (void);
// 0x00000012 System.Void MoreMountains.MMInterface.MMCheckbox::.ctor()
extern void MMCheckbox__ctor_mA8526B59860BCBB25BBCC3D24513714E0EE72861 (void);
// 0x00000013 System.Void MoreMountains.MMInterface.MMPopup::Start()
extern void MMPopup_Start_mCA105B08C30A9F3A46E31B17FA0C92EF10AB9C76 (void);
// 0x00000014 System.Void MoreMountains.MMInterface.MMPopup::Initialization()
extern void MMPopup_Initialization_mB5A7A23B7EE7899B4B63370CF3ECBCD6CC0D5C7B (void);
// 0x00000015 System.Void MoreMountains.MMInterface.MMPopup::Update()
extern void MMPopup_Update_mCE636A5676A840799AEB8C00F1505922076E0461 (void);
// 0x00000016 System.Void MoreMountains.MMInterface.MMPopup::Open()
extern void MMPopup_Open_mDE80DD95521FD1706FE55AC8B2CE7A301031554E (void);
// 0x00000017 System.Void MoreMountains.MMInterface.MMPopup::Close()
extern void MMPopup_Close_m3B70A7B771D26B6C057316CDB3918AF4252255A5 (void);
// 0x00000018 System.Void MoreMountains.MMInterface.MMPopup::.ctor()
extern void MMPopup__ctor_m27D20B73DDA54377C1A3FF7009DEAF400D4A6FAE (void);
// 0x00000019 System.Void MoreMountains.MMInterface.MMRadioButton::Initialization()
extern void MMRadioButton_Initialization_m3EC6B87D3DE6D7D37D3B2CDA748C383081BB132D (void);
// 0x0000001A System.Void MoreMountains.MMInterface.MMRadioButton::FindAllRadioButtonsFromTheSameGroup()
extern void MMRadioButton_FindAllRadioButtonsFromTheSameGroup_m369984A4F9D5D7FD548DE5449A0101BB1B4EEA27 (void);
// 0x0000001B System.Void MoreMountains.MMInterface.MMRadioButton::SpriteOn()
extern void MMRadioButton_SpriteOn_mE1EDB982AFAF65896C974D2AC0DF3335E82653D3 (void);
// 0x0000001C System.Void MoreMountains.MMInterface.MMRadioButton::.ctor()
extern void MMRadioButton__ctor_m93A2B3046847773420BA36B0A373665FADF78E5C (void);
// 0x0000001D System.Void MoreMountains.MMInterface.MMScrollviewButton::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void MMScrollviewButton_OnPointerClick_m6AAA323D5F92DBD3FB5FD8984AF23C9DCE19C351 (void);
// 0x0000001E System.Void MoreMountains.MMInterface.MMScrollviewButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void MMScrollviewButton_OnPointerUp_m79FC84243CE3045EDBB24AB02C1465034C1D3444 (void);
// 0x0000001F System.Void MoreMountains.MMInterface.MMScrollviewButton::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void MMScrollviewButton_OnPointerExit_m3C2AB6430843B1273A1A09A69E04DE6EE74FAFCE (void);
// 0x00000020 System.Void MoreMountains.MMInterface.MMScrollviewButton::.ctor()
extern void MMScrollviewButton__ctor_m67178AA224BFE371B93AA66FB0DB00CD8273ABD2 (void);
// 0x00000021 System.Boolean MoreMountains.MMInterface.MMSpriteReplace::get_CurrentValue()
extern void MMSpriteReplace_get_CurrentValue_mE6977FAD1DCE5658FEB12D80559A54CF13A05973 (void);
// 0x00000022 System.Void MoreMountains.MMInterface.MMSpriteReplace::Start()
extern void MMSpriteReplace_Start_m88D33646C54F16A8EF37928403BB896A841EC65C (void);
// 0x00000023 System.Void MoreMountains.MMInterface.MMSpriteReplace::Initialization()
extern void MMSpriteReplace_Initialization_mB9571301F23D2C4CD01EB65731A7A898462D6A04 (void);
// 0x00000024 System.Void MoreMountains.MMInterface.MMSpriteReplace::Swap()
extern void MMSpriteReplace_Swap_m4197AB580E192763B59FDFF92627CB9858CA8FA2 (void);
// 0x00000025 System.Void MoreMountains.MMInterface.MMSpriteReplace::SwitchToOffSprite()
extern void MMSpriteReplace_SwitchToOffSprite_mF6644686D00A545FA928DD646BF7FA6E105F7112 (void);
// 0x00000026 System.Void MoreMountains.MMInterface.MMSpriteReplace::SpriteOff()
extern void MMSpriteReplace_SpriteOff_m8F4313C8731306BC2597C23DC803F1E37CCE2D62 (void);
// 0x00000027 System.Void MoreMountains.MMInterface.MMSpriteReplace::SwitchToOnSprite()
extern void MMSpriteReplace_SwitchToOnSprite_m42575E627AF23AE846981FAC0E386B3BB4A0A7C5 (void);
// 0x00000028 System.Void MoreMountains.MMInterface.MMSpriteReplace::SpriteOn()
extern void MMSpriteReplace_SpriteOn_mD58D51A9B5106186E679A7A06E64932A764E82F8 (void);
// 0x00000029 System.Void MoreMountains.MMInterface.MMSpriteReplace::.ctor()
extern void MMSpriteReplace__ctor_m406EAC78A0B19BE425E2379824C723B8C23075F4 (void);
// 0x0000002A System.Void MoreMountains.MMInterface.MMSpriteReplaceCycle::Start()
extern void MMSpriteReplaceCycle_Start_m127CD5A253618D8173B13909ED5B2143F512C2C5 (void);
// 0x0000002B System.Void MoreMountains.MMInterface.MMSpriteReplaceCycle::Initialization()
extern void MMSpriteReplaceCycle_Initialization_m1ADDDC010D6965051A45F2397460CAB10F731049 (void);
// 0x0000002C System.Void MoreMountains.MMInterface.MMSpriteReplaceCycle::Swap()
extern void MMSpriteReplaceCycle_Swap_m90F8BB4324F7AE6B384F47A238C9C70F7E0F8963 (void);
// 0x0000002D System.Void MoreMountains.MMInterface.MMSpriteReplaceCycle::SwitchToIndex(System.Int32)
extern void MMSpriteReplaceCycle_SwitchToIndex_mCB40D68AB7FEB5BD10ED171538C20DDF606DD117 (void);
// 0x0000002E System.Void MoreMountains.MMInterface.MMSpriteReplaceCycle::.ctor()
extern void MMSpriteReplaceCycle__ctor_mE70EE4879604EBC9A2D9FF9B53FE69A067A0DEC0 (void);
// 0x0000002F MoreMountains.MMInterface.MMSwitch/SwitchStates MoreMountains.MMInterface.MMSwitch::get_CurrentSwitchState()
extern void MMSwitch_get_CurrentSwitchState_mEE26F4B05326516E8EF17897C105D796F7578E89 (void);
// 0x00000030 System.Void MoreMountains.MMInterface.MMSwitch::set_CurrentSwitchState(MoreMountains.MMInterface.MMSwitch/SwitchStates)
extern void MMSwitch_set_CurrentSwitchState_m42DBB03445030634F3D00B3EC01160E02FFD4211 (void);
// 0x00000031 System.Void MoreMountains.MMInterface.MMSwitch::Initialization()
extern void MMSwitch_Initialization_mB70440BDF4E746161C6F88E16B1636EEBC4D52DB (void);
// 0x00000032 System.Void MoreMountains.MMInterface.MMSwitch::InitializeState()
extern void MMSwitch_InitializeState_mFA030DE846787A27958D5F449D73629CBF7B6011 (void);
// 0x00000033 System.Void MoreMountains.MMInterface.MMSwitch::SwitchState()
extern void MMSwitch_SwitchState_mB785E363378F3C37C27063BE0B1948C3529B4BDC (void);
// 0x00000034 System.Void MoreMountains.MMInterface.MMSwitch::.ctor()
extern void MMSwitch__ctor_m7F302884996082013EE598A23BAF59B614B2E014 (void);
static Il2CppMethodPointer s_methodPointers[52] = 
{
	MMUIFollowMouse_get_TargetCanvas_mC2A16A1F1E3C181333FF7C33CD6D50DAD1032B35,
	MMUIFollowMouse_set_TargetCanvas_m8BA066A2E73DEF42A0D6295E481F391B312C54D1,
	MMUIFollowMouse_LateUpdate_mA5D7AB2CA4091CF5E612C6D569B2E8B888C5E683,
	MMUIFollowMouse__ctor_m894816A4C231066E405B17D111F2CB528311D980,
	MMCarousel_Start_mE4C7BB5CF90DABBCBFC446BD7EB82F43D389F270,
	MMCarousel_Initialization_mDA14D60DD9EEA882005F56C45171798223BAE7FE,
	MMCarousel_MoveLeft_mE3CAD5465A91B53032DB50BF8D178AA431407D5D,
	MMCarousel_MoveRight_m7BCBBBCFC158002870A857E1ADADF146911AD111,
	MMCarousel_MoveToCurrentIndex_m183FC7EF9B039C1E15354AD44620AACB7E2CAE68,
	MMCarousel_DeterminePosition_m0A5C12863533663259E47140A3D086253CA2DBF4,
	MMCarousel_CanMoveLeft_mEECDA148343FC7BEDA95F7CD765FE8E8E6608F50,
	MMCarousel_CanMoveRight_m5626332C9BC0BB6302DEA9FF1D74691417A4F022,
	MMCarousel_Update_m65D90AFBAD10CC3A24536A9E305F30A6044A6BEC,
	MMCarousel_HandleFocus_m4752F4B09A11CE67B13C7E506BB86E573E38B2AE,
	MMCarousel_HandleButtons_m240B6F8AC886197FD2CC7D9464A7B947A45A3D2D,
	MMCarousel_LerpPosition_mC2282F41656B7410E1F4AB984AA3B06C296193D7,
	MMCarousel__ctor_mCB40B2937BB85CA644781DDD6C7A47ED6D85C0C9,
	MMCheckbox__ctor_mA8526B59860BCBB25BBCC3D24513714E0EE72861,
	MMPopup_Start_mCA105B08C30A9F3A46E31B17FA0C92EF10AB9C76,
	MMPopup_Initialization_mB5A7A23B7EE7899B4B63370CF3ECBCD6CC0D5C7B,
	MMPopup_Update_mCE636A5676A840799AEB8C00F1505922076E0461,
	MMPopup_Open_mDE80DD95521FD1706FE55AC8B2CE7A301031554E,
	MMPopup_Close_m3B70A7B771D26B6C057316CDB3918AF4252255A5,
	MMPopup__ctor_m27D20B73DDA54377C1A3FF7009DEAF400D4A6FAE,
	MMRadioButton_Initialization_m3EC6B87D3DE6D7D37D3B2CDA748C383081BB132D,
	MMRadioButton_FindAllRadioButtonsFromTheSameGroup_m369984A4F9D5D7FD548DE5449A0101BB1B4EEA27,
	MMRadioButton_SpriteOn_mE1EDB982AFAF65896C974D2AC0DF3335E82653D3,
	MMRadioButton__ctor_m93A2B3046847773420BA36B0A373665FADF78E5C,
	MMScrollviewButton_OnPointerClick_m6AAA323D5F92DBD3FB5FD8984AF23C9DCE19C351,
	MMScrollviewButton_OnPointerUp_m79FC84243CE3045EDBB24AB02C1465034C1D3444,
	MMScrollviewButton_OnPointerExit_m3C2AB6430843B1273A1A09A69E04DE6EE74FAFCE,
	MMScrollviewButton__ctor_m67178AA224BFE371B93AA66FB0DB00CD8273ABD2,
	MMSpriteReplace_get_CurrentValue_mE6977FAD1DCE5658FEB12D80559A54CF13A05973,
	MMSpriteReplace_Start_m88D33646C54F16A8EF37928403BB896A841EC65C,
	MMSpriteReplace_Initialization_mB9571301F23D2C4CD01EB65731A7A898462D6A04,
	MMSpriteReplace_Swap_m4197AB580E192763B59FDFF92627CB9858CA8FA2,
	MMSpriteReplace_SwitchToOffSprite_mF6644686D00A545FA928DD646BF7FA6E105F7112,
	MMSpriteReplace_SpriteOff_m8F4313C8731306BC2597C23DC803F1E37CCE2D62,
	MMSpriteReplace_SwitchToOnSprite_m42575E627AF23AE846981FAC0E386B3BB4A0A7C5,
	MMSpriteReplace_SpriteOn_mD58D51A9B5106186E679A7A06E64932A764E82F8,
	MMSpriteReplace__ctor_m406EAC78A0B19BE425E2379824C723B8C23075F4,
	MMSpriteReplaceCycle_Start_m127CD5A253618D8173B13909ED5B2143F512C2C5,
	MMSpriteReplaceCycle_Initialization_m1ADDDC010D6965051A45F2397460CAB10F731049,
	MMSpriteReplaceCycle_Swap_m90F8BB4324F7AE6B384F47A238C9C70F7E0F8963,
	MMSpriteReplaceCycle_SwitchToIndex_mCB40D68AB7FEB5BD10ED171538C20DDF606DD117,
	MMSpriteReplaceCycle__ctor_mE70EE4879604EBC9A2D9FF9B53FE69A067A0DEC0,
	MMSwitch_get_CurrentSwitchState_mEE26F4B05326516E8EF17897C105D796F7578E89,
	MMSwitch_set_CurrentSwitchState_m42DBB03445030634F3D00B3EC01160E02FFD4211,
	MMSwitch_Initialization_mB70440BDF4E746161C6F88E16B1636EEBC4D52DB,
	MMSwitch_InitializeState_mFA030DE846787A27958D5F449D73629CBF7B6011,
	MMSwitch_SwitchState_mB785E363378F3C37C27063BE0B1948C3529B4BDC,
	MMSwitch__ctor_m7F302884996082013EE598A23BAF59B614B2E014,
};
static const int32_t s_InvokerIndices[52] = 
{
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3500,
	3473,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	2815,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3424,
	2775,
	3506,
	3506,
	3506,
	3506,
};
extern const CustomAttributesCacheGenerator g_MoreMountains_Interface_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_Interface_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_Interface_CodeGenModule = 
{
	"MoreMountains.Interface.dll",
	52,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_MoreMountains_Interface_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

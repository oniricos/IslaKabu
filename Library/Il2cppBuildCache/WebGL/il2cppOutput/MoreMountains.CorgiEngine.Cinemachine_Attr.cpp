﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Collections.BitArray
struct BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// MoreMountains.Tools.MMConditionAttribute
struct MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16;
// MoreMountains.Tools.MMEnumConditionAttribute
struct MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183;
// MoreMountains.Tools.MMInformationAttribute
struct MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45;
// MoreMountains.Tools.MMInspectorButtonAttribute
struct MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898;
// MoreMountains.Tools.MMInspectorGroupAttribute
struct MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E;
// MoreMountains.Tools.MMReadOnlyAttribute
struct MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99;
// MoreMountains.Tools.MMVectorAttribute
struct MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// Cinemachine.SaveDuringPlayAttribute
struct SaveDuringPlayAttribute_t5A81C21D4249BDE8C321218AE491D5F10F9342C9;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_0_0_0_var;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// Cinemachine.SaveDuringPlayAttribute
struct SaveDuringPlayAttribute_t5A81C21D4249BDE8C321218AE491D5F10F9342C9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// MoreMountains.Tools.MMConditionAttribute
struct MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMConditionAttribute::ConditionBoolean
	String_t* ___ConditionBoolean_0;
	// System.Boolean MoreMountains.Tools.MMConditionAttribute::Hidden
	bool ___Hidden_1;

public:
	inline static int32_t get_offset_of_ConditionBoolean_0() { return static_cast<int32_t>(offsetof(MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16, ___ConditionBoolean_0)); }
	inline String_t* get_ConditionBoolean_0() const { return ___ConditionBoolean_0; }
	inline String_t** get_address_of_ConditionBoolean_0() { return &___ConditionBoolean_0; }
	inline void set_ConditionBoolean_0(String_t* value)
	{
		___ConditionBoolean_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionBoolean_0), (void*)value);
	}

	inline static int32_t get_offset_of_Hidden_1() { return static_cast<int32_t>(offsetof(MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16, ___Hidden_1)); }
	inline bool get_Hidden_1() const { return ___Hidden_1; }
	inline bool* get_address_of_Hidden_1() { return &___Hidden_1; }
	inline void set_Hidden_1(bool value)
	{
		___Hidden_1 = value;
	}
};


// MoreMountains.Tools.MMEnumConditionAttribute
struct MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMEnumConditionAttribute::ConditionEnum
	String_t* ___ConditionEnum_0;
	// System.Boolean MoreMountains.Tools.MMEnumConditionAttribute::Hidden
	bool ___Hidden_1;
	// System.Collections.BitArray MoreMountains.Tools.MMEnumConditionAttribute::bitArray
	BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * ___bitArray_2;

public:
	inline static int32_t get_offset_of_ConditionEnum_0() { return static_cast<int32_t>(offsetof(MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183, ___ConditionEnum_0)); }
	inline String_t* get_ConditionEnum_0() const { return ___ConditionEnum_0; }
	inline String_t** get_address_of_ConditionEnum_0() { return &___ConditionEnum_0; }
	inline void set_ConditionEnum_0(String_t* value)
	{
		___ConditionEnum_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ConditionEnum_0), (void*)value);
	}

	inline static int32_t get_offset_of_Hidden_1() { return static_cast<int32_t>(offsetof(MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183, ___Hidden_1)); }
	inline bool get_Hidden_1() const { return ___Hidden_1; }
	inline bool* get_address_of_Hidden_1() { return &___Hidden_1; }
	inline void set_Hidden_1(bool value)
	{
		___Hidden_1 = value;
	}

	inline static int32_t get_offset_of_bitArray_2() { return static_cast<int32_t>(offsetof(MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183, ___bitArray_2)); }
	inline BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * get_bitArray_2() const { return ___bitArray_2; }
	inline BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 ** get_address_of_bitArray_2() { return &___bitArray_2; }
	inline void set_bitArray_2(BitArray_t5AA7A3FADA9F63D23407525B6CDC96FBB70275A0 * value)
	{
		___bitArray_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bitArray_2), (void*)value);
	}
};


// MoreMountains.Tools.MMInformationAttribute
struct MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// MoreMountains.Tools.MMInspectorButtonAttribute
struct MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMInspectorButtonAttribute::MethodName
	String_t* ___MethodName_0;

public:
	inline static int32_t get_offset_of_MethodName_0() { return static_cast<int32_t>(offsetof(MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898, ___MethodName_0)); }
	inline String_t* get_MethodName_0() const { return ___MethodName_0; }
	inline String_t** get_address_of_MethodName_0() { return &___MethodName_0; }
	inline void set_MethodName_0(String_t* value)
	{
		___MethodName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MethodName_0), (void*)value);
	}
};


// MoreMountains.Tools.MMInspectorGroupAttribute
struct MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String MoreMountains.Tools.MMInspectorGroupAttribute::GroupName
	String_t* ___GroupName_0;
	// System.Boolean MoreMountains.Tools.MMInspectorGroupAttribute::GroupAllFieldsUntilNextGroupAttribute
	bool ___GroupAllFieldsUntilNextGroupAttribute_1;
	// System.Int32 MoreMountains.Tools.MMInspectorGroupAttribute::GroupColorIndex
	int32_t ___GroupColorIndex_2;

public:
	inline static int32_t get_offset_of_GroupName_0() { return static_cast<int32_t>(offsetof(MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E, ___GroupName_0)); }
	inline String_t* get_GroupName_0() const { return ___GroupName_0; }
	inline String_t** get_address_of_GroupName_0() { return &___GroupName_0; }
	inline void set_GroupName_0(String_t* value)
	{
		___GroupName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroupName_0), (void*)value);
	}

	inline static int32_t get_offset_of_GroupAllFieldsUntilNextGroupAttribute_1() { return static_cast<int32_t>(offsetof(MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E, ___GroupAllFieldsUntilNextGroupAttribute_1)); }
	inline bool get_GroupAllFieldsUntilNextGroupAttribute_1() const { return ___GroupAllFieldsUntilNextGroupAttribute_1; }
	inline bool* get_address_of_GroupAllFieldsUntilNextGroupAttribute_1() { return &___GroupAllFieldsUntilNextGroupAttribute_1; }
	inline void set_GroupAllFieldsUntilNextGroupAttribute_1(bool value)
	{
		___GroupAllFieldsUntilNextGroupAttribute_1 = value;
	}

	inline static int32_t get_offset_of_GroupColorIndex_2() { return static_cast<int32_t>(offsetof(MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E, ___GroupColorIndex_2)); }
	inline int32_t get_GroupColorIndex_2() const { return ___GroupColorIndex_2; }
	inline int32_t* get_address_of_GroupColorIndex_2() { return &___GroupColorIndex_2; }
	inline void set_GroupColorIndex_2(int32_t value)
	{
		___GroupColorIndex_2 = value;
	}
};


// MoreMountains.Tools.MMReadOnlyAttribute
struct MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:

public:
};


// MoreMountains.Tools.MMVectorAttribute
struct MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String[] MoreMountains.Tools.MMVectorAttribute::Labels
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___Labels_0;

public:
	inline static int32_t get_offset_of_Labels_0() { return static_cast<int32_t>(offsetof(MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3, ___Labels_0)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_Labels_0() const { return ___Labels_0; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_Labels_0() { return &___Labels_0; }
	inline void set_Labels_0(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___Labels_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Labels_0), (void*)value);
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMInformationAttribute/InformationType
struct InformationType_t633DB4CABDA4EA9C03B284CF077738EFB3C2571A 
{
public:
	// System.Int32 MoreMountains.Tools.MMInformationAttribute/InformationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InformationType_t633DB4CABDA4EA9C03B284CF077738EFB3C2571A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void Cinemachine.SaveDuringPlayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SaveDuringPlayAttribute__ctor_m7E6683931B2115F1902B29FCC2B4ED8A166AE4FE (SaveDuringPlayAttribute_t5A81C21D4249BDE8C321218AE491D5F10F9342C9 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMEnumConditionAttribute::.ctor(System.String,System.Int32[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMEnumConditionAttribute__ctor_mA81D882C4D2DBD8A25C969EAE7BB6846FD0AE3BE (MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 * __this, String_t* ___conditionBoolean0, Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___enumValues1, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMVectorAttribute::.ctor(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVectorAttribute__ctor_mC708CBE51447649AC93A4B54B7480286ED8BBB52 (MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___labels0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMReadOnlyAttribute__ctor_m542563E5945344E9F070E568F1073E7325B59374 (MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMInformationAttribute::.ctor(System.String,MoreMountains.Tools.MMInformationAttribute/InformationType,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588 (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * __this, String_t* ___message0, int32_t ___type1, bool ___messageAfterProperty2, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMConditionAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * __this, String_t* ___conditionBoolean0, bool ___hideInInspector1, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMInspectorButtonAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6 (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * __this, String_t* ___MethodName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Tools.MMInspectorGroupAttribute::.ctor(System.String,System.Boolean,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167 (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * __this, String_t* ___groupName0, bool ___groupAllFieldsUntilNextGroupAttribute1, int32_t ___groupColorIndex2, const RuntimeMethod* method);
static void MoreMountains_CorgiEngine_Cinemachine_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		SaveDuringPlayAttribute_t5A81C21D4249BDE8C321218AE491D5F10F9342C9 * tmp = (SaveDuringPlayAttribute_t5A81C21D4249BDE8C321218AE491D5F10F9342C9 *)cache->attributes[1];
		SaveDuringPlayAttribute__ctor_m7E6683931B2115F1902B29FCC2B4ED8A166AE4FE(tmp, NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[2];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_LockXAxis(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x61\x78\x69\x73\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x6C\x6F\x63\x6B\x65\x64\x20\x6F\x6E\x20\x58"), NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_LockYAxis(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x61\x78\x69\x73\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x6C\x6F\x63\x6B\x65\x64\x20\x6F\x6E\x20\x59"), NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_LockZAxis(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x61\x78\x69\x73\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x6C\x6F\x63\x6B\x65\x64\x20\x6F\x6E\x20\x5A"), NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_Method(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x6D\x65\x74\x68\x6F\x64\x20\x74\x6F\x20\x6C\x6F\x63\x6B\x20\x61\x78\x69\x73\x20\x6F\x6E\x20"), NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_ForcedPosition(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 0);
		MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 * tmp = (MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 *)cache->attributes[0];
		MMEnumConditionAttribute__ctor_mA81D882C4D2DBD8A25C969EAE7BB6846FD0AE3BE(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x74\x68\x6F\x64"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x74\x6F\x20\x6C\x6F\x63\x6B\x20\x61\x78\x69\x73\x20\x62\x61\x73\x65\x64\x20\x6F\x6E"), NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_TargetCollider(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x20\x74\x6F\x20\x6C\x6F\x63\x6B\x20\x61\x78\x69\x73\x20\x6F\x6E"), NULL);
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 2);
		MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 * tmp = (MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 *)cache->attributes[1];
		MMEnumConditionAttribute__ctor_mA81D882C4D2DBD8A25C969EAE7BB6846FD0AE3BE(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x74\x68\x6F\x64"), _tmp_1, NULL);
	}
}
static void CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_TargetCollider2D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 3);
		MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 * tmp = (MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 *)cache->attributes[0];
		MMEnumConditionAttribute__ctor_mA81D882C4D2DBD8A25C969EAE7BB6846FD0AE3BE(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x74\x68\x6F\x64"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x32\x44\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x20\x74\x6F\x20\x6C\x6F\x63\x6B\x20\x61\x78\x69\x73\x20\x6F\x6E"), NULL);
	}
}
static void CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CinemachineBrain_tD6C5DB266906E4033463D5B2F94EA48246959E83_0_0_0_var), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_U3CFollowsPlayerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_FollowsAPlayer(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x69\x73\x20\x63\x61\x6D\x65\x72\x61\x20\x77\x69\x6C\x6C\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x61\x20\x70\x6C\x61\x79\x65\x72"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_ConfineCameraToLevelBounds(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x63\x61\x6D\x65\x72\x61\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x63\x6F\x6E\x66\x69\x6E\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x62\x6F\x75\x6E\x64\x73\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x64\x20\x69\x6E\x20\x74\x68\x65\x20\x4C\x65\x76\x65\x6C\x4D\x61\x6E\x61\x67\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_ManualUpDownLookDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x77\x20\x68\x69\x67\x68\x20\x28\x6F\x72\x20\x6C\x6F\x77\x29\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x50\x6C\x61\x79\x65\x72\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x73\x68\x6F\x75\x6C\x64\x20\x6D\x6F\x76\x65\x20\x77\x68\x65\x6E\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x75\x70\x2F\x64\x6F\x77\x6E"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_CharacterSpeed(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6D\x69\x6E\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x73\x70\x65\x65\x64\x20\x74\x6F\x20\x63\x6F\x6E\x73\x69\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x28\x77\x68\x65\x6E\x20\x64\x65\x61\x6C\x69\x6E\x67\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x7A\x6F\x6F\x6D\x29"), NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E"));
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x4D\x61\x78"));
		MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 * tmp = (MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 *)cache->attributes[1];
		MMVectorAttribute__ctor_mC708CBE51447649AC93A4B54B7480286ED8BBB52(tmp, _tmp_0, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_TargetCharacter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x74\x68\x69\x73\x20\x63\x61\x6D\x65\x72\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x73"), NULL);
	}
	{
		MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 * tmp = (MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 *)cache->attributes[1];
		MMReadOnlyAttribute__ctor_m542563E5945344E9F070E568F1073E7325B59374(tmp, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_TargetController(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72\x20\x62\x6F\x75\x6E\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x74\x68\x69\x73\x20\x63\x61\x6D\x65\x72\x61\x20\x66\x6F\x6C\x6C\x6F\x77\x73"), NULL);
	}
	{
		MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 * tmp = (MMReadOnlyAttribute_tEDA9655BEFD9353EA4076EEFB1ECF3583CDACB99 *)cache->attributes[1];
		MMReadOnlyAttribute__ctor_m542563E5945344E9F070E568F1073E7325B59374(tmp, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_UseOrthographicZoom(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x74\x65\x72\x6D\x69\x6E\x65\x20\x68\x65\x72\x65\x20\x74\x68\x65\x20\x6D\x69\x6E\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x7A\x6F\x6F\x6D\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x7A\x6F\x6F\x6D\x20\x73\x70\x65\x65\x64\x2E\x20\x42\x79\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x74\x68\x65\x20\x65\x6E\x67\x69\x6E\x65\x20\x77\x69\x6C\x6C\x20\x7A\x6F\x6F\x6D\x20\x6F\x75\x74\x20\x77\x68\x65\x6E\x20\x79\x6F\x75\x72\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x69\x73\x20\x67\x6F\x69\x6E\x67\x20\x61\x74\x20\x66\x75\x6C\x6C\x20\x73\x70\x65\x65\x64\x2C\x20\x61\x6E\x64\x20\x7A\x6F\x6F\x6D\x20\x69\x6E\x20\x77\x68\x65\x6E\x20\x79\x6F\x75\x20\x73\x6C\x6F\x77\x20\x64\x6F\x77\x6E\x20\x28\x6F\x72\x20\x73\x74\x6F\x70\x29\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x20\x5A\x6F\x6F\x6D"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x63\x61\x6D\x65\x72\x61\x20\x73\x68\x6F\x75\x6C\x64\x20\x7A\x6F\x6F\x6D\x20\x69\x6E\x20\x6F\x72\x20\x6F\x75\x74\x20\x61\x73\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x6D\x6F\x76\x65\x73"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_OrthographicZoom(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E"));
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x4D\x61\x78"));
		MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 * tmp = (MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 *)cache->attributes[0];
		MMVectorAttribute__ctor_mC708CBE51447649AC93A4B54B7480286ED8BBB52(tmp, _tmp_0, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6D\x69\x6E\x69\x6D\x75\x6D\x20\x26\x20\x6D\x61\x78\x69\x6D\x75\x6D\x20\x6F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x20\x63\x61\x6D\x65\x72\x61\x20\x7A\x6F\x6F\x6D"), NULL);
	}
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[2];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x4F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x5A\x6F\x6F\x6D"), true, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_InitialOrthographicZoom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x7A\x6F\x6F\x6D\x20\x76\x61\x6C\x75\x65\x20\x77\x68\x65\x6E\x20\x75\x73\x69\x6E\x67\x20\x61\x6E\x20\x6F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x20\x7A\x6F\x6F\x6D"), NULL);
	}
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[1];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x4F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x5A\x6F\x6F\x6D"), true, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_OrthographicZoomSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x70\x65\x65\x64\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x6F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x20\x63\x61\x6D\x65\x72\x61\x20\x7A\x6F\x6F\x6D\x73"), NULL);
	}
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[1];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x4F\x72\x74\x68\x6F\x67\x72\x61\x70\x68\x69\x63\x5A\x6F\x6F\x6D"), true, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_UsePerspectiveZoom(CustomAttributesCache* cache)
{
	{
		MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 * tmp = (MMInformationAttribute_t45CBB3B8D83BCDBA868AB47679D243F57EC51A45 *)cache->attributes[0];
		MMInformationAttribute__ctor_mA50CE3C7B05B97E9653171772C4CAD6CE2572588(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x74\x65\x72\x6D\x69\x6E\x65\x20\x68\x65\x72\x65\x20\x74\x68\x65\x20\x6D\x69\x6E\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x7A\x6F\x6F\x6D\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x7A\x6F\x6F\x6D\x20\x73\x70\x65\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x69\x73\x20\x69\x6E\x20\x70\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x20\x6D\x6F\x64\x65\x2E\x20\x59\x6F\x75\x20\x63\x61\x6E\x20\x70\x69\x63\x6B\x20\x74\x77\x6F\x20\x7A\x6F\x6F\x6D\x20\x6D\x65\x74\x68\x6F\x64\x73\x2C\x20\x65\x69\x74\x68\x65\x72\x20\x70\x6C\x61\x79\x69\x6E\x67\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x66\x69\x65\x6C\x64\x20\x6F\x66\x20\x76\x69\x65\x77\x20\x6F\x72\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x70\x6F\x73\x65\x72\x27\x73\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x2E"), 1LL, false, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x20\x5A\x6F\x6F\x6D"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x70\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x20\x7A\x6F\x6F\x6D\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x72\x6F\x63\x65\x73\x73\x65\x64\x20\x65\x76\x65\x72\x79\x20\x66\x72\x61\x6D\x65"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_PerspectiveZoomMethod(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x7A\x6F\x6F\x6D\x20\x6D\x65\x74\x68\x6F\x64\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[1];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x50\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x5A\x6F\x6F\x6D"), true, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_PerspectiveZoom(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x50\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x5A\x6F\x6F\x6D"), true, NULL);
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E"));
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x4D\x61\x78"));
		MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 * tmp = (MMVectorAttribute_tA5DBCF39823BEC9A2E2C24215522A4DC788ABBB3 *)cache->attributes[1];
		MMVectorAttribute__ctor_mC708CBE51447649AC93A4B54B7480286ED8BBB52(tmp, _tmp_0, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6D\x69\x6E\x20\x61\x6E\x64\x20\x6D\x61\x78\x20\x70\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x7A\x6F\x6F\x6D\x73"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_InitialPerspectiveZoom(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x50\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x5A\x6F\x6F\x6D"), true, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x7A\x6F\x6F\x6D\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x77\x68\x65\x6E\x20\x69\x6E\x20\x70\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_PerspectiveZoomSpeed(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x50\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x5A\x6F\x6F\x6D"), true, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x70\x65\x65\x64\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x70\x65\x72\x73\x70\x65\x63\x74\x69\x76\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x7A\x6F\x6F\x6D\x73"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_InstantRepositionCameraOnRespawn(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x70\x61\x77\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x77\x69\x6C\x6C\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x20\x74\x6F\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x27\x73\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x6F\x6E\x20\x72\x65\x73\x70\x61\x77\x6E\x2C\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x69\x74\x27\x6C\x6C\x20\x6D\x6F\x76\x65\x20\x74\x68\x65\x72\x65\x20\x61\x74\x20\x69\x74\x73\x20\x72\x65\x67\x75\x6C\x61\x72\x20\x73\x70\x65\x65\x64"), NULL);
	}
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[2];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, 10.0f, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_StartFollowingBtn(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x46\x6F\x6C\x6C\x6F\x77\x69\x6E\x67"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x62\x75\x67"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_StopFollowingBtn(CustomAttributesCache* cache)
{
	{
		MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 * tmp = (MMInspectorButtonAttribute_t200D1A0C049B127AF69BA4EC2B534FDD321CD898 *)cache->attributes[0];
		MMInspectorButtonAttribute__ctor_m65BFAFDD5A1CCC7A36C166C9D6E072CF68B5ACF6(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x6F\x70\x46\x6F\x6C\x6C\x6F\x77\x69\x6E\x67"), NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_CinemachineCameraController_get_FollowsPlayer_m062EA52273780A614BB2705A35A3852DF542C05A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4_CustomAttributesCacheGenerator_CorgiCinemachineZone_EnableCamera_m3F8A8F2E8050881F79E584578098B52A0CC87F22(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_0_0_0_var), NULL);
	}
}
static void CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4_CustomAttributesCacheGenerator_CorgiCinemachineZone_U3CU3En__0_m2B0BD1A14B71AD52EE8B2105DE29729579DE90A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2__ctor_m1D9DDDCC28E121A6A0C27F904A2A8F29F7E70CCD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_IDisposable_Dispose_m4AE158A18B8F6565EC11D412F07D3C9F47055B83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03610361267D7AFEEDBA116769EAA9B1C43FAB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_Reset_m30A091C8111BCAE444372D8EFF6E6C1674AEE9B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_get_Current_m0080986FA7BEECF8586B5083D26C228D4F7D6A8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider2D_tDDBF081328B83D21D0BA3B5036D77B32528BA722_0_0_0_var), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_VirtualCamera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x76\x69\x72\x74\x75\x61\x6C\x20\x63\x61\x6D\x65\x72\x61\x20\x61\x73\x73\x6F\x63\x69\x61\x74\x65\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x72\x6F\x6F\x6D"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_Confiner(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6E\x66\x69\x6E\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x69\x73\x20\x72\x6F\x6F\x6D\x2C\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x20\x74\x68\x65\x20\x76\x69\x72\x74\x75\x61\x6C\x20\x63\x61\x6D\x65\x72\x61\x2C\x20\x75\x73\x75\x61\x6C\x6C\x79\x20\x70\x6C\x61\x63\x65\x64\x20\x6F\x6E\x20\x61\x20\x63\x68\x69\x6C\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x52\x6F\x6F\x6D"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_CinemachineCameraConfiner(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x6F\x6E\x66\x69\x6E\x65\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x76\x69\x72\x74\x75\x61\x6C\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_ResizeConfinerAutomatically(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x65\x20\x63\x6F\x6E\x66\x69\x6E\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x61\x75\x74\x6F\x20\x72\x65\x73\x69\x7A\x65\x64\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x74\x6F\x20\x6D\x61\x74\x63\x68\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x27\x73\x20\x73\x69\x7A\x65\x20\x61\x6E\x64\x20\x72\x61\x74\x69\x6F"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_AutoDetectFirstRoomOnStart(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x69\x73\x20\x52\x6F\x6F\x6D\x20\x73\x68\x6F\x75\x6C\x64\x20\x6C\x6F\x6F\x6B\x20\x61\x74\x20\x74\x68\x65\x20\x6C\x65\x76\x65\x6C\x27\x73\x20\x73\x74\x61\x72\x74\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x61\x6E\x64\x20\x64\x65\x63\x6C\x61\x72\x65\x20\x69\x74\x73\x65\x6C\x66\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x72\x6F\x6F\x6D\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x20\x6F\x72\x20\x6E\x6F\x74"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_CurrentRoom(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x65"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x72\x6F\x6F\x6D\x20\x69\x73\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x72\x6F\x6F\x6D\x20\x6F\x72\x20\x6E\x6F\x74"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_RoomVisited(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x72\x6F\x6F\x6D\x20\x68\x61\x73\x20\x61\x6C\x72\x65\x61\x64\x79\x20\x62\x65\x65\x6E\x20\x76\x69\x73\x69\x74\x65\x64\x20\x6F\x72\x20\x6E\x6F\x74"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_OnPlayerEntersRoomForTheFirstTime(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x20\x74\x6F\x20\x74\x72\x69\x67\x67\x65\x72\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x65\x6E\x74\x65\x72\x73\x20\x74\x68\x65\x20\x72\x6F\x6F\x6D\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x66\x69\x72\x73\x74\x20\x74\x69\x6D\x65"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_OnPlayerEntersRoom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x20\x74\x6F\x20\x74\x72\x69\x67\x67\x65\x72\x20\x65\x76\x65\x72\x79\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x65\x6E\x74\x65\x72\x73\x20\x74\x68\x65\x20\x72\x6F\x6F\x6D"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_OnPlayerExitsRoom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x65\x76\x65\x6E\x74\x20\x74\x6F\x20\x74\x72\x69\x67\x67\x65\x72\x20\x65\x76\x65\x72\x79\x74\x69\x6D\x65\x20\x74\x68\x65\x20\x70\x6C\x61\x79\x65\x72\x20\x65\x78\x69\x74\x73\x20\x74\x68\x65\x20\x72\x6F\x6F\x6D"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_ActivationList(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x69\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x6C\x69\x73\x74\x20\x6F\x66\x20\x67\x61\x6D\x65\x6F\x62\x6A\x65\x63\x74\x73\x20\x74\x6F\x20\x65\x6E\x61\x62\x6C\x65\x20\x77\x68\x65\x6E\x20\x65\x6E\x74\x65\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x72\x6F\x6F\x6D\x2C\x20\x61\x6E\x64\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x77\x68\x65\x6E\x20\x65\x78\x69\x74\x69\x6E\x67\x20\x69\x74"), NULL);
	}
}
static void Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_Room_ResizeConfiner_mD64E0BE92ACDD5A2FE6F058A857016D3D5102D2F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_0_0_0_var), NULL);
	}
}
static void U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19__ctor_mFD9E4619E6D3E4ABD7AE146801D22E1C2F5F2A59(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_IDisposable_Dispose_m4962D021C26E0B0EE67F6CB3955EF04785AE9CE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FBA07F0972F7340AB48E3C344E3E589BB92DAF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_Reset_m62D2D3B24FC3BA71ED40F889D1B05FD0D02F4A05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_get_Current_m4526C7997761778F7232684D72638DA47D6CAC86(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x72\x67\x69\x20\x45\x6E\x67\x69\x6E\x65\x2F\x45\x6E\x76\x69\x72\x6F\x6E\x6D\x65\x6E\x74\x2F\x54\x65\x6C\x65\x70\x6F\x72\x74\x65\x72"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_OnlyAffectsPlayer(CustomAttributesCache* cache)
{
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[0];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6C\x65\x70\x6F\x72\x74\x65\x72"), true, 22LL, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x69\x73\x20\x77\x6F\x6E\x27\x74\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x20\x6E\x6F\x6E\x20\x70\x6C\x61\x79\x65\x72\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x73"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_ExitOffset(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6F\x66\x66\x73\x65\x74\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x77\x68\x65\x6E\x20\x65\x78\x69\x74\x69\x6E\x67\x20\x74\x68\x69\x73\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x72"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TeleportationMode(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x61\x74\x69\x6F\x6E\x20\x6D\x6F\x64\x65\x20"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TweenCurve(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* _tmp_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, 1);
		(_tmp_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), 1);
		MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 * tmp = (MMEnumConditionAttribute_t703F18E8647B941A0F7D253C06848456428C6183 *)cache->attributes[0];
		MMEnumConditionAttribute__ctor_mA81D882C4D2DBD8A25C969EAE7BB6846FD0AE3BE(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6C\x65\x70\x6F\x72\x74\x61\x74\x69\x6F\x6E\x4D\x6F\x64\x65"), _tmp_1, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x74\x6F\x20\x74\x68\x65\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x61\x74\x69\x6F\x6E\x20\x74\x77\x65\x65\x6E\x20"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MaintainXEntryPositionOnExit(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x6D\x61\x69\x6E\x74\x61\x69\x6E\x20\x74\x68\x65\x20\x78\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x6E\x20\x65\x78\x69\x74"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MaintainYEntryPositionOnExit(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x6F\x20\x6D\x61\x69\x6E\x74\x61\x69\x6E\x20\x74\x68\x65\x20\x79\x20\x76\x61\x6C\x75\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x6E\x20\x65\x78\x69\x74"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_Destination(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x72\x27\x73\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[1];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E"), true, 23LL, NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AddToDestinationIgnoreList(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x75\x74\x20\x6F\x6E\x20\x74\x68\x65\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x27\x73\x20\x69\x67\x6E\x6F\x72\x65\x20\x6C\x69\x73\x74\x2C\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x69\x6D\x6D\x65\x64\x69\x61\x74\x65\x20\x72\x65\x2D\x65\x6E\x74\x72\x79\x2E\x20\x49\x66\x20\x79\x6F\x75\x72\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x27\x73\x20\x6F\x66\x66\x73\x65\x74\x20\x69\x73\x20\x66\x61\x72\x20\x65\x6E\x6F\x75\x67\x68\x20\x66\x72\x6F\x6D\x20\x69\x74\x73\x20\x63\x65\x6E\x74\x65\x72\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x73\x65\x74\x20\x74\x68\x61\x74\x20\x74\x6F\x20\x66\x61\x6C\x73\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_CameraMode(CustomAttributesCache* cache)
{
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[0];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6F\x6D\x73"), true, 24LL, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x68\x6F\x73\x65\x6E\x20\x63\x61\x6D\x65\x72\x61\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_CurrentRoom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x72\x6F\x6F\x6D\x20\x74\x68\x69\x73\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x72\x20\x62\x65\x6C\x6F\x6E\x67\x73\x20\x74\x6F"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TargetRoom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x72\x6F\x6F\x6D"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TriggerFade(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x66\x20\x74\x68\x69\x73\x20\x69\x73\x20\x74\x72\x75\x65\x2C\x20\x61\x20\x66\x61\x64\x65\x20\x74\x6F\x20\x62\x6C\x61\x63\x6B\x20\x77\x69\x6C\x6C\x20\x6F\x63\x63\x75\x72\x20\x77\x68\x65\x6E\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x69\x6E\x67"), NULL);
	}
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[1];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x4D\x46\x61\x64\x65\x72\x20\x54\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E"), true, 25LL, NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FaderID(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72\x46\x61\x64\x65"), true, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x49\x44\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x61\x64\x65\x72\x20\x74\x6F\x20\x74\x61\x72\x67\x65\x74"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FadeTween(CustomAttributesCache* cache)
{
	{
		MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 * tmp = (MMConditionAttribute_tA83601343AE95A59F9820EC86A4C7B73ED893E16 *)cache->attributes[0];
		MMConditionAttribute__ctor_mFACFF7B2226C37003E25C96C47E8AFCB1E8D68DC(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x69\x67\x67\x65\x72\x46\x61\x64\x65"), true, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x75\x73\x65\x20\x74\x6F\x20\x66\x61\x64\x65\x20\x74\x6F\x20\x62\x6C\x61\x63\x6B"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMask(CustomAttributesCache* cache)
{
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[0];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x73\x6B"), true, 26LL, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x77\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x61\x73\x6B\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x61\x20\x4D\x4D\x53\x70\x72\x69\x74\x65\x4D\x61\x73\x6B\x20\x6F\x6E\x20\x61\x63\x74\x69\x76\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMaskCurve(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x63\x75\x72\x76\x65\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x68\x65\x20\x6D\x61\x73\x6B\x20\x61\x6C\x6F\x6E\x67\x20\x74\x6F"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMaskMethod(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6D\x65\x74\x68\x6F\x64\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x68\x65\x20\x6D\x61\x73\x6B"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMaskDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6D\x61\x73\x6B\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x28\x75\x73\x75\x61\x6C\x6C\x79\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x61\x73\x20\x74\x68\x65\x20\x44\x65\x6C\x61\x79\x42\x65\x74\x77\x65\x65\x6E\x46\x61\x64\x65\x73"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FreezeTime(CustomAttributesCache* cache)
{
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[0];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x46\x72\x65\x65\x7A\x65"), true, 27LL, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x69\x6D\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x66\x72\x6F\x7A\x65\x6E\x20\x64\x75\x72\x69\x6E\x67\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FreezeCharacter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x77\x68\x65\x74\x68\x65\x72\x20\x6F\x72\x20\x6E\x6F\x74\x20\x74\x68\x65\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x66\x72\x6F\x7A\x65\x6E\x20\x28\x69\x6E\x70\x75\x74\x20\x62\x6C\x6F\x63\x6B\x65\x64\x29\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_InitialDelay(CustomAttributesCache* cache)
{
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[0];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6C\x65\x70\x6F\x72\x74\x20\x53\x65\x71\x75\x65\x6E\x63\x65"), true, 28LL, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x65\x6C\x61\x79\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x62\x65\x66\x6F\x72\x65\x20\x72\x75\x6E\x6E\x69\x6E\x67\x20\x74\x68\x65\x20\x73\x65\x71\x75\x65\x6E\x63\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FadeOutDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x64\x65\x6C\x61\x79\x20\x63\x6F\x76\x65\x72\x69\x6E\x67\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x6F\x75\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_DelayBetweenFades(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x74\x6F\x20\x77\x61\x69\x74\x20\x66\x6F\x72\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x6F\x75\x74\x20\x61\x6E\x64\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x69\x6E"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FadeInDuration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x64\x65\x6C\x61\x79\x20\x63\x6F\x76\x65\x72\x69\x6E\x67\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x69\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FinalDelay(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x28\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x29\x20\x74\x6F\x20\x61\x70\x70\x6C\x79\x20\x61\x66\x74\x65\x72\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x69\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_SequenceStartFeedback(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x67\x65\x74\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x64\x65\x6C\x61\x79\x20\x73\x74\x61\x72\x74\x73"), NULL);
	}
	{
		MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E * tmp = (MMInspectorGroupAttribute_tBD72D83B933F36043FB44918653A50DE5F3E815E *)cache->attributes[1];
		MMInspectorGroupAttribute__ctor_mC1CA651667A06A82388B66735945F7F62CD8B167(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6C\x65\x70\x6F\x72\x74\x20\x53\x65\x71\x75\x65\x6E\x63\x65\x20\x46\x65\x65\x64\x62\x61\x63\x6B\x73"), true, 29LL, NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AfterInitialDelayFeedback(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x67\x65\x74\x20\x70\x6C\x61\x79\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x6F\x75\x74\x20\x64\x75\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AfterFadeOutFeedback(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x67\x65\x74\x20\x70\x6C\x61\x79\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x65\x6C\x61\x79\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x66\x61\x64\x65\x73"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AfterDelayBetweenFadesFeedback(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x67\x65\x74\x20\x70\x6C\x61\x79\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x61\x64\x65\x20\x69\x6E"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_SequenceEndFeedback(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x67\x65\x74\x20\x70\x6C\x61\x79\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x73\x74\x61\x72\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x69\x6E\x61\x6C\x20\x64\x65\x6C\x61\x79"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_ArrivedHereFeedback(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x74\x68\x61\x74\x20\x67\x65\x74\x73\x20\x70\x6C\x61\x79\x65\x64\x20\x6F\x6E\x20\x74\x68\x69\x73\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x72\x20\x77\x68\x65\x6E\x20\x61\x6E\x6F\x74\x68\x65\x72\x20\x74\x65\x6C\x65\x70\x6F\x72\x74\x65\x72\x20\x73\x65\x6E\x64\x73\x20\x61\x20\x63\x68\x61\x72\x61\x63\x74\x65\x72\x20\x68\x65\x72\x65"), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_Teleporter_TeleportSequence_mCB4AFE4F1F8114AD5DD94DAE787742ACC50148BB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_0_0_0_var), NULL);
	}
}
static void Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_Teleporter_TeleportTweenCo_mD805D9A909935BC3E4D7EAA71B0B7D9FE1905F13(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_0_0_0_var), NULL);
	}
}
static void U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47__ctor_m8AB83EFFF24D4A556C827B654ED7B8915B8DF816(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_IDisposable_Dispose_m810BF66E18978CB0ED0DF9AF0BA37584610CFF1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3DB96977E9170F1361CAC32DF7C6A7251C6BB0D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_Reset_m53A758DC63962FA755B19A04E75DF4A29BC4A292(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_get_Current_m75E54E7AB41FB6E6C76D783563F038E14CF86B75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52__ctor_m1644936594C6AC11FF6F80E342A05642C083BF68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_IDisposable_Dispose_m27BD8E4476BA2FE7469F6BE440B747D6F9758C46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CF0CB64F7ADDDEF90530D50755562A5EB7F418F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_Reset_mF9D98A4E2B68907316E1373F44042532309211F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_get_Current_m5EA2400320AFC0C2F6E5F002CA071D3221C83E9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_Cinemachine_AttributeGenerators[];
const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_Cinemachine_AttributeGenerators[104] = 
{
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator,
	CinemachineBrainController_tDB96E7E23705E98A4DD7B485B76B6D43FEE9EF00_CustomAttributesCacheGenerator,
	U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator,
	U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator,
	U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator,
	U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_LockXAxis,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_LockYAxis,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_LockZAxis,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_Method,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_ForcedPosition,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_TargetCollider,
	CinemachineAxisLocker_t64D52125143A2D463CBA7D9B26CABB36BF15DCEB_CustomAttributesCacheGenerator_TargetCollider2D,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_U3CFollowsPlayerU3Ek__BackingField,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_FollowsAPlayer,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_ConfineCameraToLevelBounds,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_ManualUpDownLookDistance,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_CharacterSpeed,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_TargetCharacter,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_TargetController,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_UseOrthographicZoom,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_OrthographicZoom,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_InitialOrthographicZoom,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_OrthographicZoomSpeed,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_UsePerspectiveZoom,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_PerspectiveZoomMethod,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_PerspectiveZoom,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_InitialPerspectiveZoom,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_PerspectiveZoomSpeed,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_InstantRepositionCameraOnRespawn,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_StartFollowingBtn,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_StopFollowingBtn,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_VirtualCamera,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_Confiner,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_CinemachineCameraConfiner,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_ResizeConfinerAutomatically,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_AutoDetectFirstRoomOnStart,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_CurrentRoom,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_RoomVisited,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_OnPlayerEntersRoomForTheFirstTime,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_OnPlayerEntersRoom,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_OnPlayerExitsRoom,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_ActivationList,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_OnlyAffectsPlayer,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_ExitOffset,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TeleportationMode,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TweenCurve,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MaintainXEntryPositionOnExit,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MaintainYEntryPositionOnExit,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_Destination,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AddToDestinationIgnoreList,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_CameraMode,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_CurrentRoom,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TargetRoom,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_TriggerFade,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FaderID,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FadeTween,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMask,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMaskCurve,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMaskMethod,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_MoveMaskDuration,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FreezeTime,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FreezeCharacter,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_InitialDelay,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FadeOutDuration,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_DelayBetweenFades,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FadeInDuration,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_FinalDelay,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_SequenceStartFeedback,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AfterInitialDelayFeedback,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AfterFadeOutFeedback,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_AfterDelayBetweenFadesFeedback,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_SequenceEndFeedback,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_ArrivedHereFeedback,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_CinemachineCameraController_get_FollowsPlayer_m062EA52273780A614BB2705A35A3852DF542C05A,
	CinemachineCameraController_tE4133BD835D7220D7B6CD5D12075D278C8079DD1_CustomAttributesCacheGenerator_CinemachineCameraController_set_FollowsPlayer_mFAEA4739F96A063637489D3FBF22260C7B89D1FD,
	CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4_CustomAttributesCacheGenerator_CorgiCinemachineZone_EnableCamera_m3F8A8F2E8050881F79E584578098B52A0CC87F22,
	CorgiCinemachineZone_t8558023D193CB06950AE52CCB2CF7C0CE2521ED4_CustomAttributesCacheGenerator_CorgiCinemachineZone_U3CU3En__0_m2B0BD1A14B71AD52EE8B2105DE29729579DE90A9,
	U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2__ctor_m1D9DDDCC28E121A6A0C27F904A2A8F29F7E70CCD,
	U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_IDisposable_Dispose_m4AE158A18B8F6565EC11D412F07D3C9F47055B83,
	U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03610361267D7AFEEDBA116769EAA9B1C43FAB4,
	U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_Reset_m30A091C8111BCAE444372D8EFF6E6C1674AEE9B5,
	U3CEnableCameraU3Ed__2_t4C3FA577D68BD261669636220153E6804A28DE9F_CustomAttributesCacheGenerator_U3CEnableCameraU3Ed__2_System_Collections_IEnumerator_get_Current_m0080986FA7BEECF8586B5083D26C228D4F7D6A8A,
	Room_t392D52D203A7F630C190D47A07148A57F4020B42_CustomAttributesCacheGenerator_Room_ResizeConfiner_mD64E0BE92ACDD5A2FE6F058A857016D3D5102D2F,
	U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19__ctor_mFD9E4619E6D3E4ABD7AE146801D22E1C2F5F2A59,
	U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_IDisposable_Dispose_m4962D021C26E0B0EE67F6CB3955EF04785AE9CE3,
	U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FBA07F0972F7340AB48E3C344E3E589BB92DAF3,
	U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_Reset_m62D2D3B24FC3BA71ED40F889D1B05FD0D02F4A05,
	U3CResizeConfinerU3Ed__19_tE89F6318A93841133F7F61F93329C6F5AA4635D3_CustomAttributesCacheGenerator_U3CResizeConfinerU3Ed__19_System_Collections_IEnumerator_get_Current_m4526C7997761778F7232684D72638DA47D6CAC86,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_Teleporter_TeleportSequence_mCB4AFE4F1F8114AD5DD94DAE787742ACC50148BB,
	Teleporter_tB4B442E1E4F5E41E305331A25E1995FA6E7EA870_CustomAttributesCacheGenerator_Teleporter_TeleportTweenCo_mD805D9A909935BC3E4D7EAA71B0B7D9FE1905F13,
	U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47__ctor_m8AB83EFFF24D4A556C827B654ED7B8915B8DF816,
	U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_IDisposable_Dispose_m810BF66E18978CB0ED0DF9AF0BA37584610CFF1E,
	U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC3DB96977E9170F1361CAC32DF7C6A7251C6BB0D,
	U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_Reset_m53A758DC63962FA755B19A04E75DF4A29BC4A292,
	U3CTeleportSequenceU3Ed__47_tEEA6399791912057667529FC0F4FAC771444ABC6_CustomAttributesCacheGenerator_U3CTeleportSequenceU3Ed__47_System_Collections_IEnumerator_get_Current_m75E54E7AB41FB6E6C76D783563F038E14CF86B75,
	U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52__ctor_m1644936594C6AC11FF6F80E342A05642C083BF68,
	U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_IDisposable_Dispose_m27BD8E4476BA2FE7469F6BE440B747D6F9758C46,
	U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8CF0CB64F7ADDDEF90530D50755562A5EB7F418F,
	U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_Reset_mF9D98A4E2B68907316E1373F44042532309211F6,
	U3CTeleportTweenCoU3Ed__52_t2C77EF643092361D86B8A29461ACF192C79C1A0B_CustomAttributesCacheGenerator_U3CTeleportTweenCoU3Ed__52_System_Collections_IEnumerator_get_Current_m5EA2400320AFC0C2F6E5F002CA071D3221C83E9A,
	MoreMountains_CorgiEngine_Cinemachine_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::get_FeedbackDuration()
extern void MMFeedbackBloom_get_FeedbackDuration_m9F634FB92CF18DE6E0ABCB9691C1B71FF7E1E0C8 (void);
// 0x00000002 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::set_FeedbackDuration(System.Single)
extern void MMFeedbackBloom_set_FeedbackDuration_m1DA3D9F8A614794C72335892C986027AEB91523A (void);
// 0x00000003 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackBloom_CustomPlayFeedback_m06D97480F2D6CBCAFBC0A68329B45E4063B34446 (void);
// 0x00000004 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackBloom_CustomStopFeedback_m6F5CF7DB5AA8E7179542DB37AF491D317D7E37FE (void);
// 0x00000005 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::.ctor()
extern void MMFeedbackBloom__ctor_m893917B5812AA8BD05FCD71C197D130D6DF35333 (void);
// 0x00000006 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::get_FeedbackDuration()
extern void MMFeedbackChromaticAberration_get_FeedbackDuration_mA9CF2B362AAC259B8B40B0DBBF362CF9B3D22466 (void);
// 0x00000007 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::set_FeedbackDuration(System.Single)
extern void MMFeedbackChromaticAberration_set_FeedbackDuration_mBD42A1C970C33EAF4AE2541088DCCF2FB3B001E5 (void);
// 0x00000008 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackChromaticAberration_CustomPlayFeedback_mDC0FA33616B2E58F7E49B8EC3599B8EB65275ECC (void);
// 0x00000009 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackChromaticAberration_CustomStopFeedback_mE6E41DFF28E36848B7BEC2D1C2E9575206096DDE (void);
// 0x0000000A System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::.ctor()
extern void MMFeedbackChromaticAberration__ctor_m6D993AA2741C2531AD6A63C740A2B5FEAE1CB799 (void);
// 0x0000000B System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::get_FeedbackDuration()
extern void MMFeedbackColorGrading_get_FeedbackDuration_m990844942C8A307F304B831DBFEEB29AFB1A4A8D (void);
// 0x0000000C System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::set_FeedbackDuration(System.Single)
extern void MMFeedbackColorGrading_set_FeedbackDuration_m381EA911DBB6B925F25CE45105769EAD829625DB (void);
// 0x0000000D System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackColorGrading_CustomPlayFeedback_m5B19EBBE04D72F12F0C8DFEF10BB7D4BC03D52D2 (void);
// 0x0000000E System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackColorGrading_CustomStopFeedback_mEED0EAF0BD8203C08A427D8E65BCB21A260D11C2 (void);
// 0x0000000F System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::.ctor()
extern void MMFeedbackColorGrading__ctor_m168385EE3B18AD7FC47DBB107BBE9E34ADCEEFB6 (void);
// 0x00000010 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::get_FeedbackDuration()
extern void MMFeedbackDepthOfField_get_FeedbackDuration_mFEF739B972CCDBCB852CABB1BE9B6A624D6D16F5 (void);
// 0x00000011 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::set_FeedbackDuration(System.Single)
extern void MMFeedbackDepthOfField_set_FeedbackDuration_m28DEB54B2CA8CEA69301063C53BC474BB7E761C5 (void);
// 0x00000012 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackDepthOfField_CustomPlayFeedback_m964964960B383FC9CE6D8A00545A3D34BFD691A3 (void);
// 0x00000013 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackDepthOfField_CustomStopFeedback_m056FB5F98585F46CD29F85502AF59290A53D9569 (void);
// 0x00000014 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::.ctor()
extern void MMFeedbackDepthOfField__ctor_mD9E25AD23C0D1EF14BCE5AC1BB7CEA7F13247AF2 (void);
// 0x00000015 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::get_FeedbackDuration()
extern void MMFeedbackGlobalPPVolumeAutoBlend_get_FeedbackDuration_m22F7C55A9F37A73B5A954618427319612AD7B259 (void);
// 0x00000016 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackGlobalPPVolumeAutoBlend_CustomPlayFeedback_m6D7CB85B59241B3E49A66D406068FFE7058E93BD (void);
// 0x00000017 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackGlobalPPVolumeAutoBlend_CustomStopFeedback_m8054F90E9A7019655A1BEAD48A52C7A09B6F9AD2 (void);
// 0x00000018 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::.ctor()
extern void MMFeedbackGlobalPPVolumeAutoBlend__ctor_m880BD008B6B8FA389221EA7328802D209C2F31D6 (void);
// 0x00000019 System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::get_FeedbackDuration()
extern void MMFeedbackLensDistortion_get_FeedbackDuration_m1BBD1E3C6EB2C204731618BAA7B80455D816ABE4 (void);
// 0x0000001A System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::set_FeedbackDuration(System.Single)
extern void MMFeedbackLensDistortion_set_FeedbackDuration_m19B09D64E30BF3C10C4BB633CFC131925716F09D (void);
// 0x0000001B System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLensDistortion_CustomPlayFeedback_mD62704646E4A16E40D92BCF4D1BA4A6AF6B8C433 (void);
// 0x0000001C System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackLensDistortion_CustomStopFeedback_mFECD9E021EBEBFD6F44176E9D9AEF00A89C320D6 (void);
// 0x0000001D System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::.ctor()
extern void MMFeedbackLensDistortion__ctor_m840EBA0C416822C558A5109CEDF47AADBF7C1B8B (void);
// 0x0000001E System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::get_FeedbackDuration()
extern void MMFeedbackVignette_get_FeedbackDuration_mEC5404D60CC1E906346734038EAE463FF0A5888C (void);
// 0x0000001F System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::set_FeedbackDuration(System.Single)
extern void MMFeedbackVignette_set_FeedbackDuration_m853FB36BD888634092DCEFCC94D967E090127606 (void);
// 0x00000020 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackVignette_CustomPlayFeedback_mD30CA31E4C3B4E7FDC0B91E495DF1EAEFCDA4306 (void);
// 0x00000021 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackVignette_CustomStopFeedback_m559407CD1535750F28EC07D1553947D023B8757D (void);
// 0x00000022 System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::.ctor()
extern void MMFeedbackVignette__ctor_mE0361C4036F149D29E49A6414E142B52A1563A0F (void);
// 0x00000023 System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::GetTime()
extern void MMGlobalPostProcessingVolumeAutoBlend_GetTime_mE5641A532632B5EB3708754A94C68306BEABE131 (void);
// 0x00000024 System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Awake()
extern void MMGlobalPostProcessingVolumeAutoBlend_Awake_mD3834A1F6F08F496BDFD987B632D68CA7CEF9EE1 (void);
// 0x00000025 System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::OnEnable()
extern void MMGlobalPostProcessingVolumeAutoBlend_OnEnable_mA5A6C44F00C1719B4E5F56D491E084B0FB843933 (void);
// 0x00000026 System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Blend()
extern void MMGlobalPostProcessingVolumeAutoBlend_Blend_m2B4F75A614EB9A4727FEA395CBFBCD71A76C3363 (void);
// 0x00000027 System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::BlendBack()
extern void MMGlobalPostProcessingVolumeAutoBlend_BlendBack_mCE347B8A9C52874236AC8E2336EB52C992C5C4D0 (void);
// 0x00000028 System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StartBlending()
extern void MMGlobalPostProcessingVolumeAutoBlend_StartBlending_m43672D0EEFCB5097DFC58946257CC865729EE21A (void);
// 0x00000029 System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StopBlending()
extern void MMGlobalPostProcessingVolumeAutoBlend_StopBlending_mD64D6573421A3F34AB85DEC7325781AA1AA40435 (void);
// 0x0000002A System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Update()
extern void MMGlobalPostProcessingVolumeAutoBlend_Update_mAB3103D2A3D74DED8868617A031A03EB50A004C5 (void);
// 0x0000002B System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::.ctor()
extern void MMGlobalPostProcessingVolumeAutoBlend__ctor_m9FE0953B52ECB231DE17B3CBF30078D6A2BB4DBD (void);
// 0x0000002C System.Void MoreMountains.FeedbacksForThirdParty.MMAutoFocus::Start()
extern void MMAutoFocus_Start_mFACF7CAB48B2D34B3239C8D101A9A1ED24DE0847 (void);
// 0x0000002D System.Void MoreMountains.FeedbacksForThirdParty.MMAutoFocus::Update()
extern void MMAutoFocus_Update_m9C977FB853A4C1F2835E9699736B6B9420BCA323 (void);
// 0x0000002E System.Void MoreMountains.FeedbacksForThirdParty.MMAutoFocus::.ctor()
extern void MMAutoFocus__ctor_m2A1704833FF8D28989A07E87FE9BA1B6E90AAC66 (void);
// 0x0000002F System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::Initialization()
extern void MMBloomShaker_Initialization_m5F9C3D84BDD679D84C9222AA129FCDE0A418CE1D (void);
// 0x00000030 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::Shake()
extern void MMBloomShaker_Shake_m6C796CD567B86ABA75F08343FEA32C8726BF2F41 (void);
// 0x00000031 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::GrabInitialValues()
extern void MMBloomShaker_GrabInitialValues_m312CE0BBDCC7DE6CF2EFA345960BC07ED7722CE7 (void);
// 0x00000032 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::OnBloomShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMBloomShaker_OnBloomShakeEvent_mF42753963ACFC55FCBC7A8C076F5ED2C66359A51 (void);
// 0x00000033 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::ResetTargetValues()
extern void MMBloomShaker_ResetTargetValues_m84B854B8DFBD5876B78701C558A4696AB312ACC4 (void);
// 0x00000034 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::ResetShakerValues()
extern void MMBloomShaker_ResetShakerValues_m93B752AF2C32CA70988BD87FEBBDC6C44FD3B878 (void);
// 0x00000035 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::StartListening()
extern void MMBloomShaker_StartListening_mF2BC5C839BD98E2D26B65FDBFA2DA378F81EB1F8 (void);
// 0x00000036 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::StopListening()
extern void MMBloomShaker_StopListening_m24FFEA7F65B5812FAAF1171B7E3EAC8D881D3B6E (void);
// 0x00000037 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::.ctor()
extern void MMBloomShaker__ctor_mB49BF5B24241D85D9F75A83677186448637CB92E (void);
// 0x00000038 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
extern void MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413 (void);
// 0x00000039 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
extern void MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465 (void);
// 0x0000003A System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
extern void MMBloomShakeEvent_Register_mB817392B689A69D225281AAA0ACDFEA1341E0914 (void);
// 0x0000003B System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
extern void MMBloomShakeEvent_Unregister_mA704D2598A9AEDABF75026B38A0540E05C79D868 (void);
// 0x0000003C System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMBloomShakeEvent_Trigger_m23D912AE020BF3DFE52136F7AC63264EC004D592 (void);
// 0x0000003D System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m8C6D6AA4764569D3426EEE34A5D435E30C3AD3B8 (void);
// 0x0000003E System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m3933FF1EAF3078E6969918306D580A60B3A809BD (void);
// 0x0000003F System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mF54F95B39682E08AE8BD0E4A85701B690B534CFE (void);
// 0x00000040 System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_mA38D6C5C060336B651DDBF6F6646EB0F33E1AF86 (void);
// 0x00000041 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::Initialization()
extern void MMChromaticAberrationShaker_Initialization_mC29C07BA5BDD10578E4215320989AE31D305BD02 (void);
// 0x00000042 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::Shake()
extern void MMChromaticAberrationShaker_Shake_m6C865F2AD8467CA1F81AAE4C0B1EFFECC9089C62 (void);
// 0x00000043 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::GrabInitialValues()
extern void MMChromaticAberrationShaker_GrabInitialValues_mCB4860BD757AE0B901FD634E2D03016FD437A4E9 (void);
// 0x00000044 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::OnMMChromaticAberrationShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMChromaticAberrationShaker_OnMMChromaticAberrationShakeEvent_m18F22A2670E1E4304B28AC53EA0FADBD0D6E2790 (void);
// 0x00000045 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::ResetTargetValues()
extern void MMChromaticAberrationShaker_ResetTargetValues_m2BDC9F2D6425D62F8C1221EAC2C5FC2BE8DA66FC (void);
// 0x00000046 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::ResetShakerValues()
extern void MMChromaticAberrationShaker_ResetShakerValues_mE0C95B6F0ED9B0A86A779A6AED1286170CE5145C (void);
// 0x00000047 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::StartListening()
extern void MMChromaticAberrationShaker_StartListening_m8B172F3BB6FA926093BB3980E95299291531C89B (void);
// 0x00000048 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::StopListening()
extern void MMChromaticAberrationShaker_StopListening_m43C5BAC155B46FE85E95732C28CF9629C82803FC (void);
// 0x00000049 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::.ctor()
extern void MMChromaticAberrationShaker__ctor_m25ADDA7CD7BEC48A695F7DE0C28A3021AAA51983 (void);
// 0x0000004A System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
extern void MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704 (void);
// 0x0000004B System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
extern void MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B (void);
// 0x0000004C System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
extern void MMChromaticAberrationShakeEvent_Register_m8D835B5379A8107ADDE5E64F7778D1B43DA3E2ED (void);
// 0x0000004D System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
extern void MMChromaticAberrationShakeEvent_Unregister_m63CA7DF579B51EA0BA149C2D09D53360F3367AF3 (void);
// 0x0000004E System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMChromaticAberrationShakeEvent_Trigger_m454DA08E74CCB18E4F40FC35B0F80C3270644EA5 (void);
// 0x0000004F System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m909A6ABE40C1CF17C2A53173B250D8C91A23495D (void);
// 0x00000050 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mECA4D50519B0FE3849D754AEC245B86CC475A4A0 (void);
// 0x00000051 System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m7A3D32292938BA78C59115F75BD58131FFAA82D4 (void);
// 0x00000052 System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m27AD3D9F84BE060F6CC7A2ADE2156671B1618472 (void);
// 0x00000053 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::Initialization()
extern void MMColorGradingShaker_Initialization_m94371A09517400534D8372AA021A67E4551115E7 (void);
// 0x00000054 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::Reset()
extern void MMColorGradingShaker_Reset_m27E6423275C138EFEE12041569966520DB35C7B7 (void);
// 0x00000055 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::Shake()
extern void MMColorGradingShaker_Shake_m9E1CBD8C217766D250F8607BC9121F3EFD911DF6 (void);
// 0x00000056 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::GrabInitialValues()
extern void MMColorGradingShaker_GrabInitialValues_m515090936C5165609704CDBDDB555F9FF31D6F27 (void);
// 0x00000057 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::OnMMColorGradingShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMColorGradingShaker_OnMMColorGradingShakeEvent_mD715EFFF4CFC86E7FF117A022D05975CA7AFC76A (void);
// 0x00000058 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ResetTargetValues()
extern void MMColorGradingShaker_ResetTargetValues_mA03B27DEBE6D72AE5FD8E5BDD606EC7696AB8ED0 (void);
// 0x00000059 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ResetShakerValues()
extern void MMColorGradingShaker_ResetShakerValues_mA7993047153E5050A018A3880866C6AE084AA348 (void);
// 0x0000005A System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::StartListening()
extern void MMColorGradingShaker_StartListening_m7DD3279C64EF4CB1E3BD44F21B8150922C44EC33 (void);
// 0x0000005B System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::StopListening()
extern void MMColorGradingShaker_StopListening_m71A1B540F66E721CAC97DF0B724288322A41F14D (void);
// 0x0000005C System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::.ctor()
extern void MMColorGradingShaker__ctor_m66FDCF22D6D5FC87F79225A6AF6FCC6C6FA49A9A (void);
// 0x0000005D System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
extern void MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653 (void);
// 0x0000005E System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
extern void MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24 (void);
// 0x0000005F System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
extern void MMColorGradingShakeEvent_Register_m0D3668E9A2FD2FDE7E425DBB5E932AE061B01CB1 (void);
// 0x00000060 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
extern void MMColorGradingShakeEvent_Unregister_m40AFF09B682A0DF86DB0CDE993F452453276E50D (void);
// 0x00000061 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMColorGradingShakeEvent_Trigger_mD517EE3BA035C260CE5D1F11AABD7B141A3AA7A1 (void);
// 0x00000062 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mAA8EA46ABCE74EEB9BB4EA63051A4923C9503903 (void);
// 0x00000063 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mB73ED89FD1DDEC3416C222C3CDA8F4E9ECCA9A47 (void);
// 0x00000064 System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m5E718F50FC0646F4B85B385FF307EA6661746E47 (void);
// 0x00000065 System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m119D754048E54FA56706DABB0D5D16E1D68C2E7D (void);
// 0x00000066 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::Initialization()
extern void MMDepthOfFieldShaker_Initialization_m5F358999FD2FBDAF0380D04BF1A14C4564E56322 (void);
// 0x00000067 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::Shake()
extern void MMDepthOfFieldShaker_Shake_m4103958B1D5123E8666E7CB327DF3BBB5C50503F (void);
// 0x00000068 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::Reset()
extern void MMDepthOfFieldShaker_Reset_mE0CA98B71F265623F84EE00D59414656664D0A38 (void);
// 0x00000069 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::GrabInitialValues()
extern void MMDepthOfFieldShaker_GrabInitialValues_m5CAB12298172802EA5B64686A91EF7A1C0793651 (void);
// 0x0000006A System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::OnDepthOfFieldShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMDepthOfFieldShaker_OnDepthOfFieldShakeEvent_m5C16E32C69542F08172F6F9845D508D5303C239B (void);
// 0x0000006B System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ResetTargetValues()
extern void MMDepthOfFieldShaker_ResetTargetValues_mF5778C81F4B849EAE780D6E6740F5233DFCDDD17 (void);
// 0x0000006C System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ResetShakerValues()
extern void MMDepthOfFieldShaker_ResetShakerValues_m7B76C6072FE77464215C7907C70DAC15896CEF8E (void);
// 0x0000006D System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::StartListening()
extern void MMDepthOfFieldShaker_StartListening_mB41FB46056BE61E90C5E3089A1A9B560D3E6F1BF (void);
// 0x0000006E System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::StopListening()
extern void MMDepthOfFieldShaker_StopListening_m83D3B4F4AD828B24461674C665FA016DEBF9D156 (void);
// 0x0000006F System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::.ctor()
extern void MMDepthOfFieldShaker__ctor_m2E8A4336941FD00B67EF6D883233D59978D32FC1 (void);
// 0x00000070 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
extern void MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553 (void);
// 0x00000071 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
extern void MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC (void);
// 0x00000072 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
extern void MMDepthOfFieldShakeEvent_Register_mADB298BBCDE1DCCA93507597413039A398C8B3FA (void);
// 0x00000073 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
extern void MMDepthOfFieldShakeEvent_Unregister_m3CAF5B2B4312D2FA80CF1B513C01573F1F15EBDB (void);
// 0x00000074 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMDepthOfFieldShakeEvent_Trigger_mEF87B2E979AAE6D96A885D4584372DD06A355790 (void);
// 0x00000075 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mD402128BE425C84FBC82232D1056363CFC3A76F7 (void);
// 0x00000076 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_mE072A8E7C58AFE376DCBD8AFAC20701518D86AED (void);
// 0x00000077 System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m2A8D5C241FB345616C72C0232EB41F9AF5BBD8F4 (void);
// 0x00000078 System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_mBD6EF50191821D9D5EB46E9A79260919E8E7A779 (void);
// 0x00000079 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::Initialization()
extern void MMLensDistortionShaker_Initialization_m3F2D70137D5A32BFC0036599991155A2B8E03F71 (void);
// 0x0000007A System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::Reset()
extern void MMLensDistortionShaker_Reset_m91A2648670EB98E2B5C2B3693315F27BDC8AAC6B (void);
// 0x0000007B System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::Shake()
extern void MMLensDistortionShaker_Shake_mE4AE6178E588A3628CDEDE41A48F7FF182FFDE35 (void);
// 0x0000007C System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::GrabInitialValues()
extern void MMLensDistortionShaker_GrabInitialValues_m8217D2424E69AA5D97D4AAC8680B114C8AA98230 (void);
// 0x0000007D System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::OnMMLensDistortionShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMLensDistortionShaker_OnMMLensDistortionShakeEvent_m8075A8A961BFD278296E52689D0304FBEF3C29F5 (void);
// 0x0000007E System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::ResetTargetValues()
extern void MMLensDistortionShaker_ResetTargetValues_mC22604AA6A91B8EC00B5EC4B77BE92BC4F55F5C1 (void);
// 0x0000007F System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::ResetShakerValues()
extern void MMLensDistortionShaker_ResetShakerValues_m3617D23C7C43C812DAD87DD1587F6746605C37E4 (void);
// 0x00000080 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::StartListening()
extern void MMLensDistortionShaker_StartListening_m2F22B87B5C8DB6E4D3952F7DE72661D3F0C45EA5 (void);
// 0x00000081 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::StopListening()
extern void MMLensDistortionShaker_StopListening_mEED6066F0DCA2FC1713BE34B6F9A542558248A0E (void);
// 0x00000082 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::.ctor()
extern void MMLensDistortionShaker__ctor_m5DF778FA2EF55456DFFF16300D6595DA5275344A (void);
// 0x00000083 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
extern void MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE (void);
// 0x00000084 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
extern void MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398 (void);
// 0x00000085 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
extern void MMLensDistortionShakeEvent_Register_m229BE3C83EF1126656363CB35AC2C2B577E33B1D (void);
// 0x00000086 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
extern void MMLensDistortionShakeEvent_Unregister_m1BD5E15B3D8C854DFE5B2603B65BA9423D543197 (void);
// 0x00000087 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMLensDistortionShakeEvent_Trigger_m1164932C876A6FE15F489A45F51C4E56BFAEA8B6 (void);
// 0x00000088 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_m6DD24C890F271356820106BCD3059A26CE2B05C5 (void);
// 0x00000089 System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m1297C5964A2FEF0A3AB0D42415275F9992AD18A1 (void);
// 0x0000008A System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_m8FD1DFF7AD9BA9D90AABD595B209BF01CC9E5CCB (void);
// 0x0000008B System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m3C760D0354ABB45A816916AABDF1D57B48DD9A1B (void);
// 0x0000008C System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::Initialization()
extern void MMVignetteShaker_Initialization_m71D7EF0DF87972186BA2EDB5929F3D7E9623376F (void);
// 0x0000008D System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::SetVignette(System.Single)
extern void MMVignetteShaker_SetVignette_m23161572032684FEEE32D330EBD7BDBF3455F505 (void);
// 0x0000008E System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::Shake()
extern void MMVignetteShaker_Shake_m1C4FEF73285B6D8A0F8FD6BECB2F3F4E1C149E2F (void);
// 0x0000008F System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::GrabInitialValues()
extern void MMVignetteShaker_GrabInitialValues_mE19E836C8042A6CDE21E85F5EB0CB8C4DEEBCDAB (void);
// 0x00000090 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::OnVignetteShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMVignetteShaker_OnVignetteShakeEvent_m08B6C9651953FCE804164A9C74AA96568D8AB2AA (void);
// 0x00000091 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::ResetTargetValues()
extern void MMVignetteShaker_ResetTargetValues_m0F8F6BCF1D3F08E86742486FC2C876B6DDC41502 (void);
// 0x00000092 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::ResetShakerValues()
extern void MMVignetteShaker_ResetShakerValues_m42FF67C335EE9AC14DA12E3B0948D21789F5EEC6 (void);
// 0x00000093 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::StartListening()
extern void MMVignetteShaker_StartListening_m7965906DC4243A36D24F3FE1BE331124E6206C52 (void);
// 0x00000094 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::StopListening()
extern void MMVignetteShaker_StopListening_m082550932C49B13099CC168F6AC01B2BFE6F5B4F (void);
// 0x00000095 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::.ctor()
extern void MMVignetteShaker__ctor_mAA731A262733B046A3B9E040519821CD7101ABA7 (void);
// 0x00000096 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
extern void MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290 (void);
// 0x00000097 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
extern void MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563 (void);
// 0x00000098 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
extern void MMVignetteShakeEvent_Register_m5D4664B4F351C71FA9CADC2A1026835F614EB11E (void);
// 0x00000099 System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
extern void MMVignetteShakeEvent_Unregister_m96CD18AB24B6DAE4FF1DD38064621ABBA6B6E974 (void);
// 0x0000009A System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void MMVignetteShakeEvent_Trigger_m6BE5A124916B56590046731459A95210B469BC3B (void);
// 0x0000009B System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
extern void Delegate__ctor_mB4D10F50ECD2FE9F53574366D7BD1C9BF9635F9E (void);
// 0x0000009C System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
extern void Delegate_Invoke_m7C0F4E59C8EBF4711224DFB293B92B7E9BC36D84 (void);
// 0x0000009D System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
extern void Delegate_BeginInvoke_mF4EAB67DF64F58FC3E626789A2A57AC1AC0412F4 (void);
// 0x0000009E System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
extern void Delegate_EndInvoke_m9DDF25A4ABE96DDC818EC98F83286D9C911EE87B (void);
static Il2CppMethodPointer s_methodPointers[158] = 
{
	MMFeedbackBloom_get_FeedbackDuration_m9F634FB92CF18DE6E0ABCB9691C1B71FF7E1E0C8,
	MMFeedbackBloom_set_FeedbackDuration_m1DA3D9F8A614794C72335892C986027AEB91523A,
	MMFeedbackBloom_CustomPlayFeedback_m06D97480F2D6CBCAFBC0A68329B45E4063B34446,
	MMFeedbackBloom_CustomStopFeedback_m6F5CF7DB5AA8E7179542DB37AF491D317D7E37FE,
	MMFeedbackBloom__ctor_m893917B5812AA8BD05FCD71C197D130D6DF35333,
	MMFeedbackChromaticAberration_get_FeedbackDuration_mA9CF2B362AAC259B8B40B0DBBF362CF9B3D22466,
	MMFeedbackChromaticAberration_set_FeedbackDuration_mBD42A1C970C33EAF4AE2541088DCCF2FB3B001E5,
	MMFeedbackChromaticAberration_CustomPlayFeedback_mDC0FA33616B2E58F7E49B8EC3599B8EB65275ECC,
	MMFeedbackChromaticAberration_CustomStopFeedback_mE6E41DFF28E36848B7BEC2D1C2E9575206096DDE,
	MMFeedbackChromaticAberration__ctor_m6D993AA2741C2531AD6A63C740A2B5FEAE1CB799,
	MMFeedbackColorGrading_get_FeedbackDuration_m990844942C8A307F304B831DBFEEB29AFB1A4A8D,
	MMFeedbackColorGrading_set_FeedbackDuration_m381EA911DBB6B925F25CE45105769EAD829625DB,
	MMFeedbackColorGrading_CustomPlayFeedback_m5B19EBBE04D72F12F0C8DFEF10BB7D4BC03D52D2,
	MMFeedbackColorGrading_CustomStopFeedback_mEED0EAF0BD8203C08A427D8E65BCB21A260D11C2,
	MMFeedbackColorGrading__ctor_m168385EE3B18AD7FC47DBB107BBE9E34ADCEEFB6,
	MMFeedbackDepthOfField_get_FeedbackDuration_mFEF739B972CCDBCB852CABB1BE9B6A624D6D16F5,
	MMFeedbackDepthOfField_set_FeedbackDuration_m28DEB54B2CA8CEA69301063C53BC474BB7E761C5,
	MMFeedbackDepthOfField_CustomPlayFeedback_m964964960B383FC9CE6D8A00545A3D34BFD691A3,
	MMFeedbackDepthOfField_CustomStopFeedback_m056FB5F98585F46CD29F85502AF59290A53D9569,
	MMFeedbackDepthOfField__ctor_mD9E25AD23C0D1EF14BCE5AC1BB7CEA7F13247AF2,
	MMFeedbackGlobalPPVolumeAutoBlend_get_FeedbackDuration_m22F7C55A9F37A73B5A954618427319612AD7B259,
	MMFeedbackGlobalPPVolumeAutoBlend_CustomPlayFeedback_m6D7CB85B59241B3E49A66D406068FFE7058E93BD,
	MMFeedbackGlobalPPVolumeAutoBlend_CustomStopFeedback_m8054F90E9A7019655A1BEAD48A52C7A09B6F9AD2,
	MMFeedbackGlobalPPVolumeAutoBlend__ctor_m880BD008B6B8FA389221EA7328802D209C2F31D6,
	MMFeedbackLensDistortion_get_FeedbackDuration_m1BBD1E3C6EB2C204731618BAA7B80455D816ABE4,
	MMFeedbackLensDistortion_set_FeedbackDuration_m19B09D64E30BF3C10C4BB633CFC131925716F09D,
	MMFeedbackLensDistortion_CustomPlayFeedback_mD62704646E4A16E40D92BCF4D1BA4A6AF6B8C433,
	MMFeedbackLensDistortion_CustomStopFeedback_mFECD9E021EBEBFD6F44176E9D9AEF00A89C320D6,
	MMFeedbackLensDistortion__ctor_m840EBA0C416822C558A5109CEDF47AADBF7C1B8B,
	MMFeedbackVignette_get_FeedbackDuration_mEC5404D60CC1E906346734038EAE463FF0A5888C,
	MMFeedbackVignette_set_FeedbackDuration_m853FB36BD888634092DCEFCC94D967E090127606,
	MMFeedbackVignette_CustomPlayFeedback_mD30CA31E4C3B4E7FDC0B91E495DF1EAEFCDA4306,
	MMFeedbackVignette_CustomStopFeedback_m559407CD1535750F28EC07D1553947D023B8757D,
	MMFeedbackVignette__ctor_mE0361C4036F149D29E49A6414E142B52A1563A0F,
	MMGlobalPostProcessingVolumeAutoBlend_GetTime_mE5641A532632B5EB3708754A94C68306BEABE131,
	MMGlobalPostProcessingVolumeAutoBlend_Awake_mD3834A1F6F08F496BDFD987B632D68CA7CEF9EE1,
	MMGlobalPostProcessingVolumeAutoBlend_OnEnable_mA5A6C44F00C1719B4E5F56D491E084B0FB843933,
	MMGlobalPostProcessingVolumeAutoBlend_Blend_m2B4F75A614EB9A4727FEA395CBFBCD71A76C3363,
	MMGlobalPostProcessingVolumeAutoBlend_BlendBack_mCE347B8A9C52874236AC8E2336EB52C992C5C4D0,
	MMGlobalPostProcessingVolumeAutoBlend_StartBlending_m43672D0EEFCB5097DFC58946257CC865729EE21A,
	MMGlobalPostProcessingVolumeAutoBlend_StopBlending_mD64D6573421A3F34AB85DEC7325781AA1AA40435,
	MMGlobalPostProcessingVolumeAutoBlend_Update_mAB3103D2A3D74DED8868617A031A03EB50A004C5,
	MMGlobalPostProcessingVolumeAutoBlend__ctor_m9FE0953B52ECB231DE17B3CBF30078D6A2BB4DBD,
	MMAutoFocus_Start_mFACF7CAB48B2D34B3239C8D101A9A1ED24DE0847,
	MMAutoFocus_Update_m9C977FB853A4C1F2835E9699736B6B9420BCA323,
	MMAutoFocus__ctor_m2A1704833FF8D28989A07E87FE9BA1B6E90AAC66,
	MMBloomShaker_Initialization_m5F9C3D84BDD679D84C9222AA129FCDE0A418CE1D,
	MMBloomShaker_Shake_m6C796CD567B86ABA75F08343FEA32C8726BF2F41,
	MMBloomShaker_GrabInitialValues_m312CE0BBDCC7DE6CF2EFA345960BC07ED7722CE7,
	MMBloomShaker_OnBloomShakeEvent_mF42753963ACFC55FCBC7A8C076F5ED2C66359A51,
	MMBloomShaker_ResetTargetValues_m84B854B8DFBD5876B78701C558A4696AB312ACC4,
	MMBloomShaker_ResetShakerValues_m93B752AF2C32CA70988BD87FEBBDC6C44FD3B878,
	MMBloomShaker_StartListening_mF2BC5C839BD98E2D26B65FDBFA2DA378F81EB1F8,
	MMBloomShaker_StopListening_m24FFEA7F65B5812FAAF1171B7E3EAC8D881D3B6E,
	MMBloomShaker__ctor_mB49BF5B24241D85D9F75A83677186448637CB92E,
	MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413,
	MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465,
	MMBloomShakeEvent_Register_mB817392B689A69D225281AAA0ACDFEA1341E0914,
	MMBloomShakeEvent_Unregister_mA704D2598A9AEDABF75026B38A0540E05C79D868,
	MMBloomShakeEvent_Trigger_m23D912AE020BF3DFE52136F7AC63264EC004D592,
	Delegate__ctor_m8C6D6AA4764569D3426EEE34A5D435E30C3AD3B8,
	Delegate_Invoke_m3933FF1EAF3078E6969918306D580A60B3A809BD,
	Delegate_BeginInvoke_mF54F95B39682E08AE8BD0E4A85701B690B534CFE,
	Delegate_EndInvoke_mA38D6C5C060336B651DDBF6F6646EB0F33E1AF86,
	MMChromaticAberrationShaker_Initialization_mC29C07BA5BDD10578E4215320989AE31D305BD02,
	MMChromaticAberrationShaker_Shake_m6C865F2AD8467CA1F81AAE4C0B1EFFECC9089C62,
	MMChromaticAberrationShaker_GrabInitialValues_mCB4860BD757AE0B901FD634E2D03016FD437A4E9,
	MMChromaticAberrationShaker_OnMMChromaticAberrationShakeEvent_m18F22A2670E1E4304B28AC53EA0FADBD0D6E2790,
	MMChromaticAberrationShaker_ResetTargetValues_m2BDC9F2D6425D62F8C1221EAC2C5FC2BE8DA66FC,
	MMChromaticAberrationShaker_ResetShakerValues_mE0C95B6F0ED9B0A86A779A6AED1286170CE5145C,
	MMChromaticAberrationShaker_StartListening_m8B172F3BB6FA926093BB3980E95299291531C89B,
	MMChromaticAberrationShaker_StopListening_m43C5BAC155B46FE85E95732C28CF9629C82803FC,
	MMChromaticAberrationShaker__ctor_m25ADDA7CD7BEC48A695F7DE0C28A3021AAA51983,
	MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704,
	MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B,
	MMChromaticAberrationShakeEvent_Register_m8D835B5379A8107ADDE5E64F7778D1B43DA3E2ED,
	MMChromaticAberrationShakeEvent_Unregister_m63CA7DF579B51EA0BA149C2D09D53360F3367AF3,
	MMChromaticAberrationShakeEvent_Trigger_m454DA08E74CCB18E4F40FC35B0F80C3270644EA5,
	Delegate__ctor_m909A6ABE40C1CF17C2A53173B250D8C91A23495D,
	Delegate_Invoke_mECA4D50519B0FE3849D754AEC245B86CC475A4A0,
	Delegate_BeginInvoke_m7A3D32292938BA78C59115F75BD58131FFAA82D4,
	Delegate_EndInvoke_m27AD3D9F84BE060F6CC7A2ADE2156671B1618472,
	MMColorGradingShaker_Initialization_m94371A09517400534D8372AA021A67E4551115E7,
	MMColorGradingShaker_Reset_m27E6423275C138EFEE12041569966520DB35C7B7,
	MMColorGradingShaker_Shake_m9E1CBD8C217766D250F8607BC9121F3EFD911DF6,
	MMColorGradingShaker_GrabInitialValues_m515090936C5165609704CDBDDB555F9FF31D6F27,
	MMColorGradingShaker_OnMMColorGradingShakeEvent_mD715EFFF4CFC86E7FF117A022D05975CA7AFC76A,
	MMColorGradingShaker_ResetTargetValues_mA03B27DEBE6D72AE5FD8E5BDD606EC7696AB8ED0,
	MMColorGradingShaker_ResetShakerValues_mA7993047153E5050A018A3880866C6AE084AA348,
	MMColorGradingShaker_StartListening_m7DD3279C64EF4CB1E3BD44F21B8150922C44EC33,
	MMColorGradingShaker_StopListening_m71A1B540F66E721CAC97DF0B724288322A41F14D,
	MMColorGradingShaker__ctor_m66FDCF22D6D5FC87F79225A6AF6FCC6C6FA49A9A,
	MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653,
	MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24,
	MMColorGradingShakeEvent_Register_m0D3668E9A2FD2FDE7E425DBB5E932AE061B01CB1,
	MMColorGradingShakeEvent_Unregister_m40AFF09B682A0DF86DB0CDE993F452453276E50D,
	MMColorGradingShakeEvent_Trigger_mD517EE3BA035C260CE5D1F11AABD7B141A3AA7A1,
	Delegate__ctor_mAA8EA46ABCE74EEB9BB4EA63051A4923C9503903,
	Delegate_Invoke_mB73ED89FD1DDEC3416C222C3CDA8F4E9ECCA9A47,
	Delegate_BeginInvoke_m5E718F50FC0646F4B85B385FF307EA6661746E47,
	Delegate_EndInvoke_m119D754048E54FA56706DABB0D5D16E1D68C2E7D,
	MMDepthOfFieldShaker_Initialization_m5F358999FD2FBDAF0380D04BF1A14C4564E56322,
	MMDepthOfFieldShaker_Shake_m4103958B1D5123E8666E7CB327DF3BBB5C50503F,
	MMDepthOfFieldShaker_Reset_mE0CA98B71F265623F84EE00D59414656664D0A38,
	MMDepthOfFieldShaker_GrabInitialValues_m5CAB12298172802EA5B64686A91EF7A1C0793651,
	MMDepthOfFieldShaker_OnDepthOfFieldShakeEvent_m5C16E32C69542F08172F6F9845D508D5303C239B,
	MMDepthOfFieldShaker_ResetTargetValues_mF5778C81F4B849EAE780D6E6740F5233DFCDDD17,
	MMDepthOfFieldShaker_ResetShakerValues_m7B76C6072FE77464215C7907C70DAC15896CEF8E,
	MMDepthOfFieldShaker_StartListening_mB41FB46056BE61E90C5E3089A1A9B560D3E6F1BF,
	MMDepthOfFieldShaker_StopListening_m83D3B4F4AD828B24461674C665FA016DEBF9D156,
	MMDepthOfFieldShaker__ctor_m2E8A4336941FD00B67EF6D883233D59978D32FC1,
	MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553,
	MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC,
	MMDepthOfFieldShakeEvent_Register_mADB298BBCDE1DCCA93507597413039A398C8B3FA,
	MMDepthOfFieldShakeEvent_Unregister_m3CAF5B2B4312D2FA80CF1B513C01573F1F15EBDB,
	MMDepthOfFieldShakeEvent_Trigger_mEF87B2E979AAE6D96A885D4584372DD06A355790,
	Delegate__ctor_mD402128BE425C84FBC82232D1056363CFC3A76F7,
	Delegate_Invoke_mE072A8E7C58AFE376DCBD8AFAC20701518D86AED,
	Delegate_BeginInvoke_m2A8D5C241FB345616C72C0232EB41F9AF5BBD8F4,
	Delegate_EndInvoke_mBD6EF50191821D9D5EB46E9A79260919E8E7A779,
	MMLensDistortionShaker_Initialization_m3F2D70137D5A32BFC0036599991155A2B8E03F71,
	MMLensDistortionShaker_Reset_m91A2648670EB98E2B5C2B3693315F27BDC8AAC6B,
	MMLensDistortionShaker_Shake_mE4AE6178E588A3628CDEDE41A48F7FF182FFDE35,
	MMLensDistortionShaker_GrabInitialValues_m8217D2424E69AA5D97D4AAC8680B114C8AA98230,
	MMLensDistortionShaker_OnMMLensDistortionShakeEvent_m8075A8A961BFD278296E52689D0304FBEF3C29F5,
	MMLensDistortionShaker_ResetTargetValues_mC22604AA6A91B8EC00B5EC4B77BE92BC4F55F5C1,
	MMLensDistortionShaker_ResetShakerValues_m3617D23C7C43C812DAD87DD1587F6746605C37E4,
	MMLensDistortionShaker_StartListening_m2F22B87B5C8DB6E4D3952F7DE72661D3F0C45EA5,
	MMLensDistortionShaker_StopListening_mEED6066F0DCA2FC1713BE34B6F9A542558248A0E,
	MMLensDistortionShaker__ctor_m5DF778FA2EF55456DFFF16300D6595DA5275344A,
	MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE,
	MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398,
	MMLensDistortionShakeEvent_Register_m229BE3C83EF1126656363CB35AC2C2B577E33B1D,
	MMLensDistortionShakeEvent_Unregister_m1BD5E15B3D8C854DFE5B2603B65BA9423D543197,
	MMLensDistortionShakeEvent_Trigger_m1164932C876A6FE15F489A45F51C4E56BFAEA8B6,
	Delegate__ctor_m6DD24C890F271356820106BCD3059A26CE2B05C5,
	Delegate_Invoke_m1297C5964A2FEF0A3AB0D42415275F9992AD18A1,
	Delegate_BeginInvoke_m8FD1DFF7AD9BA9D90AABD595B209BF01CC9E5CCB,
	Delegate_EndInvoke_m3C760D0354ABB45A816916AABDF1D57B48DD9A1B,
	MMVignetteShaker_Initialization_m71D7EF0DF87972186BA2EDB5929F3D7E9623376F,
	MMVignetteShaker_SetVignette_m23161572032684FEEE32D330EBD7BDBF3455F505,
	MMVignetteShaker_Shake_m1C4FEF73285B6D8A0F8FD6BECB2F3F4E1C149E2F,
	MMVignetteShaker_GrabInitialValues_mE19E836C8042A6CDE21E85F5EB0CB8C4DEEBCDAB,
	MMVignetteShaker_OnVignetteShakeEvent_m08B6C9651953FCE804164A9C74AA96568D8AB2AA,
	MMVignetteShaker_ResetTargetValues_m0F8F6BCF1D3F08E86742486FC2C876B6DDC41502,
	MMVignetteShaker_ResetShakerValues_m42FF67C335EE9AC14DA12E3B0948D21789F5EEC6,
	MMVignetteShaker_StartListening_m7965906DC4243A36D24F3FE1BE331124E6206C52,
	MMVignetteShaker_StopListening_m082550932C49B13099CC168F6AC01B2BFE6F5B4F,
	MMVignetteShaker__ctor_mAA731A262733B046A3B9E040519821CD7101ABA7,
	MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290,
	MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563,
	MMVignetteShakeEvent_Register_m5D4664B4F351C71FA9CADC2A1026835F614EB11E,
	MMVignetteShakeEvent_Unregister_m96CD18AB24B6DAE4FF1DD38064621ABBA6B6E974,
	MMVignetteShakeEvent_Trigger_m6BE5A124916B56590046731459A95210B469BC3B,
	Delegate__ctor_mB4D10F50ECD2FE9F53574366D7BD1C9BF9635F9E,
	Delegate_Invoke_m7C0F4E59C8EBF4711224DFB293B92B7E9BC36D84,
	Delegate_BeginInvoke_mF4EAB67DF64F58FC3E626789A2A57AC1AC0412F4,
	Delegate_EndInvoke_m9DDF25A4ABE96DDC818EC98F83286D9C911EE87B,
};
static const int32_t s_InvokerIndices[158] = 
{
	3476,
	2839,
	1743,
	1743,
	3506,
	3476,
	2839,
	1743,
	1743,
	3506,
	3476,
	2839,
	1743,
	1743,
	3506,
	3476,
	2839,
	1743,
	1743,
	3506,
	3476,
	1743,
	1743,
	3506,
	3476,
	2839,
	1743,
	1743,
	3506,
	3476,
	2839,
	1743,
	1743,
	3506,
	3476,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	12,
	3506,
	3506,
	3506,
	3506,
	3506,
	5339,
	5339,
	5339,
	5339,
	3608,
	1692,
	12,
	11,
	2815,
	3506,
	3506,
	3506,
	20,
	3506,
	3506,
	3506,
	3506,
	3506,
	5339,
	5339,
	5339,
	5339,
	3613,
	1692,
	20,
	13,
	2815,
	3506,
	3506,
	3506,
	3506,
	6,
	3506,
	3506,
	3506,
	3506,
	3506,
	5339,
	5339,
	5339,
	5339,
	3605,
	1692,
	6,
	2,
	2815,
	3506,
	3506,
	3506,
	3506,
	10,
	3506,
	3506,
	3506,
	3506,
	3506,
	5339,
	5339,
	5339,
	5339,
	3607,
	1692,
	10,
	7,
	2815,
	3506,
	3506,
	3506,
	3506,
	20,
	3506,
	3506,
	3506,
	3506,
	3506,
	5339,
	5339,
	5339,
	5339,
	3613,
	1692,
	20,
	13,
	2815,
	3506,
	2839,
	3506,
	3506,
	20,
	3506,
	3506,
	3506,
	3506,
	3506,
	5339,
	5339,
	5339,
	5339,
	3613,
	1692,
	20,
	13,
	2815,
};
extern const CustomAttributesCacheGenerator g_MoreMountains_Feedbacks_PostProcessing_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_Feedbacks_PostProcessing_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_Feedbacks_PostProcessing_CodeGenModule = 
{
	"MoreMountains.Feedbacks.PostProcessing.dll",
	158,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_MoreMountains_Feedbacks_PostProcessing_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

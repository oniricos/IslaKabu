﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MoreMountains.Tools.MMDoubleSpriteMask::Awake()
extern void MMDoubleSpriteMask_Awake_mD22C11996A351F9A0DB3718EAE5DBE229BFD6F96 (void);
// 0x00000002 System.Void MoreMountains.Tools.MMDoubleSpriteMask::SwitchCurrentMask()
extern void MMDoubleSpriteMask_SwitchCurrentMask_m7A6D3FBA62EA1E25178EA7D2FBA586768C31B9CB (void);
// 0x00000003 System.Collections.IEnumerator MoreMountains.Tools.MMDoubleSpriteMask::DoubleMaskCo(MoreMountains.Tools.MMSpriteMaskEvent)
extern void MMDoubleSpriteMask_DoubleMaskCo_mC730AF3628EAB697AF057F0B56064B86800491D8 (void);
// 0x00000004 System.Void MoreMountains.Tools.MMDoubleSpriteMask::OnMMEvent(MoreMountains.Tools.MMSpriteMaskEvent)
extern void MMDoubleSpriteMask_OnMMEvent_m923A0911227D7C433C67045AF5A4B6F3BF008B50 (void);
// 0x00000005 System.Void MoreMountains.Tools.MMDoubleSpriteMask::OnEnable()
extern void MMDoubleSpriteMask_OnEnable_m4D635305E9F1D52F5724B9BDD0FCDF28009F189F (void);
// 0x00000006 System.Void MoreMountains.Tools.MMDoubleSpriteMask::OnDisable()
extern void MMDoubleSpriteMask_OnDisable_mCBB967A194ECF0113C698F60C506C480F682DA38 (void);
// 0x00000007 System.Void MoreMountains.Tools.MMDoubleSpriteMask::.ctor()
extern void MMDoubleSpriteMask__ctor_mF718ADC7B7DC9C339608F902CC43E31862E31F53 (void);
// 0x00000008 System.Void MoreMountains.Tools.MMDoubleSpriteMask/<DoubleMaskCo>d__6::.ctor(System.Int32)
extern void U3CDoubleMaskCoU3Ed__6__ctor_m0DA458C1C8B07F4A74E543E395E7D94C2CF30C22 (void);
// 0x00000009 System.Void MoreMountains.Tools.MMDoubleSpriteMask/<DoubleMaskCo>d__6::System.IDisposable.Dispose()
extern void U3CDoubleMaskCoU3Ed__6_System_IDisposable_Dispose_mA2A205131EED731FFDF0DA621506360B0C58897E (void);
// 0x0000000A System.Boolean MoreMountains.Tools.MMDoubleSpriteMask/<DoubleMaskCo>d__6::MoveNext()
extern void U3CDoubleMaskCoU3Ed__6_MoveNext_m3375B37EFA2401720AC149FDA8EA65BC4CB6F3A6 (void);
// 0x0000000B System.Object MoreMountains.Tools.MMDoubleSpriteMask/<DoubleMaskCo>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDoubleMaskCoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FAFAE69BF7CDF9E5AF1BDC4DAABC2E6E7C05629 (void);
// 0x0000000C System.Void MoreMountains.Tools.MMDoubleSpriteMask/<DoubleMaskCo>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDoubleMaskCoU3Ed__6_System_Collections_IEnumerator_Reset_m78596805EF31746D05F8125B2EBD9D7A89651C4B (void);
// 0x0000000D System.Object MoreMountains.Tools.MMDoubleSpriteMask/<DoubleMaskCo>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDoubleMaskCoU3Ed__6_System_Collections_IEnumerator_get_Current_m86FE21B5D96DD0A60A91C815BED7CBF6FFAACAF6 (void);
// 0x0000000E System.Void MoreMountains.Tools.MMSpriteMaskEvent::.ctor(MoreMountains.Tools.MMSpriteMaskEvent/MMSpriteMaskEventTypes,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
extern void MMSpriteMaskEvent__ctor_m6C3949E81BA53ADBF7BC370886DDFAAEFC6C40B7 (void);
// 0x0000000F System.Void MoreMountains.Tools.MMSpriteMaskEvent::Trigger(MoreMountains.Tools.MMSpriteMaskEvent/MMSpriteMaskEventTypes,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
extern void MMSpriteMaskEvent_Trigger_m94383E73C7E743D4ED07BFBB86ABE48F62A9833D (void);
// 0x00000010 System.Single MoreMountains.Tools.MMSpriteMask::get_MaskTime()
extern void MMSpriteMask_get_MaskTime_m7079335367802861AB675DF2613E9FC2058120F9 (void);
// 0x00000011 System.Void MoreMountains.Tools.MMSpriteMask::Awake()
extern void MMSpriteMask_Awake_m4E5B8EA4BCA4C67D0772B91F21CA689296BFBEDD (void);
// 0x00000012 System.Void MoreMountains.Tools.MMSpriteMask::SetupMaskSettingsAutomatically()
extern void MMSpriteMask_SetupMaskSettingsAutomatically_m610DBC60D68E55A363DB158E9B0F168E043900AC (void);
// 0x00000013 System.Void MoreMountains.Tools.MMSpriteMask::MoveMaskTo(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
extern void MMSpriteMask_MoveMaskTo_m0BC6E578BDF8D848BB6BC5D2C9C578E912C92C13 (void);
// 0x00000014 System.Void MoreMountains.Tools.MMSpriteMask::ExpandAndMoveMaskTo(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
extern void MMSpriteMask_ExpandAndMoveMaskTo_mD7002603025D24708F6FE4399EF02CF0E8A4C967 (void);
// 0x00000015 System.Collections.IEnumerator MoreMountains.Tools.MMSpriteMask::MoveMaskToCoroutine(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
extern void MMSpriteMask_MoveMaskToCoroutine_mC814E9FF0FD345224BE435B47F0DE1CC6727BEEF (void);
// 0x00000016 System.Collections.IEnumerator MoreMountains.Tools.MMSpriteMask::ExpandAndMoveMaskToCoroutine(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,MoreMountains.Tools.MMTween/MMTweenCurve)
extern void MMSpriteMask_ExpandAndMoveMaskToCoroutine_m48F80242B8D61A00362DB235E20DE7F73A3B1764 (void);
// 0x00000017 UnityEngine.Vector3 MoreMountains.Tools.MMSpriteMask::ComputeTargetPosition(UnityEngine.Vector3)
extern void MMSpriteMask_ComputeTargetPosition_mACEA838E818442E681C85A36190CA367B1706C2E (void);
// 0x00000018 UnityEngine.Vector3 MoreMountains.Tools.MMSpriteMask::ComputeTargetScale(UnityEngine.Vector3)
extern void MMSpriteMask_ComputeTargetScale_m8E9B80ED0BDB3F7A6D8E059C771971D6D35C40B7 (void);
// 0x00000019 System.Void MoreMountains.Tools.MMSpriteMask::OnMMEvent(MoreMountains.Tools.MMSpriteMaskEvent)
extern void MMSpriteMask_OnMMEvent_m2B8CC9DD7983E1B0C512A90778FEBEC912A9AB08 (void);
// 0x0000001A System.Void MoreMountains.Tools.MMSpriteMask::OnEnable()
extern void MMSpriteMask_OnEnable_m562F5295165BC0C0E7EEE812B31B863870A8922B (void);
// 0x0000001B System.Void MoreMountains.Tools.MMSpriteMask::OnDisable()
extern void MMSpriteMask_OnDisable_mB61235319B4AD641697EF8348366CA90F266BAC5 (void);
// 0x0000001C System.Void MoreMountains.Tools.MMSpriteMask::.ctor()
extern void MMSpriteMask__ctor_m2CF0A584ECAF3E480BF1588F5D324E7C27FCA937 (void);
// 0x0000001D System.Void MoreMountains.Tools.MMSpriteMask/<MoveMaskToCoroutine>d__20::.ctor(System.Int32)
extern void U3CMoveMaskToCoroutineU3Ed__20__ctor_mB779205F64ECBE4E56370E24746CC6CE1CC8EC50 (void);
// 0x0000001E System.Void MoreMountains.Tools.MMSpriteMask/<MoveMaskToCoroutine>d__20::System.IDisposable.Dispose()
extern void U3CMoveMaskToCoroutineU3Ed__20_System_IDisposable_Dispose_mBACA27AB9F3A52D1661665E66535E431AACDF79B (void);
// 0x0000001F System.Boolean MoreMountains.Tools.MMSpriteMask/<MoveMaskToCoroutine>d__20::MoveNext()
extern void U3CMoveMaskToCoroutineU3Ed__20_MoveNext_mCF215A10F260A3A710E623A091B768784A77F2C1 (void);
// 0x00000020 System.Object MoreMountains.Tools.MMSpriteMask/<MoveMaskToCoroutine>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMoveMaskToCoroutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58B9BB5C06741B01341810503106721A1B9FD5DC (void);
// 0x00000021 System.Void MoreMountains.Tools.MMSpriteMask/<MoveMaskToCoroutine>d__20::System.Collections.IEnumerator.Reset()
extern void U3CMoveMaskToCoroutineU3Ed__20_System_Collections_IEnumerator_Reset_m7125C91AC5C6CCC6DBC11E50E8F96B60C6582580 (void);
// 0x00000022 System.Object MoreMountains.Tools.MMSpriteMask/<MoveMaskToCoroutine>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CMoveMaskToCoroutineU3Ed__20_System_Collections_IEnumerator_get_Current_m8F25A19996D166AA61575A940832D7A9F3B3D8F2 (void);
// 0x00000023 System.Void MoreMountains.Tools.MMSpriteMask/<ExpandAndMoveMaskToCoroutine>d__21::.ctor(System.Int32)
extern void U3CExpandAndMoveMaskToCoroutineU3Ed__21__ctor_m7BF8E015B683F8AC56F25FAAED7E4EDBAD6A18A1 (void);
// 0x00000024 System.Void MoreMountains.Tools.MMSpriteMask/<ExpandAndMoveMaskToCoroutine>d__21::System.IDisposable.Dispose()
extern void U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_IDisposable_Dispose_m2E59E1F14B28431AB8767E92526CB1DDB313ACB6 (void);
// 0x00000025 System.Boolean MoreMountains.Tools.MMSpriteMask/<ExpandAndMoveMaskToCoroutine>d__21::MoveNext()
extern void U3CExpandAndMoveMaskToCoroutineU3Ed__21_MoveNext_m060ECB2E8F25859EF2FE234156CF5F332E48C86A (void);
// 0x00000026 System.Object MoreMountains.Tools.MMSpriteMask/<ExpandAndMoveMaskToCoroutine>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B578450D00F744666A2E46965389B91E316369E (void);
// 0x00000027 System.Void MoreMountains.Tools.MMSpriteMask/<ExpandAndMoveMaskToCoroutine>d__21::System.Collections.IEnumerator.Reset()
extern void U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m9561E85AB2DD8E913626937862083845DED3E40D (void);
// 0x00000028 System.Object MoreMountains.Tools.MMSpriteMask/<ExpandAndMoveMaskToCoroutine>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_m2823E34D90A48656AE04D801E28B8A1E0276DDB3 (void);
// 0x00000029 System.Void MoreMountains.CorgiEngine.AchievementRules::OnMMEvent(MoreMountains.Tools.MMGameEvent)
extern void AchievementRules_OnMMEvent_mAAB3BE7277668870CA26B5977E0BFEDA6991157B (void);
// 0x0000002A System.Void MoreMountains.CorgiEngine.AchievementRules::OnMMEvent(MoreMountains.CorgiEngine.MMCharacterEvent)
extern void AchievementRules_OnMMEvent_mAA26A7A878581F137B604DD64DBF6872C3D5C989 (void);
// 0x0000002B System.Void MoreMountains.CorgiEngine.AchievementRules::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void AchievementRules_OnMMEvent_mCE4DD65178BE51592F96DB287008542EEC466F03 (void);
// 0x0000002C System.Void MoreMountains.CorgiEngine.AchievementRules::OnMMEvent(MoreMountains.CorgiEngine.PickableItemEvent)
extern void AchievementRules_OnMMEvent_m30B354CCC6ECE655591A463A403DCC6E76BD1978 (void);
// 0x0000002D System.Void MoreMountains.CorgiEngine.AchievementRules::OnMMEvent(MoreMountains.Tools.MMStateChangeEvent`1<MoreMountains.CorgiEngine.CharacterStates/MovementStates>)
extern void AchievementRules_OnMMEvent_m9861E0C2E7344BA6F8B123C492B15B0EA44CADCF (void);
// 0x0000002E System.Void MoreMountains.CorgiEngine.AchievementRules::OnMMEvent(MoreMountains.Tools.MMStateChangeEvent`1<MoreMountains.CorgiEngine.CharacterStates/CharacterConditions>)
extern void AchievementRules_OnMMEvent_mDC4BD4E5F22A7C06EBA3BBC3CEE255D639BE491D (void);
// 0x0000002F System.Void MoreMountains.CorgiEngine.AchievementRules::OnEnable()
extern void AchievementRules_OnEnable_mE580E4B8E72A5088EA4C88523D9CCD0A7C1132CC (void);
// 0x00000030 System.Void MoreMountains.CorgiEngine.AchievementRules::OnDisable()
extern void AchievementRules_OnDisable_mECAE33EC0AD4C08BC40119FA0D8D875ADC23FA7C (void);
// 0x00000031 System.Void MoreMountains.CorgiEngine.AchievementRules::.ctor()
extern void AchievementRules__ctor_mBB50E1369EDC16EEE5F7A78F05735B369D0B6B45 (void);
// 0x00000032 System.Void MoreMountains.CorgiEngine.AIActionChangeWeapon::Initialization()
extern void AIActionChangeWeapon_Initialization_mEF376B72339FC8FE6911AED6ECE87C3DE3A8237E (void);
// 0x00000033 System.Void MoreMountains.CorgiEngine.AIActionChangeWeapon::PerformAction()
extern void AIActionChangeWeapon_PerformAction_m4BBB85DE7F51669117BAC75F6F51D707BC8ABBE8 (void);
// 0x00000034 System.Void MoreMountains.CorgiEngine.AIActionChangeWeapon::ChangeWeapon()
extern void AIActionChangeWeapon_ChangeWeapon_m7B58694B5F7331031F9A2D7F6DF3AA195C15355D (void);
// 0x00000035 System.Void MoreMountains.CorgiEngine.AIActionChangeWeapon::OnEnterState()
extern void AIActionChangeWeapon_OnEnterState_mD38A0538BD2C0ABDE5B85BDBD4FECAF174AA809A (void);
// 0x00000036 System.Void MoreMountains.CorgiEngine.AIActionChangeWeapon::.ctor()
extern void AIActionChangeWeapon__ctor_m8C1780366D61220AA320D2CE6D0D781EFA60E6C3 (void);
// 0x00000037 System.Void MoreMountains.CorgiEngine.AIActionDash::Initialization()
extern void AIActionDash_Initialization_m67168AB154AC75EAF972EB4A80080805C6F5CA86 (void);
// 0x00000038 System.Void MoreMountains.CorgiEngine.AIActionDash::PerformAction()
extern void AIActionDash_PerformAction_m2051D7479AA6911E506B68C1A764CE3D0F78E34D (void);
// 0x00000039 System.Void MoreMountains.CorgiEngine.AIActionDash::Dash()
extern void AIActionDash_Dash_m71A73160DD3AFF631F3FAFF1604F254820D89758 (void);
// 0x0000003A System.Void MoreMountains.CorgiEngine.AIActionDash::.ctor()
extern void AIActionDash__ctor_m85BE527F8C45C66C9C56FC6071886F1B3FD9AAC7 (void);
// 0x0000003B System.Void MoreMountains.CorgiEngine.AIActionDoNothing::PerformAction()
extern void AIActionDoNothing_PerformAction_m731C067D8AD6E93FDFC43AD830FE9C85D93E98B2 (void);
// 0x0000003C System.Void MoreMountains.CorgiEngine.AIActionDoNothing::.ctor()
extern void AIActionDoNothing__ctor_m8155A8889277A226EA92F1924EC64DDFAFE572F5 (void);
// 0x0000003D System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::Initialization()
extern void AIActionFlyPatrol_Initialization_m7C372147D2230268BBFFE40ED8860513D8357289 (void);
// 0x0000003E System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::PerformAction()
extern void AIActionFlyPatrol_PerformAction_mD1BCF1834E26DB349E040949179DAD022D3580D3 (void);
// 0x0000003F System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::FlyPatrol()
extern void AIActionFlyPatrol_FlyPatrol_mB5BEC7546B52F5A42E6C24D65D40E2DA83D50322 (void);
// 0x00000040 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::OnDrawGizmosSelected()
extern void AIActionFlyPatrol_OnDrawGizmosSelected_m4FA344C333303581379CB3D96FB65009A38C9546 (void);
// 0x00000041 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::OnExitState()
extern void AIActionFlyPatrol_OnExitState_m492B259BFC80100B2A65E8CB0ACC39035DE5FA10 (void);
// 0x00000042 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::CheckForObstacles()
extern void AIActionFlyPatrol_CheckForObstacles_m3AD419F99758CE66531C20F739BB1FAAB6E8BCA4 (void);
// 0x00000043 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::ChangeDirection()
extern void AIActionFlyPatrol_ChangeDirection_mFBD5A50150703228451C9328DCCAD412B9763F8B (void);
// 0x00000044 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::OnRevive()
extern void AIActionFlyPatrol_OnRevive_mF26AB2A2A9046A93124E6E85C38A1739E0766670 (void);
// 0x00000045 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::OnEnable()
extern void AIActionFlyPatrol_OnEnable_mDF33D8F33E0AEE559553D65E010D520CA29DD600 (void);
// 0x00000046 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::OnDisable()
extern void AIActionFlyPatrol_OnDisable_mE9011E52FD1FB277E1B9EDFE441E6BA0992383FF (void);
// 0x00000047 System.Void MoreMountains.CorgiEngine.AIActionFlyPatrol::.ctor()
extern void AIActionFlyPatrol__ctor_m4DB6B7487B53DC36985FD22582019F1A6F9C176F (void);
// 0x00000048 System.Void MoreMountains.CorgiEngine.AIActionFlyTowardsTarget::Initialization()
extern void AIActionFlyTowardsTarget_Initialization_m08BE3BC6FFEC43D11D650755EEB7F883ADA58383 (void);
// 0x00000049 System.Void MoreMountains.CorgiEngine.AIActionFlyTowardsTarget::PerformAction()
extern void AIActionFlyTowardsTarget_PerformAction_m40C5DA21CF947834BE599B3C118CFEABC991E03B (void);
// 0x0000004A System.Void MoreMountains.CorgiEngine.AIActionFlyTowardsTarget::Fly()
extern void AIActionFlyTowardsTarget_Fly_m95D9BFF47E5AF88AB6FD2DE7620B28CCBBC38A33 (void);
// 0x0000004B System.Void MoreMountains.CorgiEngine.AIActionFlyTowardsTarget::OnExitState()
extern void AIActionFlyTowardsTarget_OnExitState_m87280E76CF18399F76300044E7B6B3043F9E4A90 (void);
// 0x0000004C System.Void MoreMountains.CorgiEngine.AIActionFlyTowardsTarget::.ctor()
extern void AIActionFlyTowardsTarget__ctor_mE831C418DD05222E46FF245A11FE95052C6D890F (void);
// 0x0000004D System.Void MoreMountains.CorgiEngine.AIActionJump::Initialization()
extern void AIActionJump_Initialization_m4435BD6EEF6A7A356B52FCB90020018BC1FD5588 (void);
// 0x0000004E System.Void MoreMountains.CorgiEngine.AIActionJump::PerformAction()
extern void AIActionJump_PerformAction_m9AEA983421200D8CF5A39558A01213DB150EE904 (void);
// 0x0000004F System.Void MoreMountains.CorgiEngine.AIActionJump::Jump()
extern void AIActionJump_Jump_mAA139D93A6CAEB170E6E1B1FF9C1E332190E6D34 (void);
// 0x00000050 System.Void MoreMountains.CorgiEngine.AIActionJump::OnEnterState()
extern void AIActionJump_OnEnterState_m46AF624791EB5349D25A3778189FD6E24A0A1C20 (void);
// 0x00000051 System.Void MoreMountains.CorgiEngine.AIActionJump::.ctor()
extern void AIActionJump__ctor_mFFA924E0BC6F79E7A79C44451DFB765655149564 (void);
// 0x00000052 System.Void MoreMountains.CorgiEngine.AIActionMMFeedbacks::PerformAction()
extern void AIActionMMFeedbacks_PerformAction_m2A36870E3A8A02EA89BBA7438244FAFF15B05EF7 (void);
// 0x00000053 System.Void MoreMountains.CorgiEngine.AIActionMMFeedbacks::PlayFeedbacks()
extern void AIActionMMFeedbacks_PlayFeedbacks_mAD362AA3903E74C23EF1398364AEFFE2F04F6F75 (void);
// 0x00000054 System.Void MoreMountains.CorgiEngine.AIActionMMFeedbacks::OnEnterState()
extern void AIActionMMFeedbacks_OnEnterState_m172F8DC4DD5059746628A0944AEFEDCFA2C5B153 (void);
// 0x00000055 System.Void MoreMountains.CorgiEngine.AIActionMMFeedbacks::.ctor()
extern void AIActionMMFeedbacks__ctor_mF98ABC7238855099ED7856DEF358BF4148D49A55 (void);
// 0x00000056 System.Void MoreMountains.CorgiEngine.AIActionMoveAwayFromTarget::Move()
extern void AIActionMoveAwayFromTarget_Move_m905276FAC4F8BCC6551438411D1776EF47756F65 (void);
// 0x00000057 System.Void MoreMountains.CorgiEngine.AIActionMoveAwayFromTarget::.ctor()
extern void AIActionMoveAwayFromTarget__ctor_mAD62A578D4F4B971BB3ADE3B053216F8E31E3C4F (void);
// 0x00000058 System.Void MoreMountains.CorgiEngine.AIActionMoveTowardsTarget::Initialization()
extern void AIActionMoveTowardsTarget_Initialization_m21B3343C877DE69BA3E9BF05EE002AB000E3F839 (void);
// 0x00000059 System.Void MoreMountains.CorgiEngine.AIActionMoveTowardsTarget::PerformAction()
extern void AIActionMoveTowardsTarget_PerformAction_m58D83A5C44A805C45FAC3DAF5EBF869A9D032727 (void);
// 0x0000005A System.Void MoreMountains.CorgiEngine.AIActionMoveTowardsTarget::Move()
extern void AIActionMoveTowardsTarget_Move_mD4386DD500AF8173B90BFF86E3623CFCE1EA542E (void);
// 0x0000005B System.Void MoreMountains.CorgiEngine.AIActionMoveTowardsTarget::OnEnterState()
extern void AIActionMoveTowardsTarget_OnEnterState_m202B344BDCBC983495EDBC536B1C3B660B3FA3D7 (void);
// 0x0000005C System.Void MoreMountains.CorgiEngine.AIActionMoveTowardsTarget::OnExitState()
extern void AIActionMoveTowardsTarget_OnExitState_m335F87618B2EF710DA25B1C4B386C2592DB80DF1 (void);
// 0x0000005D System.Void MoreMountains.CorgiEngine.AIActionMoveTowardsTarget::.ctor()
extern void AIActionMoveTowardsTarget__ctor_m4B07391F366B6B1C45083E4860AC9607585BABA9 (void);
// 0x0000005E System.Void MoreMountains.CorgiEngine.AIActionPatrol::Initialization()
extern void AIActionPatrol_Initialization_m2DC85D6A11ADCBB75EABF874B08F8C6B8427BF94 (void);
// 0x0000005F System.Void MoreMountains.CorgiEngine.AIActionPatrol::PerformAction()
extern void AIActionPatrol_PerformAction_mCA5A883FBF5B8387B68F63268F7DB38DD96A6718 (void);
// 0x00000060 System.Void MoreMountains.CorgiEngine.AIActionPatrol::Patrol()
extern void AIActionPatrol_Patrol_m40ABC968F8B27B46AD4F6A9044E67EC1239D6A57 (void);
// 0x00000061 System.Void MoreMountains.CorgiEngine.AIActionPatrol::OnExitState()
extern void AIActionPatrol_OnExitState_m43D4852B5FA3038107F208937845E5181EB0A230 (void);
// 0x00000062 System.Void MoreMountains.CorgiEngine.AIActionPatrol::CheckForWalls()
extern void AIActionPatrol_CheckForWalls_mEA151C8177E1FC5B5C747979CD944AC36883EB9D (void);
// 0x00000063 System.Boolean MoreMountains.CorgiEngine.AIActionPatrol::DetectObstaclesRegularLayermask()
extern void AIActionPatrol_DetectObstaclesRegularLayermask_mAFFB31595F1761285C1FD5AD8E539988FE7B540C (void);
// 0x00000064 System.Boolean MoreMountains.CorgiEngine.AIActionPatrol::DetectObstaclesCustomLayermask()
extern void AIActionPatrol_DetectObstaclesCustomLayermask_m2CCA2C1BFB58CE40FADA4727A30C9D849495F85A (void);
// 0x00000065 System.Void MoreMountains.CorgiEngine.AIActionPatrol::CheckForHoles()
extern void AIActionPatrol_CheckForHoles_m909F98127B833E3365E3A9F32A374737595C8510 (void);
// 0x00000066 System.Void MoreMountains.CorgiEngine.AIActionPatrol::ChangeDirection()
extern void AIActionPatrol_ChangeDirection_m73EBF976104F57BC74E4672B9CD2DFD29CC22993 (void);
// 0x00000067 System.Void MoreMountains.CorgiEngine.AIActionPatrol::OnRevive()
extern void AIActionPatrol_OnRevive_m480E2E2D56D8E15573FDDB9658E2E326BC4C0F91 (void);
// 0x00000068 System.Void MoreMountains.CorgiEngine.AIActionPatrol::OnEnable()
extern void AIActionPatrol_OnEnable_m35B147C245498DC1FE4A537BD59FBA8C08FA61BB (void);
// 0x00000069 System.Void MoreMountains.CorgiEngine.AIActionPatrol::OnDisable()
extern void AIActionPatrol_OnDisable_m85E7176E310B2BF9702D4D26554E921172BD596F (void);
// 0x0000006A System.Void MoreMountains.CorgiEngine.AIActionPatrol::.ctor()
extern void AIActionPatrol__ctor_m9C1AEA53F1BD15F5EFEACE80EE1D81E0B77C2F09 (void);
// 0x0000006B System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::Initialization()
extern void AIActionPatrolWithinBounds_Initialization_m916DBAA327E6E6371A6CB7D42042B6C71FFAFA2B (void);
// 0x0000006C System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::Patrol()
extern void AIActionPatrolWithinBounds_Patrol_mB94C7A94CD02CD3FAA8701271E6FAF873B458A6D (void);
// 0x0000006D System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::CheckForDistance()
extern void AIActionPatrolWithinBounds_CheckForDistance_m74CCBDE6FAF4CC36FB6F752EE9EA96ACA45454B5 (void);
// 0x0000006E System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::EstablishBounds()
extern void AIActionPatrolWithinBounds_EstablishBounds_mDE4F6527FD7D2CB50AC79BEEA07E242D3C4D16BD (void);
// 0x0000006F System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::OnEnterState()
extern void AIActionPatrolWithinBounds_OnEnterState_m125533EF4CBA659319F3C79E0FFEB52ACC40C0FB (void);
// 0x00000070 System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::OnExitState()
extern void AIActionPatrolWithinBounds_OnExitState_m7665BFC1C488D1922E86480BA3A2A01C24B9ADE9 (void);
// 0x00000071 System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::OnDrawGizmosSelected()
extern void AIActionPatrolWithinBounds_OnDrawGizmosSelected_m9C2254729BFE29AD86F65AB2499580C02340F521 (void);
// 0x00000072 System.Void MoreMountains.CorgiEngine.AIActionPatrolWithinBounds::.ctor()
extern void AIActionPatrolWithinBounds__ctor_m5DBDD38C771134F715054262CEB23D6508DFA763 (void);
// 0x00000073 System.Void MoreMountains.CorgiEngine.AIActionReload::Initialization()
extern void AIActionReload_Initialization_m966C1ECE3EDE29C55B3642CEF65A31225EAF2DB3 (void);
// 0x00000074 System.Void MoreMountains.CorgiEngine.AIActionReload::PerformAction()
extern void AIActionReload_PerformAction_m3B65877626CF619EF11D8BA821133C107971472C (void);
// 0x00000075 System.Void MoreMountains.CorgiEngine.AIActionReload::OnEnterState()
extern void AIActionReload_OnEnterState_mFC4341B16A1C832BD406D16E89DCA10EEA9079C3 (void);
// 0x00000076 System.Void MoreMountains.CorgiEngine.AIActionReload::.ctor()
extern void AIActionReload__ctor_m12FB874DD8B78D5A520E248557DA219F0B5B93F5 (void);
// 0x00000077 System.Void MoreMountains.CorgiEngine.AIActionResetTarget::PerformAction()
extern void AIActionResetTarget_PerformAction_mB8CEDC3324E1F99C82782480FEF5227B9DE39B4E (void);
// 0x00000078 System.Void MoreMountains.CorgiEngine.AIActionResetTarget::.ctor()
extern void AIActionResetTarget__ctor_mCBB8AFEE51E9072CF31341733EE74D598B7AE6CF (void);
// 0x00000079 System.Void MoreMountains.CorgiEngine.AIActionSelfDestruct::Initialization()
extern void AIActionSelfDestruct_Initialization_mACC0AD633AA9A81E3FD82C4A8249938E3FD4F6AA (void);
// 0x0000007A System.Void MoreMountains.CorgiEngine.AIActionSelfDestruct::PerformAction()
extern void AIActionSelfDestruct_PerformAction_m12DC7141407F65DF9BD5E8D18495FE9981734B0E (void);
// 0x0000007B System.Void MoreMountains.CorgiEngine.AIActionSelfDestruct::OnEnterState()
extern void AIActionSelfDestruct_OnEnterState_m47A1521E5C1800B871CEE04EBA63F635262EC52F (void);
// 0x0000007C System.Void MoreMountains.CorgiEngine.AIActionSelfDestruct::.ctor()
extern void AIActionSelfDestruct__ctor_m86E2FAFCDDBA6486765F5425153E9EF0816F5AAD (void);
// 0x0000007D System.Void MoreMountains.CorgiEngine.AIActionSetLastKnownPositionAsTarget::Initialization()
extern void AIActionSetLastKnownPositionAsTarget_Initialization_mCA16C6799DAABB53CBC4FDC9E9334C3F081A8708 (void);
// 0x0000007E System.Void MoreMountains.CorgiEngine.AIActionSetLastKnownPositionAsTarget::PerformAction()
extern void AIActionSetLastKnownPositionAsTarget_PerformAction_m2DC213E2F7775BD553539FF9240FD5A92CAE77E2 (void);
// 0x0000007F System.Void MoreMountains.CorgiEngine.AIActionSetLastKnownPositionAsTarget::.ctor()
extern void AIActionSetLastKnownPositionAsTarget__ctor_m22F1237F250DDA6D881EEB31E5AD10936B191F1A (void);
// 0x00000080 System.Void MoreMountains.CorgiEngine.AIActionShoot::Initialization()
extern void AIActionShoot_Initialization_mCFEDCBC4152CA764D3448995ED4A6F903012984F (void);
// 0x00000081 System.Void MoreMountains.CorgiEngine.AIActionShoot::PerformAction()
extern void AIActionShoot_PerformAction_mAE66A6C7BA3104000842D5322F0744B94F6B8B18 (void);
// 0x00000082 System.Void MoreMountains.CorgiEngine.AIActionShoot::Update()
extern void AIActionShoot_Update_mE13CD84B0A1BF788089688E3DACC146F77941951 (void);
// 0x00000083 System.Void MoreMountains.CorgiEngine.AIActionShoot::TestFaceTarget()
extern void AIActionShoot_TestFaceTarget_mC9C357689AB3646FB16B06B69AEA19AF91E0CBD2 (void);
// 0x00000084 System.Void MoreMountains.CorgiEngine.AIActionShoot::TestAimAtTarget()
extern void AIActionShoot_TestAimAtTarget_mA2B1D82B22BBE30927C748736F7B7743BDC970A7 (void);
// 0x00000085 System.Void MoreMountains.CorgiEngine.AIActionShoot::Shoot()
extern void AIActionShoot_Shoot_mFE3AC0A72D3521080F2E1968D612F1C8384F72A9 (void);
// 0x00000086 System.Void MoreMountains.CorgiEngine.AIActionShoot::OnEnterState()
extern void AIActionShoot_OnEnterState_m4990EFFBA382D1ADC9F922A23B174C61BE7609DC (void);
// 0x00000087 System.Void MoreMountains.CorgiEngine.AIActionShoot::OnExitState()
extern void AIActionShoot_OnExitState_m8FBACE9A4F5D27A5D1BC44375C4BD86A017397B6 (void);
// 0x00000088 System.Void MoreMountains.CorgiEngine.AIActionShoot::.ctor()
extern void AIActionShoot__ctor_m1F327CBA151E0CC52542F894AAD13A0DFD19F3F1 (void);
// 0x00000089 System.Void MoreMountains.CorgiEngine.AIActionSwapBrain::Initialization()
extern void AIActionSwapBrain_Initialization_mD781D38EF6BEB09FAB61D681F9D333D61F825817 (void);
// 0x0000008A System.Void MoreMountains.CorgiEngine.AIActionSwapBrain::PerformAction()
extern void AIActionSwapBrain_PerformAction_m9F6041404D97264A7011A625A1DD9BD119B4E1E3 (void);
// 0x0000008B System.Void MoreMountains.CorgiEngine.AIActionSwapBrain::SwapBrain()
extern void AIActionSwapBrain_SwapBrain_m357564D7FA0C4A4A6DF44A1422FDFDD43D78D62D (void);
// 0x0000008C System.Void MoreMountains.CorgiEngine.AIActionSwapBrain::.ctor()
extern void AIActionSwapBrain__ctor_m01F1CE2EA678069ED74E7C6D0A2DE6A6E96078D3 (void);
// 0x0000008D System.Void MoreMountains.CorgiEngine.AIActionUnityEvents::PerformAction()
extern void AIActionUnityEvents_PerformAction_m6F2B7C190D2DF89C118DE8DA523E5ECB4DC56E88 (void);
// 0x0000008E System.Void MoreMountains.CorgiEngine.AIActionUnityEvents::TriggerEvent()
extern void AIActionUnityEvents_TriggerEvent_m5190D7415E3F7ECD6615E49099FB88A2F8CE3154 (void);
// 0x0000008F System.Void MoreMountains.CorgiEngine.AIActionUnityEvents::OnEnterState()
extern void AIActionUnityEvents_OnEnterState_mCB57AC7D6F768BC71CD16E5D733BADBE9CAE7630 (void);
// 0x00000090 System.Void MoreMountains.CorgiEngine.AIActionUnityEvents::.ctor()
extern void AIActionUnityEvents__ctor_m8BA2795BF85E46643E2765E702E00451675D5863 (void);
// 0x00000091 System.Void MoreMountains.CorgiEngine.AIDecisionDetectTargetLine::Initialization()
extern void AIDecisionDetectTargetLine_Initialization_m77957B482806E921576BC4CB032123947F33D5E6 (void);
// 0x00000092 System.Boolean MoreMountains.CorgiEngine.AIDecisionDetectTargetLine::Decide()
extern void AIDecisionDetectTargetLine_Decide_mC3043E02EBCDAEA016A6659673D2545DAEA360E0 (void);
// 0x00000093 System.Boolean MoreMountains.CorgiEngine.AIDecisionDetectTargetLine::DetectTarget()
extern void AIDecisionDetectTargetLine_DetectTarget_mE871451FEADC1A54E6E8D916CC13FDBA3F1058E5 (void);
// 0x00000094 System.Void MoreMountains.CorgiEngine.AIDecisionDetectTargetLine::OnDrawGizmos()
extern void AIDecisionDetectTargetLine_OnDrawGizmos_mFA3A6F6A41649A0B143F1FE99A77B09B101D733F (void);
// 0x00000095 System.Void MoreMountains.CorgiEngine.AIDecisionDetectTargetLine::.ctor()
extern void AIDecisionDetectTargetLine__ctor_m7ED62F0FFDDC1D3CCE79F0A8F94B3CE52E7614CC (void);
// 0x00000096 System.Void MoreMountains.CorgiEngine.AIDecisionDetectTargetRadius::Initialization()
extern void AIDecisionDetectTargetRadius_Initialization_mB536D601614C7C1AC7A3E655008CA258C05CA07D (void);
// 0x00000097 System.Boolean MoreMountains.CorgiEngine.AIDecisionDetectTargetRadius::Decide()
extern void AIDecisionDetectTargetRadius_Decide_m1C18976840A9B819D2D0E3A0C32909BD5ABAFE00 (void);
// 0x00000098 System.Boolean MoreMountains.CorgiEngine.AIDecisionDetectTargetRadius::DetectTarget()
extern void AIDecisionDetectTargetRadius_DetectTarget_m922B50B17E4491FA13923CAA62C8CB62E351267E (void);
// 0x00000099 System.Void MoreMountains.CorgiEngine.AIDecisionDetectTargetRadius::OnDrawGizmosSelected()
extern void AIDecisionDetectTargetRadius_OnDrawGizmosSelected_m81B3E0532C1CABC673988BC3328A9A7F75CA591E (void);
// 0x0000009A System.Void MoreMountains.CorgiEngine.AIDecisionDetectTargetRadius::.ctor()
extern void AIDecisionDetectTargetRadius__ctor_mC92DA45C31601002C709B4A3F48AE44AA8B3EB9D (void);
// 0x0000009B System.Boolean MoreMountains.CorgiEngine.AIDecisionDistanceToTarget::Decide()
extern void AIDecisionDistanceToTarget_Decide_m417362C1AAF557BF18BD5C69231A0B8A191086EF (void);
// 0x0000009C System.Boolean MoreMountains.CorgiEngine.AIDecisionDistanceToTarget::EvaluateDistance()
extern void AIDecisionDistanceToTarget_EvaluateDistance_m7BE81DDB28906D54EAA3D1766DD578084B50AA2F (void);
// 0x0000009D System.Void MoreMountains.CorgiEngine.AIDecisionDistanceToTarget::.ctor()
extern void AIDecisionDistanceToTarget__ctor_m3AC88769049C7EB3C66C7D94478504A5890A5428 (void);
// 0x0000009E System.Void MoreMountains.CorgiEngine.AIDecisionGrounded::Initialization()
extern void AIDecisionGrounded_Initialization_m445003C43E3DBB239905E33358B116984771D4E5 (void);
// 0x0000009F System.Boolean MoreMountains.CorgiEngine.AIDecisionGrounded::Decide()
extern void AIDecisionGrounded_Decide_mC0BF81DA9B2B8E29B00717A3B5203FA69A2F04E0 (void);
// 0x000000A0 System.Boolean MoreMountains.CorgiEngine.AIDecisionGrounded::EvaluateGrounded()
extern void AIDecisionGrounded_EvaluateGrounded_mD9FBA8356B8A85BB06F5272A7EF36E717C780C0C (void);
// 0x000000A1 System.Void MoreMountains.CorgiEngine.AIDecisionGrounded::OnEnterState()
extern void AIDecisionGrounded_OnEnterState_m9085BAFB53015B600636135E38D64888DC138B12 (void);
// 0x000000A2 System.Void MoreMountains.CorgiEngine.AIDecisionGrounded::.ctor()
extern void AIDecisionGrounded__ctor_m397C7F2DB0649C44DA9A8FC5D8FBEDDC3DA0187B (void);
// 0x000000A3 System.Void MoreMountains.CorgiEngine.AIDecisionHealth::Initialization()
extern void AIDecisionHealth_Initialization_m5E4965639595ECB2A28387A4462E0D059E4AEE84 (void);
// 0x000000A4 System.Boolean MoreMountains.CorgiEngine.AIDecisionHealth::Decide()
extern void AIDecisionHealth_Decide_m5369C10227BA7AE07D8F77FA6DB47583D99E6089 (void);
// 0x000000A5 System.Boolean MoreMountains.CorgiEngine.AIDecisionHealth::EvaluateHealth()
extern void AIDecisionHealth_EvaluateHealth_m0996172C4F948EB51ACBC7ADF8FD02DB6C6BE26C (void);
// 0x000000A6 System.Void MoreMountains.CorgiEngine.AIDecisionHealth::.ctor()
extern void AIDecisionHealth__ctor_mF1744025167F668C6603281D80DA5A6D4D0E6171 (void);
// 0x000000A7 System.Void MoreMountains.CorgiEngine.AIDecisionHit::Initialization()
extern void AIDecisionHit_Initialization_m975FAC82D438B848513CB4508E86CFF44026D524 (void);
// 0x000000A8 System.Boolean MoreMountains.CorgiEngine.AIDecisionHit::Decide()
extern void AIDecisionHit_Decide_m72845B73FAE4216A3A7062B028C7303945C273EE (void);
// 0x000000A9 System.Boolean MoreMountains.CorgiEngine.AIDecisionHit::EvaluateHits()
extern void AIDecisionHit_EvaluateHits_mD6159A3C5A313BA834BBBD29BECB7EBA6FD3FE32 (void);
// 0x000000AA System.Void MoreMountains.CorgiEngine.AIDecisionHit::OnEnterState()
extern void AIDecisionHit_OnEnterState_mE88DBAA48FF6430BBDACFF461BF63344E305293C (void);
// 0x000000AB System.Void MoreMountains.CorgiEngine.AIDecisionHit::OnExitState()
extern void AIDecisionHit_OnExitState_m4E6AC172814252563D4543DC42660F95F16EE62C (void);
// 0x000000AC System.Void MoreMountains.CorgiEngine.AIDecisionHit::OnHit()
extern void AIDecisionHit_OnHit_m729E06971CC05FE34FAB1C6EC86953BF50ABDECC (void);
// 0x000000AD System.Void MoreMountains.CorgiEngine.AIDecisionHit::OnEnable()
extern void AIDecisionHit_OnEnable_mDDA879B5FFDB56F276B7843307724E4AC5D51F47 (void);
// 0x000000AE System.Void MoreMountains.CorgiEngine.AIDecisionHit::OnDisable()
extern void AIDecisionHit_OnDisable_mB1D74062FAC88F3518452A22A20E13C5909E93EA (void);
// 0x000000AF System.Void MoreMountains.CorgiEngine.AIDecisionHit::.ctor()
extern void AIDecisionHit__ctor_m62859448911B2DD54CF4F7796D949210C3E00501 (void);
// 0x000000B0 System.Void MoreMountains.CorgiEngine.AIDecisionLineOfSightToTarget::Initialization()
extern void AIDecisionLineOfSightToTarget_Initialization_m729B4425776C29F16EEB2E20940FB2B0BB34A4CD (void);
// 0x000000B1 System.Boolean MoreMountains.CorgiEngine.AIDecisionLineOfSightToTarget::Decide()
extern void AIDecisionLineOfSightToTarget_Decide_mD5BE1B97F4EDEF8C8FB03ADEC7935FE1717D4244 (void);
// 0x000000B2 System.Boolean MoreMountains.CorgiEngine.AIDecisionLineOfSightToTarget::CheckLineOfSight()
extern void AIDecisionLineOfSightToTarget_CheckLineOfSight_m91E671EDAB5B53EC4B591B7B8EF7E0CA85FDC778 (void);
// 0x000000B3 System.Void MoreMountains.CorgiEngine.AIDecisionLineOfSightToTarget::.ctor()
extern void AIDecisionLineOfSightToTarget__ctor_m367894A18CEE324894592FB3DE16E25B90D26DE0 (void);
// 0x000000B4 System.Boolean MoreMountains.CorgiEngine.AIDecisionNextFrame::Decide()
extern void AIDecisionNextFrame_Decide_mC79CBF33A2CEA537FC308D781615A3B616665924 (void);
// 0x000000B5 System.Void MoreMountains.CorgiEngine.AIDecisionNextFrame::.ctor()
extern void AIDecisionNextFrame__ctor_mF40CBBDDC032E793421F8314A65822F3D505894A (void);
// 0x000000B6 System.Boolean MoreMountains.CorgiEngine.AIDecisionRandom::Decide()
extern void AIDecisionRandom_Decide_m58A9F8AC84F9216B884EB628765EA91A493089CC (void);
// 0x000000B7 System.Boolean MoreMountains.CorgiEngine.AIDecisionRandom::EvaluateOdds()
extern void AIDecisionRandom_EvaluateOdds_m37E812CC7C36943284D68F47638F8F67C96D5A7E (void);
// 0x000000B8 System.Void MoreMountains.CorgiEngine.AIDecisionRandom::.ctor()
extern void AIDecisionRandom__ctor_m5F0D680D4C819AC82B5A775825D3ADF9EF1A4CA4 (void);
// 0x000000B9 System.Void MoreMountains.CorgiEngine.AIDecisionReloadNeeded::Initialization()
extern void AIDecisionReloadNeeded_Initialization_m65C8A79A03F2448A8CC4E94E9BE9C42E1A002B37 (void);
// 0x000000BA System.Boolean MoreMountains.CorgiEngine.AIDecisionReloadNeeded::Decide()
extern void AIDecisionReloadNeeded_Decide_mAD461D8D33DEEAF2A62AF36CC5B367BE37BCC2AA (void);
// 0x000000BB System.Void MoreMountains.CorgiEngine.AIDecisionReloadNeeded::.ctor()
extern void AIDecisionReloadNeeded__ctor_m018929BBC28231BF457BC0EFE6B0A1E245F85AD3 (void);
// 0x000000BC System.Boolean MoreMountains.CorgiEngine.AIDecisionTargetFacingAI::Decide()
extern void AIDecisionTargetFacingAI_Decide_mA794713C434AFDB665748DFE46BBA7FEAED900D4 (void);
// 0x000000BD System.Boolean MoreMountains.CorgiEngine.AIDecisionTargetFacingAI::EvaluateTargetFacingDirection()
extern void AIDecisionTargetFacingAI_EvaluateTargetFacingDirection_m8AA52BD991D7894A4EDFD9C3972D2E04644D9146 (void);
// 0x000000BE System.Void MoreMountains.CorgiEngine.AIDecisionTargetFacingAI::.ctor()
extern void AIDecisionTargetFacingAI__ctor_m339665929A0075E69ED47837789A93A3E61B4C4F (void);
// 0x000000BF System.Boolean MoreMountains.CorgiEngine.AIDecisionTargetIsAlive::Decide()
extern void AIDecisionTargetIsAlive_Decide_m517FE1CDD7AF55656E62B79C09119A08FCAF0CA4 (void);
// 0x000000C0 System.Boolean MoreMountains.CorgiEngine.AIDecisionTargetIsAlive::CheckIfTargetIsAlive()
extern void AIDecisionTargetIsAlive_CheckIfTargetIsAlive_mE3181A66B527CB60E75E01E68C46EBB1639A20CB (void);
// 0x000000C1 System.Void MoreMountains.CorgiEngine.AIDecisionTargetIsAlive::.ctor()
extern void AIDecisionTargetIsAlive__ctor_m690F0BAED7D4196D2C8B68800185125532687C5C (void);
// 0x000000C2 System.Boolean MoreMountains.CorgiEngine.AIDecisionTargetIsNull::Decide()
extern void AIDecisionTargetIsNull_Decide_mCE3BB32F87CDBB4914335910876D2F33968F7E73 (void);
// 0x000000C3 System.Boolean MoreMountains.CorgiEngine.AIDecisionTargetIsNull::CheckIfTargetIsNull()
extern void AIDecisionTargetIsNull_CheckIfTargetIsNull_m657AF9418D2E9D4ADDBC356A219C6A037EDF5892 (void);
// 0x000000C4 System.Void MoreMountains.CorgiEngine.AIDecisionTargetIsNull::.ctor()
extern void AIDecisionTargetIsNull__ctor_m26AE2354016519900A766C610EAE5C5D284EC217 (void);
// 0x000000C5 System.Boolean MoreMountains.CorgiEngine.AIDecisionTimeInState::Decide()
extern void AIDecisionTimeInState_Decide_m4D68D9AB4442EF3B07EFA4FDFCCD66B3CB376061 (void);
// 0x000000C6 System.Boolean MoreMountains.CorgiEngine.AIDecisionTimeInState::EvaluateTime()
extern void AIDecisionTimeInState_EvaluateTime_m82F03E0D569A646165E5B6B1739A80B2D1A9A53F (void);
// 0x000000C7 System.Void MoreMountains.CorgiEngine.AIDecisionTimeInState::Initialization()
extern void AIDecisionTimeInState_Initialization_m675AFA8D8FBFACB098ADA3CC164A5A56E6AAA2B2 (void);
// 0x000000C8 System.Void MoreMountains.CorgiEngine.AIDecisionTimeInState::OnEnterState()
extern void AIDecisionTimeInState_OnEnterState_m687551FB68240DEDEADCFF7C63CC0D1CD77A1909 (void);
// 0x000000C9 System.Void MoreMountains.CorgiEngine.AIDecisionTimeInState::RandomizeTime()
extern void AIDecisionTimeInState_RandomizeTime_m33222695F62C89609E63477B0BD8014784288DDC (void);
// 0x000000CA System.Void MoreMountains.CorgiEngine.AIDecisionTimeInState::.ctor()
extern void AIDecisionTimeInState__ctor_mB7ED16A17BCDD703E570B9599C97097874F7D364 (void);
// 0x000000CB System.Void MoreMountains.CorgiEngine.AIDecisionTimeSinceStart::Initialization()
extern void AIDecisionTimeSinceStart_Initialization_m3C8230404BF8852AC404100BCFD4A9EDEE6EA6C3 (void);
// 0x000000CC System.Boolean MoreMountains.CorgiEngine.AIDecisionTimeSinceStart::Decide()
extern void AIDecisionTimeSinceStart_Decide_m1728C0C83CD2A44DA0E774763C6001671294E0FE (void);
// 0x000000CD System.Boolean MoreMountains.CorgiEngine.AIDecisionTimeSinceStart::EvaluateTime()
extern void AIDecisionTimeSinceStart_EvaluateTime_mDDFB1EB68E187D0905D7AE4C660259A150F532F6 (void);
// 0x000000CE System.Void MoreMountains.CorgiEngine.AIDecisionTimeSinceStart::.ctor()
extern void AIDecisionTimeSinceStart__ctor_mFECBED3FD973C957D4F8502898B0A4E437D1D49D (void);
// 0x000000CF System.Void MoreMountains.CorgiEngine.PathedProjectile::Initialize(UnityEngine.Transform,System.Single)
extern void PathedProjectile_Initialize_m286B9D9604D07303E48145F582E0D74EEFD095E2 (void);
// 0x000000D0 System.Void MoreMountains.CorgiEngine.PathedProjectile::Update()
extern void PathedProjectile_Update_m36BA65CF8C67C71DA256FD314E7BBC1D1F8F4246 (void);
// 0x000000D1 System.Void MoreMountains.CorgiEngine.PathedProjectile::.ctor()
extern void PathedProjectile__ctor_mCF5BF2FFCFA3500996B284F01A540CC7C37D24E8 (void);
// 0x000000D2 System.Void MoreMountains.CorgiEngine.PathedProjectileSpawner::Start()
extern void PathedProjectileSpawner_Start_m23DB1C7024BBF36535CE0309E72B79E6B03CB9D8 (void);
// 0x000000D3 System.Void MoreMountains.CorgiEngine.PathedProjectileSpawner::Update()
extern void PathedProjectileSpawner_Update_m627EEC7379137D734D504E5009608F42337C9030 (void);
// 0x000000D4 System.Void MoreMountains.CorgiEngine.PathedProjectileSpawner::OnDrawGizmos()
extern void PathedProjectileSpawner_OnDrawGizmos_mDF492AB74E7C37E1C7B3B9C23E4F9A268D726A0F (void);
// 0x000000D5 System.Void MoreMountains.CorgiEngine.PathedProjectileSpawner::.ctor()
extern void PathedProjectileSpawner__ctor_mC0E7D872C7C0DD84C199E45DF818292ADB0BF010 (void);
// 0x000000D6 MoreMountains.Tools.MMObjectPooler MoreMountains.CorgiEngine.TimedSpawner::get_ObjectPooler()
extern void TimedSpawner_get_ObjectPooler_m260DF9B822E7790C098F47111EF9449F2BE6A44D (void);
// 0x000000D7 System.Void MoreMountains.CorgiEngine.TimedSpawner::set_ObjectPooler(MoreMountains.Tools.MMObjectPooler)
extern void TimedSpawner_set_ObjectPooler_mCB8F79F8F9924497AF822D7BFBF1CF480826F0A5 (void);
// 0x000000D8 System.Void MoreMountains.CorgiEngine.TimedSpawner::Start()
extern void TimedSpawner_Start_mBF687A6392FBC5116152B1C0DA85EF673A93036E (void);
// 0x000000D9 System.Void MoreMountains.CorgiEngine.TimedSpawner::Initialization()
extern void TimedSpawner_Initialization_mEBD31B42C765CD15F6EA96749133D307BAE1383D (void);
// 0x000000DA System.Void MoreMountains.CorgiEngine.TimedSpawner::Update()
extern void TimedSpawner_Update_mAF59551F52E1B1010F3F9801D600EEFF46BB0AE7 (void);
// 0x000000DB System.Void MoreMountains.CorgiEngine.TimedSpawner::Spawn()
extern void TimedSpawner_Spawn_m05E675915AB1898F5C36CF5A62E1A09FFA8FE668 (void);
// 0x000000DC System.Void MoreMountains.CorgiEngine.TimedSpawner::DetermineNextFrequency()
extern void TimedSpawner_DetermineNextFrequency_m3DD9B2E7ABA1A7E2F9C261E51F1E27BEF2A78B1D (void);
// 0x000000DD System.Void MoreMountains.CorgiEngine.TimedSpawner::.ctor()
extern void TimedSpawner__ctor_m2F705AEDCC7EF00BCF7497FFEC8C7528E412E160 (void);
// 0x000000DE System.Boolean MoreMountains.CorgiEngine.AIFollow::get_AgentFollowsPlayer()
extern void AIFollow_get_AgentFollowsPlayer_mDDEAACF81F993DCC02AB7EDAC15B2B3E9302E601 (void);
// 0x000000DF System.Void MoreMountains.CorgiEngine.AIFollow::set_AgentFollowsPlayer(System.Boolean)
extern void AIFollow_set_AgentFollowsPlayer_m752F008DEF41D0E5B5CF55818CE0379FA74E3B7B (void);
// 0x000000E0 System.Void MoreMountains.CorgiEngine.AIFollow::Initialization()
extern void AIFollow_Initialization_mF8DAB601C6F466E5C68066351146F546137917D3 (void);
// 0x000000E1 System.Void MoreMountains.CorgiEngine.AIFollow::Update()
extern void AIFollow_Update_mE63D1A8CE941C6DD29C22665CCF35C9D5D757964 (void);
// 0x000000E2 System.Void MoreMountains.CorgiEngine.AIFollow::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void AIFollow_OnMMEvent_m97292B4727CB4A7CDBAB61F4545BB7A46A0ADC2C (void);
// 0x000000E3 System.Void MoreMountains.CorgiEngine.AIFollow::OnEnable()
extern void AIFollow_OnEnable_mC45B51C7F6511573AC1F0CA97D1BC13DE4916376 (void);
// 0x000000E4 System.Void MoreMountains.CorgiEngine.AIFollow::OnDisable()
extern void AIFollow_OnDisable_m3D5AC16B38E6FF17F107421D49971C3D2AD97480 (void);
// 0x000000E5 System.Void MoreMountains.CorgiEngine.AIFollow::.ctor()
extern void AIFollow__ctor_m47E23AC2D5EFFDC9B0AA61F136D6F7AF62CD2D05 (void);
// 0x000000E6 System.Void MoreMountains.CorgiEngine.AIShootOnSight::Start()
extern void AIShootOnSight_Start_mE206B543D23692673F71A0603F8D96E9769F2FD6 (void);
// 0x000000E7 System.Void MoreMountains.CorgiEngine.AIShootOnSight::Update()
extern void AIShootOnSight_Update_mB129AF4A105C0211DF80D8EFAE998CAD7BC13274 (void);
// 0x000000E8 System.Void MoreMountains.CorgiEngine.AIShootOnSight::.ctor()
extern void AIShootOnSight__ctor_m2C6C7BAA2F0A4AD4BF149E8D1438229CB3CF9BCC (void);
// 0x000000E9 System.Void MoreMountains.CorgiEngine.AIWalk::Start()
extern void AIWalk_Start_m61B503C611B68C408641EDC48DA2F95CE1F46B88 (void);
// 0x000000EA System.Void MoreMountains.CorgiEngine.AIWalk::Initialization()
extern void AIWalk_Initialization_m7A3DB44DB3C683F6F5D7E48C81B3DB920266EB34 (void);
// 0x000000EB System.Void MoreMountains.CorgiEngine.AIWalk::Update()
extern void AIWalk_Update_m42A3ACC189A03E8DBE1971624514F40D63C63A85 (void);
// 0x000000EC System.Void MoreMountains.CorgiEngine.AIWalk::CheckForWalls()
extern void AIWalk_CheckForWalls_mF054C26678EC50EF5CBCE3ADD2E95862B0355BC8 (void);
// 0x000000ED System.Void MoreMountains.CorgiEngine.AIWalk::CheckForHoles()
extern void AIWalk_CheckForHoles_m8DF575ED030E2ABEB3337A86C502F99F37281872 (void);
// 0x000000EE System.Void MoreMountains.CorgiEngine.AIWalk::CheckForTarget()
extern void AIWalk_CheckForTarget_m84202716520E3FA771C7FB4FCC1C64EF7075F00E (void);
// 0x000000EF System.Void MoreMountains.CorgiEngine.AIWalk::ChangeDirection()
extern void AIWalk_ChangeDirection_m3765D91C267E74B78F7D26051B1278840B2ADDCC (void);
// 0x000000F0 System.Void MoreMountains.CorgiEngine.AIWalk::OnRevive()
extern void AIWalk_OnRevive_m63ADD8CACBC0B95AA6AB704F278075C4C839D836 (void);
// 0x000000F1 System.Void MoreMountains.CorgiEngine.AIWalk::OnEnable()
extern void AIWalk_OnEnable_m96CA5DF13066A28B1C958407F97436F09C4249CD (void);
// 0x000000F2 System.Void MoreMountains.CorgiEngine.AIWalk::OnDisable()
extern void AIWalk_OnDisable_m7DD7372366E24696935C4E7BA0213E4848921687 (void);
// 0x000000F3 System.Void MoreMountains.CorgiEngine.AIWalk::.ctor()
extern void AIWalk__ctor_mAFB38AE548693879517DA9331C1388E0442667D3 (void);
// 0x000000F4 System.Void MoreMountains.CorgiEngine.CorgiEngineCharacterAnimationParameter::.ctor(System.String,UnityEngine.AnimatorControllerParameterType)
extern void CorgiEngineCharacterAnimationParameter__ctor_m422C90AF2970DD2AB766173152F7E3962D1A22CD (void);
// 0x000000F5 System.Void MoreMountains.CorgiEngine.CharacterAnimationParametersInitializer::AddAnimationParameters()
extern void CharacterAnimationParametersInitializer_AddAnimationParameters_mEA7327F02AE35760B42C1684807EFE916DA67F3C (void);
// 0x000000F6 System.Void MoreMountains.CorgiEngine.CharacterAnimationParametersInitializer::.ctor()
extern void CharacterAnimationParametersInitializer__ctor_mDBEFFF0C37C380D9F65FFAB544F7FB19C7124BD3 (void);
// 0x000000F7 System.Boolean MoreMountains.CorgiEngine.CharacterAbility::get_AbilityAuthorized()
extern void CharacterAbility_get_AbilityAuthorized_m6D8E46BB303E61CECEF24F22B57E5A6062FEE97C (void);
// 0x000000F8 System.Boolean MoreMountains.CorgiEngine.CharacterAbility::get_AbilityInitialized()
extern void CharacterAbility_get_AbilityInitialized_mF5F68AAEFC8E00BE052230EA64C2EE90DBFD8260 (void);
// 0x000000F9 System.String MoreMountains.CorgiEngine.CharacterAbility::HelpBoxText()
extern void CharacterAbility_HelpBoxText_m573A43433F68340CC60D275CBCC5F0CA44257F64 (void);
// 0x000000FA System.Void MoreMountains.CorgiEngine.CharacterAbility::Start()
extern void CharacterAbility_Start_m51D4254F0184216EDA5BCBDE7681CA739D88B370 (void);
// 0x000000FB System.Void MoreMountains.CorgiEngine.CharacterAbility::Initialization()
extern void CharacterAbility_Initialization_m490E4A987E5686DAE37ADCEA55ADC474E3F91BDC (void);
// 0x000000FC System.Void MoreMountains.CorgiEngine.CharacterAbility::SetInputManager(MoreMountains.CorgiEngine.InputManager)
extern void CharacterAbility_SetInputManager_mD13AAFBF42600ED72C6A60059D869FEA2CDB425A (void);
// 0x000000FD System.Void MoreMountains.CorgiEngine.CharacterAbility::BindAnimator()
extern void CharacterAbility_BindAnimator_m5CB2D2F76EFCEAE696D1EC2D994F9586E3E464FA (void);
// 0x000000FE System.Void MoreMountains.CorgiEngine.CharacterAbility::InitializeAnimatorParameters()
extern void CharacterAbility_InitializeAnimatorParameters_mB8A51C1A2CABB4F0B8627722A7033D50DD8BCA32 (void);
// 0x000000FF System.Void MoreMountains.CorgiEngine.CharacterAbility::InternalHandleInput()
extern void CharacterAbility_InternalHandleInput_mA0B9113DEE10C9E8AA1B7F0282CCA40F3476F768 (void);
// 0x00000100 System.Void MoreMountains.CorgiEngine.CharacterAbility::HandleInput()
extern void CharacterAbility_HandleInput_m860A4A1D587CFA497E2483FEB32A35AE87C75F1A (void);
// 0x00000101 System.Void MoreMountains.CorgiEngine.CharacterAbility::ResetInput()
extern void CharacterAbility_ResetInput_m9FB42C43CC204C14A3C7282548280DB221D10A73 (void);
// 0x00000102 System.Void MoreMountains.CorgiEngine.CharacterAbility::EarlyProcessAbility()
extern void CharacterAbility_EarlyProcessAbility_m8F3E1203129AA4E405E8EC89C7A203EFDA125982 (void);
// 0x00000103 System.Void MoreMountains.CorgiEngine.CharacterAbility::ProcessAbility()
extern void CharacterAbility_ProcessAbility_m665947A1A59C3538FAD48919DE6A17CE62A28D6D (void);
// 0x00000104 System.Void MoreMountains.CorgiEngine.CharacterAbility::LateProcessAbility()
extern void CharacterAbility_LateProcessAbility_mF966F8941ADFF4238E62A5C60D2E8CF04C3D0DA7 (void);
// 0x00000105 System.Void MoreMountains.CorgiEngine.CharacterAbility::UpdateAnimator()
extern void CharacterAbility_UpdateAnimator_mC5DC003C2A7A38A1F4E2B6C0E8080B35A591079D (void);
// 0x00000106 System.Void MoreMountains.CorgiEngine.CharacterAbility::PermitAbility(System.Boolean)
extern void CharacterAbility_PermitAbility_mC249DBB3FD92DC52570CBD5C7170C11F089CF17C (void);
// 0x00000107 System.Void MoreMountains.CorgiEngine.CharacterAbility::Flip()
extern void CharacterAbility_Flip_m442B84D94E988B2A43A13EB2905E5DE419D51E2E (void);
// 0x00000108 System.Void MoreMountains.CorgiEngine.CharacterAbility::ResetAbility()
extern void CharacterAbility_ResetAbility_m5C3A72E516E4EBA11CC297E1FF4B95454E0D365C (void);
// 0x00000109 System.Void MoreMountains.CorgiEngine.CharacterAbility::PlayAbilityStartFeedbacks()
extern void CharacterAbility_PlayAbilityStartFeedbacks_mE39CA8B614996220E544DA2652E587A938BB8431 (void);
// 0x0000010A System.Void MoreMountains.CorgiEngine.CharacterAbility::StopStartFeedbacks()
extern void CharacterAbility_StopStartFeedbacks_mF309B9E16F031B8C241EB67C9CABA512249F73D4 (void);
// 0x0000010B System.Void MoreMountains.CorgiEngine.CharacterAbility::PlayAbilityStopFeedbacks()
extern void CharacterAbility_PlayAbilityStopFeedbacks_mF4C85DAD8631DFC946607161629677B80A039B61 (void);
// 0x0000010C System.Void MoreMountains.CorgiEngine.CharacterAbility::RegisterAnimatorParameter(System.String,UnityEngine.AnimatorControllerParameterType,System.Int32&)
extern void CharacterAbility_RegisterAnimatorParameter_m4CDAC37CE773AA77A9473687C913BCD048A9B94D (void);
// 0x0000010D System.Void MoreMountains.CorgiEngine.CharacterAbility::OnRespawn()
extern void CharacterAbility_OnRespawn_m12A1D21893D7959E9002A058F9ED523D267EC988 (void);
// 0x0000010E System.Void MoreMountains.CorgiEngine.CharacterAbility::OnDeath()
extern void CharacterAbility_OnDeath_m3FB727C8B5DE2DD9DF8C55F66931C39B8621C586 (void);
// 0x0000010F System.Void MoreMountains.CorgiEngine.CharacterAbility::OnHit()
extern void CharacterAbility_OnHit_m80761A1EFB5160C474E1D5E4EE2E776CA105AD6E (void);
// 0x00000110 System.Void MoreMountains.CorgiEngine.CharacterAbility::OnEnable()
extern void CharacterAbility_OnEnable_m610BC1FF2F7AF135A13B8B72ED0459BDF07238AA (void);
// 0x00000111 System.Void MoreMountains.CorgiEngine.CharacterAbility::OnDisable()
extern void CharacterAbility_OnDisable_m6DC24D5A2273487BA70F255BFA8DF3E7830341D8 (void);
// 0x00000112 System.Void MoreMountains.CorgiEngine.CharacterAbility::.ctor()
extern void CharacterAbility__ctor_m99A9BC4D760B415E996417AB4658940BF317D8A7 (void);
// 0x00000113 System.Void MoreMountains.CorgiEngine.CharacterAbilityNodeSwap::HandleInput()
extern void CharacterAbilityNodeSwap_HandleInput_m6647AB9323AAD2BABF34FACAD2648ADFBF4DF5BC (void);
// 0x00000114 System.Void MoreMountains.CorgiEngine.CharacterAbilityNodeSwap::Swap()
extern void CharacterAbilityNodeSwap_Swap_m70DB8A13329F42DC6E3B8E4CD5024C36BB9F1654 (void);
// 0x00000115 System.Void MoreMountains.CorgiEngine.CharacterAbilityNodeSwap::SwapAbilityNodes()
extern void CharacterAbilityNodeSwap_SwapAbilityNodes_m61CCDD35231A0073A1396ED87A9A62DA213AAD10 (void);
// 0x00000116 System.Void MoreMountains.CorgiEngine.CharacterAbilityNodeSwap::.ctor()
extern void CharacterAbilityNodeSwap__ctor_m42DFF85B6203059408FEED9EA2E87B15B3C75E98 (void);
// 0x00000117 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::Initialization()
extern void CharacterAutoMovement_Initialization_mF7D1C5B50FE344AAF634DA7C54F7B2AA49ED0152 (void);
// 0x00000118 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::HandleInput()
extern void CharacterAutoMovement_HandleInput_m01B235C642D2662C04A5EBF78F39B8C33B77B60E (void);
// 0x00000119 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::ProcessAbility()
extern void CharacterAutoMovement_ProcessAbility_mDF03BA0A8E36963B9DAB90F1AA66F8478F0D56C1 (void);
// 0x0000011A System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::TestChangeDirectionOnWallCollision()
extern void CharacterAutoMovement_TestChangeDirectionOnWallCollision_m2B04FB56D2A7805E09622FD6460CC30F9E566716 (void);
// 0x0000011B System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::TestChangeDirectionOnFallingDown()
extern void CharacterAutoMovement_TestChangeDirectionOnFallingDown_m0D53C3D84367C917FC16F5B594C13182CB5C53EA (void);
// 0x0000011C System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::LateProcessAbility()
extern void CharacterAutoMovement_LateProcessAbility_m3D5E1011FED38FE02AF371BC149FE0AF1B7AF22A (void);
// 0x0000011D System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::OnWallJump()
extern void CharacterAutoMovement_OnWallJump_mDE684CD0AB44D01E632C4F4334837CD57E78C327 (void);
// 0x0000011E System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::PauseMovement()
extern void CharacterAutoMovement_PauseMovement_mE3D2B9B571C63AE9B9FCAE4EFE9DE951AC61B0BB (void);
// 0x0000011F System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::ResumeMovement()
extern void CharacterAutoMovement_ResumeMovement_m52AF6BB2D69214DBEBE5DC965074C5AB04CF9331 (void);
// 0x00000120 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::SetDrivenInput(System.Single)
extern void CharacterAutoMovement_SetDrivenInput_m9C0573626177CE3F29F6B3F8B4D638954955F2E1 (void);
// 0x00000121 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::ChangeDirection()
extern void CharacterAutoMovement_ChangeDirection_m3D62572EBAD155BC7DA75CEAAF8C00D4DED0A7B3 (void);
// 0x00000122 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::ForceDirection(System.Single)
extern void CharacterAutoMovement_ForceDirection_m4739A4BDA29D08FBCD00F3665FFA0FA2CD876FAA (void);
// 0x00000123 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::ToggleRun()
extern void CharacterAutoMovement_ToggleRun_m0B837FB4353DE48A36EB735D9E88DCA229CCEE9A (void);
// 0x00000124 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::ForceRun(System.Boolean)
extern void CharacterAutoMovement_ForceRun_m4ECB2A335252E1D4F1D36D7347136741ECB53D6C (void);
// 0x00000125 System.Void MoreMountains.CorgiEngine.CharacterAutoMovement::.ctor()
extern void CharacterAutoMovement__ctor_m1EC35D0CF233BD2DFC11DE8B3871F4C05552C945 (void);
// 0x00000126 System.Void MoreMountains.CorgiEngine.CharacterBounce::Initialization()
extern void CharacterBounce_Initialization_m398D0572A22C86C35EFFC6EFBC19B687FE7DF44E (void);
// 0x00000127 System.Void MoreMountains.CorgiEngine.CharacterBounce::LateProcessAbility()
extern void CharacterBounce_LateProcessAbility_m781C67BB9ECE546FE846ADC1B58077B4BB500153 (void);
// 0x00000128 System.Void MoreMountains.CorgiEngine.CharacterBounce::.ctor()
extern void CharacterBounce__ctor_mFAECA713DC1AA3255EC48B4B8016B78D356C6BA8 (void);
// 0x00000129 System.String MoreMountains.CorgiEngine.CharacterButtonActivation::HelpBoxText()
extern void CharacterButtonActivation_HelpBoxText_m5422705A527F628120DF9EEEB017789184ECEC0B (void);
// 0x0000012A System.Boolean MoreMountains.CorgiEngine.CharacterButtonActivation::get_InButtonActivatedZone()
extern void CharacterButtonActivation_get_InButtonActivatedZone_mA07B6E4ED64118FA0FBDAF139DA1E11CA2D1219D (void);
// 0x0000012B System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::set_InButtonActivatedZone(System.Boolean)
extern void CharacterButtonActivation_set_InButtonActivatedZone_m0A740C560AB575C78ED3549F0D2C378D0537D46A (void);
// 0x0000012C System.Boolean MoreMountains.CorgiEngine.CharacterButtonActivation::get_InButtonAutoActivatedZone()
extern void CharacterButtonActivation_get_InButtonAutoActivatedZone_m6EB06564BB1067EF3ADA23F4CDD85E9D7D3BB2F8 (void);
// 0x0000012D System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::set_InButtonAutoActivatedZone(System.Boolean)
extern void CharacterButtonActivation_set_InButtonAutoActivatedZone_m9E58139C6B87843B92DC6F4AE7B0B32BF1AD672A (void);
// 0x0000012E System.Boolean MoreMountains.CorgiEngine.CharacterButtonActivation::get_InJumpPreventingZone()
extern void CharacterButtonActivation_get_InJumpPreventingZone_m688CF6705E6CFB3058DF219CA6302DE343248DF7 (void);
// 0x0000012F System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::set_InJumpPreventingZone(System.Boolean)
extern void CharacterButtonActivation_set_InJumpPreventingZone_m9EC4851E01A7F49E4EE9873B668D426D8F3720FD (void);
// 0x00000130 MoreMountains.CorgiEngine.ButtonActivated MoreMountains.CorgiEngine.CharacterButtonActivation::get_ButtonActivatedZone()
extern void CharacterButtonActivation_get_ButtonActivatedZone_m54156CA22EF33E38130DBDE8A3FC57682A2FB2EE (void);
// 0x00000131 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::set_ButtonActivatedZone(MoreMountains.CorgiEngine.ButtonActivated)
extern void CharacterButtonActivation_set_ButtonActivatedZone_m88D200B6E768D14A4C23CD2EE9567D4327EEAA15 (void);
// 0x00000132 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::Initialization()
extern void CharacterButtonActivation_Initialization_m61F2D5A6AE770E89A7B8A9F5DBFF5EC185B08A41 (void);
// 0x00000133 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::HandleInput()
extern void CharacterButtonActivation_HandleInput_m13923B2931B8F5DDD4F4A1E53D2C0CC88196BAE6 (void);
// 0x00000134 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::ButtonActivation()
extern void CharacterButtonActivation_ButtonActivation_m273F309184BED996588A49D73CA89EB094D90D09 (void);
// 0x00000135 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::OnDeath()
extern void CharacterButtonActivation_OnDeath_mA02AF58E27DF67CC5BC6CD43CB60743B89651EFA (void);
// 0x00000136 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::InitializeAnimatorParameters()
extern void CharacterButtonActivation_InitializeAnimatorParameters_m5D4106279F6AE6F1ED597B90B853769FEBAA6D50 (void);
// 0x00000137 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::UpdateAnimator()
extern void CharacterButtonActivation_UpdateAnimator_m1A857FAC90F4D41F3541BC96C98B1897C5F68D79 (void);
// 0x00000138 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::SetTriggerParameter()
extern void CharacterButtonActivation_SetTriggerParameter_m12F48ED86B6F2B4E267BA4C12A9F23DB8949E0E9 (void);
// 0x00000139 System.Void MoreMountains.CorgiEngine.CharacterButtonActivation::.ctor()
extern void CharacterButtonActivation__ctor_m5AD31E4DEF8952F9AA75EB29F934B5C125851204 (void);
// 0x0000013A System.String MoreMountains.CorgiEngine.CharacterCrouch::HelpBoxText()
extern void CharacterCrouch_HelpBoxText_m13BB2EFE12DB144068C0B760B6B5B4CEC3F1FC2E (void);
// 0x0000013B System.Void MoreMountains.CorgiEngine.CharacterCrouch::Initialization()
extern void CharacterCrouch_Initialization_mE7F48D3EE762F851DC25CF7ECC847D3030EAE493 (void);
// 0x0000013C System.Void MoreMountains.CorgiEngine.CharacterCrouch::ProcessAbility()
extern void CharacterCrouch_ProcessAbility_m63D6E17BF31D424300692921ED1A02A045860679 (void);
// 0x0000013D System.Void MoreMountains.CorgiEngine.CharacterCrouch::HandleInput()
extern void CharacterCrouch_HandleInput_m335C8224B073AB63C888BF26ACE1BCCD878E1C82 (void);
// 0x0000013E System.Void MoreMountains.CorgiEngine.CharacterCrouch::Crouch()
extern void CharacterCrouch_Crouch_mADC6904A06AC3B43E30BB5DD42ECD85F68A84D69 (void);
// 0x0000013F System.Void MoreMountains.CorgiEngine.CharacterCrouch::DetermineState()
extern void CharacterCrouch_DetermineState_mDCDEC3601532BDC3F12852617CDFE14B930FBCCD (void);
// 0x00000140 System.Void MoreMountains.CorgiEngine.CharacterCrouch::CheckExitCrouch()
extern void CharacterCrouch_CheckExitCrouch_m9D8D30F6DE8981B66C6B722292D9F3710868BB3D (void);
// 0x00000141 System.Void MoreMountains.CorgiEngine.CharacterCrouch::ExitCrouch()
extern void CharacterCrouch_ExitCrouch_m5CA5DD9F0B2D320BC818FFD0A6F2107F4DA5FB28 (void);
// 0x00000142 System.Void MoreMountains.CorgiEngine.CharacterCrouch::InitializeAnimatorParameters()
extern void CharacterCrouch_InitializeAnimatorParameters_mE98D1FF068E60727DB7A03B604EC7F4A9395EE6E (void);
// 0x00000143 System.Void MoreMountains.CorgiEngine.CharacterCrouch::UpdateAnimator()
extern void CharacterCrouch_UpdateAnimator_m24CC60D7F1346270008F94E684BF929ED139C72A (void);
// 0x00000144 System.Void MoreMountains.CorgiEngine.CharacterCrouch::RecalculateRays()
extern void CharacterCrouch_RecalculateRays_m56FE03D0129636C9512DD3AD82CA3F9EC412CC72 (void);
// 0x00000145 System.Void MoreMountains.CorgiEngine.CharacterCrouch::ResetAbility()
extern void CharacterCrouch_ResetAbility_m5ED490CAB9297BFC1B8B578D5B158870D078E577 (void);
// 0x00000146 System.Void MoreMountains.CorgiEngine.CharacterCrouch::.ctor()
extern void CharacterCrouch__ctor_m57914E52979905DDA2E7D7F3D67EB687F5575F9C (void);
// 0x00000147 System.String MoreMountains.CorgiEngine.CharacterCrushDetection::HelpBoxText()
extern void CharacterCrushDetection_HelpBoxText_mDACB59C4EF3F79376F855FBCED836C54D7957D21 (void);
// 0x00000148 System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::Initialization()
extern void CharacterCrushDetection_Initialization_mBC4F1D984B4787DEB6998F7FD4DE461C277AC8CA (void);
// 0x00000149 System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::ProcessAbility()
extern void CharacterCrushDetection_ProcessAbility_m3777A446291567F78B98BD5A6D1DD13757A744BF (void);
// 0x0000014A System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::DetectCrush()
extern void CharacterCrushDetection_DetectCrush_mB61FB0EBB0BC62E2A8A5492B9AACC169ED94E2DC (void);
// 0x0000014B System.Boolean MoreMountains.CorgiEngine.CharacterCrushDetection::DetectionRay(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void CharacterCrushDetection_DetectionRay_mDE8C44360E69E8A7CB9F5BEA9F18E20E0C7451BD (void);
// 0x0000014C System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::ApplyCrush()
extern void CharacterCrushDetection_ApplyCrush_m8505E156F294D6AA96DD43ADDD664BAAC2F80235 (void);
// 0x0000014D System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::InitializeAnimatorParameters()
extern void CharacterCrushDetection_InitializeAnimatorParameters_m35AD24F51FF4CE95566EBA59A33F03D2939BE824 (void);
// 0x0000014E System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::UpdateAnimator()
extern void CharacterCrushDetection_UpdateAnimator_mF2FFF4FEB64A410FD057A6A611FC552E385640BB (void);
// 0x0000014F System.Void MoreMountains.CorgiEngine.CharacterCrushDetection::.ctor()
extern void CharacterCrushDetection__ctor_m271FB1C941F03392EA3AAD6EBFCC2341E0F62039 (void);
// 0x00000150 System.Void MoreMountains.CorgiEngine.CharacterDamageDash::Initialization()
extern void CharacterDamageDash_Initialization_m0D189DF3770CFCA87BE658C36B6A18E74E9662E8 (void);
// 0x00000151 System.Void MoreMountains.CorgiEngine.CharacterDamageDash::InitiateDash()
extern void CharacterDamageDash_InitiateDash_mC9F00796333C9E6815AFC555E9812F391BAAB3D8 (void);
// 0x00000152 System.Void MoreMountains.CorgiEngine.CharacterDamageDash::StopDash()
extern void CharacterDamageDash_StopDash_m591306CC0171475E741DD16A392CB29E53E86659 (void);
// 0x00000153 System.Void MoreMountains.CorgiEngine.CharacterDamageDash::.ctor()
extern void CharacterDamageDash__ctor_m2FFEFE466D8A83CE45AC9ED63C3076395BB1BE95 (void);
// 0x00000154 System.String MoreMountains.CorgiEngine.CharacterDangling::HelpBoxText()
extern void CharacterDangling_HelpBoxText_m65DD3A94DCC99C1DC187303B2B777116C6B44FC1 (void);
// 0x00000155 System.Void MoreMountains.CorgiEngine.CharacterDangling::ProcessAbility()
extern void CharacterDangling_ProcessAbility_m93F0ECAC6DE8D911B0A703D2C965170571D38287 (void);
// 0x00000156 System.Void MoreMountains.CorgiEngine.CharacterDangling::Dangling()
extern void CharacterDangling_Dangling_mAC0DCFD4B26DB8B7C5534F021BCEAEE5BCFB2771 (void);
// 0x00000157 System.Void MoreMountains.CorgiEngine.CharacterDangling::InitializeAnimatorParameters()
extern void CharacterDangling_InitializeAnimatorParameters_mC3EDBB0AB8C1173F99CF738DE528AC30ABCA9D24 (void);
// 0x00000158 System.Void MoreMountains.CorgiEngine.CharacterDangling::UpdateAnimator()
extern void CharacterDangling_UpdateAnimator_m1CC54A355E46B719E9CF021010FE51316C3783BE (void);
// 0x00000159 System.Void MoreMountains.CorgiEngine.CharacterDangling::ResetAbility()
extern void CharacterDangling_ResetAbility_m9BCE05C08616E258F2F61B220F3241C6FD933202 (void);
// 0x0000015A System.Void MoreMountains.CorgiEngine.CharacterDangling::.ctor()
extern void CharacterDangling__ctor_m21F426898D038B80209A79A2E567890676493554 (void);
// 0x0000015B System.String MoreMountains.CorgiEngine.CharacterDash::HelpBoxText()
extern void CharacterDash_HelpBoxText_m3E4C8FFA5D999FA65B1E1FEE4F1CB95EA363C0DF (void);
// 0x0000015C System.Void MoreMountains.CorgiEngine.CharacterDash::Initialization()
extern void CharacterDash_Initialization_m0C25D1D3EE6ED3BF85A7F63813FF2E6392706A7A (void);
// 0x0000015D System.Void MoreMountains.CorgiEngine.CharacterDash::HandleInput()
extern void CharacterDash_HandleInput_m0C2D30677CFDC36D11E6C110ABA87447C389FF73 (void);
// 0x0000015E System.Void MoreMountains.CorgiEngine.CharacterDash::ProcessAbility()
extern void CharacterDash_ProcessAbility_m9752B45DBAC3A7E787A19443E36EFA5407FD38B4 (void);
// 0x0000015F System.Void MoreMountains.CorgiEngine.CharacterDash::StartDash()
extern void CharacterDash_StartDash_m3FA54EE6A34E8D5781513164BF4BDA9815F64AF6 (void);
// 0x00000160 System.Boolean MoreMountains.CorgiEngine.CharacterDash::DashConditions()
extern void CharacterDash_DashConditions_m7C4136CB51F7856556266CC49EF11C78B56E2A71 (void);
// 0x00000161 System.Void MoreMountains.CorgiEngine.CharacterDash::HandleAmountOfDashesLeft()
extern void CharacterDash_HandleAmountOfDashesLeft_mD4BBAA877887C92C13070F72974038FCD1759DB9 (void);
// 0x00000162 System.Void MoreMountains.CorgiEngine.CharacterDash::SetSuccessiveDashesLeft(System.Int32)
extern void CharacterDash_SetSuccessiveDashesLeft_mB3BD0FDC53FCDD58EFA889E9D78F576DB8B5670C (void);
// 0x00000163 System.Boolean MoreMountains.CorgiEngine.CharacterDash::DashAuthorized()
extern void CharacterDash_DashAuthorized_m78F87B06D7E44A2C669CD8E37F0D190729B26D7E (void);
// 0x00000164 System.Void MoreMountains.CorgiEngine.CharacterDash::InitiateDash()
extern void CharacterDash_InitiateDash_m592A15CC605CDE9B51E5EA8131FF210242D4570F (void);
// 0x00000165 System.Void MoreMountains.CorgiEngine.CharacterDash::ComputeDashDirection()
extern void CharacterDash_ComputeDashDirection_mFB8253A2D224AAC407D425EAEFA1A2D9E009F54B (void);
// 0x00000166 System.Void MoreMountains.CorgiEngine.CharacterDash::CheckAutoCorrectTrajectory()
extern void CharacterDash_CheckAutoCorrectTrajectory_mBA40E31851690BB9E14C8E56C06557CA6D12874C (void);
// 0x00000167 System.Void MoreMountains.CorgiEngine.CharacterDash::CheckFlipCharacter()
extern void CharacterDash_CheckFlipCharacter_m1390A069D4E3C76A3141EA4D039A03AC2D19A164 (void);
// 0x00000168 System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterDash::Dash()
extern void CharacterDash_Dash_mDB12F06669BE888C9B633AF9CBDF10560ECD4C57 (void);
// 0x00000169 System.Void MoreMountains.CorgiEngine.CharacterDash::StopDash()
extern void CharacterDash_StopDash_m686CAD4EF59D521633BA9CDFAF186EAFE6272A45 (void);
// 0x0000016A System.Void MoreMountains.CorgiEngine.CharacterDash::InitializeAnimatorParameters()
extern void CharacterDash_InitializeAnimatorParameters_m07F282AFA331F6D52CB2013CC6ABDB4C0B847CDE (void);
// 0x0000016B System.Void MoreMountains.CorgiEngine.CharacterDash::UpdateAnimator()
extern void CharacterDash_UpdateAnimator_m71E4CCB56014B24A9089386A9AD50A19E532E4B9 (void);
// 0x0000016C System.Void MoreMountains.CorgiEngine.CharacterDash::ResetAbility()
extern void CharacterDash_ResetAbility_m4CF376C14167091994AB4010609B80539325B202 (void);
// 0x0000016D System.Void MoreMountains.CorgiEngine.CharacterDash::.ctor()
extern void CharacterDash__ctor_m7AC99131421EA3C5A8A49EA307C34E028AB1A8A5 (void);
// 0x0000016E System.Void MoreMountains.CorgiEngine.CharacterDash/<Dash>d__41::.ctor(System.Int32)
extern void U3CDashU3Ed__41__ctor_mED415C8629EB908093609AE9CFA90AC7BF650D72 (void);
// 0x0000016F System.Void MoreMountains.CorgiEngine.CharacterDash/<Dash>d__41::System.IDisposable.Dispose()
extern void U3CDashU3Ed__41_System_IDisposable_Dispose_mB0B9BE14C0DB18EEE122E9E1ECCCCD786DD5D918 (void);
// 0x00000170 System.Boolean MoreMountains.CorgiEngine.CharacterDash/<Dash>d__41::MoveNext()
extern void U3CDashU3Ed__41_MoveNext_mFF07F108A4FC657FC8FEF6D7FE55B7D1A5472C51 (void);
// 0x00000171 System.Object MoreMountains.CorgiEngine.CharacterDash/<Dash>d__41::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDashU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE29C5A8BA912DFD2BA2EB58C64429FC4B89D8C2 (void);
// 0x00000172 System.Void MoreMountains.CorgiEngine.CharacterDash/<Dash>d__41::System.Collections.IEnumerator.Reset()
extern void U3CDashU3Ed__41_System_Collections_IEnumerator_Reset_mE7BA17A6E71F2A23C4622C39FCC42262F3A0788B (void);
// 0x00000173 System.Object MoreMountains.CorgiEngine.CharacterDash/<Dash>d__41::System.Collections.IEnumerator.get_Current()
extern void U3CDashU3Ed__41_System_Collections_IEnumerator_get_Current_m93173A94EB2B7CFCE72B3661A88BBB1DAC7310DD (void);
// 0x00000174 System.String MoreMountains.CorgiEngine.CharacterDive::HelpBoxText()
extern void CharacterDive_HelpBoxText_mE7DB54DC682FE886CA93A559986DF6077E729D56 (void);
// 0x00000175 System.Void MoreMountains.CorgiEngine.CharacterDive::HandleInput()
extern void CharacterDive_HandleInput_mA3B897680EB5C26BA4EE17D8DF8F39E7149FD48F (void);
// 0x00000176 System.Void MoreMountains.CorgiEngine.CharacterDive::InitiateDive()
extern void CharacterDive_InitiateDive_mD8010823BC3D5568E6D6B7D78AEFD3F87AE33C4C (void);
// 0x00000177 System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterDive::Dive()
extern void CharacterDive_Dive_mAA6DD4F76350EF401684E6309E6CAA645D6FEDA7 (void);
// 0x00000178 System.Void MoreMountains.CorgiEngine.CharacterDive::InitializeAnimatorParameters()
extern void CharacterDive_InitializeAnimatorParameters_mB4E271DF7F9601FCFFF9AFC54BE3FB1D325496E8 (void);
// 0x00000179 System.Void MoreMountains.CorgiEngine.CharacterDive::UpdateAnimator()
extern void CharacterDive_UpdateAnimator_m2ACA128914877109E85938F0CA6953B06E9BF08E (void);
// 0x0000017A System.Void MoreMountains.CorgiEngine.CharacterDive::.ctor()
extern void CharacterDive__ctor_m4A6807F3784293C263F2F20FB6FF62EC8FFB8CF8 (void);
// 0x0000017B System.Void MoreMountains.CorgiEngine.CharacterDive/<Dive>d__7::.ctor(System.Int32)
extern void U3CDiveU3Ed__7__ctor_mB3391F3DB2B4C091422BC6F211B99D80772DF112 (void);
// 0x0000017C System.Void MoreMountains.CorgiEngine.CharacterDive/<Dive>d__7::System.IDisposable.Dispose()
extern void U3CDiveU3Ed__7_System_IDisposable_Dispose_m22C8FD7C5A78567E92271987F5C7DCA2A843F5F9 (void);
// 0x0000017D System.Boolean MoreMountains.CorgiEngine.CharacterDive/<Dive>d__7::MoveNext()
extern void U3CDiveU3Ed__7_MoveNext_mC25AF638F3DACFF8A6E103C642BC22226F3F3EC3 (void);
// 0x0000017E System.Object MoreMountains.CorgiEngine.CharacterDive/<Dive>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDiveU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00BAA6EDBD4080413693E3CCDE2858595D8112E7 (void);
// 0x0000017F System.Void MoreMountains.CorgiEngine.CharacterDive/<Dive>d__7::System.Collections.IEnumerator.Reset()
extern void U3CDiveU3Ed__7_System_Collections_IEnumerator_Reset_m09751C9E3137D2C683419F572F8D641C4E0792DE (void);
// 0x00000180 System.Object MoreMountains.CorgiEngine.CharacterDive/<Dive>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CDiveU3Ed__7_System_Collections_IEnumerator_get_Current_m475FAE401A618F784E4341B22C4E20C27A90861F (void);
// 0x00000181 System.String MoreMountains.CorgiEngine.CharacterFallDamage::HelpBoxText()
extern void CharacterFallDamage_HelpBoxText_m79052D063BC9B0D2E47DA089E1EA23DDB9AFAC75 (void);
// 0x00000182 System.Void MoreMountains.CorgiEngine.CharacterFallDamage::ProcessAbility()
extern void CharacterFallDamage_ProcessAbility_mCBD703982AD134D093A8B5AA28203203AA32753A (void);
// 0x00000183 System.Void MoreMountains.CorgiEngine.CharacterFallDamage::ResetTakeOffAltitude()
extern void CharacterFallDamage_ResetTakeOffAltitude_m3BCEEA3B3CB9932C806F55E183227CE515C60651 (void);
// 0x00000184 System.Boolean MoreMountains.CorgiEngine.CharacterFallDamage::CanTakeDamage()
extern void CharacterFallDamage_CanTakeDamage_m5C1B986346256D7D949BA6AC8BE37E47724F6ED3 (void);
// 0x00000185 System.Void MoreMountains.CorgiEngine.CharacterFallDamage::ApplyDamage(System.Single)
extern void CharacterFallDamage_ApplyDamage_mB34E1B36D5F326F5EFD7720BC970581525B9EDF3 (void);
// 0x00000186 System.Void MoreMountains.CorgiEngine.CharacterFallDamage::InitializeAnimatorParameters()
extern void CharacterFallDamage_InitializeAnimatorParameters_m619C08DBFD777F23F77B961630AB322100FB6D8B (void);
// 0x00000187 System.Void MoreMountains.CorgiEngine.CharacterFallDamage::UpdateAnimator()
extern void CharacterFallDamage_UpdateAnimator_m16C2F17D9456F0FB1675B1F5C5AF391C03E95AA1 (void);
// 0x00000188 System.Void MoreMountains.CorgiEngine.CharacterFallDamage::.ctor()
extern void CharacterFallDamage__ctor_mB2BA76966EDEC3E4B306FED5AC5A1AB12CFAC911 (void);
// 0x00000189 System.String MoreMountains.CorgiEngine.CharacterFly::HelpBoxText()
extern void CharacterFly_HelpBoxText_mEFAAF8671AEBEE2ED4D09948AE9922AE0FE76AB2 (void);
// 0x0000018A System.Single MoreMountains.CorgiEngine.CharacterFly::get_MovementSpeedMultiplier()
extern void CharacterFly_get_MovementSpeedMultiplier_m9AAB00537EEC898F19EA96CCB40495A42A6BC475 (void);
// 0x0000018B System.Void MoreMountains.CorgiEngine.CharacterFly::set_MovementSpeedMultiplier(System.Single)
extern void CharacterFly_set_MovementSpeedMultiplier_m3F7297AC8D4DCE31CC505AC8DD1F31E8FC88C45F (void);
// 0x0000018C System.Void MoreMountains.CorgiEngine.CharacterFly::Initialization()
extern void CharacterFly_Initialization_mC29EB6C246E5E19134061754B1874ED38C828820 (void);
// 0x0000018D System.Void MoreMountains.CorgiEngine.CharacterFly::HandleInput()
extern void CharacterFly_HandleInput_mD2D022CEEECBAC2BCF11686E5F9554A5EF23C23A (void);
// 0x0000018E System.Void MoreMountains.CorgiEngine.CharacterFly::SetHorizontalMove(System.Single)
extern void CharacterFly_SetHorizontalMove_mE2D2F9B972958C4CAC899A8F40C1CEF0220A0547 (void);
// 0x0000018F System.Void MoreMountains.CorgiEngine.CharacterFly::SetVerticalMove(System.Single)
extern void CharacterFly_SetVerticalMove_m64E4405AB743FC82574856159EC53A238D6F2EFA (void);
// 0x00000190 System.Void MoreMountains.CorgiEngine.CharacterFly::StartFlight()
extern void CharacterFly_StartFlight_m3933E7584C3DA6492CB7FFF04B76FA3AE239258E (void);
// 0x00000191 System.Void MoreMountains.CorgiEngine.CharacterFly::StopFlight()
extern void CharacterFly_StopFlight_mF0226756A343338F7E04A0E19E274031D3714234 (void);
// 0x00000192 System.Void MoreMountains.CorgiEngine.CharacterFly::ProcessAbility()
extern void CharacterFly_ProcessAbility_m350FC2533D443BA30955C7D583FB0836780D93F6 (void);
// 0x00000193 System.Void MoreMountains.CorgiEngine.CharacterFly::HandleMovement()
extern void CharacterFly_HandleMovement_mF7EAB398054BA07F301FA36C5EE7C4A6FDD0AA33 (void);
// 0x00000194 System.Void MoreMountains.CorgiEngine.CharacterFly::OnRevive()
extern void CharacterFly_OnRevive_mC9EFA6A678D00A33E24D26112CDE2B7E9AFA2E90 (void);
// 0x00000195 System.Void MoreMountains.CorgiEngine.CharacterFly::OnDeath()
extern void CharacterFly_OnDeath_m6D44C39736C5B95B5682ABAA5AD7D110C3495802 (void);
// 0x00000196 System.Void MoreMountains.CorgiEngine.CharacterFly::OnEnable()
extern void CharacterFly_OnEnable_m137803A68E6AF6E55FC94F8B599CEBD3FAC2F539 (void);
// 0x00000197 System.Void MoreMountains.CorgiEngine.CharacterFly::OnDisable()
extern void CharacterFly_OnDisable_m7C6E92852156C244E0BBA523856F0D4554C0057E (void);
// 0x00000198 System.Void MoreMountains.CorgiEngine.CharacterFly::InitializeAnimatorParameters()
extern void CharacterFly_InitializeAnimatorParameters_m007E227576B5FBB5B0231C92E5A180E0A85EFB93 (void);
// 0x00000199 System.Void MoreMountains.CorgiEngine.CharacterFly::UpdateAnimator()
extern void CharacterFly_UpdateAnimator_m2399637D493F88384907F8ED4775A155AE95E7C9 (void);
// 0x0000019A System.Void MoreMountains.CorgiEngine.CharacterFly::ResetAbility()
extern void CharacterFly_ResetAbility_mAD9EA4E70A17D6090F6A030B1E80175785C83E9B (void);
// 0x0000019B System.Void MoreMountains.CorgiEngine.CharacterFly::.ctor()
extern void CharacterFly__ctor_m2738637520499EB54EFCE470786EAB9B45C409BC (void);
// 0x0000019C System.Single MoreMountains.CorgiEngine.CharacterFollowPath::get_MovementSpeedMultiplier()
extern void CharacterFollowPath_get_MovementSpeedMultiplier_m15C0157B890A378139F3053904C015B32D793BA5 (void);
// 0x0000019D System.Void MoreMountains.CorgiEngine.CharacterFollowPath::set_MovementSpeedMultiplier(System.Single)
extern void CharacterFollowPath_set_MovementSpeedMultiplier_m70337476AB81F2A5206C17891D6152DAC7D096EB (void);
// 0x0000019E System.Void MoreMountains.CorgiEngine.CharacterFollowPath::Initialization()
extern void CharacterFollowPath_Initialization_m3BF02BB46630E1742CC04757B9E3A3F681A0CEA7 (void);
// 0x0000019F System.Void MoreMountains.CorgiEngine.CharacterFollowPath::StartFollowingPath()
extern void CharacterFollowPath_StartFollowingPath_m7D6C6200C6CE3F0D3CD27019B6F8C029653932BF (void);
// 0x000001A0 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::StopFollowingPath()
extern void CharacterFollowPath_StopFollowingPath_m58949C6A3DEDD50F660C7BCA42F50385040E71A4 (void);
// 0x000001A1 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::ProcessAbility()
extern void CharacterFollowPath_ProcessAbility_mDB9276D7E5E12E6CB44A45BB3ACA6C636286782D (void);
// 0x000001A2 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::HandleMovement()
extern void CharacterFollowPath_HandleMovement_m0B464D5F959770C7453FF3E4A8489683C0E4FE69 (void);
// 0x000001A3 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::OnRevive()
extern void CharacterFollowPath_OnRevive_m08ECC1D598399D056A70D06D7273600C698C6780 (void);
// 0x000001A4 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::OnDeath()
extern void CharacterFollowPath_OnDeath_m239095201E53E5FB21116AE9D4971BE9BD02D5B6 (void);
// 0x000001A5 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::OnEnable()
extern void CharacterFollowPath_OnEnable_mCE3949E1C5CFEC8EFF36548787D5B984AD4E6023 (void);
// 0x000001A6 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::OnDisable()
extern void CharacterFollowPath_OnDisable_m221321017CAC4DF4897F7A6CB518C2B6EBBAA0F6 (void);
// 0x000001A7 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::InitializeAnimatorParameters()
extern void CharacterFollowPath_InitializeAnimatorParameters_mED75E32FC19C089E64939A806F460184898B13A1 (void);
// 0x000001A8 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::UpdateAnimator()
extern void CharacterFollowPath_UpdateAnimator_m2DE5992C4B519D8A4DE07D3893B1BBE6E755AAF1 (void);
// 0x000001A9 System.Void MoreMountains.CorgiEngine.CharacterFollowPath::.ctor()
extern void CharacterFollowPath__ctor_m55876E74124BB0988D7DB7E3F66175910A315CD3 (void);
// 0x000001AA System.String MoreMountains.CorgiEngine.CharacterGlide::HelpBoxText()
extern void CharacterGlide_HelpBoxText_m0D5C3A2B9F07A5A01ECB1BCE6CC0EFD4FD9B9BD5 (void);
// 0x000001AB System.Void MoreMountains.CorgiEngine.CharacterGlide::Initialization()
extern void CharacterGlide_Initialization_m119254E684B4C85EC2F8F5431442AD2260040A94 (void);
// 0x000001AC System.Void MoreMountains.CorgiEngine.CharacterGlide::HandleInput()
extern void CharacterGlide_HandleInput_mF175EA711DBBCB8757574C5B8B837B30A377D74F (void);
// 0x000001AD System.Void MoreMountains.CorgiEngine.CharacterGlide::GlideStart()
extern void CharacterGlide_GlideStart_m077C43C3536D49F70B426E4F258A3C880A7D3891 (void);
// 0x000001AE System.Void MoreMountains.CorgiEngine.CharacterGlide::GlideStop()
extern void CharacterGlide_GlideStop_m8804DC047451989E540B5F57ADC4F2F30BBDFEB2 (void);
// 0x000001AF System.Void MoreMountains.CorgiEngine.CharacterGlide::ProcessAbility()
extern void CharacterGlide_ProcessAbility_mBF924A098E53E0C3236AD699195B4B87EBBCA5AE (void);
// 0x000001B0 System.Void MoreMountains.CorgiEngine.CharacterGlide::InitializeAnimatorParameters()
extern void CharacterGlide_InitializeAnimatorParameters_m43A4B5ED0787F3E7C97CE2A2B253D31300FC0204 (void);
// 0x000001B1 System.Void MoreMountains.CorgiEngine.CharacterGlide::UpdateAnimator()
extern void CharacterGlide_UpdateAnimator_m7C919E3FC5B829C12CE2427149D623E95A718321 (void);
// 0x000001B2 System.Void MoreMountains.CorgiEngine.CharacterGlide::ResetAbility()
extern void CharacterGlide_ResetAbility_m76ED451359F046F97424135705FB140DA8081FDE (void);
// 0x000001B3 System.Void MoreMountains.CorgiEngine.CharacterGlide::.ctor()
extern void CharacterGlide__ctor_m87A6933F9E1CFF243DCEA7343DCEE9888D723039 (void);
// 0x000001B4 System.String MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::HelpBoxText()
extern void CharacterGrabCarryAndThrow_HelpBoxText_mC8C2E4515FE28AA422DD37B9E696BD99035DDCC3 (void);
// 0x000001B5 System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::Initialization()
extern void CharacterGrabCarryAndThrow_Initialization_mF8F963BF4C5CB26813EE3B8D153ABB5B232E0D4A (void);
// 0x000001B6 System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::HandleInput()
extern void CharacterGrabCarryAndThrow_HandleInput_m660F5E85DDF78AE29CBB3B1AC2C25DD768856586 (void);
// 0x000001B7 System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::GrabAttempt()
extern void CharacterGrabCarryAndThrow_GrabAttempt_m9F0D2ED3CB2E27095FB31457249FABEB071C2B1F (void);
// 0x000001B8 System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::Grab()
extern void CharacterGrabCarryAndThrow_Grab_m875CBF56DE04A578DD187AEFF1EFADEEDA6423AA (void);
// 0x000001B9 System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::Throw()
extern void CharacterGrabCarryAndThrow_Throw_m6C4FA1E3D499409926C6F0666858F9F12C83455C (void);
// 0x000001BA System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::StopFeedbacks()
extern void CharacterGrabCarryAndThrow_StopFeedbacks_m45960A5CC1DE4D337B7E873D260172A13145E612 (void);
// 0x000001BB System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::LateUpdate()
extern void CharacterGrabCarryAndThrow_LateUpdate_m027A6E6CE02EBB929CC9830ECF1CDF4A7C040667 (void);
// 0x000001BC System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::InitializeAnimatorParameters()
extern void CharacterGrabCarryAndThrow_InitializeAnimatorParameters_m855AA576687AE38C2A5BA910F69A35FED446C5F3 (void);
// 0x000001BD System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::UpdateAnimator()
extern void CharacterGrabCarryAndThrow_UpdateAnimator_mCCC7D155D079D90199C0B4FA8470F6F24061AA86 (void);
// 0x000001BE System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::ResetAbility()
extern void CharacterGrabCarryAndThrow_ResetAbility_mFF3EC5921ADE86850735F0A8869F7E5AD045C351 (void);
// 0x000001BF System.Void MoreMountains.CorgiEngine.CharacterGrabCarryAndThrow::.ctor()
extern void CharacterGrabCarryAndThrow__ctor_m82F3FDD07E082AEC2CB13D9BE45AA346417516D3 (void);
// 0x000001C0 System.String MoreMountains.CorgiEngine.CharacterGravity::HelpBoxText()
extern void CharacterGravity_HelpBoxText_mEEBAFF1F02DFC62FC33A5E522003266ABD347639 (void);
// 0x000001C1 System.Single MoreMountains.CorgiEngine.CharacterGravity::get_GravityAngle()
extern void CharacterGravity_get_GravityAngle_m5CF126752FFE6498AA98DE9CEB97F7A7FD45C9BE (void);
// 0x000001C2 UnityEngine.Vector2 MoreMountains.CorgiEngine.CharacterGravity::get_GravityDirectionVector()
extern void CharacterGravity_get_GravityDirectionVector_m9C380E449E7F504215C8A65178F714855071DEE0 (void);
// 0x000001C3 System.Boolean MoreMountains.CorgiEngine.CharacterGravity::get_InGravityPointRange()
extern void CharacterGravity_get_InGravityPointRange_m83BFB33F282DA3E536B56F97CA844006F0045432 (void);
// 0x000001C4 System.Void MoreMountains.CorgiEngine.CharacterGravity::set_InGravityPointRange(System.Boolean)
extern void CharacterGravity_set_InGravityPointRange_m1033177E4898659BDF4179C3E3C816639B13A7DB (void);
// 0x000001C5 System.Void MoreMountains.CorgiEngine.CharacterGravity::Initialization()
extern void CharacterGravity_Initialization_mC87999A5A277EF30F2C41A63447AE0770F334965 (void);
// 0x000001C6 System.Void MoreMountains.CorgiEngine.CharacterGravity::Update()
extern void CharacterGravity_Update_mE131EFB9FAAB606D5F4E98E4C6E88DC8D77BCB27 (void);
// 0x000001C7 System.Void MoreMountains.CorgiEngine.CharacterGravity::CleanGravityZones()
extern void CharacterGravity_CleanGravityZones_mAD101E0CC0ECFF2C30BF89F4ACB32ED5EFCC0734 (void);
// 0x000001C8 System.Void MoreMountains.CorgiEngine.CharacterGravity::ComputeGravityPoints()
extern void CharacterGravity_ComputeGravityPoints_mC991B0FB098BDB19915D6471DFEBD9BDA2AA2CA6 (void);
// 0x000001C9 MoreMountains.CorgiEngine.GravityPoint MoreMountains.CorgiEngine.CharacterGravity::GetClosestGravityPoint()
extern void CharacterGravity_GetClosestGravityPoint_m0136FFB1934DB14AC7383CD763CD9467858716FF (void);
// 0x000001CA System.Void MoreMountains.CorgiEngine.CharacterGravity::UpdateGravity()
extern void CharacterGravity_UpdateGravity_m61D59E4DF8566E05421EFAEA745A32270E505B2F (void);
// 0x000001CB System.Void MoreMountains.CorgiEngine.CharacterGravity::UpdateGravityPointsList()
extern void CharacterGravity_UpdateGravityPointsList_m3A291AEDBF9F64B1FE79564D5C9589CF98CC5B29 (void);
// 0x000001CC System.Void MoreMountains.CorgiEngine.CharacterGravity::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterGravity_OnTriggerEnter2D_mB95AD47C229624552F6E0C53143697359206FF6F (void);
// 0x000001CD System.Void MoreMountains.CorgiEngine.CharacterGravity::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CharacterGravity_OnTriggerExit2D_m1F68046F22A153C7455102D82AE373C24455A88C (void);
// 0x000001CE System.Void MoreMountains.CorgiEngine.CharacterGravity::SetGravityZone(MoreMountains.CorgiEngine.GravityZone)
extern void CharacterGravity_SetGravityZone_m797FCFD4A1DE6EB3CD2A470B13C2DC34392ADC42 (void);
// 0x000001CF System.Void MoreMountains.CorgiEngine.CharacterGravity::ExitGravityZone(MoreMountains.CorgiEngine.GravityZone)
extern void CharacterGravity_ExitGravityZone_m0B66B88820B5EBEF8E9AFA4F730586E46E6F0F25 (void);
// 0x000001D0 System.Void MoreMountains.CorgiEngine.CharacterGravity::StartRotating()
extern void CharacterGravity_StartRotating_mCE39E79A4CCC5BA21E0D7C288003F0D7C842BCAA (void);
// 0x000001D1 System.Void MoreMountains.CorgiEngine.CharacterGravity::Transition(System.Boolean,UnityEngine.Vector2)
extern void CharacterGravity_Transition_m0C7D6BB8057A7BE95BF638A0181CF2E69DC89EE4 (void);
// 0x000001D2 System.Boolean MoreMountains.CorgiEngine.CharacterGravity::ShouldReverseInput()
extern void CharacterGravity_ShouldReverseInput_m3B83D2072A14B7BD288D65BC92B0C964A84DF423 (void);
// 0x000001D3 System.Void MoreMountains.CorgiEngine.CharacterGravity::SetGravityAngle(System.Single)
extern void CharacterGravity_SetGravityAngle_mAA043E3B4DCA7925929A235DA3CBF891BD4FF54D (void);
// 0x000001D4 System.Void MoreMountains.CorgiEngine.CharacterGravity::ResetGravityToDefault()
extern void CharacterGravity_ResetGravityToDefault_m85B7C6D09EAAA8BDEB41A42C70F56155C6B47D8F (void);
// 0x000001D5 System.Void MoreMountains.CorgiEngine.CharacterGravity::OnRespawn()
extern void CharacterGravity_OnRespawn_m1FFEF2047FD74BF5C5513473CA3DC267A94A430F (void);
// 0x000001D6 System.Void MoreMountains.CorgiEngine.CharacterGravity::DrawGravityDebug()
extern void CharacterGravity_DrawGravityDebug_m86715B338A0C8C0E55B821EE98A34F02D3A97DB6 (void);
// 0x000001D7 System.Void MoreMountains.CorgiEngine.CharacterGravity::ResetAbility()
extern void CharacterGravity_ResetAbility_m389E21FEAC813978348103A4A7B9311861740AF1 (void);
// 0x000001D8 System.Void MoreMountains.CorgiEngine.CharacterGravity::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void CharacterGravity_OnMMEvent_m25F0ABDB934BA8A0DFDEF240D8A66A05616C6271 (void);
// 0x000001D9 System.Void MoreMountains.CorgiEngine.CharacterGravity::OnEnable()
extern void CharacterGravity_OnEnable_m507F6344E8CCBEF3F14FDE55FD7AA7FF1C7546F7 (void);
// 0x000001DA System.Void MoreMountains.CorgiEngine.CharacterGravity::OnDestroy()
extern void CharacterGravity_OnDestroy_m91C59BA1194D75D157E820E3457B4A141FB856DD (void);
// 0x000001DB System.Void MoreMountains.CorgiEngine.CharacterGravity::.ctor()
extern void CharacterGravity__ctor_m888962652512B7FD38A25F2AF7FDAC8A2DD3D72A (void);
// 0x000001DC System.String MoreMountains.CorgiEngine.CharacterGrip::HelpBoxText()
extern void CharacterGrip_HelpBoxText_mF75ACB4F85E385FC9B4FAC7CCBA0658FBC2F326B (void);
// 0x000001DD System.Boolean MoreMountains.CorgiEngine.CharacterGrip::get_CanGrip()
extern void CharacterGrip_get_CanGrip_m93DF777911BC1FB12A578DBC14E1A5EEB24C47F8 (void);
// 0x000001DE System.Void MoreMountains.CorgiEngine.CharacterGrip::Initialization()
extern void CharacterGrip_Initialization_mDA47AB6B8877C496AE74A54AC9957F286540CE89 (void);
// 0x000001DF System.Void MoreMountains.CorgiEngine.CharacterGrip::ProcessAbility()
extern void CharacterGrip_ProcessAbility_m9CC1B105E67AB754DB19AB5404E6250321ADC848 (void);
// 0x000001E0 System.Void MoreMountains.CorgiEngine.CharacterGrip::HandleInput()
extern void CharacterGrip_HandleInput_m8C13C66B47BF936048914A80D16F0783E2E62347 (void);
// 0x000001E1 System.Void MoreMountains.CorgiEngine.CharacterGrip::StartGripping(MoreMountains.CorgiEngine.Grip)
extern void CharacterGrip_StartGripping_m8BC7551B0554D0C759AC9F3A0EB7D980CF821D34 (void);
// 0x000001E2 System.Void MoreMountains.CorgiEngine.CharacterGrip::Grip()
extern void CharacterGrip_Grip_mCAA9F387BD776E8646D5C8ECBB7D492F7C0B9030 (void);
// 0x000001E3 System.Void MoreMountains.CorgiEngine.CharacterGrip::Detach()
extern void CharacterGrip_Detach_m5F0676A147CE3136456DF7DF4CD126437743DEEC (void);
// 0x000001E4 System.Void MoreMountains.CorgiEngine.CharacterGrip::InitializeAnimatorParameters()
extern void CharacterGrip_InitializeAnimatorParameters_m39A4CF915EC3EB39CA7B03E16506F99EBEE47704 (void);
// 0x000001E5 System.Void MoreMountains.CorgiEngine.CharacterGrip::UpdateAnimator()
extern void CharacterGrip_UpdateAnimator_m079915E801A30EC4D13145397D1C747248D9510A (void);
// 0x000001E6 System.Void MoreMountains.CorgiEngine.CharacterGrip::ResetAbility()
extern void CharacterGrip_ResetAbility_m90F29D4ADFCCD9CAA3A03E31E532A1B340CF1326 (void);
// 0x000001E7 System.Void MoreMountains.CorgiEngine.CharacterGrip::.ctor()
extern void CharacterGrip__ctor_m4A57B268B78286F5580507DA62F9306CA5072DAA (void);
// 0x000001E8 System.String MoreMountains.CorgiEngine.CharacterGroundNormalGravity::HelpBoxText()
extern void CharacterGroundNormalGravity_HelpBoxText_mD8C8AFA3C4D1FC9D4C5C5D2A63FB7DD60FC80323 (void);
// 0x000001E9 System.Void MoreMountains.CorgiEngine.CharacterGroundNormalGravity::ProcessAbility()
extern void CharacterGroundNormalGravity_ProcessAbility_mA9D0534A8C58B7B5EC3B76A5F01FD0E064AEC17B (void);
// 0x000001EA System.Void MoreMountains.CorgiEngine.CharacterGroundNormalGravity::.ctor()
extern void CharacterGroundNormalGravity__ctor_mBE05448CFE3CAE69F37A31C82AAFB624030CA468 (void);
// 0x000001EB System.Int32 MoreMountains.CorgiEngine.CharacterHandleSecondaryWeapon::get_HandleWeaponID()
extern void CharacterHandleSecondaryWeapon_get_HandleWeaponID_m8E40D980A861B1C727303B9E1286815D38E5E0C8 (void);
// 0x000001EC System.Void MoreMountains.CorgiEngine.CharacterHandleSecondaryWeapon::HandleInput()
extern void CharacterHandleSecondaryWeapon_HandleInput_mE4B10F51A18428C0B51E49F9786A51043367656B (void);
// 0x000001ED System.Void MoreMountains.CorgiEngine.CharacterHandleSecondaryWeapon::.ctor()
extern void CharacterHandleSecondaryWeapon__ctor_m89F105C63F0DF1418F987201D0CF08A4D683DBF6 (void);
// 0x000001EE System.String MoreMountains.CorgiEngine.CharacterHandleWeapon::HelpBoxText()
extern void CharacterHandleWeapon_HelpBoxText_m1F2CD91E7DAE5335895C9F8A177E505343C59448 (void);
// 0x000001EF System.Int32 MoreMountains.CorgiEngine.CharacterHandleWeapon::get_HandleWeaponID()
extern void CharacterHandleWeapon_get_HandleWeaponID_m253ECC414C01C2370E985D86350FADC86B3EA025 (void);
// 0x000001F0 UnityEngine.Animator MoreMountains.CorgiEngine.CharacterHandleWeapon::get_CharacterAnimator()
extern void CharacterHandleWeapon_get_CharacterAnimator_m9087C592E498AFAC64E45CC7472E30292078ED2B (void);
// 0x000001F1 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::set_CharacterAnimator(UnityEngine.Animator)
extern void CharacterHandleWeapon_set_CharacterAnimator_m5EB91817B19DC74CF3ADBBBCC7E50C2E30A81B38 (void);
// 0x000001F2 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::Initialization()
extern void CharacterHandleWeapon_Initialization_m4F6F79A36760E909A8C3084E82A6737A1E9D2619 (void);
// 0x000001F3 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::Setup()
extern void CharacterHandleWeapon_Setup_m9953000FC787FF2228A839F3D4A4A4906851AA3A (void);
// 0x000001F4 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::ProcessAbility()
extern void CharacterHandleWeapon_ProcessAbility_m4237D410432D7CC45212CF54B1088045C625F8BE (void);
// 0x000001F5 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::HandleFacingDirection()
extern void CharacterHandleWeapon_HandleFacingDirection_m7BC60970AC238574077D43B4223DEDD2C627C545 (void);
// 0x000001F6 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::HandleInput()
extern void CharacterHandleWeapon_HandleInput_m53EA27365469EA836ABADAF489F00E1716082A78 (void);
// 0x000001F7 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::HandleBuffer()
extern void CharacterHandleWeapon_HandleBuffer_mB31A25C703F3EECA70161AA80CDB7EBE61D3D542 (void);
// 0x000001F8 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::ShootStart()
extern void CharacterHandleWeapon_ShootStart_mCC40CAAA7AB770B3A1E2BBB5F9EA48756D33744C (void);
// 0x000001F9 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::ShootStop()
extern void CharacterHandleWeapon_ShootStop_m56DC86CE2274C02301004A9B97BA5DBADA27398F (void);
// 0x000001FA System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::ForceStop()
extern void CharacterHandleWeapon_ForceStop_m4A9A055AC12DCF9E5C6F902F3E06DDE54EA0C4B5 (void);
// 0x000001FB System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::Reload()
extern void CharacterHandleWeapon_Reload_m87F85248C712006E0002563FEDE0B434A88A2FE2 (void);
// 0x000001FC System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::ChangeWeapon(MoreMountains.CorgiEngine.Weapon,System.String,System.Boolean)
extern void CharacterHandleWeapon_ChangeWeapon_m7D68DB5FF3A801D6C73C3257477CDF2443F24487 (void);
// 0x000001FD System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::Flip()
extern void CharacterHandleWeapon_Flip_mEAD0ED9939D298D8ACE321F2078F579A56F69D89 (void);
// 0x000001FE System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::UpdateAmmoDisplay()
extern void CharacterHandleWeapon_UpdateAmmoDisplay_m40BC04AA4850C7D104ABBF3919D265B484A777A1 (void);
// 0x000001FF System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::OnRespawn()
extern void CharacterHandleWeapon_OnRespawn_mB1C4A2BEE398508D8812BEC529B52BF994A1EF73 (void);
// 0x00000200 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::OnHit()
extern void CharacterHandleWeapon_OnHit_m42D93B870DA503BC389BA469A26F4DE285731075 (void);
// 0x00000201 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::OnDeath()
extern void CharacterHandleWeapon_OnDeath_m8110BC139A94A29D671B9F3C7E1D2226CCEA50A3 (void);
// 0x00000202 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::ResetAbility()
extern void CharacterHandleWeapon_ResetAbility_m7684C5BDB98CDA32535729EF622D5B59C5D154B9 (void);
// 0x00000203 System.Void MoreMountains.CorgiEngine.CharacterHandleWeapon::.ctor()
extern void CharacterHandleWeapon__ctor_m4307F1828A2FE175BFCFCE4CC86AE3A68A674DDD (void);
// 0x00000204 System.String MoreMountains.CorgiEngine.CharacterHorizontalMovement::HelpBoxText()
extern void CharacterHorizontalMovement_HelpBoxText_m36BE07AB203FA9705813C280D86B42231D5F95F8 (void);
// 0x00000205 System.Single MoreMountains.CorgiEngine.CharacterHorizontalMovement::get_MovementSpeed()
extern void CharacterHorizontalMovement_get_MovementSpeed_m1BAC59D0759BEDD81F32E121EF1DA8684277858C (void);
// 0x00000206 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::set_MovementSpeed(System.Single)
extern void CharacterHorizontalMovement_set_MovementSpeed_mD09BDD9D524D8E410CD760737DC96986115B5A25 (void);
// 0x00000207 System.Single MoreMountains.CorgiEngine.CharacterHorizontalMovement::get_HorizontalMovementForce()
extern void CharacterHorizontalMovement_get_HorizontalMovementForce_mBB88FC6C7D0ED7078170EAAED93E54CD99F383D7 (void);
// 0x00000208 System.Boolean MoreMountains.CorgiEngine.CharacterHorizontalMovement::get_MovementForbidden()
extern void CharacterHorizontalMovement_get_MovementForbidden_mD0CFE8EA23F165741E64BC59146254DFF2D7EF40 (void);
// 0x00000209 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::set_MovementForbidden(System.Boolean)
extern void CharacterHorizontalMovement_set_MovementForbidden_m36309648C3BF5D31D9B2CEA6E4CEB7131517E331 (void);
// 0x0000020A System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::Initialization()
extern void CharacterHorizontalMovement_Initialization_m18A24E62D9068F889CD5EF7EA17C64045278DB86 (void);
// 0x0000020B System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::ProcessAbility()
extern void CharacterHorizontalMovement_ProcessAbility_m97EBFF78B133459EEBCB1F0CF66148D698BDC4A8 (void);
// 0x0000020C System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::HandleInput()
extern void CharacterHorizontalMovement_HandleInput_m7570002EF8ED389F56F41A783A408B96B7E96960 (void);
// 0x0000020D System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::SetAirControlDirection(System.Single)
extern void CharacterHorizontalMovement_SetAirControlDirection_mCC66ED3BE7019C7641085576353FFF9B6232F6D5 (void);
// 0x0000020E System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::SetHorizontalMove(System.Single)
extern void CharacterHorizontalMovement_SetHorizontalMove_m6321AE056939005543469B2A7547570800381D16 (void);
// 0x0000020F System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::HandleHorizontalMovement()
extern void CharacterHorizontalMovement_HandleHorizontalMovement_m1F365E7B14AF73833559DC0D1128CF5C19C14C53 (void);
// 0x00000210 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::DetectWalls()
extern void CharacterHorizontalMovement_DetectWalls_mFB7C29D3ACDA5B5F2BB81069333729FF7AD02096 (void);
// 0x00000211 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::CheckJustGotGrounded()
extern void CharacterHorizontalMovement_CheckJustGotGrounded_m349C9FEEF0B3007B8EC3C0341A0676D35DB92961 (void);
// 0x00000212 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::StoreLastTimeGrounded()
extern void CharacterHorizontalMovement_StoreLastTimeGrounded_m40D6000FBE8E54633578F420FBB2716C0BFDA791 (void);
// 0x00000213 System.Single MoreMountains.CorgiEngine.CharacterHorizontalMovement::HandleFriction(System.Single)
extern void CharacterHorizontalMovement_HandleFriction_mA207286A163CF52A6D14FD5B6EF3D44CE9F53E1C (void);
// 0x00000214 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::ResetHorizontalSpeed()
extern void CharacterHorizontalMovement_ResetHorizontalSpeed_m8A1FFFD02D6F22452E22188EC89937F8BFA93297 (void);
// 0x00000215 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::InitializeAnimatorParameters()
extern void CharacterHorizontalMovement_InitializeAnimatorParameters_mBCF1A2769FE84824D4A67F6EFA70CC6645086EA6 (void);
// 0x00000216 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::UpdateAnimator()
extern void CharacterHorizontalMovement_UpdateAnimator_mAB48C54A5BEBD0DEB169DCFB80BED460B1560B58 (void);
// 0x00000217 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::OnRevive()
extern void CharacterHorizontalMovement_OnRevive_mD26C8B70521DEC2372CBBBDAD61C83F002B46933 (void);
// 0x00000218 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::OnEnable()
extern void CharacterHorizontalMovement_OnEnable_m60C57CF70AFA36B6D1E32458D0341284BED46B7A (void);
// 0x00000219 System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::OnDisable()
extern void CharacterHorizontalMovement_OnDisable_m3EEF85AAA63F53820CA3094F2CCF406FD222B721 (void);
// 0x0000021A System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovement::.ctor()
extern void CharacterHorizontalMovement__ctor_m2EEC5128C57933BC60842F274EF1D1FEAE29FBDB (void);
// 0x0000021B MoreMountains.InventoryEngine.Inventory MoreMountains.CorgiEngine.CharacterInventory::get_MainInventory()
extern void CharacterInventory_get_MainInventory_mD88D766807F991886C6C01F4F2993CB70B9E35AE (void);
// 0x0000021C System.Void MoreMountains.CorgiEngine.CharacterInventory::set_MainInventory(MoreMountains.InventoryEngine.Inventory)
extern void CharacterInventory_set_MainInventory_mF4F70C53374B693A88BB0F632D7AF1BE7051158C (void);
// 0x0000021D MoreMountains.InventoryEngine.Inventory MoreMountains.CorgiEngine.CharacterInventory::get_WeaponInventory()
extern void CharacterInventory_get_WeaponInventory_mFE3A0953BBD6ACB0C98AE3A37BD0386883AF003B (void);
// 0x0000021E System.Void MoreMountains.CorgiEngine.CharacterInventory::set_WeaponInventory(MoreMountains.InventoryEngine.Inventory)
extern void CharacterInventory_set_WeaponInventory_m514C77C8EDC57C5BEA1968946CB45E1AC550E671 (void);
// 0x0000021F MoreMountains.InventoryEngine.Inventory MoreMountains.CorgiEngine.CharacterInventory::get_HotbarInventory()
extern void CharacterInventory_get_HotbarInventory_m4F0F2B97C5E59FFA8242A705F14842FD39C7C607 (void);
// 0x00000220 System.Void MoreMountains.CorgiEngine.CharacterInventory::set_HotbarInventory(MoreMountains.InventoryEngine.Inventory)
extern void CharacterInventory_set_HotbarInventory_mF642BCEE5005A33173BE3C475F94D97341E93D04 (void);
// 0x00000221 System.Void MoreMountains.CorgiEngine.CharacterInventory::Initialization()
extern void CharacterInventory_Initialization_m75F4E828C8D2DC4A206CEAC073BD7471929A84C2 (void);
// 0x00000222 System.Void MoreMountains.CorgiEngine.CharacterInventory::ProcessAbility()
extern void CharacterInventory_ProcessAbility_m9A841624C1705426B355EF7E1750EA5B10DCA96C (void);
// 0x00000223 System.Void MoreMountains.CorgiEngine.CharacterInventory::Setup()
extern void CharacterInventory_Setup_m30D34FF407BB82DD5CBD5EC9919377305FC38CE8 (void);
// 0x00000224 System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterInventory::AutoAddAndEquip()
extern void CharacterInventory_AutoAddAndEquip_m263F906BB99B88D2D6BB89BDA7B20C2D975AE69C (void);
// 0x00000225 System.Void MoreMountains.CorgiEngine.CharacterInventory::GrabInventories()
extern void CharacterInventory_GrabInventories_m61D3201E89B4FBF6C83B0CAB0A33A35ABEBE0FBE (void);
// 0x00000226 System.Void MoreMountains.CorgiEngine.CharacterInventory::HandleInput()
extern void CharacterInventory_HandleInput_mAFA3188B677C9A42C78AB87643D6F4F116B20C6C (void);
// 0x00000227 System.Void MoreMountains.CorgiEngine.CharacterInventory::FillAvailableWeaponsLists()
extern void CharacterInventory_FillAvailableWeaponsLists_m4A86294AA8C76F6DA499C1C9FF9161B40F34F21B (void);
// 0x00000228 System.Void MoreMountains.CorgiEngine.CharacterInventory::DetermineNextWeaponName()
extern void CharacterInventory_DetermineNextWeaponName_m7ADAF0E64525D996B5E37B5EB5D587FB3A6A6CA4 (void);
// 0x00000229 System.Void MoreMountains.CorgiEngine.CharacterInventory::EquipWeapon(System.String)
extern void CharacterInventory_EquipWeapon_m1F9D3D33FF88DB05E3DAB503D2D41034F45A725F (void);
// 0x0000022A System.Void MoreMountains.CorgiEngine.CharacterInventory::SwitchWeapon()
extern void CharacterInventory_SwitchWeapon_m1D137B41BDBA9033872AECF86DF57DB9425985C6 (void);
// 0x0000022B System.Void MoreMountains.CorgiEngine.CharacterInventory::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void CharacterInventory_OnMMEvent_mC0BB70415F46670FF7588D997F46619388F48268 (void);
// 0x0000022C System.Void MoreMountains.CorgiEngine.CharacterInventory::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void CharacterInventory_OnMMEvent_m69B7418EE8C55AA21066BCA4DF7E38288588647B (void);
// 0x0000022D System.Void MoreMountains.CorgiEngine.CharacterInventory::OnEnable()
extern void CharacterInventory_OnEnable_m182B54E1B375207EF7142F94D96242EFB6180E03 (void);
// 0x0000022E System.Void MoreMountains.CorgiEngine.CharacterInventory::OnDisable()
extern void CharacterInventory_OnDisable_m0522D3B2F24FA8B5F5EE4D6C99ED22C7F4082F7F (void);
// 0x0000022F System.Void MoreMountains.CorgiEngine.CharacterInventory::.ctor()
extern void CharacterInventory__ctor_m0A9AA89B10487035BF9EF4A0EAECA411EADB8CC6 (void);
// 0x00000230 System.Void MoreMountains.CorgiEngine.CharacterInventory/<AutoAddAndEquip>d__36::.ctor(System.Int32)
extern void U3CAutoAddAndEquipU3Ed__36__ctor_m20ADCEE5975750564CE4D2E2375CDD81808C7ADC (void);
// 0x00000231 System.Void MoreMountains.CorgiEngine.CharacterInventory/<AutoAddAndEquip>d__36::System.IDisposable.Dispose()
extern void U3CAutoAddAndEquipU3Ed__36_System_IDisposable_Dispose_m9816581479A4F717D8855F22F89F9C1A2E3F92FD (void);
// 0x00000232 System.Boolean MoreMountains.CorgiEngine.CharacterInventory/<AutoAddAndEquip>d__36::MoveNext()
extern void U3CAutoAddAndEquipU3Ed__36_MoveNext_m38424391F52B8A455DD78DB602CBEE3D832F710E (void);
// 0x00000233 System.Object MoreMountains.CorgiEngine.CharacterInventory/<AutoAddAndEquip>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAutoAddAndEquipU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m201F111228A23E549CD3BDCCCF3BBDD8FCF2808A (void);
// 0x00000234 System.Void MoreMountains.CorgiEngine.CharacterInventory/<AutoAddAndEquip>d__36::System.Collections.IEnumerator.Reset()
extern void U3CAutoAddAndEquipU3Ed__36_System_Collections_IEnumerator_Reset_mD61E950C5E4FF7E987DA2B1893DB013E8B0E8C9C (void);
// 0x00000235 System.Object MoreMountains.CorgiEngine.CharacterInventory/<AutoAddAndEquip>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CAutoAddAndEquipU3Ed__36_System_Collections_IEnumerator_get_Current_mBFDD14C6099B3B9CEABA794844957FFFAB760A24 (void);
// 0x00000236 System.String MoreMountains.CorgiEngine.CharacterJetpack::HelpBoxText()
extern void CharacterJetpack_HelpBoxText_mB9137CB1509D6B3F5BF4AF2FE02F3EAEB07166A0 (void);
// 0x00000237 System.Void MoreMountains.CorgiEngine.CharacterJetpack::Initialization()
extern void CharacterJetpack_Initialization_mF99D111A5B05C993704F6FFEBDA4CFDEDC3671DB (void);
// 0x00000238 System.Void MoreMountains.CorgiEngine.CharacterJetpack::HandleInput()
extern void CharacterJetpack_HandleInput_mD98F9B4DF4E1CC086B2B19290EDAE3239D492BC9 (void);
// 0x00000239 System.Void MoreMountains.CorgiEngine.CharacterJetpack::JetpackStart()
extern void CharacterJetpack_JetpackStart_m42B40D5AB64418BECCC02D07CAD2E65C7A639E6A (void);
// 0x0000023A System.Void MoreMountains.CorgiEngine.CharacterJetpack::JetpackStop()
extern void CharacterJetpack_JetpackStop_m8F32112309DE98A6EACC53D3756A24CA9B373227 (void);
// 0x0000023B System.Void MoreMountains.CorgiEngine.CharacterJetpack::TurnJetpackElementsOff()
extern void CharacterJetpack_TurnJetpackElementsOff_m23D2C0DA822612AB03AEB82FA5CA84693983270C (void);
// 0x0000023C System.Void MoreMountains.CorgiEngine.CharacterJetpack::ProcessAbility()
extern void CharacterJetpack_ProcessAbility_mCA38F174B38372103D40D1284DBAAC8A3F8787C4 (void);
// 0x0000023D System.Void MoreMountains.CorgiEngine.CharacterJetpack::BurnFuel()
extern void CharacterJetpack_BurnFuel_m7DAEE33EAEB475A0B084B6F816DC5C70949469F6 (void);
// 0x0000023E System.Void MoreMountains.CorgiEngine.CharacterJetpack::Refuel()
extern void CharacterJetpack_Refuel_mF3B861F9EC4ACA39411653F7E78E46B66D9C99C7 (void);
// 0x0000023F System.Void MoreMountains.CorgiEngine.CharacterJetpack::UpdateJetpackBar()
extern void CharacterJetpack_UpdateJetpackBar_mA2F3AA114B817C26FC0FAE0EB3AE3F3C31417FC3 (void);
// 0x00000240 System.Void MoreMountains.CorgiEngine.CharacterJetpack::Flip()
extern void CharacterJetpack_Flip_mF25680C8DB1CDE21416ED2CF050A659593B01444 (void);
// 0x00000241 System.Void MoreMountains.CorgiEngine.CharacterJetpack::PlayJetpackRefueledSfx()
extern void CharacterJetpack_PlayJetpackRefueledSfx_mAD682FAE5B61F0280FACCFA660E845532F30C9DE (void);
// 0x00000242 System.Void MoreMountains.CorgiEngine.CharacterJetpack::ResetAbility()
extern void CharacterJetpack_ResetAbility_m76BB3F736EB1751445AEF6ABAD6E59B55733A275 (void);
// 0x00000243 System.Void MoreMountains.CorgiEngine.CharacterJetpack::InitializeAnimatorParameters()
extern void CharacterJetpack_InitializeAnimatorParameters_m06246D21D32D4CEDDD6E589A95B0050967CC00B3 (void);
// 0x00000244 System.Void MoreMountains.CorgiEngine.CharacterJetpack::UpdateAnimator()
extern void CharacterJetpack_UpdateAnimator_m6AB55C5CFC1DFBC51D8A717FC2FD8842E7232100 (void);
// 0x00000245 System.Void MoreMountains.CorgiEngine.CharacterJetpack::.ctor()
extern void CharacterJetpack__ctor_mE8B09B090BE99BE511A5682230CD870D9ECF04E1 (void);
// 0x00000246 System.String MoreMountains.CorgiEngine.CharacterJump::HelpBoxText()
extern void CharacterJump_HelpBoxText_mDE5E84596E32613429F2DA70130CABD2B3D71A04 (void);
// 0x00000247 System.Boolean MoreMountains.CorgiEngine.CharacterJump::get_JumpHappenedThisFrame()
extern void CharacterJump_get_JumpHappenedThisFrame_m3522D290C7D10AD3AE87AB0B2A4388F88FFF3E36 (void);
// 0x00000248 System.Void MoreMountains.CorgiEngine.CharacterJump::set_JumpHappenedThisFrame(System.Boolean)
extern void CharacterJump_set_JumpHappenedThisFrame_mFE0698A874A0A741408BADA19A9D727C1158B981 (void);
// 0x00000249 System.Boolean MoreMountains.CorgiEngine.CharacterJump::get_CanJumpStop()
extern void CharacterJump_get_CanJumpStop_mF146CECC4B529B366F3BE01D13DB5B9EC40674F4 (void);
// 0x0000024A System.Void MoreMountains.CorgiEngine.CharacterJump::set_CanJumpStop(System.Boolean)
extern void CharacterJump_set_CanJumpStop_m3B47C533EBF571F54A7AD00F4C83BC26F6F50F9F (void);
// 0x0000024B System.Boolean MoreMountains.CorgiEngine.CharacterJump::get_JumpAuthorized()
extern void CharacterJump_get_JumpAuthorized_m343EB711FC1BB2127A1619056FA0FAE51090BC48 (void);
// 0x0000024C System.Void MoreMountains.CorgiEngine.CharacterJump::Initialization()
extern void CharacterJump_Initialization_mB486F49AD8225AB4975C5682E7E0E78F3ACFFA44 (void);
// 0x0000024D System.Void MoreMountains.CorgiEngine.CharacterJump::HandleInput()
extern void CharacterJump_HandleInput_m4AF6DA51988DA51F924FF7829DD2BC347E681F24 (void);
// 0x0000024E System.Void MoreMountains.CorgiEngine.CharacterJump::ProcessAbility()
extern void CharacterJump_ProcessAbility_mCC5BC6151D9CA8DFD5EA2813111BA05710CB858D (void);
// 0x0000024F System.Boolean MoreMountains.CorgiEngine.CharacterJump::EvaluateJumpTimeWindow()
extern void CharacterJump_EvaluateJumpTimeWindow_mBBE81D6241209E4CDE31B24E274EAC2C640C203A (void);
// 0x00000250 System.Boolean MoreMountains.CorgiEngine.CharacterJump::EvaluateJumpConditions()
extern void CharacterJump_EvaluateJumpConditions_mFF4C384FFF197CC6E19BF1C44704D2911D8F6606 (void);
// 0x00000251 System.Void MoreMountains.CorgiEngine.CharacterJump::JumpStart()
extern void CharacterJump_JumpStart_m46E870F1043E2A68E0E400036258E4EA2DC7A44A (void);
// 0x00000252 System.Void MoreMountains.CorgiEngine.CharacterJump::SetCanJumpStop(System.Boolean)
extern void CharacterJump_SetCanJumpStop_m347D41BAB7A7005BA103800A1158513F91E7F68B (void);
// 0x00000253 System.Boolean MoreMountains.CorgiEngine.CharacterJump::JumpDownFromOneWayPlatform()
extern void CharacterJump_JumpDownFromOneWayPlatform_mF267F3712B2F28C60C8AA1F76B4AF492FEC27C8A (void);
// 0x00000254 System.Void MoreMountains.CorgiEngine.CharacterJump::JumpFromMovingPlatform()
extern void CharacterJump_JumpFromMovingPlatform_mA6393E231348C3005E6C210BB5B5F2F69A50780A (void);
// 0x00000255 System.Void MoreMountains.CorgiEngine.CharacterJump::JumpStop()
extern void CharacterJump_JumpStop_mF42153B2690FE8F15ACE9C1E6CA6E751B6638EF9 (void);
// 0x00000256 System.Void MoreMountains.CorgiEngine.CharacterJump::ResetNumberOfJumps()
extern void CharacterJump_ResetNumberOfJumps_m4CC88B64D31BD0B195B4D030D75A950DC1B7FFFD (void);
// 0x00000257 System.Void MoreMountains.CorgiEngine.CharacterJump::SetJumpFlags()
extern void CharacterJump_SetJumpFlags_m138ED1D5F4FDB5D7B75987E8939461C9B7AB9A45 (void);
// 0x00000258 System.Void MoreMountains.CorgiEngine.CharacterJump::UpdateController()
extern void CharacterJump_UpdateController_m29F2D6D04A9163A75C7C75FBD546F552E855C9A2 (void);
// 0x00000259 System.Void MoreMountains.CorgiEngine.CharacterJump::SetNumberOfJumpsLeft(System.Int32)
extern void CharacterJump_SetNumberOfJumpsLeft_m4B30D7DE3B11F7CA383C8113B9C4DE37A046EDC5 (void);
// 0x0000025A System.Void MoreMountains.CorgiEngine.CharacterJump::ResetJumpButtonReleased()
extern void CharacterJump_ResetJumpButtonReleased_mC4AD72B4311EB1413998582C080A90C013F5CC2A (void);
// 0x0000025B System.Void MoreMountains.CorgiEngine.CharacterJump::InitializeAnimatorParameters()
extern void CharacterJump_InitializeAnimatorParameters_m47F0E80537E6352C30383CAAEABC43F1ABEB9F8C (void);
// 0x0000025C System.Void MoreMountains.CorgiEngine.CharacterJump::UpdateAnimator()
extern void CharacterJump_UpdateAnimator_mE7DF9762B523A01B59BE1B33B3972669412A465E (void);
// 0x0000025D System.Void MoreMountains.CorgiEngine.CharacterJump::ResetAbility()
extern void CharacterJump_ResetAbility_m8D92C29A98ED82969B073854EA1609B7FE87A083 (void);
// 0x0000025E System.Void MoreMountains.CorgiEngine.CharacterJump::.ctor()
extern void CharacterJump__ctor_m83EA4AA8422B154777B31BECEB53A24D40ED2117 (void);
// 0x0000025F UnityEngine.Vector2 MoreMountains.CorgiEngine.CharacterLadder::get_CurrentLadderClimbingSpeed()
extern void CharacterLadder_get_CurrentLadderClimbingSpeed_mF741CFCE119A9647BB7A68B37ABAE64894F6017E (void);
// 0x00000260 System.Void MoreMountains.CorgiEngine.CharacterLadder::set_CurrentLadderClimbingSpeed(UnityEngine.Vector2)
extern void CharacterLadder_set_CurrentLadderClimbingSpeed_m418E6C2FFAD4BB17C0805D1FA7BFCF7E1126EF0C (void);
// 0x00000261 System.Boolean MoreMountains.CorgiEngine.CharacterLadder::get_LadderColliding()
extern void CharacterLadder_get_LadderColliding_m1CB99154D477CF73B581722C47550BF67B25A847 (void);
// 0x00000262 MoreMountains.CorgiEngine.Ladder MoreMountains.CorgiEngine.CharacterLadder::get_CurrentLadder()
extern void CharacterLadder_get_CurrentLadder_m47CED0A355E658F81EEDD3DFC5DCED321C000C85 (void);
// 0x00000263 System.Void MoreMountains.CorgiEngine.CharacterLadder::set_CurrentLadder(MoreMountains.CorgiEngine.Ladder)
extern void CharacterLadder_set_CurrentLadder_mE763CBEB2C36423D079F9F5EA957336C4BDD0BA6 (void);
// 0x00000264 MoreMountains.CorgiEngine.Ladder MoreMountains.CorgiEngine.CharacterLadder::get_HighestLadder()
extern void CharacterLadder_get_HighestLadder_m857BB0C178F96F91146675690825C95EB377D4ED (void);
// 0x00000265 System.Void MoreMountains.CorgiEngine.CharacterLadder::set_HighestLadder(MoreMountains.CorgiEngine.Ladder)
extern void CharacterLadder_set_HighestLadder_mBE4B15F71B1C9EB4B186A9D9A1EE696BF8177F8F (void);
// 0x00000266 MoreMountains.CorgiEngine.Ladder MoreMountains.CorgiEngine.CharacterLadder::get_LowestLadder()
extern void CharacterLadder_get_LowestLadder_m4CC21572C05DE0F94CD5D32BB2E20C9FC761B3CF (void);
// 0x00000267 System.Void MoreMountains.CorgiEngine.CharacterLadder::set_LowestLadder(MoreMountains.CorgiEngine.Ladder)
extern void CharacterLadder_set_LowestLadder_mB1B2927FDBE257A7CBE79E1C5E3935CD75B1DEEC (void);
// 0x00000268 System.Void MoreMountains.CorgiEngine.CharacterLadder::Initialization()
extern void CharacterLadder_Initialization_mFBA49108115D37BF0C0D81FA30DDD68C0C3B7DEC (void);
// 0x00000269 System.Void MoreMountains.CorgiEngine.CharacterLadder::ProcessAbility()
extern void CharacterLadder_ProcessAbility_mB89918C0B95300A5755D58116128A8D903A2A5CC (void);
// 0x0000026A System.Void MoreMountains.CorgiEngine.CharacterLadder::AddCollidingLadder(UnityEngine.Collider2D)
extern void CharacterLadder_AddCollidingLadder_m2FFC962E473B6AF920F4B0D766C854FDA0CD7BA0 (void);
// 0x0000026B System.Void MoreMountains.CorgiEngine.CharacterLadder::RemoveCollidingLadder(UnityEngine.Collider2D)
extern void CharacterLadder_RemoveCollidingLadder_m2C50CA133D8B05141D8E79E67A23856E3445E548 (void);
// 0x0000026C System.Void MoreMountains.CorgiEngine.CharacterLadder::ComputeClosestLadder()
extern void CharacterLadder_ComputeClosestLadder_mEF92737D896CA320003FE87703FE5506C1976D1D (void);
// 0x0000026D System.Void MoreMountains.CorgiEngine.CharacterLadder::HandleLadderClimbing()
extern void CharacterLadder_HandleLadderClimbing_m699B0B6D9B9CE11448956838815F24D653412AA8 (void);
// 0x0000026E System.Void MoreMountains.CorgiEngine.CharacterLadder::HandleFeedbacks()
extern void CharacterLadder_HandleFeedbacks_m6E768DFCF50A62611F389AC6A8FA4C58DE1FFDE7 (void);
// 0x0000026F System.Void MoreMountains.CorgiEngine.CharacterLadder::StartClimbing()
extern void CharacterLadder_StartClimbing_m39887A8BFB16578B43BC76A03C457DCFA13AB311 (void);
// 0x00000270 System.Void MoreMountains.CorgiEngine.CharacterLadder::StartClimbingDown()
extern void CharacterLadder_StartClimbingDown_m7038497793937E1C258ADAA735AB26B3FC66D945 (void);
// 0x00000271 System.Void MoreMountains.CorgiEngine.CharacterLadder::SetClimbingState()
extern void CharacterLadder_SetClimbingState_m3F4CB4C26ABCF530ABFB9E8D6D5D18789FA2F83B (void);
// 0x00000272 System.Void MoreMountains.CorgiEngine.CharacterLadder::Climbing()
extern void CharacterLadder_Climbing_mEE6178EA236BAB055B9057CB68EDC45A465576B0 (void);
// 0x00000273 System.Void MoreMountains.CorgiEngine.CharacterLadder::GetOffTheLadder()
extern void CharacterLadder_GetOffTheLadder_m453CC335D7C0FE2E0CDE7BA53418031EE9669000 (void);
// 0x00000274 System.Boolean MoreMountains.CorgiEngine.CharacterLadder::AboveLadderPlatform()
extern void CharacterLadder_AboveLadderPlatform_m9B243E0318C0ABA2A661DFA52C07354179BC69B1 (void);
// 0x00000275 System.Void MoreMountains.CorgiEngine.CharacterLadder::OnDeath()
extern void CharacterLadder_OnDeath_mA786C157F2F294E81A99EB8113514C8612F8A00D (void);
// 0x00000276 System.Void MoreMountains.CorgiEngine.CharacterLadder::InitializeAnimatorParameters()
extern void CharacterLadder_InitializeAnimatorParameters_mE442C41A01FAB8B4B5C9724EED093DFB0C5B149E (void);
// 0x00000277 System.Void MoreMountains.CorgiEngine.CharacterLadder::UpdateAnimator()
extern void CharacterLadder_UpdateAnimator_mF32F2391A6FF526B7210CCEB36B3A3F56CA57BA3 (void);
// 0x00000278 System.Void MoreMountains.CorgiEngine.CharacterLadder::ResetAbility()
extern void CharacterLadder_ResetAbility_m525C10B61B34F9074B0638EFE05F0B20D0500686 (void);
// 0x00000279 System.Void MoreMountains.CorgiEngine.CharacterLadder::.ctor()
extern void CharacterLadder__ctor_m252755798E393F6B4E69F59E85554A42C64CD0AC (void);
// 0x0000027A System.String MoreMountains.CorgiEngine.CharacterLedgeHang::HelpBoxText()
extern void CharacterLedgeHang_HelpBoxText_m44BE74C932FB2BAC022E81032ECD6F47ADD2460C (void);
// 0x0000027B System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::Initialization()
extern void CharacterLedgeHang_Initialization_m19A09E9E5E888BF3AAD40BCC6875895F7A2034DB (void);
// 0x0000027C System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::HandleInput()
extern void CharacterLedgeHang_HandleInput_m23394E42CB858EAA4F50061D3C202F2E5F7EE130 (void);
// 0x0000027D System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::ProcessAbility()
extern void CharacterLedgeHang_ProcessAbility_m897D3C9135171B9B9CCB40B014239A89102C00D8 (void);
// 0x0000027E System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::OnMMEvent(MoreMountains.CorgiEngine.LedgeEvent)
extern void CharacterLedgeHang_OnMMEvent_mFAC4697E6D1F9A34D27622DBAC594EBDD50CDBEF (void);
// 0x0000027F System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::StartGrabbingLedge(MoreMountains.CorgiEngine.Ledge)
extern void CharacterLedgeHang_StartGrabbingLedge_mD7EDEFC2EEB26829633D9404B89FFBA3FB6450E5 (void);
// 0x00000280 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::HandleLedge()
extern void CharacterLedgeHang_HandleLedge_m6D03BF145A8D54EF0B5B3238DB289972A319DF54 (void);
// 0x00000281 System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterLedgeHang::Climb()
extern void CharacterLedgeHang_Climb_mD48C6E5B8F47E0E1754AEFBFDAA443780A9FD672 (void);
// 0x00000282 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::DetachFromLedge()
extern void CharacterLedgeHang_DetachFromLedge_m57B04D4D4133DA225AE992C1286CC89CBF9286FD (void);
// 0x00000283 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::InitializeAnimatorParameters()
extern void CharacterLedgeHang_InitializeAnimatorParameters_m847C49F557CB4880CB593989AF5A6C04526AB0F7 (void);
// 0x00000284 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::UpdateAnimator()
extern void CharacterLedgeHang_UpdateAnimator_mBD4F877684DE376B3CB7B0B7A5FD5E43EC6B264D (void);
// 0x00000285 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::OnEnable()
extern void CharacterLedgeHang_OnEnable_m00A8D583BC41D976127999DD160888E65FBBD987 (void);
// 0x00000286 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::OnDisable()
extern void CharacterLedgeHang_OnDisable_mE154E9F3A536E65A6CCBB6877CBE4188BDCF024E (void);
// 0x00000287 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::ResetAbility()
extern void CharacterLedgeHang_ResetAbility_m8AE283C9A2ED96E942775143BFB2BEF2548C8E63 (void);
// 0x00000288 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang::.ctor()
extern void CharacterLedgeHang__ctor_mD8E82C5342E059B5172CE34DB1B88D0A0655BF76 (void);
// 0x00000289 System.Void MoreMountains.CorgiEngine.CharacterLedgeHang/<Climb>d__14::.ctor(System.Int32)
extern void U3CClimbU3Ed__14__ctor_m37E350C3777D58F5F89A5794E4689EEBB5C6A5DD (void);
// 0x0000028A System.Void MoreMountains.CorgiEngine.CharacterLedgeHang/<Climb>d__14::System.IDisposable.Dispose()
extern void U3CClimbU3Ed__14_System_IDisposable_Dispose_m58FFA52A9E3A1DFA18330EE8C49382F20EBA108B (void);
// 0x0000028B System.Boolean MoreMountains.CorgiEngine.CharacterLedgeHang/<Climb>d__14::MoveNext()
extern void U3CClimbU3Ed__14_MoveNext_m7E08F51BE05111CEE3E8F4A53FE2DD47613C72C1 (void);
// 0x0000028C System.Object MoreMountains.CorgiEngine.CharacterLedgeHang/<Climb>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClimbU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE14886A00E4D0C59D32593DC7CE90D02B13B821 (void);
// 0x0000028D System.Void MoreMountains.CorgiEngine.CharacterLedgeHang/<Climb>d__14::System.Collections.IEnumerator.Reset()
extern void U3CClimbU3Ed__14_System_Collections_IEnumerator_Reset_m46636A25507783A4EFF15FD0D84AAA45E89638BF (void);
// 0x0000028E System.Object MoreMountains.CorgiEngine.CharacterLedgeHang/<Climb>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CClimbU3Ed__14_System_Collections_IEnumerator_get_Current_mE30A62629781ECEA058D1AF4F3C0E636D8FD95F7 (void);
// 0x0000028F System.String MoreMountains.CorgiEngine.CharacterLookUp::HelpBoxText()
extern void CharacterLookUp_HelpBoxText_mD4BEC6E4291F7C95CE59B1C1056C583954EC8C16 (void);
// 0x00000290 System.Void MoreMountains.CorgiEngine.CharacterLookUp::HandleInput()
extern void CharacterLookUp_HandleInput_m2147EA5E7FA56D577FE48C50610791CE9E581BA6 (void);
// 0x00000291 System.Void MoreMountains.CorgiEngine.CharacterLookUp::LookUp()
extern void CharacterLookUp_LookUp_m16579F1B391C369F957E385379C73DE3FBE3C815 (void);
// 0x00000292 System.Void MoreMountains.CorgiEngine.CharacterLookUp::ProcessAbility()
extern void CharacterLookUp_ProcessAbility_m79440B39223FAB6A0217E7EDABD135EA4ADE74AE (void);
// 0x00000293 System.Void MoreMountains.CorgiEngine.CharacterLookUp::ExitLookUp()
extern void CharacterLookUp_ExitLookUp_m714D9ED818E16464C6743FCFA2AB1838CA2A570E (void);
// 0x00000294 System.Void MoreMountains.CorgiEngine.CharacterLookUp::InitializeAnimatorParameters()
extern void CharacterLookUp_InitializeAnimatorParameters_m27AA4363511A744538146A4C96CDAE898B2A67DB (void);
// 0x00000295 System.Void MoreMountains.CorgiEngine.CharacterLookUp::UpdateAnimator()
extern void CharacterLookUp_UpdateAnimator_mA3EF4BDF0CB826DA7F9106A92B221948A3E8E8B3 (void);
// 0x00000296 System.Void MoreMountains.CorgiEngine.CharacterLookUp::ResetAbility()
extern void CharacterLookUp_ResetAbility_mE4EF2F13C635554598EB1235A0BB1D28CB1C0CAE (void);
// 0x00000297 System.Void MoreMountains.CorgiEngine.CharacterLookUp::.ctor()
extern void CharacterLookUp__ctor_m13D3A4A007134CF2F310668CAD15AD4331671F27 (void);
// 0x00000298 System.Void MoreMountains.CorgiEngine.CharacterParticles::ProcessAbility()
extern void CharacterParticles_ProcessAbility_m1CA903DF95C99D8744F7A28B54B6F2920B8AB34A (void);
// 0x00000299 System.Void MoreMountains.CorgiEngine.CharacterParticles::HandleParticleSystem(UnityEngine.ParticleSystem,MoreMountains.CorgiEngine.CharacterStates/MovementStates)
extern void CharacterParticles_HandleParticleSystem_mC794379577002F3CF262E838D6A8E90DE28789F7 (void);
// 0x0000029A System.Void MoreMountains.CorgiEngine.CharacterParticles::.ctor()
extern void CharacterParticles__ctor_m894527126FA8DBD2915091B3EC90D4E710DDCAE2 (void);
// 0x0000029B System.String MoreMountains.CorgiEngine.CharacterPause::HelpBoxText()
extern void CharacterPause_HelpBoxText_mD25F40DF4C1EC1EFC2254A1FF0B2A1CB7275C858 (void);
// 0x0000029C System.Void MoreMountains.CorgiEngine.CharacterPause::HandleInput()
extern void CharacterPause_HandleInput_m6B87E58F38C62B926C1098D2D4AC9B57A27A1969 (void);
// 0x0000029D System.Void MoreMountains.CorgiEngine.CharacterPause::TriggerPause()
extern void CharacterPause_TriggerPause_mD3F6F763036AE3A59701290284A79CA468852119 (void);
// 0x0000029E System.Void MoreMountains.CorgiEngine.CharacterPause::PauseCharacter()
extern void CharacterPause_PauseCharacter_mDC5073CACF2A06C97DFCBA7A30E29240DDCE4624 (void);
// 0x0000029F System.Void MoreMountains.CorgiEngine.CharacterPause::UnPauseCharacter()
extern void CharacterPause_UnPauseCharacter_mFD7B25EF833FC6FE401576BFD5C6BDC915DC0353 (void);
// 0x000002A0 System.Void MoreMountains.CorgiEngine.CharacterPause::.ctor()
extern void CharacterPause__ctor_m4B6AC64D4A41EDF867C4886B5C0FF31BA9C8C576 (void);
// 0x000002A1 System.Boolean MoreMountains.CorgiEngine.CharacterPersistence::get_Initialized()
extern void CharacterPersistence_get_Initialized_mCE134AC9AE76594C251054F7926D736356728817 (void);
// 0x000002A2 System.Void MoreMountains.CorgiEngine.CharacterPersistence::set_Initialized(System.Boolean)
extern void CharacterPersistence_set_Initialized_mBEA794708E48987FBBEDCB5B3C527AB02B16D1E5 (void);
// 0x000002A3 System.Void MoreMountains.CorgiEngine.CharacterPersistence::Initialization()
extern void CharacterPersistence_Initialization_m1AE2B32FEF35F8C961ADDAE621EAB9419F8139AD (void);
// 0x000002A4 System.Void MoreMountains.CorgiEngine.CharacterPersistence::OnDeath()
extern void CharacterPersistence_OnDeath_mC1CC3BF4BB80152A7DA4617A17B67BBFC93BD03D (void);
// 0x000002A5 System.Void MoreMountains.CorgiEngine.CharacterPersistence::OnMMEvent(MoreMountains.Tools.MMGameEvent)
extern void CharacterPersistence_OnMMEvent_m17F0CE4AF761021194BAE050C8D64D6B1A1D5265 (void);
// 0x000002A6 System.Void MoreMountains.CorgiEngine.CharacterPersistence::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void CharacterPersistence_OnMMEvent_mA99F90E06D72FA537515B5B91FF6840663F82BB3 (void);
// 0x000002A7 System.Void MoreMountains.CorgiEngine.CharacterPersistence::SaveCharacter()
extern void CharacterPersistence_SaveCharacter_m9BAAB1F813A7D5A26445F1CD517C61AD7DA00E3E (void);
// 0x000002A8 System.Void MoreMountains.CorgiEngine.CharacterPersistence::ClearSavedCharacter()
extern void CharacterPersistence_ClearSavedCharacter_m868DD83AFFF5EE451E76CDCE4034ED79926A4045 (void);
// 0x000002A9 System.Void MoreMountains.CorgiEngine.CharacterPersistence::OnEnable()
extern void CharacterPersistence_OnEnable_m217873FBD1DBB1E32A8ADD25231F6B75CADE696A (void);
// 0x000002AA System.Void MoreMountains.CorgiEngine.CharacterPersistence::OnDestroy()
extern void CharacterPersistence_OnDestroy_mFAB8620C18BF4E57052418AA709CBF5F3223080A (void);
// 0x000002AB System.Void MoreMountains.CorgiEngine.CharacterPersistence::.ctor()
extern void CharacterPersistence__ctor_mDCD174A49972633D6FE2E68A68100871EEC921B3 (void);
// 0x000002AC System.String MoreMountains.CorgiEngine.CharacterPush::HelpBoxText()
extern void CharacterPush_HelpBoxText_m44AE0FE1FE0181A0CB8755FEB3181A5994D15F6E (void);
// 0x000002AD System.Void MoreMountains.CorgiEngine.CharacterPush::Initialization()
extern void CharacterPush_Initialization_m3426A73F1EA56FA89031CCED3A7B081A1DA1370E (void);
// 0x000002AE System.Void MoreMountains.CorgiEngine.CharacterPush::ProcessAbility()
extern void CharacterPush_ProcessAbility_m69839AD0ACE8DE4BD476AD5A3223C30E8A68A8C0 (void);
// 0x000002AF System.Void MoreMountains.CorgiEngine.CharacterPush::InitializeAnimatorParameters()
extern void CharacterPush_InitializeAnimatorParameters_m14DCB06F8DF2502E77FDD70C27C18BF033E2F044 (void);
// 0x000002B0 System.Void MoreMountains.CorgiEngine.CharacterPush::UpdateAnimator()
extern void CharacterPush_UpdateAnimator_mAE96E37D7FACD1235765302EF112AC1CC4890C51 (void);
// 0x000002B1 System.Void MoreMountains.CorgiEngine.CharacterPush::ResetAbility()
extern void CharacterPush_ResetAbility_mF733E6A9567872CEE531BA06F07BCCFDDCFB9CD5 (void);
// 0x000002B2 System.Void MoreMountains.CorgiEngine.CharacterPush::.ctor()
extern void CharacterPush__ctor_mA6B3208F0376E137369439946DA7EE3E4B43A8E3 (void);
// 0x000002B3 System.String MoreMountains.CorgiEngine.CharacterPushCorgiController::HelpBoxText()
extern void CharacterPushCorgiController_HelpBoxText_m98D698793788FEE74539817A1D41AD626C6F72F1 (void);
// 0x000002B4 System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::Initialization()
extern void CharacterPushCorgiController_Initialization_m9F36E999A8930D97D893A702B50BCADBF2FBBAFC (void);
// 0x000002B5 System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::ProcessAbility()
extern void CharacterPushCorgiController_ProcessAbility_m7FD5C3E42C496B39EF09C46B9D9581EFC5FD1307 (void);
// 0x000002B6 System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::CheckForPushEnd()
extern void CharacterPushCorgiController_CheckForPushEnd_m2E818100133035584D125F17EE5A811FD6AB88E6 (void);
// 0x000002B7 System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::StopPushing()
extern void CharacterPushCorgiController_StopPushing_m8DB15144DD916948D56239188CC5727666AEFD07 (void);
// 0x000002B8 System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::InitializeAnimatorParameters()
extern void CharacterPushCorgiController_InitializeAnimatorParameters_mA2B0A56353D53FE60EA00B9681FF4467F7B4DDB1 (void);
// 0x000002B9 System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::UpdateAnimator()
extern void CharacterPushCorgiController_UpdateAnimator_mCB5A2DFF95EFFF219FA5550C618D3A4732833E68 (void);
// 0x000002BA System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::ResetAbility()
extern void CharacterPushCorgiController_ResetAbility_m93E18378FB446DFB24BC273B9FC6A26DCAB2C06E (void);
// 0x000002BB System.Void MoreMountains.CorgiEngine.CharacterPushCorgiController::.ctor()
extern void CharacterPushCorgiController__ctor_m1EBF8E3B8BAC9D5206F9ADF26536CC3A40A33C86 (void);
// 0x000002BC System.String MoreMountains.CorgiEngine.CharacterRoll::HelpBoxText()
extern void CharacterRoll_HelpBoxText_mDDDACC20EC9D23C68BD7F8D0C7BA544E42DCC70C (void);
// 0x000002BD System.Void MoreMountains.CorgiEngine.CharacterRoll::Initialization()
extern void CharacterRoll_Initialization_m59EDBC47AF75AE329A31CEB2A71B3C3A452001EB (void);
// 0x000002BE System.Void MoreMountains.CorgiEngine.CharacterRoll::HandleInput()
extern void CharacterRoll_HandleInput_mB23C4FD6500B04754841FAC1432F5FAE66B4B40C (void);
// 0x000002BF System.Void MoreMountains.CorgiEngine.CharacterRoll::ProcessAbility()
extern void CharacterRoll_ProcessAbility_m80C99742F5F11AEED2519150B2508DBB3E832D5C (void);
// 0x000002C0 System.Void MoreMountains.CorgiEngine.CharacterRoll::StartRoll()
extern void CharacterRoll_StartRoll_mA51EA58D9B9400218912011C5785E01F9B35E67F (void);
// 0x000002C1 System.Boolean MoreMountains.CorgiEngine.CharacterRoll::RollConditions()
extern void CharacterRoll_RollConditions_mEE1C3378CC6BB412304C47DBC51AB3E9C3841D26 (void);
// 0x000002C2 System.Void MoreMountains.CorgiEngine.CharacterRoll::HandleAmountOfRollsLeft()
extern void CharacterRoll_HandleAmountOfRollsLeft_m7A0C270758B7838C6D81FFA409559C87932B540F (void);
// 0x000002C3 System.Void MoreMountains.CorgiEngine.CharacterRoll::SetSuccessiveRollsLeft(System.Int32)
extern void CharacterRoll_SetSuccessiveRollsLeft_m67EDC1BA2CD7772DC4AD169854D2E0EFDC5C9DAD (void);
// 0x000002C4 System.Boolean MoreMountains.CorgiEngine.CharacterRoll::RollAuthorized()
extern void CharacterRoll_RollAuthorized_m72F3007471B2E962D21F2AAC953B1DC84A967AAB (void);
// 0x000002C5 System.Void MoreMountains.CorgiEngine.CharacterRoll::InitiateRoll()
extern void CharacterRoll_InitiateRoll_m1A2C4AEFDB4D4B88E35CC45A73A6F1DABABA4D4B (void);
// 0x000002C6 System.Void MoreMountains.CorgiEngine.CharacterRoll::ComputeRollDirection()
extern void CharacterRoll_ComputeRollDirection_mBF5EB40907852431D53139239A6142058B24D455 (void);
// 0x000002C7 System.Void MoreMountains.CorgiEngine.CharacterRoll::CheckFlipCharacter()
extern void CharacterRoll_CheckFlipCharacter_m8F52C37D2C3E1A38F31D2C32657CDC231635C975 (void);
// 0x000002C8 System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterRoll::RollCoroutine()
extern void CharacterRoll_RollCoroutine_mCACFE8CE23A29A26BF894D43FC176B08E4CA9BA2 (void);
// 0x000002C9 System.Void MoreMountains.CorgiEngine.CharacterRoll::StopRoll()
extern void CharacterRoll_StopRoll_m47CF2F5A249E8E5CBB83D18FD1E5BC368E07CE08 (void);
// 0x000002CA System.Void MoreMountains.CorgiEngine.CharacterRoll::InitializeAnimatorParameters()
extern void CharacterRoll_InitializeAnimatorParameters_m708E410587CD9951EA948999564D86F3C21A5F47 (void);
// 0x000002CB System.Void MoreMountains.CorgiEngine.CharacterRoll::UpdateAnimator()
extern void CharacterRoll_UpdateAnimator_m10418C3D3F296508C9648B645C46F9F2266C806D (void);
// 0x000002CC System.Void MoreMountains.CorgiEngine.CharacterRoll::ResetAbility()
extern void CharacterRoll_ResetAbility_m2B2CD53F68D0DB1FC0830FB5E35CF036355AA6DF (void);
// 0x000002CD System.Void MoreMountains.CorgiEngine.CharacterRoll::.ctor()
extern void CharacterRoll__ctor_m84B1B073895BD526DEC990AB2BF69DA140C2D1AB (void);
// 0x000002CE System.Void MoreMountains.CorgiEngine.CharacterRoll/<RollCoroutine>d__38::.ctor(System.Int32)
extern void U3CRollCoroutineU3Ed__38__ctor_m1C6BFDBF640A37D6699926D87A2381771063418E (void);
// 0x000002CF System.Void MoreMountains.CorgiEngine.CharacterRoll/<RollCoroutine>d__38::System.IDisposable.Dispose()
extern void U3CRollCoroutineU3Ed__38_System_IDisposable_Dispose_m6D896ECD5500046E0C3FBD52644CB0A78FB0EBEE (void);
// 0x000002D0 System.Boolean MoreMountains.CorgiEngine.CharacterRoll/<RollCoroutine>d__38::MoveNext()
extern void U3CRollCoroutineU3Ed__38_MoveNext_m0B61D0EEA1F6D258C23F02C0E4425A8E085A9D74 (void);
// 0x000002D1 System.Object MoreMountains.CorgiEngine.CharacterRoll/<RollCoroutine>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRollCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD20C0F7A572918972853B8AADD236967BB59A715 (void);
// 0x000002D2 System.Void MoreMountains.CorgiEngine.CharacterRoll/<RollCoroutine>d__38::System.Collections.IEnumerator.Reset()
extern void U3CRollCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_m20F2CC9BDF30427B4A9C2079A8A49C5118953C06 (void);
// 0x000002D3 System.Object MoreMountains.CorgiEngine.CharacterRoll/<RollCoroutine>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CRollCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mB5C1744E033A0BFB2C7CFDF30CE8A8B1DC08262A (void);
// 0x000002D4 System.String MoreMountains.CorgiEngine.CharacterRun::HelpBoxText()
extern void CharacterRun_HelpBoxText_m822B466F3653C18A8DAF48A9DDEA7F231ABDE7FA (void);
// 0x000002D5 System.Boolean MoreMountains.CorgiEngine.CharacterRun::get_ShouldRun()
extern void CharacterRun_get_ShouldRun_m913343AA7BC27BB680A89970932DE92E462C7DA1 (void);
// 0x000002D6 System.Void MoreMountains.CorgiEngine.CharacterRun::set_ShouldRun(System.Boolean)
extern void CharacterRun_set_ShouldRun_mC83E109B6E606021CB455EA99BBD6E2A240FE2D4 (void);
// 0x000002D7 System.Void MoreMountains.CorgiEngine.CharacterRun::HandleInput()
extern void CharacterRun_HandleInput_mF853EA79814A42443DC4CB797E1EB094B60DDE6F (void);
// 0x000002D8 System.Void MoreMountains.CorgiEngine.CharacterRun::ProcessAbility()
extern void CharacterRun_ProcessAbility_mB91B8B32CED93A0BF36D83DFBEC4432606634EE7 (void);
// 0x000002D9 System.Void MoreMountains.CorgiEngine.CharacterRun::HandleRunningExit()
extern void CharacterRun_HandleRunningExit_m52F3A6CD112B408C08E87BB54A0E8A840808EFA6 (void);
// 0x000002DA System.Void MoreMountains.CorgiEngine.CharacterRun::RunStart()
extern void CharacterRun_RunStart_m96C168B7BD6B4050F5110242E2353EB0B5A12404 (void);
// 0x000002DB System.Void MoreMountains.CorgiEngine.CharacterRun::RunStop()
extern void CharacterRun_RunStop_mEA4B7CE3F78C1F9C85D9D94DC226FF6B4B16D342 (void);
// 0x000002DC System.Void MoreMountains.CorgiEngine.CharacterRun::ForceRun(System.Boolean)
extern void CharacterRun_ForceRun_mE16EFD62D4AC4B7837D1D97D0F44940A8102E886 (void);
// 0x000002DD System.Void MoreMountains.CorgiEngine.CharacterRun::StopFeedbacks()
extern void CharacterRun_StopFeedbacks_m47984A547C194994DF19A0D304560AE1B2B4BA08 (void);
// 0x000002DE System.Void MoreMountains.CorgiEngine.CharacterRun::InitializeAnimatorParameters()
extern void CharacterRun_InitializeAnimatorParameters_m2AB2F2181B1D471C55C90CA1BE1007B531A2A25F (void);
// 0x000002DF System.Void MoreMountains.CorgiEngine.CharacterRun::UpdateAnimator()
extern void CharacterRun_UpdateAnimator_m3A04384A129E04B4F0878C46D9A9EBCDF7C484B8 (void);
// 0x000002E0 System.Void MoreMountains.CorgiEngine.CharacterRun::ResetAbility()
extern void CharacterRun_ResetAbility_m5E29BDC88040FA2A4EA429C5872B8C9EAF54340D (void);
// 0x000002E1 System.Void MoreMountains.CorgiEngine.CharacterRun::.ctor()
extern void CharacterRun__ctor_m4E4C7CF843A83E930DF281F407F0DE920C1FC404 (void);
// 0x000002E2 System.Void MoreMountains.CorgiEngine.CharacterSimpleDive::HandleInput()
extern void CharacterSimpleDive_HandleInput_m6C78003FF7033E8568E8FBA02EC5FFF77EE2C017 (void);
// 0x000002E3 System.Void MoreMountains.CorgiEngine.CharacterSimpleDive::.ctor()
extern void CharacterSimpleDive__ctor_m740681F3F56C0C4E8B7BE6B6344B04A1F335B401 (void);
// 0x000002E4 System.String MoreMountains.CorgiEngine.CharacterSlopeOrientation::HelpBoxText()
extern void CharacterSlopeOrientation_HelpBoxText_m641C09D7071C08CEBA14C2DC9E37860897CE0CC1 (void);
// 0x000002E5 System.Void MoreMountains.CorgiEngine.CharacterSlopeOrientation::Initialization()
extern void CharacterSlopeOrientation_Initialization_mEBCFD09FEB6EE271E8E39F1026DC40F99166C7C1 (void);
// 0x000002E6 System.Void MoreMountains.CorgiEngine.CharacterSlopeOrientation::ProcessAbility()
extern void CharacterSlopeOrientation_ProcessAbility_m2D27B5493A28F186D8F772E1568881E388D33AC2 (void);
// 0x000002E7 System.Single MoreMountains.CorgiEngine.CharacterSlopeOrientation::DetermineAngle()
extern void CharacterSlopeOrientation_DetermineAngle_m4EF6A0F661AD04C4A73740C0B2CFAA412405251C (void);
// 0x000002E8 System.Single MoreMountains.CorgiEngine.CharacterSlopeOrientation::ComputeSlopeAngle(UnityEngine.Vector2)
extern void CharacterSlopeOrientation_ComputeSlopeAngle_m028153D86FFF34EE13B13D743AAED8A109E2AD03 (void);
// 0x000002E9 System.Void MoreMountains.CorgiEngine.CharacterSlopeOrientation::.ctor()
extern void CharacterSlopeOrientation__ctor_m3FD8822BB498FC4850F2F05BC01F92CAB90E38C4 (void);
// 0x000002EA System.Void MoreMountains.CorgiEngine.CharacterSpeedState::.ctor()
extern void CharacterSpeedState__ctor_m2D23E9F206A2B051BFC3DFF219B7E0BEC4EF50EB (void);
// 0x000002EB System.Void MoreMountains.CorgiEngine.CharacterSpeed::LateUpdate()
extern void CharacterSpeed_LateUpdate_mD1211ED628DBCCE022E5134104188A93CDDF41C8 (void);
// 0x000002EC System.Void MoreMountains.CorgiEngine.CharacterSpeed::CheckStates()
extern void CharacterSpeed_CheckStates_m036E2FFD39B7F03231AA8AF85ECE806C6F5A1132 (void);
// 0x000002ED System.Void MoreMountains.CorgiEngine.CharacterSpeed::.ctor()
extern void CharacterSpeed__ctor_m92847BACD87FCDF4B848C091BF1D1620273C4C71 (void);
// 0x000002EE System.String MoreMountains.CorgiEngine.CharacterSpeedAnalysis::HelpBoxText()
extern void CharacterSpeedAnalysis_HelpBoxText_mE61AF4A8DDBCB8DA71CF979D33FA525F7085CD0D (void);
// 0x000002EF System.Void MoreMountains.CorgiEngine.CharacterSpeedAnalysis::Initialization()
extern void CharacterSpeedAnalysis_Initialization_mC9E2BDCCB648EC059F2CFA516414347A414FE93B (void);
// 0x000002F0 System.Void MoreMountains.CorgiEngine.CharacterSpeedAnalysis::LateProcessAbility()
extern void CharacterSpeedAnalysis_LateProcessAbility_mE97DD8B982412BD1C65D4DC98CFE30194EA924C6 (void);
// 0x000002F1 System.Void MoreMountains.CorgiEngine.CharacterSpeedAnalysis::Record()
extern void CharacterSpeedAnalysis_Record_m851E725637A9CC3854D2B20D0BE76171B5BD72B4 (void);
// 0x000002F2 System.Void MoreMountains.CorgiEngine.CharacterSpeedAnalysis::.ctor()
extern void CharacterSpeedAnalysis__ctor_mAE31F4852B6E0EF779B50B20067B6F08A473DCDF (void);
// 0x000002F3 System.String MoreMountains.CorgiEngine.CharacterStairs::HelpBoxText()
extern void CharacterStairs_HelpBoxText_mDA42F1DFEF878EDB4340173F1506077536877453 (void);
// 0x000002F4 System.Void MoreMountains.CorgiEngine.CharacterStairs::Initialization()
extern void CharacterStairs_Initialization_m2014FB6C9B1E72AAEA76D42709CBA5BB790B5BFE (void);
// 0x000002F5 System.Void MoreMountains.CorgiEngine.CharacterStairs::HandleInput()
extern void CharacterStairs_HandleInput_mF352B304D6509C98395BCF7591838A4C4B7EA95D (void);
// 0x000002F6 System.Void MoreMountains.CorgiEngine.CharacterStairs::ProcessAbility()
extern void CharacterStairs_ProcessAbility_m48AC21C234CB59AE4CA402721F075FDC143D06CA (void);
// 0x000002F7 System.Void MoreMountains.CorgiEngine.CharacterStairs::HandleStairsAuthorization()
extern void CharacterStairs_HandleStairsAuthorization_m76788A0A63276F5A40F4A62D9AA402D11B189A21 (void);
// 0x000002F8 System.Void MoreMountains.CorgiEngine.CharacterStairs::HandleEntryBounds()
extern void CharacterStairs_HandleEntryBounds_m8945E3BFD5936C443EEB61D38B6E7689B6E0395E (void);
// 0x000002F9 System.Void MoreMountains.CorgiEngine.CharacterStairs::AuthorizeStairs()
extern void CharacterStairs_AuthorizeStairs_m25066A89C43DA7AB60E49B310920EB2B334C9457 (void);
// 0x000002FA System.Void MoreMountains.CorgiEngine.CharacterStairs::DenyStairs()
extern void CharacterStairs_DenyStairs_m4E8B4BB342900FD160D62052062FC04CAF725352 (void);
// 0x000002FB System.Void MoreMountains.CorgiEngine.CharacterStairs::CheckIfStairsAhead()
extern void CharacterStairs_CheckIfStairsAhead_m0CD7406F2FDF51B8130B4BE673A54ACD84E20BA9 (void);
// 0x000002FC System.Void MoreMountains.CorgiEngine.CharacterStairs::CheckIfStairsBelow()
extern void CharacterStairs_CheckIfStairsBelow_mF6C2778020E19072EFC56FF666E0D9EB3F90DBB1 (void);
// 0x000002FD System.Void MoreMountains.CorgiEngine.CharacterStairs::CheckIfOnStairways()
extern void CharacterStairs_CheckIfOnStairways_mAAF1612CCBC104636EB380DB4F39E87ABE7552C9 (void);
// 0x000002FE System.Void MoreMountains.CorgiEngine.CharacterStairs::InitializeAnimatorParameters()
extern void CharacterStairs_InitializeAnimatorParameters_m15ACA9B3E790392AE4E26EDDDD3BC453763F4377 (void);
// 0x000002FF System.Void MoreMountains.CorgiEngine.CharacterStairs::UpdateAnimator()
extern void CharacterStairs_UpdateAnimator_m5CB8DF23450872E5B4EE954DD6943CEE8F6EA8AD (void);
// 0x00000300 System.Void MoreMountains.CorgiEngine.CharacterStairs::ResetAbility()
extern void CharacterStairs_ResetAbility_m29E88C811D96DE1EEF1466A6C9A91CEE44885B67 (void);
// 0x00000301 System.Void MoreMountains.CorgiEngine.CharacterStairs::.ctor()
extern void CharacterStairs__ctor_m7A9F4F960CA241F7288C070B73B1E9F6A0F41B6D (void);
// 0x00000302 System.String MoreMountains.CorgiEngine.CharacterSwap::HelpBoxText()
extern void CharacterSwap_HelpBoxText_m55A83E80DFA868F347D5961542044D3BEDA0BD72 (void);
// 0x00000303 System.Void MoreMountains.CorgiEngine.CharacterSwap::Initialization()
extern void CharacterSwap_Initialization_m7F10BE81C7BD85714FB4CA4DE1D035BD85112FE9 (void);
// 0x00000304 System.Void MoreMountains.CorgiEngine.CharacterSwap::SwapToThisCharacter()
extern void CharacterSwap_SwapToThisCharacter_m3F08500772ED63B893F893662B3FEA38A1237E8E (void);
// 0x00000305 System.Void MoreMountains.CorgiEngine.CharacterSwap::ResetCharacterSwap()
extern void CharacterSwap_ResetCharacterSwap_mC00460D52CB070C0552F84D6B6CBF33CBFD6BA02 (void);
// 0x00000306 System.Boolean MoreMountains.CorgiEngine.CharacterSwap::Current()
extern void CharacterSwap_Current_m710A4ABB26B72507167685C83FE8E1BA4237A245 (void);
// 0x00000307 System.Void MoreMountains.CorgiEngine.CharacterSwap::.ctor()
extern void CharacterSwap__ctor_m825D826E3210EC247F1223A01E576494B06ABEC4 (void);
// 0x00000308 System.String MoreMountains.CorgiEngine.CharacterSwim::HelpBoxText()
extern void CharacterSwim_HelpBoxText_m9166FE8606D00A2C7A0DEE0F1F0C007B0E21A22F (void);
// 0x00000309 System.Void MoreMountains.CorgiEngine.CharacterSwim::ProcessAbility()
extern void CharacterSwim_ProcessAbility_mD3675D2D5317D458F48271AE7A510C8ADAE19CDC (void);
// 0x0000030A System.Void MoreMountains.CorgiEngine.CharacterSwim::HandleInput()
extern void CharacterSwim_HandleInput_m69C24EEDE07103F418F9E08E20423D5F56F56BE3 (void);
// 0x0000030B System.Void MoreMountains.CorgiEngine.CharacterSwim::Swim()
extern void CharacterSwim_Swim_m8D33F1A222F8FA1B2F163E13B621153E7A4529C4 (void);
// 0x0000030C System.Void MoreMountains.CorgiEngine.CharacterSwim::EnterWater()
extern void CharacterSwim_EnterWater_mC9F5F99E1C09C93D016C6A8F9924F1B143756C84 (void);
// 0x0000030D System.Void MoreMountains.CorgiEngine.CharacterSwim::ExitWater()
extern void CharacterSwim_ExitWater_m277F67996ADC64979E14A2FD1B10EFBE8DFF07E4 (void);
// 0x0000030E System.Void MoreMountains.CorgiEngine.CharacterSwim::InitializeAnimatorParameters()
extern void CharacterSwim_InitializeAnimatorParameters_mC89B9264B3E154DD07FBE564D0E812CED7A32BD8 (void);
// 0x0000030F System.Void MoreMountains.CorgiEngine.CharacterSwim::UpdateAnimator()
extern void CharacterSwim_UpdateAnimator_mC0FA360B38F8E592AE4ECDF32BA8EBE6FE2ACC35 (void);
// 0x00000310 System.Void MoreMountains.CorgiEngine.CharacterSwim::OnDeath()
extern void CharacterSwim_OnDeath_m606A6ACA9AEBBE835C4C0281003EA28270C72497 (void);
// 0x00000311 System.Void MoreMountains.CorgiEngine.CharacterSwim::ResetAbility()
extern void CharacterSwim_ResetAbility_m035E440EDCFDA905BA19F43C23E733A413B46C27 (void);
// 0x00000312 System.Void MoreMountains.CorgiEngine.CharacterSwim::.ctor()
extern void CharacterSwim__ctor_m223F055D11DF172B0C2DFC0A861B0CF9E995EE0A (void);
// 0x00000313 System.Void MoreMountains.CorgiEngine.CharacterSwitchModel::Initialization()
extern void CharacterSwitchModel_Initialization_m9FE30129C6260D0D139411D6900FCAED103EE02F (void);
// 0x00000314 System.Void MoreMountains.CorgiEngine.CharacterSwitchModel::HandleInput()
extern void CharacterSwitchModel_HandleInput_m9A57E35DAD333D966E5EDB9584517843A8EFA91B (void);
// 0x00000315 System.Void MoreMountains.CorgiEngine.CharacterSwitchModel::Flip()
extern void CharacterSwitchModel_Flip_m4535A703C0B2944427A47E0F805E7B70E256DE91 (void);
// 0x00000316 System.Void MoreMountains.CorgiEngine.CharacterSwitchModel::SwitchModel()
extern void CharacterSwitchModel_SwitchModel_m922C496A82E18BFFF9B7C33A80246D1779C43D4D (void);
// 0x00000317 System.Void MoreMountains.CorgiEngine.CharacterSwitchModel::.ctor()
extern void CharacterSwitchModel__ctor_mB572027A0805B3A5DA4A93BA9902B7D7B645CC98 (void);
// 0x00000318 System.Void MoreMountains.CorgiEngine.CharacterTimeControl::HandleInput()
extern void CharacterTimeControl_HandleInput_mC00DD1F83A431F70306620E36CA5DA41563FE58A (void);
// 0x00000319 System.Void MoreMountains.CorgiEngine.CharacterTimeControl::Initialization()
extern void CharacterTimeControl_Initialization_m078E854D6563D7805E07427CD6B659B289B6B3D5 (void);
// 0x0000031A System.Void MoreMountains.CorgiEngine.CharacterTimeControl::TimeControlStart()
extern void CharacterTimeControl_TimeControlStart_m805C347DE7EBA5C0786E72E117AE0260E72620B9 (void);
// 0x0000031B System.Void MoreMountains.CorgiEngine.CharacterTimeControl::TimeControlStop()
extern void CharacterTimeControl_TimeControlStop_mAB52DAD434DAC3E255E3722861772EB0AEE4786C (void);
// 0x0000031C System.Void MoreMountains.CorgiEngine.CharacterTimeControl::ProcessAbility()
extern void CharacterTimeControl_ProcessAbility_mBDC5641CD7970B9A23F1F62FA15775CE42882749 (void);
// 0x0000031D System.Void MoreMountains.CorgiEngine.CharacterTimeControl::ResetAbility()
extern void CharacterTimeControl_ResetAbility_m75ED039D99DA593C55552DA70DB7329FEEACBBA8 (void);
// 0x0000031E System.Void MoreMountains.CorgiEngine.CharacterTimeControl::.ctor()
extern void CharacterTimeControl__ctor_m1656DF502FEE4D73E85A191D55B184967A5A3D82 (void);
// 0x0000031F System.String MoreMountains.CorgiEngine.CharacterWallClinging::HelpBoxText()
extern void CharacterWallClinging_HelpBoxText_mEBE174970FF80F62F75BB5256A97C2489F5421DE (void);
// 0x00000320 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::HandleInput()
extern void CharacterWallClinging_HandleInput_m45F7177B814D6EE046898DA659A552D9413C0E77 (void);
// 0x00000321 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::ProcessAbility()
extern void CharacterWallClinging_ProcessAbility_m2C2EFAA34FCB9651EE19E9BF556CE602DC2499A9 (void);
// 0x00000322 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::WallClinging()
extern void CharacterWallClinging_WallClinging_mB5D0CD095D1743BA6B936C653FD762FC9FBE6B1F (void);
// 0x00000323 System.Boolean MoreMountains.CorgiEngine.CharacterWallClinging::TestForWall()
extern void CharacterWallClinging_TestForWall_m22FC33F662D0707A8EA23B49B41BEB24A563D991 (void);
// 0x00000324 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::EnterWallClinging()
extern void CharacterWallClinging_EnterWallClinging_mFFFCBC7D92D16726749AF021A1E365EAB0023EDA (void);
// 0x00000325 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::ExitWallClinging()
extern void CharacterWallClinging_ExitWallClinging_m039F0D8C3A2EACB83EDFC4621E781904124B5A7A (void);
// 0x00000326 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::ProcessExit()
extern void CharacterWallClinging_ProcessExit_m86C0E23559132CCC8C3ECD3992D705DF2CD1EEDC (void);
// 0x00000327 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::WallClingingLastFrame()
extern void CharacterWallClinging_WallClingingLastFrame_m50DF04C381EBE168B2EC0EFA003DD10F5686ABFE (void);
// 0x00000328 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::OnDeath()
extern void CharacterWallClinging_OnDeath_mD2B0F09CCD5EFC7E09ABEB99F61CA3A3E0408FBE (void);
// 0x00000329 System.Void MoreMountains.CorgiEngine.CharacterWallClinging::InitializeAnimatorParameters()
extern void CharacterWallClinging_InitializeAnimatorParameters_mCC4022284758A48023345069B3AE90896DCB05D6 (void);
// 0x0000032A System.Void MoreMountains.CorgiEngine.CharacterWallClinging::UpdateAnimator()
extern void CharacterWallClinging_UpdateAnimator_m62BC9D9783CE42B0F0CD8114E7CFF2D0004F7890 (void);
// 0x0000032B System.Void MoreMountains.CorgiEngine.CharacterWallClinging::ResetAbility()
extern void CharacterWallClinging_ResetAbility_mF28C2BBB84FB63A02B264786619F69E8A388A6BC (void);
// 0x0000032C System.Void MoreMountains.CorgiEngine.CharacterWallClinging::.ctor()
extern void CharacterWallClinging__ctor_mB4F757937EDE00E184FCC212914FCDD20CF1906E (void);
// 0x0000032D System.String MoreMountains.CorgiEngine.CharacterWalljump::HelpBoxText()
extern void CharacterWalljump_HelpBoxText_mE6FD42B2A742B6678DACBA6C226D9774BACC204B (void);
// 0x0000032E System.Boolean MoreMountains.CorgiEngine.CharacterWalljump::get_WallJumpHappenedThisFrame()
extern void CharacterWalljump_get_WallJumpHappenedThisFrame_m94AFE760D539697067CA0EB8EED751D227116795 (void);
// 0x0000032F System.Void MoreMountains.CorgiEngine.CharacterWalljump::set_WallJumpHappenedThisFrame(System.Boolean)
extern void CharacterWalljump_set_WallJumpHappenedThisFrame_m54CA7B3C8F154A79685A0CB5F3F04F458B6162E8 (void);
// 0x00000330 System.Void MoreMountains.CorgiEngine.CharacterWalljump::Initialization()
extern void CharacterWalljump_Initialization_mFC98394B5F4B87EDD2EC41466258AC66BE78F6E4 (void);
// 0x00000331 System.Void MoreMountains.CorgiEngine.CharacterWalljump::HandleInput()
extern void CharacterWalljump_HandleInput_m4D138FB23F9E013A7EAE312A542DDB87C1227895 (void);
// 0x00000332 System.Void MoreMountains.CorgiEngine.CharacterWalljump::Walljump()
extern void CharacterWalljump_Walljump_m1DD39434CB5CF09407B04A77B351973DCC69DDBF (void);
// 0x00000333 System.Void MoreMountains.CorgiEngine.CharacterWalljump::InitializeAnimatorParameters()
extern void CharacterWalljump_InitializeAnimatorParameters_mEBD552AAE3F5DAAB0965B4E0CFF086E8A179AEB7 (void);
// 0x00000334 System.Void MoreMountains.CorgiEngine.CharacterWalljump::UpdateAnimator()
extern void CharacterWalljump_UpdateAnimator_mDC8FD149BC13BDBC43724E133DCCB46FC1AF45A1 (void);
// 0x00000335 System.Void MoreMountains.CorgiEngine.CharacterWalljump::ResetAbility()
extern void CharacterWalljump_ResetAbility_mFC65EF3BAFB227581AB43BF6E0E86277D824A844 (void);
// 0x00000336 System.Void MoreMountains.CorgiEngine.CharacterWalljump::.ctor()
extern void CharacterWalljump__ctor_m894DB116B10501DB6A65D3DA011944A2A966D186 (void);
// 0x00000337 System.Void MoreMountains.CorgiEngine.CharacterWalljump/OnWallJumpDelegate::.ctor(System.Object,System.IntPtr)
extern void OnWallJumpDelegate__ctor_m7323EC40DC723745C99E57C637E3379C2F142029 (void);
// 0x00000338 System.Void MoreMountains.CorgiEngine.CharacterWalljump/OnWallJumpDelegate::Invoke()
extern void OnWallJumpDelegate_Invoke_m537B66EB87074C2389485E52F2ACE11F00187103 (void);
// 0x00000339 System.IAsyncResult MoreMountains.CorgiEngine.CharacterWalljump/OnWallJumpDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnWallJumpDelegate_BeginInvoke_m35459B9C2970D3BE7CB1B199FBB2703EE3B43106 (void);
// 0x0000033A System.Void MoreMountains.CorgiEngine.CharacterWalljump/OnWallJumpDelegate::EndInvoke(System.IAsyncResult)
extern void OnWallJumpDelegate_EndInvoke_m176398E5C3EEBBDA340240F837133D785B5D2A74 (void);
// 0x0000033B MoreMountains.CorgiEngine.CharacterStates MoreMountains.CorgiEngine.Character::get_CharacterState()
extern void Character_get_CharacterState_m68204DDDC24AD4F0905DFC1BA9199A59EE0C261F (void);
// 0x0000033C System.Void MoreMountains.CorgiEngine.Character::set_CharacterState(MoreMountains.CorgiEngine.CharacterStates)
extern void Character_set_CharacterState_m45358ABCECB9FD83395651DB8F6ACE2943EE78B0 (void);
// 0x0000033D System.Boolean MoreMountains.CorgiEngine.Character::get_IsFacingRight()
extern void Character_get_IsFacingRight_m32FDCE8E5315D865719D4E84152E4573522C5002 (void);
// 0x0000033E System.Void MoreMountains.CorgiEngine.Character::set_IsFacingRight(System.Boolean)
extern void Character_set_IsFacingRight_mEBD11075EDCF1BCD4157BEAC3F2DB6B2E4CD3553 (void);
// 0x0000033F System.Boolean MoreMountains.CorgiEngine.Character::get_Airborne()
extern void Character_get_Airborne_m48A1CCDAFA86ABBEE197CB19108E41AC31784294 (void);
// 0x00000340 MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.Character::get_SceneCamera()
extern void Character_get_SceneCamera_mAF0E69CEAC5DD036815E2562A0D883E68AF5D527 (void);
// 0x00000341 System.Void MoreMountains.CorgiEngine.Character::set_SceneCamera(MoreMountains.CorgiEngine.CameraController)
extern void Character_set_SceneCamera_m07FE020D782C8521447BF8DD323D531E008DAE2F (void);
// 0x00000342 MoreMountains.CorgiEngine.InputManager MoreMountains.CorgiEngine.Character::get_LinkedInputManager()
extern void Character_get_LinkedInputManager_mC3E5510A21C49F4F45F88FF080E2759CA278CA5E (void);
// 0x00000343 System.Void MoreMountains.CorgiEngine.Character::set_LinkedInputManager(MoreMountains.CorgiEngine.InputManager)
extern void Character_set_LinkedInputManager_m3DF4D2D90666A97F98570072161428B8908BE3DE (void);
// 0x00000344 UnityEngine.Animator MoreMountains.CorgiEngine.Character::get__animator()
extern void Character_get__animator_m4BF94C0891905F25D1EC6ACDAD620EDF9FFAF139 (void);
// 0x00000345 System.Void MoreMountains.CorgiEngine.Character::set__animator(UnityEngine.Animator)
extern void Character_set__animator_m9D7D5AA8ADA397FE1A3086A9E9B38073258DDC1C (void);
// 0x00000346 System.Collections.Generic.HashSet`1<System.Int32> MoreMountains.CorgiEngine.Character::get__animatorParameters()
extern void Character_get__animatorParameters_mFF7C1B58BE37E7F9712181D65F5F57898D725ABE (void);
// 0x00000347 System.Void MoreMountains.CorgiEngine.Character::set__animatorParameters(System.Collections.Generic.HashSet`1<System.Int32>)
extern void Character_set__animatorParameters_m652403DF1D0832E6CA0CA5FA4384E465606F6C37 (void);
// 0x00000348 System.Boolean MoreMountains.CorgiEngine.Character::get_CanFlip()
extern void Character_get_CanFlip_mA55F6B5B5841EB91BB449603DF4D4DE0756BC9E5 (void);
// 0x00000349 System.Void MoreMountains.CorgiEngine.Character::set_CanFlip(System.Boolean)
extern void Character_set_CanFlip_m332A336C9D5E1D9C865D20C7754BB26A481D356E (void);
// 0x0000034A System.Void MoreMountains.CorgiEngine.Character::Awake()
extern void Character_Awake_mFB256746E019FFE1185B1BA426E9342E93D898EF (void);
// 0x0000034B System.Void MoreMountains.CorgiEngine.Character::Initialization()
extern void Character_Initialization_m637BEC2E867C1D56003EECE7710B62D2CCB1607A (void);
// 0x0000034C System.Void MoreMountains.CorgiEngine.Character::GetMainCamera()
extern void Character_GetMainCamera_mE2AAB23FB104AD75839252C21D469B9F6546C1A5 (void);
// 0x0000034D System.Void MoreMountains.CorgiEngine.Character::CacheAbilitiesAtInit()
extern void Character_CacheAbilitiesAtInit_m085DD2AE0E4A7A840751175E1CEE566739E8AA8D (void);
// 0x0000034E System.Void MoreMountains.CorgiEngine.Character::CacheAbilities()
extern void Character_CacheAbilities_m78EB1FCE491C71C4F879F384C44CE3F3407EB3F1 (void);
// 0x0000034F T MoreMountains.CorgiEngine.Character::FindAbility()
// 0x00000350 System.Void MoreMountains.CorgiEngine.Character::ChangeAnimator(UnityEngine.Animator)
extern void Character_ChangeAnimator_m7BDE131DE149CAD479D5E8C67D3D67DFB3FE269D (void);
// 0x00000351 System.Void MoreMountains.CorgiEngine.Character::AssignAnimator()
extern void Character_AssignAnimator_m84A644E2090E273FBC0CC6312D1FDAE74154E8E6 (void);
// 0x00000352 System.Void MoreMountains.CorgiEngine.Character::SetInputManager()
extern void Character_SetInputManager_m2C82433F78A1054A6C49C01F8179985CF2526DFC (void);
// 0x00000353 System.Void MoreMountains.CorgiEngine.Character::SetInputManager(MoreMountains.CorgiEngine.InputManager)
extern void Character_SetInputManager_mDCB964751A6BA65660E52BA2C1B91C36CAC79DB0 (void);
// 0x00000354 System.Void MoreMountains.CorgiEngine.Character::UpdateInputManagersInAbilities()
extern void Character_UpdateInputManagersInAbilities_mB13FCCF55C265DD8275D430B75641C849777DAAC (void);
// 0x00000355 System.Void MoreMountains.CorgiEngine.Character::ResetInput()
extern void Character_ResetInput_m41488BFD7AFBD2014FC38E3E0FEAA0B2F10F277F (void);
// 0x00000356 System.Void MoreMountains.CorgiEngine.Character::SetPlayerID(System.String)
extern void Character_SetPlayerID_m060CB74776697D1020D799B44015CD9276237FA5 (void);
// 0x00000357 System.Void MoreMountains.CorgiEngine.Character::Update()
extern void Character_Update_mD84ABA5AFA7DB1BAA4EBF15A817EFB46904D8C47 (void);
// 0x00000358 System.Void MoreMountains.CorgiEngine.Character::EveryFrame()
extern void Character_EveryFrame_m9640EE31870C841DD625A4330022054050575543 (void);
// 0x00000359 System.Void MoreMountains.CorgiEngine.Character::RotateModel()
extern void Character_RotateModel_m847600579DDEBE33BBFB75B3F1E097CA26DFB7D4 (void);
// 0x0000035A System.Void MoreMountains.CorgiEngine.Character::EarlyProcessAbilities()
extern void Character_EarlyProcessAbilities_m278A86B7F7FAA5C5AC5E4E67BE59681DAC56DE20 (void);
// 0x0000035B System.Void MoreMountains.CorgiEngine.Character::ProcessAbilities()
extern void Character_ProcessAbilities_m857B3A09B79FD6E9905D114BC83747CFAEE381AB (void);
// 0x0000035C System.Void MoreMountains.CorgiEngine.Character::LateProcessAbilities()
extern void Character_LateProcessAbilities_m32800B0FCCCE0FFEE599822155B328952002E5C9 (void);
// 0x0000035D System.Void MoreMountains.CorgiEngine.Character::InitializeAnimatorParameters()
extern void Character_InitializeAnimatorParameters_m7A8B59B5927E38321B74578185E013BBFB7D73A7 (void);
// 0x0000035E System.Void MoreMountains.CorgiEngine.Character::UpdateAnimators()
extern void Character_UpdateAnimators_m7B088454F4DC64F27DEF86E314DD42EA6FE30924 (void);
// 0x0000035F System.Void MoreMountains.CorgiEngine.Character::UpdateAnimationRandomNumber()
extern void Character_UpdateAnimationRandomNumber_m0B350D9CD9A54D76CDE3821E81CCF3B2003D78A5 (void);
// 0x00000360 System.Void MoreMountains.CorgiEngine.Character::HandleCharacterStatus()
extern void Character_HandleCharacterStatus_mB49C1AECE10944EC19796F420D4B857B61587A9D (void);
// 0x00000361 System.Void MoreMountains.CorgiEngine.Character::Freeze()
extern void Character_Freeze_mC3A3EDEBBC34785DA35B7D7D8BAE88BBDF93293B (void);
// 0x00000362 System.Void MoreMountains.CorgiEngine.Character::UnFreeze()
extern void Character_UnFreeze_mF1CAD4A50FE03916C58388F854EF8C152D3829CC (void);
// 0x00000363 System.Void MoreMountains.CorgiEngine.Character::RecalculateRays()
extern void Character_RecalculateRays_mF42A4916FEF6F5F3A306ABAC4015DBD42B3CEBED (void);
// 0x00000364 System.Void MoreMountains.CorgiEngine.Character::Disable()
extern void Character_Disable_m2E49B3ECEC14CFED346B786D1F4CAC59F78AABB8 (void);
// 0x00000365 System.Void MoreMountains.CorgiEngine.Character::RespawnAt(UnityEngine.Transform,MoreMountains.CorgiEngine.Character/FacingDirections)
extern void Character_RespawnAt_mE7CCB58816077BDE8482F0F18D4122A6E9BC6BDE (void);
// 0x00000366 System.Void MoreMountains.CorgiEngine.Character::Flip(System.Boolean)
extern void Character_Flip_m25AFE2C7A62DC623827CE9F879C4077FDB95569C (void);
// 0x00000367 System.Void MoreMountains.CorgiEngine.Character::FlipModel()
extern void Character_FlipModel_m88F35EA64E018EE251C2AA1213DF9B8812B5BCCB (void);
// 0x00000368 System.Void MoreMountains.CorgiEngine.Character::ForceSpawnDirection()
extern void Character_ForceSpawnDirection_mF32A6D828703D0DB86C741E386D81D4674B5D4F2 (void);
// 0x00000369 System.Void MoreMountains.CorgiEngine.Character::Face(MoreMountains.CorgiEngine.Character/FacingDirections)
extern void Character_Face_m79A182BF3AD841B75A579DAA4890FBFB22FFDD57 (void);
// 0x0000036A System.Void MoreMountains.CorgiEngine.Character::HandleCameraTarget()
extern void Character_HandleCameraTarget_mD8A6C4ADF8A938D2EF927BBF30E99ECBD2E53F2D (void);
// 0x0000036B System.Void MoreMountains.CorgiEngine.Character::SetCameraTargetOffset(UnityEngine.Vector3)
extern void Character_SetCameraTargetOffset_m55565FDA7BBF89953EA60632AA45E036916A2714 (void);
// 0x0000036C System.Void MoreMountains.CorgiEngine.Character::Reset()
extern void Character_Reset_mB5F1D1BBDC23E0560AEFAB3851BF86133F52D1C8 (void);
// 0x0000036D System.Void MoreMountains.CorgiEngine.Character::OnRevive()
extern void Character_OnRevive_m460F682C2EABC329CC0E14440B85B67A5673D6FB (void);
// 0x0000036E System.Void MoreMountains.CorgiEngine.Character::OnDeath()
extern void Character_OnDeath_m1275A971A9E6B8F852A6AB7E2797C5FC21255DA1 (void);
// 0x0000036F System.Void MoreMountains.CorgiEngine.Character::OnEnable()
extern void Character_OnEnable_mB9E41FAC1B74F8397361EAAEA443C03BBB40995B (void);
// 0x00000370 System.Void MoreMountains.CorgiEngine.Character::OnDisable()
extern void Character_OnDisable_m864BBF0ACB67782C1A4537FFF45B9E012F28C49D (void);
// 0x00000371 System.Void MoreMountains.CorgiEngine.Character::.ctor()
extern void Character__ctor_mFB835AAF7B6F56B25876C0DCD0A92338F6FF64AC (void);
// 0x00000372 System.Void MoreMountains.CorgiEngine.MMCharacterEvent::.ctor(MoreMountains.CorgiEngine.Character,MoreMountains.CorgiEngine.MMCharacterEventTypes)
extern void MMCharacterEvent__ctor_mE6B7788A4EDF79D0DEB62E4E58569FA2AE2A4D52 (void);
// 0x00000373 System.Void MoreMountains.CorgiEngine.MMCharacterEvent::Trigger(MoreMountains.CorgiEngine.Character,MoreMountains.CorgiEngine.MMCharacterEventTypes)
extern void MMCharacterEvent_Trigger_m4DDAB2F799B2C28587325C02281FFEB9E2EB019F (void);
// 0x00000374 System.Void MoreMountains.CorgiEngine.MMDamageTakenEvent::.ctor(MoreMountains.CorgiEngine.Character,UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void MMDamageTakenEvent__ctor_m0BFC19EF73E4AC6B6B61A5B7AE47EFAC5E1FE6AF (void);
// 0x00000375 System.Void MoreMountains.CorgiEngine.MMDamageTakenEvent::Trigger(MoreMountains.CorgiEngine.Character,UnityEngine.GameObject,System.Single,System.Single,System.Single)
extern void MMDamageTakenEvent_Trigger_mF8E7654C1AA20B2FA2D8DD81DDE6730113F5D774 (void);
// 0x00000376 System.Void MoreMountains.CorgiEngine.CharacterLevelBounds::Start()
extern void CharacterLevelBounds_Start_m63A09EC2B51A6BF5209339B6E6E3FA769959AF26 (void);
// 0x00000377 System.Void MoreMountains.CorgiEngine.CharacterLevelBounds::LateUpdate()
extern void CharacterLevelBounds_LateUpdate_m23C6E6DE601DCD56BB792D4C662B567C40EF4F91 (void);
// 0x00000378 System.Void MoreMountains.CorgiEngine.CharacterLevelBounds::ApplyBoundsBehavior(MoreMountains.CorgiEngine.CharacterLevelBounds/BoundsBehavior,UnityEngine.Vector2)
extern void CharacterLevelBounds_ApplyBoundsBehavior_mD6036D72D009FD582F5F6C59B1CF6C527AF2CF4C (void);
// 0x00000379 System.Void MoreMountains.CorgiEngine.CharacterLevelBounds::.ctor()
extern void CharacterLevelBounds__ctor_m89F483452BD3C651CC3E3841455454B9E0D2DE8C (void);
// 0x0000037A System.Void MoreMountains.CorgiEngine.CharacterStates::.ctor()
extern void CharacterStates__ctor_m69684410BA96D578CF5071CD60872BDA49212904 (void);
// 0x0000037B MoreMountains.CorgiEngine.CorgiControllerState MoreMountains.CorgiEngine.CorgiController::get_State()
extern void CorgiController_get_State_mD9A5220A2421FCD11F87C7ACAE0AF5B69EEA399E (void);
// 0x0000037C System.Void MoreMountains.CorgiEngine.CorgiController::set_State(MoreMountains.CorgiEngine.CorgiControllerState)
extern void CorgiController_set_State_mAF2166592DFF75C26B8E7042D37EA8C88A5BDAD1 (void);
// 0x0000037D MoreMountains.CorgiEngine.CorgiControllerParameters MoreMountains.CorgiEngine.CorgiController::get_Parameters()
extern void CorgiController_get_Parameters_m41B53F710860E4AAC7746732175D094B027A423B (void);
// 0x0000037E UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::get_StandingOnLastFrame()
extern void CorgiController_get_StandingOnLastFrame_mF75E7ADB576D63A40BA1C3FDCE1403308E87C7F2 (void);
// 0x0000037F System.Void MoreMountains.CorgiEngine.CorgiController::set_StandingOnLastFrame(UnityEngine.GameObject)
extern void CorgiController_set_StandingOnLastFrame_m1AB075CB3EC715F77F80E9A57A6608631DD780CB (void);
// 0x00000380 UnityEngine.Collider2D MoreMountains.CorgiEngine.CorgiController::get_StandingOnCollider()
extern void CorgiController_get_StandingOnCollider_mC249AC30C7C2657659EE255D268BD085DE3DEB34 (void);
// 0x00000381 System.Void MoreMountains.CorgiEngine.CorgiController::set_StandingOnCollider(UnityEngine.Collider2D)
extern void CorgiController_set_StandingOnCollider_mBBC7D52AC19C045BEC0F72237595090CDBFF1C27 (void);
// 0x00000382 UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_Speed()
extern void CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE (void);
// 0x00000383 UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_WorldSpeed()
extern void CorgiController_get_WorldSpeed_mF63A3A1849EA831B3134F76F2EC996DC9CCE4D34 (void);
// 0x00000384 UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_ForcesApplied()
extern void CorgiController_get_ForcesApplied_m391574DB371DD71651E8024381474279928A4591 (void);
// 0x00000385 System.Void MoreMountains.CorgiEngine.CorgiController::set_ForcesApplied(UnityEngine.Vector2)
extern void CorgiController_set_ForcesApplied_mA335D2750CB89C9CCD4B5A6DB3ECCB2A1ECA15C0 (void);
// 0x00000386 UnityEngine.GameObject MoreMountains.CorgiEngine.CorgiController::get_CurrentWallCollider()
extern void CorgiController_get_CurrentWallCollider_m7410ABF1E99AB735A317678AE1147F080527E3E4 (void);
// 0x00000387 System.Void MoreMountains.CorgiEngine.CorgiController::set_CurrentWallCollider(UnityEngine.GameObject)
extern void CorgiController_set_CurrentWallCollider_mC41F3A8AAF55DBAD0916E2055FFEE36716CC3473 (void);
// 0x00000388 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_ColliderSize()
extern void CorgiController_get_ColliderSize_m653C621A7F25C185D5027BD9C98B8BD7E5C30480 (void);
// 0x00000389 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_ColliderCenterPosition()
extern void CorgiController_get_ColliderCenterPosition_mEDCDF25B290BAC4BA6AE2384C16016D824914374 (void);
// 0x0000038A UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_ColliderBottomPosition()
extern void CorgiController_get_ColliderBottomPosition_mFB531F139F5F5475EFFC375C1AF3DDA8876BF97E (void);
// 0x0000038B UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_ColliderLeftPosition()
extern void CorgiController_get_ColliderLeftPosition_mB67C36B277DDF6B639AAEABB5DEB49ADA65362DE (void);
// 0x0000038C UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_ColliderTopPosition()
extern void CorgiController_get_ColliderTopPosition_m89C6B1B1118877DDA5F247F271F2EB0654A838AF (void);
// 0x0000038D UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_ColliderRightPosition()
extern void CorgiController_get_ColliderRightPosition_mFA67CC2967C59A9C4D017EAD5DDC2AD6AB25835C (void);
// 0x0000038E System.Boolean MoreMountains.CorgiEngine.CorgiController::get_IsGravityActive()
extern void CorgiController_get_IsGravityActive_mEB4B8C8B49CD7177337C2755FECCBCE4DB37D338 (void);
// 0x0000038F System.Single MoreMountains.CorgiEngine.CorgiController::get_DeltaTime()
extern void CorgiController_get_DeltaTime_m5A258C89B26CA795BDB172CD31020A62659639D8 (void);
// 0x00000390 System.Single MoreMountains.CorgiEngine.CorgiController::get_Friction()
extern void CorgiController_get_Friction_mE114188A7E5940CC3879D90EA47DCD3EECC2EA25 (void);
// 0x00000391 System.Single MoreMountains.CorgiEngine.CorgiController::Width()
extern void CorgiController_Width_mC1214B778477D7FBF830574134527CED7B8D92B0 (void);
// 0x00000392 System.Single MoreMountains.CorgiEngine.CorgiController::Height()
extern void CorgiController_Height_mB035A67AA6325F750D63EBD76DEC19195BF9A0BA (void);
// 0x00000393 UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_Bounds()
extern void CorgiController_get_Bounds_mB32E24DA765C10C9B80B877E8CE74113368035CF (void);
// 0x00000394 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsTopLeftCorner()
extern void CorgiController_get_BoundsTopLeftCorner_mA5701D30927165EB119B6ABE9BAF87C3B958A112 (void);
// 0x00000395 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsBottomLeftCorner()
extern void CorgiController_get_BoundsBottomLeftCorner_m54B35B01561656164C96BB4155D0A81E04F7C5C2 (void);
// 0x00000396 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsTopRightCorner()
extern void CorgiController_get_BoundsTopRightCorner_mF244F6B620B420C6EDE8906160F1B71329C622FE (void);
// 0x00000397 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsBottomRightCorner()
extern void CorgiController_get_BoundsBottomRightCorner_m3E471F0A5C7BD54D932CC4FB0A56CF1A8526AE6A (void);
// 0x00000398 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsTop()
extern void CorgiController_get_BoundsTop_m95A6F0F3282688418AED0D5A9FD77643F5EB066D (void);
// 0x00000399 UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsBottom()
extern void CorgiController_get_BoundsBottom_mF01AB5EAA7A30A61E8BF39514239E46427D5A769 (void);
// 0x0000039A UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsRight()
extern void CorgiController_get_BoundsRight_m59999362140641938B2344A974415C09752A725A (void);
// 0x0000039B UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsLeft()
extern void CorgiController_get_BoundsLeft_mEB01DBDC5F23B5EED436C56CE11E0C603518BF76 (void);
// 0x0000039C UnityEngine.Vector3 MoreMountains.CorgiEngine.CorgiController::get_BoundsCenter()
extern void CorgiController_get_BoundsCenter_m9A7F60F2EE20BF6B598765AAA9956ECD5C659DDF (void);
// 0x0000039D System.Single MoreMountains.CorgiEngine.CorgiController::get_DistanceToTheGround()
extern void CorgiController_get_DistanceToTheGround_m5431BC2330C1030E29DF4D95D7798FB0C0474AA4 (void);
// 0x0000039E UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::get_ExternalForce()
extern void CorgiController_get_ExternalForce_mAC7BED9C9B8638A3E3500AEC806704339F3A9F9D (void);
// 0x0000039F System.Void MoreMountains.CorgiEngine.CorgiController::Awake()
extern void CorgiController_Awake_mD31F0EBCDC304466CB30700C464BEE8A764F0B56 (void);
// 0x000003A0 System.Void MoreMountains.CorgiEngine.CorgiController::Initialization()
extern void CorgiController_Initialization_m1C1A7DE9A938C86123675288229F36AB236E071D (void);
// 0x000003A1 System.Void MoreMountains.CorgiEngine.CorgiController::ApplyPhysicsSettings()
extern void CorgiController_ApplyPhysicsSettings_mBE177C9A753DDBD98E236465B903DE4D46CFB796 (void);
// 0x000003A2 System.Void MoreMountains.CorgiEngine.CorgiController::ApplyGravitySettings()
extern void CorgiController_ApplyGravitySettings_mEF501CA833A8ECA7CFA9C300B1A2266531529C62 (void);
// 0x000003A3 System.Void MoreMountains.CorgiEngine.CorgiController::AddForce(UnityEngine.Vector2)
extern void CorgiController_AddForce_mD29E6C1B989CD3DBC77204CF6B5FD807351FC310 (void);
// 0x000003A4 System.Void MoreMountains.CorgiEngine.CorgiController::AddHorizontalForce(System.Single)
extern void CorgiController_AddHorizontalForce_mF17EF2BBE85EF4B2443356038DDA4D93D4000723 (void);
// 0x000003A5 System.Void MoreMountains.CorgiEngine.CorgiController::AddVerticalForce(System.Single)
extern void CorgiController_AddVerticalForce_mE8AE53FF733D0226412315015BA5B5E337E93432 (void);
// 0x000003A6 System.Void MoreMountains.CorgiEngine.CorgiController::SetForce(UnityEngine.Vector2)
extern void CorgiController_SetForce_mBE673B74C42FE562B64C9BE191FC483D13060D53 (void);
// 0x000003A7 System.Void MoreMountains.CorgiEngine.CorgiController::SetHorizontalForce(System.Single)
extern void CorgiController_SetHorizontalForce_m4AC33AFB81A3D76B50D707F4231D7FA8EF78A2F0 (void);
// 0x000003A8 System.Void MoreMountains.CorgiEngine.CorgiController::SetVerticalForce(System.Single)
extern void CorgiController_SetVerticalForce_m984E8CF61DCC6BBB0F229BDEACB2DC7D2BEF3D26 (void);
// 0x000003A9 System.Void MoreMountains.CorgiEngine.CorgiController::FixedUpdate()
extern void CorgiController_FixedUpdate_m51564BE55240E35EAD8C3B08561046B196CC952A (void);
// 0x000003AA System.Void MoreMountains.CorgiEngine.CorgiController::Update()
extern void CorgiController_Update_m8727CC305C6A27BD0CCCA0D81BFD039DA26DB48A (void);
// 0x000003AB System.Void MoreMountains.CorgiEngine.CorgiController::LateUpdate()
extern void CorgiController_LateUpdate_mEA2A085F9F4F50BB3FC0961B1BACF222A933DAF3 (void);
// 0x000003AC System.Void MoreMountains.CorgiEngine.CorgiController::EveryFrame()
extern void CorgiController_EveryFrame_mEA547372531D16FDE334A014055F4EFC64E2A42A (void);
// 0x000003AD System.Void MoreMountains.CorgiEngine.CorgiController::FrameInitialization()
extern void CorgiController_FrameInitialization_m8C6B23AB67EFE5A1C9664D3FE5648A4545B8AEF0 (void);
// 0x000003AE System.Void MoreMountains.CorgiEngine.CorgiController::FrameExit()
extern void CorgiController_FrameExit_m6388A506E6D48D142E7716B7AF2F5C62A1F684C2 (void);
// 0x000003AF System.Void MoreMountains.CorgiEngine.CorgiController::DetermineMovementDirection()
extern void CorgiController_DetermineMovementDirection_mCA744F3E3F8A2A4DBFAD66168DB499DFD9C065C8 (void);
// 0x000003B0 System.Void MoreMountains.CorgiEngine.CorgiController::MoveTransform()
extern void CorgiController_MoveTransform_mCCA21A455F50EDB7EB6399D6DD6D96FDD81BCD3A (void);
// 0x000003B1 System.Void MoreMountains.CorgiEngine.CorgiController::ApplyGravity()
extern void CorgiController_ApplyGravity_mDCC7601C5553AFF07FB71A2734E19020DFC97117 (void);
// 0x000003B2 System.Void MoreMountains.CorgiEngine.CorgiController::HandleMovingPlatforms()
extern void CorgiController_HandleMovingPlatforms_mB46B8EFAE7F8496D3BABE911D01AC8B43497F3EA (void);
// 0x000003B3 System.Void MoreMountains.CorgiEngine.CorgiController::DetachFromMovingPlatform()
extern void CorgiController_DetachFromMovingPlatform_m92CCE5ABC530ABCC9F5CEA68A10B205D67CF4916 (void);
// 0x000003B4 System.Boolean MoreMountains.CorgiEngine.CorgiController::CastRays(MoreMountains.CorgiEngine.CorgiController/RaycastDirections,System.Single,UnityEngine.Color,UnityEngine.RaycastHit2D[]&)
extern void CorgiController_CastRays_m61FB71FBABEF808B6B6D84EF4555FD2DA0D62998 (void);
// 0x000003B5 System.Void MoreMountains.CorgiEngine.CorgiController::CastRaysToTheLeft()
extern void CorgiController_CastRaysToTheLeft_m332E5DF7EE42865D12B814A49EFD787C60A6D807 (void);
// 0x000003B6 System.Void MoreMountains.CorgiEngine.CorgiController::CastRaysToTheRight()
extern void CorgiController_CastRaysToTheRight_m60A9CB316960762C747107CF7CACC836B85C1403 (void);
// 0x000003B7 System.Void MoreMountains.CorgiEngine.CorgiController::CastRaysToTheSides(System.Single)
extern void CorgiController_CastRaysToTheSides_m10887BE20A237884889FDCA17A2F027E82EE0FB2 (void);
// 0x000003B8 System.Void MoreMountains.CorgiEngine.CorgiController::CastRaysBelow()
extern void CorgiController_CastRaysBelow_mEEA1AF96A2851736F17BB433AA18FE3F427865ED (void);
// 0x000003B9 System.Void MoreMountains.CorgiEngine.CorgiController::CastRaysAbove()
extern void CorgiController_CastRaysAbove_m1634EF0416DB5352161564D6EFC7D715A3FE07C5 (void);
// 0x000003BA System.Void MoreMountains.CorgiEngine.CorgiController::StickToSlope()
extern void CorgiController_StickToSlope_m79872A8A96B1B25BD75ACA207D562D4CF7278C1F (void);
// 0x000003BB System.Void MoreMountains.CorgiEngine.CorgiController::ComputeNewSpeed()
extern void CorgiController_ComputeNewSpeed_m927F0367AEF7D078EF1C60CB8204BE7889423535 (void);
// 0x000003BC System.Void MoreMountains.CorgiEngine.CorgiController::ClampSpeed()
extern void CorgiController_ClampSpeed_mD7858501FA1858D9E7BEB61B0D77796A8D8E209A (void);
// 0x000003BD System.Void MoreMountains.CorgiEngine.CorgiController::ClampExternalForce()
extern void CorgiController_ClampExternalForce_m8AB80AD2640106EC3EE0CB4D623C4527BF710743 (void);
// 0x000003BE System.Void MoreMountains.CorgiEngine.CorgiController::SetStates()
extern void CorgiController_SetStates_m2742CD4CC2908FDE6256D88A67E6F7666F667074 (void);
// 0x000003BF System.Void MoreMountains.CorgiEngine.CorgiController::ComputeDistanceToTheGround()
extern void CorgiController_ComputeDistanceToTheGround_mAD9FC0C675226B21A2643B80193564E99B36CC6B (void);
// 0x000003C0 System.Void MoreMountains.CorgiEngine.CorgiController::SetRaysParameters()
extern void CorgiController_SetRaysParameters_m856588FF304A10E23BA66A17E976EE1C791E6AB1 (void);
// 0x000003C1 System.Void MoreMountains.CorgiEngine.CorgiController::SetIgnoreCollider(UnityEngine.Collider2D)
extern void CorgiController_SetIgnoreCollider_m6A62811AB6C0B5AB62F142D7A38A00C8F67F8323 (void);
// 0x000003C2 System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiController::DisableCollisions(System.Single)
extern void CorgiController_DisableCollisions_m8BD6564FD460A18CC0BBB015FA587CD6362475D1 (void);
// 0x000003C3 System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOn()
extern void CorgiController_CollisionsOn_mBE525E4DCE59DABFF9BE65A1A671D3BF57F8C298 (void);
// 0x000003C4 System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOff()
extern void CorgiController_CollisionsOff_m16FD771561D503E206F3011115BC976C9699AC34 (void);
// 0x000003C5 System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiController::DisableCollisionsWithOneWayPlatforms(System.Single)
extern void CorgiController_DisableCollisionsWithOneWayPlatforms_mB7C8EB0EA2F458B912F2845255987C3804761DD4 (void);
// 0x000003C6 System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiController::DisableCollisionsWithMovingPlatforms(System.Single)
extern void CorgiController_DisableCollisionsWithMovingPlatforms_m917B7957DFE4C0B3262E31231AB93F990A60B1E8 (void);
// 0x000003C7 System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOffWithOneWayPlatformsLayer()
extern void CorgiController_CollisionsOffWithOneWayPlatformsLayer_mD6064A0E4D4C01A66D9773AF5598C07D06569771 (void);
// 0x000003C8 System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOffWithMovingPlatformsLayer()
extern void CorgiController_CollisionsOffWithMovingPlatformsLayer_mA46BBCCBE77705FEF49EFB5AD8B862FC7FC45F9E (void);
// 0x000003C9 System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOnWithStairs()
extern void CorgiController_CollisionsOnWithStairs_m0BAAD50CA1E516AE1ADC8C18B1A67167F493758A (void);
// 0x000003CA System.Void MoreMountains.CorgiEngine.CorgiController::CollisionsOffWithStairs()
extern void CorgiController_CollisionsOffWithStairs_m4B41ED3D66F57156C62424DF8A092131E7882CC1 (void);
// 0x000003CB System.Void MoreMountains.CorgiEngine.CorgiController::ResetParameters()
extern void CorgiController_ResetParameters_mB4A9F3FC567B91E3E748D5FDDD1E412CEA0B2B96 (void);
// 0x000003CC System.Void MoreMountains.CorgiEngine.CorgiController::SlowFall(System.Single)
extern void CorgiController_SlowFall_m0C2DF75592A94C9CEC7FBACDCFEB8A9D7B0768A5 (void);
// 0x000003CD System.Void MoreMountains.CorgiEngine.CorgiController::GravityActive(System.Boolean)
extern void CorgiController_GravityActive_m7C5478D6AB8F19DA7F3CC2DF317CE504018F7344 (void);
// 0x000003CE System.Void MoreMountains.CorgiEngine.CorgiController::ResizeCollider(UnityEngine.Vector2)
extern void CorgiController_ResizeCollider_m444F3F162A02876BE6754F42D024C8B95285C96C (void);
// 0x000003CF System.Void MoreMountains.CorgiEngine.CorgiController::ResetColliderSize()
extern void CorgiController_ResetColliderSize_m17ECEFE020F1EFBFE424C72F1B3CC9CAE5774094 (void);
// 0x000003D0 System.Boolean MoreMountains.CorgiEngine.CorgiController::CanGoBackToOriginalSize()
extern void CorgiController_CanGoBackToOriginalSize_mA265A35AED6C7BD949647C91E1089E096776DE50 (void);
// 0x000003D1 System.Void MoreMountains.CorgiEngine.CorgiController::SetTransformPosition(UnityEngine.Vector3)
extern void CorgiController_SetTransformPosition_m3749033CA3B68533F25F0DAC7DF38B85B64A3CFE (void);
// 0x000003D2 UnityEngine.Vector2 MoreMountains.CorgiEngine.CorgiController::GetClosestSafePosition(UnityEngine.Vector2)
extern void CorgiController_GetClosestSafePosition_m29C0A2E0637981759B9E345C4DA96EE91DD87689 (void);
// 0x000003D3 System.Void MoreMountains.CorgiEngine.CorgiController::AnchorToGround()
extern void CorgiController_AnchorToGround_m7DA4E85C46B78A517546BCC21360B771EF29E548 (void);
// 0x000003D4 System.Void MoreMountains.CorgiEngine.CorgiController::OnCorgiColliderHit()
extern void CorgiController_OnCorgiColliderHit_m52C74DBFD58E340423702BFD864B13D905EECD32 (void);
// 0x000003D5 System.Void MoreMountains.CorgiEngine.CorgiController::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CorgiController_OnTriggerEnter2D_m7F2015A6FCB89C96264C29AFE4FBA1861A7E684D (void);
// 0x000003D6 System.Void MoreMountains.CorgiEngine.CorgiController::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CorgiController_OnTriggerStay2D_m7DC6F27C64392FD9D0C648E82743D4DE12FADF73 (void);
// 0x000003D7 System.Void MoreMountains.CorgiEngine.CorgiController::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CorgiController_OnTriggerExit2D_m5AE8B9692466F50004426337A18617D01F4E9B4E (void);
// 0x000003D8 System.Void MoreMountains.CorgiEngine.CorgiController::.ctor()
extern void CorgiController__ctor_mDC3C29E67F85BED36C7D3F0899FA1881953B8445 (void);
// 0x000003D9 System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisions>d__198::.ctor(System.Int32)
extern void U3CDisableCollisionsU3Ed__198__ctor_mF44F0091754A0EFA15025BD63AC8131B27CA963E (void);
// 0x000003DA System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisions>d__198::System.IDisposable.Dispose()
extern void U3CDisableCollisionsU3Ed__198_System_IDisposable_Dispose_m2C8DD91413592FF312F5EA00F9F7CB0F4BEB0640 (void);
// 0x000003DB System.Boolean MoreMountains.CorgiEngine.CorgiController/<DisableCollisions>d__198::MoveNext()
extern void U3CDisableCollisionsU3Ed__198_MoveNext_m2D09586373F670D40B62652BEED53AA61B7C1C91 (void);
// 0x000003DC System.Object MoreMountains.CorgiEngine.CorgiController/<DisableCollisions>d__198::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisableCollisionsU3Ed__198_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0671FD7547B12CE3686C8B0ED64C9041D3BC42B5 (void);
// 0x000003DD System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisions>d__198::System.Collections.IEnumerator.Reset()
extern void U3CDisableCollisionsU3Ed__198_System_Collections_IEnumerator_Reset_m4C210A97910CD3F8CB161263DF2F1AB7CB82632B (void);
// 0x000003DE System.Object MoreMountains.CorgiEngine.CorgiController/<DisableCollisions>d__198::System.Collections.IEnumerator.get_Current()
extern void U3CDisableCollisionsU3Ed__198_System_Collections_IEnumerator_get_Current_mE02E60DB5C782DB99A674193252886B49DD05C49 (void);
// 0x000003DF System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithOneWayPlatforms>d__201::.ctor(System.Int32)
extern void U3CDisableCollisionsWithOneWayPlatformsU3Ed__201__ctor_m731AE102A4B9E6026AE38130AAA9C0472C8C07C5 (void);
// 0x000003E0 System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithOneWayPlatforms>d__201::System.IDisposable.Dispose()
extern void U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_IDisposable_Dispose_mD786ED9F56FE48363957F3D58D07F25643B455DA (void);
// 0x000003E1 System.Boolean MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithOneWayPlatforms>d__201::MoveNext()
extern void U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_MoveNext_mFFDAF70F638FB7111FDC37855399A992559BF53E (void);
// 0x000003E2 System.Object MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithOneWayPlatforms>d__201::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m808AD5B1D89BF1A300F2B5A08A8C357DED20AF3E (void);
// 0x000003E3 System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithOneWayPlatforms>d__201::System.Collections.IEnumerator.Reset()
extern void U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_Collections_IEnumerator_Reset_mD5ABB7A6FB6D1D6D6E825A8190526902505B2948 (void);
// 0x000003E4 System.Object MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithOneWayPlatforms>d__201::System.Collections.IEnumerator.get_Current()
extern void U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_Collections_IEnumerator_get_Current_m8927C225827CD87B3A96E02F9E188580E7D90CA5 (void);
// 0x000003E5 System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithMovingPlatforms>d__202::.ctor(System.Int32)
extern void U3CDisableCollisionsWithMovingPlatformsU3Ed__202__ctor_m71B3F449CEEB48110ABB7655FFF1745C83CBB6F7 (void);
// 0x000003E6 System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithMovingPlatforms>d__202::System.IDisposable.Dispose()
extern void U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_IDisposable_Dispose_m0933A9CD0FE2B0636A84F4F36D9DA7D6CC0F73D5 (void);
// 0x000003E7 System.Boolean MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithMovingPlatforms>d__202::MoveNext()
extern void U3CDisableCollisionsWithMovingPlatformsU3Ed__202_MoveNext_m12CE5197A391E1591928DD7FE05FF6317A6391BC (void);
// 0x000003E8 System.Object MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithMovingPlatforms>d__202::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8BE89F96AA0CFE55F68BAD3CDAE0176FFCB5D9C (void);
// 0x000003E9 System.Void MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithMovingPlatforms>d__202::System.Collections.IEnumerator.Reset()
extern void U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_Collections_IEnumerator_Reset_mBB903477FCAAD9EAE3404FEE5D035D7D8B0B0CB8 (void);
// 0x000003EA System.Object MoreMountains.CorgiEngine.CorgiController/<DisableCollisionsWithMovingPlatforms>d__202::System.Collections.IEnumerator.get_Current()
extern void U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_Collections_IEnumerator_get_Current_mA97872466788AE53B13374C4A02EB504959BA840 (void);
// 0x000003EB System.Void MoreMountains.CorgiEngine.CorgiControllerParameters::.ctor()
extern void CorgiControllerParameters__ctor_mCD9818D33171699B1040F736B1ED3ABA25E16297 (void);
// 0x000003EC System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsCollidingRight()
extern void CorgiControllerState_get_IsCollidingRight_m5E055D9DE70761CF6A7EB800BB613A1EDD233D3D (void);
// 0x000003ED System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_IsCollidingRight(System.Boolean)
extern void CorgiControllerState_set_IsCollidingRight_mBDAE2D2B87262E4F14B362FBA070A492BC1105B6 (void);
// 0x000003EE System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsCollidingLeft()
extern void CorgiControllerState_get_IsCollidingLeft_m1DB1E7A8B2A8252A9913AF20BCC503CCC0A207E2 (void);
// 0x000003EF System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_IsCollidingLeft(System.Boolean)
extern void CorgiControllerState_set_IsCollidingLeft_mD6B285F6FF980BBA04B56FE66527DC26806E39AB (void);
// 0x000003F0 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsCollidingAbove()
extern void CorgiControllerState_get_IsCollidingAbove_m3B3F21C54217C16D3CEAEAB701619AA6E425888D (void);
// 0x000003F1 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_IsCollidingAbove(System.Boolean)
extern void CorgiControllerState_set_IsCollidingAbove_m66B77601CB198F2F3C653125143AC67BD9C241C7 (void);
// 0x000003F2 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsCollidingBelow()
extern void CorgiControllerState_get_IsCollidingBelow_m9B41EB0712BBAC49C5C1447CB97A4CE186340EC0 (void);
// 0x000003F3 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_IsCollidingBelow(System.Boolean)
extern void CorgiControllerState_set_IsCollidingBelow_m6C72E7251F4439949291010C28306A5FE467CC1E (void);
// 0x000003F4 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_HasCollisions()
extern void CorgiControllerState_get_HasCollisions_m7B8E1EB04C4E98C81002EFB47AB3CD8E84B2B808 (void);
// 0x000003F5 System.Single MoreMountains.CorgiEngine.CorgiControllerState::get_LateralSlopeAngle()
extern void CorgiControllerState_get_LateralSlopeAngle_mB86D998EFF29270E59CC7E39028DF7EF4B387624 (void);
// 0x000003F6 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_LateralSlopeAngle(System.Single)
extern void CorgiControllerState_set_LateralSlopeAngle_m7CA8FFF77D5C7A93AB4299968B021AC45E94BD32 (void);
// 0x000003F7 System.Single MoreMountains.CorgiEngine.CorgiControllerState::get_BelowSlopeAngle()
extern void CorgiControllerState_get_BelowSlopeAngle_m475A6925EA7BD5E664C2A957F4258437BA7976F2 (void);
// 0x000003F8 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_BelowSlopeAngle(System.Single)
extern void CorgiControllerState_set_BelowSlopeAngle_m88005661C7E72332BE414B6E3509F6385C9CE943 (void);
// 0x000003F9 System.Single MoreMountains.CorgiEngine.CorgiControllerState::get_BelowSlopeAngleAbsolute()
extern void CorgiControllerState_get_BelowSlopeAngleAbsolute_m3D708DF41C91C62BD96B4470AB914E624F4C0750 (void);
// 0x000003FA System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_BelowSlopeAngleAbsolute(System.Single)
extern void CorgiControllerState_set_BelowSlopeAngleAbsolute_m17BD9785062B6C615000669DB26041B691839B3E (void);
// 0x000003FB System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_SlopeAngleOK()
extern void CorgiControllerState_get_SlopeAngleOK_mF4DA59E920D8B8CBD0010709680C471A68B59860 (void);
// 0x000003FC System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_SlopeAngleOK(System.Boolean)
extern void CorgiControllerState_set_SlopeAngleOK_mB99AC173A98113BE09238B1EFCFDEF772D76454A (void);
// 0x000003FD System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_OnAMovingPlatform()
extern void CorgiControllerState_get_OnAMovingPlatform_m43996CF0791875751C92E634020691FEE4F37A4D (void);
// 0x000003FE System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_OnAMovingPlatform(System.Boolean)
extern void CorgiControllerState_set_OnAMovingPlatform_m7A1B1899D16A3AC05A4028ADDB2F5DD9017ECD97 (void);
// 0x000003FF System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsGrounded()
extern void CorgiControllerState_get_IsGrounded_m522D2A3105AC737DE8C45C16635DBB40E4C9FA80 (void);
// 0x00000400 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsFalling()
extern void CorgiControllerState_get_IsFalling_m532AC0C8363A1649CC634B660C898BDCA9BD20C1 (void);
// 0x00000401 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_IsFalling(System.Boolean)
extern void CorgiControllerState_set_IsFalling_mAD62F15191B70C561610994B46583C6658029228 (void);
// 0x00000402 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_IsJumping()
extern void CorgiControllerState_get_IsJumping_mAA87D1F71654E347E9D3A899BC1256C7EEC826E6 (void);
// 0x00000403 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_IsJumping(System.Boolean)
extern void CorgiControllerState_set_IsJumping_mFA8491028E9CDF47E1EFED840C17D3183DEA9FFA (void);
// 0x00000404 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_WasGroundedLastFrame()
extern void CorgiControllerState_get_WasGroundedLastFrame_m3571F51506565A217F5EBF5F1167844B93BDD739 (void);
// 0x00000405 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_WasGroundedLastFrame(System.Boolean)
extern void CorgiControllerState_set_WasGroundedLastFrame_m4544D35A347CAFE5A9E5580CEF5298C1A658E024 (void);
// 0x00000406 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_WasTouchingTheCeilingLastFrame()
extern void CorgiControllerState_get_WasTouchingTheCeilingLastFrame_mAC0AD82DF03A3587C2D29674FA85852E5F9ABDC8 (void);
// 0x00000407 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_WasTouchingTheCeilingLastFrame(System.Boolean)
extern void CorgiControllerState_set_WasTouchingTheCeilingLastFrame_m6EE0B50B1419C7FC18180908BE82434CC9B446D4 (void);
// 0x00000408 System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_JustGotGrounded()
extern void CorgiControllerState_get_JustGotGrounded_mA1E82A8871CE4CC14141743B17B19670787EED94 (void);
// 0x00000409 System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_JustGotGrounded(System.Boolean)
extern void CorgiControllerState_set_JustGotGrounded_m04B0B387B7A940DDC778AD74599B175F36D00E85 (void);
// 0x0000040A System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_ColliderResized()
extern void CorgiControllerState_get_ColliderResized_m3F7772B4510BB4075405C0DC06F5D1C61B588E5B (void);
// 0x0000040B System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_ColliderResized(System.Boolean)
extern void CorgiControllerState_set_ColliderResized_mEF258683C28922A1E0BDDF1E7D40273CEDBD37E9 (void);
// 0x0000040C System.Boolean MoreMountains.CorgiEngine.CorgiControllerState::get_TouchingLevelBounds()
extern void CorgiControllerState_get_TouchingLevelBounds_m9AFEA95CF894487EC23DDC679A56E7B07429DA3D (void);
// 0x0000040D System.Void MoreMountains.CorgiEngine.CorgiControllerState::set_TouchingLevelBounds(System.Boolean)
extern void CorgiControllerState_set_TouchingLevelBounds_mBADA7E2EEDB93E8E39CF6AF6B5E727885DCD646E (void);
// 0x0000040E System.Void MoreMountains.CorgiEngine.CorgiControllerState::Reset()
extern void CorgiControllerState_Reset_mEC56055D828626CE61B5B36A06D06F3D947566CF (void);
// 0x0000040F System.String MoreMountains.CorgiEngine.CorgiControllerState::ToString()
extern void CorgiControllerState_ToString_mB50B2AB34A1C56BC7E7A5554333B64233D4C1253 (void);
// 0x00000410 System.Void MoreMountains.CorgiEngine.CorgiControllerState::.ctor()
extern void CorgiControllerState__ctor_m35D3A0A21F8683F3A2DC5FADADAD497ACCAE8309 (void);
// 0x00000411 System.Void MoreMountains.CorgiEngine.DamageOnTouch::Awake()
extern void DamageOnTouch_Awake_m3BF53D821ADAF9ABF93DADFB63CC4D3A64C3F4E7 (void);
// 0x00000412 System.Void MoreMountains.CorgiEngine.DamageOnTouch::SetCorgiController(MoreMountains.CorgiEngine.CorgiController)
extern void DamageOnTouch_SetCorgiController_mD33A54A66338EC1C1C77DB2244D76F833E07F90C (void);
// 0x00000413 System.Void MoreMountains.CorgiEngine.DamageOnTouch::InitializeFeedbacks()
extern void DamageOnTouch_InitializeFeedbacks_mB01ED16B5E14B3A1A8CC178BF1B5CF7B30BC4B64 (void);
// 0x00000414 System.Void MoreMountains.CorgiEngine.DamageOnTouch::OnEnable()
extern void DamageOnTouch_OnEnable_mD8F09C64E027EE33FF691A0C4A53F3CB7888079E (void);
// 0x00000415 System.Void MoreMountains.CorgiEngine.DamageOnTouch::Update()
extern void DamageOnTouch_Update_m4DFC0071794313A424231A414BCB9491F4DCCFFF (void);
// 0x00000416 System.Void MoreMountains.CorgiEngine.DamageOnTouch::IgnoreGameObject(UnityEngine.GameObject)
extern void DamageOnTouch_IgnoreGameObject_mF0E3D6A5ADF76D6EAB16AA38E755995551390B83 (void);
// 0x00000417 System.Void MoreMountains.CorgiEngine.DamageOnTouch::StopIgnoringObject(UnityEngine.GameObject)
extern void DamageOnTouch_StopIgnoringObject_m0F14F070F68C8515E7BEBBAEBF28A1DD7EEF7EF8 (void);
// 0x00000418 System.Void MoreMountains.CorgiEngine.DamageOnTouch::ClearIgnoreList()
extern void DamageOnTouch_ClearIgnoreList_mB197BA56BB853DA6308559992213222736B79146 (void);
// 0x00000419 System.Void MoreMountains.CorgiEngine.DamageOnTouch::ComputeVelocity()
extern void DamageOnTouch_ComputeVelocity_m60E06E778785BD0DEC49CBD2A4F16504AE3C219A (void);
// 0x0000041A System.Void MoreMountains.CorgiEngine.DamageOnTouch::OnTriggerStay2D(UnityEngine.Collider2D)
extern void DamageOnTouch_OnTriggerStay2D_m30F90AAFF2796A998EB098C822AB429BF62ED241 (void);
// 0x0000041B System.Void MoreMountains.CorgiEngine.DamageOnTouch::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void DamageOnTouch_OnTriggerEnter2D_mBBDE1EE2091F6D55D38637E086DB39FA2755AD67 (void);
// 0x0000041C System.Void MoreMountains.CorgiEngine.DamageOnTouch::Colliding(UnityEngine.Collider2D)
extern void DamageOnTouch_Colliding_mBB3A7F28F1B4E3AE445A8D68350E36223861CCF0 (void);
// 0x0000041D System.Void MoreMountains.CorgiEngine.DamageOnTouch::OnCollideWithDamageable(MoreMountains.CorgiEngine.Health)
extern void DamageOnTouch_OnCollideWithDamageable_m41C9116F7D8D5E4CC8B5AD311C0A65250ED19147 (void);
// 0x0000041E System.Void MoreMountains.CorgiEngine.DamageOnTouch::ApplyDamageCausedKnockback()
extern void DamageOnTouch_ApplyDamageCausedKnockback_m2344DDDA703D596CB056488745B33C3CCCC0BBDE (void);
// 0x0000041F System.Void MoreMountains.CorgiEngine.DamageOnTouch::ApplyDamageTakenKnockback()
extern void DamageOnTouch_ApplyDamageTakenKnockback_m1A4C3F7E2B127ECD903C53F41D255E3463157F52 (void);
// 0x00000420 System.Void MoreMountains.CorgiEngine.DamageOnTouch::OnCollideWithNonDamageable()
extern void DamageOnTouch_OnCollideWithNonDamageable_m539BC0ABC05CD11B3E1C316C73E8842C90FAAFA2 (void);
// 0x00000421 System.Void MoreMountains.CorgiEngine.DamageOnTouch::SelfDamage(System.Int32)
extern void DamageOnTouch_SelfDamage_mF373FF72DB3B6994B3EEAD21E168B3FF866AAD1F (void);
// 0x00000422 System.Void MoreMountains.CorgiEngine.DamageOnTouch::OnDrawGizmos()
extern void DamageOnTouch_OnDrawGizmos_m2FAA67F4EC8490CEA809003019F6A4755D65F02C (void);
// 0x00000423 System.Void MoreMountains.CorgiEngine.DamageOnTouch::.ctor()
extern void DamageOnTouch__ctor_mD63C365D24B33B4FF7265CB4533D202402BA78BB (void);
// 0x00000424 System.Void MoreMountains.CorgiEngine.DamageOnTouch/OnHitDelegate::.ctor(System.Object,System.IntPtr)
extern void OnHitDelegate__ctor_mFBC14D88D4C97ADE06768049AAB21A12C24A3B32 (void);
// 0x00000425 System.Void MoreMountains.CorgiEngine.DamageOnTouch/OnHitDelegate::Invoke()
extern void OnHitDelegate_Invoke_m79568188F1F5016DD62CE77AA8B1ABCCF797B424 (void);
// 0x00000426 System.IAsyncResult MoreMountains.CorgiEngine.DamageOnTouch/OnHitDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnHitDelegate_BeginInvoke_m64D346AC4D7598DD55A567A737AA057FDD00B7DB (void);
// 0x00000427 System.Void MoreMountains.CorgiEngine.DamageOnTouch/OnHitDelegate::EndInvoke(System.IAsyncResult)
extern void OnHitDelegate_EndInvoke_mF9A85B66956B32689DDD97B92D68ACA0FCF031E7 (void);
// 0x00000428 System.Void MoreMountains.CorgiEngine.KillPlayerOnTouch::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void KillPlayerOnTouch_OnTriggerEnter2D_mC23362B9E6E012485A549DCC92C1AD9EA09BACB7 (void);
// 0x00000429 System.Void MoreMountains.CorgiEngine.KillPlayerOnTouch::.ctor()
extern void KillPlayerOnTouch__ctor_m225506A31006C99253ECB186864767C9506D4636 (void);
// 0x0000042A System.Void MoreMountains.CorgiEngine.Stompable::Start()
extern void Stompable_Start_m3E188EEFDF86E9AAA7489731DFE010AD4CE47BF9 (void);
// 0x0000042B System.Void MoreMountains.CorgiEngine.Stompable::LateUpdate()
extern void Stompable_LateUpdate_m7DC09A30137A600385A9AA298364D3C9C22B922F (void);
// 0x0000042C System.Void MoreMountains.CorgiEngine.Stompable::CastRaysAbove()
extern void Stompable_CastRaysAbove_m100273F6D0FBFA2E89595A50229294D0F2D116B8 (void);
// 0x0000042D System.Boolean MoreMountains.CorgiEngine.Stompable::HitIsValid()
extern void Stompable_HitIsValid_m5C89ACE4B9C0A22549A39C7025E4E4817FCD5C4D (void);
// 0x0000042E System.Void MoreMountains.CorgiEngine.Stompable::InitializeRay()
extern void Stompable_InitializeRay_mDCEC5745B3E3FD7440F50E4FB534D402FB1DC994 (void);
// 0x0000042F System.Void MoreMountains.CorgiEngine.Stompable::PerformStomp(MoreMountains.CorgiEngine.CorgiController)
extern void Stompable_PerformStomp_mA98FE695E6C0C5F30DF9A5B65577D342F94E533E (void);
// 0x00000430 System.Void MoreMountains.CorgiEngine.Stompable::.ctor()
extern void Stompable__ctor_m62F9819D9D91BDCE17E885CD55471230BEC5E823 (void);
// 0x00000431 System.Int32 MoreMountains.CorgiEngine.Health::get_LastDamage()
extern void Health_get_LastDamage_mD22C96C3F4DCBD4504335E27B75C576DFD949150 (void);
// 0x00000432 System.Void MoreMountains.CorgiEngine.Health::set_LastDamage(System.Int32)
extern void Health_set_LastDamage_mC623B6A075FAD4491C719387D7AC9EF28007DCFE (void);
// 0x00000433 UnityEngine.Vector3 MoreMountains.CorgiEngine.Health::get_LastDamageDirection()
extern void Health_get_LastDamageDirection_m1ABAC9322C17725AB1C03107FD77EB3783EDF576 (void);
// 0x00000434 System.Void MoreMountains.CorgiEngine.Health::set_LastDamageDirection(UnityEngine.Vector3)
extern void Health_set_LastDamageDirection_mA50F89BA00230490CB99BFC0F600174995E50162 (void);
// 0x00000435 System.Void MoreMountains.CorgiEngine.Health::Start()
extern void Health_Start_m66B163A28D25277F082B151E26802C84BFFB6EAE (void);
// 0x00000436 System.Void MoreMountains.CorgiEngine.Health::Initialization()
extern void Health_Initialization_m5FA1B03AFC34508F445DE3E0EC3A8C57353B7237 (void);
// 0x00000437 System.Void MoreMountains.CorgiEngine.Health::StoreInitialPosition()
extern void Health_StoreInitialPosition_mBA109269E0810BEF698CA0BE909EBCBEE372324B (void);
// 0x00000438 System.Void MoreMountains.CorgiEngine.Health::InitializeSpriteColor()
extern void Health_InitializeSpriteColor_m03BF06871A4618E6636EE9FD097ACDEC2DC5CA10 (void);
// 0x00000439 System.Void MoreMountains.CorgiEngine.Health::ResetSpriteColor()
extern void Health_ResetSpriteColor_mFC858D9EF56EFBC621793B7F9A2592EA8AEBFC5D (void);
// 0x0000043A System.Void MoreMountains.CorgiEngine.Health::Damage(System.Int32,UnityEngine.GameObject,System.Single,System.Single,UnityEngine.Vector3)
extern void Health_Damage_mB2C11834434E052ED36D93A0B10F8CAD2877049C (void);
// 0x0000043B System.Void MoreMountains.CorgiEngine.Health::Kill()
extern void Health_Kill_m6E1EE3396B9C526C7CBAE4192AB958DFE96BF01D (void);
// 0x0000043C System.Void MoreMountains.CorgiEngine.Health::Revive()
extern void Health_Revive_mAFF5C4E1ADA175545E7B5C503B11EE2824B39C14 (void);
// 0x0000043D System.Void MoreMountains.CorgiEngine.Health::DestroyObject()
extern void Health_DestroyObject_m82B9173DB256A1070184604508F661A2AB47A7F3 (void);
// 0x0000043E System.Void MoreMountains.CorgiEngine.Health::GetHealth(System.Int32,UnityEngine.GameObject)
extern void Health_GetHealth_m99A611A1CCC7B07666AF7C909BA279AB18AD8A23 (void);
// 0x0000043F System.Void MoreMountains.CorgiEngine.Health::SetHealth(System.Int32,UnityEngine.GameObject)
extern void Health_SetHealth_m36846027897C16E97C6823E85C09C1CB5FAD4CCB (void);
// 0x00000440 System.Void MoreMountains.CorgiEngine.Health::ResetHealthToMaxHealth()
extern void Health_ResetHealthToMaxHealth_m6F11008EA41889C8FC89438D2BEEEAC9763DC548 (void);
// 0x00000441 System.Void MoreMountains.CorgiEngine.Health::UpdateHealthBar(System.Boolean)
extern void Health_UpdateHealthBar_mDBBF761AF1DD362B7882925EF60735118AB18337 (void);
// 0x00000442 System.Void MoreMountains.CorgiEngine.Health::DamageDisabled()
extern void Health_DamageDisabled_m6AE8AAFD06C916C603E0D23CAE0F8409FE8FE54B (void);
// 0x00000443 System.Void MoreMountains.CorgiEngine.Health::DamageEnabled()
extern void Health_DamageEnabled_m4D8D5AC3DC1815C77DFC4D357E649E221B855CD0 (void);
// 0x00000444 System.Void MoreMountains.CorgiEngine.Health::EnablePostDamageInvulnerability()
extern void Health_EnablePostDamageInvulnerability_m700AD9A4BA16A435F603712C6036D4D209BAA23D (void);
// 0x00000445 System.Void MoreMountains.CorgiEngine.Health::DisablePostDamageInvulnerability()
extern void Health_DisablePostDamageInvulnerability_m16AD3ADA2D6623704457E902058878AAB6D66EBF (void);
// 0x00000446 System.Collections.IEnumerator MoreMountains.CorgiEngine.Health::DisablePostDamageInvulnerability(System.Single)
extern void Health_DisablePostDamageInvulnerability_mF847417B0F14C5FAA3D08E1E3FD876F8FB634570 (void);
// 0x00000447 System.Collections.IEnumerator MoreMountains.CorgiEngine.Health::DamageEnabled(System.Single)
extern void Health_DamageEnabled_m50CEC9C15AA2120DB45E58B07788630D4B3CC5F8 (void);
// 0x00000448 System.Void MoreMountains.CorgiEngine.Health::OnEnable()
extern void Health_OnEnable_m8BCC03E38EF3E6149D4206D23110E99DE9784CAE (void);
// 0x00000449 System.Void MoreMountains.CorgiEngine.Health::OnDisable()
extern void Health_OnDisable_m0457D1D61D080718EE3316D297CC6EE3B6389170 (void);
// 0x0000044A System.Void MoreMountains.CorgiEngine.Health::Update()
extern void Health_Update_m133803A77C9642EE3F9995773ECEFAA2CFC7BE4C (void);
// 0x0000044B System.Void MoreMountains.CorgiEngine.Health::.ctor()
extern void Health__ctor_mC4AD594519EE02992BED5B75AC1ADDB3CC0F25AB (void);
// 0x0000044C System.Void MoreMountains.CorgiEngine.Health/OnHitDelegate::.ctor(System.Object,System.IntPtr)
extern void OnHitDelegate__ctor_mB9BDFF74215D777D81C9AD61BB8FD7C3E6A6FF05 (void);
// 0x0000044D System.Void MoreMountains.CorgiEngine.Health/OnHitDelegate::Invoke()
extern void OnHitDelegate_Invoke_m54BD00801CA9F530CC86D5F09C6145153EFF417B (void);
// 0x0000044E System.IAsyncResult MoreMountains.CorgiEngine.Health/OnHitDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnHitDelegate_BeginInvoke_mA3E934711F3040414417C7116706FBC1BE5A8CEB (void);
// 0x0000044F System.Void MoreMountains.CorgiEngine.Health/OnHitDelegate::EndInvoke(System.IAsyncResult)
extern void OnHitDelegate_EndInvoke_mE69101087B2743DB48C6D412BFC9102AB65DE435 (void);
// 0x00000450 System.Void MoreMountains.CorgiEngine.Health/OnHitZeroDelegate::.ctor(System.Object,System.IntPtr)
extern void OnHitZeroDelegate__ctor_m90565A40CD38270C18E13800DA83078D1712A778 (void);
// 0x00000451 System.Void MoreMountains.CorgiEngine.Health/OnHitZeroDelegate::Invoke()
extern void OnHitZeroDelegate_Invoke_mABD708A076CD158D0A6BA78009D0BD65D60D32F3 (void);
// 0x00000452 System.IAsyncResult MoreMountains.CorgiEngine.Health/OnHitZeroDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnHitZeroDelegate_BeginInvoke_m876BF1CC669FB235D7B6E0FFB83ADBE3FB5AE9E2 (void);
// 0x00000453 System.Void MoreMountains.CorgiEngine.Health/OnHitZeroDelegate::EndInvoke(System.IAsyncResult)
extern void OnHitZeroDelegate_EndInvoke_m0011CE610D4423C9C2B060F52AF28D7EA8F4D0B1 (void);
// 0x00000454 System.Void MoreMountains.CorgiEngine.Health/OnReviveDelegate::.ctor(System.Object,System.IntPtr)
extern void OnReviveDelegate__ctor_m09326A5F5670740073774F997F723D17AA7F4204 (void);
// 0x00000455 System.Void MoreMountains.CorgiEngine.Health/OnReviveDelegate::Invoke()
extern void OnReviveDelegate_Invoke_mBEBB92BC794CB1961B780FA5561B0E4612975244 (void);
// 0x00000456 System.IAsyncResult MoreMountains.CorgiEngine.Health/OnReviveDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnReviveDelegate_BeginInvoke_m1F6CF908B13354F1279CCF2DDF17919BB582F3B9 (void);
// 0x00000457 System.Void MoreMountains.CorgiEngine.Health/OnReviveDelegate::EndInvoke(System.IAsyncResult)
extern void OnReviveDelegate_EndInvoke_m9717C7E9C043F43C61043646B9B42C654464DDBD (void);
// 0x00000458 System.Void MoreMountains.CorgiEngine.Health/OnDeathDelegate::.ctor(System.Object,System.IntPtr)
extern void OnDeathDelegate__ctor_m890BD0E273FC72CA8A93254B7553228C3D97FB5E (void);
// 0x00000459 System.Void MoreMountains.CorgiEngine.Health/OnDeathDelegate::Invoke()
extern void OnDeathDelegate_Invoke_mAD2BCCB85275C25B43BCDCD8B15CD47C6077F8C6 (void);
// 0x0000045A System.IAsyncResult MoreMountains.CorgiEngine.Health/OnDeathDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnDeathDelegate_BeginInvoke_mA6776B2CFA48691D7ACD8EBCC727E52A539B92DB (void);
// 0x0000045B System.Void MoreMountains.CorgiEngine.Health/OnDeathDelegate::EndInvoke(System.IAsyncResult)
extern void OnDeathDelegate_EndInvoke_mEC73B9EFED35CA55A0403CDB92077BAA5118BFB1 (void);
// 0x0000045C System.Void MoreMountains.CorgiEngine.Health/<DisablePostDamageInvulnerability>d__71::.ctor(System.Int32)
extern void U3CDisablePostDamageInvulnerabilityU3Ed__71__ctor_mD0DB4364AB0919E5E326210501BD5C53D7C07971 (void);
// 0x0000045D System.Void MoreMountains.CorgiEngine.Health/<DisablePostDamageInvulnerability>d__71::System.IDisposable.Dispose()
extern void U3CDisablePostDamageInvulnerabilityU3Ed__71_System_IDisposable_Dispose_m901E6215FC106A69FD0D2C163482D5A884F58C5B (void);
// 0x0000045E System.Boolean MoreMountains.CorgiEngine.Health/<DisablePostDamageInvulnerability>d__71::MoveNext()
extern void U3CDisablePostDamageInvulnerabilityU3Ed__71_MoveNext_mB4CFDEA1E0DA695A1D754441239AF67B57EF5EA0 (void);
// 0x0000045F System.Object MoreMountains.CorgiEngine.Health/<DisablePostDamageInvulnerability>d__71::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisablePostDamageInvulnerabilityU3Ed__71_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01BA262BA443B182E602264E6E2904DCB8A2D1B7 (void);
// 0x00000460 System.Void MoreMountains.CorgiEngine.Health/<DisablePostDamageInvulnerability>d__71::System.Collections.IEnumerator.Reset()
extern void U3CDisablePostDamageInvulnerabilityU3Ed__71_System_Collections_IEnumerator_Reset_mC6729F4FD5AD7C629198FE6BB9E75A9D5BE705E7 (void);
// 0x00000461 System.Object MoreMountains.CorgiEngine.Health/<DisablePostDamageInvulnerability>d__71::System.Collections.IEnumerator.get_Current()
extern void U3CDisablePostDamageInvulnerabilityU3Ed__71_System_Collections_IEnumerator_get_Current_m0DF4A24E5A54F71F3D0745844C34C82293ECDEB3 (void);
// 0x00000462 System.Void MoreMountains.CorgiEngine.Health/<DamageEnabled>d__72::.ctor(System.Int32)
extern void U3CDamageEnabledU3Ed__72__ctor_m73FF2F50F426DBD509C1C628C5615A7434D5A78B (void);
// 0x00000463 System.Void MoreMountains.CorgiEngine.Health/<DamageEnabled>d__72::System.IDisposable.Dispose()
extern void U3CDamageEnabledU3Ed__72_System_IDisposable_Dispose_mA8F9B4123DE2A678350D7CD811FBF6AAE6EE483F (void);
// 0x00000464 System.Boolean MoreMountains.CorgiEngine.Health/<DamageEnabled>d__72::MoveNext()
extern void U3CDamageEnabledU3Ed__72_MoveNext_m62B86B60BBF1CE1A0329A5551E5431A3A8672F3E (void);
// 0x00000465 System.Object MoreMountains.CorgiEngine.Health/<DamageEnabled>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDamageEnabledU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4264404C5B6109B6853C49397A651EAD520B6C6F (void);
// 0x00000466 System.Void MoreMountains.CorgiEngine.Health/<DamageEnabled>d__72::System.Collections.IEnumerator.Reset()
extern void U3CDamageEnabledU3Ed__72_System_Collections_IEnumerator_Reset_m046C7B5E86485F93A74D0523682D2F09D6810204 (void);
// 0x00000467 System.Object MoreMountains.CorgiEngine.Health/<DamageEnabled>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CDamageEnabledU3Ed__72_System_Collections_IEnumerator_get_Current_m07B580BEC1F7BDA71C6B75C723C253153CDC4203 (void);
// 0x00000468 System.Void MoreMountains.CorgiEngine.HealthAutoRefill::Awake()
extern void HealthAutoRefill_Awake_m6F1D4A92B98D8C4B854DCFC1D5FC9571C4B2AF85 (void);
// 0x00000469 System.Void MoreMountains.CorgiEngine.HealthAutoRefill::Initialization()
extern void HealthAutoRefill_Initialization_m131DFD11AC3CD78ED93237DE428C0CCE940AC4E7 (void);
// 0x0000046A System.Void MoreMountains.CorgiEngine.HealthAutoRefill::Update()
extern void HealthAutoRefill_Update_mD773B31BFC5789FB372B52EF85BDA4094E66D1C8 (void);
// 0x0000046B System.Void MoreMountains.CorgiEngine.HealthAutoRefill::ProcessRefillHealth()
extern void HealthAutoRefill_ProcessRefillHealth_m63D46E31C8DDAE9F35C6CFD596317C611351701C (void);
// 0x0000046C System.Void MoreMountains.CorgiEngine.HealthAutoRefill::OnHit()
extern void HealthAutoRefill_OnHit_m9EA38E9676152C84C66E58861E38ACD35438A0D6 (void);
// 0x0000046D System.Void MoreMountains.CorgiEngine.HealthAutoRefill::OnEnable()
extern void HealthAutoRefill_OnEnable_mFF56909935C04F501CD7D721D5BC9BE46F862AD6 (void);
// 0x0000046E System.Void MoreMountains.CorgiEngine.HealthAutoRefill::OnDisable()
extern void HealthAutoRefill_OnDisable_m306FF5E7FB66F36643494795D7987DF3CC3B332D (void);
// 0x0000046F System.Void MoreMountains.CorgiEngine.HealthAutoRefill::.ctor()
extern void HealthAutoRefill__ctor_mE4B562BF9326E0D0D26FCCA0B0BC2903772BACD9 (void);
// 0x00000470 System.Void MoreMountains.CorgiEngine.AimMarker::Awake()
extern void AimMarker_Awake_m67E1CBC3382760AD02D2EA10ED2F7D1982818540 (void);
// 0x00000471 System.Void MoreMountains.CorgiEngine.AimMarker::Update()
extern void AimMarker_Update_mE03BB6305CC9FF0970933F109897EFDC9AC19BD5 (void);
// 0x00000472 System.Void MoreMountains.CorgiEngine.AimMarker::FollowTarget()
extern void AimMarker_FollowTarget_m4EC1100C77F901F189A68C0E94DB7BC2B946A244 (void);
// 0x00000473 System.Void MoreMountains.CorgiEngine.AimMarker::SetTarget(UnityEngine.Transform)
extern void AimMarker_SetTarget_mAACFED6FB799BF5F04B30CBFE99E26C61582A69C (void);
// 0x00000474 System.Void MoreMountains.CorgiEngine.AimMarker::HandleTargetChange()
extern void AimMarker_HandleTargetChange_mBA577D81A1408B1C2CC777989F9D8FD8D7B3DC3D (void);
// 0x00000475 System.Void MoreMountains.CorgiEngine.AimMarker::NoMoreTargets()
extern void AimMarker_NoMoreTargets_mF9FB19864485D73517873C0FA3C1988464557C6D (void);
// 0x00000476 System.Void MoreMountains.CorgiEngine.AimMarker::FirstTargetFound()
extern void AimMarker_FirstTargetFound_mCBA8A40E709AE9F6D3DEF55DA60CE2233FF49D80 (void);
// 0x00000477 System.Void MoreMountains.CorgiEngine.AimMarker::NewTargetFound()
extern void AimMarker_NewTargetFound_m796C5EF8F53DBABECBB97E19547C174E4C2250F1 (void);
// 0x00000478 System.Void MoreMountains.CorgiEngine.AimMarker::Disable()
extern void AimMarker_Disable_m1847368FB1085F318D01C563BDDF9FDAC4E484F9 (void);
// 0x00000479 System.Void MoreMountains.CorgiEngine.AimMarker::.ctor()
extern void AimMarker__ctor_m1D980F68D29845F8901E571E926605BBE76A6945 (void);
// 0x0000047A System.Void MoreMountains.CorgiEngine.Bomb::OnEnable()
extern void Bomb_OnEnable_mA8E9E4A6AEAE5F201C66A009EEC9B2339FA580E9 (void);
// 0x0000047B System.Void MoreMountains.CorgiEngine.Bomb::Initialization()
extern void Bomb_Initialization_m2107ABBEF6178AFA6566901A73B2D5ADA1229E92 (void);
// 0x0000047C System.Void MoreMountains.CorgiEngine.Bomb::Update()
extern void Bomb_Update_mE1BB1F43C5A42214B21CF233EEB02136393530A9 (void);
// 0x0000047D System.Void MoreMountains.CorgiEngine.Bomb::Destroy()
extern void Bomb_Destroy_mD657F64EBB650E9723D25A6D539F95485FBD9DCC (void);
// 0x0000047E System.Void MoreMountains.CorgiEngine.Bomb::EnableDamageArea()
extern void Bomb_EnableDamageArea_m3BC7ACE32F6F964A116BC5CD57C8DCD40CF6121E (void);
// 0x0000047F System.Void MoreMountains.CorgiEngine.Bomb::DisableDamageArea()
extern void Bomb_DisableDamageArea_mDB2DB6872EA558C886270E195BFD47453C8C2F45 (void);
// 0x00000480 System.Void MoreMountains.CorgiEngine.Bomb::.ctor()
extern void Bomb__ctor_m980C5E63C896897593C75AF26CA459CA4D66A7B9 (void);
// 0x00000481 System.Void MoreMountains.CorgiEngine.BouncyProjectile::Initialization()
extern void BouncyProjectile_Initialization_m57AB496520BA1C3CFD299BD35D352042EDFBB0A4 (void);
// 0x00000482 System.Void MoreMountains.CorgiEngine.BouncyProjectile::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BouncyProjectile_OnTriggerEnter2D_mC05D1805780F95F4A34F7EC1D3666CFB145552CD (void);
// 0x00000483 System.Void MoreMountains.CorgiEngine.BouncyProjectile::Colliding(UnityEngine.GameObject)
extern void BouncyProjectile_Colliding_mB346D732194B128C55FDAFD36E7F25CB8AA9BB59 (void);
// 0x00000484 System.Void MoreMountains.CorgiEngine.BouncyProjectile::PreventedCollision2D(UnityEngine.RaycastHit2D)
extern void BouncyProjectile_PreventedCollision2D_mEB798BE26BB13D991CBFB6F2DFDD350B3C31E076 (void);
// 0x00000485 System.Void MoreMountains.CorgiEngine.BouncyProjectile::EvaluateHit(UnityEngine.RaycastHit2D)
extern void BouncyProjectile_EvaluateHit_m957B208884895DF419248D9151A7B4C56D96E084 (void);
// 0x00000486 System.Void MoreMountains.CorgiEngine.BouncyProjectile::Bounce(UnityEngine.RaycastHit2D)
extern void BouncyProjectile_Bounce_m3A763AE958723A4D5F30ECC86712D9643FFB187D (void);
// 0x00000487 System.Void MoreMountains.CorgiEngine.BouncyProjectile::LateUpdate()
extern void BouncyProjectile_LateUpdate_m243B9BD5F72997919E3E655BAFDD93BF41529C19 (void);
// 0x00000488 System.Void MoreMountains.CorgiEngine.BouncyProjectile::.ctor()
extern void BouncyProjectile__ctor_m4263BBB05E4FE743F9C189CAE5E265D60FFE1C21 (void);
// 0x00000489 System.Boolean MoreMountains.CorgiEngine.ComboWeapon::get_ComboInProgress()
extern void ComboWeapon_get_ComboInProgress_m6E465DE96792C7463B36FFA38A6793C2042F74AA (void);
// 0x0000048A System.Void MoreMountains.CorgiEngine.ComboWeapon::Start()
extern void ComboWeapon_Start_m2ED5454C963E0D76770A8A17F60E0CB804293FC6 (void);
// 0x0000048B System.Void MoreMountains.CorgiEngine.ComboWeapon::Initialization()
extern void ComboWeapon_Initialization_m14B960B536642B33E0EF18EE488BF7102E5A28FC (void);
// 0x0000048C System.Void MoreMountains.CorgiEngine.ComboWeapon::Update()
extern void ComboWeapon_Update_m08B8D8F92B59FCAEC549F32E63E4E6EE8963C2FD (void);
// 0x0000048D System.Void MoreMountains.CorgiEngine.ComboWeapon::ResetCombo()
extern void ComboWeapon_ResetCombo_mC12F1962CE2B19E50189283DD4041707F9754A90 (void);
// 0x0000048E System.Void MoreMountains.CorgiEngine.ComboWeapon::WeaponStarted(MoreMountains.CorgiEngine.Weapon)
extern void ComboWeapon_WeaponStarted_m74276CAF382C84B9CCD6CD393A2ED39ECCDEBB2A (void);
// 0x0000048F System.Void MoreMountains.CorgiEngine.ComboWeapon::WeaponStopped(MoreMountains.CorgiEngine.Weapon)
extern void ComboWeapon_WeaponStopped_mF4CA6D170FC92B50C5F35F0ED05F837C038D59BC (void);
// 0x00000490 System.Void MoreMountains.CorgiEngine.ComboWeapon::FlipUnusedWeapons()
extern void ComboWeapon_FlipUnusedWeapons_m147E1753A729D0A502906223254AA814D7D4D95D (void);
// 0x00000491 System.Void MoreMountains.CorgiEngine.ComboWeapon::InitializeUnusedWeapons()
extern void ComboWeapon_InitializeUnusedWeapons_m3D5A2F7984B6991C4FC38CD487357CAD9C794DC0 (void);
// 0x00000492 System.Void MoreMountains.CorgiEngine.ComboWeapon::.ctor()
extern void ComboWeapon__ctor_mDFDF9B1D543CC4F0233F05AAB5D6C8994C1F0611 (void);
// 0x00000493 UnityEngine.RaycastHit MoreMountains.CorgiEngine.HitscanWeapon::get__hit()
extern void HitscanWeapon_get__hit_mFB19BD41A36D7D002A8467CD8378E8A1D51E8C26 (void);
// 0x00000494 System.Void MoreMountains.CorgiEngine.HitscanWeapon::set__hit(UnityEngine.RaycastHit)
extern void HitscanWeapon_set__hit_m317F95A5E550597963A755077292ED4EE439394C (void);
// 0x00000495 UnityEngine.RaycastHit2D MoreMountains.CorgiEngine.HitscanWeapon::get__hit2D()
extern void HitscanWeapon_get__hit2D_m3401E6B69A05D29D51F046A6633EB468685F269A (void);
// 0x00000496 System.Void MoreMountains.CorgiEngine.HitscanWeapon::set__hit2D(UnityEngine.RaycastHit2D)
extern void HitscanWeapon_set__hit2D_m7CF864E5C1798305329B67F9C214AC6A3A959D55 (void);
// 0x00000497 UnityEngine.Vector3 MoreMountains.CorgiEngine.HitscanWeapon::get__origin()
extern void HitscanWeapon_get__origin_m4610A6C6C917A9EB88C8362FFD5A3BFB8CBF9175 (void);
// 0x00000498 System.Void MoreMountains.CorgiEngine.HitscanWeapon::set__origin(UnityEngine.Vector3)
extern void HitscanWeapon_set__origin_m9F04B51AB1A06BF5443C20479F52D0637FF13F55 (void);
// 0x00000499 System.Void MoreMountains.CorgiEngine.HitscanWeapon::TestShoot()
extern void HitscanWeapon_TestShoot_m4DBF480C21E06701ED82465923B4262808B8237B (void);
// 0x0000049A System.Void MoreMountains.CorgiEngine.HitscanWeapon::Initialization()
extern void HitscanWeapon_Initialization_m170547C5BB838D87F0172DDDDC3E0AF92E71482A (void);
// 0x0000049B System.Void MoreMountains.CorgiEngine.HitscanWeapon::WeaponUse()
extern void HitscanWeapon_WeaponUse_m24F7B44F0379BBCD72C25C112F683D946F673CA9 (void);
// 0x0000049C System.Void MoreMountains.CorgiEngine.HitscanWeapon::DetermineDirection()
extern void HitscanWeapon_DetermineDirection_mD8F59F5C94690652180C183B101299287F18DEBE (void);
// 0x0000049D System.Void MoreMountains.CorgiEngine.HitscanWeapon::SpawnProjectile(UnityEngine.Vector3,System.Boolean)
extern void HitscanWeapon_SpawnProjectile_m3D2D33929A05A8ED0B348ACE423A73351E3AF329 (void);
// 0x0000049E System.Void MoreMountains.CorgiEngine.HitscanWeapon::HandleDamage()
extern void HitscanWeapon_HandleDamage_mCCC42CB6A7E76072CB4502591205F74B6DF9292B (void);
// 0x0000049F System.Void MoreMountains.CorgiEngine.HitscanWeapon::DetermineSpawnPosition()
extern void HitscanWeapon_DetermineSpawnPosition_mE69F628CBB34109D78614C28C25C901DDD21069B (void);
// 0x000004A0 System.Void MoreMountains.CorgiEngine.HitscanWeapon::OnDrawGizmosSelected()
extern void HitscanWeapon_OnDrawGizmosSelected_m64B1E66A2FF4F0A93FCE68C0081C588E2B547A97 (void);
// 0x000004A1 System.Void MoreMountains.CorgiEngine.HitscanWeapon::.ctor()
extern void HitscanWeapon__ctor_mEE6C38ED5A691B078ED24063D33E14E71A5C2E94 (void);
// 0x000004A2 System.Void MoreMountains.CorgiEngine.MeleeWeapon::Initialization()
extern void MeleeWeapon_Initialization_m57A3B84C9B52BB02B441326520031C80E07F0574 (void);
// 0x000004A3 System.Void MoreMountains.CorgiEngine.MeleeWeapon::CreateDamageArea()
extern void MeleeWeapon_CreateDamageArea_mB72B9119544887277870B6E266AAA829502620DC (void);
// 0x000004A4 System.Void MoreMountains.CorgiEngine.MeleeWeapon::WeaponUse()
extern void MeleeWeapon_WeaponUse_mA44AECA872A988B6C9632145A422437CF7F10E26 (void);
// 0x000004A5 System.Collections.IEnumerator MoreMountains.CorgiEngine.MeleeWeapon::MeleeWeaponAttack()
extern void MeleeWeapon_MeleeWeaponAttack_m8F12591D174504C2CE3A489F486EC98C9C3ABE41 (void);
// 0x000004A6 System.Void MoreMountains.CorgiEngine.MeleeWeapon::EnableDamageArea()
extern void MeleeWeapon_EnableDamageArea_mD04A9FFAF329694C1404714018CF0DC404C05905 (void);
// 0x000004A7 System.Void MoreMountains.CorgiEngine.MeleeWeapon::HandleMiss()
extern void MeleeWeapon_HandleMiss_m7EE09BBFFD4795F3639FC092722F465A89DF44FE (void);
// 0x000004A8 System.Void MoreMountains.CorgiEngine.MeleeWeapon::DisableDamageArea()
extern void MeleeWeapon_DisableDamageArea_mC3C33E91BBAD02D5335859C4BDA8430687F6A440 (void);
// 0x000004A9 System.Void MoreMountains.CorgiEngine.MeleeWeapon::DrawGizmos()
extern void MeleeWeapon_DrawGizmos_mE5C73834B6C0058EF6DFFFB5BA88972B240C5F95 (void);
// 0x000004AA System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnDrawGizmosSelected()
extern void MeleeWeapon_OnDrawGizmosSelected_mEFC8AAC66352208645E5E8F0B9BF550016034C2F (void);
// 0x000004AB System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnHit()
extern void MeleeWeapon_OnHit_m40389608B4F57E35956443395B72B5BE467F2B35 (void);
// 0x000004AC System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnHitDamageable()
extern void MeleeWeapon_OnHitDamageable_m9715D322A52C2352CDB99B1152A845FC21BB33C9 (void);
// 0x000004AD System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnHitNonDamageable()
extern void MeleeWeapon_OnHitNonDamageable_mAF41A7BB0AD3498CDE6A576D138F82B9323100F6 (void);
// 0x000004AE System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnKill()
extern void MeleeWeapon_OnKill_m31D4DE44909D72AEA1138CF05C07D67B0927F625 (void);
// 0x000004AF System.Void MoreMountains.CorgiEngine.MeleeWeapon::RegisterEvents()
extern void MeleeWeapon_RegisterEvents_m2CDFE68688419BEEC9585470271A29A92ED24578 (void);
// 0x000004B0 System.Void MoreMountains.CorgiEngine.MeleeWeapon::TurnWeaponOff()
extern void MeleeWeapon_TurnWeaponOff_m3856D496CE4C73E461AAB2852035FCFDDEF1E3E8 (void);
// 0x000004B1 System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnEnable()
extern void MeleeWeapon_OnEnable_m38888367454F083FFD5A597637CED81FF400B760 (void);
// 0x000004B2 System.Void MoreMountains.CorgiEngine.MeleeWeapon::OnDisable()
extern void MeleeWeapon_OnDisable_m9407384DEDC1BF7268FF77BACB20837CAF986693 (void);
// 0x000004B3 System.Void MoreMountains.CorgiEngine.MeleeWeapon::.ctor()
extern void MeleeWeapon__ctor_m4B8BEE7857917F349454C8E22C7B0D9FCE7F207E (void);
// 0x000004B4 System.Void MoreMountains.CorgiEngine.MeleeWeapon/<MeleeWeaponAttack>d__28::.ctor(System.Int32)
extern void U3CMeleeWeaponAttackU3Ed__28__ctor_m7F5D89C706B7D08CEBF0BC11E7579D24A2CF65FF (void);
// 0x000004B5 System.Void MoreMountains.CorgiEngine.MeleeWeapon/<MeleeWeaponAttack>d__28::System.IDisposable.Dispose()
extern void U3CMeleeWeaponAttackU3Ed__28_System_IDisposable_Dispose_m0C4F4F8F343BFD1A5EA64CB6BAA537B61D7AD098 (void);
// 0x000004B6 System.Boolean MoreMountains.CorgiEngine.MeleeWeapon/<MeleeWeaponAttack>d__28::MoveNext()
extern void U3CMeleeWeaponAttackU3Ed__28_MoveNext_m72CDF5C67A14E1806A1CEA42E7EB42E5E356A394 (void);
// 0x000004B7 System.Object MoreMountains.CorgiEngine.MeleeWeapon/<MeleeWeaponAttack>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMeleeWeaponAttackU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F52255C6251DA1C0546DC06A397EB124ED009FB (void);
// 0x000004B8 System.Void MoreMountains.CorgiEngine.MeleeWeapon/<MeleeWeaponAttack>d__28::System.Collections.IEnumerator.Reset()
extern void U3CMeleeWeaponAttackU3Ed__28_System_Collections_IEnumerator_Reset_m01C14AE9FBB61A37850C2309212246955971F862 (void);
// 0x000004B9 System.Object MoreMountains.CorgiEngine.MeleeWeapon/<MeleeWeaponAttack>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CMeleeWeaponAttackU3Ed__28_System_Collections_IEnumerator_get_Current_mB0312F55F4B46A74DE7A7BEDDC8AFE85E16F35A4 (void);
// 0x000004BA MoreMountains.CorgiEngine.DamageOnTouch MoreMountains.CorgiEngine.Projectile::get_TargetDamageOnTouch()
extern void Projectile_get_TargetDamageOnTouch_m3D2F299C54074C9AE58CCE3FEC750F285745E747 (void);
// 0x000004BB System.Void MoreMountains.CorgiEngine.Projectile::Awake()
extern void Projectile_Awake_mE5022422223D2408194C55D74507C65A217731E7 (void);
// 0x000004BC System.Collections.IEnumerator MoreMountains.CorgiEngine.Projectile::InitialInvulnerability()
extern void Projectile_InitialInvulnerability_mC0824FD08892F41F46AA3E9CA0FC8FF63BBA69F1 (void);
// 0x000004BD System.Void MoreMountains.CorgiEngine.Projectile::Initialization()
extern void Projectile_Initialization_mB3CFCDA2EA38424F42380B375E91493C0C6D6C1E (void);
// 0x000004BE System.Void MoreMountains.CorgiEngine.Projectile::CheckForCollider()
extern void Projectile_CheckForCollider_mE832D9C38838245062986D85B1D6263E4086011C (void);
// 0x000004BF System.Void MoreMountains.CorgiEngine.Projectile::FixedUpdate()
extern void Projectile_FixedUpdate_m277ABA04AB4E7AE9156DA37B6A1A6127BA5AF4B5 (void);
// 0x000004C0 System.Void MoreMountains.CorgiEngine.Projectile::Movement()
extern void Projectile_Movement_m866F18E295EA8D84BFF5ACDCC04B0F05185BBD61 (void);
// 0x000004C1 System.Void MoreMountains.CorgiEngine.Projectile::SetDirection(UnityEngine.Vector3,UnityEngine.Quaternion,System.Boolean)
extern void Projectile_SetDirection_m0143B2BBC37E1DA6BCFAE92DB2AEF2AB83A062F9 (void);
// 0x000004C2 System.Void MoreMountains.CorgiEngine.Projectile::Flip()
extern void Projectile_Flip_m376D14A7B088875B2520E2033F6F53CA7214E824 (void);
// 0x000004C3 System.Void MoreMountains.CorgiEngine.Projectile::SetWeapon(MoreMountains.CorgiEngine.Weapon)
extern void Projectile_SetWeapon_mF9C6DF314E0804EA50F8ED2E5C7817167029D552 (void);
// 0x000004C4 System.Void MoreMountains.CorgiEngine.Projectile::SetOwner(UnityEngine.GameObject)
extern void Projectile_SetOwner_mAFA258A3695263A0438EAAF46848EBCA37C429F3 (void);
// 0x000004C5 System.Void MoreMountains.CorgiEngine.Projectile::SetDamage(System.Int32)
extern void Projectile_SetDamage_m0BA9725DB63B40FE90BF1E7DA8FA40780A17A5A9 (void);
// 0x000004C6 System.Void MoreMountains.CorgiEngine.Projectile::OnHit()
extern void Projectile_OnHit_m58E0513336BFACE2FBA13DC73C47FCA004A78B0F (void);
// 0x000004C7 System.Void MoreMountains.CorgiEngine.Projectile::OnHitDamageable()
extern void Projectile_OnHitDamageable_m124515AC4C7A633D4B7AFA877D522157870C45E0 (void);
// 0x000004C8 System.Void MoreMountains.CorgiEngine.Projectile::OnHitNonDamageable()
extern void Projectile_OnHitNonDamageable_m103479B350805A1B06557744E034279F236F8B45 (void);
// 0x000004C9 System.Void MoreMountains.CorgiEngine.Projectile::OnKill()
extern void Projectile_OnKill_m85A5CA49B52843E677424FC5E655F99F1488DEE8 (void);
// 0x000004CA System.Void MoreMountains.CorgiEngine.Projectile::OnEnable()
extern void Projectile_OnEnable_m14AC5733DDA31140C9B099398362CF2D50DDD9C4 (void);
// 0x000004CB System.Void MoreMountains.CorgiEngine.Projectile::OnDisable()
extern void Projectile_OnDisable_m4ED895414905F60AC35DBB64C1F617D6120074A2 (void);
// 0x000004CC System.Void MoreMountains.CorgiEngine.Projectile::.ctor()
extern void Projectile__ctor_m3FFECCB2BF3F12AE198F5B0B57CBC5823F95F3B6 (void);
// 0x000004CD System.Void MoreMountains.CorgiEngine.Projectile/<InitialInvulnerability>d__31::.ctor(System.Int32)
extern void U3CInitialInvulnerabilityU3Ed__31__ctor_m43C16B5EA3DD013749C5E9B1DF3F956F1C54C669 (void);
// 0x000004CE System.Void MoreMountains.CorgiEngine.Projectile/<InitialInvulnerability>d__31::System.IDisposable.Dispose()
extern void U3CInitialInvulnerabilityU3Ed__31_System_IDisposable_Dispose_m07AB19D4C935C8CE66050D853B883C6EE957DD32 (void);
// 0x000004CF System.Boolean MoreMountains.CorgiEngine.Projectile/<InitialInvulnerability>d__31::MoveNext()
extern void U3CInitialInvulnerabilityU3Ed__31_MoveNext_mBBB8143BE71E5ED7AE8382A50722C92A85F1A562 (void);
// 0x000004D0 System.Object MoreMountains.CorgiEngine.Projectile/<InitialInvulnerability>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitialInvulnerabilityU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDE762D64A6B16D8A6249599D31E6B15520CD27E (void);
// 0x000004D1 System.Void MoreMountains.CorgiEngine.Projectile/<InitialInvulnerability>d__31::System.Collections.IEnumerator.Reset()
extern void U3CInitialInvulnerabilityU3Ed__31_System_Collections_IEnumerator_Reset_m53E51EB7FE241719BE1BE174BE70627E34C2544D (void);
// 0x000004D2 System.Object MoreMountains.CorgiEngine.Projectile/<InitialInvulnerability>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CInitialInvulnerabilityU3Ed__31_System_Collections_IEnumerator_get_Current_mFEF8D82FF2DD9DA6C06CF3750EB808364DC548AA (void);
// 0x000004D3 MoreMountains.Tools.MMObjectPooler MoreMountains.CorgiEngine.ProjectileWeapon::get_ObjectPooler()
extern void ProjectileWeapon_get_ObjectPooler_m8BD2D66EBED6AC297E766B0D2D34BF70DAE0865B (void);
// 0x000004D4 System.Void MoreMountains.CorgiEngine.ProjectileWeapon::set_ObjectPooler(MoreMountains.Tools.MMObjectPooler)
extern void ProjectileWeapon_set_ObjectPooler_mA872BB162C9B6CC2739E2BCAF8FD68539D4274EC (void);
// 0x000004D5 System.Void MoreMountains.CorgiEngine.ProjectileWeapon::Initialization()
extern void ProjectileWeapon_Initialization_m8302C2DE40BBEB81D2B167D72F08A44F257B18E4 (void);
// 0x000004D6 System.Void MoreMountains.CorgiEngine.ProjectileWeapon::WeaponUse()
extern void ProjectileWeapon_WeaponUse_mB16D44C2D9BDF19C232114582121F586B9D759BE (void);
// 0x000004D7 UnityEngine.GameObject MoreMountains.CorgiEngine.ProjectileWeapon::SpawnProjectile(UnityEngine.Vector3,System.Int32,System.Int32,System.Boolean)
extern void ProjectileWeapon_SpawnProjectile_m29AD2F1870D294DB513E4EC04863045837E4E529 (void);
// 0x000004D8 System.Void MoreMountains.CorgiEngine.ProjectileWeapon::DetermineSpawnPosition()
extern void ProjectileWeapon_DetermineSpawnPosition_m728EE202E57B7A24B938D4061B6D1AD498B1B0DA (void);
// 0x000004D9 System.Void MoreMountains.CorgiEngine.ProjectileWeapon::OnDrawGizmosSelected()
extern void ProjectileWeapon_OnDrawGizmosSelected_mB09E805F100AEE2C5E4043D78374A95840555AE6 (void);
// 0x000004DA System.Void MoreMountains.CorgiEngine.ProjectileWeapon::.ctor()
extern void ProjectileWeapon__ctor_m711DDB15E46EADB7923DE85413B83E9FC852BD1D (void);
// 0x000004DB System.Void MoreMountains.CorgiEngine.ThrownObject::Initialization()
extern void ThrownObject_Initialization_m448A65F0CEDDDF5D7442FAFE5EF04492907FF1D3 (void);
// 0x000004DC System.Void MoreMountains.CorgiEngine.ThrownObject::OnEnable()
extern void ThrownObject_OnEnable_m85C6A9797C31FCEBA57A8DF68F43A4F7400B6E64 (void);
// 0x000004DD System.Void MoreMountains.CorgiEngine.ThrownObject::Movement()
extern void ThrownObject_Movement_m5CA7492F36D279BEDD2D199849A08ED45876D26C (void);
// 0x000004DE System.Void MoreMountains.CorgiEngine.ThrownObject::OrientAlongTrajectory()
extern void ThrownObject_OrientAlongTrajectory_m6FBE7B1DA5408411142F7B44922AB7A243CEEEA5 (void);
// 0x000004DF System.Void MoreMountains.CorgiEngine.ThrownObject::.ctor()
extern void ThrownObject__ctor_mCAD22F80E4DDD4963495701863CA29B303028E72 (void);
// 0x000004E0 System.Void MoreMountains.CorgiEngine.WeaponRecoilProperties::.ctor()
extern void WeaponRecoilProperties__ctor_m23D2763552B876C56C8EB5736D2F2CC400690E8C (void);
// 0x000004E1 System.String MoreMountains.CorgiEngine.Weapon::get_WeaponID()
extern void Weapon_get_WeaponID_m3BA0C85BF07B3D6BA755AB917C19573A6881F138 (void);
// 0x000004E2 System.Void MoreMountains.CorgiEngine.Weapon::set_WeaponID(System.String)
extern void Weapon_set_WeaponID_m66BE3D925576B538927B95DCD09AFBEEE034C8DC (void);
// 0x000004E3 MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.Weapon::get_Owner()
extern void Weapon_get_Owner_m33C456C2AAD62CDE75B48081955AD8791276BD5E (void);
// 0x000004E4 System.Void MoreMountains.CorgiEngine.Weapon::set_Owner(MoreMountains.CorgiEngine.Character)
extern void Weapon_set_Owner_m7DEEE4FB816571992318C753D4F66DA07DE97B65 (void);
// 0x000004E5 MoreMountains.CorgiEngine.CharacterHandleWeapon MoreMountains.CorgiEngine.Weapon::get_CharacterHandleWeapon()
extern void Weapon_get_CharacterHandleWeapon_mFEFB5EA89E00CFCA1B0ABF2DEC35FDCC682BEA76 (void);
// 0x000004E6 System.Void MoreMountains.CorgiEngine.Weapon::set_CharacterHandleWeapon(MoreMountains.CorgiEngine.CharacterHandleWeapon)
extern void Weapon_set_CharacterHandleWeapon_m3E6676FEDC239160638844EC0060EC087B7A076A (void);
// 0x000004E7 System.Boolean MoreMountains.CorgiEngine.Weapon::get_Flipped()
extern void Weapon_get_Flipped_m4EFFAB8CE7700C8C12020A56766D331F372F8110 (void);
// 0x000004E8 System.Void MoreMountains.CorgiEngine.Weapon::set_Flipped(System.Boolean)
extern void Weapon_set_Flipped_mD31AF7387ADFC67AC46BFE6009624E12A2EAD9F0 (void);
// 0x000004E9 MoreMountains.CorgiEngine.WeaponAmmo MoreMountains.CorgiEngine.Weapon::get_WeaponAmmo()
extern void Weapon_get_WeaponAmmo_m59B1F659CE7AC02E0D6D31E0C62404ED6D0E3070 (void);
// 0x000004EA System.Void MoreMountains.CorgiEngine.Weapon::set_WeaponAmmo(MoreMountains.CorgiEngine.WeaponAmmo)
extern void Weapon_set_WeaponAmmo_mD46CA94057BADBBC3F7457A907935BBD047DB8EE (void);
// 0x000004EB System.Void MoreMountains.CorgiEngine.Weapon::Initialization()
extern void Weapon_Initialization_mE583EADE95F9662407E870032E33BB04C1131DA2 (void);
// 0x000004EC System.Void MoreMountains.CorgiEngine.Weapon::InitializeFeedbacks()
extern void Weapon_InitializeFeedbacks_mD5B0F86179535587E9902C7576C047C55EB55ADD (void);
// 0x000004ED System.Void MoreMountains.CorgiEngine.Weapon::InitializeComboWeapons()
extern void Weapon_InitializeComboWeapons_mE63CF1398D090F918C581B90F02B47728B0ADDE5 (void);
// 0x000004EE System.Void MoreMountains.CorgiEngine.Weapon::SetOwner(MoreMountains.CorgiEngine.Character,MoreMountains.CorgiEngine.CharacterHandleWeapon)
extern void Weapon_SetOwner_m71388CE21523933A05AD40544F2CE020414F3C8E (void);
// 0x000004EF System.Void MoreMountains.CorgiEngine.Weapon::Update()
extern void Weapon_Update_mE5F951C484C0CBFBF08FA5874B87126278BBAAF9 (void);
// 0x000004F0 System.Void MoreMountains.CorgiEngine.Weapon::LateUpdate()
extern void Weapon_LateUpdate_m92B9FC9396A5416E4503CCB35531A556923CFA87 (void);
// 0x000004F1 System.Void MoreMountains.CorgiEngine.Weapon::WeaponInputStart()
extern void Weapon_WeaponInputStart_mB0273B84A333CB474ECD6B057F1470B7ADE5FB42 (void);
// 0x000004F2 System.Void MoreMountains.CorgiEngine.Weapon::WeaponInputStop()
extern void Weapon_WeaponInputStop_m2FAC0FE9CD300266D21BEEE73974CDB4D8DA4ED7 (void);
// 0x000004F3 System.Void MoreMountains.CorgiEngine.Weapon::TurnWeaponOn()
extern void Weapon_TurnWeaponOn_m9E671CD06FC56BEDA1EC55C443A67FFF5CB89B37 (void);
// 0x000004F4 System.Void MoreMountains.CorgiEngine.Weapon::TurnWeaponOff()
extern void Weapon_TurnWeaponOff_m0D8614D13E4224D1E65656156226B1365AA0AF89 (void);
// 0x000004F5 System.Void MoreMountains.CorgiEngine.Weapon::Interrupt()
extern void Weapon_Interrupt_m15177FA11F47B787C6F1ED2999455214BAC02183 (void);
// 0x000004F6 System.Collections.IEnumerator MoreMountains.CorgiEngine.Weapon::ApplyForceWhileInUseCo()
extern void Weapon_ApplyForceWhileInUseCo_m7F7A446997F107B73A167110542067612A6F7073 (void);
// 0x000004F7 System.Void MoreMountains.CorgiEngine.Weapon::ProcessWeaponState()
extern void Weapon_ProcessWeaponState_m3A2D374D5595BFAACDFDB57B627B344C7E9CCCD6 (void);
// 0x000004F8 System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponIdle()
extern void Weapon_CaseWeaponIdle_m63DB78BB7B169856EED8BEB4B3FF3509B88B81AC (void);
// 0x000004F9 System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponStart()
extern void Weapon_CaseWeaponStart_m22A4AE37DDD7C5B0B258DF7DCAE46C5953FBAFA0 (void);
// 0x000004FA System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponDelayBeforeUse()
extern void Weapon_CaseWeaponDelayBeforeUse_m131CFD68B90796EB41BC4102FEC611791E03A732 (void);
// 0x000004FB System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponUse()
extern void Weapon_CaseWeaponUse_mAD6C83A153DFAFDC14186078DE44476EDB24F9F2 (void);
// 0x000004FC System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponDelayBetweenUses()
extern void Weapon_CaseWeaponDelayBetweenUses_mCC263AFED161B173397DA681B5A091324101692C (void);
// 0x000004FD System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponStop()
extern void Weapon_CaseWeaponStop_mEFCAB0B70CCA738EB42C5B92A32862C7CD37D92E (void);
// 0x000004FE System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponReloadNeeded()
extern void Weapon_CaseWeaponReloadNeeded_m33EF76452C1DC25075FCC915D1379E17D3A5AF56 (void);
// 0x000004FF System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponReloadStart()
extern void Weapon_CaseWeaponReloadStart_m15FEDC2CB9F74FB69DDA40D2598D152BA87E25BF (void);
// 0x00000500 System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponReload()
extern void Weapon_CaseWeaponReload_m50D7F7FF875F3816BC81A7C90CDB1D437AFEC7A0 (void);
// 0x00000501 System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponReloadStop()
extern void Weapon_CaseWeaponReloadStop_m7BE485F7785A6E7E8919ED64F49C3BD3272D8562 (void);
// 0x00000502 System.Void MoreMountains.CorgiEngine.Weapon::CaseWeaponInterrupted()
extern void Weapon_CaseWeaponInterrupted_m030675C3A9309707E3F947349D56E397E5A8DA97 (void);
// 0x00000503 System.Void MoreMountains.CorgiEngine.Weapon::WeaponUse()
extern void Weapon_WeaponUse_mF9C6516D1B47595AB900A1794138C6EA59A051EF (void);
// 0x00000504 System.Void MoreMountains.CorgiEngine.Weapon::ShootRequest()
extern void Weapon_ShootRequest_mCF5C96795F217B6D33B5528A1E2DD77B87A71AD3 (void);
// 0x00000505 System.Void MoreMountains.CorgiEngine.Weapon::ResetMovementMultiplier()
extern void Weapon_ResetMovementMultiplier_mDC6C4C2C9E96EE388D70BF0B35BCEA65A3E68B4B (void);
// 0x00000506 System.Void MoreMountains.CorgiEngine.Weapon::ReloadNeeded()
extern void Weapon_ReloadNeeded_m3C57D1BC7F415996422AB1D320F2DDBAA519988C (void);
// 0x00000507 System.Void MoreMountains.CorgiEngine.Weapon::InitiateReloadWeapon()
extern void Weapon_InitiateReloadWeapon_mCA350C0E659D17F304E1C1EBB8B7E838631F6397 (void);
// 0x00000508 System.Void MoreMountains.CorgiEngine.Weapon::ReloadWeapon()
extern void Weapon_ReloadWeapon_m29B538AC012D0AAFFE27956E5A6E072AF37CAFB0 (void);
// 0x00000509 System.Void MoreMountains.CorgiEngine.Weapon::FlipWeapon()
extern void Weapon_FlipWeapon_mD0F109B436E5327FD9B7ECA23244746689E08C5B (void);
// 0x0000050A System.Void MoreMountains.CorgiEngine.Weapon::FlipWeaponModel()
extern void Weapon_FlipWeaponModel_mC3FE6F89D96B813BE5862BF6E76371398E5974F2 (void);
// 0x0000050B System.Collections.IEnumerator MoreMountains.CorgiEngine.Weapon::WeaponDestruction()
extern void Weapon_WeaponDestruction_m0BC299FA9C8E6464F8E0CBC803E0E3926F9BD0D9 (void);
// 0x0000050C System.Void MoreMountains.CorgiEngine.Weapon::ApplyOffset()
extern void Weapon_ApplyOffset_m0280780D3FB2037D4088D45B0AB5BD68662DB794 (void);
// 0x0000050D System.Void MoreMountains.CorgiEngine.Weapon::ApplyRecoil(System.Boolean,MoreMountains.CorgiEngine.WeaponRecoilProperties)
extern void Weapon_ApplyRecoil_mDAD0AD779CB6E7C961658499BFDD639863E6243E (void);
// 0x0000050E System.Collections.IEnumerator MoreMountains.CorgiEngine.Weapon::ApplyRecoilCoroutine(MoreMountains.CorgiEngine.WeaponRecoilProperties)
extern void Weapon_ApplyRecoilCoroutine_m26B62BE3D96E653C5315D4AA2686D36185E6C2E7 (void);
// 0x0000050F System.Void MoreMountains.CorgiEngine.Weapon::ApplyRecoilInternal(MoreMountains.CorgiEngine.WeaponRecoilProperties)
extern void Weapon_ApplyRecoilInternal_m9D1E4A1A13F510F5D49DAF692B3C70040CEE912F (void);
// 0x00000510 UnityEngine.Vector2 MoreMountains.CorgiEngine.Weapon::GetRecoilDirection()
extern void Weapon_GetRecoilDirection_m37C305C245B9CB0DB4992CA0195DD49025723081 (void);
// 0x00000511 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponStartFeedback()
extern void Weapon_TriggerWeaponStartFeedback_m13AE595369163C3CC32DC82C80068C516259097F (void);
// 0x00000512 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponUsedFeedback()
extern void Weapon_TriggerWeaponUsedFeedback_mD9681D98858F68CEF99E92A1AF13482275CCFEAB (void);
// 0x00000513 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponStopFeedback()
extern void Weapon_TriggerWeaponStopFeedback_m85CA495ED0D365C18DCE9941A78AAAABAA8E847B (void);
// 0x00000514 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponReloadNeededFeedback()
extern void Weapon_TriggerWeaponReloadNeededFeedback_m18AB646A2701B0E1285F685D078261330C5ED6EC (void);
// 0x00000515 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponReloadFeedback()
extern void Weapon_TriggerWeaponReloadFeedback_mC86A2EA0E8461B31A1C6301C5A4A3E6EC8A318E1 (void);
// 0x00000516 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponOnHitFeedback()
extern void Weapon_TriggerWeaponOnHitFeedback_mB8ABFB77982A7051C541F1C0A46F558A13D2EC82 (void);
// 0x00000517 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponOnMissFeedback()
extern void Weapon_TriggerWeaponOnMissFeedback_mB680DADE8022B89DC87504CB12105297D71659BA (void);
// 0x00000518 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponOnHitDamageableFeedback()
extern void Weapon_TriggerWeaponOnHitDamageableFeedback_m47DB44701C9C875AE3228394CB1DB29B950FA4E0 (void);
// 0x00000519 System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponOnHitNonDamageableFeedback()
extern void Weapon_TriggerWeaponOnHitNonDamageableFeedback_mA9D4DF193F320A189DEB36996A09B8E61C2C9FE5 (void);
// 0x0000051A System.Void MoreMountains.CorgiEngine.Weapon::TriggerWeaponOnKillFeedback()
extern void Weapon_TriggerWeaponOnKillFeedback_mC6C0D5BF50DC2017F8F4215776DD6EC0D983E4B7 (void);
// 0x0000051B System.Void MoreMountains.CorgiEngine.Weapon::InitializeAnimatorParameters()
extern void Weapon_InitializeAnimatorParameters_mD5483E3C9206C46033704BF41B948D3951A3EA3E (void);
// 0x0000051C System.Void MoreMountains.CorgiEngine.Weapon::AddParametersToAnimator(UnityEngine.Animator,System.Collections.Generic.HashSet`1<System.Int32>)
extern void Weapon_AddParametersToAnimator_m5184F357457D9AD11706D6410329FEE29AA1D46E (void);
// 0x0000051D System.Void MoreMountains.CorgiEngine.Weapon::UpdateAnimator()
extern void Weapon_UpdateAnimator_mFF2A7945599C8FCBA6D2F467A2433FCDFA9B76A5 (void);
// 0x0000051E System.Void MoreMountains.CorgiEngine.Weapon::UpdateAnimator(UnityEngine.Animator,System.Collections.Generic.HashSet`1<System.Int32>)
extern void Weapon_UpdateAnimator_m3EBF3210C46C801DB3C9A25723146917AE4470AD (void);
// 0x0000051F System.Void MoreMountains.CorgiEngine.Weapon::ResetComboAnimatorParameter()
extern void Weapon_ResetComboAnimatorParameter_m6218A0348140784468E3E6CBEF44445B019786C5 (void);
// 0x00000520 System.Void MoreMountains.CorgiEngine.Weapon::WeaponHit()
extern void Weapon_WeaponHit_m932E9333D8C4E2EA71730D619B63EB8507CB7D16 (void);
// 0x00000521 System.Void MoreMountains.CorgiEngine.Weapon::WeaponHitDamageable()
extern void Weapon_WeaponHitDamageable_mEFC81A45706D9646BFEA3A7AFD48194E6EF1AFF5 (void);
// 0x00000522 System.Void MoreMountains.CorgiEngine.Weapon::WeaponHitNonDamageable()
extern void Weapon_WeaponHitNonDamageable_m383C1B65067F5C2E2A99A093F061266AD65682D0 (void);
// 0x00000523 System.Void MoreMountains.CorgiEngine.Weapon::WeaponMiss()
extern void Weapon_WeaponMiss_m9FDDFEC2999428B597AB0AE967DB2761DAB14AD3 (void);
// 0x00000524 System.Void MoreMountains.CorgiEngine.Weapon::WeaponKill()
extern void Weapon_WeaponKill_m62BED09B8D7C50EA82E1C4B0F9245AE6FA85A682 (void);
// 0x00000525 System.Void MoreMountains.CorgiEngine.Weapon::.ctor()
extern void Weapon__ctor_m6C8A5A9BB0A404907A5BD60364DC16EC2F4120EF (void);
// 0x00000526 System.Void MoreMountains.CorgiEngine.Weapon/<ApplyForceWhileInUseCo>d__135::.ctor(System.Int32)
extern void U3CApplyForceWhileInUseCoU3Ed__135__ctor_mA3E7077C208827B7A935E0F6A68339E80468D584 (void);
// 0x00000527 System.Void MoreMountains.CorgiEngine.Weapon/<ApplyForceWhileInUseCo>d__135::System.IDisposable.Dispose()
extern void U3CApplyForceWhileInUseCoU3Ed__135_System_IDisposable_Dispose_m139FD857A8162434C3A543038FF1D499D2BC0E97 (void);
// 0x00000528 System.Boolean MoreMountains.CorgiEngine.Weapon/<ApplyForceWhileInUseCo>d__135::MoveNext()
extern void U3CApplyForceWhileInUseCoU3Ed__135_MoveNext_m4ED638F718C524910FAA05ECE3EFDF19A8C2BAAF (void);
// 0x00000529 System.Object MoreMountains.CorgiEngine.Weapon/<ApplyForceWhileInUseCo>d__135::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CApplyForceWhileInUseCoU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65107CD9E2E3E0500C17F1F661BACF0F8B365EA6 (void);
// 0x0000052A System.Void MoreMountains.CorgiEngine.Weapon/<ApplyForceWhileInUseCo>d__135::System.Collections.IEnumerator.Reset()
extern void U3CApplyForceWhileInUseCoU3Ed__135_System_Collections_IEnumerator_Reset_mAAEE89E144840F6C5377DCF02784430551A247CD (void);
// 0x0000052B System.Object MoreMountains.CorgiEngine.Weapon/<ApplyForceWhileInUseCo>d__135::System.Collections.IEnumerator.get_Current()
extern void U3CApplyForceWhileInUseCoU3Ed__135_System_Collections_IEnumerator_get_Current_mF82B316DE2341D6AD771A43029BCF2F313C7D0C8 (void);
// 0x0000052C System.Void MoreMountains.CorgiEngine.Weapon/<WeaponDestruction>d__156::.ctor(System.Int32)
extern void U3CWeaponDestructionU3Ed__156__ctor_m7162209A17BFAA25544973FB46E8CAC146F503C6 (void);
// 0x0000052D System.Void MoreMountains.CorgiEngine.Weapon/<WeaponDestruction>d__156::System.IDisposable.Dispose()
extern void U3CWeaponDestructionU3Ed__156_System_IDisposable_Dispose_mBEA2739CED71C08A3E851BAF1E0088B7E715ABC4 (void);
// 0x0000052E System.Boolean MoreMountains.CorgiEngine.Weapon/<WeaponDestruction>d__156::MoveNext()
extern void U3CWeaponDestructionU3Ed__156_MoveNext_mEC38E3394D2C2F4BF5F23463B153FF5BB0FA9C81 (void);
// 0x0000052F System.Object MoreMountains.CorgiEngine.Weapon/<WeaponDestruction>d__156::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWeaponDestructionU3Ed__156_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6CE42878024E6115D2B046A2794DE2DE8574AE1F (void);
// 0x00000530 System.Void MoreMountains.CorgiEngine.Weapon/<WeaponDestruction>d__156::System.Collections.IEnumerator.Reset()
extern void U3CWeaponDestructionU3Ed__156_System_Collections_IEnumerator_Reset_m204CF06A0AD71CCD2911D4F706995ECDF277599A (void);
// 0x00000531 System.Object MoreMountains.CorgiEngine.Weapon/<WeaponDestruction>d__156::System.Collections.IEnumerator.get_Current()
extern void U3CWeaponDestructionU3Ed__156_System_Collections_IEnumerator_get_Current_mB5D3B0F99AA0A245ECCF13193891E50B3516FA69 (void);
// 0x00000532 System.Void MoreMountains.CorgiEngine.Weapon/<ApplyRecoilCoroutine>d__159::.ctor(System.Int32)
extern void U3CApplyRecoilCoroutineU3Ed__159__ctor_mBF7DFABB805A2299B15743A6B0E4F507E6A38999 (void);
// 0x00000533 System.Void MoreMountains.CorgiEngine.Weapon/<ApplyRecoilCoroutine>d__159::System.IDisposable.Dispose()
extern void U3CApplyRecoilCoroutineU3Ed__159_System_IDisposable_Dispose_mC87843CFAB0E108524F53C42B10AC8C2642D6E4C (void);
// 0x00000534 System.Boolean MoreMountains.CorgiEngine.Weapon/<ApplyRecoilCoroutine>d__159::MoveNext()
extern void U3CApplyRecoilCoroutineU3Ed__159_MoveNext_m6F5838A8DEEC33CBC50E06A3FF061957D589BF07 (void);
// 0x00000535 System.Object MoreMountains.CorgiEngine.Weapon/<ApplyRecoilCoroutine>d__159::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CApplyRecoilCoroutineU3Ed__159_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1AE09477C3DB7182B481A5FBEA033AEC3FD0410 (void);
// 0x00000536 System.Void MoreMountains.CorgiEngine.Weapon/<ApplyRecoilCoroutine>d__159::System.Collections.IEnumerator.Reset()
extern void U3CApplyRecoilCoroutineU3Ed__159_System_Collections_IEnumerator_Reset_m49A1B6FE8F36DA6271E2F609A54F8C482AB7EFF1 (void);
// 0x00000537 System.Object MoreMountains.CorgiEngine.Weapon/<ApplyRecoilCoroutine>d__159::System.Collections.IEnumerator.get_Current()
extern void U3CApplyRecoilCoroutineU3Ed__159_System_Collections_IEnumerator_get_Current_mBBFAB9E3759A5389F74C39A96E6C0A54017D2972 (void);
// 0x00000538 UnityEngine.Quaternion MoreMountains.CorgiEngine.WeaponAim::get_CurrentRotation()
extern void WeaponAim_get_CurrentRotation_m98AAB13839D535F3C2B21947CF6BC1D434251549 (void);
// 0x00000539 System.Single MoreMountains.CorgiEngine.WeaponAim::get_CurrentAngle()
extern void WeaponAim_get_CurrentAngle_m955AE63B7CE667B6A775D5FD3D1AFAFBA3FBE5B5 (void);
// 0x0000053A System.Void MoreMountains.CorgiEngine.WeaponAim::set_CurrentAngle(System.Single)
extern void WeaponAim_set_CurrentAngle_mD33B85C16678F400959749D8EBC1DFFCCF82D02D (void);
// 0x0000053B System.Single MoreMountains.CorgiEngine.WeaponAim::get_CurrentAngleRelative()
extern void WeaponAim_get_CurrentAngleRelative_m3E84E69AA4528117DFC8120E1012B61403794949 (void);
// 0x0000053C UnityEngine.Vector2 MoreMountains.CorgiEngine.WeaponAim::get_CurrentAimMultiplier()
extern void WeaponAim_get_CurrentAimMultiplier_m18A6A08FC9DFC939973F91088B1AC0A202FA4425 (void);
// 0x0000053D System.Void MoreMountains.CorgiEngine.WeaponAim::set_CurrentAimMultiplier(UnityEngine.Vector2)
extern void WeaponAim_set_CurrentAimMultiplier_mF4DC17A475DC99521C7BC26C334C51DD6E106EC6 (void);
// 0x0000053E System.Void MoreMountains.CorgiEngine.WeaponAim::Start()
extern void WeaponAim_Start_m8C1B6D59C342934121EF14B81306DA4DC299F3E7 (void);
// 0x0000053F System.Void MoreMountains.CorgiEngine.WeaponAim::Initialization()
extern void WeaponAim_Initialization_m1AF42288A130A0256FFE7443489EE8E9AFA066CB (void);
// 0x00000540 System.Void MoreMountains.CorgiEngine.WeaponAim::SetCurrentAim(UnityEngine.Vector3)
extern void WeaponAim_SetCurrentAim_m50A2A19BAE8725467EA651C75A25F8379B79DDCA (void);
// 0x00000541 System.Void MoreMountains.CorgiEngine.WeaponAim::GetCurrentAim()
extern void WeaponAim_GetCurrentAim_mB3B5FF1418979D4CFE236174B4D035A560EE12DA (void);
// 0x00000542 System.Void MoreMountains.CorgiEngine.WeaponAim::LateUpdate()
extern void WeaponAim_LateUpdate_m2C79338E8166C4AF59F3D94D24D09462D3370568 (void);
// 0x00000543 System.Void MoreMountains.CorgiEngine.WeaponAim::ResetCurrentAimMultiplier()
extern void WeaponAim_ResetCurrentAimMultiplier_mB14E148F8C6695598751132A8E98B265568D59D1 (void);
// 0x00000544 System.Void MoreMountains.CorgiEngine.WeaponAim::DetermineWeaponRotation()
extern void WeaponAim_DetermineWeaponRotation_mD5F416FE513A28524BC30B81CE428730EF7A8825 (void);
// 0x00000545 System.Void MoreMountains.CorgiEngine.WeaponAim::RotateWeapon(UnityEngine.Quaternion)
extern void WeaponAim_RotateWeapon_m5FCE5915A5FD73DD298423148D433ABE6D53CF79 (void);
// 0x00000546 System.Void MoreMountains.CorgiEngine.WeaponAim::InitializeReticle()
extern void WeaponAim_InitializeReticle_mE2332DDF9F1EFCA2088FC07765C67B8757D96E50 (void);
// 0x00000547 System.Void MoreMountains.CorgiEngine.WeaponAim::MoveReticle()
extern void WeaponAim_MoveReticle_mB5B9084DACCB0172100CF7903B40835C9A758E82 (void);
// 0x00000548 System.Void MoreMountains.CorgiEngine.WeaponAim::HideReticle()
extern void WeaponAim_HideReticle_m4D1D33E1238A307B7AEFFF86028B2869C91A1E86 (void);
// 0x00000549 System.Void MoreMountains.CorgiEngine.WeaponAim::AddAdditionalAngle(System.Single)
extern void WeaponAim_AddAdditionalAngle_m9F932187DCF45F6B72EF55311DD8C5BAC1709142 (void);
// 0x0000054A System.Void MoreMountains.CorgiEngine.WeaponAim::ResetAdditionalAngle()
extern void WeaponAim_ResetAdditionalAngle_mC679BE79F59A6BE3AEC7CC1C6068258E465F411B (void);
// 0x0000054B System.Void MoreMountains.CorgiEngine.WeaponAim::.ctor()
extern void WeaponAim__ctor_m5356DDFE1E1EE3EB912E0F91229BA378149C2101 (void);
// 0x0000054C MoreMountains.InventoryEngine.Inventory MoreMountains.CorgiEngine.WeaponAmmo::get_AmmoInventory()
extern void WeaponAmmo_get_AmmoInventory_mADB244648D44D7D996EDBE89741F6630080D9AA5 (void);
// 0x0000054D System.Void MoreMountains.CorgiEngine.WeaponAmmo::set_AmmoInventory(MoreMountains.InventoryEngine.Inventory)
extern void WeaponAmmo_set_AmmoInventory_mD65D062DE46D0DC4DB9DA5F2D3CB08D77162D31E (void);
// 0x0000054E System.Void MoreMountains.CorgiEngine.WeaponAmmo::Start()
extern void WeaponAmmo_Start_m0CC790782217139591AA8D162850CB988B2A13DC (void);
// 0x0000054F System.Void MoreMountains.CorgiEngine.WeaponAmmo::LoadOnStart()
extern void WeaponAmmo_LoadOnStart_mCCB575888F34BD9BF0F4ADA40E715767EC02E308 (void);
// 0x00000550 System.Void MoreMountains.CorgiEngine.WeaponAmmo::RefreshCurrentAmmoAvailable()
extern void WeaponAmmo_RefreshCurrentAmmoAvailable_m46371EE425FE214B4D1B568186D8D65453AA88DB (void);
// 0x00000551 System.Boolean MoreMountains.CorgiEngine.WeaponAmmo::EnoughAmmoToFire()
extern void WeaponAmmo_EnoughAmmoToFire_mC7E2457C87078504E806E597C248E2367F378B5E (void);
// 0x00000552 System.Void MoreMountains.CorgiEngine.WeaponAmmo::ConsumeAmmo()
extern void WeaponAmmo_ConsumeAmmo_m2AB217DB5E1DD1E1A49145B7F4223725C9BA373B (void);
// 0x00000553 System.Void MoreMountains.CorgiEngine.WeaponAmmo::FillWeaponWithAmmo()
extern void WeaponAmmo_FillWeaponWithAmmo_mB87B761B376B5D489E54F18F0899A98ECCB6D03A (void);
// 0x00000554 System.Void MoreMountains.CorgiEngine.WeaponAmmo::EmptyMagazine(System.Boolean)
extern void WeaponAmmo_EmptyMagazine_m8B9FB5523E6B066958963CF75782849E330E70BE (void);
// 0x00000555 System.Void MoreMountains.CorgiEngine.WeaponAmmo::OnMMEvent(MoreMountains.Tools.MMStateChangeEvent`1<MoreMountains.CorgiEngine.Weapon/WeaponStates>)
extern void WeaponAmmo_OnMMEvent_m3F98856E9FA1C0F73EE37A49BE44AA267846A2EF (void);
// 0x00000556 System.Void MoreMountains.CorgiEngine.WeaponAmmo::OnMMEvent(MoreMountains.InventoryEngine.MMInventoryEvent)
extern void WeaponAmmo_OnMMEvent_m104C9E273DEB556DA9F0A00C48DBD3C36C642939 (void);
// 0x00000557 System.Void MoreMountains.CorgiEngine.WeaponAmmo::OnMMEvent(MoreMountains.Tools.MMGameEvent)
extern void WeaponAmmo_OnMMEvent_m04B87B17D7A9FDFA47357909DDE3FDC50E9D4904 (void);
// 0x00000558 System.Void MoreMountains.CorgiEngine.WeaponAmmo::OnDestroy()
extern void WeaponAmmo_OnDestroy_m537A65E419D681FC1D518D94426EF3A530ADA4A5 (void);
// 0x00000559 System.Void MoreMountains.CorgiEngine.WeaponAmmo::OnEnable()
extern void WeaponAmmo_OnEnable_m463BF83028A579689071670BB4983596035DDDA9 (void);
// 0x0000055A System.Void MoreMountains.CorgiEngine.WeaponAmmo::OnDisable()
extern void WeaponAmmo_OnDisable_mE9F79BAC78D93917EF581A4AB4FDBE2A6421E3E2 (void);
// 0x0000055B System.Void MoreMountains.CorgiEngine.WeaponAmmo::.ctor()
extern void WeaponAmmo__ctor_mD9040A0C502FCCFAFEE6F0865FCE1424B643F0AD (void);
// 0x0000055C System.Void MoreMountains.CorgiEngine.WeaponAutoAim::Start()
extern void WeaponAutoAim_Start_m3B36E49CC878ADF737124C82839D1BA86B33FFAD (void);
// 0x0000055D System.Void MoreMountains.CorgiEngine.WeaponAutoAim::Initialization()
extern void WeaponAutoAim_Initialization_m2C116330F24BE4907A31754CEE268894CCD1455C (void);
// 0x0000055E System.Void MoreMountains.CorgiEngine.WeaponAutoAim::Update()
extern void WeaponAutoAim_Update_mBE54644D7474BB962345912607E9F048C1E72A47 (void);
// 0x0000055F System.Void MoreMountains.CorgiEngine.WeaponAutoAim::DetermineRaycastOrigin()
extern void WeaponAutoAim_DetermineRaycastOrigin_mA693454E289DFCD5DC3EC38FC8988A58AAAB59C8 (void);
// 0x00000560 System.Boolean MoreMountains.CorgiEngine.WeaponAutoAim::ScanForTargets()
extern void WeaponAutoAim_ScanForTargets_mEA219308E33FD903F93CB0F63104DFB2BD94830B (void);
// 0x00000561 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::SetAim()
extern void WeaponAutoAim_SetAim_mE58CB87A8FA633075B9EF1565B3F20CA7BC1D010 (void);
// 0x00000562 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::HandleTargetChange()
extern void WeaponAutoAim_HandleTargetChange_m980FC918D623B7E8638316553CAA5B8592C4D674 (void);
// 0x00000563 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::HandleToggleIgnoreSlopeRotation()
extern void WeaponAutoAim_HandleToggleIgnoreSlopeRotation_m50DD7A6DE216AF8C569AA5DBFDC5E98EEC37E01B (void);
// 0x00000564 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::NoMoreTargets()
extern void WeaponAutoAim_NoMoreTargets_mB73D3196A646578991EC078996E356053ADD71C0 (void);
// 0x00000565 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::FirstTargetFound()
extern void WeaponAutoAim_FirstTargetFound_m5CDDE84E9D46381377A6C5DEFD9330FE525DC2B1 (void);
// 0x00000566 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::NewTargetFound()
extern void WeaponAutoAim_NewTargetFound_m6D50CF956ED8521273D4299EA874BDF9355D82D3 (void);
// 0x00000567 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::HandleMoveCameraTarget()
extern void WeaponAutoAim_HandleMoveCameraTarget_mAB9EE5986561020905ED4766FC8341911D516CE2 (void);
// 0x00000568 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::ScanIfNeeded()
extern void WeaponAutoAim_ScanIfNeeded_m8E0FE5761CCEE558CEA8F4CD79E9ADE902052A0D (void);
// 0x00000569 System.Void MoreMountains.CorgiEngine.WeaponAutoAim::HandleTarget()
extern void WeaponAutoAim_HandleTarget_mB68D34E16120BB37FC648550D304CA6B1818EE92 (void);
// 0x0000056A System.Void MoreMountains.CorgiEngine.WeaponAutoAim::OnDrawGizmos()
extern void WeaponAutoAim_OnDrawGizmos_mFDDBF721AE44D46B341D63A1D3C55009DD89F532 (void);
// 0x0000056B System.Void MoreMountains.CorgiEngine.WeaponAutoAim::OnDisable()
extern void WeaponAutoAim_OnDisable_mB68E7BBFB9F762AFC45700C797701991694C981F (void);
// 0x0000056C System.Void MoreMountains.CorgiEngine.WeaponAutoAim::.ctor()
extern void WeaponAutoAim__ctor_m9FBC327F3EC6BBFE0B25F0F8EB87786E9A3DF2E3 (void);
// 0x0000056D System.Void MoreMountains.CorgiEngine.WeaponIK::Start()
extern void WeaponIK_Start_m86FEBF89AAFB299E6B3D2DD970745FCEBC0F21E8 (void);
// 0x0000056E System.Void MoreMountains.CorgiEngine.WeaponIK::OnAnimatorIK(System.Int32)
extern void WeaponIK_OnAnimatorIK_mFDD443E726C10060168DECF133DAE5E36BAF45AA (void);
// 0x0000056F System.Void MoreMountains.CorgiEngine.WeaponIK::AttachHandToHandle(UnityEngine.AvatarIKGoal,UnityEngine.Transform)
extern void WeaponIK_AttachHandToHandle_mC8E055CBEF31908A45438D8A43A663BC7EDB0369 (void);
// 0x00000570 System.Void MoreMountains.CorgiEngine.WeaponIK::DetachHandFromHandle(UnityEngine.AvatarIKGoal)
extern void WeaponIK_DetachHandFromHandle_mD3329C38E05EA68C50CF51A84F23F3C4FD36F10B (void);
// 0x00000571 System.Void MoreMountains.CorgiEngine.WeaponIK::SetHandles(UnityEngine.Transform,UnityEngine.Transform)
extern void WeaponIK_SetHandles_m2F50657F3A8FACF950FCCF0943E4BCB79EC4E168 (void);
// 0x00000572 System.Void MoreMountains.CorgiEngine.WeaponIK::.ctor()
extern void WeaponIK__ctor_m427B4FBC3721C40F6744DACDC0E4E764371DDD7E (void);
// 0x00000573 System.Void MoreMountains.CorgiEngine.WeaponLaserSight::Start()
extern void WeaponLaserSight_Start_m4393C8C47D2A526D9121CB5B84AB808B6C83E0DB (void);
// 0x00000574 System.Void MoreMountains.CorgiEngine.WeaponLaserSight::Initialization()
extern void WeaponLaserSight_Initialization_m1131843380F95E122BD7CEA4A44F61D5D7A64DB1 (void);
// 0x00000575 System.Void MoreMountains.CorgiEngine.WeaponLaserSight::Update()
extern void WeaponLaserSight_Update_mA1396DD59F52DAAA33B94648B37AD9DE7F4145E7 (void);
// 0x00000576 System.Void MoreMountains.CorgiEngine.WeaponLaserSight::ShootLaser()
extern void WeaponLaserSight_ShootLaser_mD030A75474599AAE027FE7F3072284BA1CE0A4BC (void);
// 0x00000577 System.Void MoreMountains.CorgiEngine.WeaponLaserSight::LaserActive(System.Boolean)
extern void WeaponLaserSight_LaserActive_m080DC5EB7E0FDE45B2516E1D8543C7EC1ACA742B (void);
// 0x00000578 System.Void MoreMountains.CorgiEngine.WeaponLaserSight::.ctor()
extern void WeaponLaserSight__ctor_m6D43CF0B55436E6D11E778A40CA9266732DE6B52 (void);
// 0x00000579 System.Boolean MoreMountains.CorgiEngine.CameraController::get_FollowsPlayer()
extern void CameraController_get_FollowsPlayer_mB6B77CDEBB08A53068536E9249028A650CABF6EE (void);
// 0x0000057A System.Void MoreMountains.CorgiEngine.CameraController::set_FollowsPlayer(System.Boolean)
extern void CameraController_set_FollowsPlayer_m92599C3119AC286A6F8E1237D23EE0C72483C1F6 (void);
// 0x0000057B System.Void MoreMountains.CorgiEngine.CameraController::Initialization()
extern void CameraController_Initialization_m2C307DD7D71BE80C89109830E66E80DDFC6C846D (void);
// 0x0000057C System.Void MoreMountains.CorgiEngine.CameraController::AssignTarget()
extern void CameraController_AssignTarget_m0FB4A61D0774B5300EAAFC4058AF4FF80844469E (void);
// 0x0000057D System.Void MoreMountains.CorgiEngine.CameraController::SetTarget(UnityEngine.Transform)
extern void CameraController_SetTarget_m97D15A7FD049C9D54ABD6D0227E4E533D851CC19 (void);
// 0x0000057E System.Void MoreMountains.CorgiEngine.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m9BB71940632CF3182FFC9CD84F5F506F19EF598D (void);
// 0x0000057F System.Void MoreMountains.CorgiEngine.CameraController::Shake(UnityEngine.Vector3)
extern void CameraController_Shake_m6AF2CDFF90F3607648150F34193996BA5A374F1A (void);
// 0x00000580 System.Void MoreMountains.CorgiEngine.CameraController::LookUp()
extern void CameraController_LookUp_m9F87EBB8CEACF6A1C1657374A630CA067B35AEA0 (void);
// 0x00000581 System.Void MoreMountains.CorgiEngine.CameraController::LookDown()
extern void CameraController_LookDown_m8F264807273094FFA129023311EEFDABF26C18AA (void);
// 0x00000582 System.Void MoreMountains.CorgiEngine.CameraController::ResetLookUpDown()
extern void CameraController_ResetLookUpDown_m6DEE85B623855CFFCB46B0B7D23234DB3D9D61FA (void);
// 0x00000583 System.Void MoreMountains.CorgiEngine.CameraController::MakeCameraPixelPerfect()
extern void CameraController_MakeCameraPixelPerfect_m6DB9C5A7DE662B4768A47B4837DF5986BDCAC736 (void);
// 0x00000584 System.Void MoreMountains.CorgiEngine.CameraController::FollowPlayer()
extern void CameraController_FollowPlayer_m9AEE1B3B572E035A741F0E8C3B556B8DAE741211 (void);
// 0x00000585 System.Void MoreMountains.CorgiEngine.CameraController::Zoom()
extern void CameraController_Zoom_mB7E97A26ACDB104943471AE030153575AE5339EC (void);
// 0x00000586 System.Void MoreMountains.CorgiEngine.CameraController::GetLevelBounds()
extern void CameraController_GetLevelBounds_m918E4642037AD382C145A23B49B33A345038023E (void);
// 0x00000587 System.Void MoreMountains.CorgiEngine.CameraController::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void CameraController_OnMMEvent_mD635E642A94678584CEE856C6C72C02FBAD28EB2 (void);
// 0x00000588 System.Void MoreMountains.CorgiEngine.CameraController::StartFollowing()
extern void CameraController_StartFollowing_m342DBE8C569A4B71FB4476296D6B104B1AC829DB (void);
// 0x00000589 System.Void MoreMountains.CorgiEngine.CameraController::StopFollowing()
extern void CameraController_StopFollowing_m260B22AA581BAF120E3E4250D55CAD0F926209A4 (void);
// 0x0000058A System.Void MoreMountains.CorgiEngine.CameraController::OnMMEvent(MoreMountains.CorgiEngine.MMCameraEvent)
extern void CameraController_OnMMEvent_m2CF66391C49359C3490128F1CB9F630C28D3884B (void);
// 0x0000058B System.Void MoreMountains.CorgiEngine.CameraController::OnCameraShakeEvent(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Int32,System.Boolean)
extern void CameraController_OnCameraShakeEvent_m1CEF4769B144C9FCA0D8C21B666C76A0F7E26288 (void);
// 0x0000058C System.Void MoreMountains.CorgiEngine.CameraController::TeleportCameraToTarget()
extern void CameraController_TeleportCameraToTarget_m7689BD74B865BF617FB44A2F4E92F47006FF56B4 (void);
// 0x0000058D System.Void MoreMountains.CorgiEngine.CameraController::OnEnable()
extern void CameraController_OnEnable_m9B272EA8A5A8AE8D969109ED016D4401666B291E (void);
// 0x0000058E System.Void MoreMountains.CorgiEngine.CameraController::OnDisable()
extern void CameraController_OnDisable_m3AFA862ADE8FCA4A18E7C094EDE8F3EF5268E65E (void);
// 0x0000058F System.Void MoreMountains.CorgiEngine.CameraController::.ctor()
extern void CameraController__ctor_mE22AE01F7663664D43F1FD9A02C4E21A4ECE8AF2 (void);
// 0x00000590 System.Void MoreMountains.CorgiEngine.LevelBackground::OnEnable()
extern void LevelBackground_OnEnable_m4E32440BE7ED235A763F71CFF81F92D3AFC6078A (void);
// 0x00000591 System.Void MoreMountains.CorgiEngine.LevelBackground::StartFollowing()
extern void LevelBackground_StartFollowing_m68988E166DB80FED43DED62F4AFAD2F3BE34CAED (void);
// 0x00000592 System.Void MoreMountains.CorgiEngine.LevelBackground::StopFollowing()
extern void LevelBackground_StopFollowing_m3324003E3E1DF6576125675485F8FD47ACB8B0E5 (void);
// 0x00000593 System.Void MoreMountains.CorgiEngine.LevelBackground::SetZOffset(System.Single)
extern void LevelBackground_SetZOffset_m2B80CEC5D1FF51FCFF751203C92DD2FAD575F7F3 (void);
// 0x00000594 System.Void MoreMountains.CorgiEngine.LevelBackground::.ctor()
extern void LevelBackground__ctor_m23CB2B51A9B6E8219A299F0FC74CFCF0434B6F01 (void);
// 0x00000595 System.Void MoreMountains.CorgiEngine.MMCameraEvent::.ctor(MoreMountains.CorgiEngine.MMCameraEventTypes,MoreMountains.CorgiEngine.Character,UnityEngine.Collider,UnityEngine.Collider2D)
extern void MMCameraEvent__ctor_mBE0879CC29D68FAEA3CEBF1FA047E13543BC0EB0 (void);
// 0x00000596 System.Void MoreMountains.CorgiEngine.MMCameraEvent::Trigger(MoreMountains.CorgiEngine.MMCameraEventTypes,MoreMountains.CorgiEngine.Character,UnityEngine.Collider,UnityEngine.Collider2D)
extern void MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8 (void);
// 0x00000597 System.Collections.Generic.List`1<UnityEngine.Transform> MoreMountains.CorgiEngine.MultiplayerCameraController::get_Players()
extern void MultiplayerCameraController_get_Players_m2FA82F3D2E0C40F11195945234B423B87A96D000 (void);
// 0x00000598 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::set_Players(System.Collections.Generic.List`1<UnityEngine.Transform>)
extern void MultiplayerCameraController_set_Players_mBD7644735DB2451488B4CE38D86F3FCE47AE9DC8 (void);
// 0x00000599 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::Start()
extern void MultiplayerCameraController_Start_m4585252E76B7BCAFA14A25F85D06A2CD93CE2A0E (void);
// 0x0000059A System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::Initialization()
extern void MultiplayerCameraController_Initialization_m21EF94B074D52757444362ED9EEEDDA41F4BFAE5 (void);
// 0x0000059B System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::DetectTargets()
extern void MultiplayerCameraController_DetectTargets_mF4CC5A046113B8A762A8CF8C6970BB9CF8F4BE7E (void);
// 0x0000059C System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::FixedUpdate()
extern void MultiplayerCameraController_FixedUpdate_mECC6DAFCCF4E5971585931D5B00888314C048A4C (void);
// 0x0000059D System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::CameraMovement()
extern void MultiplayerCameraController_CameraMovement_m2014654B2479FE98DF02C288BB8EE470EBFCB748 (void);
// 0x0000059E System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::MoveCamera()
extern void MultiplayerCameraController_MoveCamera_m79C36A4597BC914D23D6338276E00C84EE40A5E2 (void);
// 0x0000059F System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::CleanPlayersList()
extern void MultiplayerCameraController_CleanPlayersList_m20C434EBC94C779DC2735B279674F03EBE357C91 (void);
// 0x000005A0 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::FindAveragePosition()
extern void MultiplayerCameraController_FindAveragePosition_m550EDBB0458CFBAD689EBB7BEDD5BC03C53CF200 (void);
// 0x000005A1 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::ComputeZoom()
extern void MultiplayerCameraController_ComputeZoom_m2107204A6C02E87C21AF0919BD6A629975A97348 (void);
// 0x000005A2 System.Single MoreMountains.CorgiEngine.MultiplayerCameraController::FindRequiredOrthographicSize()
extern void MultiplayerCameraController_FindRequiredOrthographicSize_mF73504D9727126475396E8143F575007C986433F (void);
// 0x000005A3 System.Single MoreMountains.CorgiEngine.MultiplayerCameraController::FindRequiredDistance()
extern void MultiplayerCameraController_FindRequiredDistance_m7A3E157EBC04FA87F826B1CE2F012C54D7028648 (void);
// 0x000005A4 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::GetLevelBounds()
extern void MultiplayerCameraController_GetLevelBounds_m69DC36755F36358B010D026A7C686637E7C1A019 (void);
// 0x000005A5 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::ClampNewPosition()
extern void MultiplayerCameraController_ClampNewPosition_m13EE2D8027895BD9A6DF300D6BBB4DA95B3045A3 (void);
// 0x000005A6 System.Void MoreMountains.CorgiEngine.MultiplayerCameraController::.ctor()
extern void MultiplayerCameraController__ctor_m0720F08064C9028C25DFB2ED7C739E1C92C7EB18 (void);
// 0x000005A7 System.Void MoreMountains.CorgiEngine.ParallaxCamera::.ctor()
extern void ParallaxCamera__ctor_mD4DACD6AB36F418EFC05FAA6BD1F2818B6492BDE (void);
// 0x000005A8 System.Void MoreMountains.CorgiEngine.ParallaxElement::OnEnable()
extern void ParallaxElement_OnEnable_m1BCA249F79E61BFD1AC178D8187D067C32E3C801 (void);
// 0x000005A9 System.Void MoreMountains.CorgiEngine.ParallaxElement::FixedUpdate()
extern void ParallaxElement_FixedUpdate_m08A75C5A675C722F806646755C054667C194DA03 (void);
// 0x000005AA System.Void MoreMountains.CorgiEngine.ParallaxElement::Update()
extern void ParallaxElement_Update_m1AD1AD3780C2FA329541D7C1D2ABE0FB9C5DA221 (void);
// 0x000005AB System.Void MoreMountains.CorgiEngine.ParallaxElement::LateUpdate()
extern void ParallaxElement_LateUpdate_m056BC7F3DD5F205B51BB89ED0EF5315F68699063 (void);
// 0x000005AC System.Void MoreMountains.CorgiEngine.ParallaxElement::ProcessParallax()
extern void ParallaxElement_ProcessParallax_mB69AD2E004C08C291D73F3F969A2BA91B3EB9E90 (void);
// 0x000005AD System.Void MoreMountains.CorgiEngine.ParallaxElement::.ctor()
extern void ParallaxElement__ctor_m2936E32033787F095422059DC9B228928A4FCDA2 (void);
// 0x000005AE System.Void MoreMountains.CorgiEngine.AppearDisappear::Start()
extern void AppearDisappear_Start_m20C73B9D0616E5E5534E6DA544D14B1409441DB8 (void);
// 0x000005AF System.Void MoreMountains.CorgiEngine.AppearDisappear::Initialization()
extern void AppearDisappear_Initialization_m26D66835BBFE7EF3DAB96ED6C67DD27E46CCABE8 (void);
// 0x000005B0 System.Void MoreMountains.CorgiEngine.AppearDisappear::Activate(System.Boolean)
extern void AppearDisappear_Activate_m58DCE38D11881026B56357AD5087265655959F58 (void);
// 0x000005B1 System.Void MoreMountains.CorgiEngine.AppearDisappear::Update()
extern void AppearDisappear_Update_m915438D190727FFF51A6CDFC1E3D23EFA03B4FCA (void);
// 0x000005B2 System.Void MoreMountains.CorgiEngine.AppearDisappear::ProcessTriggerArea()
extern void AppearDisappear_ProcessTriggerArea_mF48DC3F205F5B32D45863E89CE534DF4539FE9A3 (void);
// 0x000005B3 System.Void MoreMountains.CorgiEngine.AppearDisappear::ProcessStateMachine()
extern void AppearDisappear_ProcessStateMachine_m3D541AB5C55836E7FDCF931A1F761AB311DD175C (void);
// 0x000005B4 System.Void MoreMountains.CorgiEngine.AppearDisappear::DetermineNextState()
extern void AppearDisappear_DetermineNextState_m26D11497EB35F323D758EBECF9FF95B139584ED9 (void);
// 0x000005B5 System.Void MoreMountains.CorgiEngine.AppearDisappear::ChangeState()
extern void AppearDisappear_ChangeState_m3CFF851DA9A095D2E08B048F2E1CE9E7B472D339 (void);
// 0x000005B6 System.Void MoreMountains.CorgiEngine.AppearDisappear::UpdateBoundComponents(System.Boolean)
extern void AppearDisappear_UpdateBoundComponents_m8906E2E0528C86D7EBD03D560BA367165A4984BD (void);
// 0x000005B7 System.Void MoreMountains.CorgiEngine.AppearDisappear::RandomizeDurations()
extern void AppearDisappear_RandomizeDurations_mE9A08FC1C18C5B9D623FDDE9D95A737D0418B938 (void);
// 0x000005B8 System.Void MoreMountains.CorgiEngine.AppearDisappear::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void AppearDisappear_OnTriggerEnter2D_m252A0C1909AF045A26219E83146A999446FCD11A (void);
// 0x000005B9 System.Void MoreMountains.CorgiEngine.AppearDisappear::.ctor()
extern void AppearDisappear__ctor_m3FF4367A2FF9AD4EE3291EDDD254BC8ED49EC0C9 (void);
// 0x000005BA System.Void MoreMountains.CorgiEngine.AutoMovementControlZone::Awake()
extern void AutoMovementControlZone_Awake_mFE640991E1F48BCBE4B5F35282CB587255B95D9B (void);
// 0x000005BB System.Void MoreMountains.CorgiEngine.AutoMovementControlZone::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void AutoMovementControlZone_OnTriggerEnter2D_m685B28F89B99E38515E25070B6738D7F54BE0862 (void);
// 0x000005BC System.Void MoreMountains.CorgiEngine.AutoMovementControlZone::HandleCollision(UnityEngine.Collider2D)
extern void AutoMovementControlZone_HandleCollision_m58860BC5324A4EF801881C2A75F3170D0F76867F (void);
// 0x000005BD System.Void MoreMountains.CorgiEngine.AutoMovementControlZone::.ctor()
extern void AutoMovementControlZone__ctor_mCED1F0D3CC8CD4FCF154F401FD2285E80445BD69 (void);
// 0x000005BE System.Void MoreMountains.CorgiEngine.BonusBlock::Start()
extern void BonusBlock_Start_m0287EAD14E2D20951C38249ADD75454A2BD01E30 (void);
// 0x000005BF System.Void MoreMountains.CorgiEngine.BonusBlock::Initialization()
extern void BonusBlock_Initialization_m274A4609575164B9D6155C95B1171D0E55BD64BC (void);
// 0x000005C0 System.Void MoreMountains.CorgiEngine.BonusBlock::Update()
extern void BonusBlock_Update_mF637C4E3E3401F48C05DD6C92978EC476D9128DE (void);
// 0x000005C1 System.Void MoreMountains.CorgiEngine.BonusBlock::UpdateAnimator()
extern void BonusBlock_UpdateAnimator_m496DDEE326E122057BE2DBFF978B5F64DDF0D1C5 (void);
// 0x000005C2 System.Void MoreMountains.CorgiEngine.BonusBlock::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void BonusBlock_OnTriggerEnter2D_m014FD55E3B86EBC9F74849116FBE4074CD7EB4C1 (void);
// 0x000005C3 System.Void MoreMountains.CorgiEngine.BonusBlock::OnTriggerExit2D(UnityEngine.Collider2D)
extern void BonusBlock_OnTriggerExit2D_mE2217511DA4D73CD92B022C0592CE58065175ACA (void);
// 0x000005C4 System.Void MoreMountains.CorgiEngine.BonusBlock::OnPlayerRespawn(MoreMountains.CorgiEngine.CheckPoint,MoreMountains.CorgiEngine.Character)
extern void BonusBlock_OnPlayerRespawn_m2A93CD20968110E75ADDBA4FF75D193AA5E03887 (void);
// 0x000005C5 System.Void MoreMountains.CorgiEngine.BonusBlock::.ctor()
extern void BonusBlock__ctor_m1035A39BBC8F78270CEDAB13B18D4F47C0DACC14 (void);
// 0x000005C6 System.Void MoreMountains.CorgiEngine.ButtonActivator::.ctor()
extern void ButtonActivator__ctor_mEB200DE9ACFF6CBC26705751E2DFC2C5165FE33F (void);
// 0x000005C7 System.Void MoreMountains.CorgiEngine.CharacterDetector::Start()
extern void CharacterDetector_Start_m94256B2BD8DF46BC0F63C25D3068470A169FD9C7 (void);
// 0x000005C8 System.Void MoreMountains.CorgiEngine.CharacterDetector::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterDetector_OnTriggerEnter2D_m60C243CF6B4C73B4075B2BE40E609483BD4FB4C1 (void);
// 0x000005C9 System.Void MoreMountains.CorgiEngine.CharacterDetector::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CharacterDetector_OnTriggerStay2D_m346921080A376A3C541EA9D6B117898E834E796A (void);
// 0x000005CA System.Void MoreMountains.CorgiEngine.CharacterDetector::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CharacterDetector_OnTriggerExit2D_m9788B87CC660CD37FF608FECFA6375C53EC36780 (void);
// 0x000005CB System.Boolean MoreMountains.CorgiEngine.CharacterDetector::TargetFound(UnityEngine.Collider2D)
extern void CharacterDetector_TargetFound_mFF5D764A7353F34777338A811B937086972DB7A6 (void);
// 0x000005CC System.Void MoreMountains.CorgiEngine.CharacterDetector::.ctor()
extern void CharacterDetector__ctor_m4CB748F91F98B2372E9EEE4EF83953CB76756DC8 (void);
// 0x000005CD System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovementOverride::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterHorizontalMovementOverride_OnTriggerEnter2D_mBE647F65C5B841AA77E7729F8D7E508B560F61D9 (void);
// 0x000005CE System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovementOverride::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CharacterHorizontalMovementOverride_OnTriggerExit2D_m064CB7C154F5C3C1210B1C7E9DD844C39EC3D331 (void);
// 0x000005CF System.Void MoreMountains.CorgiEngine.CharacterHorizontalMovementOverride::.ctor()
extern void CharacterHorizontalMovementOverride__ctor_mBA5034C599F4107E60317C257F90D24FC90CB438 (void);
// 0x000005D0 System.Void MoreMountains.CorgiEngine.CharacterJumpOverride::Update()
extern void CharacterJumpOverride_Update_m295BDFCD985F76442DD70282998C7C357A728863 (void);
// 0x000005D1 System.Void MoreMountains.CorgiEngine.CharacterJumpOverride::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterJumpOverride_OnTriggerEnter2D_m26A7BF991DBA4A82429D9F901EF2164A24361BD5 (void);
// 0x000005D2 System.Void MoreMountains.CorgiEngine.CharacterJumpOverride::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CharacterJumpOverride_OnTriggerExit2D_m3DEA016E6339FAC36ECEC8E1422B56461B9DB987 (void);
// 0x000005D3 System.Void MoreMountains.CorgiEngine.CharacterJumpOverride::Restore()
extern void CharacterJumpOverride_Restore_mF97D47411CFCEB8FBBFA0C3276F57BA2A111038D (void);
// 0x000005D4 System.Void MoreMountains.CorgiEngine.CharacterJumpOverride::.ctor()
extern void CharacterJumpOverride__ctor_mA6D2EFF53C9E0101F0BD8BD26147DDB3CE63C795 (void);
// 0x000005D5 System.Void MoreMountains.CorgiEngine.CorgiControllerPhysicsVolume2D::.ctor()
extern void CorgiControllerPhysicsVolume2D__ctor_mD2D76C479B0997E2B702FCB5DE0AC5367C21B225 (void);
// 0x000005D6 System.Void MoreMountains.CorgiEngine.Water::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Water_OnTriggerEnter2D_mF717EB620EA0C66B7E31CB53F3DEA13A405AA247 (void);
// 0x000005D7 System.Void MoreMountains.CorgiEngine.Water::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Water_OnTriggerExit2D_mD9755D399B066E8345CA17DBBB79EC7A5622CC3A (void);
// 0x000005D8 System.Void MoreMountains.CorgiEngine.Water::.ctor()
extern void Water__ctor_mB37D4AEE740896A0B9E8135B36BCEBE6836F1156 (void);
// 0x000005D9 System.Void MoreMountains.CorgiEngine.FallingPlatform::Start()
extern void FallingPlatform_Start_m6DEEE759250C482E2D293371830CAF9103F3F8EF (void);
// 0x000005DA System.Void MoreMountains.CorgiEngine.FallingPlatform::Initialization()
extern void FallingPlatform_Initialization_mB73A6B2DA508C2BBBF66F4B0458E4C7703A6EF60 (void);
// 0x000005DB System.Void MoreMountains.CorgiEngine.FallingPlatform::FixedUpdate()
extern void FallingPlatform_FixedUpdate_m7BB093DF0F81215D39108C427C422CD6B9D408A4 (void);
// 0x000005DC System.Void MoreMountains.CorgiEngine.FallingPlatform::DisableFallingPlatform()
extern void FallingPlatform_DisableFallingPlatform_m6631F1F513860CCEB63E24AAED7574A81EB337C0 (void);
// 0x000005DD System.Void MoreMountains.CorgiEngine.FallingPlatform::UpdateAnimator()
extern void FallingPlatform_UpdateAnimator_m6DCC847675D2B7E640B4F7E97CC6FBE1D53EEF1A (void);
// 0x000005DE System.Void MoreMountains.CorgiEngine.FallingPlatform::OnTriggerStay2D(UnityEngine.Collider2D)
extern void FallingPlatform_OnTriggerStay2D_mDD1C237300C5AFFB057ECC42E2C7A2A086C2B7A4 (void);
// 0x000005DF System.Void MoreMountains.CorgiEngine.FallingPlatform::OnTriggerExit2D(UnityEngine.Collider2D)
extern void FallingPlatform_OnTriggerExit2D_m3E3ACD34C0DCCEFB7BB2629D40A9ADC42B102983 (void);
// 0x000005E0 System.Void MoreMountains.CorgiEngine.FallingPlatform::OnRevive()
extern void FallingPlatform_OnRevive_mD16A179AE6A55F1B609D6747CC93D485A6661509 (void);
// 0x000005E1 System.Void MoreMountains.CorgiEngine.FallingPlatform::OnEnable()
extern void FallingPlatform_OnEnable_m1B8CB388E05AAD7FE7D34D5870A65F075810171F (void);
// 0x000005E2 System.Void MoreMountains.CorgiEngine.FallingPlatform::OnDisable()
extern void FallingPlatform_OnDisable_m936356E3F2EB063C5F4E99F69806189D9B2D0C31 (void);
// 0x000005E3 System.Void MoreMountains.CorgiEngine.FallingPlatform::.ctor()
extern void FallingPlatform__ctor_m335BFC4FF51471CB685F4CE9409379DB7375900C (void);
// 0x000005E4 System.Void MoreMountains.CorgiEngine.ForceZone::Awake()
extern void ForceZone_Awake_mC6AF6224210661470264FB02E4845ED7647BA55E (void);
// 0x000005E5 System.Void MoreMountains.CorgiEngine.ForceZone::Initialization()
extern void ForceZone_Initialization_mA541AC209431C88B5715F766E612940C1ECC9B25 (void);
// 0x000005E6 System.Void MoreMountains.CorgiEngine.ForceZone::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ForceZone_OnTriggerEnter2D_m1FEE0719A9F90C0594B2EE56680240FB5399061B (void);
// 0x000005E7 System.Void MoreMountains.CorgiEngine.ForceZone::ApplyForce(UnityEngine.Collider2D)
extern void ForceZone_ApplyForce_m3126E1FA05299E577346AA9033302D0CC5F1ADB1 (void);
// 0x000005E8 System.Void MoreMountains.CorgiEngine.ForceZone::.ctor()
extern void ForceZone__ctor_mEE424719AF868043A478DFB45574AE155CADC048 (void);
// 0x000005E9 System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject::Awake()
extern void GrabCarryAndThrowObject_Awake_m08CA43320643346F381BAD2330F9512D7E2568BB (void);
// 0x000005EA System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject::Initialization()
extern void GrabCarryAndThrowObject_Initialization_m966843A59FED23CAE4A0B346AF7F3EFC9C45BF41 (void);
// 0x000005EB System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject::Grab(UnityEngine.Transform)
extern void GrabCarryAndThrowObject_Grab_m68BE7D843A5CC3BF9CB30E891D2F9379D2339B09 (void);
// 0x000005EC System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject::Throw(System.Int32,System.Single)
extern void GrabCarryAndThrowObject_Throw_m0812E2E4A64BA3B416FB3AAF99037EF0522543FF (void);
// 0x000005ED System.Collections.IEnumerator MoreMountains.CorgiEngine.GrabCarryAndThrowObject::ResetCollisions()
extern void GrabCarryAndThrowObject_ResetCollisions_mE7DAAD44C14146EFBEB4CC1DE4868520A8D6D984 (void);
// 0x000005EE System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject::.ctor()
extern void GrabCarryAndThrowObject__ctor_m29A92881EAE8C813BC94B99EF0212DB858C459CD (void);
// 0x000005EF System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject/<ResetCollisions>d__21::.ctor(System.Int32)
extern void U3CResetCollisionsU3Ed__21__ctor_m0966EAD5E98CB004C71798D2E5A021D75EF8F6BB (void);
// 0x000005F0 System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject/<ResetCollisions>d__21::System.IDisposable.Dispose()
extern void U3CResetCollisionsU3Ed__21_System_IDisposable_Dispose_m418B21E668A9202D3A33E4F6A3FB22C4E6F08AEA (void);
// 0x000005F1 System.Boolean MoreMountains.CorgiEngine.GrabCarryAndThrowObject/<ResetCollisions>d__21::MoveNext()
extern void U3CResetCollisionsU3Ed__21_MoveNext_m08048E67DA1A4966BD7FB25EB5C910C5E574F6AE (void);
// 0x000005F2 System.Object MoreMountains.CorgiEngine.GrabCarryAndThrowObject/<ResetCollisions>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResetCollisionsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF75379575970AE82ABA19E884CEA973CDD3FF747 (void);
// 0x000005F3 System.Void MoreMountains.CorgiEngine.GrabCarryAndThrowObject/<ResetCollisions>d__21::System.Collections.IEnumerator.Reset()
extern void U3CResetCollisionsU3Ed__21_System_Collections_IEnumerator_Reset_m78A6063A24ADFD6AEE30977CA1F91ADE012608B1 (void);
// 0x000005F4 System.Object MoreMountains.CorgiEngine.GrabCarryAndThrowObject/<ResetCollisions>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CResetCollisionsU3Ed__21_System_Collections_IEnumerator_get_Current_mFB5D5AB67F4C4508A552B3F574C2935E3F7ACE10 (void);
// 0x000005F5 System.Void MoreMountains.CorgiEngine.GravityPoint::OnDrawGizmos()
extern void GravityPoint_OnDrawGizmos_m9FC4909128D9CBC997F10FC4F9C971F5686EC072 (void);
// 0x000005F6 System.Void MoreMountains.CorgiEngine.GravityPoint::.ctor()
extern void GravityPoint__ctor_m0E9B6E0B4CF09CDB727D5CAF50D01865A344CD24 (void);
// 0x000005F7 UnityEngine.Vector2 MoreMountains.CorgiEngine.GravityZone::get_GravityDirectionVector()
extern void GravityZone_get_GravityDirectionVector_m60F53523214E3EA7E7D4E4DF7413E237BD482628 (void);
// 0x000005F8 System.Void MoreMountains.CorgiEngine.GravityZone::OnDrawGizmosSelected()
extern void GravityZone_OnDrawGizmosSelected_mC98A35228F11B117489F46BCEEA837D0DE4F0D5D (void);
// 0x000005F9 System.Void MoreMountains.CorgiEngine.GravityZone::.ctor()
extern void GravityZone__ctor_mDE4F4AA2D6AB6FE27EF1727C9BA0772EB06BD6A5 (void);
// 0x000005FA System.Void MoreMountains.CorgiEngine.Grip::Start()
extern void Grip_Start_m3E0CD9A8CE162F517FA965D32C0CE0360B41E392 (void);
// 0x000005FB System.Void MoreMountains.CorgiEngine.Grip::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Grip_OnTriggerEnter2D_m635A4C72BF1568B9AFE6D997E5F0A5EB776CB572 (void);
// 0x000005FC System.Void MoreMountains.CorgiEngine.Grip::.ctor()
extern void Grip__ctor_m0E04ADAEF551F7F2DDD0A530F105B7591E0C933A (void);
// 0x000005FD System.Void MoreMountains.CorgiEngine.Hittable::Start()
extern void Hittable_Start_m18F25BABF4E86959F7628EEFE0909DA7B91144EF (void);
// 0x000005FE System.Void MoreMountains.CorgiEngine.Hittable::OnHit()
extern void Hittable_OnHit_mF8175AA786A7A547B5A00A9C14FA32C6FA6D41F6 (void);
// 0x000005FF System.Void MoreMountains.CorgiEngine.Hittable::OnHitZero()
extern void Hittable_OnHitZero_m47C94E7351F3E9231C6BDA912ACA9C83060DF37C (void);
// 0x00000600 System.Void MoreMountains.CorgiEngine.Hittable::OnEnable()
extern void Hittable_OnEnable_m52C2DA190630BD88DE6E2B2C70DB7B70D1B63F7F (void);
// 0x00000601 System.Void MoreMountains.CorgiEngine.Hittable::OnDisable()
extern void Hittable_OnDisable_m7F3A003555A2C18B0175B3BB605EFB3F41EE58D4 (void);
// 0x00000602 System.Void MoreMountains.CorgiEngine.Hittable::.ctor()
extern void Hittable__ctor_m0504A3B0E886B87E0635C2A596023B10D0CEAAEA (void);
// 0x00000603 System.Void MoreMountains.CorgiEngine.Jumper::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Jumper_OnTriggerEnter2D_m725BDA6ACF4079ABD231041DF2E03E2D307FE770 (void);
// 0x00000604 System.Void MoreMountains.CorgiEngine.Jumper::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Jumper_OnTriggerExit2D_m1BE6F5E87D949EB9D2BB5FE3FE6FC70287C1B8E0 (void);
// 0x00000605 System.Void MoreMountains.CorgiEngine.Jumper::LateUpdate()
extern void Jumper_LateUpdate_m1AB499C13814D9677DF0A33A7D4E7225B8366357 (void);
// 0x00000606 System.Void MoreMountains.CorgiEngine.Jumper::.ctor()
extern void Jumper__ctor_mB25885882B09980788D73E7065151FE06C1FF4DC (void);
// 0x00000607 System.Void MoreMountains.CorgiEngine.KeyOperatedZone::Start()
extern void KeyOperatedZone_Start_mF1D0EDB7C1BC4C61BA15BE5B2B6F9C97BBB82709 (void);
// 0x00000608 System.Void MoreMountains.CorgiEngine.KeyOperatedZone::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void KeyOperatedZone_OnTriggerEnter2D_m7ABAC41AF526ABA3AB044E4886A2FD42705D2B94 (void);
// 0x00000609 System.Void MoreMountains.CorgiEngine.KeyOperatedZone::TriggerButtonAction(UnityEngine.GameObject)
extern void KeyOperatedZone_TriggerButtonAction_mC7941EC55A7F5AD3E4111DCE5BA2AD68963ADA01 (void);
// 0x0000060A System.Void MoreMountains.CorgiEngine.KeyOperatedZone::TriggerKeyAction()
extern void KeyOperatedZone_TriggerKeyAction_mCD9A071B989A5A1CB715FAA3BC943635AC242F0D (void);
// 0x0000060B System.Void MoreMountains.CorgiEngine.KeyOperatedZone::.ctor()
extern void KeyOperatedZone__ctor_mFD87329B0E5E6BE9193177ADD06B7646DB1E726F (void);
// 0x0000060C UnityEngine.BoxCollider2D MoreMountains.CorgiEngine.Ladder::get_LadderPlatformBoxCollider2D()
extern void Ladder_get_LadderPlatformBoxCollider2D_mC18808CFC8E377DEDCCD3A08C5C04AB6ADB91C2F (void);
// 0x0000060D System.Void MoreMountains.CorgiEngine.Ladder::set_LadderPlatformBoxCollider2D(UnityEngine.BoxCollider2D)
extern void Ladder_set_LadderPlatformBoxCollider2D_mBE1E253275CE3A7C7977630CA47B9DBC65A45AD8 (void);
// 0x0000060E UnityEngine.EdgeCollider2D MoreMountains.CorgiEngine.Ladder::get_LadderPlatformEdgeCollider2D()
extern void Ladder_get_LadderPlatformEdgeCollider2D_mDF263178A858AB23D00FE0B3F2E64B46361A5DB8 (void);
// 0x0000060F System.Void MoreMountains.CorgiEngine.Ladder::set_LadderPlatformEdgeCollider2D(UnityEngine.EdgeCollider2D)
extern void Ladder_set_LadderPlatformEdgeCollider2D_mA49F09499AAA70EF6EBC57F420D0B4DB870CD980 (void);
// 0x00000610 System.Void MoreMountains.CorgiEngine.Ladder::Start()
extern void Ladder_Start_mAC1F08CE950EBDD694FC5C6E20AECAEC77C91BB1 (void);
// 0x00000611 System.Void MoreMountains.CorgiEngine.Ladder::Initialization()
extern void Ladder_Initialization_mF176E1D20545A61F3B8CBFA3D7C3618D0F3810C1 (void);
// 0x00000612 System.Void MoreMountains.CorgiEngine.Ladder::RepositionLadderPlatform()
extern void Ladder_RepositionLadderPlatform_mE481456A0154C04F40D0CDBA83B1AC045C595B8F (void);
// 0x00000613 System.Void MoreMountains.CorgiEngine.Ladder::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Ladder_OnTriggerEnter2D_mC532D3DC087F0D0715D3D9AD485F03F44C8AA18C (void);
// 0x00000614 System.Void MoreMountains.CorgiEngine.Ladder::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Ladder_OnTriggerExit2D_m7C320C8A0CDD24053C5986235ABE1E304F597367 (void);
// 0x00000615 System.Void MoreMountains.CorgiEngine.Ladder::.ctor()
extern void Ladder__ctor_m638DE40B9FEA4F497FB832D7D85C4E2FF6F6ADC1 (void);
// 0x00000616 System.Void MoreMountains.CorgiEngine.LedgeEvent::.ctor(UnityEngine.Collider2D,MoreMountains.CorgiEngine.Ledge)
extern void LedgeEvent__ctor_mF87F02B223BF24015920133ECA5E206185B3B372 (void);
// 0x00000617 System.Void MoreMountains.CorgiEngine.LedgeEvent::Trigger(UnityEngine.Collider2D,MoreMountains.CorgiEngine.Ledge)
extern void LedgeEvent_Trigger_m326B964B96C57946A63725380335BD5082C37CA7 (void);
// 0x00000618 System.Void MoreMountains.CorgiEngine.Ledge::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Ledge_OnTriggerEnter2D_m8A197DBA37F88CA4028C64076CAA95A568617B90 (void);
// 0x00000619 System.Void MoreMountains.CorgiEngine.Ledge::OnDrawGizmosSelected()
extern void Ledge_OnDrawGizmosSelected_mC6A798CCB9E50E29933700DD9583703E30A03B41 (void);
// 0x0000061A System.Void MoreMountains.CorgiEngine.Ledge::.ctor()
extern void Ledge__ctor_mF4C4BB1FAE4858F18620119E509539A0D2C53743 (void);
// 0x0000061B System.Void MoreMountains.CorgiEngine.Magnetic::Awake()
extern void Magnetic_Awake_m6480A9FBFCD7ED0866F03FEC9E204AAFA4877B08 (void);
// 0x0000061C System.Void MoreMountains.CorgiEngine.Magnetic::Initialization()
extern void Magnetic_Initialization_mBE5E17FCDE35D41FDED86C7F849546A793B5DE5D (void);
// 0x0000061D System.Void MoreMountains.CorgiEngine.Magnetic::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Magnetic_OnTriggerEnter2D_m0C68AF4E58827F9639B57AF5970C4BF510BCF4A4 (void);
// 0x0000061E System.Void MoreMountains.CorgiEngine.Magnetic::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Magnetic_OnTriggerExit2D_mB593B52EDCE03360958F1AD44FBE465034AB198F (void);
// 0x0000061F System.Void MoreMountains.CorgiEngine.Magnetic::Update()
extern void Magnetic_Update_m523C7C248A6AF444AFA3975CC9E57FF9064B847E (void);
// 0x00000620 System.Void MoreMountains.CorgiEngine.Magnetic::FixedUpdate()
extern void Magnetic_FixedUpdate_mD9134CE71849BAD8977D21E05925FB1FC839ECEE (void);
// 0x00000621 System.Void MoreMountains.CorgiEngine.Magnetic::LateUpdate()
extern void Magnetic_LateUpdate_m082143E4C40D86A2492F9837F39A746C2D8BA385 (void);
// 0x00000622 System.Void MoreMountains.CorgiEngine.Magnetic::FollowTargetPosition()
extern void Magnetic_FollowTargetPosition_mC9DB0E5387220A1FC27DAEF215779D8E6ADAACCA (void);
// 0x00000623 System.Void MoreMountains.CorgiEngine.Magnetic::StopFollowing()
extern void Magnetic_StopFollowing_mFFC7E1E8C24554872D8E9F1AC6AF5B2047824738 (void);
// 0x00000624 System.Void MoreMountains.CorgiEngine.Magnetic::StartFollowing()
extern void Magnetic_StartFollowing_m8652ED26E5D4FA3674A95951EB5E384739AA7A8A (void);
// 0x00000625 System.Void MoreMountains.CorgiEngine.Magnetic::SetTarget(UnityEngine.Transform)
extern void Magnetic_SetTarget_m8F1F9552DCD4E4F837D7F8F45ECED97106116AA0 (void);
// 0x00000626 System.Void MoreMountains.CorgiEngine.Magnetic::.ctor()
extern void Magnetic__ctor_m5AEB42A01A9AF39F4ED940E0D8A2DD64A7C482F2 (void);
// 0x00000627 System.Void MoreMountains.CorgiEngine.MagneticEnabler::Awake()
extern void MagneticEnabler_Awake_mF78C061AE43A3998B973136F841063E9C334E8DE (void);
// 0x00000628 System.Void MoreMountains.CorgiEngine.MagneticEnabler::Initialization()
extern void MagneticEnabler_Initialization_m37A0ABBFDE3085BFDDC356A88551B89E7FB8A960 (void);
// 0x00000629 System.Void MoreMountains.CorgiEngine.MagneticEnabler::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void MagneticEnabler_OnTriggerEnter2D_m935FF6106FC7BCF5186187DDD36E02F69315254B (void);
// 0x0000062A System.Void MoreMountains.CorgiEngine.MagneticEnabler::.ctor()
extern void MagneticEnabler__ctor_mE9A593577517B25F15DCB3845502255D8EB7EBAB (void);
// 0x0000062B System.Void MoreMountains.CorgiEngine.MovingPlatform::Initialization()
extern void MovingPlatform_Initialization_m152028608FEAD7228447096F78AD63964EE55565 (void);
// 0x0000062C System.Boolean MoreMountains.CorgiEngine.MovingPlatform::get_CanMove()
extern void MovingPlatform_get_CanMove_m83BFC98212B598CF82EDF8F231321F74C70724D9 (void);
// 0x0000062D System.Void MoreMountains.CorgiEngine.MovingPlatform::SetMovementAuthorization(System.Boolean)
extern void MovingPlatform_SetMovementAuthorization_m4E035418B5DD22E6A58807796BDB8B96EFA5EA71 (void);
// 0x0000062E System.Void MoreMountains.CorgiEngine.MovingPlatform::AuthorizeMovement()
extern void MovingPlatform_AuthorizeMovement_m502C7C3A8BACDE74642D00BC1D7DE1DC70C935B7 (void);
// 0x0000062F System.Void MoreMountains.CorgiEngine.MovingPlatform::ForbidMovement()
extern void MovingPlatform_ForbidMovement_mC70D97DA1963800DBC678D772BB4E2F82420F841 (void);
// 0x00000630 System.Void MoreMountains.CorgiEngine.MovingPlatform::ToggleMovementAuthorization()
extern void MovingPlatform_ToggleMovementAuthorization_m8AB45241446AB8FF1E071FBCAB3985BAE98316B0 (void);
// 0x00000631 System.Void MoreMountains.CorgiEngine.MovingPlatform::ResetEndReached()
extern void MovingPlatform_ResetEndReached_m231B86848281C28491BABC1F4A042B480082BDEA (void);
// 0x00000632 System.Void MoreMountains.CorgiEngine.MovingPlatform::MoveTowardsStart()
extern void MovingPlatform_MoveTowardsStart_m18B71AE6BCA057D2EABD33F220802BCEF1EED074 (void);
// 0x00000633 System.Void MoreMountains.CorgiEngine.MovingPlatform::MoveTowardsEnd()
extern void MovingPlatform_MoveTowardsEnd_mAD33CE6FEE3BEB1B6D09971F65FC69C6ED7E7DBA (void);
// 0x00000634 System.Void MoreMountains.CorgiEngine.MovingPlatform::PointReached()
extern void MovingPlatform_PointReached_m3EAB41899B7513788E00541A88F9273C55C6E1C5 (void);
// 0x00000635 System.Void MoreMountains.CorgiEngine.MovingPlatform::EndReached()
extern void MovingPlatform_EndReached_mF3D2B47122FB38478813DB94564C8B4A09D43BF5 (void);
// 0x00000636 System.Void MoreMountains.CorgiEngine.MovingPlatform::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void MovingPlatform_OnTriggerEnter2D_m6C34439B1AFCE5284ABA58E54E0490CD846C819F (void);
// 0x00000637 System.Void MoreMountains.CorgiEngine.MovingPlatform::OnTriggerExit2D(UnityEngine.Collider2D)
extern void MovingPlatform_OnTriggerExit2D_m732BED39AD2E311A251F3A91425D3A19D7CCEDED (void);
// 0x00000638 System.Void MoreMountains.CorgiEngine.MovingPlatform::OnPlayerRespawn(MoreMountains.CorgiEngine.CheckPoint,MoreMountains.CorgiEngine.Character)
extern void MovingPlatform_OnPlayerRespawn_m70B84174755001A9A45DA9A42A4D057B6FBD7BF3 (void);
// 0x00000639 System.Void MoreMountains.CorgiEngine.MovingPlatform::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void MovingPlatform_OnMMEvent_m65DC8B3092B64DEFD13E59F91F76B5D362816444 (void);
// 0x0000063A System.Void MoreMountains.CorgiEngine.MovingPlatform::OnEnable()
extern void MovingPlatform_OnEnable_m96472322B4FC1916DA99FB1D554EB0546C96D7E2 (void);
// 0x0000063B System.Void MoreMountains.CorgiEngine.MovingPlatform::OnDisable()
extern void MovingPlatform_OnDisable_m1AAD9A2F609B247B3079EDE68D3E47A872D58FF2 (void);
// 0x0000063C System.Void MoreMountains.CorgiEngine.MovingPlatform::.ctor()
extern void MovingPlatform__ctor_m8A937339F9E3823EE63955A77F0BC3086EB5F0F9 (void);
// 0x0000063D System.Void MoreMountains.CorgiEngine.MovingPlatformFree::Update()
extern void MovingPlatformFree_Update_m4AF4D3487A845D366109EE53E5EF24E17D9475B0 (void);
// 0x0000063E System.Void MoreMountains.CorgiEngine.MovingPlatformFree::LateUpdate()
extern void MovingPlatformFree_LateUpdate_m2699A6A1CCFB7B475DA36B326498BB408F3845FF (void);
// 0x0000063F System.Void MoreMountains.CorgiEngine.MovingPlatformFree::ComputeSpeed()
extern void MovingPlatformFree_ComputeSpeed_m1AAA921DE95498FBE89752F6D323DC7B5D0FD055 (void);
// 0x00000640 System.Void MoreMountains.CorgiEngine.MovingPlatformFree::.ctor()
extern void MovingPlatformFree__ctor_m937A35E57527A371B137DC459E233A43AE0D8EC5 (void);
// 0x00000641 System.Void MoreMountains.CorgiEngine.ProximityManaged::DebugAddObject()
extern void ProximityManaged_DebugAddObject_m1C55DED79BB27DD4500C3DD1F67103E4A197937A (void);
// 0x00000642 System.Void MoreMountains.CorgiEngine.ProximityManaged::.ctor()
extern void ProximityManaged__ctor_mF4331229D988220DB64E3DE39823137A189D96E7 (void);
// 0x00000643 System.Void MoreMountains.CorgiEngine.Pushable::Awake()
extern void Pushable_Awake_m35A1B6141DC8D49708C74872983131FAE910381B (void);
// 0x00000644 System.Void MoreMountains.CorgiEngine.Pushable::Attach(MoreMountains.CorgiEngine.CorgiController)
extern void Pushable_Attach_m963DBD48B7784A916D1773A3D617CFF414C765B4 (void);
// 0x00000645 System.Void MoreMountains.CorgiEngine.Pushable::Detach(MoreMountains.CorgiEngine.CorgiController)
extern void Pushable_Detach_m3477345DBFC14AF62CC17802C2D801D15F881A6E (void);
// 0x00000646 System.Void MoreMountains.CorgiEngine.Pushable::Update()
extern void Pushable_Update_m12CAE94E31E1365746DB495679F70312941F744D (void);
// 0x00000647 System.Void MoreMountains.CorgiEngine.Pushable::CheckIfGrounded()
extern void Pushable_CheckIfGrounded_m7D55F0A88321087A27ED5F0E39D100E4B87D1235 (void);
// 0x00000648 System.Void MoreMountains.CorgiEngine.Pushable::.ctor()
extern void Pushable__ctor_m42CDFD7189EBC511C8D15E56CBB7AA6C90375DE1 (void);
// 0x00000649 System.Void MoreMountains.CorgiEngine.RandomSprite::Start()
extern void RandomSprite_Start_mA042656AA0CC118C86EC10E8C32D9088EF01411C (void);
// 0x0000064A System.Void MoreMountains.CorgiEngine.RandomSprite::Randomize()
extern void RandomSprite_Randomize_m1360EF7D2C3969A3C3A918911542ED303C541884 (void);
// 0x0000064B System.Void MoreMountains.CorgiEngine.RandomSprite::.ctor()
extern void RandomSprite__ctor_mEB66A50DBD80FCA1AECC28347370FF225EA68F90 (void);
// 0x0000064C System.Void MoreMountains.CorgiEngine.SurfaceModifierTarget::.ctor()
extern void SurfaceModifierTarget__ctor_mAF82A25E673540E37EC69DBE40255FD895D053FA (void);
// 0x0000064D System.Collections.Generic.List`1<MoreMountains.CorgiEngine.SurfaceModifierTarget> MoreMountains.CorgiEngine.SurfaceModifier::get__targets()
extern void SurfaceModifier_get__targets_m3CBE8FC16453216D642DF383592DA8BE3FD00726 (void);
// 0x0000064E System.Void MoreMountains.CorgiEngine.SurfaceModifier::set__targets(System.Collections.Generic.List`1<MoreMountains.CorgiEngine.SurfaceModifierTarget>)
extern void SurfaceModifier_set__targets_m46B97154C7F209986C26B4E194D470353A1E6EA7 (void);
// 0x0000064F System.Void MoreMountains.CorgiEngine.SurfaceModifier::Awake()
extern void SurfaceModifier_Awake_m437D7C9939A0C4F9ADAB70D5A27BB4E1AE01B042 (void);
// 0x00000650 System.Void MoreMountains.CorgiEngine.SurfaceModifier::OnTriggerStay2D(UnityEngine.Collider2D)
extern void SurfaceModifier_OnTriggerStay2D_m0BE4BB964C6CED7FFC53BFBA1B293FC76903AE08 (void);
// 0x00000651 System.Void MoreMountains.CorgiEngine.SurfaceModifier::OnTriggerExit2D(UnityEngine.Collider2D)
extern void SurfaceModifier_OnTriggerExit2D_mA23E5FDF3E7C14152053F4940C1B16B9D4955F2C (void);
// 0x00000652 System.Void MoreMountains.CorgiEngine.SurfaceModifier::Update()
extern void SurfaceModifier_Update_m52342F2D8F60F56AB3BACA35BF1A7913501C0D5B (void);
// 0x00000653 System.Void MoreMountains.CorgiEngine.SurfaceModifier::ProcessSurface()
extern void SurfaceModifier_ProcessSurface_m3229B3120C8B13FE458062C4802A8EDBAB7C355A (void);
// 0x00000654 System.Boolean MoreMountains.CorgiEngine.SurfaceModifier::ForceApplicationConditionsMet()
extern void SurfaceModifier_ForceApplicationConditionsMet_m33F125E779FD846577863A300D6CDCFAF08EE7F6 (void);
// 0x00000655 System.Void MoreMountains.CorgiEngine.SurfaceModifier::ApplyHorizontalForce(MoreMountains.CorgiEngine.SurfaceModifierTarget)
extern void SurfaceModifier_ApplyHorizontalForce_mF13F87BBE52D6A09EB4B73C7DBFF81A694D08B97 (void);
// 0x00000656 System.Void MoreMountains.CorgiEngine.SurfaceModifier::ApplyVerticalForce(MoreMountains.CorgiEngine.SurfaceModifierTarget)
extern void SurfaceModifier_ApplyVerticalForce_m5262718E0F14254CDD0E225C79CC034E822E0F5E (void);
// 0x00000657 System.Void MoreMountains.CorgiEngine.SurfaceModifier::.ctor()
extern void SurfaceModifier__ctor_m413C545049BED66CF85D5B2F4F5F467603A06F0C (void);
// 0x00000658 System.Void MoreMountains.CorgiEngine.TimeZone::TriggerButtonAction(UnityEngine.GameObject)
extern void TimeZone_TriggerButtonAction_mE548C812C1E431B82F1A570907C6D7B1C42327AC (void);
// 0x00000659 System.Void MoreMountains.CorgiEngine.TimeZone::TriggerExitAction(UnityEngine.GameObject)
extern void TimeZone_TriggerExitAction_m6F1E2700E2A75BBBA63101CEBFF683A9ADE5AEAF (void);
// 0x0000065A System.Void MoreMountains.CorgiEngine.TimeZone::ControlTime()
extern void TimeZone_ControlTime_m268FD9640E69B8C9DED5994C7F7BD3B7B2CCA640 (void);
// 0x0000065B System.Void MoreMountains.CorgiEngine.TimeZone::.ctor()
extern void TimeZone__ctor_m9BB582EE8872D9945C151600314C3345C8E922C4 (void);
// 0x0000065C System.Void MoreMountains.CorgiEngine.WallClingingOverride::.ctor()
extern void WallClingingOverride__ctor_mCDD788202BB54E286C51E3E51B2D3ED7BFCF207C (void);
// 0x0000065D System.Void MoreMountains.CorgiEngine.Zipline::Start()
extern void Zipline_Start_mE21FF5FC145B7DFF9B9E7D7B77F220298C675A12 (void);
// 0x0000065E System.Void MoreMountains.CorgiEngine.Zipline::Update()
extern void Zipline_Update_m05D92E76486C3EB28F6888963480D651B088E78B (void);
// 0x0000065F System.Void MoreMountains.CorgiEngine.Zipline::ResetPosition()
extern void Zipline_ResetPosition_m3C8B6AE7E8B92371A6D30152C95645FE1C366752 (void);
// 0x00000660 System.Void MoreMountains.CorgiEngine.Zipline::.ctor()
extern void Zipline__ctor_mC86E0B682986242062C213B4DA428271E3F0C720 (void);
// 0x00000661 System.Void MoreMountains.CorgiEngine.MMFeedbackCorgiEngineFloatingText::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCorgiEngineFloatingText_CustomPlayFeedback_mA40899DE8CE7CC2C8527B36D3C00F933FA5E449A (void);
// 0x00000662 System.Void MoreMountains.CorgiEngine.MMFeedbackCorgiEngineFloatingText::.ctor()
extern void MMFeedbackCorgiEngineFloatingText__ctor_mB22202038E8689E13E54F6A3C5C1296D74A1FE24 (void);
// 0x00000663 System.Void MoreMountains.CorgiEngine.MMFeedbackCorgiEngineSound::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCorgiEngineSound_CustomPlayFeedback_m29D5F000508C3A7ED09B2DDBE32722A940D31998 (void);
// 0x00000664 System.Void MoreMountains.CorgiEngine.MMFeedbackCorgiEngineSound::CustomStopFeedback(UnityEngine.Vector3,System.Single)
extern void MMFeedbackCorgiEngineSound_CustomStopFeedback_m257BEEE194D205CFFE59BC0065CEA3072225B6EC (void);
// 0x00000665 System.Void MoreMountains.CorgiEngine.MMFeedbackCorgiEngineSound::.ctor()
extern void MMFeedbackCorgiEngineSound__ctor_mC34C7AB07D4BB2B26AD98C8CF85DDA0EA2613161 (void);
// 0x00000666 System.Void MoreMountains.CorgiEngine.AmmoDisplay::Initialization()
extern void AmmoDisplay_Initialization_m83E1B783CE0E99ADF030F1A8F9ECC0DD8A99732E (void);
// 0x00000667 System.Void MoreMountains.CorgiEngine.AmmoDisplay::UpdateTextDisplay(System.String)
extern void AmmoDisplay_UpdateTextDisplay_mD6D4601D96142B86E5CDBC18B72A24C9024700ED (void);
// 0x00000668 System.Void MoreMountains.CorgiEngine.AmmoDisplay::UpdateAmmoDisplays(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void AmmoDisplay_UpdateAmmoDisplays_m4012F292567A9E246702841E60D1EA5A8C3CBEA1 (void);
// 0x00000669 System.Void MoreMountains.CorgiEngine.AmmoDisplay::.ctor()
extern void AmmoDisplay__ctor_mCB5A9F8A536532E153557CF8D6FC32C65E008A68 (void);
// 0x0000066A System.Void MoreMountains.CorgiEngine.ButtonActivated::OnEnable()
extern void ButtonActivated_OnEnable_mA322924D4CC2B6BAC94277975CD9B458FF93FA08 (void);
// 0x0000066B System.Void MoreMountains.CorgiEngine.ButtonActivated::Initialization()
extern void ButtonActivated_Initialization_m85B5B45F4E6E96D89E7F4FE8C4ACA064B82B2A9D (void);
// 0x0000066C System.Void MoreMountains.CorgiEngine.ButtonActivated::MakeActivable()
extern void ButtonActivated_MakeActivable_m2245C3CADE8FAC63B178E699D6BF47167F6C1BAB (void);
// 0x0000066D System.Void MoreMountains.CorgiEngine.ButtonActivated::MakeUnactivable()
extern void ButtonActivated_MakeUnactivable_mD67FF69BCF4779CA9F06587ADDD2FE9FA748D595 (void);
// 0x0000066E System.Void MoreMountains.CorgiEngine.ButtonActivated::ToggleActivable()
extern void ButtonActivated_ToggleActivable_m21D8BB59C68BAAFAC9326AE3515A1E667169B52E (void);
// 0x0000066F System.Void MoreMountains.CorgiEngine.ButtonActivated::TriggerButtonAction(UnityEngine.GameObject)
extern void ButtonActivated_TriggerButtonAction_m90ABA5F03388FD3B7B2DC788838C3D638F02ED6F (void);
// 0x00000670 System.Void MoreMountains.CorgiEngine.ButtonActivated::TriggerExitAction(UnityEngine.GameObject)
extern void ButtonActivated_TriggerExitAction_m97D1EAB36E83B9D79288723D747EB956CFAE66D8 (void);
// 0x00000671 System.Void MoreMountains.CorgiEngine.ButtonActivated::ActivateZone()
extern void ButtonActivated_ActivateZone_m16343B254AB6C2A2E55673FC0E90733925A73B4B (void);
// 0x00000672 System.Void MoreMountains.CorgiEngine.ButtonActivated::DisableAfterActivation()
extern void ButtonActivated_DisableAfterActivation_m433C13F25D47903C7DC20A234541A949E1ACAC62 (void);
// 0x00000673 System.Void MoreMountains.CorgiEngine.ButtonActivated::PromptError()
extern void ButtonActivated_PromptError_m3EB905906646471FFA79AE2FA48AA6DCB75A0BF7 (void);
// 0x00000674 System.Void MoreMountains.CorgiEngine.ButtonActivated::ShowPrompt()
extern void ButtonActivated_ShowPrompt_m6F5B8F1B11F01471630BB7CF809D64ACCA795BCD (void);
// 0x00000675 System.Void MoreMountains.CorgiEngine.ButtonActivated::HidePrompt()
extern void ButtonActivated_HidePrompt_m2B5FE78D843E05244B08F87C37A533235350D218 (void);
// 0x00000676 System.Void MoreMountains.CorgiEngine.ButtonActivated::DisableZone()
extern void ButtonActivated_DisableZone_m2886F9F326156B7CA159B3CEDD58EA31CF8D64D8 (void);
// 0x00000677 System.Void MoreMountains.CorgiEngine.ButtonActivated::EnableZone()
extern void ButtonActivated_EnableZone_mCBDE9A433FBC4719AE77FAA1B46D30868DF4D450 (void);
// 0x00000678 System.Void MoreMountains.CorgiEngine.ButtonActivated::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ButtonActivated_OnTriggerEnter2D_m658128E15BA4BDCCF88ABA9970E0BFB9A6C305BB (void);
// 0x00000679 System.Void MoreMountains.CorgiEngine.ButtonActivated::OnTriggerStay2D(UnityEngine.Collider2D)
extern void ButtonActivated_OnTriggerStay2D_m9C2A689D96D707EC3B7AA81744CCF933F087BB13 (void);
// 0x0000067A System.Void MoreMountains.CorgiEngine.ButtonActivated::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ButtonActivated_OnTriggerExit2D_mEA78A3D90AA324B7FCBAD5EBA9CEAED8BCB081E2 (void);
// 0x0000067B System.Void MoreMountains.CorgiEngine.ButtonActivated::TriggerEnter(UnityEngine.GameObject)
extern void ButtonActivated_TriggerEnter_m243976EE70F5E8C263F80C611022F569572C3D90 (void);
// 0x0000067C System.Void MoreMountains.CorgiEngine.ButtonActivated::TriggerExit(UnityEngine.GameObject)
extern void ButtonActivated_TriggerExit_mD108186D2B38EE6D1C558910DE5FFC2DEEB8A112 (void);
// 0x0000067D System.Boolean MoreMountains.CorgiEngine.ButtonActivated::TestForLastObject(UnityEngine.GameObject)
extern void ButtonActivated_TestForLastObject_mF8867455118D1FFEF38D753B2445B84608B95938 (void);
// 0x0000067E System.Boolean MoreMountains.CorgiEngine.ButtonActivated::CheckNumberOfUses()
extern void ButtonActivated_CheckNumberOfUses_m42CD34C39D77578D98A04190DF96AD146EFC3499 (void);
// 0x0000067F System.Boolean MoreMountains.CorgiEngine.ButtonActivated::CheckConditions(UnityEngine.GameObject)
extern void ButtonActivated_CheckConditions_mA9ACC7DAD99CF203C9D02C81BB802EC4B3F3EBFF (void);
// 0x00000680 System.Void MoreMountains.CorgiEngine.ButtonActivated::.ctor()
extern void ButtonActivated__ctor_m6FEA8A488C4C38817E424F66B6956EB282CFEE81 (void);
// 0x00000681 System.Void MoreMountains.CorgiEngine.ButtonPrompt::Initialization()
extern void ButtonPrompt_Initialization_m15BD9CFBB4470CCF9D0B91BEAD79EFF4CFDCB45D (void);
// 0x00000682 System.Void MoreMountains.CorgiEngine.ButtonPrompt::SetText(System.String)
extern void ButtonPrompt_SetText_m654F69B263BFB1812B8254D9F4B78747A35DEC8C (void);
// 0x00000683 System.Void MoreMountains.CorgiEngine.ButtonPrompt::SetBackgroundColor(UnityEngine.Color)
extern void ButtonPrompt_SetBackgroundColor_m06ED7E2D7AC5B5E38097941A7EEF7201677BE8DC (void);
// 0x00000684 System.Void MoreMountains.CorgiEngine.ButtonPrompt::SetTextColor(UnityEngine.Color)
extern void ButtonPrompt_SetTextColor_m1F33D1EE6DDC9BC1153FAC544D940D3138223B6A (void);
// 0x00000685 System.Void MoreMountains.CorgiEngine.ButtonPrompt::Show()
extern void ButtonPrompt_Show_m797864412C5CE0EC59062EB64C623D47A3E77356 (void);
// 0x00000686 System.Void MoreMountains.CorgiEngine.ButtonPrompt::Hide()
extern void ButtonPrompt_Hide_m36B0C96FB33036E41CEE7E503A85AF17D659D28C (void);
// 0x00000687 System.Collections.IEnumerator MoreMountains.CorgiEngine.ButtonPrompt::HideCo()
extern void ButtonPrompt_HideCo_m8E26A74507A52BFCF21D7A829DB8DC6A7A057DD0 (void);
// 0x00000688 System.Void MoreMountains.CorgiEngine.ButtonPrompt::.ctor()
extern void ButtonPrompt__ctor_mBB7FBF6FEE9E6670761D774D18DA8636E31BCA18 (void);
// 0x00000689 System.Void MoreMountains.CorgiEngine.ButtonPrompt/<HideCo>d__16::.ctor(System.Int32)
extern void U3CHideCoU3Ed__16__ctor_m371127392E9A25FFD5660D16196AE80B4F8FBC33 (void);
// 0x0000068A System.Void MoreMountains.CorgiEngine.ButtonPrompt/<HideCo>d__16::System.IDisposable.Dispose()
extern void U3CHideCoU3Ed__16_System_IDisposable_Dispose_mCEF923AB115F4C6FDE5180B1CD5CCDADFC05157A (void);
// 0x0000068B System.Boolean MoreMountains.CorgiEngine.ButtonPrompt/<HideCo>d__16::MoveNext()
extern void U3CHideCoU3Ed__16_MoveNext_mEC7D6211A4E0750D62540C6E1B94DF13CF9F6F31 (void);
// 0x0000068C System.Object MoreMountains.CorgiEngine.ButtonPrompt/<HideCo>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHideCoU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD066706C248E09602848E2A0EB7E19A058C6C114 (void);
// 0x0000068D System.Void MoreMountains.CorgiEngine.ButtonPrompt/<HideCo>d__16::System.Collections.IEnumerator.Reset()
extern void U3CHideCoU3Ed__16_System_Collections_IEnumerator_Reset_m5184A37CAEAB09581A7437EC383295162690D579 (void);
// 0x0000068E System.Object MoreMountains.CorgiEngine.ButtonPrompt/<HideCo>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CHideCoU3Ed__16_System_Collections_IEnumerator_get_Current_mE2258AFE39AAB29B4B932D6AFA427C132A244704 (void);
// 0x0000068F System.Void MoreMountains.CorgiEngine.DialogueBox::ChangeText(System.String)
extern void DialogueBox_ChangeText_m3C8ADDDF16E94B81DEA547F98CE672E7EC138DC0 (void);
// 0x00000690 System.Void MoreMountains.CorgiEngine.DialogueBox::ButtonActive(System.Boolean)
extern void DialogueBox_ButtonActive_m95F191D6BF48CA90CEF3B1F063E36A6E1FF50664 (void);
// 0x00000691 System.Void MoreMountains.CorgiEngine.DialogueBox::ChangeColor(UnityEngine.Color,UnityEngine.Color)
extern void DialogueBox_ChangeColor_mCEDE7DE9F0D5A9DBC6C27565B8A972D6B3E8F2F9 (void);
// 0x00000692 System.Void MoreMountains.CorgiEngine.DialogueBox::FadeIn(System.Single)
extern void DialogueBox_FadeIn_mCBFCA91D96F54F7E11973FCF8B2DBD5419803CD4 (void);
// 0x00000693 System.Void MoreMountains.CorgiEngine.DialogueBox::FadeOut(System.Single)
extern void DialogueBox_FadeOut_mF6695ABE43126C13EC145DA71FDE6D2040C1764E (void);
// 0x00000694 System.Void MoreMountains.CorgiEngine.DialogueBox::HideArrow()
extern void DialogueBox_HideArrow_mC121FD152AEBBB3545E4484075A7982D4F4805E8 (void);
// 0x00000695 System.Void MoreMountains.CorgiEngine.DialogueBox::.ctor()
extern void DialogueBox__ctor_m172D40F2CB347EC22DCEFA1AB4A5CB9197D53955 (void);
// 0x00000696 System.Void MoreMountains.CorgiEngine.DialogueZone::OnEnable()
extern void DialogueZone_OnEnable_m8ED457488EAF4E1E6F45E72ED571CAE16E7406D8 (void);
// 0x00000697 System.Void MoreMountains.CorgiEngine.DialogueZone::TriggerButtonAction(UnityEngine.GameObject)
extern void DialogueZone_TriggerButtonAction_m0CBFAF0D3D4734AEDC8FE04EDDEB1286B93E3C48 (void);
// 0x00000698 System.Void MoreMountains.CorgiEngine.DialogueZone::DisableAfterActivation()
extern void DialogueZone_DisableAfterActivation_m1148D021C00E3E63F73BE849F22D9E8AA036E712 (void);
// 0x00000699 System.Void MoreMountains.CorgiEngine.DialogueZone::StartDialogue()
extern void DialogueZone_StartDialogue_m977B341948614833560581F9372276B2F9DB572A (void);
// 0x0000069A System.Collections.IEnumerator MoreMountains.CorgiEngine.DialogueZone::PlayNextDialogue()
extern void DialogueZone_PlayNextDialogue_mA9B88416AA47B6762DAB5933D04FA2B5A8876209 (void);
// 0x0000069B System.Collections.IEnumerator MoreMountains.CorgiEngine.DialogueZone::AutoNextDialogue()
extern void DialogueZone_AutoNextDialogue_m8D3AA3CC8BC2280C60171FF7747B7CA23909865F (void);
// 0x0000069C System.Collections.IEnumerator MoreMountains.CorgiEngine.DialogueZone::Reactivate()
extern void DialogueZone_Reactivate_m021FC5E3354B896489ED4387DF3F91078DDFE56B (void);
// 0x0000069D System.Void MoreMountains.CorgiEngine.DialogueZone::.ctor()
extern void DialogueZone__ctor_m181FCDD4E6A7B8F9556A2C60FE9BA769352CC4E6 (void);
// 0x0000069E System.Void MoreMountains.CorgiEngine.DialogueZone/<PlayNextDialogue>d__29::.ctor(System.Int32)
extern void U3CPlayNextDialogueU3Ed__29__ctor_m11E43E1FEE132C3ABDD5793C7626DCF509761233 (void);
// 0x0000069F System.Void MoreMountains.CorgiEngine.DialogueZone/<PlayNextDialogue>d__29::System.IDisposable.Dispose()
extern void U3CPlayNextDialogueU3Ed__29_System_IDisposable_Dispose_m180E5363B857E3A3FFD61C8E7C48FDF3DE43A4EE (void);
// 0x000006A0 System.Boolean MoreMountains.CorgiEngine.DialogueZone/<PlayNextDialogue>d__29::MoveNext()
extern void U3CPlayNextDialogueU3Ed__29_MoveNext_mA2588A9BC1B97A5B8A0047A70FFAEE9A54B31E91 (void);
// 0x000006A1 System.Object MoreMountains.CorgiEngine.DialogueZone/<PlayNextDialogue>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayNextDialogueU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF58A3F42B461CB1CF8A5354828F5D0A98404F792 (void);
// 0x000006A2 System.Void MoreMountains.CorgiEngine.DialogueZone/<PlayNextDialogue>d__29::System.Collections.IEnumerator.Reset()
extern void U3CPlayNextDialogueU3Ed__29_System_Collections_IEnumerator_Reset_mEAF8F9183C6FB7C0EB8670D90056AB5B01F62E0A (void);
// 0x000006A3 System.Object MoreMountains.CorgiEngine.DialogueZone/<PlayNextDialogue>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CPlayNextDialogueU3Ed__29_System_Collections_IEnumerator_get_Current_m9DA02A9FE9981E93A2C4C6EF6A9CAF0A8FAC43AA (void);
// 0x000006A4 System.Void MoreMountains.CorgiEngine.DialogueZone/<AutoNextDialogue>d__30::.ctor(System.Int32)
extern void U3CAutoNextDialogueU3Ed__30__ctor_m320C6303EAB6DC0C1B9E9E169831A517AB8898BE (void);
// 0x000006A5 System.Void MoreMountains.CorgiEngine.DialogueZone/<AutoNextDialogue>d__30::System.IDisposable.Dispose()
extern void U3CAutoNextDialogueU3Ed__30_System_IDisposable_Dispose_m1F35BA55BA32A59249941C09CE0965EC27F87D36 (void);
// 0x000006A6 System.Boolean MoreMountains.CorgiEngine.DialogueZone/<AutoNextDialogue>d__30::MoveNext()
extern void U3CAutoNextDialogueU3Ed__30_MoveNext_m679BC6C113AC2FF548F05A2650BBB77A35DF84E3 (void);
// 0x000006A7 System.Object MoreMountains.CorgiEngine.DialogueZone/<AutoNextDialogue>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAutoNextDialogueU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09813C1D58F52D43D7440294F08BB22088200189 (void);
// 0x000006A8 System.Void MoreMountains.CorgiEngine.DialogueZone/<AutoNextDialogue>d__30::System.Collections.IEnumerator.Reset()
extern void U3CAutoNextDialogueU3Ed__30_System_Collections_IEnumerator_Reset_m66DCC1B31E2C959B0F0FF0EF94E5E28345F1A048 (void);
// 0x000006A9 System.Object MoreMountains.CorgiEngine.DialogueZone/<AutoNextDialogue>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CAutoNextDialogueU3Ed__30_System_Collections_IEnumerator_get_Current_m61D159A6E110272F54A67D2A143467979840345E (void);
// 0x000006AA System.Void MoreMountains.CorgiEngine.DialogueZone/<Reactivate>d__31::.ctor(System.Int32)
extern void U3CReactivateU3Ed__31__ctor_m34F08A93EB12AF320F77CCA7F5B648D845618290 (void);
// 0x000006AB System.Void MoreMountains.CorgiEngine.DialogueZone/<Reactivate>d__31::System.IDisposable.Dispose()
extern void U3CReactivateU3Ed__31_System_IDisposable_Dispose_mF04C107126D75335216D418B6F8555716F394146 (void);
// 0x000006AC System.Boolean MoreMountains.CorgiEngine.DialogueZone/<Reactivate>d__31::MoveNext()
extern void U3CReactivateU3Ed__31_MoveNext_m105D11C282979075800B41853F5D396F7EB3E922 (void);
// 0x000006AD System.Object MoreMountains.CorgiEngine.DialogueZone/<Reactivate>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReactivateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9C55EAE5041CF68DB3F40EF6D35B79A0BE46560 (void);
// 0x000006AE System.Void MoreMountains.CorgiEngine.DialogueZone/<Reactivate>d__31::System.Collections.IEnumerator.Reset()
extern void U3CReactivateU3Ed__31_System_Collections_IEnumerator_Reset_mBD274240225A5A8A4D2F5C06FF0DBE2B697D7822 (void);
// 0x000006AF System.Object MoreMountains.CorgiEngine.DialogueZone/<Reactivate>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CReactivateU3Ed__31_System_Collections_IEnumerator_get_Current_mD0A42D9D6D0C0D3EE7CD11AD943C49D88E0B9838 (void);
// 0x000006B0 System.Void MoreMountains.CorgiEngine.GUIManager::Awake()
extern void GUIManager_Awake_mCCB90D20024E78083726441008AE2C7E639E8C89 (void);
// 0x000006B1 System.Void MoreMountains.CorgiEngine.GUIManager::Start()
extern void GUIManager_Start_m92CF04CD26C1F149A9007046F3A423EF04A43262 (void);
// 0x000006B2 System.Void MoreMountains.CorgiEngine.GUIManager::SetHUDActive(System.Boolean)
extern void GUIManager_SetHUDActive_m115FEC751884ACD6A226880A507F8F14D92D6062 (void);
// 0x000006B3 System.Void MoreMountains.CorgiEngine.GUIManager::SetAvatarActive(System.Boolean)
extern void GUIManager_SetAvatarActive_mD7393680DDA255CFC5C09F2BD916C0D1DB4BAD37 (void);
// 0x000006B4 System.Void MoreMountains.CorgiEngine.GUIManager::SetMobileControlsActive(System.Boolean,MoreMountains.CorgiEngine.InputManager/MovementControls)
extern void GUIManager_SetMobileControlsActive_m198DAB0D458FB457993B8F8C159FBB2329E8D6E0 (void);
// 0x000006B5 System.Void MoreMountains.CorgiEngine.GUIManager::SetPause(System.Boolean)
extern void GUIManager_SetPause_m015B377C7A110B43E49743D1055FB62598D2CFD7 (void);
// 0x000006B6 System.Void MoreMountains.CorgiEngine.GUIManager::SetJetpackBar(System.Boolean,System.String)
extern void GUIManager_SetJetpackBar_m2F53211389C9C6070C41BA7357F5DC4100D081C3 (void);
// 0x000006B7 System.Void MoreMountains.CorgiEngine.GUIManager::SetAmmoDisplays(System.Boolean,System.String,System.Int32)
extern void GUIManager_SetAmmoDisplays_m6975C15FEAA4977BB6ECE087F488C4467378DFD5 (void);
// 0x000006B8 System.Void MoreMountains.CorgiEngine.GUIManager::SetTimeSplash(System.Boolean)
extern void GUIManager_SetTimeSplash_m2EEA46125DFF2566D060D85153A36D0904ECB509 (void);
// 0x000006B9 System.Void MoreMountains.CorgiEngine.GUIManager::RefreshPoints()
extern void GUIManager_RefreshPoints_m546A8FE786034AF253826E1C5CDCC971EC23AADE (void);
// 0x000006BA System.Void MoreMountains.CorgiEngine.GUIManager::UpdateHealthBar(System.Single,System.Single,System.Single,System.String)
extern void GUIManager_UpdateHealthBar_mE16DCC61B4D46BA7B8C77672B63DA8FEA6069710 (void);
// 0x000006BB System.Void MoreMountains.CorgiEngine.GUIManager::UpdateJetpackBar(System.Single,System.Single,System.Single,System.String)
extern void GUIManager_UpdateJetpackBar_m577B62FBE2B7D797FD0928C852C44778342728E0 (void);
// 0x000006BC System.Void MoreMountains.CorgiEngine.GUIManager::UpdateAmmoDisplays(System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.String,System.Int32,System.Boolean)
extern void GUIManager_UpdateAmmoDisplays_mCAFB24A21E053A2E552D2766A771F3B2A4C05AF5 (void);
// 0x000006BD System.Void MoreMountains.CorgiEngine.GUIManager::SetLevelName(System.String)
extern void GUIManager_SetLevelName_m8300F2A94AA5825E948588D8C990CB66B17E5518 (void);
// 0x000006BE System.Void MoreMountains.CorgiEngine.GUIManager::OnMMEvent(MoreMountains.CorgiEngine.LevelNameEvent)
extern void GUIManager_OnMMEvent_m2BDB5BCD2B7FAB57D35BAF7114F21C347FD9DB71 (void);
// 0x000006BF System.Void MoreMountains.CorgiEngine.GUIManager::OnMMEvent(MoreMountains.CorgiEngine.ControlsModeEvent)
extern void GUIManager_OnMMEvent_mC6EBE330ED7292F2FA8D545ABBE846BD7DE28419 (void);
// 0x000006C0 System.Void MoreMountains.CorgiEngine.GUIManager::OnEnable()
extern void GUIManager_OnEnable_mA6F3F63E914526C542DF29CD4373F1F0D01BC96B (void);
// 0x000006C1 System.Void MoreMountains.CorgiEngine.GUIManager::OnDisable()
extern void GUIManager_OnDisable_mB9C57ECD69FFDDE22F22081A9566F88E87E98356 (void);
// 0x000006C2 System.Void MoreMountains.CorgiEngine.GUIManager::.ctor()
extern void GUIManager__ctor_m6FF23ADF78E962A688E7A29256CB50F232F9E84B (void);
// 0x000006C3 System.Void MoreMountains.CorgiEngine.HeartsGUI::Start()
extern void HeartsGUI_Start_m48B131703BFC57D9DB8373D946504BA5BB818214 (void);
// 0x000006C4 System.Void MoreMountains.CorgiEngine.HeartsGUI::Initialization()
extern void HeartsGUI_Initialization_mBA0F2E846ABC68DCD11E46F06B49688A10433A75 (void);
// 0x000006C5 System.Void MoreMountains.CorgiEngine.HeartsGUI::DrawHearts()
extern void HeartsGUI_DrawHearts_mCB9B54B65F09C5ADC8DEBEF1577E0C91DDFD4501 (void);
// 0x000006C6 System.Void MoreMountains.CorgiEngine.HeartsGUI::Update()
extern void HeartsGUI_Update_m5B675C668A7F06AB067E7C40D6A657B9F90458B4 (void);
// 0x000006C7 System.Void MoreMountains.CorgiEngine.HeartsGUI::UpdateHearts()
extern void HeartsGUI_UpdateHearts_m41DCA3164FE2D4DFF699E18DFA70AE95B205DEAF (void);
// 0x000006C8 System.Void MoreMountains.CorgiEngine.HeartsGUI::.ctor()
extern void HeartsGUI__ctor_mC422BD35C1160D97998EAE410118489CC6453A5C (void);
// 0x000006C9 System.Void MoreMountains.CorgiEngine.HipsterGUI::Start()
extern void HipsterGUI_Start_m5F8F9D53263CE13FE415DF5DE72E2BD05B2870D5 (void);
// 0x000006CA System.Void MoreMountains.CorgiEngine.HipsterGUI::.ctor()
extern void HipsterGUI__ctor_mC1F7A43D65EED54C2BA8CA77CABF4BF6ED9BCCFC (void);
// 0x000006CB System.Void MoreMountains.CorgiEngine.LevelSelector::GoToLevel()
extern void LevelSelector_GoToLevel_m184887F4B962560F0C0C37F8282C931C73B811E0 (void);
// 0x000006CC System.Void MoreMountains.CorgiEngine.LevelSelector::RestartLevel()
extern void LevelSelector_RestartLevel_mCE3B767BE79F6149BF130652836D18E4F23BE242 (void);
// 0x000006CD System.Void MoreMountains.CorgiEngine.LevelSelector::.ctor()
extern void LevelSelector__ctor_mB39418971A43B41B2BA4AB70AD59C750AC7D0606 (void);
// 0x000006CE System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::Start()
extern void LevelSelectorGUI_Start_mA4F4C75F4404AD49665CBFE924D480112CE6198A (void);
// 0x000006CF System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::SetLevelName(System.String)
extern void LevelSelectorGUI_SetLevelName_m3F9CD411F27FA261418DE6221ABB8BEEB0C464F4 (void);
// 0x000006D0 System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::TurnOffLevelName()
extern void LevelSelectorGUI_TurnOffLevelName_m7AB3511F23F400B4AFA7BBAEBE9462B564E2FDBE (void);
// 0x000006D1 System.Void MoreMountains.CorgiEngine.LevelSelectorGUI::.ctor()
extern void LevelSelectorGUI__ctor_mA842F021F843776051A47B7CB7E4CA0E6D078370 (void);
// 0x000006D2 System.Void MoreMountains.CorgiEngine.MultiplayerGUIManager::ShowMultiplayerEndgame()
extern void MultiplayerGUIManager_ShowMultiplayerEndgame_mE6746CA0D5A13FC57E2969CB7E94CC5B0DA8725C (void);
// 0x000006D3 System.Void MoreMountains.CorgiEngine.MultiplayerGUIManager::SetMultiplayerEndgameText(System.String)
extern void MultiplayerGUIManager_SetMultiplayerEndgameText_m98DCF67D75602321FCC59112C299F0BAB6CB906D (void);
// 0x000006D4 System.Void MoreMountains.CorgiEngine.MultiplayerGUIManager::.ctor()
extern void MultiplayerGUIManager__ctor_m8E05E9C9A1FE3DF1C85C047D4A8226FE61D9E4A4 (void);
// 0x000006D5 System.Void MoreMountains.CorgiEngine.MusicSwitch::On()
extern void MusicSwitch_On_mFB538F333996A1AB5D0BD39CB112EF571BAC6A7A (void);
// 0x000006D6 System.Void MoreMountains.CorgiEngine.MusicSwitch::Off()
extern void MusicSwitch_Off_m9BC9FF945964174353F806E284DD48F6DCD1C4EB (void);
// 0x000006D7 System.Void MoreMountains.CorgiEngine.MusicSwitch::.ctor()
extern void MusicSwitch__ctor_m74182A4F91083966C6080FE7B3B2E10D00FC8DD4 (void);
// 0x000006D8 System.Void MoreMountains.CorgiEngine.PauseButton::PauseButtonAction()
extern void PauseButton_PauseButtonAction_m378E4823D1C822971A3DBD4DC66D61822AEFCF07 (void);
// 0x000006D9 System.Collections.IEnumerator MoreMountains.CorgiEngine.PauseButton::PauseButtonCo()
extern void PauseButton_PauseButtonCo_m4F2B2E2208F45B76612F7F9FCFD4D6FA6744672B (void);
// 0x000006DA System.Void MoreMountains.CorgiEngine.PauseButton::.ctor()
extern void PauseButton__ctor_m5A3666A8BE921523D3F79BD502DC2130E13EFB4A (void);
// 0x000006DB System.Void MoreMountains.CorgiEngine.PauseButton/<PauseButtonCo>d__1::.ctor(System.Int32)
extern void U3CPauseButtonCoU3Ed__1__ctor_mF1420880758978D531D14446B6007387BEA26509 (void);
// 0x000006DC System.Void MoreMountains.CorgiEngine.PauseButton/<PauseButtonCo>d__1::System.IDisposable.Dispose()
extern void U3CPauseButtonCoU3Ed__1_System_IDisposable_Dispose_m987F976C8EFEF09D6AE5ECAE1A49AFB8CEA16CDC (void);
// 0x000006DD System.Boolean MoreMountains.CorgiEngine.PauseButton/<PauseButtonCo>d__1::MoveNext()
extern void U3CPauseButtonCoU3Ed__1_MoveNext_mEAF1FA385F389F19CC1D1793E883C7752CBAC4AB (void);
// 0x000006DE System.Object MoreMountains.CorgiEngine.PauseButton/<PauseButtonCo>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPauseButtonCoU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF67FD9554DCD05AB4FBE04B8AD497C7C8AC7B37 (void);
// 0x000006DF System.Void MoreMountains.CorgiEngine.PauseButton/<PauseButtonCo>d__1::System.Collections.IEnumerator.Reset()
extern void U3CPauseButtonCoU3Ed__1_System_Collections_IEnumerator_Reset_mD2BD98F94117A9ABBAF4D37003008A778C0D7613 (void);
// 0x000006E0 System.Object MoreMountains.CorgiEngine.PauseButton/<PauseButtonCo>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CPauseButtonCoU3Ed__1_System_Collections_IEnumerator_get_Current_m7AFDC398D89441896D3008D1B2B0415EAD735ABA (void);
// 0x000006E1 System.Void MoreMountains.CorgiEngine.SaveResetButton::ResetAllSaves()
extern void SaveResetButton_ResetAllSaves_mB42FD8A9F9FBA2B18B4B00E55845FEAAF52E5AF6 (void);
// 0x000006E2 System.Void MoreMountains.CorgiEngine.SaveResetButton::.ctor()
extern void SaveResetButton__ctor_m3C1EB835D7A757A978C99997EF575390B9D97DE1 (void);
// 0x000006E3 System.Void MoreMountains.CorgiEngine.SfxSwitch::On()
extern void SfxSwitch_On_m184EB8711AD33C0999C0CCB0BDBF05A77828FA72 (void);
// 0x000006E4 System.Void MoreMountains.CorgiEngine.SfxSwitch::Off()
extern void SfxSwitch_Off_m3EEFBBEE676025B14B8CA14152FD6AE3C2EC87E6 (void);
// 0x000006E5 System.Void MoreMountains.CorgiEngine.SfxSwitch::.ctor()
extern void SfxSwitch__ctor_m0EA89B025E29E332939F76EDD4E801B2B096EF6E (void);
// 0x000006E6 System.Void MoreMountains.CorgiEngine.StartScreen::Awake()
extern void StartScreen_Awake_mDEA4F8562CEC276185406B00C550BE86D9880AC7 (void);
// 0x000006E7 System.Void MoreMountains.CorgiEngine.StartScreen::Start()
extern void StartScreen_Start_m8E0A400D58DD6B52222E1993E538B98305ACE98A (void);
// 0x000006E8 System.Void MoreMountains.CorgiEngine.StartScreen::Update()
extern void StartScreen_Update_mF6BB05FEAAC94881B3A05CFD3D55FF53188E97BD (void);
// 0x000006E9 System.Void MoreMountains.CorgiEngine.StartScreen::ButtonPressed()
extern void StartScreen_ButtonPressed_m8CB13A9EC971F01623564D78A568B5BD5582A8EB (void);
// 0x000006EA System.Collections.IEnumerator MoreMountains.CorgiEngine.StartScreen::LoadFirstLevel()
extern void StartScreen_LoadFirstLevel_m7D50C59812E85C25570FE3785D5E0CA0E61AA5E3 (void);
// 0x000006EB System.Void MoreMountains.CorgiEngine.StartScreen::.ctor()
extern void StartScreen__ctor_m09C899B309D6FDF680812ECDB06DF725735E563A (void);
// 0x000006EC System.Void MoreMountains.CorgiEngine.StartScreen/<Start>d__8::MoveNext()
extern void U3CStartU3Ed__8_MoveNext_m6657946AA5BE7DB9648EE3CD5E3B54E30B01096C (void);
// 0x000006ED System.Void MoreMountains.CorgiEngine.StartScreen/<Start>d__8::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__8_SetStateMachine_m74E3151055CC61E7A5033703DF6031249BAC0790 (void);
// 0x000006EE System.Void MoreMountains.CorgiEngine.StartScreen/<LoadFirstLevel>d__11::.ctor(System.Int32)
extern void U3CLoadFirstLevelU3Ed__11__ctor_m148453B20FC694DCD92E6E4C27F2868ACAD23488 (void);
// 0x000006EF System.Void MoreMountains.CorgiEngine.StartScreen/<LoadFirstLevel>d__11::System.IDisposable.Dispose()
extern void U3CLoadFirstLevelU3Ed__11_System_IDisposable_Dispose_m05D34CEEDE35DD16A5B62648B86F900D61451D17 (void);
// 0x000006F0 System.Boolean MoreMountains.CorgiEngine.StartScreen/<LoadFirstLevel>d__11::MoveNext()
extern void U3CLoadFirstLevelU3Ed__11_MoveNext_mB213D3C241F131CB555C3DF23A3BED0E46D0F8E1 (void);
// 0x000006F1 System.Object MoreMountains.CorgiEngine.StartScreen/<LoadFirstLevel>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadFirstLevelU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67D32B9CF698FD499B1EFAC2705E982A6765633D (void);
// 0x000006F2 System.Void MoreMountains.CorgiEngine.StartScreen/<LoadFirstLevel>d__11::System.Collections.IEnumerator.Reset()
extern void U3CLoadFirstLevelU3Ed__11_System_Collections_IEnumerator_Reset_mCAEF2AD5DE4959BA7D9F151FB5F1A78EBE80EF66 (void);
// 0x000006F3 System.Object MoreMountains.CorgiEngine.StartScreen/<LoadFirstLevel>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CLoadFirstLevelU3Ed__11_System_Collections_IEnumerator_get_Current_m68D05441657E9955AE25B1A08417F7B177F0E8DB (void);
// 0x000006F4 System.Void MoreMountains.CorgiEngine.InventoryEngineChest::Start()
extern void InventoryEngineChest_Start_mA244EFB8FE8B4FFDC39373DCC5131277D7FDD78C (void);
// 0x000006F5 System.Void MoreMountains.CorgiEngine.InventoryEngineChest::OpenChest()
extern void InventoryEngineChest_OpenChest_m36505217127344B876DEA3423B04BD4A86981C71 (void);
// 0x000006F6 System.Void MoreMountains.CorgiEngine.InventoryEngineChest::TriggerOpeningAnimation()
extern void InventoryEngineChest_TriggerOpeningAnimation_m43657D8550A74B990C2CBC000D8BBAF98B938936 (void);
// 0x000006F7 System.Void MoreMountains.CorgiEngine.InventoryEngineChest::PickChestContents()
extern void InventoryEngineChest_PickChestContents_mF401A005AC74F9B26C8D406F6D68F7119C2DE936 (void);
// 0x000006F8 System.Void MoreMountains.CorgiEngine.InventoryEngineChest::.ctor()
extern void InventoryEngineChest__ctor_mB3FB91CB6DD3ED46CB320382FE2F896A7D76E1CA (void);
// 0x000006F9 System.Boolean MoreMountains.CorgiEngine.InventoryEngineHealth::Use(System.String)
extern void InventoryEngineHealth_Use_mCA0CF06A431305B501863DD4B87B3CA0C34DEF1B (void);
// 0x000006FA System.Void MoreMountains.CorgiEngine.InventoryEngineHealth::.ctor()
extern void InventoryEngineHealth__ctor_mEA787E7437ECEA1FAC3FF0AB259A5D79FCF5DC2E (void);
// 0x000006FB System.Boolean MoreMountains.CorgiEngine.InventoryEngineKey::Use(System.String)
extern void InventoryEngineKey_Use_mF9DEB3CA9C7FBEC46F07B22EC51AA7781D087088 (void);
// 0x000006FC System.Void MoreMountains.CorgiEngine.InventoryEngineKey::.ctor()
extern void InventoryEngineKey__ctor_m67F90E9FDB38D264417E641A9A5A0E31ABD2BE19 (void);
// 0x000006FD System.Boolean MoreMountains.CorgiEngine.InventoryEnginePoints::Pick(System.String)
extern void InventoryEnginePoints_Pick_m72FDE9CCE2EB38953D9574FE4565EB615585F7FB (void);
// 0x000006FE System.Void MoreMountains.CorgiEngine.InventoryEnginePoints::.ctor()
extern void InventoryEnginePoints__ctor_mB93C9ED3BB7E0852B2B77E82F51E22F2A0CD6019 (void);
// 0x000006FF System.Boolean MoreMountains.CorgiEngine.InventoryEngineTimeModifier::Use(System.String)
extern void InventoryEngineTimeModifier_Use_m8C2598A48F40BCEE65A01D3923139610522266C2 (void);
// 0x00000700 System.Void MoreMountains.CorgiEngine.InventoryEngineTimeModifier::.ctor()
extern void InventoryEngineTimeModifier__ctor_mE9C6995B1FBB277893DABEDDE2944B4D514E6C9E (void);
// 0x00000701 System.Boolean MoreMountains.CorgiEngine.InventoryEngineWeapon::Equip(System.String)
extern void InventoryEngineWeapon_Equip_m108B9A2A0AC1BFC44508072D0B0B6E4A0166543B (void);
// 0x00000702 System.Boolean MoreMountains.CorgiEngine.InventoryEngineWeapon::UnEquip(System.String)
extern void InventoryEngineWeapon_UnEquip_m91E76AA4A82322AA4617F130ADA38DA05240431D (void);
// 0x00000703 System.Void MoreMountains.CorgiEngine.InventoryEngineWeapon::EquipWeapon(MoreMountains.CorgiEngine.Weapon,System.String)
extern void InventoryEngineWeapon_EquipWeapon_m4C9D883EC5BB0A52799D613601703DA71B1FDC3A (void);
// 0x00000704 System.Void MoreMountains.CorgiEngine.InventoryEngineWeapon::.ctor()
extern void InventoryEngineWeapon__ctor_m11FEAC7A429B63D740D974D49A5AF89F5954A15A (void);
// 0x00000705 System.Void MoreMountains.CorgiEngine.Coin::Pick()
extern void Coin_Pick_m62295EA0FF28AC524B91D49BB9D85F249C669BB7 (void);
// 0x00000706 System.Void MoreMountains.CorgiEngine.Coin::.ctor()
extern void Coin__ctor_m2F8A3D35F761C8FD050B3AB2F824C97E4DBE6AD3 (void);
// 0x00000707 System.Void MoreMountains.CorgiEngine.InventoryPickableItem::PickSuccess()
extern void InventoryPickableItem_PickSuccess_mAE7C49DA437F7F5D70CE542584F97A10B1E550DB (void);
// 0x00000708 System.Void MoreMountains.CorgiEngine.InventoryPickableItem::Effects()
extern void InventoryPickableItem_Effects_m08AD585801AA32178E6492BA09A8CD051098E629 (void);
// 0x00000709 System.Void MoreMountains.CorgiEngine.InventoryPickableItem::.ctor()
extern void InventoryPickableItem__ctor_mB5079E59CF427BDD4B192A7886448B25402E7672 (void);
// 0x0000070A System.Void MoreMountains.CorgiEngine.Loot::Awake()
extern void Loot_Awake_mDE7B59B238CFC42E38A67D80B2078EA794948060 (void);
// 0x0000070B System.Void MoreMountains.CorgiEngine.Loot::ResetRemainingQuantity()
extern void Loot_ResetRemainingQuantity_mED1DC8D53F4E25E819EBA2E6CA29BB0018029C8A (void);
// 0x0000070C System.Void MoreMountains.CorgiEngine.Loot::InitializeLootTable()
extern void Loot_InitializeLootTable_mDCC02EF6E3A07B23F9934C53CC912A429529EDCD (void);
// 0x0000070D System.Void MoreMountains.CorgiEngine.Loot::SpawnLoot()
extern void Loot_SpawnLoot_mD24D796F0BA83406E9E486047A61E35E1103329D (void);
// 0x0000070E System.Void MoreMountains.CorgiEngine.Loot::SpawnLootDebug()
extern void Loot_SpawnLootDebug_m445D57BBAE3A32A99740911C360D24D885752ADD (void);
// 0x0000070F System.Collections.IEnumerator MoreMountains.CorgiEngine.Loot::SpawnLootCo()
extern void Loot_SpawnLootCo_mC338BF194916D173F265EED2CEFA232BC9640547 (void);
// 0x00000710 System.Void MoreMountains.CorgiEngine.Loot::SpawnOneLoot()
extern void Loot_SpawnOneLoot_m5CF41912021D884F95B8284F2B734E79067CF116 (void);
// 0x00000711 UnityEngine.GameObject MoreMountains.CorgiEngine.Loot::GetObject()
extern void Loot_GetObject_m84FDAE0B5DC313CA90C0E65CC8CB8F5E7BFB6512 (void);
// 0x00000712 System.Void MoreMountains.CorgiEngine.Loot::OnHit()
extern void Loot_OnHit_m3836E0BFEE9024EB64E3E082D1D4E76711AE5227 (void);
// 0x00000713 System.Void MoreMountains.CorgiEngine.Loot::OnDeath()
extern void Loot_OnDeath_m1F921E44EFED5A8E2DB969D51EEFF3172116CF00 (void);
// 0x00000714 System.Void MoreMountains.CorgiEngine.Loot::OnEnable()
extern void Loot_OnEnable_mD4BDC945E34685A8ADEC6FEBBAD2152BC7A26CD4 (void);
// 0x00000715 System.Void MoreMountains.CorgiEngine.Loot::OnDisable()
extern void Loot_OnDisable_m66CB9A96524C3AA8419C74DBA10D7A53B67615D3 (void);
// 0x00000716 System.Void MoreMountains.CorgiEngine.Loot::OnDrawGizmos()
extern void Loot_OnDrawGizmos_m3AAB4BAFC5AF0BDE92993C7397740D3D7034656E (void);
// 0x00000717 System.Void MoreMountains.CorgiEngine.Loot::.ctor()
extern void Loot__ctor_m4285F00165AE2CF8377E6A4EE7EBFDBA77EBAEBE (void);
// 0x00000718 System.Void MoreMountains.CorgiEngine.Loot/<SpawnLootCo>d__33::.ctor(System.Int32)
extern void U3CSpawnLootCoU3Ed__33__ctor_mBE644A8C2503E0F6FB6AF7BE2B0B184B94B2848F (void);
// 0x00000719 System.Void MoreMountains.CorgiEngine.Loot/<SpawnLootCo>d__33::System.IDisposable.Dispose()
extern void U3CSpawnLootCoU3Ed__33_System_IDisposable_Dispose_mB481ECE8ECC126883B1FA7C36D7536420FF18118 (void);
// 0x0000071A System.Boolean MoreMountains.CorgiEngine.Loot/<SpawnLootCo>d__33::MoveNext()
extern void U3CSpawnLootCoU3Ed__33_MoveNext_mC75AA102DC0611DAC62C612D4DADD20E35F70EC2 (void);
// 0x0000071B System.Object MoreMountains.CorgiEngine.Loot/<SpawnLootCo>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSpawnLootCoU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m347A80D6745618A10A44AD579DDA4501EC7FC279 (void);
// 0x0000071C System.Void MoreMountains.CorgiEngine.Loot/<SpawnLootCo>d__33::System.Collections.IEnumerator.Reset()
extern void U3CSpawnLootCoU3Ed__33_System_Collections_IEnumerator_Reset_m4101457E6041C181B1F3D82EA53FAE48DCFC0D11 (void);
// 0x0000071D System.Object MoreMountains.CorgiEngine.Loot/<SpawnLootCo>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CSpawnLootCoU3Ed__33_System_Collections_IEnumerator_get_Current_m291A7D6949A19EF3BFFB0B1F983FCE677687595C (void);
// 0x0000071E System.Void MoreMountains.CorgiEngine.PickableAction::Pick()
extern void PickableAction_Pick_m49960385643A9663C9BC55619109C752CDA4AB73 (void);
// 0x0000071F System.Void MoreMountains.CorgiEngine.PickableAction::.ctor()
extern void PickableAction__ctor_mEF8C667FE0BA98631B7E721AF33A0EA1AEEBADF8 (void);
// 0x00000720 System.Void MoreMountains.CorgiEngine.PickableItemEvent::.ctor(MoreMountains.CorgiEngine.PickableItem)
extern void PickableItemEvent__ctor_m9007FE4203B4D474DECD5E651631D3C2BB82E3C1 (void);
// 0x00000721 System.Void MoreMountains.CorgiEngine.PickableItemEvent::Trigger(MoreMountains.CorgiEngine.PickableItem)
extern void PickableItemEvent_Trigger_m6BB7CDBF4A424E4A7B4CD805BC6EDFF71AAB9512 (void);
// 0x00000722 System.Void MoreMountains.CorgiEngine.PickableItem::Start()
extern void PickableItem_Start_mA869FA4809079960DFE30E24B97B084A52AB9F32 (void);
// 0x00000723 System.Void MoreMountains.CorgiEngine.PickableItem::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PickableItem_OnTriggerEnter2D_m5CE353B8DF7D5918EEE2FE7960711F688E08DCC0 (void);
// 0x00000724 System.Void MoreMountains.CorgiEngine.PickableItem::PickItem(UnityEngine.GameObject)
extern void PickableItem_PickItem_m2278A8F1E099E6A10570B9BE4BEF0E5B6737FCFC (void);
// 0x00000725 System.Boolean MoreMountains.CorgiEngine.PickableItem::CheckIfPickable()
extern void PickableItem_CheckIfPickable_mA0DD9891120BBA4D63313990C6F55A707A25A022 (void);
// 0x00000726 System.Void MoreMountains.CorgiEngine.PickableItem::Effects()
extern void PickableItem_Effects_mDAE97894A5C2912F6E10E306645EAF3E55E3CB53 (void);
// 0x00000727 System.Void MoreMountains.CorgiEngine.PickableItem::Pick()
extern void PickableItem_Pick_mE4E939C88A97354B97647F8E6484A02D07BF0084 (void);
// 0x00000728 System.Void MoreMountains.CorgiEngine.PickableItem::.ctor()
extern void PickableItem__ctor_m28A81D4FBEFCE32B10E6DDA43A87624CA4CE02A1 (void);
// 0x00000729 System.Boolean MoreMountains.CorgiEngine.PickableJetpack::CheckIfPickable()
extern void PickableJetpack_CheckIfPickable_m001207595C0BA100C53D398C47BBD6A19D6FD027 (void);
// 0x0000072A System.Void MoreMountains.CorgiEngine.PickableJetpack::Pick()
extern void PickableJetpack_Pick_mF7AA4E65304A3A9042BA80C159437C790B232369 (void);
// 0x0000072B System.Void MoreMountains.CorgiEngine.PickableJetpack::.ctor()
extern void PickableJetpack__ctor_m4C478F5E97A366C8EC1C919D9F3F400AD4CFBE12 (void);
// 0x0000072C System.Void MoreMountains.CorgiEngine.PickableOneUp::Pick()
extern void PickableOneUp_Pick_m59DB945B639B82F6AC08B1EDD2F09787024BE387 (void);
// 0x0000072D System.Void MoreMountains.CorgiEngine.PickableOneUp::.ctor()
extern void PickableOneUp__ctor_mC76F8C423605CEE04556563B613AB30923B401D0 (void);
// 0x0000072E System.Void MoreMountains.CorgiEngine.PickableWeapon::Pick()
extern void PickableWeapon_Pick_m4FCA044643B40DD1E5D709945DC7702CEFD46757 (void);
// 0x0000072F System.Boolean MoreMountains.CorgiEngine.PickableWeapon::CheckIfPickable()
extern void PickableWeapon_CheckIfPickable_mBA808421C2823B2E802D99E74285EB9CF9196086 (void);
// 0x00000730 System.Void MoreMountains.CorgiEngine.PickableWeapon::.ctor()
extern void PickableWeapon__ctor_mE11D9558785CE1A786EEA1FF6293EAB22DF842A9 (void);
// 0x00000731 System.Void MoreMountains.CorgiEngine.Star::Pick()
extern void Star_Pick_mB3A8DC54DDE573E7D153C5B21178EBE0F9F396B3 (void);
// 0x00000732 System.Void MoreMountains.CorgiEngine.Star::.ctor()
extern void Star__ctor_m778DAC91D16195F08F69B41C82CD752E3F82078B (void);
// 0x00000733 System.Void MoreMountains.CorgiEngine.Stimpack::Pick()
extern void Stimpack_Pick_m807D91CA7EBCBA55AF69D5AE1E8B58CF37241726 (void);
// 0x00000734 System.Void MoreMountains.CorgiEngine.Stimpack::.ctor()
extern void Stimpack__ctor_m4BE89DC444493B64640589705FBD1AC11908CDC9 (void);
// 0x00000735 System.Void MoreMountains.CorgiEngine.TimeModifier::Pick()
extern void TimeModifier_Pick_mECA14F5999E169D3E24BCD686D6E24442F910F98 (void);
// 0x00000736 System.Collections.IEnumerator MoreMountains.CorgiEngine.TimeModifier::ChangeTime()
extern void TimeModifier_ChangeTime_m4D116BBAAD7F8863BEA3F31F1E1E792F6BD18DE4 (void);
// 0x00000737 System.Void MoreMountains.CorgiEngine.TimeModifier::.ctor()
extern void TimeModifier__ctor_mD08DB9B282CBDBE5305BFF43B9B1B410201CC097 (void);
// 0x00000738 System.Void MoreMountains.CorgiEngine.TimeModifier/<ChangeTime>d__4::.ctor(System.Int32)
extern void U3CChangeTimeU3Ed__4__ctor_m202C97EFD11801DBE0BF4D3ED573965C953468CF (void);
// 0x00000739 System.Void MoreMountains.CorgiEngine.TimeModifier/<ChangeTime>d__4::System.IDisposable.Dispose()
extern void U3CChangeTimeU3Ed__4_System_IDisposable_Dispose_m7CB6E539B765CFC38719705B9F1935042A090F89 (void);
// 0x0000073A System.Boolean MoreMountains.CorgiEngine.TimeModifier/<ChangeTime>d__4::MoveNext()
extern void U3CChangeTimeU3Ed__4_MoveNext_m02C2911462BE2495A4F2DB92D4128040A8D77787 (void);
// 0x0000073B System.Object MoreMountains.CorgiEngine.TimeModifier/<ChangeTime>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeTimeU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m844658BC6CAB9EE89780F4FADE95307C10A90551 (void);
// 0x0000073C System.Void MoreMountains.CorgiEngine.TimeModifier/<ChangeTime>d__4::System.Collections.IEnumerator.Reset()
extern void U3CChangeTimeU3Ed__4_System_Collections_IEnumerator_Reset_m424FE3642AE7CE3372A5765A81664ABBEEF8B9D1 (void);
// 0x0000073D System.Object MoreMountains.CorgiEngine.TimeModifier/<ChangeTime>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CChangeTimeU3Ed__4_System_Collections_IEnumerator_get_Current_m37BD0AFE60F573543BAB1B72A075933E906B1DC9 (void);
// 0x0000073E System.Void MoreMountains.CorgiEngine.CharacterSwapManager::Start()
extern void CharacterSwapManager_Start_m62E78F3EC96D02234B59A781C8525177BC7B89DD (void);
// 0x0000073F System.Void MoreMountains.CorgiEngine.CharacterSwapManager::UpdateList()
extern void CharacterSwapManager_UpdateList_m1A2EF08CE80F405F3DC24C82162E6B9A4DE9C55F (void);
// 0x00000740 System.Int32 MoreMountains.CorgiEngine.CharacterSwapManager::SortSwapsByOrder(MoreMountains.CorgiEngine.CharacterSwap,MoreMountains.CorgiEngine.CharacterSwap)
extern void CharacterSwapManager_SortSwapsByOrder_m2120AE908C73D5B749D50FE9A3F2C9C9F89F2197 (void);
// 0x00000741 System.Void MoreMountains.CorgiEngine.CharacterSwapManager::Update()
extern void CharacterSwapManager_Update_mD880E8AB009EAFE33412983B8EA268538AD10E7C (void);
// 0x00000742 System.Void MoreMountains.CorgiEngine.CharacterSwapManager::HandleInput()
extern void CharacterSwapManager_HandleInput_m6219C4AAA56F1811CBB19A5833A99F289FAFFD17 (void);
// 0x00000743 System.Void MoreMountains.CorgiEngine.CharacterSwapManager::SwapCharacter()
extern void CharacterSwapManager_SwapCharacter_mB66084252BE960BDFB45B9DA29F329447E0BBB2B (void);
// 0x00000744 System.Void MoreMountains.CorgiEngine.CharacterSwapManager::.ctor()
extern void CharacterSwapManager__ctor_m22CD01BB4323C15492F6DEECD8B51E4A89C6DD2B (void);
// 0x00000745 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::ForceCharacterSwitch()
extern void CharacterSwitchManager_ForceCharacterSwitch_m9641E6B7DCB2182DC873665EC0B24F385EE460DE (void);
// 0x00000746 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::ForceCharacterSwitchTo(System.Int32)
extern void CharacterSwitchManager_ForceCharacterSwitchTo_m2A154823CF8AF3C70F10FE5C703D32A11634456E (void);
// 0x00000747 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::Awake()
extern void CharacterSwitchManager_Awake_m4228BF75C9EB636033A0C95BE346FA452285B1FA (void);
// 0x00000748 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::GetInputManager()
extern void CharacterSwitchManager_GetInputManager_m2AC6580E874F4033F560AEB86F830DCA6F597946 (void);
// 0x00000749 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::InstantiateCharacters()
extern void CharacterSwitchManager_InstantiateCharacters_mDA549F8D77137B18A263A2270CD4FFD869F2D5B7 (void);
// 0x0000074A System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::InstantiateVFX()
extern void CharacterSwitchManager_InstantiateVFX_mB45E61DA57BA89499B3125B46A87AF60393F04D2 (void);
// 0x0000074B System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::Update()
extern void CharacterSwitchManager_Update_m74022CA011710C0311B480B8099F95EA28C4E067 (void);
// 0x0000074C System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::DebugCharacterSwitchToTargetIndex()
extern void CharacterSwitchManager_DebugCharacterSwitchToTargetIndex_m23DBED3CFEDC8B92E3B520148F9AEA30E698E940 (void);
// 0x0000074D System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::DetermineNextIndex()
extern void CharacterSwitchManager_DetermineNextIndex_mAE8421FD66A4DA24D8E6F197D99E2BF0FC0839CF (void);
// 0x0000074E System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterSwitchManager::SwitchCharacter()
extern void CharacterSwitchManager_SwitchCharacter_m106021C6A83C821E16572628B56D56A04D4CF497 (void);
// 0x0000074F System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterSwitchManager::SwitchCharacter(System.Int32)
extern void CharacterSwitchManager_SwitchCharacter_m165E313F24974B7EDAD5E4B9E3B4C55430E62EBF (void);
// 0x00000750 System.Collections.IEnumerator MoreMountains.CorgiEngine.CharacterSwitchManager::OperateSwitch()
extern void CharacterSwitchManager_OperateSwitch_m78896B53CC6C76BB4028CD2EEF4BDF560210F150 (void);
// 0x00000751 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager::.ctor()
extern void CharacterSwitchManager__ctor_mD3ED61468D9A64E7ADADB8C92783A6EFA5EAB1AD (void);
// 0x00000752 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__24::.ctor(System.Int32)
extern void U3CSwitchCharacterU3Ed__24__ctor_m35E4E3379561072F083B906FC6C0D05B0851245A (void);
// 0x00000753 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__24::System.IDisposable.Dispose()
extern void U3CSwitchCharacterU3Ed__24_System_IDisposable_Dispose_m5F7FD5F397ECBB4C1BD983F08A1FABEF7B258F8B (void);
// 0x00000754 System.Boolean MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__24::MoveNext()
extern void U3CSwitchCharacterU3Ed__24_MoveNext_m342431D82BE8980CC774358DC02E4AE65AF00CF7 (void);
// 0x00000755 System.Object MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSwitchCharacterU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2E59D7795ADF632E09549DE34886D1E068A4B8A (void);
// 0x00000756 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__24::System.Collections.IEnumerator.Reset()
extern void U3CSwitchCharacterU3Ed__24_System_Collections_IEnumerator_Reset_m34089795EF3BA97D4F0BA86E0F00E69B542DCF6B (void);
// 0x00000757 System.Object MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CSwitchCharacterU3Ed__24_System_Collections_IEnumerator_get_Current_mBF3C45B67594C8D0754F5990D1DAC63636930911 (void);
// 0x00000758 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__25::.ctor(System.Int32)
extern void U3CSwitchCharacterU3Ed__25__ctor_mDB77914A75BFB4D697F234F8A7F6B5F5AFB26BD2 (void);
// 0x00000759 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__25::System.IDisposable.Dispose()
extern void U3CSwitchCharacterU3Ed__25_System_IDisposable_Dispose_mFF3091D4925F77B8632278667E1CB9EC628595BE (void);
// 0x0000075A System.Boolean MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__25::MoveNext()
extern void U3CSwitchCharacterU3Ed__25_MoveNext_mC20CFBD4D5C01F5A21DE0DCB622AC8AA6CBD249E (void);
// 0x0000075B System.Object MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSwitchCharacterU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B2D28B76FE01EAE8151ECFF1387C4801784EF23 (void);
// 0x0000075C System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__25::System.Collections.IEnumerator.Reset()
extern void U3CSwitchCharacterU3Ed__25_System_Collections_IEnumerator_Reset_m9FF46BD57256489A5D281182CEE5FB58DF984F8D (void);
// 0x0000075D System.Object MoreMountains.CorgiEngine.CharacterSwitchManager/<SwitchCharacter>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CSwitchCharacterU3Ed__25_System_Collections_IEnumerator_get_Current_mED9CDBA798F0E5523CEA093A72CE135F46A44EF8 (void);
// 0x0000075E System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<OperateSwitch>d__26::.ctor(System.Int32)
extern void U3COperateSwitchU3Ed__26__ctor_m0A96D9F8DB3BE2D83747558A115150B192F8F955 (void);
// 0x0000075F System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<OperateSwitch>d__26::System.IDisposable.Dispose()
extern void U3COperateSwitchU3Ed__26_System_IDisposable_Dispose_mCB0A4C57C7E9D1EC54C7DDBA8ED0EEF70C836262 (void);
// 0x00000760 System.Boolean MoreMountains.CorgiEngine.CharacterSwitchManager/<OperateSwitch>d__26::MoveNext()
extern void U3COperateSwitchU3Ed__26_MoveNext_m0B515FF18B2B82E0E4F37971073E439B5421CCA0 (void);
// 0x00000761 System.Object MoreMountains.CorgiEngine.CharacterSwitchManager/<OperateSwitch>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COperateSwitchU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2844504259EEE571F8FEE0740ADCDCB006107158 (void);
// 0x00000762 System.Void MoreMountains.CorgiEngine.CharacterSwitchManager/<OperateSwitch>d__26::System.Collections.IEnumerator.Reset()
extern void U3COperateSwitchU3Ed__26_System_Collections_IEnumerator_Reset_mD0623EDB3009CFBA076CD2307790FF8FFBB8962A (void);
// 0x00000763 System.Object MoreMountains.CorgiEngine.CharacterSwitchManager/<OperateSwitch>d__26::System.Collections.IEnumerator.get_Current()
extern void U3COperateSwitchU3Ed__26_System_Collections_IEnumerator_get_Current_m3B262E84A7C9560B54C85646969FB84DF3458AA0 (void);
// 0x00000764 System.Void MoreMountains.CorgiEngine.CorgiEngineEvent::.ctor(MoreMountains.CorgiEngine.CorgiEngineEventTypes)
extern void CorgiEngineEvent__ctor_m271C5C3FA61B0EBC5A1B763403A382C9A7B87029 (void);
// 0x00000765 System.Void MoreMountains.CorgiEngine.CorgiEngineEvent::Trigger(MoreMountains.CorgiEngine.CorgiEngineEventTypes)
extern void CorgiEngineEvent_Trigger_mB2F63DDFFCA55CD90EA578E552FBC063E561F633 (void);
// 0x00000766 System.Void MoreMountains.CorgiEngine.CorgiEngineStarEvent::.ctor(System.String,System.Int32)
extern void CorgiEngineStarEvent__ctor_mDE4FE213A164E3CFFE9D27FB421E145E4E88CF40 (void);
// 0x00000767 System.Void MoreMountains.CorgiEngine.CorgiEngineStarEvent::Trigger(System.String,System.Int32)
extern void CorgiEngineStarEvent_Trigger_m59BBB26D3FC2B68976371674FB4730350BE19E3F (void);
// 0x00000768 System.Void MoreMountains.CorgiEngine.CorgiEnginePointsEvent::.ctor(MoreMountains.CorgiEngine.PointsMethods,System.Int32)
extern void CorgiEnginePointsEvent__ctor_m4A569E9B13E1E553B656675A137DB2C0537F0200 (void);
// 0x00000769 System.Void MoreMountains.CorgiEngine.CorgiEnginePointsEvent::Trigger(MoreMountains.CorgiEngine.PointsMethods,System.Int32)
extern void CorgiEnginePointsEvent_Trigger_mE88B8E6726432F7D5F674D2057C173CABD647CC6 (void);
// 0x0000076A System.Void MoreMountains.CorgiEngine.PointsOfEntryStorage::.ctor(System.String,System.Int32,MoreMountains.CorgiEngine.Character/FacingDirections)
extern void PointsOfEntryStorage__ctor_m7C17224A561FAF565AA86FEFB829AAB89522C0C8 (void);
// 0x0000076B System.Int32 MoreMountains.CorgiEngine.GameManager::get_Points()
extern void GameManager_get_Points_mA6C2B3C7A921E385C0FD0B4F1CEA86A5933F133A (void);
// 0x0000076C System.Void MoreMountains.CorgiEngine.GameManager::set_Points(System.Int32)
extern void GameManager_set_Points_mB6BAC2DE32C446130A853C402DB23E782A2112CB (void);
// 0x0000076D System.Boolean MoreMountains.CorgiEngine.GameManager::get_Paused()
extern void GameManager_get_Paused_m9174A539CD970E639B908A04A435CABA34B42A9D (void);
// 0x0000076E System.Void MoreMountains.CorgiEngine.GameManager::set_Paused(System.Boolean)
extern void GameManager_set_Paused_m1D0BB494FB062E819B14B03D7F06032EB85EDF9E (void);
// 0x0000076F System.Boolean MoreMountains.CorgiEngine.GameManager::get_StoredLevelMapPosition()
extern void GameManager_get_StoredLevelMapPosition_m08B6869E7E9E4A0290804DE50C16976CC0E26F4E (void);
// 0x00000770 System.Void MoreMountains.CorgiEngine.GameManager::set_StoredLevelMapPosition(System.Boolean)
extern void GameManager_set_StoredLevelMapPosition_m5B58AFCCA29FDD58077A16E59D1A38246853AA67 (void);
// 0x00000771 UnityEngine.Vector2 MoreMountains.CorgiEngine.GameManager::get_LevelMapPosition()
extern void GameManager_get_LevelMapPosition_m28E4FCF400D62585F90B3D8D311510C4EF76BBF1 (void);
// 0x00000772 System.Void MoreMountains.CorgiEngine.GameManager::set_LevelMapPosition(UnityEngine.Vector2)
extern void GameManager_set_LevelMapPosition_m6F6B297ED7D9522CAD10244448B7F59C0587B1AC (void);
// 0x00000773 MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.GameManager::get_StoredCharacter()
extern void GameManager_get_StoredCharacter_mCC7662792DA0A1467CA2A4160AE311B56D2A0D25 (void);
// 0x00000774 System.Void MoreMountains.CorgiEngine.GameManager::set_StoredCharacter(MoreMountains.CorgiEngine.Character)
extern void GameManager_set_StoredCharacter_m22668F25A12C107D17E1961C4B73CE76AB9682EC (void);
// 0x00000775 MoreMountains.CorgiEngine.Character MoreMountains.CorgiEngine.GameManager::get_PersistentCharacter()
extern void GameManager_get_PersistentCharacter_m50680A37DDAA4C53558BCF87AF27FA2292DC1540 (void);
// 0x00000776 System.Void MoreMountains.CorgiEngine.GameManager::set_PersistentCharacter(MoreMountains.CorgiEngine.Character)
extern void GameManager_set_PersistentCharacter_m7F4059ECAD7FF8F6C3E3C6FB8140B802E42EDCE4 (void);
// 0x00000777 System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointsOfEntryStorage> MoreMountains.CorgiEngine.GameManager::get_PointsOfEntry()
extern void GameManager_get_PointsOfEntry_m9B3ABDC522D65A00ABF3ED0165CF7B9F5935E073 (void);
// 0x00000778 System.Void MoreMountains.CorgiEngine.GameManager::set_PointsOfEntry(System.Collections.Generic.List`1<MoreMountains.CorgiEngine.PointsOfEntryStorage>)
extern void GameManager_set_PointsOfEntry_mC3F482DFCDBDA6264B8F489AD77CC1435EB567AF (void);
// 0x00000779 System.Void MoreMountains.CorgiEngine.GameManager::Awake()
extern void GameManager_Awake_mE79699E7E898268E6C7231B39D1FD15104F7E864 (void);
// 0x0000077A System.Void MoreMountains.CorgiEngine.GameManager::Start()
extern void GameManager_Start_mB7A694BCBBB3B56646BF623255AD575180A56B37 (void);
// 0x0000077B System.Void MoreMountains.CorgiEngine.GameManager::Reset()
extern void GameManager_Reset_m6963D8CD865A8E48218EA9CD317A27FE15166373 (void);
// 0x0000077C System.Void MoreMountains.CorgiEngine.GameManager::LoseLife()
extern void GameManager_LoseLife_mB44A9A6FDCB455815A5F96D06F08DD76B7B1E842 (void);
// 0x0000077D System.Void MoreMountains.CorgiEngine.GameManager::GainLives(System.Int32)
extern void GameManager_GainLives_mDFF2B9D0405FB678F69FD320E3A0995EA7819364 (void);
// 0x0000077E System.Void MoreMountains.CorgiEngine.GameManager::AddLives(System.Int32,System.Boolean)
extern void GameManager_AddLives_mAACABF63506F5F1C16EBE3C6A3EA738ECDB07DE4 (void);
// 0x0000077F System.Void MoreMountains.CorgiEngine.GameManager::ResetLives()
extern void GameManager_ResetLives_mAC5A951E9918711F4D53AB6179718F4AEC4F8612 (void);
// 0x00000780 System.Void MoreMountains.CorgiEngine.GameManager::AddPoints(System.Int32)
extern void GameManager_AddPoints_mEC78F37F940318214328C48DB00C1DC7D1BEA599 (void);
// 0x00000781 System.Void MoreMountains.CorgiEngine.GameManager::SetPoints(System.Int32)
extern void GameManager_SetPoints_m7A69A40B9F6BD775D54CD012A0024983FBA8BC2D (void);
// 0x00000782 System.Void MoreMountains.CorgiEngine.GameManager::SetActiveInventoryInputManager(System.Boolean)
extern void GameManager_SetActiveInventoryInputManager_m473B4DE1B595AE783D706944B2B4015FE8CDAD99 (void);
// 0x00000783 System.Void MoreMountains.CorgiEngine.GameManager::Pause(MoreMountains.CorgiEngine.PauseMethods)
extern void GameManager_Pause_m387390E4465FA45C9FDA74B22905878365912B68 (void);
// 0x00000784 System.Void MoreMountains.CorgiEngine.GameManager::UnPause(MoreMountains.CorgiEngine.PauseMethods)
extern void GameManager_UnPause_m5D194DE3B834BC39699356AA0EE4F984D8180F8C (void);
// 0x00000785 System.Void MoreMountains.CorgiEngine.GameManager::ResetAllSaves()
extern void GameManager_ResetAllSaves_mFD981A6845A8A2DC1C33FE77206DAA557C5B0DD2 (void);
// 0x00000786 System.Void MoreMountains.CorgiEngine.GameManager::StorePointsOfEntry(System.String,System.Int32,MoreMountains.CorgiEngine.Character/FacingDirections)
extern void GameManager_StorePointsOfEntry_m6D8235A233A241D660FBEBACBE672C92C2C2AE42 (void);
// 0x00000787 MoreMountains.CorgiEngine.PointsOfEntryStorage MoreMountains.CorgiEngine.GameManager::GetPointsOfEntry(System.String)
extern void GameManager_GetPointsOfEntry_mA815C42B3127B4F213EF8B6AD3B99D211A1E9AE9 (void);
// 0x00000788 System.Void MoreMountains.CorgiEngine.GameManager::ClearPointOfEntry(System.String)
extern void GameManager_ClearPointOfEntry_m39712C670EDFBD3078BBE81F0557969641C8D352 (void);
// 0x00000789 System.Void MoreMountains.CorgiEngine.GameManager::ClearAllPointsOfEntry()
extern void GameManager_ClearAllPointsOfEntry_mECF481F4B4E44919DE04AC334AD51E70AE93717A (void);
// 0x0000078A System.Void MoreMountains.CorgiEngine.GameManager::SetPersistentCharacter(MoreMountains.CorgiEngine.Character)
extern void GameManager_SetPersistentCharacter_mE60B9578BB0BE60828E8914B76E8981E9745FE73 (void);
// 0x0000078B System.Void MoreMountains.CorgiEngine.GameManager::DestroyPersistentCharacter()
extern void GameManager_DestroyPersistentCharacter_mEACDEB0474A111344B76FBFA96047650779FFE07 (void);
// 0x0000078C System.Void MoreMountains.CorgiEngine.GameManager::StoreSelectedCharacter(MoreMountains.CorgiEngine.Character)
extern void GameManager_StoreSelectedCharacter_m41795391983404FA714537B1F55B20F0DDFB1ED5 (void);
// 0x0000078D System.Void MoreMountains.CorgiEngine.GameManager::ClearStoredCharacter()
extern void GameManager_ClearStoredCharacter_m4236A377E7D6DA85C29B015CB4163ABE70080D6A (void);
// 0x0000078E System.Void MoreMountains.CorgiEngine.GameManager::OnMMEvent(MoreMountains.Tools.MMGameEvent)
extern void GameManager_OnMMEvent_m17BB0A37BAA6D8F2582227A5F4A996D4B09DBC0C (void);
// 0x0000078F System.Void MoreMountains.CorgiEngine.GameManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void GameManager_OnMMEvent_mBB7088A7E550312216B19BE541352097B2DA0C0B (void);
// 0x00000790 System.Void MoreMountains.CorgiEngine.GameManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEnginePointsEvent)
extern void GameManager_OnMMEvent_m81C3CF93253446BDBB305CF836E1EAE0E9F68AC1 (void);
// 0x00000791 System.Void MoreMountains.CorgiEngine.GameManager::OnEnable()
extern void GameManager_OnEnable_mE7FFB42CC6620C31D8B6F32BB3B03348153A0F6B (void);
// 0x00000792 System.Void MoreMountains.CorgiEngine.GameManager::OnDisable()
extern void GameManager_OnDisable_m2BCB8A0BEBF0B831B5095AA2B7BE72C4E9F358BA (void);
// 0x00000793 System.Void MoreMountains.CorgiEngine.GameManager::.ctor()
extern void GameManager__ctor_m311129C0AC5254A2643BFC255E9E46A432C62ACA (void);
// 0x00000794 System.Void MoreMountains.CorgiEngine.ControlsModeEvent::.ctor(System.Boolean,MoreMountains.CorgiEngine.InputManager/MovementControls)
extern void ControlsModeEvent__ctor_mF0BFBFC090EFE50574B19BD5DE2F6C36ABB9F6E1 (void);
// 0x00000795 System.Void MoreMountains.CorgiEngine.ControlsModeEvent::Trigger(System.Boolean,MoreMountains.CorgiEngine.InputManager/MovementControls)
extern void ControlsModeEvent_Trigger_m14F7EABA84BA03A48DB4E55095D4FD9C74B53A69 (void);
// 0x00000796 System.Boolean MoreMountains.CorgiEngine.InputManager::get_IsMobile()
extern void InputManager_get_IsMobile_mD06485D9FB0011F9BB9F2F93716FD28CE192EC17 (void);
// 0x00000797 System.Void MoreMountains.CorgiEngine.InputManager::set_IsMobile(System.Boolean)
extern void InputManager_set_IsMobile_mE83696E34EDE7CB686DA2FA9FA43FBA04898CE1D (void);
// 0x00000798 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_JumpButton()
extern void InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641 (void);
// 0x00000799 System.Void MoreMountains.CorgiEngine.InputManager::set_JumpButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_JumpButton_mF0611CFC5D4DCEF678610865A4E9FEBC4D1900EE (void);
// 0x0000079A MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SwimButton()
extern void InputManager_get_SwimButton_m5519300F8D4DE3030F0DFA46C7B2E16D30BDBD29 (void);
// 0x0000079B System.Void MoreMountains.CorgiEngine.InputManager::set_SwimButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_SwimButton_mBF538BD96562770320DEE979019CC8FABA2ABA68 (void);
// 0x0000079C MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_GlideButton()
extern void InputManager_get_GlideButton_mE83F8C46DBFFCDAAE2DF29B3E6AA7662CD072CF1 (void);
// 0x0000079D System.Void MoreMountains.CorgiEngine.InputManager::set_GlideButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_GlideButton_m05EA60FE16119943431B3C44C61872A2318D9995 (void);
// 0x0000079E MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_InteractButton()
extern void InputManager_get_InteractButton_mE19955DDD59C98044E1D1A691ED12CB20CED0E22 (void);
// 0x0000079F System.Void MoreMountains.CorgiEngine.InputManager::set_InteractButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_InteractButton_mA9F148ED5D504E27F7BD3667815EA90BB6E59D1A (void);
// 0x000007A0 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_JetpackButton()
extern void InputManager_get_JetpackButton_mDD8A0EEEC4592EE09E2612A6B38097050BA4D5E1 (void);
// 0x000007A1 System.Void MoreMountains.CorgiEngine.InputManager::set_JetpackButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_JetpackButton_mAEB6C4DD90A8D0C15B8F2A9AA7E8089B59A2FC05 (void);
// 0x000007A2 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_FlyButton()
extern void InputManager_get_FlyButton_m8FE2143D47033FF22CF4537A6EEB6373A79AB0AE (void);
// 0x000007A3 System.Void MoreMountains.CorgiEngine.InputManager::set_FlyButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_FlyButton_m3834005F5963B7CAC79A1E5107A98BA35BED1091 (void);
// 0x000007A4 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_RunButton()
extern void InputManager_get_RunButton_m1BF0C918767106B3814D73F3550245C440035A66 (void);
// 0x000007A5 System.Void MoreMountains.CorgiEngine.InputManager::set_RunButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_RunButton_m0415BFBAEDC99B349F64229FF379C853AF1E03D0 (void);
// 0x000007A6 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_DashButton()
extern void InputManager_get_DashButton_m039788AFC1F68B651EF3179B1FF1739E0515225C (void);
// 0x000007A7 System.Void MoreMountains.CorgiEngine.InputManager::set_DashButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_DashButton_m6DEF997E0D9748D7010B0B9C1C87C38FAF0F9BE4 (void);
// 0x000007A8 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_RollButton()
extern void InputManager_get_RollButton_m24B6F0B7438ACF95C52AFA73BD4C28299A079F55 (void);
// 0x000007A9 System.Void MoreMountains.CorgiEngine.InputManager::set_RollButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_RollButton_m6A3EBFEFAD1C6DB711D7EF5314A3276AA975519D (void);
// 0x000007AA MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_GrabButton()
extern void InputManager_get_GrabButton_m35B3226127EA6466B0DC4DFF7DC4BF7989242BD4 (void);
// 0x000007AB System.Void MoreMountains.CorgiEngine.InputManager::set_GrabButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_GrabButton_mDC705CE610874D3B4824860C411CA20AC263EF4B (void);
// 0x000007AC MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_ThrowButton()
extern void InputManager_get_ThrowButton_m36E076F34EE4A98E8A26E301D3D8914BB52C66EA (void);
// 0x000007AD System.Void MoreMountains.CorgiEngine.InputManager::set_ThrowButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_ThrowButton_mFD1805CB173D31741E35FEA0CA1EF47D86AE5333 (void);
// 0x000007AE MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_ShootButton()
extern void InputManager_get_ShootButton_m67631ED77F704CF4C3B470F57B7649B399484B03 (void);
// 0x000007AF System.Void MoreMountains.CorgiEngine.InputManager::set_ShootButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_ShootButton_m74658C556B88698DCF7739AB997A7F1886A8092B (void);
// 0x000007B0 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SecondaryShootButton()
extern void InputManager_get_SecondaryShootButton_m089E50BE434BEEFD14863D19A45E8FD81099CFAA (void);
// 0x000007B1 System.Void MoreMountains.CorgiEngine.InputManager::set_SecondaryShootButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_SecondaryShootButton_m0AC0003444AF61A2E0092A0D8A81F466F57AB402 (void);
// 0x000007B2 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_ReloadButton()
extern void InputManager_get_ReloadButton_m42E2F7163F6D6D0A54BFB82AB63F5A5E9DC44E36 (void);
// 0x000007B3 System.Void MoreMountains.CorgiEngine.InputManager::set_ReloadButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_ReloadButton_mA722196BE6CAA90762EA238B0BA648AB5E38B16E (void);
// 0x000007B4 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_PushButton()
extern void InputManager_get_PushButton_mC45D8348FAB555B5AD423A4411B6629B42F3E8F7 (void);
// 0x000007B5 System.Void MoreMountains.CorgiEngine.InputManager::set_PushButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_PushButton_mAB6181CCBFC870DCB16A07852EEA86168C352E99 (void);
// 0x000007B6 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_GripButton()
extern void InputManager_get_GripButton_m411C2464701DF74A8E05104BBC8F07222EE408D9 (void);
// 0x000007B7 System.Void MoreMountains.CorgiEngine.InputManager::set_GripButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_GripButton_m290C218A7F8A6B551491F77746C0FF4DEE70C489 (void);
// 0x000007B8 MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_PauseButton()
extern void InputManager_get_PauseButton_mDA714172CF94F6397EC18BBD8A9CB1535BBDC1A5 (void);
// 0x000007B9 System.Void MoreMountains.CorgiEngine.InputManager::set_PauseButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_PauseButton_m01DEDDEEA3F0B3CBB35E6AC20F123002AC108C66 (void);
// 0x000007BA MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SwitchCharacterButton()
extern void InputManager_get_SwitchCharacterButton_mD19BFC6172D61F4E182C03EB13D961C86CB58A70 (void);
// 0x000007BB System.Void MoreMountains.CorgiEngine.InputManager::set_SwitchCharacterButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_SwitchCharacterButton_mF5E79FC292FAE059F2C462141486DDAA780D2ECE (void);
// 0x000007BC MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SwitchWeaponButton()
extern void InputManager_get_SwitchWeaponButton_m0290A37CB978A5F5CA6996B3E4B2D5CA961002F2 (void);
// 0x000007BD System.Void MoreMountains.CorgiEngine.InputManager::set_SwitchWeaponButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_SwitchWeaponButton_m4996A2354CE3EE2686AF74542AD4784A673E2174 (void);
// 0x000007BE MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_TimeControlButton()
extern void InputManager_get_TimeControlButton_m06F1083AE3A4A9BE95C3473B859C2FE9FB208461 (void);
// 0x000007BF System.Void MoreMountains.CorgiEngine.InputManager::set_TimeControlButton(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_set_TimeControlButton_m6C82676EDACF45214A733D34AC923363845DFACD (void);
// 0x000007C0 MoreMountains.Tools.MMInput/ButtonStates MoreMountains.CorgiEngine.InputManager::get_ShootAxis()
extern void InputManager_get_ShootAxis_m21A084BB2DCF4FF9A7D413501EFAD24A280B54A0 (void);
// 0x000007C1 System.Void MoreMountains.CorgiEngine.InputManager::set_ShootAxis(MoreMountains.Tools.MMInput/ButtonStates)
extern void InputManager_set_ShootAxis_m7811EEBFB6F6AA2FFB5FDCE18284132E2ECB1421 (void);
// 0x000007C2 MoreMountains.Tools.MMInput/ButtonStates MoreMountains.CorgiEngine.InputManager::get_SecondaryShootAxis()
extern void InputManager_get_SecondaryShootAxis_m2474EE4142A30C7B6B65F2AF5E1C149E94D92480 (void);
// 0x000007C3 System.Void MoreMountains.CorgiEngine.InputManager::set_SecondaryShootAxis(MoreMountains.Tools.MMInput/ButtonStates)
extern void InputManager_set_SecondaryShootAxis_m2277D295BF43B67CFF7B258DCDD6DF3300F11C09 (void);
// 0x000007C4 UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::get_PrimaryMovement()
extern void InputManager_get_PrimaryMovement_m67D3A20043621ACB7B8436051BB09487FFAC5DFD (void);
// 0x000007C5 UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::get_SecondaryMovement()
extern void InputManager_get_SecondaryMovement_mE4A326EEB872065F2DE44EF8F499C3A5BEC07B04 (void);
// 0x000007C6 System.Void MoreMountains.CorgiEngine.InputManager::Start()
extern void InputManager_Start_mC92AF37B2946CAF89B484EBD2DEE990B21629881 (void);
// 0x000007C7 System.Void MoreMountains.CorgiEngine.InputManager::Initialization()
extern void InputManager_Initialization_m1D51ADD53BEDAB595CB0819EB2EA164D0BD61C71 (void);
// 0x000007C8 System.Void MoreMountains.CorgiEngine.InputManager::ControlsModeDetection()
extern void InputManager_ControlsModeDetection_m0A7A6712F26EB0117842F4139F3632304FB938A8 (void);
// 0x000007C9 System.Void MoreMountains.CorgiEngine.InputManager::InitializeButtons()
extern void InputManager_InitializeButtons_m1DF29416B3E1FE85AFFCD927C39A46E7E2DD85B9 (void);
// 0x000007CA System.Void MoreMountains.CorgiEngine.InputManager::InitializeAxis()
extern void InputManager_InitializeAxis_mAD5CEDA5BA5FF7DE0E0BD41DE40F23C45DCC58AF (void);
// 0x000007CB System.Void MoreMountains.CorgiEngine.InputManager::LateUpdate()
extern void InputManager_LateUpdate_mD4FCA0A5BDF37427E5C0AF14A15E518DD9436DE8 (void);
// 0x000007CC System.Void MoreMountains.CorgiEngine.InputManager::Update()
extern void InputManager_Update_m98806EDC5F225A93DD117C744C5DF296CC6F4164 (void);
// 0x000007CD System.Void MoreMountains.CorgiEngine.InputManager::GetInputButtons()
extern void InputManager_GetInputButtons_mAB7863F012ECC7A283B3F977DAACE8AB759563C1 (void);
// 0x000007CE System.Void MoreMountains.CorgiEngine.InputManager::ProcessButtonStates()
extern void InputManager_ProcessButtonStates_m1AF619776329621E61CF5004F3BA65D720DC2C41 (void);
// 0x000007CF System.Collections.IEnumerator MoreMountains.CorgiEngine.InputManager::DelayButtonPress(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_DelayButtonPress_m723F39166B11B8D4AB46990842264D812C363876 (void);
// 0x000007D0 System.Collections.IEnumerator MoreMountains.CorgiEngine.InputManager::DelayButtonRelease(MoreMountains.Tools.MMInput/IMButton)
extern void InputManager_DelayButtonRelease_mE77481814D3679E4B2410A6BA769713315875256 (void);
// 0x000007D1 System.Void MoreMountains.CorgiEngine.InputManager::SetMovement()
extern void InputManager_SetMovement_m823A76758FE520DE5D745DAD290EA2C4FFB9D049 (void);
// 0x000007D2 System.Void MoreMountains.CorgiEngine.InputManager::SetSecondaryMovement()
extern void InputManager_SetSecondaryMovement_mAB537815A89158FBCA5D459049F76C883D8BB74A (void);
// 0x000007D3 System.Void MoreMountains.CorgiEngine.InputManager::SetShootAxis()
extern void InputManager_SetShootAxis_mEE160BE32D4D2229BC8C66F3BC2BCC33CD24A640 (void);
// 0x000007D4 System.Void MoreMountains.CorgiEngine.InputManager::SetMovement(UnityEngine.Vector2)
extern void InputManager_SetMovement_mABF31A3F0AE5C43FCAD3C94997E02248FAE16F06 (void);
// 0x000007D5 System.Void MoreMountains.CorgiEngine.InputManager::SetSecondaryMovement(UnityEngine.Vector2)
extern void InputManager_SetSecondaryMovement_m877926EE30286E52E4463E67EE1288A63EDCF6EF (void);
// 0x000007D6 System.Void MoreMountains.CorgiEngine.InputManager::SetHorizontalMovement(System.Single)
extern void InputManager_SetHorizontalMovement_m68444D881C2E129ACC6490686737D010E4EDA14E (void);
// 0x000007D7 System.Void MoreMountains.CorgiEngine.InputManager::SetVerticalMovement(System.Single)
extern void InputManager_SetVerticalMovement_m4E62B0D57FF2207FDC0447174AECF28E34FBAD70 (void);
// 0x000007D8 System.Void MoreMountains.CorgiEngine.InputManager::SetSecondaryHorizontalMovement(System.Single)
extern void InputManager_SetSecondaryHorizontalMovement_m1E47105772E78F9B8C8BEE0B7EB23883F6A3A02A (void);
// 0x000007D9 System.Void MoreMountains.CorgiEngine.InputManager::SetSecondaryVerticalMovement(System.Single)
extern void InputManager_SetSecondaryVerticalMovement_m133F06CB1FD4974ABC382CB4810CDE459EE14FD8 (void);
// 0x000007DA System.Void MoreMountains.CorgiEngine.InputManager::OnApplicationFocus(System.Boolean)
extern void InputManager_OnApplicationFocus_m06D4C28672DBDBE1898C510BFEE38EADAC477C63 (void);
// 0x000007DB System.Void MoreMountains.CorgiEngine.InputManager::JumpButtonDown()
extern void InputManager_JumpButtonDown_mBA46B77B5C71798D025862F6684F817CFABEF3CA (void);
// 0x000007DC System.Void MoreMountains.CorgiEngine.InputManager::JumpButtonPressed()
extern void InputManager_JumpButtonPressed_m6FDB0DE4AAD41F4BCE3CFD64EBA9453FB354CC50 (void);
// 0x000007DD System.Void MoreMountains.CorgiEngine.InputManager::JumpButtonUp()
extern void InputManager_JumpButtonUp_m573431B291398A22A4EA5145A9C48163E705194E (void);
// 0x000007DE System.Void MoreMountains.CorgiEngine.InputManager::SwimButtonDown()
extern void InputManager_SwimButtonDown_mDABE74C23188478E482A07A501121A85C8CF4AF6 (void);
// 0x000007DF System.Void MoreMountains.CorgiEngine.InputManager::SwimButtonPressed()
extern void InputManager_SwimButtonPressed_m071F6E25BB87BCEC5EE7DD8CF197F92A01A8FCE1 (void);
// 0x000007E0 System.Void MoreMountains.CorgiEngine.InputManager::SwimButtonUp()
extern void InputManager_SwimButtonUp_mC63E7A723ACB0EAF7DEA9799B4A4F793159B2341 (void);
// 0x000007E1 System.Void MoreMountains.CorgiEngine.InputManager::GlideButtonDown()
extern void InputManager_GlideButtonDown_mB5DEA1529E6F578DEB1ED438A731FFDA4536A331 (void);
// 0x000007E2 System.Void MoreMountains.CorgiEngine.InputManager::GlideButtonPressed()
extern void InputManager_GlideButtonPressed_mBDDE6066F18C9BC90E98F3ECDC632B8A3014EB9C (void);
// 0x000007E3 System.Void MoreMountains.CorgiEngine.InputManager::GlideButtonUp()
extern void InputManager_GlideButtonUp_m947EE05AC1602788E95C7F6BC61DA23DFB6ADB4D (void);
// 0x000007E4 System.Void MoreMountains.CorgiEngine.InputManager::InteractButtonDown()
extern void InputManager_InteractButtonDown_m20F969ED3DE95FEB2223262B8F5DE7342E947736 (void);
// 0x000007E5 System.Void MoreMountains.CorgiEngine.InputManager::InteractButtonPressed()
extern void InputManager_InteractButtonPressed_m015476D2345027BA6AC92B916B47F75E8DE67E30 (void);
// 0x000007E6 System.Void MoreMountains.CorgiEngine.InputManager::InteractButtonUp()
extern void InputManager_InteractButtonUp_m09306C95B86D75BBEFC3698058ED782ACC38661D (void);
// 0x000007E7 System.Void MoreMountains.CorgiEngine.InputManager::DashButtonDown()
extern void InputManager_DashButtonDown_m654D57B9C690DD5FCE66B156165561E5CDE4C09D (void);
// 0x000007E8 System.Void MoreMountains.CorgiEngine.InputManager::DashButtonPressed()
extern void InputManager_DashButtonPressed_m6E0C61616EC5FD5E9B629B8ED8486B0448681227 (void);
// 0x000007E9 System.Void MoreMountains.CorgiEngine.InputManager::DashButtonUp()
extern void InputManager_DashButtonUp_mC585988764745A5E95E4FC7E5D49EE967C40B2F1 (void);
// 0x000007EA System.Void MoreMountains.CorgiEngine.InputManager::RollButtonDown()
extern void InputManager_RollButtonDown_m22007CC1874984CE4C7C104553BE8C6D9B6FB8D6 (void);
// 0x000007EB System.Void MoreMountains.CorgiEngine.InputManager::RollButtonPressed()
extern void InputManager_RollButtonPressed_m29556619D7B5D52E5493F3B7CED80ECE7EDD5E43 (void);
// 0x000007EC System.Void MoreMountains.CorgiEngine.InputManager::RollButtonUp()
extern void InputManager_RollButtonUp_mBD23A39A76AFF9DA7C8F7535866EA49074C247F2 (void);
// 0x000007ED System.Void MoreMountains.CorgiEngine.InputManager::FlyButtonDown()
extern void InputManager_FlyButtonDown_m12F637B2B9A27EBC0D2FFC82975B91AF9C26E842 (void);
// 0x000007EE System.Void MoreMountains.CorgiEngine.InputManager::FlyButtonPressed()
extern void InputManager_FlyButtonPressed_m7B654BBB4DCC9911506F41E2C46224FE3EF9FB6A (void);
// 0x000007EF System.Void MoreMountains.CorgiEngine.InputManager::FlyButtonUp()
extern void InputManager_FlyButtonUp_m5B3DB98FD6CD2E271EB73C8D387A9AE720FB419D (void);
// 0x000007F0 System.Void MoreMountains.CorgiEngine.InputManager::RunButtonDown()
extern void InputManager_RunButtonDown_mAF1E7D1F1998F0A81C7BAA21C27BE301324AFB8D (void);
// 0x000007F1 System.Void MoreMountains.CorgiEngine.InputManager::RunButtonPressed()
extern void InputManager_RunButtonPressed_mFCB33D75203D0B8F337065988491F2720ADE2757 (void);
// 0x000007F2 System.Void MoreMountains.CorgiEngine.InputManager::RunButtonUp()
extern void InputManager_RunButtonUp_m0191DFFBBEAF1D0BBCBAB82AE99A5C03D750A529 (void);
// 0x000007F3 System.Void MoreMountains.CorgiEngine.InputManager::JetpackButtonDown()
extern void InputManager_JetpackButtonDown_m7F52CA3280C6BBE9AC1D831C3561B37AE3AFF343 (void);
// 0x000007F4 System.Void MoreMountains.CorgiEngine.InputManager::JetpackButtonPressed()
extern void InputManager_JetpackButtonPressed_mD3654E4118923978F4C6345C40DDE1CFEF9064C8 (void);
// 0x000007F5 System.Void MoreMountains.CorgiEngine.InputManager::JetpackButtonUp()
extern void InputManager_JetpackButtonUp_m6A62A9CEBEB32EF3760F0D2F1B3EC9A53C122E85 (void);
// 0x000007F6 System.Void MoreMountains.CorgiEngine.InputManager::ReloadButtonDown()
extern void InputManager_ReloadButtonDown_m08A76864F60F1F1793FB6B2E8EE3C372F7309867 (void);
// 0x000007F7 System.Void MoreMountains.CorgiEngine.InputManager::ReloadButtonPressed()
extern void InputManager_ReloadButtonPressed_mCFB151C091B9CEADF6070117C48F23A9E784A636 (void);
// 0x000007F8 System.Void MoreMountains.CorgiEngine.InputManager::ReloadButtonUp()
extern void InputManager_ReloadButtonUp_mC86B335275C8FC0E5B28421A7397C0C56CF54364 (void);
// 0x000007F9 System.Void MoreMountains.CorgiEngine.InputManager::PushButtonDown()
extern void InputManager_PushButtonDown_mF75A67242CF6B1F6E64F4FECA859FE23527C09C3 (void);
// 0x000007FA System.Void MoreMountains.CorgiEngine.InputManager::PushButtonPressed()
extern void InputManager_PushButtonPressed_mE61AB82EF201049A5DE24EF9FE2CF90173D2F397 (void);
// 0x000007FB System.Void MoreMountains.CorgiEngine.InputManager::PushButtonUp()
extern void InputManager_PushButtonUp_mFEFB8510884BC1BE39BD4404601EE45B66A53419 (void);
// 0x000007FC System.Void MoreMountains.CorgiEngine.InputManager::ShootButtonDown()
extern void InputManager_ShootButtonDown_mDD047441E4EA77A6CD96EFF348DE905DFF077DB9 (void);
// 0x000007FD System.Void MoreMountains.CorgiEngine.InputManager::ShootButtonPressed()
extern void InputManager_ShootButtonPressed_m961D763383FFDF4ECE0BC4EF656F0BB8D85C0ABA (void);
// 0x000007FE System.Void MoreMountains.CorgiEngine.InputManager::ShootButtonUp()
extern void InputManager_ShootButtonUp_mE52241598B28588D3317E2904D9A7817C2C312A4 (void);
// 0x000007FF System.Void MoreMountains.CorgiEngine.InputManager::GripButtonDown()
extern void InputManager_GripButtonDown_m45AD414784F8AEF804F4C22A107557C9DE05DAAA (void);
// 0x00000800 System.Void MoreMountains.CorgiEngine.InputManager::GripButtonPressed()
extern void InputManager_GripButtonPressed_m25C7AA25F7F908E81639D4903AE6F533A7A73331 (void);
// 0x00000801 System.Void MoreMountains.CorgiEngine.InputManager::GripButtonUp()
extern void InputManager_GripButtonUp_mEDDCD4AEA7498FCCEE1BC259F9CBC98EC3F1B91C (void);
// 0x00000802 System.Void MoreMountains.CorgiEngine.InputManager::SecondaryShootButtonDown()
extern void InputManager_SecondaryShootButtonDown_mD088DFA78718A9F28A962F86DE86075FCC632E63 (void);
// 0x00000803 System.Void MoreMountains.CorgiEngine.InputManager::SecondaryShootButtonPressed()
extern void InputManager_SecondaryShootButtonPressed_mF2E875589DAED34AF7E31463D0B90CEF51936239 (void);
// 0x00000804 System.Void MoreMountains.CorgiEngine.InputManager::SecondaryShootButtonUp()
extern void InputManager_SecondaryShootButtonUp_m124CDE2DD7A5EF98EF5487B3B47384AAFA1CE8BA (void);
// 0x00000805 System.Void MoreMountains.CorgiEngine.InputManager::PauseButtonDown()
extern void InputManager_PauseButtonDown_mFAB5620C7E508ED476743AB0AADAF1844FF36C5A (void);
// 0x00000806 System.Void MoreMountains.CorgiEngine.InputManager::PauseButtonPressed()
extern void InputManager_PauseButtonPressed_mC99F4CD24AF983B3E8CE78143D25AFC23AAC1345 (void);
// 0x00000807 System.Void MoreMountains.CorgiEngine.InputManager::PauseButtonUp()
extern void InputManager_PauseButtonUp_mABF827D36DD5ADFD867629174850425C6F1099A0 (void);
// 0x00000808 System.Void MoreMountains.CorgiEngine.InputManager::SwitchWeaponButtonDown()
extern void InputManager_SwitchWeaponButtonDown_m660CEBC61B3ED645DA805E7F32EEC2A4F0971AC6 (void);
// 0x00000809 System.Void MoreMountains.CorgiEngine.InputManager::SwitchWeaponButtonPressed()
extern void InputManager_SwitchWeaponButtonPressed_mCD9CBB9C11DEB7DD7EDB6326AF4598EE0954BB1B (void);
// 0x0000080A System.Void MoreMountains.CorgiEngine.InputManager::SwitchWeaponButtonUp()
extern void InputManager_SwitchWeaponButtonUp_m42E89A3BF9341DD25CF8F36D36278EADFD8D8754 (void);
// 0x0000080B System.Void MoreMountains.CorgiEngine.InputManager::SwitchCharacterButtonDown()
extern void InputManager_SwitchCharacterButtonDown_m38552D6DFB015AFA5D4A50CE3A86FE3C60875C41 (void);
// 0x0000080C System.Void MoreMountains.CorgiEngine.InputManager::SwitchCharacterButtonPressed()
extern void InputManager_SwitchCharacterButtonPressed_m092BE8CF9AA69962881ACCFCBCD1A9F563BEED4C (void);
// 0x0000080D System.Void MoreMountains.CorgiEngine.InputManager::SwitchCharacterButtonUp()
extern void InputManager_SwitchCharacterButtonUp_m64A551BDEB2A7593A4025F938E3E51B7632A8280 (void);
// 0x0000080E System.Void MoreMountains.CorgiEngine.InputManager::TimeControlButtonDown()
extern void InputManager_TimeControlButtonDown_m52E28E33B600B9D3622C6289EB7413000AE826F0 (void);
// 0x0000080F System.Void MoreMountains.CorgiEngine.InputManager::TimeControlButtonPressed()
extern void InputManager_TimeControlButtonPressed_mC1F30DB869A398A13EC40704703A0CA024EEB914 (void);
// 0x00000810 System.Void MoreMountains.CorgiEngine.InputManager::TimeControlButtonUp()
extern void InputManager_TimeControlButtonUp_m691E1F901247103D193A32401A7C11BB61A069E7 (void);
// 0x00000811 System.Void MoreMountains.CorgiEngine.InputManager::GrabButtonDown()
extern void InputManager_GrabButtonDown_m3AE7C86E252F08C4CA76EB7E1879A2324BFACD83 (void);
// 0x00000812 System.Void MoreMountains.CorgiEngine.InputManager::GrabButtonPressed()
extern void InputManager_GrabButtonPressed_m6978C95682DE47022A1E689443F6F52522E0B08F (void);
// 0x00000813 System.Void MoreMountains.CorgiEngine.InputManager::GrabButtonUp()
extern void InputManager_GrabButtonUp_m90089FFFA0DC24F2716D9A3039895AB5E3DB4853 (void);
// 0x00000814 System.Void MoreMountains.CorgiEngine.InputManager::ThrowButtonDown()
extern void InputManager_ThrowButtonDown_mFADAB06658616AC246245546DDC20A9A041070DD (void);
// 0x00000815 System.Void MoreMountains.CorgiEngine.InputManager::ThrowButtonPressed()
extern void InputManager_ThrowButtonPressed_m39C613CA1B8D7C429DEF4BD7D077723963EE1845 (void);
// 0x00000816 System.Void MoreMountains.CorgiEngine.InputManager::ThrowButtonUp()
extern void InputManager_ThrowButtonUp_mC3C1BF8F5073096F605844E722C0A0F078027903 (void);
// 0x00000817 System.Void MoreMountains.CorgiEngine.InputManager::.ctor()
extern void InputManager__ctor_m8C9F54C63E83C19DEAB926944E5CCC11FCD2EF78 (void);
// 0x00000818 System.Void MoreMountains.CorgiEngine.InputManager/<DelayButtonPress>d__126::.ctor(System.Int32)
extern void U3CDelayButtonPressU3Ed__126__ctor_mAC4CF5653FB48B7732171CD585161D8A70ACD119 (void);
// 0x00000819 System.Void MoreMountains.CorgiEngine.InputManager/<DelayButtonPress>d__126::System.IDisposable.Dispose()
extern void U3CDelayButtonPressU3Ed__126_System_IDisposable_Dispose_mC51D42389E6DF930F84B59C55756000DC0ABCCC0 (void);
// 0x0000081A System.Boolean MoreMountains.CorgiEngine.InputManager/<DelayButtonPress>d__126::MoveNext()
extern void U3CDelayButtonPressU3Ed__126_MoveNext_m4ADA1AF2668FDC2D6720B40B7C01B860FA05CBA1 (void);
// 0x0000081B System.Object MoreMountains.CorgiEngine.InputManager/<DelayButtonPress>d__126::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayButtonPressU3Ed__126_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23D7BE989362C5AEDE604A88801FF93B73E5E40E (void);
// 0x0000081C System.Void MoreMountains.CorgiEngine.InputManager/<DelayButtonPress>d__126::System.Collections.IEnumerator.Reset()
extern void U3CDelayButtonPressU3Ed__126_System_Collections_IEnumerator_Reset_m64C80CAA37B8744D75349DB5DA606F0E2C1CE3EF (void);
// 0x0000081D System.Object MoreMountains.CorgiEngine.InputManager/<DelayButtonPress>d__126::System.Collections.IEnumerator.get_Current()
extern void U3CDelayButtonPressU3Ed__126_System_Collections_IEnumerator_get_Current_mE20B5FC773311C807A1E36952CD0C273ACE67A14 (void);
// 0x0000081E System.Void MoreMountains.CorgiEngine.InputManager/<DelayButtonRelease>d__127::.ctor(System.Int32)
extern void U3CDelayButtonReleaseU3Ed__127__ctor_mD46DD939D600F2A822855CB7538AE66C4DCEDACD (void);
// 0x0000081F System.Void MoreMountains.CorgiEngine.InputManager/<DelayButtonRelease>d__127::System.IDisposable.Dispose()
extern void U3CDelayButtonReleaseU3Ed__127_System_IDisposable_Dispose_m6601669EA33E611957E8766EBC353C629A69A531 (void);
// 0x00000820 System.Boolean MoreMountains.CorgiEngine.InputManager/<DelayButtonRelease>d__127::MoveNext()
extern void U3CDelayButtonReleaseU3Ed__127_MoveNext_m48869728468B37310A76C82C4C7BA532C8028D97 (void);
// 0x00000821 System.Object MoreMountains.CorgiEngine.InputManager/<DelayButtonRelease>d__127::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayButtonReleaseU3Ed__127_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BCC5829459C931A182BD2FBD9E1D9CC14EF9627 (void);
// 0x00000822 System.Void MoreMountains.CorgiEngine.InputManager/<DelayButtonRelease>d__127::System.Collections.IEnumerator.Reset()
extern void U3CDelayButtonReleaseU3Ed__127_System_Collections_IEnumerator_Reset_m5DE2670AC5FC794627E1FF71B5AAF7B8B4C34936 (void);
// 0x00000823 System.Object MoreMountains.CorgiEngine.InputManager/<DelayButtonRelease>d__127::System.Collections.IEnumerator.get_Current()
extern void U3CDelayButtonReleaseU3Ed__127_System_Collections_IEnumerator_get_Current_mBFAFFECF5F685151959AB0B3524F846DE45A5752 (void);
// 0x00000824 System.Void MoreMountains.CorgiEngine.LayerManager::.cctor()
extern void LayerManager__cctor_m470D4EB01D68C7B4D23677187B2313518FD3603E (void);
// 0x00000825 System.Void MoreMountains.CorgiEngine.LevelNameEvent::.ctor(System.String)
extern void LevelNameEvent__ctor_mA505A3AAFBAA2FE2B3B975077CFE9635C6ACCDCD (void);
// 0x00000826 System.Void MoreMountains.CorgiEngine.LevelNameEvent::Trigger(System.String)
extern void LevelNameEvent_Trigger_mC787686ABDB2DED88363E8A01D83D1E27E8AEADC (void);
// 0x00000827 UnityEngine.Collider MoreMountains.CorgiEngine.LevelManager::get_BoundsCollider()
extern void LevelManager_get_BoundsCollider_mF941E9DDAD3B216DE55E1A7ECD29F1A3D349E922 (void);
// 0x00000828 System.Void MoreMountains.CorgiEngine.LevelManager::set_BoundsCollider(UnityEngine.Collider)
extern void LevelManager_set_BoundsCollider_m44E20473650E6AD06BDF66B22918BF97579199CE (void);
// 0x00000829 UnityEngine.Collider2D MoreMountains.CorgiEngine.LevelManager::get_BoundsCollider2D()
extern void LevelManager_get_BoundsCollider2D_m38E36A905EB6DF0E0B21758DDA955453641339B0 (void);
// 0x0000082A System.Void MoreMountains.CorgiEngine.LevelManager::set_BoundsCollider2D(UnityEngine.Collider2D)
extern void LevelManager_set_BoundsCollider2D_mBC2BBF7279D702EB3DAE3C6D63D1B009A6BA5AF6 (void);
// 0x0000082B System.TimeSpan MoreMountains.CorgiEngine.LevelManager::get_RunningTime()
extern void LevelManager_get_RunningTime_m9E9475EBB8686A0E1F2832A34A77E6F48F3E0828 (void);
// 0x0000082C MoreMountains.CorgiEngine.CameraController MoreMountains.CorgiEngine.LevelManager::get_LevelCameraController()
extern void LevelManager_get_LevelCameraController_m81BFB44DD7452381F90CE3F622A7FF33A5953152 (void);
// 0x0000082D System.Void MoreMountains.CorgiEngine.LevelManager::set_LevelCameraController(MoreMountains.CorgiEngine.CameraController)
extern void LevelManager_set_LevelCameraController_m5F06B18F9583BFDE427343793B96FBA1F1FCBC03 (void);
// 0x0000082E System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character> MoreMountains.CorgiEngine.LevelManager::get_Players()
extern void LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7 (void);
// 0x0000082F System.Void MoreMountains.CorgiEngine.LevelManager::set_Players(System.Collections.Generic.List`1<MoreMountains.CorgiEngine.Character>)
extern void LevelManager_set_Players_mA2DF40751ACA47C16085490346C62C1F6AEBA78E (void);
// 0x00000830 System.Collections.Generic.List`1<MoreMountains.CorgiEngine.CheckPoint> MoreMountains.CorgiEngine.LevelManager::get_Checkpoints()
extern void LevelManager_get_Checkpoints_m60A1768AC654A00CB7061A3A7E6E508B43606617 (void);
// 0x00000831 System.Void MoreMountains.CorgiEngine.LevelManager::set_Checkpoints(System.Collections.Generic.List`1<MoreMountains.CorgiEngine.CheckPoint>)
extern void LevelManager_set_Checkpoints_mF60E720FDAA9EFB922600772B12AC0D47BB4CD54 (void);
// 0x00000832 System.Void MoreMountains.CorgiEngine.LevelManager::Awake()
extern void LevelManager_Awake_m6663A05293EE49A8B761C2CC06E7839867BF3C6D (void);
// 0x00000833 System.Void MoreMountains.CorgiEngine.LevelManager::InstantiatePlayableCharacters()
extern void LevelManager_InstantiatePlayableCharacters_mB2ACB590651DB6C41E1D5D4513D9E88F65796751 (void);
// 0x00000834 System.Void MoreMountains.CorgiEngine.LevelManager::Start()
extern void LevelManager_Start_m606417779B17ECCDC0EE2BEBA335B3810E409676 (void);
// 0x00000835 System.Void MoreMountains.CorgiEngine.LevelManager::Initialization()
extern void LevelManager_Initialization_mDE8D6D1DF642695C7F2258230B2F564ECEC8955C (void);
// 0x00000836 System.Void MoreMountains.CorgiEngine.LevelManager::CheckpointAssignment()
extern void LevelManager_CheckpointAssignment_m4035FACE16ADE006C76696D1C2A073481B1B1F9D (void);
// 0x00000837 System.Void MoreMountains.CorgiEngine.LevelManager::LevelGUIStart()
extern void LevelManager_LevelGUIStart_m2CB604FBCF9B8A78F529F5BC7451FDABB29182D5 (void);
// 0x00000838 System.Void MoreMountains.CorgiEngine.LevelManager::SpawnSingleCharacter()
extern void LevelManager_SpawnSingleCharacter_m7388D91F3AA85EBDB4590353A26D67C0D0DF8495 (void);
// 0x00000839 System.Void MoreMountains.CorgiEngine.LevelManager::RegularSpawnSingleCharacter()
extern void LevelManager_RegularSpawnSingleCharacter_mA5302AC157AD878DC52105B8DFC077D04C8603E0 (void);
// 0x0000083A System.Void MoreMountains.CorgiEngine.LevelManager::SpawnMultipleCharacters()
extern void LevelManager_SpawnMultipleCharacters_m4B2312CEFD02A9482E4CC0E3105329099CF369E2 (void);
// 0x0000083B System.Void MoreMountains.CorgiEngine.LevelManager::SetCurrentCheckpoint(MoreMountains.CorgiEngine.CheckPoint)
extern void LevelManager_SetCurrentCheckpoint_m20D3DA12F673D300F1D826611BE92E491AC544D4 (void);
// 0x0000083C System.Void MoreMountains.CorgiEngine.LevelManager::SetNextLevel(System.String)
extern void LevelManager_SetNextLevel_mD95E12975C5C79462A32EAF795941F001989360C (void);
// 0x0000083D System.Void MoreMountains.CorgiEngine.LevelManager::GotoNextLevel()
extern void LevelManager_GotoNextLevel_m4B136E7FC13A8D165B7587E0197C7ED6F5314548 (void);
// 0x0000083E System.Void MoreMountains.CorgiEngine.LevelManager::GotoLevel(System.String,System.Boolean,System.Boolean)
extern void LevelManager_GotoLevel_mB68FBCB4780C290C60298DE5FFED11CAE4D3CB75 (void);
// 0x0000083F System.Collections.IEnumerator MoreMountains.CorgiEngine.LevelManager::GotoLevelCo(System.String,System.Boolean)
extern void LevelManager_GotoLevelCo_m20304615D717DDE00BE332CF051ECF95828AEB79 (void);
// 0x00000840 System.Void MoreMountains.CorgiEngine.LevelManager::KillPlayer(MoreMountains.CorgiEngine.Character)
extern void LevelManager_KillPlayer_m14BDFAE690266DBC75496EE70576660CEF09C968 (void);
// 0x00000841 System.Void MoreMountains.CorgiEngine.LevelManager::Cleanup()
extern void LevelManager_Cleanup_m4F358D708A18997242B8B0837DC02DBD004B423A (void);
// 0x00000842 System.Collections.IEnumerator MoreMountains.CorgiEngine.LevelManager::SoloModeRestart()
extern void LevelManager_SoloModeRestart_m5B0ECB363FE6DF71D246F99D6AAF286317DBF494 (void);
// 0x00000843 System.Void MoreMountains.CorgiEngine.LevelManager::FreezeCharacters()
extern void LevelManager_FreezeCharacters_m8D619F9AF7F4FF1EFE349ABE236686942390BB29 (void);
// 0x00000844 System.Void MoreMountains.CorgiEngine.LevelManager::UnFreezeCharacters()
extern void LevelManager_UnFreezeCharacters_m0B9EF058DEC38CF8164B484318C8834918489C74 (void);
// 0x00000845 System.Void MoreMountains.CorgiEngine.LevelManager::ToggleCharacterPause()
extern void LevelManager_ToggleCharacterPause_m2F9F02306FA5BF0033A54DF061E50CD2A09682EE (void);
// 0x00000846 System.Void MoreMountains.CorgiEngine.LevelManager::ResetLevelBoundsToOriginalBounds()
extern void LevelManager_ResetLevelBoundsToOriginalBounds_mC1FE8AEC43C5C04CAA92F2870D8CD7AF5187D461 (void);
// 0x00000847 System.Void MoreMountains.CorgiEngine.LevelManager::SetNewMinLevelBounds(UnityEngine.Vector3)
extern void LevelManager_SetNewMinLevelBounds_mD35B9E95E1FD2CC44BB8F848FE629601E4EE2EA2 (void);
// 0x00000848 System.Void MoreMountains.CorgiEngine.LevelManager::SetNewMaxLevelBounds(UnityEngine.Vector3)
extern void LevelManager_SetNewMaxLevelBounds_mE7E84DCC2E27B2DDB3CCB30F51911E9FA1DDFC24 (void);
// 0x00000849 System.Void MoreMountains.CorgiEngine.LevelManager::SetNewLevelBounds(UnityEngine.Bounds)
extern void LevelManager_SetNewLevelBounds_m5C1CDDCE8ED365A3752702CF69466EEDFB121B14 (void);
// 0x0000084A System.Void MoreMountains.CorgiEngine.LevelManager::UpdateBoundsCollider()
extern void LevelManager_UpdateBoundsCollider_mDE3576E4D6320BD5FFFFAA359B044744E1699573 (void);
// 0x0000084B System.Void MoreMountains.CorgiEngine.LevelManager::GenerateColliderBounds()
extern void LevelManager_GenerateColliderBounds_m71CB19B1F13861BA54698B282824F5BCE605C7E5 (void);
// 0x0000084C System.Void MoreMountains.CorgiEngine.LevelManager::.ctor()
extern void LevelManager__ctor_m9173267A7501E5FC7465F02A4475F62AF793E04C (void);
// 0x0000084D System.Void MoreMountains.CorgiEngine.LevelManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m7A5B85089DDA03F6A24E6C725C4464AE0D3D8E82 (void);
// 0x0000084E System.Void MoreMountains.CorgiEngine.LevelManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m80561866531FD9296EF176A1AF80830086873032 (void);
// 0x0000084F System.Single MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_0(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_0_m7852DD0D858FAEBB65EBD28059CF51D5F021C1AB (void);
// 0x00000850 System.Single MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_1(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_1_mA377F3F6166EB42C09ADB76EFCF63D6DCCF75C50 (void);
// 0x00000851 System.Single MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_2(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_2_m0C2001C2AC0F6D8C95CE3C7CF3F5CA469DF0DF9B (void);
// 0x00000852 System.Single MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_3(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_3_mCD9627595B0E51B1D7B4A25F4E2831A8034FBCB2 (void);
// 0x00000853 System.Single MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_4(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_4_mFA082451DFC33B2AE9C309704489C21777C9DBF7 (void);
// 0x00000854 System.Single MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_5(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_5_mC178C56A614CEB7F64673011CB6991C0FF1FEAF8 (void);
// 0x00000855 System.Int32 MoreMountains.CorgiEngine.LevelManager/<>c::<Initialization>b__53_6(MoreMountains.CorgiEngine.CheckPoint)
extern void U3CU3Ec_U3CInitializationU3Eb__53_6_m177590BCE789DCDAAB33119B640B6C1EE912AF23 (void);
// 0x00000856 System.Void MoreMountains.CorgiEngine.LevelManager/<GotoLevelCo>d__63::.ctor(System.Int32)
extern void U3CGotoLevelCoU3Ed__63__ctor_m54C25D3CC52247F6097BCF1D2398F0074A922299 (void);
// 0x00000857 System.Void MoreMountains.CorgiEngine.LevelManager/<GotoLevelCo>d__63::System.IDisposable.Dispose()
extern void U3CGotoLevelCoU3Ed__63_System_IDisposable_Dispose_mA252953C2F811BFCCD957B3CC301C9394272E934 (void);
// 0x00000858 System.Boolean MoreMountains.CorgiEngine.LevelManager/<GotoLevelCo>d__63::MoveNext()
extern void U3CGotoLevelCoU3Ed__63_MoveNext_mB22B77A03FB11F69B3BB1E539BE9722BC8268BEC (void);
// 0x00000859 System.Object MoreMountains.CorgiEngine.LevelManager/<GotoLevelCo>d__63::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGotoLevelCoU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m90C4C40BED85C61338E36D8567B6B98908AC4A79 (void);
// 0x0000085A System.Void MoreMountains.CorgiEngine.LevelManager/<GotoLevelCo>d__63::System.Collections.IEnumerator.Reset()
extern void U3CGotoLevelCoU3Ed__63_System_Collections_IEnumerator_Reset_m770A78158227D664C272C936D64986E88AD30912 (void);
// 0x0000085B System.Object MoreMountains.CorgiEngine.LevelManager/<GotoLevelCo>d__63::System.Collections.IEnumerator.get_Current()
extern void U3CGotoLevelCoU3Ed__63_System_Collections_IEnumerator_get_Current_mF4AD1F04E1AE9E994B1027B541D648ABE2C16949 (void);
// 0x0000085C System.Void MoreMountains.CorgiEngine.LevelManager/<SoloModeRestart>d__66::.ctor(System.Int32)
extern void U3CSoloModeRestartU3Ed__66__ctor_mC989B5660989A9C5CAC3455ED0DE021F7868BF5B (void);
// 0x0000085D System.Void MoreMountains.CorgiEngine.LevelManager/<SoloModeRestart>d__66::System.IDisposable.Dispose()
extern void U3CSoloModeRestartU3Ed__66_System_IDisposable_Dispose_mF2EF84CC336BA905F4EE93D71CE5275D5985FBC0 (void);
// 0x0000085E System.Boolean MoreMountains.CorgiEngine.LevelManager/<SoloModeRestart>d__66::MoveNext()
extern void U3CSoloModeRestartU3Ed__66_MoveNext_mA0018D2D66AC126D14C9B2F2B0526D31C6FB30C7 (void);
// 0x0000085F System.Object MoreMountains.CorgiEngine.LevelManager/<SoloModeRestart>d__66::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSoloModeRestartU3Ed__66_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32A94B3DAA4F9066742EEAEC9628A72301D3A95B (void);
// 0x00000860 System.Void MoreMountains.CorgiEngine.LevelManager/<SoloModeRestart>d__66::System.Collections.IEnumerator.Reset()
extern void U3CSoloModeRestartU3Ed__66_System_Collections_IEnumerator_Reset_m19B364D73ADFF729A1BE70721A88A228AF218724 (void);
// 0x00000861 System.Object MoreMountains.CorgiEngine.LevelManager/<SoloModeRestart>d__66::System.Collections.IEnumerator.get_Current()
extern void U3CSoloModeRestartU3Ed__66_System_Collections_IEnumerator_get_Current_m0591E492485FE51D1CEA9C057187B2DA66444A2D (void);
// 0x00000862 System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager::CheckMultiplayerEndGame()
extern void MultiplayerLevelManager_CheckMultiplayerEndGame_mB707BA9168A83F4AE542272C5D51565E2B4D0E9D (void);
// 0x00000863 System.Collections.IEnumerator MoreMountains.CorgiEngine.MultiplayerLevelManager::MultiplayerEndGame(System.String)
extern void MultiplayerLevelManager_MultiplayerEndGame_m6573546B15C81907C79A22050393F52E790E0019 (void);
// 0x00000864 System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager::KillPlayer(MoreMountains.CorgiEngine.Character)
extern void MultiplayerLevelManager_KillPlayer_mF878CF218D2EC6B0F7381CD3E258BAC6B7E18D78 (void);
// 0x00000865 System.Collections.IEnumerator MoreMountains.CorgiEngine.MultiplayerLevelManager::RemovePlayer(MoreMountains.CorgiEngine.Character)
extern void MultiplayerLevelManager_RemovePlayer_mA5B3BC884CDE86253BFA3D05207F47ED138F6A26 (void);
// 0x00000866 System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager::.ctor()
extern void MultiplayerLevelManager__ctor_m0147E74E961D2503F145E18997619068868A40C5 (void);
// 0x00000867 System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager/<MultiplayerEndGame>d__1::.ctor(System.Int32)
extern void U3CMultiplayerEndGameU3Ed__1__ctor_m2FD1F85559F2D52A7739D671283ED4C338886E38 (void);
// 0x00000868 System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager/<MultiplayerEndGame>d__1::System.IDisposable.Dispose()
extern void U3CMultiplayerEndGameU3Ed__1_System_IDisposable_Dispose_mB0747C8358903F67216A106AE7C0521D5963EA57 (void);
// 0x00000869 System.Boolean MoreMountains.CorgiEngine.MultiplayerLevelManager/<MultiplayerEndGame>d__1::MoveNext()
extern void U3CMultiplayerEndGameU3Ed__1_MoveNext_m7BF00B691FCBC3446748D2EE76D5F219A9EE274A (void);
// 0x0000086A System.Object MoreMountains.CorgiEngine.MultiplayerLevelManager/<MultiplayerEndGame>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMultiplayerEndGameU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20B173E8AC86C43F20E8B23A43145CAC95EA2AF8 (void);
// 0x0000086B System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager/<MultiplayerEndGame>d__1::System.Collections.IEnumerator.Reset()
extern void U3CMultiplayerEndGameU3Ed__1_System_Collections_IEnumerator_Reset_m43B353AE82D732015F2B9F78C54EE8E21A9A8081 (void);
// 0x0000086C System.Object MoreMountains.CorgiEngine.MultiplayerLevelManager/<MultiplayerEndGame>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CMultiplayerEndGameU3Ed__1_System_Collections_IEnumerator_get_Current_m2800EE7C4B6AFD4C35FBDDADAEDFA76E795E0707 (void);
// 0x0000086D System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager/<RemovePlayer>d__3::.ctor(System.Int32)
extern void U3CRemovePlayerU3Ed__3__ctor_m14C7E0979678D2948DFEDBB7394FF04C81C8C63A (void);
// 0x0000086E System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager/<RemovePlayer>d__3::System.IDisposable.Dispose()
extern void U3CRemovePlayerU3Ed__3_System_IDisposable_Dispose_mF5034DA0562D1571A7C20F3D7BCECF7C75303C5B (void);
// 0x0000086F System.Boolean MoreMountains.CorgiEngine.MultiplayerLevelManager/<RemovePlayer>d__3::MoveNext()
extern void U3CRemovePlayerU3Ed__3_MoveNext_m4BAD30513EE5C402AE8BB906ECE15FDB0DE25C98 (void);
// 0x00000870 System.Object MoreMountains.CorgiEngine.MultiplayerLevelManager/<RemovePlayer>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRemovePlayerU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m522AD5255BB066B43B63561D7900E7CA8670A948 (void);
// 0x00000871 System.Void MoreMountains.CorgiEngine.MultiplayerLevelManager/<RemovePlayer>d__3::System.Collections.IEnumerator.Reset()
extern void U3CRemovePlayerU3Ed__3_System_Collections_IEnumerator_Reset_m9D7B8803DB320B65B0B5CCAE8D76ED6290D50EFA (void);
// 0x00000872 System.Object MoreMountains.CorgiEngine.MultiplayerLevelManager/<RemovePlayer>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CRemovePlayerU3Ed__3_System_Collections_IEnumerator_get_Current_m28B25CBA4093A84DE18C6D8DAD112B04F18350F4 (void);
// 0x00000873 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::SetPreventGoingBack(System.Boolean)
extern void OneWayLevelManager_SetPreventGoingBack_m40881610A852D7AF8878133D9EFA09556B993EF7 (void);
// 0x00000874 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::SetOneWayLevelAutoScrolling(System.Boolean)
extern void OneWayLevelManager_SetOneWayLevelAutoScrolling_mFEB1C1B540872E749D49D830D9B543074227CA53 (void);
// 0x00000875 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::SetOneWayLevelAutoScrollingSpeed(System.Single)
extern void OneWayLevelManager_SetOneWayLevelAutoScrollingSpeed_m16E942BBBEB0D061DF1D3AF5706C0B5A3925164A (void);
// 0x00000876 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::Initialization()
extern void OneWayLevelManager_Initialization_m918161CF39826EC1C8A31B1D45A398B612CD23BD (void);
// 0x00000877 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::Update()
extern void OneWayLevelManager_Update_m707F58CBBD92450C8F55CB351E5A0A203894AD9A (void);
// 0x00000878 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::HandlePreventGoingBack()
extern void OneWayLevelManager_HandlePreventGoingBack_m2B526139BC6D6B5F7217BFDF02509E6F12250456 (void);
// 0x00000879 System.Void MoreMountains.CorgiEngine.OneWayLevelManager::HandleAutoMovement()
extern void OneWayLevelManager_HandleAutoMovement_mA60BFE478A58AD6B4073D3EBF1084612A9C9AE19 (void);
// 0x0000087A System.Void MoreMountains.CorgiEngine.OneWayLevelManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void OneWayLevelManager_OnMMEvent_m7EA1AEEAA57CFB59C1AC5BE7E37D08157347B3BA (void);
// 0x0000087B System.Void MoreMountains.CorgiEngine.OneWayLevelManager::OnEnable()
extern void OneWayLevelManager_OnEnable_mE91D8668768AD3BC8AEFA605AC84F28CB2AD4848 (void);
// 0x0000087C System.Void MoreMountains.CorgiEngine.OneWayLevelManager::OnDisable()
extern void OneWayLevelManager_OnDisable_m266A78FCCF297B31FBD77FD1643C4417EF2BBBD4 (void);
// 0x0000087D System.Void MoreMountains.CorgiEngine.OneWayLevelManager::.ctor()
extern void OneWayLevelManager__ctor_m3913B0760E00498C52367C59DBDFBE5FF7878B3E (void);
// 0x0000087E System.Void MoreMountains.CorgiEngine.ProximityManager::Start()
extern void ProximityManager_Start_m933E957AE61DE895F3B6C62691BEC533E46198A9 (void);
// 0x0000087F System.Void MoreMountains.CorgiEngine.ProximityManager::GrabControlledObjects()
extern void ProximityManager_GrabControlledObjects_m9946DB3A9FAD2725870CBD03EE0A731EC0656FC4 (void);
// 0x00000880 System.Void MoreMountains.CorgiEngine.ProximityManager::AddControlledObject(MoreMountains.CorgiEngine.ProximityManaged)
extern void ProximityManager_AddControlledObject_m7A388F048A231A858CE0B25228F20F3BDCFEC7BC (void);
// 0x00000881 System.Void MoreMountains.CorgiEngine.ProximityManager::SetPlayerAsTarget()
extern void ProximityManager_SetPlayerAsTarget_mB6A89CE4F2582D5E894E54258FB7E2247111C9AB (void);
// 0x00000882 System.Void MoreMountains.CorgiEngine.ProximityManager::Update()
extern void ProximityManager_Update_mD0EDB6FFFCA7110143CC0879687F6CCBD4585134 (void);
// 0x00000883 System.Void MoreMountains.CorgiEngine.ProximityManager::EvaluateDistance()
extern void ProximityManager_EvaluateDistance_m32DEE4E3FFFBD782F15FFC37670EFF2213B1BEC4 (void);
// 0x00000884 System.Void MoreMountains.CorgiEngine.ProximityManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void ProximityManager_OnMMEvent_mBB06A9BD84692C80A7444DE6C774265B8FEC594D (void);
// 0x00000885 System.Void MoreMountains.CorgiEngine.ProximityManager::OnEnable()
extern void ProximityManager_OnEnable_m73511187D113CE40E04A483C26C8A479B268C699 (void);
// 0x00000886 System.Void MoreMountains.CorgiEngine.ProximityManager::OnDisable()
extern void ProximityManager_OnDisable_mF7E20C464204672A73814F36720A6AB0B9E10086 (void);
// 0x00000887 System.Void MoreMountains.CorgiEngine.ProximityManager::.ctor()
extern void ProximityManager__ctor_m3E8C316C792B7243B323A3B0C8EB7AA452B2EED9 (void);
// 0x00000888 System.Void MoreMountains.CorgiEngine.SoundSettings::.ctor()
extern void SoundSettings__ctor_m74D825F974A0E3818DDDA6E9AF23B6B54B5BDBEE (void);
// 0x00000889 System.Void MoreMountains.CorgiEngine.SoundManager::PlayBackgroundMusic(UnityEngine.AudioSource)
extern void SoundManager_PlayBackgroundMusic_m7480100934A9A10DDE5551505C5421A1BF78058F (void);
// 0x0000088A UnityEngine.AudioSource MoreMountains.CorgiEngine.SoundManager::PlaySound(UnityEngine.AudioClip,UnityEngine.Vector3,System.Boolean)
extern void SoundManager_PlaySound_m6DA66D64E1BE804EFB8FD8564E95719BF1B2B905 (void);
// 0x0000088B UnityEngine.AudioSource MoreMountains.CorgiEngine.SoundManager::PlaySound(UnityEngine.AudioClip,UnityEngine.Vector3,System.Single,System.Single,System.Single,System.Single,System.Boolean,UnityEngine.AudioSource,UnityEngine.Audio.AudioMixerGroup)
extern void SoundManager_PlaySound_m52351C8F80B1934BB7F6ED8164F1E7D97B029A78 (void);
// 0x0000088C System.Void MoreMountains.CorgiEngine.SoundManager::StopLoopingSound(UnityEngine.AudioSource)
extern void SoundManager_StopLoopingSound_mA3C807031796D5ECFFCD4E858D586AB1900FE068 (void);
// 0x0000088D System.Void MoreMountains.CorgiEngine.SoundManager::SetMusic(System.Boolean)
extern void SoundManager_SetMusic_m3C6E6809B43CCEF5890647BB74DD128429684DE6 (void);
// 0x0000088E System.Void MoreMountains.CorgiEngine.SoundManager::SetSfx(System.Boolean)
extern void SoundManager_SetSfx_mB2759880AA04E7A806088BF4E185B170D12CFC00 (void);
// 0x0000088F System.Void MoreMountains.CorgiEngine.SoundManager::MusicOn()
extern void SoundManager_MusicOn_mF5D6EC78106A338922300569162A6640D14F2A6F (void);
// 0x00000890 System.Void MoreMountains.CorgiEngine.SoundManager::MusicOff()
extern void SoundManager_MusicOff_mBE8723A71F2D590B59BEA4CE91FA5A3DD3198B38 (void);
// 0x00000891 System.Void MoreMountains.CorgiEngine.SoundManager::SfxOn()
extern void SoundManager_SfxOn_m26469A260353484608AF4189FFB5BFDDE0BB135E (void);
// 0x00000892 System.Void MoreMountains.CorgiEngine.SoundManager::SfxOff()
extern void SoundManager_SfxOff_mBE76DFD942F418FA78C3FDD9FA50E4DCC0990640 (void);
// 0x00000893 System.Void MoreMountains.CorgiEngine.SoundManager::SaveSoundSettings()
extern void SoundManager_SaveSoundSettings_m951107F11B1AE635F8B8E9F154311A95188CBE8E (void);
// 0x00000894 System.Void MoreMountains.CorgiEngine.SoundManager::LoadSoundSettings()
extern void SoundManager_LoadSoundSettings_m26F288CD6350D80B73AEAFC600DD544285A3939A (void);
// 0x00000895 System.Void MoreMountains.CorgiEngine.SoundManager::ResetSoundSettings()
extern void SoundManager_ResetSoundSettings_m4F2026396638CD4E2DF1BDF246698AE7FA2A5381 (void);
// 0x00000896 System.Void MoreMountains.CorgiEngine.SoundManager::OnMMSfxEvent(UnityEngine.AudioClip,UnityEngine.Audio.AudioMixerGroup,System.Single,System.Single)
extern void SoundManager_OnMMSfxEvent_m8105170A03B322B9555E4C3D724FA9CC4B93BA6B (void);
// 0x00000897 System.Void MoreMountains.CorgiEngine.SoundManager::OnMMEvent(MoreMountains.Tools.MMGameEvent)
extern void SoundManager_OnMMEvent_m6EBD080A9DD1C55EA74A9FF7BFB4C7FFF81EA2FD (void);
// 0x00000898 System.Void MoreMountains.CorgiEngine.SoundManager::OnMMEvent(MoreMountains.CorgiEngine.CorgiEngineEvent)
extern void SoundManager_OnMMEvent_m8AB2A1EF39B5CD0B4E7EE8267464DDB479C58EF0 (void);
// 0x00000899 System.Void MoreMountains.CorgiEngine.SoundManager::MuteAllSfx()
extern void SoundManager_MuteAllSfx_m214A0097CF75C548955071D7CA7FB18EB22AD7E9 (void);
// 0x0000089A System.Void MoreMountains.CorgiEngine.SoundManager::UnmuteAllSfx()
extern void SoundManager_UnmuteAllSfx_m58D505875FB2D0E269A950E043F91D0F0708CADB (void);
// 0x0000089B System.Void MoreMountains.CorgiEngine.SoundManager::UnmuteBackgroundMusic()
extern void SoundManager_UnmuteBackgroundMusic_mB67C356A39B10F4BA6918EBCA0A1C48A341DA958 (void);
// 0x0000089C System.Void MoreMountains.CorgiEngine.SoundManager::MuteBackgroundMusic()
extern void SoundManager_MuteBackgroundMusic_mEB6C64A6C4ACA1DE9C1026A56B2A0CE466D101DD (void);
// 0x0000089D System.Void MoreMountains.CorgiEngine.SoundManager::OnEnable()
extern void SoundManager_OnEnable_m59E8D0568748FE6DDB27818A492A0A8230C89BC7 (void);
// 0x0000089E System.Void MoreMountains.CorgiEngine.SoundManager::OnDisable()
extern void SoundManager_OnDisable_mC321240BE759E3B6C7E8585962F62EE0D7892C83 (void);
// 0x0000089F System.Void MoreMountains.CorgiEngine.SoundManager::.ctor()
extern void SoundManager__ctor_m8CD89E3BF4DF30118A310A8EE7B8E9BCCB0EFC37 (void);
// 0x000008A0 System.Void MoreMountains.CorgiEngine.TilemapLevelGenerator::Awake()
extern void TilemapLevelGenerator_Awake_m65E588995EE3B36FAAEB3E1B6738809E7C7BD752 (void);
// 0x000008A1 System.Void MoreMountains.CorgiEngine.TilemapLevelGenerator::Generate()
extern void TilemapLevelGenerator_Generate_m1511614AAD0D47FDBBC6BE3330532791451A7A10 (void);
// 0x000008A2 System.Void MoreMountains.CorgiEngine.TilemapLevelGenerator::ResizeLevelManager()
extern void TilemapLevelGenerator_ResizeLevelManager_mCD299A9211F850E247CAC01DA1C4CA84ACF8C112 (void);
// 0x000008A3 System.Void MoreMountains.CorgiEngine.TilemapLevelGenerator::PlaceEntryAndExit()
extern void TilemapLevelGenerator_PlaceEntryAndExit_mAADBBDC1011DCF8380AA18471DA9F18B5CA55155 (void);
// 0x000008A4 System.Void MoreMountains.CorgiEngine.TilemapLevelGenerator::PlaceFloatingObjects()
extern void TilemapLevelGenerator_PlaceFloatingObjects_mF919C637BDE5F20E7A790600EC12424E0F895E0C (void);
// 0x000008A5 System.Void MoreMountains.CorgiEngine.TilemapLevelGenerator::.ctor()
extern void TilemapLevelGenerator__ctor_mF7EA39306A073843D62350CF9E14E30738A15928 (void);
// 0x000008A6 System.Void MoreMountains.CorgiEngine.BackgroundMusic::Start()
extern void BackgroundMusic_Start_mB5D8ED68407EC48A0B7AAA36126268D396B4B391 (void);
// 0x000008A7 System.Void MoreMountains.CorgiEngine.BackgroundMusic::.ctor()
extern void BackgroundMusic__ctor_m6C59FAC9058123FB7D9A2B9DE1F1C7F9E8CCCE00 (void);
// 0x000008A8 System.Void MoreMountains.CorgiEngine.AutoRespawn::Start()
extern void AutoRespawn_Start_m3C7733608EB514362F618ED2638793C5E50B03DB (void);
// 0x000008A9 System.Void MoreMountains.CorgiEngine.AutoRespawn::OnPlayerRespawn(MoreMountains.CorgiEngine.CheckPoint,MoreMountains.CorgiEngine.Character)
extern void AutoRespawn_OnPlayerRespawn_m8045F2226FF743E929A39B8D711FAA2F24E46A9C (void);
// 0x000008AA System.Void MoreMountains.CorgiEngine.AutoRespawn::Update()
extern void AutoRespawn_Update_mD42D92AF53947F15848601E12D2FE58727D372D8 (void);
// 0x000008AB System.Void MoreMountains.CorgiEngine.AutoRespawn::Kill()
extern void AutoRespawn_Kill_m05DD0D123B242AB38463CC78CD0B85CF6EBAB6E1 (void);
// 0x000008AC System.Void MoreMountains.CorgiEngine.AutoRespawn::Revive()
extern void AutoRespawn_Revive_mCA571251F8E82CDF313EA7959A3BF12B2BC7917E (void);
// 0x000008AD System.Void MoreMountains.CorgiEngine.AutoRespawn::.ctor()
extern void AutoRespawn__ctor_m2DE5FE2CDC1AD2B5449CA493CAB8D594DB97BFF5 (void);
// 0x000008AE System.Void MoreMountains.CorgiEngine.AutoRespawn/OnReviveDelegate::.ctor(System.Object,System.IntPtr)
extern void OnReviveDelegate__ctor_mB932216BFD1E603279DC64EEA152CB80B4D02EF4 (void);
// 0x000008AF System.Void MoreMountains.CorgiEngine.AutoRespawn/OnReviveDelegate::Invoke()
extern void OnReviveDelegate_Invoke_m5AE88D0C530285B224C29CEFB3D450D9EFBAD605 (void);
// 0x000008B0 System.IAsyncResult MoreMountains.CorgiEngine.AutoRespawn/OnReviveDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnReviveDelegate_BeginInvoke_mC2F811CE7856F2C070276C62FBD12BE564C106D9 (void);
// 0x000008B1 System.Void MoreMountains.CorgiEngine.AutoRespawn/OnReviveDelegate::EndInvoke(System.IAsyncResult)
extern void OnReviveDelegate_EndInvoke_m1F2F937FC7CB813F4E4DBABB3EE04109E1ADBE2B (void);
// 0x000008B2 System.Void MoreMountains.CorgiEngine.CharacterSelector::StoreCharacterSelection()
extern void CharacterSelector_StoreCharacterSelection_m7CAEF44E40213AD165462A6910466670E3053AB6 (void);
// 0x000008B3 System.Void MoreMountains.CorgiEngine.CharacterSelector::LoadNextScene()
extern void CharacterSelector_LoadNextScene_m056B6C8BC6C263931F50569994C2E2CA90D95EB5 (void);
// 0x000008B4 System.Void MoreMountains.CorgiEngine.CharacterSelector::.ctor()
extern void CharacterSelector__ctor_mB94BB67E790CD9083A3D4B3DD85008E4B0EFE875 (void);
// 0x000008B5 System.Void MoreMountains.CorgiEngine.CheckPoint::Awake()
extern void CheckPoint_Awake_mAE8CEDAFABEBF879CF0AEE8CB1AE9F85B3F95403 (void);
// 0x000008B6 System.Void MoreMountains.CorgiEngine.CheckPoint::SpawnPlayer(MoreMountains.CorgiEngine.Character)
extern void CheckPoint_SpawnPlayer_m0F1353E4BD7EE34F41524E9CECD4046396D7484B (void);
// 0x000008B7 System.Void MoreMountains.CorgiEngine.CheckPoint::AssignObjectToCheckPoint(MoreMountains.CorgiEngine.Respawnable)
extern void CheckPoint_AssignObjectToCheckPoint_m24BB28E388D9BBBB911930F0D10850111E06433B (void);
// 0x000008B8 System.Void MoreMountains.CorgiEngine.CheckPoint::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CheckPoint_OnTriggerEnter2D_m75BAEABB7E7FC92522255E8AAFE1078E07C96CAC (void);
// 0x000008B9 System.Void MoreMountains.CorgiEngine.CheckPoint::OnDrawGizmos()
extern void CheckPoint_OnDrawGizmos_m0D19398D6ED9E2C577A9E8242DA0129DDF6D0B13 (void);
// 0x000008BA System.Void MoreMountains.CorgiEngine.CheckPoint::.ctor()
extern void CheckPoint__ctor_mB43D6F915DF9F644DAA12AB3224971B3B4E5C9D1 (void);
// 0x000008BB System.Void MoreMountains.CorgiEngine.FinishLevel::Initialization()
extern void FinishLevel_Initialization_m22A453317168ACA7C69FD3C7B92AA7B091C5B534 (void);
// 0x000008BC System.Void MoreMountains.CorgiEngine.FinishLevel::TriggerButtonAction(UnityEngine.GameObject)
extern void FinishLevel_TriggerButtonAction_mC129DA187BBF045FBB38C782BF6DCE69A54BEA98 (void);
// 0x000008BD System.Collections.IEnumerator MoreMountains.CorgiEngine.FinishLevel::GoToNextLevelCoroutine()
extern void FinishLevel_GoToNextLevelCoroutine_m41BE1F809323D567B2932316FA8BBED5C485D61B (void);
// 0x000008BE System.Void MoreMountains.CorgiEngine.FinishLevel::GoToNextLevel()
extern void FinishLevel_GoToNextLevel_mB3160F319410C34441CCD519517A9DA4B63DB19E (void);
// 0x000008BF System.Void MoreMountains.CorgiEngine.FinishLevel::.ctor()
extern void FinishLevel__ctor_m01928DBC9628666ADFA3760A58298DC85566498F (void);
// 0x000008C0 System.Void MoreMountains.CorgiEngine.FinishLevel/<GoToNextLevelCoroutine>d__11::.ctor(System.Int32)
extern void U3CGoToNextLevelCoroutineU3Ed__11__ctor_m3D343D49C6472E9ED51658DA9B1E0B3CFB5A9C20 (void);
// 0x000008C1 System.Void MoreMountains.CorgiEngine.FinishLevel/<GoToNextLevelCoroutine>d__11::System.IDisposable.Dispose()
extern void U3CGoToNextLevelCoroutineU3Ed__11_System_IDisposable_Dispose_m75CAA28400D03CD10E1E404A32CA71548B9F16C5 (void);
// 0x000008C2 System.Boolean MoreMountains.CorgiEngine.FinishLevel/<GoToNextLevelCoroutine>d__11::MoveNext()
extern void U3CGoToNextLevelCoroutineU3Ed__11_MoveNext_m07275CD0B3040F46C80CF32E2D44987D49C5CC74 (void);
// 0x000008C3 System.Object MoreMountains.CorgiEngine.FinishLevel/<GoToNextLevelCoroutine>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGoToNextLevelCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7097FBEA4F979310809478774DEF9822109304BD (void);
// 0x000008C4 System.Void MoreMountains.CorgiEngine.FinishLevel/<GoToNextLevelCoroutine>d__11::System.Collections.IEnumerator.Reset()
extern void U3CGoToNextLevelCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_m7FB796138113BF05CA15FA90B9F86363501411D7 (void);
// 0x000008C5 System.Object MoreMountains.CorgiEngine.FinishLevel/<GoToNextLevelCoroutine>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CGoToNextLevelCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_mAA6E375544B528CE8BB013638E292BF99AF1C8E9 (void);
// 0x000008C6 System.Void MoreMountains.CorgiEngine.GoToLevelEntryPoint::GoToNextLevel()
extern void GoToLevelEntryPoint_GoToNextLevel_mBF5F0C7CAF1497956D471307280E6A7E160D64E5 (void);
// 0x000008C7 System.Void MoreMountains.CorgiEngine.GoToLevelEntryPoint::.ctor()
extern void GoToLevelEntryPoint__ctor_m80E1E6929251098AD07466A8C871B9753D4E97DF (void);
// 0x000008C8 System.Void MoreMountains.CorgiEngine.LevelRestarter::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void LevelRestarter_OnTriggerEnter2D_m9566117FE5366530ACB6BBFAE4972C1E63EC23C1 (void);
// 0x000008C9 System.Void MoreMountains.CorgiEngine.LevelRestarter::.ctor()
extern void LevelRestarter__ctor_m1B25336E2CCCA966C3394EAF034914252089029C (void);
// 0x000008CA System.Void MoreMountains.CorgiEngine.Respawnable::OnPlayerRespawn(MoreMountains.CorgiEngine.CheckPoint,MoreMountains.CorgiEngine.Character)
static Il2CppMethodPointer s_methodPointers[2250] = 
{
	MMDoubleSpriteMask_Awake_mD22C11996A351F9A0DB3718EAE5DBE229BFD6F96,
	MMDoubleSpriteMask_SwitchCurrentMask_m7A6D3FBA62EA1E25178EA7D2FBA586768C31B9CB,
	MMDoubleSpriteMask_DoubleMaskCo_mC730AF3628EAB697AF057F0B56064B86800491D8,
	MMDoubleSpriteMask_OnMMEvent_m923A0911227D7C433C67045AF5A4B6F3BF008B50,
	MMDoubleSpriteMask_OnEnable_m4D635305E9F1D52F5724B9BDD0FCDF28009F189F,
	MMDoubleSpriteMask_OnDisable_mCBB967A194ECF0113C698F60C506C480F682DA38,
	MMDoubleSpriteMask__ctor_mF718ADC7B7DC9C339608F902CC43E31862E31F53,
	U3CDoubleMaskCoU3Ed__6__ctor_m0DA458C1C8B07F4A74E543E395E7D94C2CF30C22,
	U3CDoubleMaskCoU3Ed__6_System_IDisposable_Dispose_mA2A205131EED731FFDF0DA621506360B0C58897E,
	U3CDoubleMaskCoU3Ed__6_MoveNext_m3375B37EFA2401720AC149FDA8EA65BC4CB6F3A6,
	U3CDoubleMaskCoU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0FAFAE69BF7CDF9E5AF1BDC4DAABC2E6E7C05629,
	U3CDoubleMaskCoU3Ed__6_System_Collections_IEnumerator_Reset_m78596805EF31746D05F8125B2EBD9D7A89651C4B,
	U3CDoubleMaskCoU3Ed__6_System_Collections_IEnumerator_get_Current_m86FE21B5D96DD0A60A91C815BED7CBF6FFAACAF6,
	MMSpriteMaskEvent__ctor_m6C3949E81BA53ADBF7BC370886DDFAAEFC6C40B7,
	MMSpriteMaskEvent_Trigger_m94383E73C7E743D4ED07BFBB86ABE48F62A9833D,
	MMSpriteMask_get_MaskTime_m7079335367802861AB675DF2613E9FC2058120F9,
	MMSpriteMask_Awake_m4E5B8EA4BCA4C67D0772B91F21CA689296BFBEDD,
	MMSpriteMask_SetupMaskSettingsAutomatically_m610DBC60D68E55A363DB158E9B0F168E043900AC,
	MMSpriteMask_MoveMaskTo_m0BC6E578BDF8D848BB6BC5D2C9C578E912C92C13,
	MMSpriteMask_ExpandAndMoveMaskTo_mD7002603025D24708F6FE4399EF02CF0E8A4C967,
	MMSpriteMask_MoveMaskToCoroutine_mC814E9FF0FD345224BE435B47F0DE1CC6727BEEF,
	MMSpriteMask_ExpandAndMoveMaskToCoroutine_m48F80242B8D61A00362DB235E20DE7F73A3B1764,
	MMSpriteMask_ComputeTargetPosition_mACEA838E818442E681C85A36190CA367B1706C2E,
	MMSpriteMask_ComputeTargetScale_m8E9B80ED0BDB3F7A6D8E059C771971D6D35C40B7,
	MMSpriteMask_OnMMEvent_m2B8CC9DD7983E1B0C512A90778FEBEC912A9AB08,
	MMSpriteMask_OnEnable_m562F5295165BC0C0E7EEE812B31B863870A8922B,
	MMSpriteMask_OnDisable_mB61235319B4AD641697EF8348366CA90F266BAC5,
	MMSpriteMask__ctor_m2CF0A584ECAF3E480BF1588F5D324E7C27FCA937,
	U3CMoveMaskToCoroutineU3Ed__20__ctor_mB779205F64ECBE4E56370E24746CC6CE1CC8EC50,
	U3CMoveMaskToCoroutineU3Ed__20_System_IDisposable_Dispose_mBACA27AB9F3A52D1661665E66535E431AACDF79B,
	U3CMoveMaskToCoroutineU3Ed__20_MoveNext_mCF215A10F260A3A710E623A091B768784A77F2C1,
	U3CMoveMaskToCoroutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m58B9BB5C06741B01341810503106721A1B9FD5DC,
	U3CMoveMaskToCoroutineU3Ed__20_System_Collections_IEnumerator_Reset_m7125C91AC5C6CCC6DBC11E50E8F96B60C6582580,
	U3CMoveMaskToCoroutineU3Ed__20_System_Collections_IEnumerator_get_Current_m8F25A19996D166AA61575A940832D7A9F3B3D8F2,
	U3CExpandAndMoveMaskToCoroutineU3Ed__21__ctor_m7BF8E015B683F8AC56F25FAAED7E4EDBAD6A18A1,
	U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_IDisposable_Dispose_m2E59E1F14B28431AB8767E92526CB1DDB313ACB6,
	U3CExpandAndMoveMaskToCoroutineU3Ed__21_MoveNext_m060ECB2E8F25859EF2FE234156CF5F332E48C86A,
	U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1B578450D00F744666A2E46965389B91E316369E,
	U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_Collections_IEnumerator_Reset_m9561E85AB2DD8E913626937862083845DED3E40D,
	U3CExpandAndMoveMaskToCoroutineU3Ed__21_System_Collections_IEnumerator_get_Current_m2823E34D90A48656AE04D801E28B8A1E0276DDB3,
	AchievementRules_OnMMEvent_mAAB3BE7277668870CA26B5977E0BFEDA6991157B,
	AchievementRules_OnMMEvent_mAA26A7A878581F137B604DD64DBF6872C3D5C989,
	AchievementRules_OnMMEvent_mCE4DD65178BE51592F96DB287008542EEC466F03,
	AchievementRules_OnMMEvent_m30B354CCC6ECE655591A463A403DCC6E76BD1978,
	AchievementRules_OnMMEvent_m9861E0C2E7344BA6F8B123C492B15B0EA44CADCF,
	AchievementRules_OnMMEvent_mDC4BD4E5F22A7C06EBA3BBC3CEE255D639BE491D,
	AchievementRules_OnEnable_mE580E4B8E72A5088EA4C88523D9CCD0A7C1132CC,
	AchievementRules_OnDisable_mECAE33EC0AD4C08BC40119FA0D8D875ADC23FA7C,
	AchievementRules__ctor_mBB50E1369EDC16EEE5F7A78F05735B369D0B6B45,
	AIActionChangeWeapon_Initialization_mEF376B72339FC8FE6911AED6ECE87C3DE3A8237E,
	AIActionChangeWeapon_PerformAction_m4BBB85DE7F51669117BAC75F6F51D707BC8ABBE8,
	AIActionChangeWeapon_ChangeWeapon_m7B58694B5F7331031F9A2D7F6DF3AA195C15355D,
	AIActionChangeWeapon_OnEnterState_mD38A0538BD2C0ABDE5B85BDBD4FECAF174AA809A,
	AIActionChangeWeapon__ctor_m8C1780366D61220AA320D2CE6D0D781EFA60E6C3,
	AIActionDash_Initialization_m67168AB154AC75EAF972EB4A80080805C6F5CA86,
	AIActionDash_PerformAction_m2051D7479AA6911E506B68C1A764CE3D0F78E34D,
	AIActionDash_Dash_m71A73160DD3AFF631F3FAFF1604F254820D89758,
	AIActionDash__ctor_m85BE527F8C45C66C9C56FC6071886F1B3FD9AAC7,
	AIActionDoNothing_PerformAction_m731C067D8AD6E93FDFC43AD830FE9C85D93E98B2,
	AIActionDoNothing__ctor_m8155A8889277A226EA92F1924EC64DDFAFE572F5,
	AIActionFlyPatrol_Initialization_m7C372147D2230268BBFFE40ED8860513D8357289,
	AIActionFlyPatrol_PerformAction_mD1BCF1834E26DB349E040949179DAD022D3580D3,
	AIActionFlyPatrol_FlyPatrol_mB5BEC7546B52F5A42E6C24D65D40E2DA83D50322,
	AIActionFlyPatrol_OnDrawGizmosSelected_m4FA344C333303581379CB3D96FB65009A38C9546,
	AIActionFlyPatrol_OnExitState_m492B259BFC80100B2A65E8CB0ACC39035DE5FA10,
	AIActionFlyPatrol_CheckForObstacles_m3AD419F99758CE66531C20F739BB1FAAB6E8BCA4,
	AIActionFlyPatrol_ChangeDirection_mFBD5A50150703228451C9328DCCAD412B9763F8B,
	AIActionFlyPatrol_OnRevive_mF26AB2A2A9046A93124E6E85C38A1739E0766670,
	AIActionFlyPatrol_OnEnable_mDF33D8F33E0AEE559553D65E010D520CA29DD600,
	AIActionFlyPatrol_OnDisable_mE9011E52FD1FB277E1B9EDFE441E6BA0992383FF,
	AIActionFlyPatrol__ctor_m4DB6B7487B53DC36985FD22582019F1A6F9C176F,
	AIActionFlyTowardsTarget_Initialization_m08BE3BC6FFEC43D11D650755EEB7F883ADA58383,
	AIActionFlyTowardsTarget_PerformAction_m40C5DA21CF947834BE599B3C118CFEABC991E03B,
	AIActionFlyTowardsTarget_Fly_m95D9BFF47E5AF88AB6FD2DE7620B28CCBBC38A33,
	AIActionFlyTowardsTarget_OnExitState_m87280E76CF18399F76300044E7B6B3043F9E4A90,
	AIActionFlyTowardsTarget__ctor_mE831C418DD05222E46FF245A11FE95052C6D890F,
	AIActionJump_Initialization_m4435BD6EEF6A7A356B52FCB90020018BC1FD5588,
	AIActionJump_PerformAction_m9AEA983421200D8CF5A39558A01213DB150EE904,
	AIActionJump_Jump_mAA139D93A6CAEB170E6E1B1FF9C1E332190E6D34,
	AIActionJump_OnEnterState_m46AF624791EB5349D25A3778189FD6E24A0A1C20,
	AIActionJump__ctor_mFFA924E0BC6F79E7A79C44451DFB765655149564,
	AIActionMMFeedbacks_PerformAction_m2A36870E3A8A02EA89BBA7438244FAFF15B05EF7,
	AIActionMMFeedbacks_PlayFeedbacks_mAD362AA3903E74C23EF1398364AEFFE2F04F6F75,
	AIActionMMFeedbacks_OnEnterState_m172F8DC4DD5059746628A0944AEFEDCFA2C5B153,
	AIActionMMFeedbacks__ctor_mF98ABC7238855099ED7856DEF358BF4148D49A55,
	AIActionMoveAwayFromTarget_Move_m905276FAC4F8BCC6551438411D1776EF47756F65,
	AIActionMoveAwayFromTarget__ctor_mAD62A578D4F4B971BB3ADE3B053216F8E31E3C4F,
	AIActionMoveTowardsTarget_Initialization_m21B3343C877DE69BA3E9BF05EE002AB000E3F839,
	AIActionMoveTowardsTarget_PerformAction_m58D83A5C44A805C45FAC3DAF5EBF869A9D032727,
	AIActionMoveTowardsTarget_Move_mD4386DD500AF8173B90BFF86E3623CFCE1EA542E,
	AIActionMoveTowardsTarget_OnEnterState_m202B344BDCBC983495EDBC536B1C3B660B3FA3D7,
	AIActionMoveTowardsTarget_OnExitState_m335F87618B2EF710DA25B1C4B386C2592DB80DF1,
	AIActionMoveTowardsTarget__ctor_m4B07391F366B6B1C45083E4860AC9607585BABA9,
	AIActionPatrol_Initialization_m2DC85D6A11ADCBB75EABF874B08F8C6B8427BF94,
	AIActionPatrol_PerformAction_mCA5A883FBF5B8387B68F63268F7DB38DD96A6718,
	AIActionPatrol_Patrol_m40ABC968F8B27B46AD4F6A9044E67EC1239D6A57,
	AIActionPatrol_OnExitState_m43D4852B5FA3038107F208937845E5181EB0A230,
	AIActionPatrol_CheckForWalls_mEA151C8177E1FC5B5C747979CD944AC36883EB9D,
	AIActionPatrol_DetectObstaclesRegularLayermask_mAFFB31595F1761285C1FD5AD8E539988FE7B540C,
	AIActionPatrol_DetectObstaclesCustomLayermask_m2CCA2C1BFB58CE40FADA4727A30C9D849495F85A,
	AIActionPatrol_CheckForHoles_m909F98127B833E3365E3A9F32A374737595C8510,
	AIActionPatrol_ChangeDirection_m73EBF976104F57BC74E4672B9CD2DFD29CC22993,
	AIActionPatrol_OnRevive_m480E2E2D56D8E15573FDDB9658E2E326BC4C0F91,
	AIActionPatrol_OnEnable_m35B147C245498DC1FE4A537BD59FBA8C08FA61BB,
	AIActionPatrol_OnDisable_m85E7176E310B2BF9702D4D26554E921172BD596F,
	AIActionPatrol__ctor_m9C1AEA53F1BD15F5EFEACE80EE1D81E0B77C2F09,
	AIActionPatrolWithinBounds_Initialization_m916DBAA327E6E6371A6CB7D42042B6C71FFAFA2B,
	AIActionPatrolWithinBounds_Patrol_mB94C7A94CD02CD3FAA8701271E6FAF873B458A6D,
	AIActionPatrolWithinBounds_CheckForDistance_m74CCBDE6FAF4CC36FB6F752EE9EA96ACA45454B5,
	AIActionPatrolWithinBounds_EstablishBounds_mDE4F6527FD7D2CB50AC79BEEA07E242D3C4D16BD,
	AIActionPatrolWithinBounds_OnEnterState_m125533EF4CBA659319F3C79E0FFEB52ACC40C0FB,
	AIActionPatrolWithinBounds_OnExitState_m7665BFC1C488D1922E86480BA3A2A01C24B9ADE9,
	AIActionPatrolWithinBounds_OnDrawGizmosSelected_m9C2254729BFE29AD86F65AB2499580C02340F521,
	AIActionPatrolWithinBounds__ctor_m5DBDD38C771134F715054262CEB23D6508DFA763,
	AIActionReload_Initialization_m966C1ECE3EDE29C55B3642CEF65A31225EAF2DB3,
	AIActionReload_PerformAction_m3B65877626CF619EF11D8BA821133C107971472C,
	AIActionReload_OnEnterState_mFC4341B16A1C832BD406D16E89DCA10EEA9079C3,
	AIActionReload__ctor_m12FB874DD8B78D5A520E248557DA219F0B5B93F5,
	AIActionResetTarget_PerformAction_mB8CEDC3324E1F99C82782480FEF5227B9DE39B4E,
	AIActionResetTarget__ctor_mCBB8AFEE51E9072CF31341733EE74D598B7AE6CF,
	AIActionSelfDestruct_Initialization_mACC0AD633AA9A81E3FD82C4A8249938E3FD4F6AA,
	AIActionSelfDestruct_PerformAction_m12DC7141407F65DF9BD5E8D18495FE9981734B0E,
	AIActionSelfDestruct_OnEnterState_m47A1521E5C1800B871CEE04EBA63F635262EC52F,
	AIActionSelfDestruct__ctor_m86E2FAFCDDBA6486765F5425153E9EF0816F5AAD,
	AIActionSetLastKnownPositionAsTarget_Initialization_mCA16C6799DAABB53CBC4FDC9E9334C3F081A8708,
	AIActionSetLastKnownPositionAsTarget_PerformAction_m2DC213E2F7775BD553539FF9240FD5A92CAE77E2,
	AIActionSetLastKnownPositionAsTarget__ctor_m22F1237F250DDA6D881EEB31E5AD10936B191F1A,
	AIActionShoot_Initialization_mCFEDCBC4152CA764D3448995ED4A6F903012984F,
	AIActionShoot_PerformAction_mAE66A6C7BA3104000842D5322F0744B94F6B8B18,
	AIActionShoot_Update_mE13CD84B0A1BF788089688E3DACC146F77941951,
	AIActionShoot_TestFaceTarget_mC9C357689AB3646FB16B06B69AEA19AF91E0CBD2,
	AIActionShoot_TestAimAtTarget_mA2B1D82B22BBE30927C748736F7B7743BDC970A7,
	AIActionShoot_Shoot_mFE3AC0A72D3521080F2E1968D612F1C8384F72A9,
	AIActionShoot_OnEnterState_m4990EFFBA382D1ADC9F922A23B174C61BE7609DC,
	AIActionShoot_OnExitState_m8FBACE9A4F5D27A5D1BC44375C4BD86A017397B6,
	AIActionShoot__ctor_m1F327CBA151E0CC52542F894AAD13A0DFD19F3F1,
	AIActionSwapBrain_Initialization_mD781D38EF6BEB09FAB61D681F9D333D61F825817,
	AIActionSwapBrain_PerformAction_m9F6041404D97264A7011A625A1DD9BD119B4E1E3,
	AIActionSwapBrain_SwapBrain_m357564D7FA0C4A4A6DF44A1422FDFDD43D78D62D,
	AIActionSwapBrain__ctor_m01F1CE2EA678069ED74E7C6D0A2DE6A6E96078D3,
	AIActionUnityEvents_PerformAction_m6F2B7C190D2DF89C118DE8DA523E5ECB4DC56E88,
	AIActionUnityEvents_TriggerEvent_m5190D7415E3F7ECD6615E49099FB88A2F8CE3154,
	AIActionUnityEvents_OnEnterState_mCB57AC7D6F768BC71CD16E5D733BADBE9CAE7630,
	AIActionUnityEvents__ctor_m8BA2795BF85E46643E2765E702E00451675D5863,
	AIDecisionDetectTargetLine_Initialization_m77957B482806E921576BC4CB032123947F33D5E6,
	AIDecisionDetectTargetLine_Decide_mC3043E02EBCDAEA016A6659673D2545DAEA360E0,
	AIDecisionDetectTargetLine_DetectTarget_mE871451FEADC1A54E6E8D916CC13FDBA3F1058E5,
	AIDecisionDetectTargetLine_OnDrawGizmos_mFA3A6F6A41649A0B143F1FE99A77B09B101D733F,
	AIDecisionDetectTargetLine__ctor_m7ED62F0FFDDC1D3CCE79F0A8F94B3CE52E7614CC,
	AIDecisionDetectTargetRadius_Initialization_mB536D601614C7C1AC7A3E655008CA258C05CA07D,
	AIDecisionDetectTargetRadius_Decide_m1C18976840A9B819D2D0E3A0C32909BD5ABAFE00,
	AIDecisionDetectTargetRadius_DetectTarget_m922B50B17E4491FA13923CAA62C8CB62E351267E,
	AIDecisionDetectTargetRadius_OnDrawGizmosSelected_m81B3E0532C1CABC673988BC3328A9A7F75CA591E,
	AIDecisionDetectTargetRadius__ctor_mC92DA45C31601002C709B4A3F48AE44AA8B3EB9D,
	AIDecisionDistanceToTarget_Decide_m417362C1AAF557BF18BD5C69231A0B8A191086EF,
	AIDecisionDistanceToTarget_EvaluateDistance_m7BE81DDB28906D54EAA3D1766DD578084B50AA2F,
	AIDecisionDistanceToTarget__ctor_m3AC88769049C7EB3C66C7D94478504A5890A5428,
	AIDecisionGrounded_Initialization_m445003C43E3DBB239905E33358B116984771D4E5,
	AIDecisionGrounded_Decide_mC0BF81DA9B2B8E29B00717A3B5203FA69A2F04E0,
	AIDecisionGrounded_EvaluateGrounded_mD9FBA8356B8A85BB06F5272A7EF36E717C780C0C,
	AIDecisionGrounded_OnEnterState_m9085BAFB53015B600636135E38D64888DC138B12,
	AIDecisionGrounded__ctor_m397C7F2DB0649C44DA9A8FC5D8FBEDDC3DA0187B,
	AIDecisionHealth_Initialization_m5E4965639595ECB2A28387A4462E0D059E4AEE84,
	AIDecisionHealth_Decide_m5369C10227BA7AE07D8F77FA6DB47583D99E6089,
	AIDecisionHealth_EvaluateHealth_m0996172C4F948EB51ACBC7ADF8FD02DB6C6BE26C,
	AIDecisionHealth__ctor_mF1744025167F668C6603281D80DA5A6D4D0E6171,
	AIDecisionHit_Initialization_m975FAC82D438B848513CB4508E86CFF44026D524,
	AIDecisionHit_Decide_m72845B73FAE4216A3A7062B028C7303945C273EE,
	AIDecisionHit_EvaluateHits_mD6159A3C5A313BA834BBBD29BECB7EBA6FD3FE32,
	AIDecisionHit_OnEnterState_mE88DBAA48FF6430BBDACFF461BF63344E305293C,
	AIDecisionHit_OnExitState_m4E6AC172814252563D4543DC42660F95F16EE62C,
	AIDecisionHit_OnHit_m729E06971CC05FE34FAB1C6EC86953BF50ABDECC,
	AIDecisionHit_OnEnable_mDDA879B5FFDB56F276B7843307724E4AC5D51F47,
	AIDecisionHit_OnDisable_mB1D74062FAC88F3518452A22A20E13C5909E93EA,
	AIDecisionHit__ctor_m62859448911B2DD54CF4F7796D949210C3E00501,
	AIDecisionLineOfSightToTarget_Initialization_m729B4425776C29F16EEB2E20940FB2B0BB34A4CD,
	AIDecisionLineOfSightToTarget_Decide_mD5BE1B97F4EDEF8C8FB03ADEC7935FE1717D4244,
	AIDecisionLineOfSightToTarget_CheckLineOfSight_m91E671EDAB5B53EC4B591B7B8EF7E0CA85FDC778,
	AIDecisionLineOfSightToTarget__ctor_m367894A18CEE324894592FB3DE16E25B90D26DE0,
	AIDecisionNextFrame_Decide_mC79CBF33A2CEA537FC308D781615A3B616665924,
	AIDecisionNextFrame__ctor_mF40CBBDDC032E793421F8314A65822F3D505894A,
	AIDecisionRandom_Decide_m58A9F8AC84F9216B884EB628765EA91A493089CC,
	AIDecisionRandom_EvaluateOdds_m37E812CC7C36943284D68F47638F8F67C96D5A7E,
	AIDecisionRandom__ctor_m5F0D680D4C819AC82B5A775825D3ADF9EF1A4CA4,
	AIDecisionReloadNeeded_Initialization_m65C8A79A03F2448A8CC4E94E9BE9C42E1A002B37,
	AIDecisionReloadNeeded_Decide_mAD461D8D33DEEAF2A62AF36CC5B367BE37BCC2AA,
	AIDecisionReloadNeeded__ctor_m018929BBC28231BF457BC0EFE6B0A1E245F85AD3,
	AIDecisionTargetFacingAI_Decide_mA794713C434AFDB665748DFE46BBA7FEAED900D4,
	AIDecisionTargetFacingAI_EvaluateTargetFacingDirection_m8AA52BD991D7894A4EDFD9C3972D2E04644D9146,
	AIDecisionTargetFacingAI__ctor_m339665929A0075E69ED47837789A93A3E61B4C4F,
	AIDecisionTargetIsAlive_Decide_m517FE1CDD7AF55656E62B79C09119A08FCAF0CA4,
	AIDecisionTargetIsAlive_CheckIfTargetIsAlive_mE3181A66B527CB60E75E01E68C46EBB1639A20CB,
	AIDecisionTargetIsAlive__ctor_m690F0BAED7D4196D2C8B68800185125532687C5C,
	AIDecisionTargetIsNull_Decide_mCE3BB32F87CDBB4914335910876D2F33968F7E73,
	AIDecisionTargetIsNull_CheckIfTargetIsNull_m657AF9418D2E9D4ADDBC356A219C6A037EDF5892,
	AIDecisionTargetIsNull__ctor_m26AE2354016519900A766C610EAE5C5D284EC217,
	AIDecisionTimeInState_Decide_m4D68D9AB4442EF3B07EFA4FDFCCD66B3CB376061,
	AIDecisionTimeInState_EvaluateTime_m82F03E0D569A646165E5B6B1739A80B2D1A9A53F,
	AIDecisionTimeInState_Initialization_m675AFA8D8FBFACB098ADA3CC164A5A56E6AAA2B2,
	AIDecisionTimeInState_OnEnterState_m687551FB68240DEDEADCFF7C63CC0D1CD77A1909,
	AIDecisionTimeInState_RandomizeTime_m33222695F62C89609E63477B0BD8014784288DDC,
	AIDecisionTimeInState__ctor_mB7ED16A17BCDD703E570B9599C97097874F7D364,
	AIDecisionTimeSinceStart_Initialization_m3C8230404BF8852AC404100BCFD4A9EDEE6EA6C3,
	AIDecisionTimeSinceStart_Decide_m1728C0C83CD2A44DA0E774763C6001671294E0FE,
	AIDecisionTimeSinceStart_EvaluateTime_mDDFB1EB68E187D0905D7AE4C660259A150F532F6,
	AIDecisionTimeSinceStart__ctor_mFECBED3FD973C957D4F8502898B0A4E437D1D49D,
	PathedProjectile_Initialize_m286B9D9604D07303E48145F582E0D74EEFD095E2,
	PathedProjectile_Update_m36BA65CF8C67C71DA256FD314E7BBC1D1F8F4246,
	PathedProjectile__ctor_mCF5BF2FFCFA3500996B284F01A540CC7C37D24E8,
	PathedProjectileSpawner_Start_m23DB1C7024BBF36535CE0309E72B79E6B03CB9D8,
	PathedProjectileSpawner_Update_m627EEC7379137D734D504E5009608F42337C9030,
	PathedProjectileSpawner_OnDrawGizmos_mDF492AB74E7C37E1C7B3B9C23E4F9A268D726A0F,
	PathedProjectileSpawner__ctor_mC0E7D872C7C0DD84C199E45DF818292ADB0BF010,
	TimedSpawner_get_ObjectPooler_m260DF9B822E7790C098F47111EF9449F2BE6A44D,
	TimedSpawner_set_ObjectPooler_mCB8F79F8F9924497AF822D7BFBF1CF480826F0A5,
	TimedSpawner_Start_mBF687A6392FBC5116152B1C0DA85EF673A93036E,
	TimedSpawner_Initialization_mEBD31B42C765CD15F6EA96749133D307BAE1383D,
	TimedSpawner_Update_mAF59551F52E1B1010F3F9801D600EEFF46BB0AE7,
	TimedSpawner_Spawn_m05E675915AB1898F5C36CF5A62E1A09FFA8FE668,
	TimedSpawner_DetermineNextFrequency_m3DD9B2E7ABA1A7E2F9C261E51F1E27BEF2A78B1D,
	TimedSpawner__ctor_m2F705AEDCC7EF00BCF7497FFEC8C7528E412E160,
	AIFollow_get_AgentFollowsPlayer_mDDEAACF81F993DCC02AB7EDAC15B2B3E9302E601,
	AIFollow_set_AgentFollowsPlayer_m752F008DEF41D0E5B5CF55818CE0379FA74E3B7B,
	AIFollow_Initialization_mF8DAB601C6F466E5C68066351146F546137917D3,
	AIFollow_Update_mE63D1A8CE941C6DD29C22665CCF35C9D5D757964,
	AIFollow_OnMMEvent_m97292B4727CB4A7CDBAB61F4545BB7A46A0ADC2C,
	AIFollow_OnEnable_mC45B51C7F6511573AC1F0CA97D1BC13DE4916376,
	AIFollow_OnDisable_m3D5AC16B38E6FF17F107421D49971C3D2AD97480,
	AIFollow__ctor_m47E23AC2D5EFFDC9B0AA61F136D6F7AF62CD2D05,
	AIShootOnSight_Start_mE206B543D23692673F71A0603F8D96E9769F2FD6,
	AIShootOnSight_Update_mB129AF4A105C0211DF80D8EFAE998CAD7BC13274,
	AIShootOnSight__ctor_m2C6C7BAA2F0A4AD4BF149E8D1438229CB3CF9BCC,
	AIWalk_Start_m61B503C611B68C408641EDC48DA2F95CE1F46B88,
	AIWalk_Initialization_m7A3DB44DB3C683F6F5D7E48C81B3DB920266EB34,
	AIWalk_Update_m42A3ACC189A03E8DBE1971624514F40D63C63A85,
	AIWalk_CheckForWalls_mF054C26678EC50EF5CBCE3ADD2E95862B0355BC8,
	AIWalk_CheckForHoles_m8DF575ED030E2ABEB3337A86C502F99F37281872,
	AIWalk_CheckForTarget_m84202716520E3FA771C7FB4FCC1C64EF7075F00E,
	AIWalk_ChangeDirection_m3765D91C267E74B78F7D26051B1278840B2ADDCC,
	AIWalk_OnRevive_m63ADD8CACBC0B95AA6AB704F278075C4C839D836,
	AIWalk_OnEnable_m96CA5DF13066A28B1C958407F97436F09C4249CD,
	AIWalk_OnDisable_m7DD7372366E24696935C4E7BA0213E4848921687,
	AIWalk__ctor_mAFB38AE548693879517DA9331C1388E0442667D3,
	CorgiEngineCharacterAnimationParameter__ctor_m422C90AF2970DD2AB766173152F7E3962D1A22CD,
	CharacterAnimationParametersInitializer_AddAnimationParameters_mEA7327F02AE35760B42C1684807EFE916DA67F3C,
	CharacterAnimationParametersInitializer__ctor_mDBEFFF0C37C380D9F65FFAB544F7FB19C7124BD3,
	CharacterAbility_get_AbilityAuthorized_m6D8E46BB303E61CECEF24F22B57E5A6062FEE97C,
	CharacterAbility_get_AbilityInitialized_mF5F68AAEFC8E00BE052230EA64C2EE90DBFD8260,
	CharacterAbility_HelpBoxText_m573A43433F68340CC60D275CBCC5F0CA44257F64,
	CharacterAbility_Start_m51D4254F0184216EDA5BCBDE7681CA739D88B370,
	CharacterAbility_Initialization_m490E4A987E5686DAE37ADCEA55ADC474E3F91BDC,
	CharacterAbility_SetInputManager_mD13AAFBF42600ED72C6A60059D869FEA2CDB425A,
	CharacterAbility_BindAnimator_m5CB2D2F76EFCEAE696D1EC2D994F9586E3E464FA,
	CharacterAbility_InitializeAnimatorParameters_mB8A51C1A2CABB4F0B8627722A7033D50DD8BCA32,
	CharacterAbility_InternalHandleInput_mA0B9113DEE10C9E8AA1B7F0282CCA40F3476F768,
	CharacterAbility_HandleInput_m860A4A1D587CFA497E2483FEB32A35AE87C75F1A,
	CharacterAbility_ResetInput_m9FB42C43CC204C14A3C7282548280DB221D10A73,
	CharacterAbility_EarlyProcessAbility_m8F3E1203129AA4E405E8EC89C7A203EFDA125982,
	CharacterAbility_ProcessAbility_m665947A1A59C3538FAD48919DE6A17CE62A28D6D,
	CharacterAbility_LateProcessAbility_mF966F8941ADFF4238E62A5C60D2E8CF04C3D0DA7,
	CharacterAbility_UpdateAnimator_mC5DC003C2A7A38A1F4E2B6C0E8080B35A591079D,
	CharacterAbility_PermitAbility_mC249DBB3FD92DC52570CBD5C7170C11F089CF17C,
	CharacterAbility_Flip_m442B84D94E988B2A43A13EB2905E5DE419D51E2E,
	CharacterAbility_ResetAbility_m5C3A72E516E4EBA11CC297E1FF4B95454E0D365C,
	CharacterAbility_PlayAbilityStartFeedbacks_mE39CA8B614996220E544DA2652E587A938BB8431,
	CharacterAbility_StopStartFeedbacks_mF309B9E16F031B8C241EB67C9CABA512249F73D4,
	CharacterAbility_PlayAbilityStopFeedbacks_mF4C85DAD8631DFC946607161629677B80A039B61,
	CharacterAbility_RegisterAnimatorParameter_m4CDAC37CE773AA77A9473687C913BCD048A9B94D,
	CharacterAbility_OnRespawn_m12A1D21893D7959E9002A058F9ED523D267EC988,
	CharacterAbility_OnDeath_m3FB727C8B5DE2DD9DF8C55F66931C39B8621C586,
	CharacterAbility_OnHit_m80761A1EFB5160C474E1D5E4EE2E776CA105AD6E,
	CharacterAbility_OnEnable_m610BC1FF2F7AF135A13B8B72ED0459BDF07238AA,
	CharacterAbility_OnDisable_m6DC24D5A2273487BA70F255BFA8DF3E7830341D8,
	CharacterAbility__ctor_m99A9BC4D760B415E996417AB4658940BF317D8A7,
	CharacterAbilityNodeSwap_HandleInput_m6647AB9323AAD2BABF34FACAD2648ADFBF4DF5BC,
	CharacterAbilityNodeSwap_Swap_m70DB8A13329F42DC6E3B8E4CD5024C36BB9F1654,
	CharacterAbilityNodeSwap_SwapAbilityNodes_m61CCDD35231A0073A1396ED87A9A62DA213AAD10,
	CharacterAbilityNodeSwap__ctor_m42DFF85B6203059408FEED9EA2E87B15B3C75E98,
	CharacterAutoMovement_Initialization_mF7D1C5B50FE344AAF634DA7C54F7B2AA49ED0152,
	CharacterAutoMovement_HandleInput_m01B235C642D2662C04A5EBF78F39B8C33B77B60E,
	CharacterAutoMovement_ProcessAbility_mDF03BA0A8E36963B9DAB90F1AA66F8478F0D56C1,
	CharacterAutoMovement_TestChangeDirectionOnWallCollision_m2B04FB56D2A7805E09622FD6460CC30F9E566716,
	CharacterAutoMovement_TestChangeDirectionOnFallingDown_m0D53C3D84367C917FC16F5B594C13182CB5C53EA,
	CharacterAutoMovement_LateProcessAbility_m3D5E1011FED38FE02AF371BC149FE0AF1B7AF22A,
	CharacterAutoMovement_OnWallJump_mDE684CD0AB44D01E632C4F4334837CD57E78C327,
	CharacterAutoMovement_PauseMovement_mE3D2B9B571C63AE9B9FCAE4EFE9DE951AC61B0BB,
	CharacterAutoMovement_ResumeMovement_m52AF6BB2D69214DBEBE5DC965074C5AB04CF9331,
	CharacterAutoMovement_SetDrivenInput_m9C0573626177CE3F29F6B3F8B4D638954955F2E1,
	CharacterAutoMovement_ChangeDirection_m3D62572EBAD155BC7DA75CEAAF8C00D4DED0A7B3,
	CharacterAutoMovement_ForceDirection_m4739A4BDA29D08FBCD00F3665FFA0FA2CD876FAA,
	CharacterAutoMovement_ToggleRun_m0B837FB4353DE48A36EB735D9E88DCA229CCEE9A,
	CharacterAutoMovement_ForceRun_m4ECB2A335252E1D4F1D36D7347136741ECB53D6C,
	CharacterAutoMovement__ctor_m1EC35D0CF233BD2DFC11DE8B3871F4C05552C945,
	CharacterBounce_Initialization_m398D0572A22C86C35EFFC6EFBC19B687FE7DF44E,
	CharacterBounce_LateProcessAbility_m781C67BB9ECE546FE846ADC1B58077B4BB500153,
	CharacterBounce__ctor_mFAECA713DC1AA3255EC48B4B8016B78D356C6BA8,
	CharacterButtonActivation_HelpBoxText_m5422705A527F628120DF9EEEB017789184ECEC0B,
	CharacterButtonActivation_get_InButtonActivatedZone_mA07B6E4ED64118FA0FBDAF139DA1E11CA2D1219D,
	CharacterButtonActivation_set_InButtonActivatedZone_m0A740C560AB575C78ED3549F0D2C378D0537D46A,
	CharacterButtonActivation_get_InButtonAutoActivatedZone_m6EB06564BB1067EF3ADA23F4CDD85E9D7D3BB2F8,
	CharacterButtonActivation_set_InButtonAutoActivatedZone_m9E58139C6B87843B92DC6F4AE7B0B32BF1AD672A,
	CharacterButtonActivation_get_InJumpPreventingZone_m688CF6705E6CFB3058DF219CA6302DE343248DF7,
	CharacterButtonActivation_set_InJumpPreventingZone_m9EC4851E01A7F49E4EE9873B668D426D8F3720FD,
	CharacterButtonActivation_get_ButtonActivatedZone_m54156CA22EF33E38130DBDE8A3FC57682A2FB2EE,
	CharacterButtonActivation_set_ButtonActivatedZone_m88D200B6E768D14A4C23CD2EE9567D4327EEAA15,
	CharacterButtonActivation_Initialization_m61F2D5A6AE770E89A7B8A9F5DBFF5EC185B08A41,
	CharacterButtonActivation_HandleInput_m13923B2931B8F5DDD4F4A1E53D2C0CC88196BAE6,
	CharacterButtonActivation_ButtonActivation_m273F309184BED996588A49D73CA89EB094D90D09,
	CharacterButtonActivation_OnDeath_mA02AF58E27DF67CC5BC6CD43CB60743B89651EFA,
	CharacterButtonActivation_InitializeAnimatorParameters_m5D4106279F6AE6F1ED597B90B853769FEBAA6D50,
	CharacterButtonActivation_UpdateAnimator_m1A857FAC90F4D41F3541BC96C98B1897C5F68D79,
	CharacterButtonActivation_SetTriggerParameter_m12F48ED86B6F2B4E267BA4C12A9F23DB8949E0E9,
	CharacterButtonActivation__ctor_m5AD31E4DEF8952F9AA75EB29F934B5C125851204,
	CharacterCrouch_HelpBoxText_m13BB2EFE12DB144068C0B760B6B5B4CEC3F1FC2E,
	CharacterCrouch_Initialization_mE7F48D3EE762F851DC25CF7ECC847D3030EAE493,
	CharacterCrouch_ProcessAbility_m63D6E17BF31D424300692921ED1A02A045860679,
	CharacterCrouch_HandleInput_m335C8224B073AB63C888BF26ACE1BCCD878E1C82,
	CharacterCrouch_Crouch_mADC6904A06AC3B43E30BB5DD42ECD85F68A84D69,
	CharacterCrouch_DetermineState_mDCDEC3601532BDC3F12852617CDFE14B930FBCCD,
	CharacterCrouch_CheckExitCrouch_m9D8D30F6DE8981B66C6B722292D9F3710868BB3D,
	CharacterCrouch_ExitCrouch_m5CA5DD9F0B2D320BC818FFD0A6F2107F4DA5FB28,
	CharacterCrouch_InitializeAnimatorParameters_mE98D1FF068E60727DB7A03B604EC7F4A9395EE6E,
	CharacterCrouch_UpdateAnimator_m24CC60D7F1346270008F94E684BF929ED139C72A,
	CharacterCrouch_RecalculateRays_m56FE03D0129636C9512DD3AD82CA3F9EC412CC72,
	CharacterCrouch_ResetAbility_m5ED490CAB9297BFC1B8B578D5B158870D078E577,
	CharacterCrouch__ctor_m57914E52979905DDA2E7D7F3D67EB687F5575F9C,
	CharacterCrushDetection_HelpBoxText_mDACB59C4EF3F79376F855FBCED836C54D7957D21,
	CharacterCrushDetection_Initialization_mBC4F1D984B4787DEB6998F7FD4DE461C277AC8CA,
	CharacterCrushDetection_ProcessAbility_m3777A446291567F78B98BD5A6D1DD13757A744BF,
	CharacterCrushDetection_DetectCrush_mB61FB0EBB0BC62E2A8A5492B9AACC169ED94E2DC,
	CharacterCrushDetection_DetectionRay_mDE8C44360E69E8A7CB9F5BEA9F18E20E0C7451BD,
	CharacterCrushDetection_ApplyCrush_m8505E156F294D6AA96DD43ADDD664BAAC2F80235,
	CharacterCrushDetection_InitializeAnimatorParameters_m35AD24F51FF4CE95566EBA59A33F03D2939BE824,
	CharacterCrushDetection_UpdateAnimator_mF2FFF4FEB64A410FD057A6A611FC552E385640BB,
	CharacterCrushDetection__ctor_m271FB1C941F03392EA3AAD6EBFCC2341E0F62039,
	CharacterDamageDash_Initialization_m0D189DF3770CFCA87BE658C36B6A18E74E9662E8,
	CharacterDamageDash_InitiateDash_mC9F00796333C9E6815AFC555E9812F391BAAB3D8,
	CharacterDamageDash_StopDash_m591306CC0171475E741DD16A392CB29E53E86659,
	CharacterDamageDash__ctor_m2FFEFE466D8A83CE45AC9ED63C3076395BB1BE95,
	CharacterDangling_HelpBoxText_m65DD3A94DCC99C1DC187303B2B777116C6B44FC1,
	CharacterDangling_ProcessAbility_m93F0ECAC6DE8D911B0A703D2C965170571D38287,
	CharacterDangling_Dangling_mAC0DCFD4B26DB8B7C5534F021BCEAEE5BCFB2771,
	CharacterDangling_InitializeAnimatorParameters_mC3EDBB0AB8C1173F99CF738DE528AC30ABCA9D24,
	CharacterDangling_UpdateAnimator_m1CC54A355E46B719E9CF021010FE51316C3783BE,
	CharacterDangling_ResetAbility_m9BCE05C08616E258F2F61B220F3241C6FD933202,
	CharacterDangling__ctor_m21F426898D038B80209A79A2E567890676493554,
	CharacterDash_HelpBoxText_m3E4C8FFA5D999FA65B1E1FEE4F1CB95EA363C0DF,
	CharacterDash_Initialization_m0C25D1D3EE6ED3BF85A7F63813FF2E6392706A7A,
	CharacterDash_HandleInput_m0C2D30677CFDC36D11E6C110ABA87447C389FF73,
	CharacterDash_ProcessAbility_m9752B45DBAC3A7E787A19443E36EFA5407FD38B4,
	CharacterDash_StartDash_m3FA54EE6A34E8D5781513164BF4BDA9815F64AF6,
	CharacterDash_DashConditions_m7C4136CB51F7856556266CC49EF11C78B56E2A71,
	CharacterDash_HandleAmountOfDashesLeft_mD4BBAA877887C92C13070F72974038FCD1759DB9,
	CharacterDash_SetSuccessiveDashesLeft_mB3BD0FDC53FCDD58EFA889E9D78F576DB8B5670C,
	CharacterDash_DashAuthorized_m78F87B06D7E44A2C669CD8E37F0D190729B26D7E,
	CharacterDash_InitiateDash_m592A15CC605CDE9B51E5EA8131FF210242D4570F,
	CharacterDash_ComputeDashDirection_mFB8253A2D224AAC407D425EAEFA1A2D9E009F54B,
	CharacterDash_CheckAutoCorrectTrajectory_mBA40E31851690BB9E14C8E56C06557CA6D12874C,
	CharacterDash_CheckFlipCharacter_m1390A069D4E3C76A3141EA4D039A03AC2D19A164,
	CharacterDash_Dash_mDB12F06669BE888C9B633AF9CBDF10560ECD4C57,
	CharacterDash_StopDash_m686CAD4EF59D521633BA9CDFAF186EAFE6272A45,
	CharacterDash_InitializeAnimatorParameters_m07F282AFA331F6D52CB2013CC6ABDB4C0B847CDE,
	CharacterDash_UpdateAnimator_m71E4CCB56014B24A9089386A9AD50A19E532E4B9,
	CharacterDash_ResetAbility_m4CF376C14167091994AB4010609B80539325B202,
	CharacterDash__ctor_m7AC99131421EA3C5A8A49EA307C34E028AB1A8A5,
	U3CDashU3Ed__41__ctor_mED415C8629EB908093609AE9CFA90AC7BF650D72,
	U3CDashU3Ed__41_System_IDisposable_Dispose_mB0B9BE14C0DB18EEE122E9E1ECCCCD786DD5D918,
	U3CDashU3Ed__41_MoveNext_mFF07F108A4FC657FC8FEF6D7FE55B7D1A5472C51,
	U3CDashU3Ed__41_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDE29C5A8BA912DFD2BA2EB58C64429FC4B89D8C2,
	U3CDashU3Ed__41_System_Collections_IEnumerator_Reset_mE7BA17A6E71F2A23C4622C39FCC42262F3A0788B,
	U3CDashU3Ed__41_System_Collections_IEnumerator_get_Current_m93173A94EB2B7CFCE72B3661A88BBB1DAC7310DD,
	CharacterDive_HelpBoxText_mE7DB54DC682FE886CA93A559986DF6077E729D56,
	CharacterDive_HandleInput_mA3B897680EB5C26BA4EE17D8DF8F39E7149FD48F,
	CharacterDive_InitiateDive_mD8010823BC3D5568E6D6B7D78AEFD3F87AE33C4C,
	CharacterDive_Dive_mAA6DD4F76350EF401684E6309E6CAA645D6FEDA7,
	CharacterDive_InitializeAnimatorParameters_mB4E271DF7F9601FCFFF9AFC54BE3FB1D325496E8,
	CharacterDive_UpdateAnimator_m2ACA128914877109E85938F0CA6953B06E9BF08E,
	CharacterDive__ctor_m4A6807F3784293C263F2F20FB6FF62EC8FFB8CF8,
	U3CDiveU3Ed__7__ctor_mB3391F3DB2B4C091422BC6F211B99D80772DF112,
	U3CDiveU3Ed__7_System_IDisposable_Dispose_m22C8FD7C5A78567E92271987F5C7DCA2A843F5F9,
	U3CDiveU3Ed__7_MoveNext_mC25AF638F3DACFF8A6E103C642BC22226F3F3EC3,
	U3CDiveU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m00BAA6EDBD4080413693E3CCDE2858595D8112E7,
	U3CDiveU3Ed__7_System_Collections_IEnumerator_Reset_m09751C9E3137D2C683419F572F8D641C4E0792DE,
	U3CDiveU3Ed__7_System_Collections_IEnumerator_get_Current_m475FAE401A618F784E4341B22C4E20C27A90861F,
	CharacterFallDamage_HelpBoxText_m79052D063BC9B0D2E47DA089E1EA23DDB9AFAC75,
	CharacterFallDamage_ProcessAbility_mCBD703982AD134D093A8B5AA28203203AA32753A,
	CharacterFallDamage_ResetTakeOffAltitude_m3BCEEA3B3CB9932C806F55E183227CE515C60651,
	CharacterFallDamage_CanTakeDamage_m5C1B986346256D7D949BA6AC8BE37E47724F6ED3,
	CharacterFallDamage_ApplyDamage_mB34E1B36D5F326F5EFD7720BC970581525B9EDF3,
	CharacterFallDamage_InitializeAnimatorParameters_m619C08DBFD777F23F77B961630AB322100FB6D8B,
	CharacterFallDamage_UpdateAnimator_m16C2F17D9456F0FB1675B1F5C5AF391C03E95AA1,
	CharacterFallDamage__ctor_mB2BA76966EDEC3E4B306FED5AC5A1AB12CFAC911,
	CharacterFly_HelpBoxText_mEFAAF8671AEBEE2ED4D09948AE9922AE0FE76AB2,
	CharacterFly_get_MovementSpeedMultiplier_m9AAB00537EEC898F19EA96CCB40495A42A6BC475,
	CharacterFly_set_MovementSpeedMultiplier_m3F7297AC8D4DCE31CC505AC8DD1F31E8FC88C45F,
	CharacterFly_Initialization_mC29EB6C246E5E19134061754B1874ED38C828820,
	CharacterFly_HandleInput_mD2D022CEEECBAC2BCF11686E5F9554A5EF23C23A,
	CharacterFly_SetHorizontalMove_mE2D2F9B972958C4CAC899A8F40C1CEF0220A0547,
	CharacterFly_SetVerticalMove_m64E4405AB743FC82574856159EC53A238D6F2EFA,
	CharacterFly_StartFlight_m3933E7584C3DA6492CB7FFF04B76FA3AE239258E,
	CharacterFly_StopFlight_mF0226756A343338F7E04A0E19E274031D3714234,
	CharacterFly_ProcessAbility_m350FC2533D443BA30955C7D583FB0836780D93F6,
	CharacterFly_HandleMovement_mF7EAB398054BA07F301FA36C5EE7C4A6FDD0AA33,
	CharacterFly_OnRevive_mC9EFA6A678D00A33E24D26112CDE2B7E9AFA2E90,
	CharacterFly_OnDeath_m6D44C39736C5B95B5682ABAA5AD7D110C3495802,
	CharacterFly_OnEnable_m137803A68E6AF6E55FC94F8B599CEBD3FAC2F539,
	CharacterFly_OnDisable_m7C6E92852156C244E0BBA523856F0D4554C0057E,
	CharacterFly_InitializeAnimatorParameters_m007E227576B5FBB5B0231C92E5A180E0A85EFB93,
	CharacterFly_UpdateAnimator_m2399637D493F88384907F8ED4775A155AE95E7C9,
	CharacterFly_ResetAbility_mAD9EA4E70A17D6090F6A030B1E80175785C83E9B,
	CharacterFly__ctor_m2738637520499EB54EFCE470786EAB9B45C409BC,
	CharacterFollowPath_get_MovementSpeedMultiplier_m15C0157B890A378139F3053904C015B32D793BA5,
	CharacterFollowPath_set_MovementSpeedMultiplier_m70337476AB81F2A5206C17891D6152DAC7D096EB,
	CharacterFollowPath_Initialization_m3BF02BB46630E1742CC04757B9E3A3F681A0CEA7,
	CharacterFollowPath_StartFollowingPath_m7D6C6200C6CE3F0D3CD27019B6F8C029653932BF,
	CharacterFollowPath_StopFollowingPath_m58949C6A3DEDD50F660C7BCA42F50385040E71A4,
	CharacterFollowPath_ProcessAbility_mDB9276D7E5E12E6CB44A45BB3ACA6C636286782D,
	CharacterFollowPath_HandleMovement_m0B464D5F959770C7453FF3E4A8489683C0E4FE69,
	CharacterFollowPath_OnRevive_m08ECC1D598399D056A70D06D7273600C698C6780,
	CharacterFollowPath_OnDeath_m239095201E53E5FB21116AE9D4971BE9BD02D5B6,
	CharacterFollowPath_OnEnable_mCE3949E1C5CFEC8EFF36548787D5B984AD4E6023,
	CharacterFollowPath_OnDisable_m221321017CAC4DF4897F7A6CB518C2B6EBBAA0F6,
	CharacterFollowPath_InitializeAnimatorParameters_mED75E32FC19C089E64939A806F460184898B13A1,
	CharacterFollowPath_UpdateAnimator_m2DE5992C4B519D8A4DE07D3893B1BBE6E755AAF1,
	CharacterFollowPath__ctor_m55876E74124BB0988D7DB7E3F66175910A315CD3,
	CharacterGlide_HelpBoxText_m0D5C3A2B9F07A5A01ECB1BCE6CC0EFD4FD9B9BD5,
	CharacterGlide_Initialization_m119254E684B4C85EC2F8F5431442AD2260040A94,
	CharacterGlide_HandleInput_mF175EA711DBBCB8757574C5B8B837B30A377D74F,
	CharacterGlide_GlideStart_m077C43C3536D49F70B426E4F258A3C880A7D3891,
	CharacterGlide_GlideStop_m8804DC047451989E540B5F57ADC4F2F30BBDFEB2,
	CharacterGlide_ProcessAbility_mBF924A098E53E0C3236AD699195B4B87EBBCA5AE,
	CharacterGlide_InitializeAnimatorParameters_m43A4B5ED0787F3E7C97CE2A2B253D31300FC0204,
	CharacterGlide_UpdateAnimator_m7C919E3FC5B829C12CE2427149D623E95A718321,
	CharacterGlide_ResetAbility_m76ED451359F046F97424135705FB140DA8081FDE,
	CharacterGlide__ctor_m87A6933F9E1CFF243DCEA7343DCEE9888D723039,
	CharacterGrabCarryAndThrow_HelpBoxText_mC8C2E4515FE28AA422DD37B9E696BD99035DDCC3,
	CharacterGrabCarryAndThrow_Initialization_mF8F963BF4C5CB26813EE3B8D153ABB5B232E0D4A,
	CharacterGrabCarryAndThrow_HandleInput_m660F5E85DDF78AE29CBB3B1AC2C25DD768856586,
	CharacterGrabCarryAndThrow_GrabAttempt_m9F0D2ED3CB2E27095FB31457249FABEB071C2B1F,
	CharacterGrabCarryAndThrow_Grab_m875CBF56DE04A578DD187AEFF1EFADEEDA6423AA,
	CharacterGrabCarryAndThrow_Throw_m6C4FA1E3D499409926C6F0666858F9F12C83455C,
	CharacterGrabCarryAndThrow_StopFeedbacks_m45960A5CC1DE4D337B7E873D260172A13145E612,
	CharacterGrabCarryAndThrow_LateUpdate_m027A6E6CE02EBB929CC9830ECF1CDF4A7C040667,
	CharacterGrabCarryAndThrow_InitializeAnimatorParameters_m855AA576687AE38C2A5BA910F69A35FED446C5F3,
	CharacterGrabCarryAndThrow_UpdateAnimator_mCCC7D155D079D90199C0B4FA8470F6F24061AA86,
	CharacterGrabCarryAndThrow_ResetAbility_mFF3EC5921ADE86850735F0A8869F7E5AD045C351,
	CharacterGrabCarryAndThrow__ctor_m82F3FDD07E082AEC2CB13D9BE45AA346417516D3,
	CharacterGravity_HelpBoxText_mEEBAFF1F02DFC62FC33A5E522003266ABD347639,
	CharacterGravity_get_GravityAngle_m5CF126752FFE6498AA98DE9CEB97F7A7FD45C9BE,
	CharacterGravity_get_GravityDirectionVector_m9C380E449E7F504215C8A65178F714855071DEE0,
	CharacterGravity_get_InGravityPointRange_m83BFB33F282DA3E536B56F97CA844006F0045432,
	CharacterGravity_set_InGravityPointRange_m1033177E4898659BDF4179C3E3C816639B13A7DB,
	CharacterGravity_Initialization_mC87999A5A277EF30F2C41A63447AE0770F334965,
	CharacterGravity_Update_mE131EFB9FAAB606D5F4E98E4C6E88DC8D77BCB27,
	CharacterGravity_CleanGravityZones_mAD101E0CC0ECFF2C30BF89F4ACB32ED5EFCC0734,
	CharacterGravity_ComputeGravityPoints_mC991B0FB098BDB19915D6471DFEBD9BDA2AA2CA6,
	CharacterGravity_GetClosestGravityPoint_m0136FFB1934DB14AC7383CD763CD9467858716FF,
	CharacterGravity_UpdateGravity_m61D59E4DF8566E05421EFAEA745A32270E505B2F,
	CharacterGravity_UpdateGravityPointsList_m3A291AEDBF9F64B1FE79564D5C9589CF98CC5B29,
	CharacterGravity_OnTriggerEnter2D_mB95AD47C229624552F6E0C53143697359206FF6F,
	CharacterGravity_OnTriggerExit2D_m1F68046F22A153C7455102D82AE373C24455A88C,
	CharacterGravity_SetGravityZone_m797FCFD4A1DE6EB3CD2A470B13C2DC34392ADC42,
	CharacterGravity_ExitGravityZone_m0B66B88820B5EBEF8E9AFA4F730586E46E6F0F25,
	CharacterGravity_StartRotating_mCE39E79A4CCC5BA21E0D7C288003F0D7C842BCAA,
	CharacterGravity_Transition_m0C7D6BB8057A7BE95BF638A0181CF2E69DC89EE4,
	CharacterGravity_ShouldReverseInput_m3B83D2072A14B7BD288D65BC92B0C964A84DF423,
	CharacterGravity_SetGravityAngle_mAA043E3B4DCA7925929A235DA3CBF891BD4FF54D,
	CharacterGravity_ResetGravityToDefault_m85B7C6D09EAAA8BDEB41A42C70F56155C6B47D8F,
	CharacterGravity_OnRespawn_m1FFEF2047FD74BF5C5513473CA3DC267A94A430F,
	CharacterGravity_DrawGravityDebug_m86715B338A0C8C0E55B821EE98A34F02D3A97DB6,
	CharacterGravity_ResetAbility_m389E21FEAC813978348103A4A7B9311861740AF1,
	CharacterGravity_OnMMEvent_m25F0ABDB934BA8A0DFDEF240D8A66A05616C6271,
	CharacterGravity_OnEnable_m507F6344E8CCBEF3F14FDE55FD7AA7FF1C7546F7,
	CharacterGravity_OnDestroy_m91C59BA1194D75D157E820E3457B4A141FB856DD,
	CharacterGravity__ctor_m888962652512B7FD38A25F2AF7FDAC8A2DD3D72A,
	CharacterGrip_HelpBoxText_mF75ACB4F85E385FC9B4FAC7CCBA0658FBC2F326B,
	CharacterGrip_get_CanGrip_m93DF777911BC1FB12A578DBC14E1A5EEB24C47F8,
	CharacterGrip_Initialization_mDA47AB6B8877C496AE74A54AC9957F286540CE89,
	CharacterGrip_ProcessAbility_m9CC1B105E67AB754DB19AB5404E6250321ADC848,
	CharacterGrip_HandleInput_m8C13C66B47BF936048914A80D16F0783E2E62347,
	CharacterGrip_StartGripping_m8BC7551B0554D0C759AC9F3A0EB7D980CF821D34,
	CharacterGrip_Grip_mCAA9F387BD776E8646D5C8ECBB7D492F7C0B9030,
	CharacterGrip_Detach_m5F0676A147CE3136456DF7DF4CD126437743DEEC,
	CharacterGrip_InitializeAnimatorParameters_m39A4CF915EC3EB39CA7B03E16506F99EBEE47704,
	CharacterGrip_UpdateAnimator_m079915E801A30EC4D13145397D1C747248D9510A,
	CharacterGrip_ResetAbility_m90F29D4ADFCCD9CAA3A03E31E532A1B340CF1326,
	CharacterGrip__ctor_m4A57B268B78286F5580507DA62F9306CA5072DAA,
	CharacterGroundNormalGravity_HelpBoxText_mD8C8AFA3C4D1FC9D4C5C5D2A63FB7DD60FC80323,
	CharacterGroundNormalGravity_ProcessAbility_mA9D0534A8C58B7B5EC3B76A5F01FD0E064AEC17B,
	CharacterGroundNormalGravity__ctor_mBE05448CFE3CAE69F37A31C82AAFB624030CA468,
	CharacterHandleSecondaryWeapon_get_HandleWeaponID_m8E40D980A861B1C727303B9E1286815D38E5E0C8,
	CharacterHandleSecondaryWeapon_HandleInput_mE4B10F51A18428C0B51E49F9786A51043367656B,
	CharacterHandleSecondaryWeapon__ctor_m89F105C63F0DF1418F987201D0CF08A4D683DBF6,
	CharacterHandleWeapon_HelpBoxText_m1F2CD91E7DAE5335895C9F8A177E505343C59448,
	CharacterHandleWeapon_get_HandleWeaponID_m253ECC414C01C2370E985D86350FADC86B3EA025,
	CharacterHandleWeapon_get_CharacterAnimator_m9087C592E498AFAC64E45CC7472E30292078ED2B,
	CharacterHandleWeapon_set_CharacterAnimator_m5EB91817B19DC74CF3ADBBBCC7E50C2E30A81B38,
	CharacterHandleWeapon_Initialization_m4F6F79A36760E909A8C3084E82A6737A1E9D2619,
	CharacterHandleWeapon_Setup_m9953000FC787FF2228A839F3D4A4A4906851AA3A,
	CharacterHandleWeapon_ProcessAbility_m4237D410432D7CC45212CF54B1088045C625F8BE,
	CharacterHandleWeapon_HandleFacingDirection_m7BC60970AC238574077D43B4223DEDD2C627C545,
	CharacterHandleWeapon_HandleInput_m53EA27365469EA836ABADAF489F00E1716082A78,
	CharacterHandleWeapon_HandleBuffer_mB31A25C703F3EECA70161AA80CDB7EBE61D3D542,
	CharacterHandleWeapon_ShootStart_mCC40CAAA7AB770B3A1E2BBB5F9EA48756D33744C,
	CharacterHandleWeapon_ShootStop_m56DC86CE2274C02301004A9B97BA5DBADA27398F,
	CharacterHandleWeapon_ForceStop_m4A9A055AC12DCF9E5C6F902F3E06DDE54EA0C4B5,
	CharacterHandleWeapon_Reload_m87F85248C712006E0002563FEDE0B434A88A2FE2,
	CharacterHandleWeapon_ChangeWeapon_m7D68DB5FF3A801D6C73C3257477CDF2443F24487,
	CharacterHandleWeapon_Flip_mEAD0ED9939D298D8ACE321F2078F579A56F69D89,
	CharacterHandleWeapon_UpdateAmmoDisplay_m40BC04AA4850C7D104ABBF3919D265B484A777A1,
	CharacterHandleWeapon_OnRespawn_mB1C4A2BEE398508D8812BEC529B52BF994A1EF73,
	CharacterHandleWeapon_OnHit_m42D93B870DA503BC389BA469A26F4DE285731075,
	CharacterHandleWeapon_OnDeath_m8110BC139A94A29D671B9F3C7E1D2226CCEA50A3,
	CharacterHandleWeapon_ResetAbility_m7684C5BDB98CDA32535729EF622D5B59C5D154B9,
	CharacterHandleWeapon__ctor_m4307F1828A2FE175BFCFCE4CC86AE3A68A674DDD,
	CharacterHorizontalMovement_HelpBoxText_m36BE07AB203FA9705813C280D86B42231D5F95F8,
	CharacterHorizontalMovement_get_MovementSpeed_m1BAC59D0759BEDD81F32E121EF1DA8684277858C,
	CharacterHorizontalMovement_set_MovementSpeed_mD09BDD9D524D8E410CD760737DC96986115B5A25,
	CharacterHorizontalMovement_get_HorizontalMovementForce_mBB88FC6C7D0ED7078170EAAED93E54CD99F383D7,
	CharacterHorizontalMovement_get_MovementForbidden_mD0CFE8EA23F165741E64BC59146254DFF2D7EF40,
	CharacterHorizontalMovement_set_MovementForbidden_m36309648C3BF5D31D9B2CEA6E4CEB7131517E331,
	CharacterHorizontalMovement_Initialization_m18A24E62D9068F889CD5EF7EA17C64045278DB86,
	CharacterHorizontalMovement_ProcessAbility_m97EBFF78B133459EEBCB1F0CF66148D698BDC4A8,
	CharacterHorizontalMovement_HandleInput_m7570002EF8ED389F56F41A783A408B96B7E96960,
	CharacterHorizontalMovement_SetAirControlDirection_mCC66ED3BE7019C7641085576353FFF9B6232F6D5,
	CharacterHorizontalMovement_SetHorizontalMove_m6321AE056939005543469B2A7547570800381D16,
	CharacterHorizontalMovement_HandleHorizontalMovement_m1F365E7B14AF73833559DC0D1128CF5C19C14C53,
	CharacterHorizontalMovement_DetectWalls_mFB7C29D3ACDA5B5F2BB81069333729FF7AD02096,
	CharacterHorizontalMovement_CheckJustGotGrounded_m349C9FEEF0B3007B8EC3C0341A0676D35DB92961,
	CharacterHorizontalMovement_StoreLastTimeGrounded_m40D6000FBE8E54633578F420FBB2716C0BFDA791,
	CharacterHorizontalMovement_HandleFriction_mA207286A163CF52A6D14FD5B6EF3D44CE9F53E1C,
	CharacterHorizontalMovement_ResetHorizontalSpeed_m8A1FFFD02D6F22452E22188EC89937F8BFA93297,
	CharacterHorizontalMovement_InitializeAnimatorParameters_mBCF1A2769FE84824D4A67F6EFA70CC6645086EA6,
	CharacterHorizontalMovement_UpdateAnimator_mAB48C54A5BEBD0DEB169DCFB80BED460B1560B58,
	CharacterHorizontalMovement_OnRevive_mD26C8B70521DEC2372CBBBDAD61C83F002B46933,
	CharacterHorizontalMovement_OnEnable_m60C57CF70AFA36B6D1E32458D0341284BED46B7A,
	CharacterHorizontalMovement_OnDisable_m3EEF85AAA63F53820CA3094F2CCF406FD222B721,
	CharacterHorizontalMovement__ctor_m2EEC5128C57933BC60842F274EF1D1FEAE29FBDB,
	CharacterInventory_get_MainInventory_mD88D766807F991886C6C01F4F2993CB70B9E35AE,
	CharacterInventory_set_MainInventory_mF4F70C53374B693A88BB0F632D7AF1BE7051158C,
	CharacterInventory_get_WeaponInventory_mFE3A0953BBD6ACB0C98AE3A37BD0386883AF003B,
	CharacterInventory_set_WeaponInventory_m514C77C8EDC57C5BEA1968946CB45E1AC550E671,
	CharacterInventory_get_HotbarInventory_m4F0F2B97C5E59FFA8242A705F14842FD39C7C607,
	CharacterInventory_set_HotbarInventory_mF642BCEE5005A33173BE3C475F94D97341E93D04,
	CharacterInventory_Initialization_m75F4E828C8D2DC4A206CEAC073BD7471929A84C2,
	CharacterInventory_ProcessAbility_m9A841624C1705426B355EF7E1750EA5B10DCA96C,
	CharacterInventory_Setup_m30D34FF407BB82DD5CBD5EC9919377305FC38CE8,
	CharacterInventory_AutoAddAndEquip_m263F906BB99B88D2D6BB89BDA7B20C2D975AE69C,
	CharacterInventory_GrabInventories_m61D3201E89B4FBF6C83B0CAB0A33A35ABEBE0FBE,
	CharacterInventory_HandleInput_mAFA3188B677C9A42C78AB87643D6F4F116B20C6C,
	CharacterInventory_FillAvailableWeaponsLists_m4A86294AA8C76F6DA499C1C9FF9161B40F34F21B,
	CharacterInventory_DetermineNextWeaponName_m7ADAF0E64525D996B5E37B5EB5D587FB3A6A6CA4,
	CharacterInventory_EquipWeapon_m1F9D3D33FF88DB05E3DAB503D2D41034F45A725F,
	CharacterInventory_SwitchWeapon_m1D137B41BDBA9033872AECF86DF57DB9425985C6,
	CharacterInventory_OnMMEvent_mC0BB70415F46670FF7588D997F46619388F48268,
	CharacterInventory_OnMMEvent_m69B7418EE8C55AA21066BCA4DF7E38288588647B,
	CharacterInventory_OnEnable_m182B54E1B375207EF7142F94D96242EFB6180E03,
	CharacterInventory_OnDisable_m0522D3B2F24FA8B5F5EE4D6C99ED22C7F4082F7F,
	CharacterInventory__ctor_m0A9AA89B10487035BF9EF4A0EAECA411EADB8CC6,
	U3CAutoAddAndEquipU3Ed__36__ctor_m20ADCEE5975750564CE4D2E2375CDD81808C7ADC,
	U3CAutoAddAndEquipU3Ed__36_System_IDisposable_Dispose_m9816581479A4F717D8855F22F89F9C1A2E3F92FD,
	U3CAutoAddAndEquipU3Ed__36_MoveNext_m38424391F52B8A455DD78DB602CBEE3D832F710E,
	U3CAutoAddAndEquipU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m201F111228A23E549CD3BDCCCF3BBDD8FCF2808A,
	U3CAutoAddAndEquipU3Ed__36_System_Collections_IEnumerator_Reset_mD61E950C5E4FF7E987DA2B1893DB013E8B0E8C9C,
	U3CAutoAddAndEquipU3Ed__36_System_Collections_IEnumerator_get_Current_mBFDD14C6099B3B9CEABA794844957FFFAB760A24,
	CharacterJetpack_HelpBoxText_mB9137CB1509D6B3F5BF4AF2FE02F3EAEB07166A0,
	CharacterJetpack_Initialization_mF99D111A5B05C993704F6FFEBDA4CFDEDC3671DB,
	CharacterJetpack_HandleInput_mD98F9B4DF4E1CC086B2B19290EDAE3239D492BC9,
	CharacterJetpack_JetpackStart_m42B40D5AB64418BECCC02D07CAD2E65C7A639E6A,
	CharacterJetpack_JetpackStop_m8F32112309DE98A6EACC53D3756A24CA9B373227,
	CharacterJetpack_TurnJetpackElementsOff_m23D2C0DA822612AB03AEB82FA5CA84693983270C,
	CharacterJetpack_ProcessAbility_mCA38F174B38372103D40D1284DBAAC8A3F8787C4,
	CharacterJetpack_BurnFuel_m7DAEE33EAEB475A0B084B6F816DC5C70949469F6,
	CharacterJetpack_Refuel_mF3B861F9EC4ACA39411653F7E78E46B66D9C99C7,
	CharacterJetpack_UpdateJetpackBar_mA2F3AA114B817C26FC0FAE0EB3AE3F3C31417FC3,
	CharacterJetpack_Flip_mF25680C8DB1CDE21416ED2CF050A659593B01444,
	CharacterJetpack_PlayJetpackRefueledSfx_mAD682FAE5B61F0280FACCFA660E845532F30C9DE,
	CharacterJetpack_ResetAbility_m76BB3F736EB1751445AEF6ABAD6E59B55733A275,
	CharacterJetpack_InitializeAnimatorParameters_m06246D21D32D4CEDDD6E589A95B0050967CC00B3,
	CharacterJetpack_UpdateAnimator_m6AB55C5CFC1DFBC51D8A717FC2FD8842E7232100,
	CharacterJetpack__ctor_mE8B09B090BE99BE511A5682230CD870D9ECF04E1,
	CharacterJump_HelpBoxText_mDE5E84596E32613429F2DA70130CABD2B3D71A04,
	CharacterJump_get_JumpHappenedThisFrame_m3522D290C7D10AD3AE87AB0B2A4388F88FFF3E36,
	CharacterJump_set_JumpHappenedThisFrame_mFE0698A874A0A741408BADA19A9D727C1158B981,
	CharacterJump_get_CanJumpStop_mF146CECC4B529B366F3BE01D13DB5B9EC40674F4,
	CharacterJump_set_CanJumpStop_m3B47C533EBF571F54A7AD00F4C83BC26F6F50F9F,
	CharacterJump_get_JumpAuthorized_m343EB711FC1BB2127A1619056FA0FAE51090BC48,
	CharacterJump_Initialization_mB486F49AD8225AB4975C5682E7E0E78F3ACFFA44,
	CharacterJump_HandleInput_m4AF6DA51988DA51F924FF7829DD2BC347E681F24,
	CharacterJump_ProcessAbility_mCC5BC6151D9CA8DFD5EA2813111BA05710CB858D,
	CharacterJump_EvaluateJumpTimeWindow_mBBE81D6241209E4CDE31B24E274EAC2C640C203A,
	CharacterJump_EvaluateJumpConditions_mFF4C384FFF197CC6E19BF1C44704D2911D8F6606,
	CharacterJump_JumpStart_m46E870F1043E2A68E0E400036258E4EA2DC7A44A,
	CharacterJump_SetCanJumpStop_m347D41BAB7A7005BA103800A1158513F91E7F68B,
	CharacterJump_JumpDownFromOneWayPlatform_mF267F3712B2F28C60C8AA1F76B4AF492FEC27C8A,
	CharacterJump_JumpFromMovingPlatform_mA6393E231348C3005E6C210BB5B5F2F69A50780A,
	CharacterJump_JumpStop_mF42153B2690FE8F15ACE9C1E6CA6E751B6638EF9,
	CharacterJump_ResetNumberOfJumps_m4CC88B64D31BD0B195B4D030D75A950DC1B7FFFD,
	CharacterJump_SetJumpFlags_m138ED1D5F4FDB5D7B75987E8939461C9B7AB9A45,
	CharacterJump_UpdateController_m29F2D6D04A9163A75C7C75FBD546F552E855C9A2,
	CharacterJump_SetNumberOfJumpsLeft_m4B30D7DE3B11F7CA383C8113B9C4DE37A046EDC5,
	CharacterJump_ResetJumpButtonReleased_mC4AD72B4311EB1413998582C080A90C013F5CC2A,
	CharacterJump_InitializeAnimatorParameters_m47F0E80537E6352C30383CAAEABC43F1ABEB9F8C,
	CharacterJump_UpdateAnimator_mE7DF9762B523A01B59BE1B33B3972669412A465E,
	CharacterJump_ResetAbility_m8D92C29A98ED82969B073854EA1609B7FE87A083,
	CharacterJump__ctor_m83EA4AA8422B154777B31BECEB53A24D40ED2117,
	CharacterLadder_get_CurrentLadderClimbingSpeed_mF741CFCE119A9647BB7A68B37ABAE64894F6017E,
	CharacterLadder_set_CurrentLadderClimbingSpeed_m418E6C2FFAD4BB17C0805D1FA7BFCF7E1126EF0C,
	CharacterLadder_get_LadderColliding_m1CB99154D477CF73B581722C47550BF67B25A847,
	CharacterLadder_get_CurrentLadder_m47CED0A355E658F81EEDD3DFC5DCED321C000C85,
	CharacterLadder_set_CurrentLadder_mE763CBEB2C36423D079F9F5EA957336C4BDD0BA6,
	CharacterLadder_get_HighestLadder_m857BB0C178F96F91146675690825C95EB377D4ED,
	CharacterLadder_set_HighestLadder_mBE4B15F71B1C9EB4B186A9D9A1EE696BF8177F8F,
	CharacterLadder_get_LowestLadder_m4CC21572C05DE0F94CD5D32BB2E20C9FC761B3CF,
	CharacterLadder_set_LowestLadder_mB1B2927FDBE257A7CBE79E1C5E3935CD75B1DEEC,
	CharacterLadder_Initialization_mFBA49108115D37BF0C0D81FA30DDD68C0C3B7DEC,
	CharacterLadder_ProcessAbility_mB89918C0B95300A5755D58116128A8D903A2A5CC,
	CharacterLadder_AddCollidingLadder_m2FFC962E473B6AF920F4B0D766C854FDA0CD7BA0,
	CharacterLadder_RemoveCollidingLadder_m2C50CA133D8B05141D8E79E67A23856E3445E548,
	CharacterLadder_ComputeClosestLadder_mEF92737D896CA320003FE87703FE5506C1976D1D,
	CharacterLadder_HandleLadderClimbing_m699B0B6D9B9CE11448956838815F24D653412AA8,
	CharacterLadder_HandleFeedbacks_m6E768DFCF50A62611F389AC6A8FA4C58DE1FFDE7,
	CharacterLadder_StartClimbing_m39887A8BFB16578B43BC76A03C457DCFA13AB311,
	CharacterLadder_StartClimbingDown_m7038497793937E1C258ADAA735AB26B3FC66D945,
	CharacterLadder_SetClimbingState_m3F4CB4C26ABCF530ABFB9E8D6D5D18789FA2F83B,
	CharacterLadder_Climbing_mEE6178EA236BAB055B9057CB68EDC45A465576B0,
	CharacterLadder_GetOffTheLadder_m453CC335D7C0FE2E0CDE7BA53418031EE9669000,
	CharacterLadder_AboveLadderPlatform_m9B243E0318C0ABA2A661DFA52C07354179BC69B1,
	CharacterLadder_OnDeath_mA786C157F2F294E81A99EB8113514C8612F8A00D,
	CharacterLadder_InitializeAnimatorParameters_mE442C41A01FAB8B4B5C9724EED093DFB0C5B149E,
	CharacterLadder_UpdateAnimator_mF32F2391A6FF526B7210CCEB36B3A3F56CA57BA3,
	CharacterLadder_ResetAbility_m525C10B61B34F9074B0638EFE05F0B20D0500686,
	CharacterLadder__ctor_m252755798E393F6B4E69F59E85554A42C64CD0AC,
	CharacterLedgeHang_HelpBoxText_m44BE74C932FB2BAC022E81032ECD6F47ADD2460C,
	CharacterLedgeHang_Initialization_m19A09E9E5E888BF3AAD40BCC6875895F7A2034DB,
	CharacterLedgeHang_HandleInput_m23394E42CB858EAA4F50061D3C202F2E5F7EE130,
	CharacterLedgeHang_ProcessAbility_m897D3C9135171B9B9CCB40B014239A89102C00D8,
	CharacterLedgeHang_OnMMEvent_mFAC4697E6D1F9A34D27622DBAC594EBDD50CDBEF,
	CharacterLedgeHang_StartGrabbingLedge_mD7EDEFC2EEB26829633D9404B89FFBA3FB6450E5,
	CharacterLedgeHang_HandleLedge_m6D03BF145A8D54EF0B5B3238DB289972A319DF54,
	CharacterLedgeHang_Climb_mD48C6E5B8F47E0E1754AEFBFDAA443780A9FD672,
	CharacterLedgeHang_DetachFromLedge_m57B04D4D4133DA225AE992C1286CC89CBF9286FD,
	CharacterLedgeHang_InitializeAnimatorParameters_m847C49F557CB4880CB593989AF5A6C04526AB0F7,
	CharacterLedgeHang_UpdateAnimator_mBD4F877684DE376B3CB7B0B7A5FD5E43EC6B264D,
	CharacterLedgeHang_OnEnable_m00A8D583BC41D976127999DD160888E65FBBD987,
	CharacterLedgeHang_OnDisable_mE154E9F3A536E65A6CCBB6877CBE4188BDCF024E,
	CharacterLedgeHang_ResetAbility_m8AE283C9A2ED96E942775143BFB2BEF2548C8E63,
	CharacterLedgeHang__ctor_mD8E82C5342E059B5172CE34DB1B88D0A0655BF76,
	U3CClimbU3Ed__14__ctor_m37E350C3777D58F5F89A5794E4689EEBB5C6A5DD,
	U3CClimbU3Ed__14_System_IDisposable_Dispose_m58FFA52A9E3A1DFA18330EE8C49382F20EBA108B,
	U3CClimbU3Ed__14_MoveNext_m7E08F51BE05111CEE3E8F4A53FE2DD47613C72C1,
	U3CClimbU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFE14886A00E4D0C59D32593DC7CE90D02B13B821,
	U3CClimbU3Ed__14_System_Collections_IEnumerator_Reset_m46636A25507783A4EFF15FD0D84AAA45E89638BF,
	U3CClimbU3Ed__14_System_Collections_IEnumerator_get_Current_mE30A62629781ECEA058D1AF4F3C0E636D8FD95F7,
	CharacterLookUp_HelpBoxText_mD4BEC6E4291F7C95CE59B1C1056C583954EC8C16,
	CharacterLookUp_HandleInput_m2147EA5E7FA56D577FE48C50610791CE9E581BA6,
	CharacterLookUp_LookUp_m16579F1B391C369F957E385379C73DE3FBE3C815,
	CharacterLookUp_ProcessAbility_m79440B39223FAB6A0217E7EDABD135EA4ADE74AE,
	CharacterLookUp_ExitLookUp_m714D9ED818E16464C6743FCFA2AB1838CA2A570E,
	CharacterLookUp_InitializeAnimatorParameters_m27AA4363511A744538146A4C96CDAE898B2A67DB,
	CharacterLookUp_UpdateAnimator_mA3EF4BDF0CB826DA7F9106A92B221948A3E8E8B3,
	CharacterLookUp_ResetAbility_mE4EF2F13C635554598EB1235A0BB1D28CB1C0CAE,
	CharacterLookUp__ctor_m13D3A4A007134CF2F310668CAD15AD4331671F27,
	CharacterParticles_ProcessAbility_m1CA903DF95C99D8744F7A28B54B6F2920B8AB34A,
	CharacterParticles_HandleParticleSystem_mC794379577002F3CF262E838D6A8E90DE28789F7,
	CharacterParticles__ctor_m894527126FA8DBD2915091B3EC90D4E710DDCAE2,
	CharacterPause_HelpBoxText_mD25F40DF4C1EC1EFC2254A1FF0B2A1CB7275C858,
	CharacterPause_HandleInput_m6B87E58F38C62B926C1098D2D4AC9B57A27A1969,
	CharacterPause_TriggerPause_mD3F6F763036AE3A59701290284A79CA468852119,
	CharacterPause_PauseCharacter_mDC5073CACF2A06C97DFCBA7A30E29240DDCE4624,
	CharacterPause_UnPauseCharacter_mFD7B25EF833FC6FE401576BFD5C6BDC915DC0353,
	CharacterPause__ctor_m4B6AC64D4A41EDF867C4886B5C0FF31BA9C8C576,
	CharacterPersistence_get_Initialized_mCE134AC9AE76594C251054F7926D736356728817,
	CharacterPersistence_set_Initialized_mBEA794708E48987FBBEDCB5B3C527AB02B16D1E5,
	CharacterPersistence_Initialization_m1AE2B32FEF35F8C961ADDAE621EAB9419F8139AD,
	CharacterPersistence_OnDeath_mC1CC3BF4BB80152A7DA4617A17B67BBFC93BD03D,
	CharacterPersistence_OnMMEvent_m17F0CE4AF761021194BAE050C8D64D6B1A1D5265,
	CharacterPersistence_OnMMEvent_mA99F90E06D72FA537515B5B91FF6840663F82BB3,
	CharacterPersistence_SaveCharacter_m9BAAB1F813A7D5A26445F1CD517C61AD7DA00E3E,
	CharacterPersistence_ClearSavedCharacter_m868DD83AFFF5EE451E76CDCE4034ED79926A4045,
	CharacterPersistence_OnEnable_m217873FBD1DBB1E32A8ADD25231F6B75CADE696A,
	CharacterPersistence_OnDestroy_mFAB8620C18BF4E57052418AA709CBF5F3223080A,
	CharacterPersistence__ctor_mDCD174A49972633D6FE2E68A68100871EEC921B3,
	CharacterPush_HelpBoxText_m44AE0FE1FE0181A0CB8755FEB3181A5994D15F6E,
	CharacterPush_Initialization_m3426A73F1EA56FA89031CCED3A7B081A1DA1370E,
	CharacterPush_ProcessAbility_m69839AD0ACE8DE4BD476AD5A3223C30E8A68A8C0,
	CharacterPush_InitializeAnimatorParameters_m14DCB06F8DF2502E77FDD70C27C18BF033E2F044,
	CharacterPush_UpdateAnimator_mAE96E37D7FACD1235765302EF112AC1CC4890C51,
	CharacterPush_ResetAbility_mF733E6A9567872CEE531BA06F07BCCFDDCFB9CD5,
	CharacterPush__ctor_mA6B3208F0376E137369439946DA7EE3E4B43A8E3,
	CharacterPushCorgiController_HelpBoxText_m98D698793788FEE74539817A1D41AD626C6F72F1,
	CharacterPushCorgiController_Initialization_m9F36E999A8930D97D893A702B50BCADBF2FBBAFC,
	CharacterPushCorgiController_ProcessAbility_m7FD5C3E42C496B39EF09C46B9D9581EFC5FD1307,
	CharacterPushCorgiController_CheckForPushEnd_m2E818100133035584D125F17EE5A811FD6AB88E6,
	CharacterPushCorgiController_StopPushing_m8DB15144DD916948D56239188CC5727666AEFD07,
	CharacterPushCorgiController_InitializeAnimatorParameters_mA2B0A56353D53FE60EA00B9681FF4467F7B4DDB1,
	CharacterPushCorgiController_UpdateAnimator_mCB5A2DFF95EFFF219FA5550C618D3A4732833E68,
	CharacterPushCorgiController_ResetAbility_m93E18378FB446DFB24BC273B9FC6A26DCAB2C06E,
	CharacterPushCorgiController__ctor_m1EBF8E3B8BAC9D5206F9ADF26536CC3A40A33C86,
	CharacterRoll_HelpBoxText_mDDDACC20EC9D23C68BD7F8D0C7BA544E42DCC70C,
	CharacterRoll_Initialization_m59EDBC47AF75AE329A31CEB2A71B3C3A452001EB,
	CharacterRoll_HandleInput_mB23C4FD6500B04754841FAC1432F5FAE66B4B40C,
	CharacterRoll_ProcessAbility_m80C99742F5F11AEED2519150B2508DBB3E832D5C,
	CharacterRoll_StartRoll_mA51EA58D9B9400218912011C5785E01F9B35E67F,
	CharacterRoll_RollConditions_mEE1C3378CC6BB412304C47DBC51AB3E9C3841D26,
	CharacterRoll_HandleAmountOfRollsLeft_m7A0C270758B7838C6D81FFA409559C87932B540F,
	CharacterRoll_SetSuccessiveRollsLeft_m67EDC1BA2CD7772DC4AD169854D2E0EFDC5C9DAD,
	CharacterRoll_RollAuthorized_m72F3007471B2E962D21F2AAC953B1DC84A967AAB,
	CharacterRoll_InitiateRoll_m1A2C4AEFDB4D4B88E35CC45A73A6F1DABABA4D4B,
	CharacterRoll_ComputeRollDirection_mBF5EB40907852431D53139239A6142058B24D455,
	CharacterRoll_CheckFlipCharacter_m8F52C37D2C3E1A38F31D2C32657CDC231635C975,
	CharacterRoll_RollCoroutine_mCACFE8CE23A29A26BF894D43FC176B08E4CA9BA2,
	CharacterRoll_StopRoll_m47CF2F5A249E8E5CBB83D18FD1E5BC368E07CE08,
	CharacterRoll_InitializeAnimatorParameters_m708E410587CD9951EA948999564D86F3C21A5F47,
	CharacterRoll_UpdateAnimator_m10418C3D3F296508C9648B645C46F9F2266C806D,
	CharacterRoll_ResetAbility_m2B2CD53F68D0DB1FC0830FB5E35CF036355AA6DF,
	CharacterRoll__ctor_m84B1B073895BD526DEC990AB2BF69DA140C2D1AB,
	U3CRollCoroutineU3Ed__38__ctor_m1C6BFDBF640A37D6699926D87A2381771063418E,
	U3CRollCoroutineU3Ed__38_System_IDisposable_Dispose_m6D896ECD5500046E0C3FBD52644CB0A78FB0EBEE,
	U3CRollCoroutineU3Ed__38_MoveNext_m0B61D0EEA1F6D258C23F02C0E4425A8E085A9D74,
	U3CRollCoroutineU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD20C0F7A572918972853B8AADD236967BB59A715,
	U3CRollCoroutineU3Ed__38_System_Collections_IEnumerator_Reset_m20F2CC9BDF30427B4A9C2079A8A49C5118953C06,
	U3CRollCoroutineU3Ed__38_System_Collections_IEnumerator_get_Current_mB5C1744E033A0BFB2C7CFDF30CE8A8B1DC08262A,
	CharacterRun_HelpBoxText_m822B466F3653C18A8DAF48A9DDEA7F231ABDE7FA,
	CharacterRun_get_ShouldRun_m913343AA7BC27BB680A89970932DE92E462C7DA1,
	CharacterRun_set_ShouldRun_mC83E109B6E606021CB455EA99BBD6E2A240FE2D4,
	CharacterRun_HandleInput_mF853EA79814A42443DC4CB797E1EB094B60DDE6F,
	CharacterRun_ProcessAbility_mB91B8B32CED93A0BF36D83DFBEC4432606634EE7,
	CharacterRun_HandleRunningExit_m52F3A6CD112B408C08E87BB54A0E8A840808EFA6,
	CharacterRun_RunStart_m96C168B7BD6B4050F5110242E2353EB0B5A12404,
	CharacterRun_RunStop_mEA4B7CE3F78C1F9C85D9D94DC226FF6B4B16D342,
	CharacterRun_ForceRun_mE16EFD62D4AC4B7837D1D97D0F44940A8102E886,
	CharacterRun_StopFeedbacks_m47984A547C194994DF19A0D304560AE1B2B4BA08,
	CharacterRun_InitializeAnimatorParameters_m2AB2F2181B1D471C55C90CA1BE1007B531A2A25F,
	CharacterRun_UpdateAnimator_m3A04384A129E04B4F0878C46D9A9EBCDF7C484B8,
	CharacterRun_ResetAbility_m5E29BDC88040FA2A4EA429C5872B8C9EAF54340D,
	CharacterRun__ctor_m4E4C7CF843A83E930DF281F407F0DE920C1FC404,
	CharacterSimpleDive_HandleInput_m6C78003FF7033E8568E8FBA02EC5FFF77EE2C017,
	CharacterSimpleDive__ctor_m740681F3F56C0C4E8B7BE6B6344B04A1F335B401,
	CharacterSlopeOrientation_HelpBoxText_m641C09D7071C08CEBA14C2DC9E37860897CE0CC1,
	CharacterSlopeOrientation_Initialization_mEBCFD09FEB6EE271E8E39F1026DC40F99166C7C1,
	CharacterSlopeOrientation_ProcessAbility_m2D27B5493A28F186D8F772E1568881E388D33AC2,
	CharacterSlopeOrientation_DetermineAngle_m4EF6A0F661AD04C4A73740C0B2CFAA412405251C,
	CharacterSlopeOrientation_ComputeSlopeAngle_m028153D86FFF34EE13B13D743AAED8A109E2AD03,
	CharacterSlopeOrientation__ctor_m3FD8822BB498FC4850F2F05BC01F92CAB90E38C4,
	CharacterSpeedState__ctor_m2D23E9F206A2B051BFC3DFF219B7E0BEC4EF50EB,
	CharacterSpeed_LateUpdate_mD1211ED628DBCCE022E5134104188A93CDDF41C8,
	CharacterSpeed_CheckStates_m036E2FFD39B7F03231AA8AF85ECE806C6F5A1132,
	CharacterSpeed__ctor_m92847BACD87FCDF4B848C091BF1D1620273C4C71,
	CharacterSpeedAnalysis_HelpBoxText_mE61AF4A8DDBCB8DA71CF979D33FA525F7085CD0D,
	CharacterSpeedAnalysis_Initialization_mC9E2BDCCB648EC059F2CFA516414347A414FE93B,
	CharacterSpeedAnalysis_LateProcessAbility_mE97DD8B982412BD1C65D4DC98CFE30194EA924C6,
	CharacterSpeedAnalysis_Record_m851E725637A9CC3854D2B20D0BE76171B5BD72B4,
	CharacterSpeedAnalysis__ctor_mAE31F4852B6E0EF779B50B20067B6F08A473DCDF,
	CharacterStairs_HelpBoxText_mDA42F1DFEF878EDB4340173F1506077536877453,
	CharacterStairs_Initialization_m2014FB6C9B1E72AAEA76D42709CBA5BB790B5BFE,
	CharacterStairs_HandleInput_mF352B304D6509C98395BCF7591838A4C4B7EA95D,
	CharacterStairs_ProcessAbility_m48AC21C234CB59AE4CA402721F075FDC143D06CA,
	CharacterStairs_HandleStairsAuthorization_m76788A0A63276F5A40F4A62D9AA402D11B189A21,
	CharacterStairs_HandleEntryBounds_m8945E3BFD5936C443EEB61D38B6E7689B6E0395E,
	CharacterStairs_AuthorizeStairs_m25066A89C43DA7AB60E49B310920EB2B334C9457,
	CharacterStairs_DenyStairs_m4E8B4BB342900FD160D62052062FC04CAF725352,
	CharacterStairs_CheckIfStairsAhead_m0CD7406F2FDF51B8130B4BE673A54ACD84E20BA9,
	CharacterStairs_CheckIfStairsBelow_mF6C2778020E19072EFC56FF666E0D9EB3F90DBB1,
	CharacterStairs_CheckIfOnStairways_mAAF1612CCBC104636EB380DB4F39E87ABE7552C9,
	CharacterStairs_InitializeAnimatorParameters_m15ACA9B3E790392AE4E26EDDDD3BC453763F4377,
	CharacterStairs_UpdateAnimator_m5CB8DF23450872E5B4EE954DD6943CEE8F6EA8AD,
	CharacterStairs_ResetAbility_m29E88C811D96DE1EEF1466A6C9A91CEE44885B67,
	CharacterStairs__ctor_m7A9F4F960CA241F7288C070B73B1E9F6A0F41B6D,
	CharacterSwap_HelpBoxText_m55A83E80DFA868F347D5961542044D3BEDA0BD72,
	CharacterSwap_Initialization_m7F10BE81C7BD85714FB4CA4DE1D035BD85112FE9,
	CharacterSwap_SwapToThisCharacter_m3F08500772ED63B893F893662B3FEA38A1237E8E,
	CharacterSwap_ResetCharacterSwap_mC00460D52CB070C0552F84D6B6CBF33CBFD6BA02,
	CharacterSwap_Current_m710A4ABB26B72507167685C83FE8E1BA4237A245,
	CharacterSwap__ctor_m825D826E3210EC247F1223A01E576494B06ABEC4,
	CharacterSwim_HelpBoxText_m9166FE8606D00A2C7A0DEE0F1F0C007B0E21A22F,
	CharacterSwim_ProcessAbility_mD3675D2D5317D458F48271AE7A510C8ADAE19CDC,
	CharacterSwim_HandleInput_m69C24EEDE07103F418F9E08E20423D5F56F56BE3,
	CharacterSwim_Swim_m8D33F1A222F8FA1B2F163E13B621153E7A4529C4,
	CharacterSwim_EnterWater_mC9F5F99E1C09C93D016C6A8F9924F1B143756C84,
	CharacterSwim_ExitWater_m277F67996ADC64979E14A2FD1B10EFBE8DFF07E4,
	CharacterSwim_InitializeAnimatorParameters_mC89B9264B3E154DD07FBE564D0E812CED7A32BD8,
	CharacterSwim_UpdateAnimator_mC0FA360B38F8E592AE4ECDF32BA8EBE6FE2ACC35,
	CharacterSwim_OnDeath_m606A6ACA9AEBBE835C4C0281003EA28270C72497,
	CharacterSwim_ResetAbility_m035E440EDCFDA905BA19F43C23E733A413B46C27,
	CharacterSwim__ctor_m223F055D11DF172B0C2DFC0A861B0CF9E995EE0A,
	CharacterSwitchModel_Initialization_m9FE30129C6260D0D139411D6900FCAED103EE02F,
	CharacterSwitchModel_HandleInput_m9A57E35DAD333D966E5EDB9584517843A8EFA91B,
	CharacterSwitchModel_Flip_m4535A703C0B2944427A47E0F805E7B70E256DE91,
	CharacterSwitchModel_SwitchModel_m922C496A82E18BFFF9B7C33A80246D1779C43D4D,
	CharacterSwitchModel__ctor_mB572027A0805B3A5DA4A93BA9902B7D7B645CC98,
	CharacterTimeControl_HandleInput_mC00DD1F83A431F70306620E36CA5DA41563FE58A,
	CharacterTimeControl_Initialization_m078E854D6563D7805E07427CD6B659B289B6B3D5,
	CharacterTimeControl_TimeControlStart_m805C347DE7EBA5C0786E72E117AE0260E72620B9,
	CharacterTimeControl_TimeControlStop_mAB52DAD434DAC3E255E3722861772EB0AEE4786C,
	CharacterTimeControl_ProcessAbility_mBDC5641CD7970B9A23F1F62FA15775CE42882749,
	CharacterTimeControl_ResetAbility_m75ED039D99DA593C55552DA70DB7329FEEACBBA8,
	CharacterTimeControl__ctor_m1656DF502FEE4D73E85A191D55B184967A5A3D82,
	CharacterWallClinging_HelpBoxText_mEBE174970FF80F62F75BB5256A97C2489F5421DE,
	CharacterWallClinging_HandleInput_m45F7177B814D6EE046898DA659A552D9413C0E77,
	CharacterWallClinging_ProcessAbility_m2C2EFAA34FCB9651EE19E9BF556CE602DC2499A9,
	CharacterWallClinging_WallClinging_mB5D0CD095D1743BA6B936C653FD762FC9FBE6B1F,
	CharacterWallClinging_TestForWall_m22FC33F662D0707A8EA23B49B41BEB24A563D991,
	CharacterWallClinging_EnterWallClinging_mFFFCBC7D92D16726749AF021A1E365EAB0023EDA,
	CharacterWallClinging_ExitWallClinging_m039F0D8C3A2EACB83EDFC4621E781904124B5A7A,
	CharacterWallClinging_ProcessExit_m86C0E23559132CCC8C3ECD3992D705DF2CD1EEDC,
	CharacterWallClinging_WallClingingLastFrame_m50DF04C381EBE168B2EC0EFA003DD10F5686ABFE,
	CharacterWallClinging_OnDeath_mD2B0F09CCD5EFC7E09ABEB99F61CA3A3E0408FBE,
	CharacterWallClinging_InitializeAnimatorParameters_mCC4022284758A48023345069B3AE90896DCB05D6,
	CharacterWallClinging_UpdateAnimator_m62BC9D9783CE42B0F0CD8114E7CFF2D0004F7890,
	CharacterWallClinging_ResetAbility_mF28C2BBB84FB63A02B264786619F69E8A388A6BC,
	CharacterWallClinging__ctor_mB4F757937EDE00E184FCC212914FCDD20CF1906E,
	CharacterWalljump_HelpBoxText_mE6FD42B2A742B6678DACBA6C226D9774BACC204B,
	CharacterWalljump_get_WallJumpHappenedThisFrame_m94AFE760D539697067CA0EB8EED751D227116795,
	CharacterWalljump_set_WallJumpHappenedThisFrame_m54CA7B3C8F154A79685A0CB5F3F04F458B6162E8,
	CharacterWalljump_Initialization_mFC98394B5F4B87EDD2EC41466258AC66BE78F6E4,
	CharacterWalljump_HandleInput_m4D138FB23F9E013A7EAE312A542DDB87C1227895,
	CharacterWalljump_Walljump_m1DD39434CB5CF09407B04A77B351973DCC69DDBF,
	CharacterWalljump_InitializeAnimatorParameters_mEBD552AAE3F5DAAB0965B4E0CFF086E8A179AEB7,
	CharacterWalljump_UpdateAnimator_mDC8FD149BC13BDBC43724E133DCCB46FC1AF45A1,
	CharacterWalljump_ResetAbility_mFC65EF3BAFB227581AB43BF6E0E86277D824A844,
	CharacterWalljump__ctor_m894DB116B10501DB6A65D3DA011944A2A966D186,
	OnWallJumpDelegate__ctor_m7323EC40DC723745C99E57C637E3379C2F142029,
	OnWallJumpDelegate_Invoke_m537B66EB87074C2389485E52F2ACE11F00187103,
	OnWallJumpDelegate_BeginInvoke_m35459B9C2970D3BE7CB1B199FBB2703EE3B43106,
	OnWallJumpDelegate_EndInvoke_m176398E5C3EEBBDA340240F837133D785B5D2A74,
	Character_get_CharacterState_m68204DDDC24AD4F0905DFC1BA9199A59EE0C261F,
	Character_set_CharacterState_m45358ABCECB9FD83395651DB8F6ACE2943EE78B0,
	Character_get_IsFacingRight_m32FDCE8E5315D865719D4E84152E4573522C5002,
	Character_set_IsFacingRight_mEBD11075EDCF1BCD4157BEAC3F2DB6B2E4CD3553,
	Character_get_Airborne_m48A1CCDAFA86ABBEE197CB19108E41AC31784294,
	Character_get_SceneCamera_mAF0E69CEAC5DD036815E2562A0D883E68AF5D527,
	Character_set_SceneCamera_m07FE020D782C8521447BF8DD323D531E008DAE2F,
	Character_get_LinkedInputManager_mC3E5510A21C49F4F45F88FF080E2759CA278CA5E,
	Character_set_LinkedInputManager_m3DF4D2D90666A97F98570072161428B8908BE3DE,
	Character_get__animator_m4BF94C0891905F25D1EC6ACDAD620EDF9FFAF139,
	Character_set__animator_m9D7D5AA8ADA397FE1A3086A9E9B38073258DDC1C,
	Character_get__animatorParameters_mFF7C1B58BE37E7F9712181D65F5F57898D725ABE,
	Character_set__animatorParameters_m652403DF1D0832E6CA0CA5FA4384E465606F6C37,
	Character_get_CanFlip_mA55F6B5B5841EB91BB449603DF4D4DE0756BC9E5,
	Character_set_CanFlip_m332A336C9D5E1D9C865D20C7754BB26A481D356E,
	Character_Awake_mFB256746E019FFE1185B1BA426E9342E93D898EF,
	Character_Initialization_m637BEC2E867C1D56003EECE7710B62D2CCB1607A,
	Character_GetMainCamera_mE2AAB23FB104AD75839252C21D469B9F6546C1A5,
	Character_CacheAbilitiesAtInit_m085DD2AE0E4A7A840751175E1CEE566739E8AA8D,
	Character_CacheAbilities_m78EB1FCE491C71C4F879F384C44CE3F3407EB3F1,
	NULL,
	Character_ChangeAnimator_m7BDE131DE149CAD479D5E8C67D3D67DFB3FE269D,
	Character_AssignAnimator_m84A644E2090E273FBC0CC6312D1FDAE74154E8E6,
	Character_SetInputManager_m2C82433F78A1054A6C49C01F8179985CF2526DFC,
	Character_SetInputManager_mDCB964751A6BA65660E52BA2C1B91C36CAC79DB0,
	Character_UpdateInputManagersInAbilities_mB13FCCF55C265DD8275D430B75641C849777DAAC,
	Character_ResetInput_m41488BFD7AFBD2014FC38E3E0FEAA0B2F10F277F,
	Character_SetPlayerID_m060CB74776697D1020D799B44015CD9276237FA5,
	Character_Update_mD84ABA5AFA7DB1BAA4EBF15A817EFB46904D8C47,
	Character_EveryFrame_m9640EE31870C841DD625A4330022054050575543,
	Character_RotateModel_m847600579DDEBE33BBFB75B3F1E097CA26DFB7D4,
	Character_EarlyProcessAbilities_m278A86B7F7FAA5C5AC5E4E67BE59681DAC56DE20,
	Character_ProcessAbilities_m857B3A09B79FD6E9905D114BC83747CFAEE381AB,
	Character_LateProcessAbilities_m32800B0FCCCE0FFEE599822155B328952002E5C9,
	Character_InitializeAnimatorParameters_m7A8B59B5927E38321B74578185E013BBFB7D73A7,
	Character_UpdateAnimators_m7B088454F4DC64F27DEF86E314DD42EA6FE30924,
	Character_UpdateAnimationRandomNumber_m0B350D9CD9A54D76CDE3821E81CCF3B2003D78A5,
	Character_HandleCharacterStatus_mB49C1AECE10944EC19796F420D4B857B61587A9D,
	Character_Freeze_mC3A3EDEBBC34785DA35B7D7D8BAE88BBDF93293B,
	Character_UnFreeze_mF1CAD4A50FE03916C58388F854EF8C152D3829CC,
	Character_RecalculateRays_mF42A4916FEF6F5F3A306ABAC4015DBD42B3CEBED,
	Character_Disable_m2E49B3ECEC14CFED346B786D1F4CAC59F78AABB8,
	Character_RespawnAt_mE7CCB58816077BDE8482F0F18D4122A6E9BC6BDE,
	Character_Flip_m25AFE2C7A62DC623827CE9F879C4077FDB95569C,
	Character_FlipModel_m88F35EA64E018EE251C2AA1213DF9B8812B5BCCB,
	Character_ForceSpawnDirection_mF32A6D828703D0DB86C741E386D81D4674B5D4F2,
	Character_Face_m79A182BF3AD841B75A579DAA4890FBFB22FFDD57,
	Character_HandleCameraTarget_mD8A6C4ADF8A938D2EF927BBF30E99ECBD2E53F2D,
	Character_SetCameraTargetOffset_m55565FDA7BBF89953EA60632AA45E036916A2714,
	Character_Reset_mB5F1D1BBDC23E0560AEFAB3851BF86133F52D1C8,
	Character_OnRevive_m460F682C2EABC329CC0E14440B85B67A5673D6FB,
	Character_OnDeath_m1275A971A9E6B8F852A6AB7E2797C5FC21255DA1,
	Character_OnEnable_mB9E41FAC1B74F8397361EAAEA443C03BBB40995B,
	Character_OnDisable_m864BBF0ACB67782C1A4537FFF45B9E012F28C49D,
	Character__ctor_mFB835AAF7B6F56B25876C0DCD0A92338F6FF64AC,
	MMCharacterEvent__ctor_mE6B7788A4EDF79D0DEB62E4E58569FA2AE2A4D52,
	MMCharacterEvent_Trigger_m4DDAB2F799B2C28587325C02281FFEB9E2EB019F,
	MMDamageTakenEvent__ctor_m0BFC19EF73E4AC6B6B61A5B7AE47EFAC5E1FE6AF,
	MMDamageTakenEvent_Trigger_mF8E7654C1AA20B2FA2D8DD81DDE6730113F5D774,
	CharacterLevelBounds_Start_m63A09EC2B51A6BF5209339B6E6E3FA769959AF26,
	CharacterLevelBounds_LateUpdate_m23C6E6DE601DCD56BB792D4C662B567C40EF4F91,
	CharacterLevelBounds_ApplyBoundsBehavior_mD6036D72D009FD582F5F6C59B1CF6C527AF2CF4C,
	CharacterLevelBounds__ctor_m89F483452BD3C651CC3E3841455454B9E0D2DE8C,
	CharacterStates__ctor_m69684410BA96D578CF5071CD60872BDA49212904,
	CorgiController_get_State_mD9A5220A2421FCD11F87C7ACAE0AF5B69EEA399E,
	CorgiController_set_State_mAF2166592DFF75C26B8E7042D37EA8C88A5BDAD1,
	CorgiController_get_Parameters_m41B53F710860E4AAC7746732175D094B027A423B,
	CorgiController_get_StandingOnLastFrame_mF75E7ADB576D63A40BA1C3FDCE1403308E87C7F2,
	CorgiController_set_StandingOnLastFrame_m1AB075CB3EC715F77F80E9A57A6608631DD780CB,
	CorgiController_get_StandingOnCollider_mC249AC30C7C2657659EE255D268BD085DE3DEB34,
	CorgiController_set_StandingOnCollider_mBBC7D52AC19C045BEC0F72237595090CDBFF1C27,
	CorgiController_get_Speed_m0778B74AD7E953D3C9D370E8F65EDBA345589DBE,
	CorgiController_get_WorldSpeed_mF63A3A1849EA831B3134F76F2EC996DC9CCE4D34,
	CorgiController_get_ForcesApplied_m391574DB371DD71651E8024381474279928A4591,
	CorgiController_set_ForcesApplied_mA335D2750CB89C9CCD4B5A6DB3ECCB2A1ECA15C0,
	CorgiController_get_CurrentWallCollider_m7410ABF1E99AB735A317678AE1147F080527E3E4,
	CorgiController_set_CurrentWallCollider_mC41F3A8AAF55DBAD0916E2055FFEE36716CC3473,
	CorgiController_get_ColliderSize_m653C621A7F25C185D5027BD9C98B8BD7E5C30480,
	CorgiController_get_ColliderCenterPosition_mEDCDF25B290BAC4BA6AE2384C16016D824914374,
	CorgiController_get_ColliderBottomPosition_mFB531F139F5F5475EFFC375C1AF3DDA8876BF97E,
	CorgiController_get_ColliderLeftPosition_mB67C36B277DDF6B639AAEABB5DEB49ADA65362DE,
	CorgiController_get_ColliderTopPosition_m89C6B1B1118877DDA5F247F271F2EB0654A838AF,
	CorgiController_get_ColliderRightPosition_mFA67CC2967C59A9C4D017EAD5DDC2AD6AB25835C,
	CorgiController_get_IsGravityActive_mEB4B8C8B49CD7177337C2755FECCBCE4DB37D338,
	CorgiController_get_DeltaTime_m5A258C89B26CA795BDB172CD31020A62659639D8,
	CorgiController_get_Friction_mE114188A7E5940CC3879D90EA47DCD3EECC2EA25,
	CorgiController_Width_mC1214B778477D7FBF830574134527CED7B8D92B0,
	CorgiController_Height_mB035A67AA6325F750D63EBD76DEC19195BF9A0BA,
	CorgiController_get_Bounds_mB32E24DA765C10C9B80B877E8CE74113368035CF,
	CorgiController_get_BoundsTopLeftCorner_mA5701D30927165EB119B6ABE9BAF87C3B958A112,
	CorgiController_get_BoundsBottomLeftCorner_m54B35B01561656164C96BB4155D0A81E04F7C5C2,
	CorgiController_get_BoundsTopRightCorner_mF244F6B620B420C6EDE8906160F1B71329C622FE,
	CorgiController_get_BoundsBottomRightCorner_m3E471F0A5C7BD54D932CC4FB0A56CF1A8526AE6A,
	CorgiController_get_BoundsTop_m95A6F0F3282688418AED0D5A9FD77643F5EB066D,
	CorgiController_get_BoundsBottom_mF01AB5EAA7A30A61E8BF39514239E46427D5A769,
	CorgiController_get_BoundsRight_m59999362140641938B2344A974415C09752A725A,
	CorgiController_get_BoundsLeft_mEB01DBDC5F23B5EED436C56CE11E0C603518BF76,
	CorgiController_get_BoundsCenter_m9A7F60F2EE20BF6B598765AAA9956ECD5C659DDF,
	CorgiController_get_DistanceToTheGround_m5431BC2330C1030E29DF4D95D7798FB0C0474AA4,
	CorgiController_get_ExternalForce_mAC7BED9C9B8638A3E3500AEC806704339F3A9F9D,
	CorgiController_Awake_mD31F0EBCDC304466CB30700C464BEE8A764F0B56,
	CorgiController_Initialization_m1C1A7DE9A938C86123675288229F36AB236E071D,
	CorgiController_ApplyPhysicsSettings_mBE177C9A753DDBD98E236465B903DE4D46CFB796,
	CorgiController_ApplyGravitySettings_mEF501CA833A8ECA7CFA9C300B1A2266531529C62,
	CorgiController_AddForce_mD29E6C1B989CD3DBC77204CF6B5FD807351FC310,
	CorgiController_AddHorizontalForce_mF17EF2BBE85EF4B2443356038DDA4D93D4000723,
	CorgiController_AddVerticalForce_mE8AE53FF733D0226412315015BA5B5E337E93432,
	CorgiController_SetForce_mBE673B74C42FE562B64C9BE191FC483D13060D53,
	CorgiController_SetHorizontalForce_m4AC33AFB81A3D76B50D707F4231D7FA8EF78A2F0,
	CorgiController_SetVerticalForce_m984E8CF61DCC6BBB0F229BDEACB2DC7D2BEF3D26,
	CorgiController_FixedUpdate_m51564BE55240E35EAD8C3B08561046B196CC952A,
	CorgiController_Update_m8727CC305C6A27BD0CCCA0D81BFD039DA26DB48A,
	CorgiController_LateUpdate_mEA2A085F9F4F50BB3FC0961B1BACF222A933DAF3,
	CorgiController_EveryFrame_mEA547372531D16FDE334A014055F4EFC64E2A42A,
	CorgiController_FrameInitialization_m8C6B23AB67EFE5A1C9664D3FE5648A4545B8AEF0,
	CorgiController_FrameExit_m6388A506E6D48D142E7716B7AF2F5C62A1F684C2,
	CorgiController_DetermineMovementDirection_mCA744F3E3F8A2A4DBFAD66168DB499DFD9C065C8,
	CorgiController_MoveTransform_mCCA21A455F50EDB7EB6399D6DD6D96FDD81BCD3A,
	CorgiController_ApplyGravity_mDCC7601C5553AFF07FB71A2734E19020DFC97117,
	CorgiController_HandleMovingPlatforms_mB46B8EFAE7F8496D3BABE911D01AC8B43497F3EA,
	CorgiController_DetachFromMovingPlatform_m92CCE5ABC530ABCC9F5CEA68A10B205D67CF4916,
	CorgiController_CastRays_m61FB71FBABEF808B6B6D84EF4555FD2DA0D62998,
	CorgiController_CastRaysToTheLeft_m332E5DF7EE42865D12B814A49EFD787C60A6D807,
	CorgiController_CastRaysToTheRight_m60A9CB316960762C747107CF7CACC836B85C1403,
	CorgiController_CastRaysToTheSides_m10887BE20A237884889FDCA17A2F027E82EE0FB2,
	CorgiController_CastRaysBelow_mEEA1AF96A2851736F17BB433AA18FE3F427865ED,
	CorgiController_CastRaysAbove_m1634EF0416DB5352161564D6EFC7D715A3FE07C5,
	CorgiController_StickToSlope_m79872A8A96B1B25BD75ACA207D562D4CF7278C1F,
	CorgiController_ComputeNewSpeed_m927F0367AEF7D078EF1C60CB8204BE7889423535,
	CorgiController_ClampSpeed_mD7858501FA1858D9E7BEB61B0D77796A8D8E209A,
	CorgiController_ClampExternalForce_m8AB80AD2640106EC3EE0CB4D623C4527BF710743,
	CorgiController_SetStates_m2742CD4CC2908FDE6256D88A67E6F7666F667074,
	CorgiController_ComputeDistanceToTheGround_mAD9FC0C675226B21A2643B80193564E99B36CC6B,
	CorgiController_SetRaysParameters_m856588FF304A10E23BA66A17E976EE1C791E6AB1,
	CorgiController_SetIgnoreCollider_m6A62811AB6C0B5AB62F142D7A38A00C8F67F8323,
	CorgiController_DisableCollisions_m8BD6564FD460A18CC0BBB015FA587CD6362475D1,
	CorgiController_CollisionsOn_mBE525E4DCE59DABFF9BE65A1A671D3BF57F8C298,
	CorgiController_CollisionsOff_m16FD771561D503E206F3011115BC976C9699AC34,
	CorgiController_DisableCollisionsWithOneWayPlatforms_mB7C8EB0EA2F458B912F2845255987C3804761DD4,
	CorgiController_DisableCollisionsWithMovingPlatforms_m917B7957DFE4C0B3262E31231AB93F990A60B1E8,
	CorgiController_CollisionsOffWithOneWayPlatformsLayer_mD6064A0E4D4C01A66D9773AF5598C07D06569771,
	CorgiController_CollisionsOffWithMovingPlatformsLayer_mA46BBCCBE77705FEF49EFB5AD8B862FC7FC45F9E,
	CorgiController_CollisionsOnWithStairs_m0BAAD50CA1E516AE1ADC8C18B1A67167F493758A,
	CorgiController_CollisionsOffWithStairs_m4B41ED3D66F57156C62424DF8A092131E7882CC1,
	CorgiController_ResetParameters_mB4A9F3FC567B91E3E748D5FDDD1E412CEA0B2B96,
	CorgiController_SlowFall_m0C2DF75592A94C9CEC7FBACDCFEB8A9D7B0768A5,
	CorgiController_GravityActive_m7C5478D6AB8F19DA7F3CC2DF317CE504018F7344,
	CorgiController_ResizeCollider_m444F3F162A02876BE6754F42D024C8B95285C96C,
	CorgiController_ResetColliderSize_m17ECEFE020F1EFBFE424C72F1B3CC9CAE5774094,
	CorgiController_CanGoBackToOriginalSize_mA265A35AED6C7BD949647C91E1089E096776DE50,
	CorgiController_SetTransformPosition_m3749033CA3B68533F25F0DAC7DF38B85B64A3CFE,
	CorgiController_GetClosestSafePosition_m29C0A2E0637981759B9E345C4DA96EE91DD87689,
	CorgiController_AnchorToGround_m7DA4E85C46B78A517546BCC21360B771EF29E548,
	CorgiController_OnCorgiColliderHit_m52C74DBFD58E340423702BFD864B13D905EECD32,
	CorgiController_OnTriggerEnter2D_m7F2015A6FCB89C96264C29AFE4FBA1861A7E684D,
	CorgiController_OnTriggerStay2D_m7DC6F27C64392FD9D0C648E82743D4DE12FADF73,
	CorgiController_OnTriggerExit2D_m5AE8B9692466F50004426337A18617D01F4E9B4E,
	CorgiController__ctor_mDC3C29E67F85BED36C7D3F0899FA1881953B8445,
	U3CDisableCollisionsU3Ed__198__ctor_mF44F0091754A0EFA15025BD63AC8131B27CA963E,
	U3CDisableCollisionsU3Ed__198_System_IDisposable_Dispose_m2C8DD91413592FF312F5EA00F9F7CB0F4BEB0640,
	U3CDisableCollisionsU3Ed__198_MoveNext_m2D09586373F670D40B62652BEED53AA61B7C1C91,
	U3CDisableCollisionsU3Ed__198_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0671FD7547B12CE3686C8B0ED64C9041D3BC42B5,
	U3CDisableCollisionsU3Ed__198_System_Collections_IEnumerator_Reset_m4C210A97910CD3F8CB161263DF2F1AB7CB82632B,
	U3CDisableCollisionsU3Ed__198_System_Collections_IEnumerator_get_Current_mE02E60DB5C782DB99A674193252886B49DD05C49,
	U3CDisableCollisionsWithOneWayPlatformsU3Ed__201__ctor_m731AE102A4B9E6026AE38130AAA9C0472C8C07C5,
	U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_IDisposable_Dispose_mD786ED9F56FE48363957F3D58D07F25643B455DA,
	U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_MoveNext_mFFDAF70F638FB7111FDC37855399A992559BF53E,
	U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m808AD5B1D89BF1A300F2B5A08A8C357DED20AF3E,
	U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_Collections_IEnumerator_Reset_mD5ABB7A6FB6D1D6D6E825A8190526902505B2948,
	U3CDisableCollisionsWithOneWayPlatformsU3Ed__201_System_Collections_IEnumerator_get_Current_m8927C225827CD87B3A96E02F9E188580E7D90CA5,
	U3CDisableCollisionsWithMovingPlatformsU3Ed__202__ctor_m71B3F449CEEB48110ABB7655FFF1745C83CBB6F7,
	U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_IDisposable_Dispose_m0933A9CD0FE2B0636A84F4F36D9DA7D6CC0F73D5,
	U3CDisableCollisionsWithMovingPlatformsU3Ed__202_MoveNext_m12CE5197A391E1591928DD7FE05FF6317A6391BC,
	U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC8BE89F96AA0CFE55F68BAD3CDAE0176FFCB5D9C,
	U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_Collections_IEnumerator_Reset_mBB903477FCAAD9EAE3404FEE5D035D7D8B0B0CB8,
	U3CDisableCollisionsWithMovingPlatformsU3Ed__202_System_Collections_IEnumerator_get_Current_mA97872466788AE53B13374C4A02EB504959BA840,
	CorgiControllerParameters__ctor_mCD9818D33171699B1040F736B1ED3ABA25E16297,
	CorgiControllerState_get_IsCollidingRight_m5E055D9DE70761CF6A7EB800BB613A1EDD233D3D,
	CorgiControllerState_set_IsCollidingRight_mBDAE2D2B87262E4F14B362FBA070A492BC1105B6,
	CorgiControllerState_get_IsCollidingLeft_m1DB1E7A8B2A8252A9913AF20BCC503CCC0A207E2,
	CorgiControllerState_set_IsCollidingLeft_mD6B285F6FF980BBA04B56FE66527DC26806E39AB,
	CorgiControllerState_get_IsCollidingAbove_m3B3F21C54217C16D3CEAEAB701619AA6E425888D,
	CorgiControllerState_set_IsCollidingAbove_m66B77601CB198F2F3C653125143AC67BD9C241C7,
	CorgiControllerState_get_IsCollidingBelow_m9B41EB0712BBAC49C5C1447CB97A4CE186340EC0,
	CorgiControllerState_set_IsCollidingBelow_m6C72E7251F4439949291010C28306A5FE467CC1E,
	CorgiControllerState_get_HasCollisions_m7B8E1EB04C4E98C81002EFB47AB3CD8E84B2B808,
	CorgiControllerState_get_LateralSlopeAngle_mB86D998EFF29270E59CC7E39028DF7EF4B387624,
	CorgiControllerState_set_LateralSlopeAngle_m7CA8FFF77D5C7A93AB4299968B021AC45E94BD32,
	CorgiControllerState_get_BelowSlopeAngle_m475A6925EA7BD5E664C2A957F4258437BA7976F2,
	CorgiControllerState_set_BelowSlopeAngle_m88005661C7E72332BE414B6E3509F6385C9CE943,
	CorgiControllerState_get_BelowSlopeAngleAbsolute_m3D708DF41C91C62BD96B4470AB914E624F4C0750,
	CorgiControllerState_set_BelowSlopeAngleAbsolute_m17BD9785062B6C615000669DB26041B691839B3E,
	CorgiControllerState_get_SlopeAngleOK_mF4DA59E920D8B8CBD0010709680C471A68B59860,
	CorgiControllerState_set_SlopeAngleOK_mB99AC173A98113BE09238B1EFCFDEF772D76454A,
	CorgiControllerState_get_OnAMovingPlatform_m43996CF0791875751C92E634020691FEE4F37A4D,
	CorgiControllerState_set_OnAMovingPlatform_m7A1B1899D16A3AC05A4028ADDB2F5DD9017ECD97,
	CorgiControllerState_get_IsGrounded_m522D2A3105AC737DE8C45C16635DBB40E4C9FA80,
	CorgiControllerState_get_IsFalling_m532AC0C8363A1649CC634B660C898BDCA9BD20C1,
	CorgiControllerState_set_IsFalling_mAD62F15191B70C561610994B46583C6658029228,
	CorgiControllerState_get_IsJumping_mAA87D1F71654E347E9D3A899BC1256C7EEC826E6,
	CorgiControllerState_set_IsJumping_mFA8491028E9CDF47E1EFED840C17D3183DEA9FFA,
	CorgiControllerState_get_WasGroundedLastFrame_m3571F51506565A217F5EBF5F1167844B93BDD739,
	CorgiControllerState_set_WasGroundedLastFrame_m4544D35A347CAFE5A9E5580CEF5298C1A658E024,
	CorgiControllerState_get_WasTouchingTheCeilingLastFrame_mAC0AD82DF03A3587C2D29674FA85852E5F9ABDC8,
	CorgiControllerState_set_WasTouchingTheCeilingLastFrame_m6EE0B50B1419C7FC18180908BE82434CC9B446D4,
	CorgiControllerState_get_JustGotGrounded_mA1E82A8871CE4CC14141743B17B19670787EED94,
	CorgiControllerState_set_JustGotGrounded_m04B0B387B7A940DDC778AD74599B175F36D00E85,
	CorgiControllerState_get_ColliderResized_m3F7772B4510BB4075405C0DC06F5D1C61B588E5B,
	CorgiControllerState_set_ColliderResized_mEF258683C28922A1E0BDDF1E7D40273CEDBD37E9,
	CorgiControllerState_get_TouchingLevelBounds_m9AFEA95CF894487EC23DDC679A56E7B07429DA3D,
	CorgiControllerState_set_TouchingLevelBounds_mBADA7E2EEDB93E8E39CF6AF6B5E727885DCD646E,
	CorgiControllerState_Reset_mEC56055D828626CE61B5B36A06D06F3D947566CF,
	CorgiControllerState_ToString_mB50B2AB34A1C56BC7E7A5554333B64233D4C1253,
	CorgiControllerState__ctor_m35D3A0A21F8683F3A2DC5FADADAD497ACCAE8309,
	DamageOnTouch_Awake_m3BF53D821ADAF9ABF93DADFB63CC4D3A64C3F4E7,
	DamageOnTouch_SetCorgiController_mD33A54A66338EC1C1C77DB2244D76F833E07F90C,
	DamageOnTouch_InitializeFeedbacks_mB01ED16B5E14B3A1A8CC178BF1B5CF7B30BC4B64,
	DamageOnTouch_OnEnable_mD8F09C64E027EE33FF691A0C4A53F3CB7888079E,
	DamageOnTouch_Update_m4DFC0071794313A424231A414BCB9491F4DCCFFF,
	DamageOnTouch_IgnoreGameObject_mF0E3D6A5ADF76D6EAB16AA38E755995551390B83,
	DamageOnTouch_StopIgnoringObject_m0F14F070F68C8515E7BEBBAEBF28A1DD7EEF7EF8,
	DamageOnTouch_ClearIgnoreList_mB197BA56BB853DA6308559992213222736B79146,
	DamageOnTouch_ComputeVelocity_m60E06E778785BD0DEC49CBD2A4F16504AE3C219A,
	DamageOnTouch_OnTriggerStay2D_m30F90AAFF2796A998EB098C822AB429BF62ED241,
	DamageOnTouch_OnTriggerEnter2D_mBBDE1EE2091F6D55D38637E086DB39FA2755AD67,
	DamageOnTouch_Colliding_mBB3A7F28F1B4E3AE445A8D68350E36223861CCF0,
	DamageOnTouch_OnCollideWithDamageable_m41C9116F7D8D5E4CC8B5AD311C0A65250ED19147,
	DamageOnTouch_ApplyDamageCausedKnockback_m2344DDDA703D596CB056488745B33C3CCCC0BBDE,
	DamageOnTouch_ApplyDamageTakenKnockback_m1A4C3F7E2B127ECD903C53F41D255E3463157F52,
	DamageOnTouch_OnCollideWithNonDamageable_m539BC0ABC05CD11B3E1C316C73E8842C90FAAFA2,
	DamageOnTouch_SelfDamage_mF373FF72DB3B6994B3EEAD21E168B3FF866AAD1F,
	DamageOnTouch_OnDrawGizmos_m2FAA67F4EC8490CEA809003019F6A4755D65F02C,
	DamageOnTouch__ctor_mD63C365D24B33B4FF7265CB4533D202402BA78BB,
	OnHitDelegate__ctor_mFBC14D88D4C97ADE06768049AAB21A12C24A3B32,
	OnHitDelegate_Invoke_m79568188F1F5016DD62CE77AA8B1ABCCF797B424,
	OnHitDelegate_BeginInvoke_m64D346AC4D7598DD55A567A737AA057FDD00B7DB,
	OnHitDelegate_EndInvoke_mF9A85B66956B32689DDD97B92D68ACA0FCF031E7,
	KillPlayerOnTouch_OnTriggerEnter2D_mC23362B9E6E012485A549DCC92C1AD9EA09BACB7,
	KillPlayerOnTouch__ctor_m225506A31006C99253ECB186864767C9506D4636,
	Stompable_Start_m3E188EEFDF86E9AAA7489731DFE010AD4CE47BF9,
	Stompable_LateUpdate_m7DC09A30137A600385A9AA298364D3C9C22B922F,
	Stompable_CastRaysAbove_m100273F6D0FBFA2E89595A50229294D0F2D116B8,
	Stompable_HitIsValid_m5C89ACE4B9C0A22549A39C7025E4E4817FCD5C4D,
	Stompable_InitializeRay_mDCEC5745B3E3FD7440F50E4FB534D402FB1DC994,
	Stompable_PerformStomp_mA98FE695E6C0C5F30DF9A5B65577D342F94E533E,
	Stompable__ctor_m62F9819D9D91BDCE17E885CD55471230BEC5E823,
	Health_get_LastDamage_mD22C96C3F4DCBD4504335E27B75C576DFD949150,
	Health_set_LastDamage_mC623B6A075FAD4491C719387D7AC9EF28007DCFE,
	Health_get_LastDamageDirection_m1ABAC9322C17725AB1C03107FD77EB3783EDF576,
	Health_set_LastDamageDirection_mA50F89BA00230490CB99BFC0F600174995E50162,
	Health_Start_m66B163A28D25277F082B151E26802C84BFFB6EAE,
	Health_Initialization_m5FA1B03AFC34508F445DE3E0EC3A8C57353B7237,
	Health_StoreInitialPosition_mBA109269E0810BEF698CA0BE909EBCBEE372324B,
	Health_InitializeSpriteColor_m03BF06871A4618E6636EE9FD097ACDEC2DC5CA10,
	Health_ResetSpriteColor_mFC858D9EF56EFBC621793B7F9A2592EA8AEBFC5D,
	Health_Damage_mB2C11834434E052ED36D93A0B10F8CAD2877049C,
	Health_Kill_m6E1EE3396B9C526C7CBAE4192AB958DFE96BF01D,
	Health_Revive_mAFF5C4E1ADA175545E7B5C503B11EE2824B39C14,
	Health_DestroyObject_m82B9173DB256A1070184604508F661A2AB47A7F3,
	Health_GetHealth_m99A611A1CCC7B07666AF7C909BA279AB18AD8A23,
	Health_SetHealth_m36846027897C16E97C6823E85C09C1CB5FAD4CCB,
	Health_ResetHealthToMaxHealth_m6F11008EA41889C8FC89438D2BEEEAC9763DC548,
	Health_UpdateHealthBar_mDBBF761AF1DD362B7882925EF60735118AB18337,
	Health_DamageDisabled_m6AE8AAFD06C916C603E0D23CAE0F8409FE8FE54B,
	Health_DamageEnabled_m4D8D5AC3DC1815C77DFC4D357E649E221B855CD0,
	Health_EnablePostDamageInvulnerability_m700AD9A4BA16A435F603712C6036D4D209BAA23D,
	Health_DisablePostDamageInvulnerability_m16AD3ADA2D6623704457E902058878AAB6D66EBF,
	Health_DisablePostDamageInvulnerability_mF847417B0F14C5FAA3D08E1E3FD876F8FB634570,
	Health_DamageEnabled_m50CEC9C15AA2120DB45E58B07788630D4B3CC5F8,
	Health_OnEnable_m8BCC03E38EF3E6149D4206D23110E99DE9784CAE,
	Health_OnDisable_m0457D1D61D080718EE3316D297CC6EE3B6389170,
	Health_Update_m133803A77C9642EE3F9995773ECEFAA2CFC7BE4C,
	Health__ctor_mC4AD594519EE02992BED5B75AC1ADDB3CC0F25AB,
	OnHitDelegate__ctor_mB9BDFF74215D777D81C9AD61BB8FD7C3E6A6FF05,
	OnHitDelegate_Invoke_m54BD00801CA9F530CC86D5F09C6145153EFF417B,
	OnHitDelegate_BeginInvoke_mA3E934711F3040414417C7116706FBC1BE5A8CEB,
	OnHitDelegate_EndInvoke_mE69101087B2743DB48C6D412BFC9102AB65DE435,
	OnHitZeroDelegate__ctor_m90565A40CD38270C18E13800DA83078D1712A778,
	OnHitZeroDelegate_Invoke_mABD708A076CD158D0A6BA78009D0BD65D60D32F3,
	OnHitZeroDelegate_BeginInvoke_m876BF1CC669FB235D7B6E0FFB83ADBE3FB5AE9E2,
	OnHitZeroDelegate_EndInvoke_m0011CE610D4423C9C2B060F52AF28D7EA8F4D0B1,
	OnReviveDelegate__ctor_m09326A5F5670740073774F997F723D17AA7F4204,
	OnReviveDelegate_Invoke_mBEBB92BC794CB1961B780FA5561B0E4612975244,
	OnReviveDelegate_BeginInvoke_m1F6CF908B13354F1279CCF2DDF17919BB582F3B9,
	OnReviveDelegate_EndInvoke_m9717C7E9C043F43C61043646B9B42C654464DDBD,
	OnDeathDelegate__ctor_m890BD0E273FC72CA8A93254B7553228C3D97FB5E,
	OnDeathDelegate_Invoke_mAD2BCCB85275C25B43BCDCD8B15CD47C6077F8C6,
	OnDeathDelegate_BeginInvoke_mA6776B2CFA48691D7ACD8EBCC727E52A539B92DB,
	OnDeathDelegate_EndInvoke_mEC73B9EFED35CA55A0403CDB92077BAA5118BFB1,
	U3CDisablePostDamageInvulnerabilityU3Ed__71__ctor_mD0DB4364AB0919E5E326210501BD5C53D7C07971,
	U3CDisablePostDamageInvulnerabilityU3Ed__71_System_IDisposable_Dispose_m901E6215FC106A69FD0D2C163482D5A884F58C5B,
	U3CDisablePostDamageInvulnerabilityU3Ed__71_MoveNext_mB4CFDEA1E0DA695A1D754441239AF67B57EF5EA0,
	U3CDisablePostDamageInvulnerabilityU3Ed__71_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01BA262BA443B182E602264E6E2904DCB8A2D1B7,
	U3CDisablePostDamageInvulnerabilityU3Ed__71_System_Collections_IEnumerator_Reset_mC6729F4FD5AD7C629198FE6BB9E75A9D5BE705E7,
	U3CDisablePostDamageInvulnerabilityU3Ed__71_System_Collections_IEnumerator_get_Current_m0DF4A24E5A54F71F3D0745844C34C82293ECDEB3,
	U3CDamageEnabledU3Ed__72__ctor_m73FF2F50F426DBD509C1C628C5615A7434D5A78B,
	U3CDamageEnabledU3Ed__72_System_IDisposable_Dispose_mA8F9B4123DE2A678350D7CD811FBF6AAE6EE483F,
	U3CDamageEnabledU3Ed__72_MoveNext_m62B86B60BBF1CE1A0329A5551E5431A3A8672F3E,
	U3CDamageEnabledU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4264404C5B6109B6853C49397A651EAD520B6C6F,
	U3CDamageEnabledU3Ed__72_System_Collections_IEnumerator_Reset_m046C7B5E86485F93A74D0523682D2F09D6810204,
	U3CDamageEnabledU3Ed__72_System_Collections_IEnumerator_get_Current_m07B580BEC1F7BDA71C6B75C723C253153CDC4203,
	HealthAutoRefill_Awake_m6F1D4A92B98D8C4B854DCFC1D5FC9571C4B2AF85,
	HealthAutoRefill_Initialization_m131DFD11AC3CD78ED93237DE428C0CCE940AC4E7,
	HealthAutoRefill_Update_mD773B31BFC5789FB372B52EF85BDA4094E66D1C8,
	HealthAutoRefill_ProcessRefillHealth_m63D46E31C8DDAE9F35C6CFD596317C611351701C,
	HealthAutoRefill_OnHit_m9EA38E9676152C84C66E58861E38ACD35438A0D6,
	HealthAutoRefill_OnEnable_mFF56909935C04F501CD7D721D5BC9BE46F862AD6,
	HealthAutoRefill_OnDisable_m306FF5E7FB66F36643494795D7987DF3CC3B332D,
	HealthAutoRefill__ctor_mE4B562BF9326E0D0D26FCCA0B0BC2903772BACD9,
	AimMarker_Awake_m67E1CBC3382760AD02D2EA10ED2F7D1982818540,
	AimMarker_Update_mE03BB6305CC9FF0970933F109897EFDC9AC19BD5,
	AimMarker_FollowTarget_m4EC1100C77F901F189A68C0E94DB7BC2B946A244,
	AimMarker_SetTarget_mAACFED6FB799BF5F04B30CBFE99E26C61582A69C,
	AimMarker_HandleTargetChange_mBA577D81A1408B1C2CC777989F9D8FD8D7B3DC3D,
	AimMarker_NoMoreTargets_mF9FB19864485D73517873C0FA3C1988464557C6D,
	AimMarker_FirstTargetFound_mCBA8A40E709AE9F6D3DEF55DA60CE2233FF49D80,
	AimMarker_NewTargetFound_m796C5EF8F53DBABECBB97E19547C174E4C2250F1,
	AimMarker_Disable_m1847368FB1085F318D01C563BDDF9FDAC4E484F9,
	AimMarker__ctor_m1D980F68D29845F8901E571E926605BBE76A6945,
	Bomb_OnEnable_mA8E9E4A6AEAE5F201C66A009EEC9B2339FA580E9,
	Bomb_Initialization_m2107ABBEF6178AFA6566901A73B2D5ADA1229E92,
	Bomb_Update_mE1BB1F43C5A42214B21CF233EEB02136393530A9,
	Bomb_Destroy_mD657F64EBB650E9723D25A6D539F95485FBD9DCC,
	Bomb_EnableDamageArea_m3BC7ACE32F6F964A116BC5CD57C8DCD40CF6121E,
	Bomb_DisableDamageArea_mDB2DB6872EA558C886270E195BFD47453C8C2F45,
	Bomb__ctor_m980C5E63C896897593C75AF26CA459CA4D66A7B9,
	BouncyProjectile_Initialization_m57AB496520BA1C3CFD299BD35D352042EDFBB0A4,
	BouncyProjectile_OnTriggerEnter2D_mC05D1805780F95F4A34F7EC1D3666CFB145552CD,
	BouncyProjectile_Colliding_mB346D732194B128C55FDAFD36E7F25CB8AA9BB59,
	BouncyProjectile_PreventedCollision2D_mEB798BE26BB13D991CBFB6F2DFDD350B3C31E076,
	BouncyProjectile_EvaluateHit_m957B208884895DF419248D9151A7B4C56D96E084,
	BouncyProjectile_Bounce_m3A763AE958723A4D5F30ECC86712D9643FFB187D,
	BouncyProjectile_LateUpdate_m243B9BD5F72997919E3E655BAFDD93BF41529C19,
	BouncyProjectile__ctor_m4263BBB05E4FE743F9C189CAE5E265D60FFE1C21,
	ComboWeapon_get_ComboInProgress_m6E465DE96792C7463B36FFA38A6793C2042F74AA,
	ComboWeapon_Start_m2ED5454C963E0D76770A8A17F60E0CB804293FC6,
	ComboWeapon_Initialization_m14B960B536642B33E0EF18EE488BF7102E5A28FC,
	ComboWeapon_Update_m08B8D8F92B59FCAEC549F32E63E4E6EE8963C2FD,
	ComboWeapon_ResetCombo_mC12F1962CE2B19E50189283DD4041707F9754A90,
	ComboWeapon_WeaponStarted_m74276CAF382C84B9CCD6CD393A2ED39ECCDEBB2A,
	ComboWeapon_WeaponStopped_mF4CA6D170FC92B50C5F35F0ED05F837C038D59BC,
	ComboWeapon_FlipUnusedWeapons_m147E1753A729D0A502906223254AA814D7D4D95D,
	ComboWeapon_InitializeUnusedWeapons_m3D5A2F7984B6991C4FC38CD487357CAD9C794DC0,
	ComboWeapon__ctor_mDFDF9B1D543CC4F0233F05AAB5D6C8994C1F0611,
	HitscanWeapon_get__hit_mFB19BD41A36D7D002A8467CD8378E8A1D51E8C26,
	HitscanWeapon_set__hit_m317F95A5E550597963A755077292ED4EE439394C,
	HitscanWeapon_get__hit2D_m3401E6B69A05D29D51F046A6633EB468685F269A,
	HitscanWeapon_set__hit2D_m7CF864E5C1798305329B67F9C214AC6A3A959D55,
	HitscanWeapon_get__origin_m4610A6C6C917A9EB88C8362FFD5A3BFB8CBF9175,
	HitscanWeapon_set__origin_m9F04B51AB1A06BF5443C20479F52D0637FF13F55,
	HitscanWeapon_TestShoot_m4DBF480C21E06701ED82465923B4262808B8237B,
	HitscanWeapon_Initialization_m170547C5BB838D87F0172DDDDC3E0AF92E71482A,
	HitscanWeapon_WeaponUse_m24F7B44F0379BBCD72C25C112F683D946F673CA9,
	HitscanWeapon_DetermineDirection_mD8F59F5C94690652180C183B101299287F18DEBE,
	HitscanWeapon_SpawnProjectile_m3D2D33929A05A8ED0B348ACE423A73351E3AF329,
	HitscanWeapon_HandleDamage_mCCC42CB6A7E76072CB4502591205F74B6DF9292B,
	HitscanWeapon_DetermineSpawnPosition_mE69F628CBB34109D78614C28C25C901DDD21069B,
	HitscanWeapon_OnDrawGizmosSelected_m64B1E66A2FF4F0A93FCE68C0081C588E2B547A97,
	HitscanWeapon__ctor_mEE6C38ED5A691B078ED24063D33E14E71A5C2E94,
	MeleeWeapon_Initialization_m57A3B84C9B52BB02B441326520031C80E07F0574,
	MeleeWeapon_CreateDamageArea_mB72B9119544887277870B6E266AAA829502620DC,
	MeleeWeapon_WeaponUse_mA44AECA872A988B6C9632145A422437CF7F10E26,
	MeleeWeapon_MeleeWeaponAttack_m8F12591D174504C2CE3A489F486EC98C9C3ABE41,
	MeleeWeapon_EnableDamageArea_mD04A9FFAF329694C1404714018CF0DC404C05905,
	MeleeWeapon_HandleMiss_m7EE09BBFFD4795F3639FC092722F465A89DF44FE,
	MeleeWeapon_DisableDamageArea_mC3C33E91BBAD02D5335859C4BDA8430687F6A440,
	MeleeWeapon_DrawGizmos_mE5C73834B6C0058EF6DFFFB5BA88972B240C5F95,
	MeleeWeapon_OnDrawGizmosSelected_mEFC8AAC66352208645E5E8F0B9BF550016034C2F,
	MeleeWeapon_OnHit_m40389608B4F57E35956443395B72B5BE467F2B35,
	MeleeWeapon_OnHitDamageable_m9715D322A52C2352CDB99B1152A845FC21BB33C9,
	MeleeWeapon_OnHitNonDamageable_mAF41A7BB0AD3498CDE6A576D138F82B9323100F6,
	MeleeWeapon_OnKill_m31D4DE44909D72AEA1138CF05C07D67B0927F625,
	MeleeWeapon_RegisterEvents_m2CDFE68688419BEEC9585470271A29A92ED24578,
	MeleeWeapon_TurnWeaponOff_m3856D496CE4C73E461AAB2852035FCFDDEF1E3E8,
	MeleeWeapon_OnEnable_m38888367454F083FFD5A597637CED81FF400B760,
	MeleeWeapon_OnDisable_m9407384DEDC1BF7268FF77BACB20837CAF986693,
	MeleeWeapon__ctor_m4B8BEE7857917F349454C8E22C7B0D9FCE7F207E,
	U3CMeleeWeaponAttackU3Ed__28__ctor_m7F5D89C706B7D08CEBF0BC11E7579D24A2CF65FF,
	U3CMeleeWeaponAttackU3Ed__28_System_IDisposable_Dispose_m0C4F4F8F343BFD1A5EA64CB6BAA537B61D7AD098,
	U3CMeleeWeaponAttackU3Ed__28_MoveNext_m72CDF5C67A14E1806A1CEA42E7EB42E5E356A394,
	U3CMeleeWeaponAttackU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9F52255C6251DA1C0546DC06A397EB124ED009FB,
	U3CMeleeWeaponAttackU3Ed__28_System_Collections_IEnumerator_Reset_m01C14AE9FBB61A37850C2309212246955971F862,
	U3CMeleeWeaponAttackU3Ed__28_System_Collections_IEnumerator_get_Current_mB0312F55F4B46A74DE7A7BEDDC8AFE85E16F35A4,
	Projectile_get_TargetDamageOnTouch_m3D2F299C54074C9AE58CCE3FEC750F285745E747,
	Projectile_Awake_mE5022422223D2408194C55D74507C65A217731E7,
	Projectile_InitialInvulnerability_mC0824FD08892F41F46AA3E9CA0FC8FF63BBA69F1,
	Projectile_Initialization_mB3CFCDA2EA38424F42380B375E91493C0C6D6C1E,
	Projectile_CheckForCollider_mE832D9C38838245062986D85B1D6263E4086011C,
	Projectile_FixedUpdate_m277ABA04AB4E7AE9156DA37B6A1A6127BA5AF4B5,
	Projectile_Movement_m866F18E295EA8D84BFF5ACDCC04B0F05185BBD61,
	Projectile_SetDirection_m0143B2BBC37E1DA6BCFAE92DB2AEF2AB83A062F9,
	Projectile_Flip_m376D14A7B088875B2520E2033F6F53CA7214E824,
	Projectile_SetWeapon_mF9C6DF314E0804EA50F8ED2E5C7817167029D552,
	Projectile_SetOwner_mAFA258A3695263A0438EAAF46848EBCA37C429F3,
	Projectile_SetDamage_m0BA9725DB63B40FE90BF1E7DA8FA40780A17A5A9,
	Projectile_OnHit_m58E0513336BFACE2FBA13DC73C47FCA004A78B0F,
	Projectile_OnHitDamageable_m124515AC4C7A633D4B7AFA877D522157870C45E0,
	Projectile_OnHitNonDamageable_m103479B350805A1B06557744E034279F236F8B45,
	Projectile_OnKill_m85A5CA49B52843E677424FC5E655F99F1488DEE8,
	Projectile_OnEnable_m14AC5733DDA31140C9B099398362CF2D50DDD9C4,
	Projectile_OnDisable_m4ED895414905F60AC35DBB64C1F617D6120074A2,
	Projectile__ctor_m3FFECCB2BF3F12AE198F5B0B57CBC5823F95F3B6,
	U3CInitialInvulnerabilityU3Ed__31__ctor_m43C16B5EA3DD013749C5E9B1DF3F956F1C54C669,
	U3CInitialInvulnerabilityU3Ed__31_System_IDisposable_Dispose_m07AB19D4C935C8CE66050D853B883C6EE957DD32,
	U3CInitialInvulnerabilityU3Ed__31_MoveNext_mBBB8143BE71E5ED7AE8382A50722C92A85F1A562,
	U3CInitialInvulnerabilityU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBDE762D64A6B16D8A6249599D31E6B15520CD27E,
	U3CInitialInvulnerabilityU3Ed__31_System_Collections_IEnumerator_Reset_m53E51EB7FE241719BE1BE174BE70627E34C2544D,
	U3CInitialInvulnerabilityU3Ed__31_System_Collections_IEnumerator_get_Current_mFEF8D82FF2DD9DA6C06CF3750EB808364DC548AA,
	ProjectileWeapon_get_ObjectPooler_m8BD2D66EBED6AC297E766B0D2D34BF70DAE0865B,
	ProjectileWeapon_set_ObjectPooler_mA872BB162C9B6CC2739E2BCAF8FD68539D4274EC,
	ProjectileWeapon_Initialization_m8302C2DE40BBEB81D2B167D72F08A44F257B18E4,
	ProjectileWeapon_WeaponUse_mB16D44C2D9BDF19C232114582121F586B9D759BE,
	ProjectileWeapon_SpawnProjectile_m29AD2F1870D294DB513E4EC04863045837E4E529,
	ProjectileWeapon_DetermineSpawnPosition_m728EE202E57B7A24B938D4061B6D1AD498B1B0DA,
	ProjectileWeapon_OnDrawGizmosSelected_mB09E805F100AEE2C5E4043D78374A95840555AE6,
	ProjectileWeapon__ctor_m711DDB15E46EADB7923DE85413B83E9FC852BD1D,
	ThrownObject_Initialization_m448A65F0CEDDDF5D7442FAFE5EF04492907FF1D3,
	ThrownObject_OnEnable_m85C6A9797C31FCEBA57A8DF68F43A4F7400B6E64,
	ThrownObject_Movement_m5CA7492F36D279BEDD2D199849A08ED45876D26C,
	ThrownObject_OrientAlongTrajectory_m6FBE7B1DA5408411142F7B44922AB7A243CEEEA5,
	ThrownObject__ctor_mCAD22F80E4DDD4963495701863CA29B303028E72,
	WeaponRecoilProperties__ctor_m23D2763552B876C56C8EB5736D2F2CC400690E8C,
	Weapon_get_WeaponID_m3BA0C85BF07B3D6BA755AB917C19573A6881F138,
	Weapon_set_WeaponID_m66BE3D925576B538927B95DCD09AFBEEE034C8DC,
	Weapon_get_Owner_m33C456C2AAD62CDE75B48081955AD8791276BD5E,
	Weapon_set_Owner_m7DEEE4FB816571992318C753D4F66DA07DE97B65,
	Weapon_get_CharacterHandleWeapon_mFEFB5EA89E00CFCA1B0ABF2DEC35FDCC682BEA76,
	Weapon_set_CharacterHandleWeapon_m3E6676FEDC239160638844EC0060EC087B7A076A,
	Weapon_get_Flipped_m4EFFAB8CE7700C8C12020A56766D331F372F8110,
	Weapon_set_Flipped_mD31AF7387ADFC67AC46BFE6009624E12A2EAD9F0,
	Weapon_get_WeaponAmmo_m59B1F659CE7AC02E0D6D31E0C62404ED6D0E3070,
	Weapon_set_WeaponAmmo_mD46CA94057BADBBC3F7457A907935BBD047DB8EE,
	Weapon_Initialization_mE583EADE95F9662407E870032E33BB04C1131DA2,
	Weapon_InitializeFeedbacks_mD5B0F86179535587E9902C7576C047C55EB55ADD,
	Weapon_InitializeComboWeapons_mE63CF1398D090F918C581B90F02B47728B0ADDE5,
	Weapon_SetOwner_m71388CE21523933A05AD40544F2CE020414F3C8E,
	Weapon_Update_mE5F951C484C0CBFBF08FA5874B87126278BBAAF9,
	Weapon_LateUpdate_m92B9FC9396A5416E4503CCB35531A556923CFA87,
	Weapon_WeaponInputStart_mB0273B84A333CB474ECD6B057F1470B7ADE5FB42,
	Weapon_WeaponInputStop_m2FAC0FE9CD300266D21BEEE73974CDB4D8DA4ED7,
	Weapon_TurnWeaponOn_m9E671CD06FC56BEDA1EC55C443A67FFF5CB89B37,
	Weapon_TurnWeaponOff_m0D8614D13E4224D1E65656156226B1365AA0AF89,
	Weapon_Interrupt_m15177FA11F47B787C6F1ED2999455214BAC02183,
	Weapon_ApplyForceWhileInUseCo_m7F7A446997F107B73A167110542067612A6F7073,
	Weapon_ProcessWeaponState_m3A2D374D5595BFAACDFDB57B627B344C7E9CCCD6,
	Weapon_CaseWeaponIdle_m63DB78BB7B169856EED8BEB4B3FF3509B88B81AC,
	Weapon_CaseWeaponStart_m22A4AE37DDD7C5B0B258DF7DCAE46C5953FBAFA0,
	Weapon_CaseWeaponDelayBeforeUse_m131CFD68B90796EB41BC4102FEC611791E03A732,
	Weapon_CaseWeaponUse_mAD6C83A153DFAFDC14186078DE44476EDB24F9F2,
	Weapon_CaseWeaponDelayBetweenUses_mCC263AFED161B173397DA681B5A091324101692C,
	Weapon_CaseWeaponStop_mEFCAB0B70CCA738EB42C5B92A32862C7CD37D92E,
	Weapon_CaseWeaponReloadNeeded_m33EF76452C1DC25075FCC915D1379E17D3A5AF56,
	Weapon_CaseWeaponReloadStart_m15FEDC2CB9F74FB69DDA40D2598D152BA87E25BF,
	Weapon_CaseWeaponReload_m50D7F7FF875F3816BC81A7C90CDB1D437AFEC7A0,
	Weapon_CaseWeaponReloadStop_m7BE485F7785A6E7E8919ED64F49C3BD3272D8562,
	Weapon_CaseWeaponInterrupted_m030675C3A9309707E3F947349D56E397E5A8DA97,
	Weapon_WeaponUse_mF9C6516D1B47595AB900A1794138C6EA59A051EF,
	Weapon_ShootRequest_mCF5C96795F217B6D33B5528A1E2DD77B87A71AD3,
	Weapon_ResetMovementMultiplier_mDC6C4C2C9E96EE388D70BF0B35BCEA65A3E68B4B,
	Weapon_ReloadNeeded_m3C57D1BC7F415996422AB1D320F2DDBAA519988C,
	Weapon_InitiateReloadWeapon_mCA350C0E659D17F304E1C1EBB8B7E838631F6397,
	Weapon_ReloadWeapon_m29B538AC012D0AAFFE27956E5A6E072AF37CAFB0,
	Weapon_FlipWeapon_mD0F109B436E5327FD9B7ECA23244746689E08C5B,
	Weapon_FlipWeaponModel_mC3FE6F89D96B813BE5862BF6E76371398E5974F2,
	Weapon_WeaponDestruction_m0BC299FA9C8E6464F8E0CBC803E0E3926F9BD0D9,
	Weapon_ApplyOffset_m0280780D3FB2037D4088D45B0AB5BD68662DB794,
	Weapon_ApplyRecoil_mDAD0AD779CB6E7C961658499BFDD639863E6243E,
	Weapon_ApplyRecoilCoroutine_m26B62BE3D96E653C5315D4AA2686D36185E6C2E7,
	Weapon_ApplyRecoilInternal_m9D1E4A1A13F510F5D49DAF692B3C70040CEE912F,
	Weapon_GetRecoilDirection_m37C305C245B9CB0DB4992CA0195DD49025723081,
	Weapon_TriggerWeaponStartFeedback_m13AE595369163C3CC32DC82C80068C516259097F,
	Weapon_TriggerWeaponUsedFeedback_mD9681D98858F68CEF99E92A1AF13482275CCFEAB,
	Weapon_TriggerWeaponStopFeedback_m85CA495ED0D365C18DCE9941A78AAAABAA8E847B,
	Weapon_TriggerWeaponReloadNeededFeedback_m18AB646A2701B0E1285F685D078261330C5ED6EC,
	Weapon_TriggerWeaponReloadFeedback_mC86A2EA0E8461B31A1C6301C5A4A3E6EC8A318E1,
	Weapon_TriggerWeaponOnHitFeedback_mB8ABFB77982A7051C541F1C0A46F558A13D2EC82,
	Weapon_TriggerWeaponOnMissFeedback_mB680DADE8022B89DC87504CB12105297D71659BA,
	Weapon_TriggerWeaponOnHitDamageableFeedback_m47DB44701C9C875AE3228394CB1DB29B950FA4E0,
	Weapon_TriggerWeaponOnHitNonDamageableFeedback_mA9D4DF193F320A189DEB36996A09B8E61C2C9FE5,
	Weapon_TriggerWeaponOnKillFeedback_mC6C0D5BF50DC2017F8F4215776DD6EC0D983E4B7,
	Weapon_InitializeAnimatorParameters_mD5483E3C9206C46033704BF41B948D3951A3EA3E,
	Weapon_AddParametersToAnimator_m5184F357457D9AD11706D6410329FEE29AA1D46E,
	Weapon_UpdateAnimator_mFF2A7945599C8FCBA6D2F467A2433FCDFA9B76A5,
	Weapon_UpdateAnimator_m3EBF3210C46C801DB3C9A25723146917AE4470AD,
	Weapon_ResetComboAnimatorParameter_m6218A0348140784468E3E6CBEF44445B019786C5,
	Weapon_WeaponHit_m932E9333D8C4E2EA71730D619B63EB8507CB7D16,
	Weapon_WeaponHitDamageable_mEFC81A45706D9646BFEA3A7AFD48194E6EF1AFF5,
	Weapon_WeaponHitNonDamageable_m383C1B65067F5C2E2A99A093F061266AD65682D0,
	Weapon_WeaponMiss_m9FDDFEC2999428B597AB0AE967DB2761DAB14AD3,
	Weapon_WeaponKill_m62BED09B8D7C50EA82E1C4B0F9245AE6FA85A682,
	Weapon__ctor_m6C8A5A9BB0A404907A5BD60364DC16EC2F4120EF,
	U3CApplyForceWhileInUseCoU3Ed__135__ctor_mA3E7077C208827B7A935E0F6A68339E80468D584,
	U3CApplyForceWhileInUseCoU3Ed__135_System_IDisposable_Dispose_m139FD857A8162434C3A543038FF1D499D2BC0E97,
	U3CApplyForceWhileInUseCoU3Ed__135_MoveNext_m4ED638F718C524910FAA05ECE3EFDF19A8C2BAAF,
	U3CApplyForceWhileInUseCoU3Ed__135_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m65107CD9E2E3E0500C17F1F661BACF0F8B365EA6,
	U3CApplyForceWhileInUseCoU3Ed__135_System_Collections_IEnumerator_Reset_mAAEE89E144840F6C5377DCF02784430551A247CD,
	U3CApplyForceWhileInUseCoU3Ed__135_System_Collections_IEnumerator_get_Current_mF82B316DE2341D6AD771A43029BCF2F313C7D0C8,
	U3CWeaponDestructionU3Ed__156__ctor_m7162209A17BFAA25544973FB46E8CAC146F503C6,
	U3CWeaponDestructionU3Ed__156_System_IDisposable_Dispose_mBEA2739CED71C08A3E851BAF1E0088B7E715ABC4,
	U3CWeaponDestructionU3Ed__156_MoveNext_mEC38E3394D2C2F4BF5F23463B153FF5BB0FA9C81,
	U3CWeaponDestructionU3Ed__156_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6CE42878024E6115D2B046A2794DE2DE8574AE1F,
	U3CWeaponDestructionU3Ed__156_System_Collections_IEnumerator_Reset_m204CF06A0AD71CCD2911D4F706995ECDF277599A,
	U3CWeaponDestructionU3Ed__156_System_Collections_IEnumerator_get_Current_mB5D3B0F99AA0A245ECCF13193891E50B3516FA69,
	U3CApplyRecoilCoroutineU3Ed__159__ctor_mBF7DFABB805A2299B15743A6B0E4F507E6A38999,
	U3CApplyRecoilCoroutineU3Ed__159_System_IDisposable_Dispose_mC87843CFAB0E108524F53C42B10AC8C2642D6E4C,
	U3CApplyRecoilCoroutineU3Ed__159_MoveNext_m6F5838A8DEEC33CBC50E06A3FF061957D589BF07,
	U3CApplyRecoilCoroutineU3Ed__159_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB1AE09477C3DB7182B481A5FBEA033AEC3FD0410,
	U3CApplyRecoilCoroutineU3Ed__159_System_Collections_IEnumerator_Reset_m49A1B6FE8F36DA6271E2F609A54F8C482AB7EFF1,
	U3CApplyRecoilCoroutineU3Ed__159_System_Collections_IEnumerator_get_Current_mBBFAB9E3759A5389F74C39A96E6C0A54017D2972,
	WeaponAim_get_CurrentRotation_m98AAB13839D535F3C2B21947CF6BC1D434251549,
	WeaponAim_get_CurrentAngle_m955AE63B7CE667B6A775D5FD3D1AFAFBA3FBE5B5,
	WeaponAim_set_CurrentAngle_mD33B85C16678F400959749D8EBC1DFFCCF82D02D,
	WeaponAim_get_CurrentAngleRelative_m3E84E69AA4528117DFC8120E1012B61403794949,
	WeaponAim_get_CurrentAimMultiplier_m18A6A08FC9DFC939973F91088B1AC0A202FA4425,
	WeaponAim_set_CurrentAimMultiplier_mF4DC17A475DC99521C7BC26C334C51DD6E106EC6,
	WeaponAim_Start_m8C1B6D59C342934121EF14B81306DA4DC299F3E7,
	WeaponAim_Initialization_m1AF42288A130A0256FFE7443489EE8E9AFA066CB,
	WeaponAim_SetCurrentAim_m50A2A19BAE8725467EA651C75A25F8379B79DDCA,
	WeaponAim_GetCurrentAim_mB3B5FF1418979D4CFE236174B4D035A560EE12DA,
	WeaponAim_LateUpdate_m2C79338E8166C4AF59F3D94D24D09462D3370568,
	WeaponAim_ResetCurrentAimMultiplier_mB14E148F8C6695598751132A8E98B265568D59D1,
	WeaponAim_DetermineWeaponRotation_mD5F416FE513A28524BC30B81CE428730EF7A8825,
	WeaponAim_RotateWeapon_m5FCE5915A5FD73DD298423148D433ABE6D53CF79,
	WeaponAim_InitializeReticle_mE2332DDF9F1EFCA2088FC07765C67B8757D96E50,
	WeaponAim_MoveReticle_mB5B9084DACCB0172100CF7903B40835C9A758E82,
	WeaponAim_HideReticle_m4D1D33E1238A307B7AEFFF86028B2869C91A1E86,
	WeaponAim_AddAdditionalAngle_m9F932187DCF45F6B72EF55311DD8C5BAC1709142,
	WeaponAim_ResetAdditionalAngle_mC679BE79F59A6BE3AEC7CC1C6068258E465F411B,
	WeaponAim__ctor_m5356DDFE1E1EE3EB912E0F91229BA378149C2101,
	WeaponAmmo_get_AmmoInventory_mADB244648D44D7D996EDBE89741F6630080D9AA5,
	WeaponAmmo_set_AmmoInventory_mD65D062DE46D0DC4DB9DA5F2D3CB08D77162D31E,
	WeaponAmmo_Start_m0CC790782217139591AA8D162850CB988B2A13DC,
	WeaponAmmo_LoadOnStart_mCCB575888F34BD9BF0F4ADA40E715767EC02E308,
	WeaponAmmo_RefreshCurrentAmmoAvailable_m46371EE425FE214B4D1B568186D8D65453AA88DB,
	WeaponAmmo_EnoughAmmoToFire_mC7E2457C87078504E806E597C248E2367F378B5E,
	WeaponAmmo_ConsumeAmmo_m2AB217DB5E1DD1E1A49145B7F4223725C9BA373B,
	WeaponAmmo_FillWeaponWithAmmo_mB87B761B376B5D489E54F18F0899A98ECCB6D03A,
	WeaponAmmo_EmptyMagazine_m8B9FB5523E6B066958963CF75782849E330E70BE,
	WeaponAmmo_OnMMEvent_m3F98856E9FA1C0F73EE37A49BE44AA267846A2EF,
	WeaponAmmo_OnMMEvent_m104C9E273DEB556DA9F0A00C48DBD3C36C642939,
	WeaponAmmo_OnMMEvent_m04B87B17D7A9FDFA47357909DDE3FDC50E9D4904,
	WeaponAmmo_OnDestroy_m537A65E419D681FC1D518D94426EF3A530ADA4A5,
	WeaponAmmo_OnEnable_m463BF83028A579689071670BB4983596035DDDA9,
	WeaponAmmo_OnDisable_mE9F79BAC78D93917EF581A4AB4FDBE2A6421E3E2,
	WeaponAmmo__ctor_mD9040A0C502FCCFAFEE6F0865FCE1424B643F0AD,
	WeaponAutoAim_Start_m3B36E49CC878ADF737124C82839D1BA86B33FFAD,
	WeaponAutoAim_Initialization_m2C116330F24BE4907A31754CEE268894CCD1455C,
	WeaponAutoAim_Update_mBE54644D7474BB962345912607E9F048C1E72A47,
	WeaponAutoAim_DetermineRaycastOrigin_mA693454E289DFCD5DC3EC38FC8988A58AAAB59C8,
	WeaponAutoAim_ScanForTargets_mEA219308E33FD903F93CB0F63104DFB2BD94830B,
	WeaponAutoAim_SetAim_mE58CB87A8FA633075B9EF1565B3F20CA7BC1D010,
	WeaponAutoAim_HandleTargetChange_m980FC918D623B7E8638316553CAA5B8592C4D674,
	WeaponAutoAim_HandleToggleIgnoreSlopeRotation_m50DD7A6DE216AF8C569AA5DBFDC5E98EEC37E01B,
	WeaponAutoAim_NoMoreTargets_mB73D3196A646578991EC078996E356053ADD71C0,
	WeaponAutoAim_FirstTargetFound_m5CDDE84E9D46381377A6C5DEFD9330FE525DC2B1,
	WeaponAutoAim_NewTargetFound_m6D50CF956ED8521273D4299EA874BDF9355D82D3,
	WeaponAutoAim_HandleMoveCameraTarget_mAB9EE5986561020905ED4766FC8341911D516CE2,
	WeaponAutoAim_ScanIfNeeded_m8E0FE5761CCEE558CEA8F4CD79E9ADE902052A0D,
	WeaponAutoAim_HandleTarget_mB68D34E16120BB37FC648550D304CA6B1818EE92,
	WeaponAutoAim_OnDrawGizmos_mFDDBF721AE44D46B341D63A1D3C55009DD89F532,
	WeaponAutoAim_OnDisable_mB68E7BBFB9F762AFC45700C797701991694C981F,
	WeaponAutoAim__ctor_m9FBC327F3EC6BBFE0B25F0F8EB87786E9A3DF2E3,
	WeaponIK_Start_m86FEBF89AAFB299E6B3D2DD970745FCEBC0F21E8,
	WeaponIK_OnAnimatorIK_mFDD443E726C10060168DECF133DAE5E36BAF45AA,
	WeaponIK_AttachHandToHandle_mC8E055CBEF31908A45438D8A43A663BC7EDB0369,
	WeaponIK_DetachHandFromHandle_mD3329C38E05EA68C50CF51A84F23F3C4FD36F10B,
	WeaponIK_SetHandles_m2F50657F3A8FACF950FCCF0943E4BCB79EC4E168,
	WeaponIK__ctor_m427B4FBC3721C40F6744DACDC0E4E764371DDD7E,
	WeaponLaserSight_Start_m4393C8C47D2A526D9121CB5B84AB808B6C83E0DB,
	WeaponLaserSight_Initialization_m1131843380F95E122BD7CEA4A44F61D5D7A64DB1,
	WeaponLaserSight_Update_mA1396DD59F52DAAA33B94648B37AD9DE7F4145E7,
	WeaponLaserSight_ShootLaser_mD030A75474599AAE027FE7F3072284BA1CE0A4BC,
	WeaponLaserSight_LaserActive_m080DC5EB7E0FDE45B2516E1D8543C7EC1ACA742B,
	WeaponLaserSight__ctor_m6D43CF0B55436E6D11E778A40CA9266732DE6B52,
	CameraController_get_FollowsPlayer_mB6B77CDEBB08A53068536E9249028A650CABF6EE,
	CameraController_set_FollowsPlayer_m92599C3119AC286A6F8E1237D23EE0C72483C1F6,
	CameraController_Initialization_m2C307DD7D71BE80C89109830E66E80DDFC6C846D,
	CameraController_AssignTarget_m0FB4A61D0774B5300EAAFC4058AF4FF80844469E,
	CameraController_SetTarget_m97D15A7FD049C9D54ABD6D0227E4E533D851CC19,
	CameraController_LateUpdate_m9BB71940632CF3182FFC9CD84F5F506F19EF598D,
	CameraController_Shake_m6AF2CDFF90F3607648150F34193996BA5A374F1A,
	CameraController_LookUp_m9F87EBB8CEACF6A1C1657374A630CA067B35AEA0,
	CameraController_LookDown_m8F264807273094FFA129023311EEFDABF26C18AA,
	CameraController_ResetLookUpDown_m6DEE85B623855CFFCB46B0B7D23234DB3D9D61FA,
	CameraController_MakeCameraPixelPerfect_m6DB9C5A7DE662B4768A47B4837DF5986BDCAC736,
	CameraController_FollowPlayer_m9AEE1B3B572E035A741F0E8C3B556B8DAE741211,
	CameraController_Zoom_mB7E97A26ACDB104943471AE030153575AE5339EC,
	CameraController_GetLevelBounds_m918E4642037AD382C145A23B49B33A345038023E,
	CameraController_OnMMEvent_mD635E642A94678584CEE856C6C72C02FBAD28EB2,
	CameraController_StartFollowing_m342DBE8C569A4B71FB4476296D6B104B1AC829DB,
	CameraController_StopFollowing_m260B22AA581BAF120E3E4250D55CAD0F926209A4,
	CameraController_OnMMEvent_m2CF66391C49359C3490128F1CB9F630C28D3884B,
	CameraController_OnCameraShakeEvent_m1CEF4769B144C9FCA0D8C21B666C76A0F7E26288,
	CameraController_TeleportCameraToTarget_m7689BD74B865BF617FB44A2F4E92F47006FF56B4,
	CameraController_OnEnable_m9B272EA8A5A8AE8D969109ED016D4401666B291E,
	CameraController_OnDisable_m3AFA862ADE8FCA4A18E7C094EDE8F3EF5268E65E,
	CameraController__ctor_mE22AE01F7663664D43F1FD9A02C4E21A4ECE8AF2,
	LevelBackground_OnEnable_m4E32440BE7ED235A763F71CFF81F92D3AFC6078A,
	LevelBackground_StartFollowing_m68988E166DB80FED43DED62F4AFAD2F3BE34CAED,
	LevelBackground_StopFollowing_m3324003E3E1DF6576125675485F8FD47ACB8B0E5,
	LevelBackground_SetZOffset_m2B80CEC5D1FF51FCFF751203C92DD2FAD575F7F3,
	LevelBackground__ctor_m23CB2B51A9B6E8219A299F0FC74CFCF0434B6F01,
	MMCameraEvent__ctor_mBE0879CC29D68FAEA3CEBF1FA047E13543BC0EB0,
	MMCameraEvent_Trigger_m341B29350E7F8208DAE50055BA58326E1B2FE7A8,
	MultiplayerCameraController_get_Players_m2FA82F3D2E0C40F11195945234B423B87A96D000,
	MultiplayerCameraController_set_Players_mBD7644735DB2451488B4CE38D86F3FCE47AE9DC8,
	MultiplayerCameraController_Start_m4585252E76B7BCAFA14A25F85D06A2CD93CE2A0E,
	MultiplayerCameraController_Initialization_m21EF94B074D52757444362ED9EEEDDA41F4BFAE5,
	MultiplayerCameraController_DetectTargets_mF4CC5A046113B8A762A8CF8C6970BB9CF8F4BE7E,
	MultiplayerCameraController_FixedUpdate_mECC6DAFCCF4E5971585931D5B00888314C048A4C,
	MultiplayerCameraController_CameraMovement_m2014654B2479FE98DF02C288BB8EE470EBFCB748,
	MultiplayerCameraController_MoveCamera_m79C36A4597BC914D23D6338276E00C84EE40A5E2,
	MultiplayerCameraController_CleanPlayersList_m20C434EBC94C779DC2735B279674F03EBE357C91,
	MultiplayerCameraController_FindAveragePosition_m550EDBB0458CFBAD689EBB7BEDD5BC03C53CF200,
	MultiplayerCameraController_ComputeZoom_m2107204A6C02E87C21AF0919BD6A629975A97348,
	MultiplayerCameraController_FindRequiredOrthographicSize_mF73504D9727126475396E8143F575007C986433F,
	MultiplayerCameraController_FindRequiredDistance_m7A3E157EBC04FA87F826B1CE2F012C54D7028648,
	MultiplayerCameraController_GetLevelBounds_m69DC36755F36358B010D026A7C686637E7C1A019,
	MultiplayerCameraController_ClampNewPosition_m13EE2D8027895BD9A6DF300D6BBB4DA95B3045A3,
	MultiplayerCameraController__ctor_m0720F08064C9028C25DFB2ED7C739E1C92C7EB18,
	ParallaxCamera__ctor_mD4DACD6AB36F418EFC05FAA6BD1F2818B6492BDE,
	ParallaxElement_OnEnable_m1BCA249F79E61BFD1AC178D8187D067C32E3C801,
	ParallaxElement_FixedUpdate_m08A75C5A675C722F806646755C054667C194DA03,
	ParallaxElement_Update_m1AD1AD3780C2FA329541D7C1D2ABE0FB9C5DA221,
	ParallaxElement_LateUpdate_m056BC7F3DD5F205B51BB89ED0EF5315F68699063,
	ParallaxElement_ProcessParallax_mB69AD2E004C08C291D73F3F969A2BA91B3EB9E90,
	ParallaxElement__ctor_m2936E32033787F095422059DC9B228928A4FCDA2,
	AppearDisappear_Start_m20C73B9D0616E5E5534E6DA544D14B1409441DB8,
	AppearDisappear_Initialization_m26D66835BBFE7EF3DAB96ED6C67DD27E46CCABE8,
	AppearDisappear_Activate_m58DCE38D11881026B56357AD5087265655959F58,
	AppearDisappear_Update_m915438D190727FFF51A6CDFC1E3D23EFA03B4FCA,
	AppearDisappear_ProcessTriggerArea_mF48DC3F205F5B32D45863E89CE534DF4539FE9A3,
	AppearDisappear_ProcessStateMachine_m3D541AB5C55836E7FDCF931A1F761AB311DD175C,
	AppearDisappear_DetermineNextState_m26D11497EB35F323D758EBECF9FF95B139584ED9,
	AppearDisappear_ChangeState_m3CFF851DA9A095D2E08B048F2E1CE9E7B472D339,
	AppearDisappear_UpdateBoundComponents_m8906E2E0528C86D7EBD03D560BA367165A4984BD,
	AppearDisappear_RandomizeDurations_mE9A08FC1C18C5B9D623FDDE9D95A737D0418B938,
	AppearDisappear_OnTriggerEnter2D_m252A0C1909AF045A26219E83146A999446FCD11A,
	AppearDisappear__ctor_m3FF4367A2FF9AD4EE3291EDDD254BC8ED49EC0C9,
	AutoMovementControlZone_Awake_mFE640991E1F48BCBE4B5F35282CB587255B95D9B,
	AutoMovementControlZone_OnTriggerEnter2D_m685B28F89B99E38515E25070B6738D7F54BE0862,
	AutoMovementControlZone_HandleCollision_m58860BC5324A4EF801881C2A75F3170D0F76867F,
	AutoMovementControlZone__ctor_mCED1F0D3CC8CD4FCF154F401FD2285E80445BD69,
	BonusBlock_Start_m0287EAD14E2D20951C38249ADD75454A2BD01E30,
	BonusBlock_Initialization_m274A4609575164B9D6155C95B1171D0E55BD64BC,
	BonusBlock_Update_mF637C4E3E3401F48C05DD6C92978EC476D9128DE,
	BonusBlock_UpdateAnimator_m496DDEE326E122057BE2DBFF978B5F64DDF0D1C5,
	BonusBlock_OnTriggerEnter2D_m014FD55E3B86EBC9F74849116FBE4074CD7EB4C1,
	BonusBlock_OnTriggerExit2D_mE2217511DA4D73CD92B022C0592CE58065175ACA,
	BonusBlock_OnPlayerRespawn_m2A93CD20968110E75ADDBA4FF75D193AA5E03887,
	BonusBlock__ctor_m1035A39BBC8F78270CEDAB13B18D4F47C0DACC14,
	ButtonActivator__ctor_mEB200DE9ACFF6CBC26705751E2DFC2C5165FE33F,
	CharacterDetector_Start_m94256B2BD8DF46BC0F63C25D3068470A169FD9C7,
	CharacterDetector_OnTriggerEnter2D_m60C243CF6B4C73B4075B2BE40E609483BD4FB4C1,
	CharacterDetector_OnTriggerStay2D_m346921080A376A3C541EA9D6B117898E834E796A,
	CharacterDetector_OnTriggerExit2D_m9788B87CC660CD37FF608FECFA6375C53EC36780,
	CharacterDetector_TargetFound_mFF5D764A7353F34777338A811B937086972DB7A6,
	CharacterDetector__ctor_m4CB748F91F98B2372E9EEE4EF83953CB76756DC8,
	CharacterHorizontalMovementOverride_OnTriggerEnter2D_mBE647F65C5B841AA77E7729F8D7E508B560F61D9,
	CharacterHorizontalMovementOverride_OnTriggerExit2D_m064CB7C154F5C3C1210B1C7E9DD844C39EC3D331,
	CharacterHorizontalMovementOverride__ctor_mBA5034C599F4107E60317C257F90D24FC90CB438,
	CharacterJumpOverride_Update_m295BDFCD985F76442DD70282998C7C357A728863,
	CharacterJumpOverride_OnTriggerEnter2D_m26A7BF991DBA4A82429D9F901EF2164A24361BD5,
	CharacterJumpOverride_OnTriggerExit2D_m3DEA016E6339FAC36ECEC8E1422B56461B9DB987,
	CharacterJumpOverride_Restore_mF97D47411CFCEB8FBBFA0C3276F57BA2A111038D,
	CharacterJumpOverride__ctor_mA6D2EFF53C9E0101F0BD8BD26147DDB3CE63C795,
	CorgiControllerPhysicsVolume2D__ctor_mD2D76C479B0997E2B702FCB5DE0AC5367C21B225,
	Water_OnTriggerEnter2D_mF717EB620EA0C66B7E31CB53F3DEA13A405AA247,
	Water_OnTriggerExit2D_mD9755D399B066E8345CA17DBBB79EC7A5622CC3A,
	Water__ctor_mB37D4AEE740896A0B9E8135B36BCEBE6836F1156,
	FallingPlatform_Start_m6DEEE759250C482E2D293371830CAF9103F3F8EF,
	FallingPlatform_Initialization_mB73A6B2DA508C2BBBF66F4B0458E4C7703A6EF60,
	FallingPlatform_FixedUpdate_m7BB093DF0F81215D39108C427C422CD6B9D408A4,
	FallingPlatform_DisableFallingPlatform_m6631F1F513860CCEB63E24AAED7574A81EB337C0,
	FallingPlatform_UpdateAnimator_m6DCC847675D2B7E640B4F7E97CC6FBE1D53EEF1A,
	FallingPlatform_OnTriggerStay2D_mDD1C237300C5AFFB057ECC42E2C7A2A086C2B7A4,
	FallingPlatform_OnTriggerExit2D_m3E3ACD34C0DCCEFB7BB2629D40A9ADC42B102983,
	FallingPlatform_OnRevive_mD16A179AE6A55F1B609D6747CC93D485A6661509,
	FallingPlatform_OnEnable_m1B8CB388E05AAD7FE7D34D5870A65F075810171F,
	FallingPlatform_OnDisable_m936356E3F2EB063C5F4E99F69806189D9B2D0C31,
	FallingPlatform__ctor_m335BFC4FF51471CB685F4CE9409379DB7375900C,
	ForceZone_Awake_mC6AF6224210661470264FB02E4845ED7647BA55E,
	ForceZone_Initialization_mA541AC209431C88B5715F766E612940C1ECC9B25,
	ForceZone_OnTriggerEnter2D_m1FEE0719A9F90C0594B2EE56680240FB5399061B,
	ForceZone_ApplyForce_m3126E1FA05299E577346AA9033302D0CC5F1ADB1,
	ForceZone__ctor_mEE424719AF868043A478DFB45574AE155CADC048,
	GrabCarryAndThrowObject_Awake_m08CA43320643346F381BAD2330F9512D7E2568BB,
	GrabCarryAndThrowObject_Initialization_m966843A59FED23CAE4A0B346AF7F3EFC9C45BF41,
	GrabCarryAndThrowObject_Grab_m68BE7D843A5CC3BF9CB30E891D2F9379D2339B09,
	GrabCarryAndThrowObject_Throw_m0812E2E4A64BA3B416FB3AAF99037EF0522543FF,
	GrabCarryAndThrowObject_ResetCollisions_mE7DAAD44C14146EFBEB4CC1DE4868520A8D6D984,
	GrabCarryAndThrowObject__ctor_m29A92881EAE8C813BC94B99EF0212DB858C459CD,
	U3CResetCollisionsU3Ed__21__ctor_m0966EAD5E98CB004C71798D2E5A021D75EF8F6BB,
	U3CResetCollisionsU3Ed__21_System_IDisposable_Dispose_m418B21E668A9202D3A33E4F6A3FB22C4E6F08AEA,
	U3CResetCollisionsU3Ed__21_MoveNext_m08048E67DA1A4966BD7FB25EB5C910C5E574F6AE,
	U3CResetCollisionsU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF75379575970AE82ABA19E884CEA973CDD3FF747,
	U3CResetCollisionsU3Ed__21_System_Collections_IEnumerator_Reset_m78A6063A24ADFD6AEE30977CA1F91ADE012608B1,
	U3CResetCollisionsU3Ed__21_System_Collections_IEnumerator_get_Current_mFB5D5AB67F4C4508A552B3F574C2935E3F7ACE10,
	GravityPoint_OnDrawGizmos_m9FC4909128D9CBC997F10FC4F9C971F5686EC072,
	GravityPoint__ctor_m0E9B6E0B4CF09CDB727D5CAF50D01865A344CD24,
	GravityZone_get_GravityDirectionVector_m60F53523214E3EA7E7D4E4DF7413E237BD482628,
	GravityZone_OnDrawGizmosSelected_mC98A35228F11B117489F46BCEEA837D0DE4F0D5D,
	GravityZone__ctor_mDE4F4AA2D6AB6FE27EF1727C9BA0772EB06BD6A5,
	Grip_Start_m3E0CD9A8CE162F517FA965D32C0CE0360B41E392,
	Grip_OnTriggerEnter2D_m635A4C72BF1568B9AFE6D997E5F0A5EB776CB572,
	Grip__ctor_m0E04ADAEF551F7F2DDD0A530F105B7591E0C933A,
	Hittable_Start_m18F25BABF4E86959F7628EEFE0909DA7B91144EF,
	Hittable_OnHit_mF8175AA786A7A547B5A00A9C14FA32C6FA6D41F6,
	Hittable_OnHitZero_m47C94E7351F3E9231C6BDA912ACA9C83060DF37C,
	Hittable_OnEnable_m52C2DA190630BD88DE6E2B2C70DB7B70D1B63F7F,
	Hittable_OnDisable_m7F3A003555A2C18B0175B3BB605EFB3F41EE58D4,
	Hittable__ctor_m0504A3B0E886B87E0635C2A596023B10D0CEAAEA,
	Jumper_OnTriggerEnter2D_m725BDA6ACF4079ABD231041DF2E03E2D307FE770,
	Jumper_OnTriggerExit2D_m1BE6F5E87D949EB9D2BB5FE3FE6FC70287C1B8E0,
	Jumper_LateUpdate_m1AB499C13814D9677DF0A33A7D4E7225B8366357,
	Jumper__ctor_mB25885882B09980788D73E7065151FE06C1FF4DC,
	KeyOperatedZone_Start_mF1D0EDB7C1BC4C61BA15BE5B2B6F9C97BBB82709,
	KeyOperatedZone_OnTriggerEnter2D_m7ABAC41AF526ABA3AB044E4886A2FD42705D2B94,
	KeyOperatedZone_TriggerButtonAction_mC7941EC55A7F5AD3E4111DCE5BA2AD68963ADA01,
	KeyOperatedZone_TriggerKeyAction_mCD9A071B989A5A1CB715FAA3BC943635AC242F0D,
	KeyOperatedZone__ctor_mFD87329B0E5E6BE9193177ADD06B7646DB1E726F,
	Ladder_get_LadderPlatformBoxCollider2D_mC18808CFC8E377DEDCCD3A08C5C04AB6ADB91C2F,
	Ladder_set_LadderPlatformBoxCollider2D_mBE1E253275CE3A7C7977630CA47B9DBC65A45AD8,
	Ladder_get_LadderPlatformEdgeCollider2D_mDF263178A858AB23D00FE0B3F2E64B46361A5DB8,
	Ladder_set_LadderPlatformEdgeCollider2D_mA49F09499AAA70EF6EBC57F420D0B4DB870CD980,
	Ladder_Start_mAC1F08CE950EBDD694FC5C6E20AECAEC77C91BB1,
	Ladder_Initialization_mF176E1D20545A61F3B8CBFA3D7C3618D0F3810C1,
	Ladder_RepositionLadderPlatform_mE481456A0154C04F40D0CDBA83B1AC045C595B8F,
	Ladder_OnTriggerEnter2D_mC532D3DC087F0D0715D3D9AD485F03F44C8AA18C,
	Ladder_OnTriggerExit2D_m7C320C8A0CDD24053C5986235ABE1E304F597367,
	Ladder__ctor_m638DE40B9FEA4F497FB832D7D85C4E2FF6F6ADC1,
	LedgeEvent__ctor_mF87F02B223BF24015920133ECA5E206185B3B372,
	LedgeEvent_Trigger_m326B964B96C57946A63725380335BD5082C37CA7,
	Ledge_OnTriggerEnter2D_m8A197DBA37F88CA4028C64076CAA95A568617B90,
	Ledge_OnDrawGizmosSelected_mC6A798CCB9E50E29933700DD9583703E30A03B41,
	Ledge__ctor_mF4C4BB1FAE4858F18620119E509539A0D2C53743,
	Magnetic_Awake_m6480A9FBFCD7ED0866F03FEC9E204AAFA4877B08,
	Magnetic_Initialization_mBE5E17FCDE35D41FDED86C7F849546A793B5DE5D,
	Magnetic_OnTriggerEnter2D_m0C68AF4E58827F9639B57AF5970C4BF510BCF4A4,
	Magnetic_OnTriggerExit2D_mB593B52EDCE03360958F1AD44FBE465034AB198F,
	Magnetic_Update_m523C7C248A6AF444AFA3975CC9E57FF9064B847E,
	Magnetic_FixedUpdate_mD9134CE71849BAD8977D21E05925FB1FC839ECEE,
	Magnetic_LateUpdate_m082143E4C40D86A2492F9837F39A746C2D8BA385,
	Magnetic_FollowTargetPosition_mC9DB0E5387220A1FC27DAEF215779D8E6ADAACCA,
	Magnetic_StopFollowing_mFFC7E1E8C24554872D8E9F1AC6AF5B2047824738,
	Magnetic_StartFollowing_m8652ED26E5D4FA3674A95951EB5E384739AA7A8A,
	Magnetic_SetTarget_m8F1F9552DCD4E4F837D7F8F45ECED97106116AA0,
	Magnetic__ctor_m5AEB42A01A9AF39F4ED940E0D8A2DD64A7C482F2,
	MagneticEnabler_Awake_mF78C061AE43A3998B973136F841063E9C334E8DE,
	MagneticEnabler_Initialization_m37A0ABBFDE3085BFDDC356A88551B89E7FB8A960,
	MagneticEnabler_OnTriggerEnter2D_m935FF6106FC7BCF5186187DDD36E02F69315254B,
	MagneticEnabler__ctor_mE9A593577517B25F15DCB3845502255D8EB7EBAB,
	MovingPlatform_Initialization_m152028608FEAD7228447096F78AD63964EE55565,
	MovingPlatform_get_CanMove_m83BFC98212B598CF82EDF8F231321F74C70724D9,
	MovingPlatform_SetMovementAuthorization_m4E035418B5DD22E6A58807796BDB8B96EFA5EA71,
	MovingPlatform_AuthorizeMovement_m502C7C3A8BACDE74642D00BC1D7DE1DC70C935B7,
	MovingPlatform_ForbidMovement_mC70D97DA1963800DBC678D772BB4E2F82420F841,
	MovingPlatform_ToggleMovementAuthorization_m8AB45241446AB8FF1E071FBCAB3985BAE98316B0,
	MovingPlatform_ResetEndReached_m231B86848281C28491BABC1F4A042B480082BDEA,
	MovingPlatform_MoveTowardsStart_m18B71AE6BCA057D2EABD33F220802BCEF1EED074,
	MovingPlatform_MoveTowardsEnd_mAD33CE6FEE3BEB1B6D09971F65FC69C6ED7E7DBA,
	MovingPlatform_PointReached_m3EAB41899B7513788E00541A88F9273C55C6E1C5,
	MovingPlatform_EndReached_mF3D2B47122FB38478813DB94564C8B4A09D43BF5,
	MovingPlatform_OnTriggerEnter2D_m6C34439B1AFCE5284ABA58E54E0490CD846C819F,
	MovingPlatform_OnTriggerExit2D_m732BED39AD2E311A251F3A91425D3A19D7CCEDED,
	MovingPlatform_OnPlayerRespawn_m70B84174755001A9A45DA9A42A4D057B6FBD7BF3,
	MovingPlatform_OnMMEvent_m65DC8B3092B64DEFD13E59F91F76B5D362816444,
	MovingPlatform_OnEnable_m96472322B4FC1916DA99FB1D554EB0546C96D7E2,
	MovingPlatform_OnDisable_m1AAD9A2F609B247B3079EDE68D3E47A872D58FF2,
	MovingPlatform__ctor_m8A937339F9E3823EE63955A77F0BC3086EB5F0F9,
	MovingPlatformFree_Update_m4AF4D3487A845D366109EE53E5EF24E17D9475B0,
	MovingPlatformFree_LateUpdate_m2699A6A1CCFB7B475DA36B326498BB408F3845FF,
	MovingPlatformFree_ComputeSpeed_m1AAA921DE95498FBE89752F6D323DC7B5D0FD055,
	MovingPlatformFree__ctor_m937A35E57527A371B137DC459E233A43AE0D8EC5,
	ProximityManaged_DebugAddObject_m1C55DED79BB27DD4500C3DD1F67103E4A197937A,
	ProximityManaged__ctor_mF4331229D988220DB64E3DE39823137A189D96E7,
	Pushable_Awake_m35A1B6141DC8D49708C74872983131FAE910381B,
	Pushable_Attach_m963DBD48B7784A916D1773A3D617CFF414C765B4,
	Pushable_Detach_m3477345DBFC14AF62CC17802C2D801D15F881A6E,
	Pushable_Update_m12CAE94E31E1365746DB495679F70312941F744D,
	Pushable_CheckIfGrounded_m7D55F0A88321087A27ED5F0E39D100E4B87D1235,
	Pushable__ctor_m42CDFD7189EBC511C8D15E56CBB7AA6C90375DE1,
	RandomSprite_Start_mA042656AA0CC118C86EC10E8C32D9088EF01411C,
	RandomSprite_Randomize_m1360EF7D2C3969A3C3A918911542ED303C541884,
	RandomSprite__ctor_mEB66A50DBD80FCA1AECC28347370FF225EA68F90,
	SurfaceModifierTarget__ctor_mAF82A25E673540E37EC69DBE40255FD895D053FA,
	SurfaceModifier_get__targets_m3CBE8FC16453216D642DF383592DA8BE3FD00726,
	SurfaceModifier_set__targets_m46B97154C7F209986C26B4E194D470353A1E6EA7,
	SurfaceModifier_Awake_m437D7C9939A0C4F9ADAB70D5A27BB4E1AE01B042,
	SurfaceModifier_OnTriggerStay2D_m0BE4BB964C6CED7FFC53BFBA1B293FC76903AE08,
	SurfaceModifier_OnTriggerExit2D_mA23E5FDF3E7C14152053F4940C1B16B9D4955F2C,
	SurfaceModifier_Update_m52342F2D8F60F56AB3BACA35BF1A7913501C0D5B,
	SurfaceModifier_ProcessSurface_m3229B3120C8B13FE458062C4802A8EDBAB7C355A,
	SurfaceModifier_ForceApplicationConditionsMet_m33F125E779FD846577863A300D6CDCFAF08EE7F6,
	SurfaceModifier_ApplyHorizontalForce_mF13F87BBE52D6A09EB4B73C7DBFF81A694D08B97,
	SurfaceModifier_ApplyVerticalForce_m5262718E0F14254CDD0E225C79CC034E822E0F5E,
	SurfaceModifier__ctor_m413C545049BED66CF85D5B2F4F5F467603A06F0C,
	TimeZone_TriggerButtonAction_mE548C812C1E431B82F1A570907C6D7B1C42327AC,
	TimeZone_TriggerExitAction_m6F1E2700E2A75BBBA63101CEBFF683A9ADE5AEAF,
	TimeZone_ControlTime_m268FD9640E69B8C9DED5994C7F7BD3B7B2CCA640,
	TimeZone__ctor_m9BB582EE8872D9945C151600314C3345C8E922C4,
	WallClingingOverride__ctor_mCDD788202BB54E286C51E3E51B2D3ED7BFCF207C,
	Zipline_Start_mE21FF5FC145B7DFF9B9E7D7B77F220298C675A12,
	Zipline_Update_m05D92E76486C3EB28F6888963480D651B088E78B,
	Zipline_ResetPosition_m3C8B6AE7E8B92371A6D30152C95645FE1C366752,
	Zipline__ctor_mC86E0B682986242062C213B4DA428271E3F0C720,
	MMFeedbackCorgiEngineFloatingText_CustomPlayFeedback_mA40899DE8CE7CC2C8527B36D3C00F933FA5E449A,
	MMFeedbackCorgiEngineFloatingText__ctor_mB22202038E8689E13E54F6A3C5C1296D74A1FE24,
	MMFeedbackCorgiEngineSound_CustomPlayFeedback_m29D5F000508C3A7ED09B2DDBE32722A940D31998,
	MMFeedbackCorgiEngineSound_CustomStopFeedback_m257BEEE194D205CFFE59BC0065CEA3072225B6EC,
	MMFeedbackCorgiEngineSound__ctor_mC34C7AB07D4BB2B26AD98C8CF85DDA0EA2613161,
	AmmoDisplay_Initialization_m83E1B783CE0E99ADF030F1A8F9ECC0DD8A99732E,
	AmmoDisplay_UpdateTextDisplay_mD6D4601D96142B86E5CDBC18B72A24C9024700ED,
	AmmoDisplay_UpdateAmmoDisplays_m4012F292567A9E246702841E60D1EA5A8C3CBEA1,
	AmmoDisplay__ctor_mCB5A9F8A536532E153557CF8D6FC32C65E008A68,
	ButtonActivated_OnEnable_mA322924D4CC2B6BAC94277975CD9B458FF93FA08,
	ButtonActivated_Initialization_m85B5B45F4E6E96D89E7F4FE8C4ACA064B82B2A9D,
	ButtonActivated_MakeActivable_m2245C3CADE8FAC63B178E699D6BF47167F6C1BAB,
	ButtonActivated_MakeUnactivable_mD67FF69BCF4779CA9F06587ADDD2FE9FA748D595,
	ButtonActivated_ToggleActivable_m21D8BB59C68BAAFAC9326AE3515A1E667169B52E,
	ButtonActivated_TriggerButtonAction_m90ABA5F03388FD3B7B2DC788838C3D638F02ED6F,
	ButtonActivated_TriggerExitAction_m97D1EAB36E83B9D79288723D747EB956CFAE66D8,
	ButtonActivated_ActivateZone_m16343B254AB6C2A2E55673FC0E90733925A73B4B,
	ButtonActivated_DisableAfterActivation_m433C13F25D47903C7DC20A234541A949E1ACAC62,
	ButtonActivated_PromptError_m3EB905906646471FFA79AE2FA48AA6DCB75A0BF7,
	ButtonActivated_ShowPrompt_m6F5B8F1B11F01471630BB7CF809D64ACCA795BCD,
	ButtonActivated_HidePrompt_m2B5FE78D843E05244B08F87C37A533235350D218,
	ButtonActivated_DisableZone_m2886F9F326156B7CA159B3CEDD58EA31CF8D64D8,
	ButtonActivated_EnableZone_mCBDE9A433FBC4719AE77FAA1B46D30868DF4D450,
	ButtonActivated_OnTriggerEnter2D_m658128E15BA4BDCCF88ABA9970E0BFB9A6C305BB,
	ButtonActivated_OnTriggerStay2D_m9C2A689D96D707EC3B7AA81744CCF933F087BB13,
	ButtonActivated_OnTriggerExit2D_mEA78A3D90AA324B7FCBAD5EBA9CEAED8BCB081E2,
	ButtonActivated_TriggerEnter_m243976EE70F5E8C263F80C611022F569572C3D90,
	ButtonActivated_TriggerExit_mD108186D2B38EE6D1C558910DE5FFC2DEEB8A112,
	ButtonActivated_TestForLastObject_mF8867455118D1FFEF38D753B2445B84608B95938,
	ButtonActivated_CheckNumberOfUses_m42CD34C39D77578D98A04190DF96AD146EFC3499,
	ButtonActivated_CheckConditions_mA9ACC7DAD99CF203C9D02C81BB802EC4B3F3EBFF,
	ButtonActivated__ctor_m6FEA8A488C4C38817E424F66B6956EB282CFEE81,
	ButtonPrompt_Initialization_m15BD9CFBB4470CCF9D0B91BEAD79EFF4CFDCB45D,
	ButtonPrompt_SetText_m654F69B263BFB1812B8254D9F4B78747A35DEC8C,
	ButtonPrompt_SetBackgroundColor_m06ED7E2D7AC5B5E38097941A7EEF7201677BE8DC,
	ButtonPrompt_SetTextColor_m1F33D1EE6DDC9BC1153FAC544D940D3138223B6A,
	ButtonPrompt_Show_m797864412C5CE0EC59062EB64C623D47A3E77356,
	ButtonPrompt_Hide_m36B0C96FB33036E41CEE7E503A85AF17D659D28C,
	ButtonPrompt_HideCo_m8E26A74507A52BFCF21D7A829DB8DC6A7A057DD0,
	ButtonPrompt__ctor_mBB7FBF6FEE9E6670761D774D18DA8636E31BCA18,
	U3CHideCoU3Ed__16__ctor_m371127392E9A25FFD5660D16196AE80B4F8FBC33,
	U3CHideCoU3Ed__16_System_IDisposable_Dispose_mCEF923AB115F4C6FDE5180B1CD5CCDADFC05157A,
	U3CHideCoU3Ed__16_MoveNext_mEC7D6211A4E0750D62540C6E1B94DF13CF9F6F31,
	U3CHideCoU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD066706C248E09602848E2A0EB7E19A058C6C114,
	U3CHideCoU3Ed__16_System_Collections_IEnumerator_Reset_m5184A37CAEAB09581A7437EC383295162690D579,
	U3CHideCoU3Ed__16_System_Collections_IEnumerator_get_Current_mE2258AFE39AAB29B4B932D6AFA427C132A244704,
	DialogueBox_ChangeText_m3C8ADDDF16E94B81DEA547F98CE672E7EC138DC0,
	DialogueBox_ButtonActive_m95F191D6BF48CA90CEF3B1F063E36A6E1FF50664,
	DialogueBox_ChangeColor_mCEDE7DE9F0D5A9DBC6C27565B8A972D6B3E8F2F9,
	DialogueBox_FadeIn_mCBFCA91D96F54F7E11973FCF8B2DBD5419803CD4,
	DialogueBox_FadeOut_mF6695ABE43126C13EC145DA71FDE6D2040C1764E,
	DialogueBox_HideArrow_mC121FD152AEBBB3545E4484075A7982D4F4805E8,
	DialogueBox__ctor_m172D40F2CB347EC22DCEFA1AB4A5CB9197D53955,
	DialogueZone_OnEnable_m8ED457488EAF4E1E6F45E72ED571CAE16E7406D8,
	DialogueZone_TriggerButtonAction_m0CBFAF0D3D4734AEDC8FE04EDDEB1286B93E3C48,
	DialogueZone_DisableAfterActivation_m1148D021C00E3E63F73BE849F22D9E8AA036E712,
	DialogueZone_StartDialogue_m977B341948614833560581F9372276B2F9DB572A,
	DialogueZone_PlayNextDialogue_mA9B88416AA47B6762DAB5933D04FA2B5A8876209,
	DialogueZone_AutoNextDialogue_m8D3AA3CC8BC2280C60171FF7747B7CA23909865F,
	DialogueZone_Reactivate_m021FC5E3354B896489ED4387DF3F91078DDFE56B,
	DialogueZone__ctor_m181FCDD4E6A7B8F9556A2C60FE9BA769352CC4E6,
	U3CPlayNextDialogueU3Ed__29__ctor_m11E43E1FEE132C3ABDD5793C7626DCF509761233,
	U3CPlayNextDialogueU3Ed__29_System_IDisposable_Dispose_m180E5363B857E3A3FFD61C8E7C48FDF3DE43A4EE,
	U3CPlayNextDialogueU3Ed__29_MoveNext_mA2588A9BC1B97A5B8A0047A70FFAEE9A54B31E91,
	U3CPlayNextDialogueU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF58A3F42B461CB1CF8A5354828F5D0A98404F792,
	U3CPlayNextDialogueU3Ed__29_System_Collections_IEnumerator_Reset_mEAF8F9183C6FB7C0EB8670D90056AB5B01F62E0A,
	U3CPlayNextDialogueU3Ed__29_System_Collections_IEnumerator_get_Current_m9DA02A9FE9981E93A2C4C6EF6A9CAF0A8FAC43AA,
	U3CAutoNextDialogueU3Ed__30__ctor_m320C6303EAB6DC0C1B9E9E169831A517AB8898BE,
	U3CAutoNextDialogueU3Ed__30_System_IDisposable_Dispose_m1F35BA55BA32A59249941C09CE0965EC27F87D36,
	U3CAutoNextDialogueU3Ed__30_MoveNext_m679BC6C113AC2FF548F05A2650BBB77A35DF84E3,
	U3CAutoNextDialogueU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09813C1D58F52D43D7440294F08BB22088200189,
	U3CAutoNextDialogueU3Ed__30_System_Collections_IEnumerator_Reset_m66DCC1B31E2C959B0F0FF0EF94E5E28345F1A048,
	U3CAutoNextDialogueU3Ed__30_System_Collections_IEnumerator_get_Current_m61D159A6E110272F54A67D2A143467979840345E,
	U3CReactivateU3Ed__31__ctor_m34F08A93EB12AF320F77CCA7F5B648D845618290,
	U3CReactivateU3Ed__31_System_IDisposable_Dispose_mF04C107126D75335216D418B6F8555716F394146,
	U3CReactivateU3Ed__31_MoveNext_m105D11C282979075800B41853F5D396F7EB3E922,
	U3CReactivateU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB9C55EAE5041CF68DB3F40EF6D35B79A0BE46560,
	U3CReactivateU3Ed__31_System_Collections_IEnumerator_Reset_mBD274240225A5A8A4D2F5C06FF0DBE2B697D7822,
	U3CReactivateU3Ed__31_System_Collections_IEnumerator_get_Current_mD0A42D9D6D0C0D3EE7CD11AD943C49D88E0B9838,
	GUIManager_Awake_mCCB90D20024E78083726441008AE2C7E639E8C89,
	GUIManager_Start_m92CF04CD26C1F149A9007046F3A423EF04A43262,
	GUIManager_SetHUDActive_m115FEC751884ACD6A226880A507F8F14D92D6062,
	GUIManager_SetAvatarActive_mD7393680DDA255CFC5C09F2BD916C0D1DB4BAD37,
	GUIManager_SetMobileControlsActive_m198DAB0D458FB457993B8F8C159FBB2329E8D6E0,
	GUIManager_SetPause_m015B377C7A110B43E49743D1055FB62598D2CFD7,
	GUIManager_SetJetpackBar_m2F53211389C9C6070C41BA7357F5DC4100D081C3,
	GUIManager_SetAmmoDisplays_m6975C15FEAA4977BB6ECE087F488C4467378DFD5,
	GUIManager_SetTimeSplash_m2EEA46125DFF2566D060D85153A36D0904ECB509,
	GUIManager_RefreshPoints_m546A8FE786034AF253826E1C5CDCC971EC23AADE,
	GUIManager_UpdateHealthBar_mE16DCC61B4D46BA7B8C77672B63DA8FEA6069710,
	GUIManager_UpdateJetpackBar_m577B62FBE2B7D797FD0928C852C44778342728E0,
	GUIManager_UpdateAmmoDisplays_mCAFB24A21E053A2E552D2766A771F3B2A4C05AF5,
	GUIManager_SetLevelName_m8300F2A94AA5825E948588D8C990CB66B17E5518,
	GUIManager_OnMMEvent_m2BDB5BCD2B7FAB57D35BAF7114F21C347FD9DB71,
	GUIManager_OnMMEvent_mC6EBE330ED7292F2FA8D545ABBE846BD7DE28419,
	GUIManager_OnEnable_mA6F3F63E914526C542DF29CD4373F1F0D01BC96B,
	GUIManager_OnDisable_mB9C57ECD69FFDDE22F22081A9566F88E87E98356,
	GUIManager__ctor_m6FF23ADF78E962A688E7A29256CB50F232F9E84B,
	HeartsGUI_Start_m48B131703BFC57D9DB8373D946504BA5BB818214,
	HeartsGUI_Initialization_mBA0F2E846ABC68DCD11E46F06B49688A10433A75,
	HeartsGUI_DrawHearts_mCB9B54B65F09C5ADC8DEBEF1577E0C91DDFD4501,
	HeartsGUI_Update_m5B675C668A7F06AB067E7C40D6A657B9F90458B4,
	HeartsGUI_UpdateHearts_m41DCA3164FE2D4DFF699E18DFA70AE95B205DEAF,
	HeartsGUI__ctor_mC422BD35C1160D97998EAE410118489CC6453A5C,
	HipsterGUI_Start_m5F8F9D53263CE13FE415DF5DE72E2BD05B2870D5,
	HipsterGUI__ctor_mC1F7A43D65EED54C2BA8CA77CABF4BF6ED9BCCFC,
	LevelSelector_GoToLevel_m184887F4B962560F0C0C37F8282C931C73B811E0,
	LevelSelector_RestartLevel_mCE3B767BE79F6149BF130652836D18E4F23BE242,
	LevelSelector__ctor_mB39418971A43B41B2BA4AB70AD59C750AC7D0606,
	LevelSelectorGUI_Start_mA4F4C75F4404AD49665CBFE924D480112CE6198A,
	LevelSelectorGUI_SetLevelName_m3F9CD411F27FA261418DE6221ABB8BEEB0C464F4,
	LevelSelectorGUI_TurnOffLevelName_m7AB3511F23F400B4AFA7BBAEBE9462B564E2FDBE,
	LevelSelectorGUI__ctor_mA842F021F843776051A47B7CB7E4CA0E6D078370,
	MultiplayerGUIManager_ShowMultiplayerEndgame_mE6746CA0D5A13FC57E2969CB7E94CC5B0DA8725C,
	MultiplayerGUIManager_SetMultiplayerEndgameText_m98DCF67D75602321FCC59112C299F0BAB6CB906D,
	MultiplayerGUIManager__ctor_m8E05E9C9A1FE3DF1C85C047D4A8226FE61D9E4A4,
	MusicSwitch_On_mFB538F333996A1AB5D0BD39CB112EF571BAC6A7A,
	MusicSwitch_Off_m9BC9FF945964174353F806E284DD48F6DCD1C4EB,
	MusicSwitch__ctor_m74182A4F91083966C6080FE7B3B2E10D00FC8DD4,
	PauseButton_PauseButtonAction_m378E4823D1C822971A3DBD4DC66D61822AEFCF07,
	PauseButton_PauseButtonCo_m4F2B2E2208F45B76612F7F9FCFD4D6FA6744672B,
	PauseButton__ctor_m5A3666A8BE921523D3F79BD502DC2130E13EFB4A,
	U3CPauseButtonCoU3Ed__1__ctor_mF1420880758978D531D14446B6007387BEA26509,
	U3CPauseButtonCoU3Ed__1_System_IDisposable_Dispose_m987F976C8EFEF09D6AE5ECAE1A49AFB8CEA16CDC,
	U3CPauseButtonCoU3Ed__1_MoveNext_mEAF1FA385F389F19CC1D1793E883C7752CBAC4AB,
	U3CPauseButtonCoU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF67FD9554DCD05AB4FBE04B8AD497C7C8AC7B37,
	U3CPauseButtonCoU3Ed__1_System_Collections_IEnumerator_Reset_mD2BD98F94117A9ABBAF4D37003008A778C0D7613,
	U3CPauseButtonCoU3Ed__1_System_Collections_IEnumerator_get_Current_m7AFDC398D89441896D3008D1B2B0415EAD735ABA,
	SaveResetButton_ResetAllSaves_mB42FD8A9F9FBA2B18B4B00E55845FEAAF52E5AF6,
	SaveResetButton__ctor_m3C1EB835D7A757A978C99997EF575390B9D97DE1,
	SfxSwitch_On_m184EB8711AD33C0999C0CCB0BDBF05A77828FA72,
	SfxSwitch_Off_m3EEFBBEE676025B14B8CA14152FD6AE3C2EC87E6,
	SfxSwitch__ctor_m0EA89B025E29E332939F76EDD4E801B2B096EF6E,
	StartScreen_Awake_mDEA4F8562CEC276185406B00C550BE86D9880AC7,
	StartScreen_Start_m8E0A400D58DD6B52222E1993E538B98305ACE98A,
	StartScreen_Update_mF6BB05FEAAC94881B3A05CFD3D55FF53188E97BD,
	StartScreen_ButtonPressed_m8CB13A9EC971F01623564D78A568B5BD5582A8EB,
	StartScreen_LoadFirstLevel_m7D50C59812E85C25570FE3785D5E0CA0E61AA5E3,
	StartScreen__ctor_m09C899B309D6FDF680812ECDB06DF725735E563A,
	U3CStartU3Ed__8_MoveNext_m6657946AA5BE7DB9648EE3CD5E3B54E30B01096C,
	U3CStartU3Ed__8_SetStateMachine_m74E3151055CC61E7A5033703DF6031249BAC0790,
	U3CLoadFirstLevelU3Ed__11__ctor_m148453B20FC694DCD92E6E4C27F2868ACAD23488,
	U3CLoadFirstLevelU3Ed__11_System_IDisposable_Dispose_m05D34CEEDE35DD16A5B62648B86F900D61451D17,
	U3CLoadFirstLevelU3Ed__11_MoveNext_mB213D3C241F131CB555C3DF23A3BED0E46D0F8E1,
	U3CLoadFirstLevelU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m67D32B9CF698FD499B1EFAC2705E982A6765633D,
	U3CLoadFirstLevelU3Ed__11_System_Collections_IEnumerator_Reset_mCAEF2AD5DE4959BA7D9F151FB5F1A78EBE80EF66,
	U3CLoadFirstLevelU3Ed__11_System_Collections_IEnumerator_get_Current_m68D05441657E9955AE25B1A08417F7B177F0E8DB,
	InventoryEngineChest_Start_mA244EFB8FE8B4FFDC39373DCC5131277D7FDD78C,
	InventoryEngineChest_OpenChest_m36505217127344B876DEA3423B04BD4A86981C71,
	InventoryEngineChest_TriggerOpeningAnimation_m43657D8550A74B990C2CBC000D8BBAF98B938936,
	InventoryEngineChest_PickChestContents_mF401A005AC74F9B26C8D406F6D68F7119C2DE936,
	InventoryEngineChest__ctor_mB3FB91CB6DD3ED46CB320382FE2F896A7D76E1CA,
	InventoryEngineHealth_Use_mCA0CF06A431305B501863DD4B87B3CA0C34DEF1B,
	InventoryEngineHealth__ctor_mEA787E7437ECEA1FAC3FF0AB259A5D79FCF5DC2E,
	InventoryEngineKey_Use_mF9DEB3CA9C7FBEC46F07B22EC51AA7781D087088,
	InventoryEngineKey__ctor_m67F90E9FDB38D264417E641A9A5A0E31ABD2BE19,
	InventoryEnginePoints_Pick_m72FDE9CCE2EB38953D9574FE4565EB615585F7FB,
	InventoryEnginePoints__ctor_mB93C9ED3BB7E0852B2B77E82F51E22F2A0CD6019,
	InventoryEngineTimeModifier_Use_m8C2598A48F40BCEE65A01D3923139610522266C2,
	InventoryEngineTimeModifier__ctor_mE9C6995B1FBB277893DABEDDE2944B4D514E6C9E,
	InventoryEngineWeapon_Equip_m108B9A2A0AC1BFC44508072D0B0B6E4A0166543B,
	InventoryEngineWeapon_UnEquip_m91E76AA4A82322AA4617F130ADA38DA05240431D,
	InventoryEngineWeapon_EquipWeapon_m4C9D883EC5BB0A52799D613601703DA71B1FDC3A,
	InventoryEngineWeapon__ctor_m11FEAC7A429B63D740D974D49A5AF89F5954A15A,
	Coin_Pick_m62295EA0FF28AC524B91D49BB9D85F249C669BB7,
	Coin__ctor_m2F8A3D35F761C8FD050B3AB2F824C97E4DBE6AD3,
	InventoryPickableItem_PickSuccess_mAE7C49DA437F7F5D70CE542584F97A10B1E550DB,
	InventoryPickableItem_Effects_m08AD585801AA32178E6492BA09A8CD051098E629,
	InventoryPickableItem__ctor_mB5079E59CF427BDD4B192A7886448B25402E7672,
	Loot_Awake_mDE7B59B238CFC42E38A67D80B2078EA794948060,
	Loot_ResetRemainingQuantity_mED1DC8D53F4E25E819EBA2E6CA29BB0018029C8A,
	Loot_InitializeLootTable_mDCC02EF6E3A07B23F9934C53CC912A429529EDCD,
	Loot_SpawnLoot_mD24D796F0BA83406E9E486047A61E35E1103329D,
	Loot_SpawnLootDebug_m445D57BBAE3A32A99740911C360D24D885752ADD,
	Loot_SpawnLootCo_mC338BF194916D173F265EED2CEFA232BC9640547,
	Loot_SpawnOneLoot_m5CF41912021D884F95B8284F2B734E79067CF116,
	Loot_GetObject_m84FDAE0B5DC313CA90C0E65CC8CB8F5E7BFB6512,
	Loot_OnHit_m3836E0BFEE9024EB64E3E082D1D4E76711AE5227,
	Loot_OnDeath_m1F921E44EFED5A8E2DB969D51EEFF3172116CF00,
	Loot_OnEnable_mD4BDC945E34685A8ADEC6FEBBAD2152BC7A26CD4,
	Loot_OnDisable_m66CB9A96524C3AA8419C74DBA10D7A53B67615D3,
	Loot_OnDrawGizmos_m3AAB4BAFC5AF0BDE92993C7397740D3D7034656E,
	Loot__ctor_m4285F00165AE2CF8377E6A4EE7EBFDBA77EBAEBE,
	U3CSpawnLootCoU3Ed__33__ctor_mBE644A8C2503E0F6FB6AF7BE2B0B184B94B2848F,
	U3CSpawnLootCoU3Ed__33_System_IDisposable_Dispose_mB481ECE8ECC126883B1FA7C36D7536420FF18118,
	U3CSpawnLootCoU3Ed__33_MoveNext_mC75AA102DC0611DAC62C612D4DADD20E35F70EC2,
	U3CSpawnLootCoU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m347A80D6745618A10A44AD579DDA4501EC7FC279,
	U3CSpawnLootCoU3Ed__33_System_Collections_IEnumerator_Reset_m4101457E6041C181B1F3D82EA53FAE48DCFC0D11,
	U3CSpawnLootCoU3Ed__33_System_Collections_IEnumerator_get_Current_m291A7D6949A19EF3BFFB0B1F983FCE677687595C,
	PickableAction_Pick_m49960385643A9663C9BC55619109C752CDA4AB73,
	PickableAction__ctor_mEF8C667FE0BA98631B7E721AF33A0EA1AEEBADF8,
	PickableItemEvent__ctor_m9007FE4203B4D474DECD5E651631D3C2BB82E3C1,
	PickableItemEvent_Trigger_m6BB7CDBF4A424E4A7B4CD805BC6EDFF71AAB9512,
	PickableItem_Start_mA869FA4809079960DFE30E24B97B084A52AB9F32,
	PickableItem_OnTriggerEnter2D_m5CE353B8DF7D5918EEE2FE7960711F688E08DCC0,
	PickableItem_PickItem_m2278A8F1E099E6A10570B9BE4BEF0E5B6737FCFC,
	PickableItem_CheckIfPickable_mA0DD9891120BBA4D63313990C6F55A707A25A022,
	PickableItem_Effects_mDAE97894A5C2912F6E10E306645EAF3E55E3CB53,
	PickableItem_Pick_mE4E939C88A97354B97647F8E6484A02D07BF0084,
	PickableItem__ctor_m28A81D4FBEFCE32B10E6DDA43A87624CA4CE02A1,
	PickableJetpack_CheckIfPickable_m001207595C0BA100C53D398C47BBD6A19D6FD027,
	PickableJetpack_Pick_mF7AA4E65304A3A9042BA80C159437C790B232369,
	PickableJetpack__ctor_m4C478F5E97A366C8EC1C919D9F3F400AD4CFBE12,
	PickableOneUp_Pick_m59DB945B639B82F6AC08B1EDD2F09787024BE387,
	PickableOneUp__ctor_mC76F8C423605CEE04556563B613AB30923B401D0,
	PickableWeapon_Pick_m4FCA044643B40DD1E5D709945DC7702CEFD46757,
	PickableWeapon_CheckIfPickable_mBA808421C2823B2E802D99E74285EB9CF9196086,
	PickableWeapon__ctor_mE11D9558785CE1A786EEA1FF6293EAB22DF842A9,
	Star_Pick_mB3A8DC54DDE573E7D153C5B21178EBE0F9F396B3,
	Star__ctor_m778DAC91D16195F08F69B41C82CD752E3F82078B,
	Stimpack_Pick_m807D91CA7EBCBA55AF69D5AE1E8B58CF37241726,
	Stimpack__ctor_m4BE89DC444493B64640589705FBD1AC11908CDC9,
	TimeModifier_Pick_mECA14F5999E169D3E24BCD686D6E24442F910F98,
	TimeModifier_ChangeTime_m4D116BBAAD7F8863BEA3F31F1E1E792F6BD18DE4,
	TimeModifier__ctor_mD08DB9B282CBDBE5305BFF43B9B1B410201CC097,
	U3CChangeTimeU3Ed__4__ctor_m202C97EFD11801DBE0BF4D3ED573965C953468CF,
	U3CChangeTimeU3Ed__4_System_IDisposable_Dispose_m7CB6E539B765CFC38719705B9F1935042A090F89,
	U3CChangeTimeU3Ed__4_MoveNext_m02C2911462BE2495A4F2DB92D4128040A8D77787,
	U3CChangeTimeU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m844658BC6CAB9EE89780F4FADE95307C10A90551,
	U3CChangeTimeU3Ed__4_System_Collections_IEnumerator_Reset_m424FE3642AE7CE3372A5765A81664ABBEEF8B9D1,
	U3CChangeTimeU3Ed__4_System_Collections_IEnumerator_get_Current_m37BD0AFE60F573543BAB1B72A075933E906B1DC9,
	CharacterSwapManager_Start_m62E78F3EC96D02234B59A781C8525177BC7B89DD,
	CharacterSwapManager_UpdateList_m1A2EF08CE80F405F3DC24C82162E6B9A4DE9C55F,
	CharacterSwapManager_SortSwapsByOrder_m2120AE908C73D5B749D50FE9A3F2C9C9F89F2197,
	CharacterSwapManager_Update_mD880E8AB009EAFE33412983B8EA268538AD10E7C,
	CharacterSwapManager_HandleInput_m6219C4AAA56F1811CBB19A5833A99F289FAFFD17,
	CharacterSwapManager_SwapCharacter_mB66084252BE960BDFB45B9DA29F329447E0BBB2B,
	CharacterSwapManager__ctor_m22CD01BB4323C15492F6DEECD8B51E4A89C6DD2B,
	CharacterSwitchManager_ForceCharacterSwitch_m9641E6B7DCB2182DC873665EC0B24F385EE460DE,
	CharacterSwitchManager_ForceCharacterSwitchTo_m2A154823CF8AF3C70F10FE5C703D32A11634456E,
	CharacterSwitchManager_Awake_m4228BF75C9EB636033A0C95BE346FA452285B1FA,
	CharacterSwitchManager_GetInputManager_m2AC6580E874F4033F560AEB86F830DCA6F597946,
	CharacterSwitchManager_InstantiateCharacters_mDA549F8D77137B18A263A2270CD4FFD869F2D5B7,
	CharacterSwitchManager_InstantiateVFX_mB45E61DA57BA89499B3125B46A87AF60393F04D2,
	CharacterSwitchManager_Update_m74022CA011710C0311B480B8099F95EA28C4E067,
	CharacterSwitchManager_DebugCharacterSwitchToTargetIndex_m23DBED3CFEDC8B92E3B520148F9AEA30E698E940,
	CharacterSwitchManager_DetermineNextIndex_mAE8421FD66A4DA24D8E6F197D99E2BF0FC0839CF,
	CharacterSwitchManager_SwitchCharacter_m106021C6A83C821E16572628B56D56A04D4CF497,
	CharacterSwitchManager_SwitchCharacter_m165E313F24974B7EDAD5E4B9E3B4C55430E62EBF,
	CharacterSwitchManager_OperateSwitch_m78896B53CC6C76BB4028CD2EEF4BDF560210F150,
	CharacterSwitchManager__ctor_mD3ED61468D9A64E7ADADB8C92783A6EFA5EAB1AD,
	U3CSwitchCharacterU3Ed__24__ctor_m35E4E3379561072F083B906FC6C0D05B0851245A,
	U3CSwitchCharacterU3Ed__24_System_IDisposable_Dispose_m5F7FD5F397ECBB4C1BD983F08A1FABEF7B258F8B,
	U3CSwitchCharacterU3Ed__24_MoveNext_m342431D82BE8980CC774358DC02E4AE65AF00CF7,
	U3CSwitchCharacterU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2E59D7795ADF632E09549DE34886D1E068A4B8A,
	U3CSwitchCharacterU3Ed__24_System_Collections_IEnumerator_Reset_m34089795EF3BA97D4F0BA86E0F00E69B542DCF6B,
	U3CSwitchCharacterU3Ed__24_System_Collections_IEnumerator_get_Current_mBF3C45B67594C8D0754F5990D1DAC63636930911,
	U3CSwitchCharacterU3Ed__25__ctor_mDB77914A75BFB4D697F234F8A7F6B5F5AFB26BD2,
	U3CSwitchCharacterU3Ed__25_System_IDisposable_Dispose_mFF3091D4925F77B8632278667E1CB9EC628595BE,
	U3CSwitchCharacterU3Ed__25_MoveNext_mC20CFBD4D5C01F5A21DE0DCB622AC8AA6CBD249E,
	U3CSwitchCharacterU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4B2D28B76FE01EAE8151ECFF1387C4801784EF23,
	U3CSwitchCharacterU3Ed__25_System_Collections_IEnumerator_Reset_m9FF46BD57256489A5D281182CEE5FB58DF984F8D,
	U3CSwitchCharacterU3Ed__25_System_Collections_IEnumerator_get_Current_mED9CDBA798F0E5523CEA093A72CE135F46A44EF8,
	U3COperateSwitchU3Ed__26__ctor_m0A96D9F8DB3BE2D83747558A115150B192F8F955,
	U3COperateSwitchU3Ed__26_System_IDisposable_Dispose_mCB0A4C57C7E9D1EC54C7DDBA8ED0EEF70C836262,
	U3COperateSwitchU3Ed__26_MoveNext_m0B515FF18B2B82E0E4F37971073E439B5421CCA0,
	U3COperateSwitchU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2844504259EEE571F8FEE0740ADCDCB006107158,
	U3COperateSwitchU3Ed__26_System_Collections_IEnumerator_Reset_mD0623EDB3009CFBA076CD2307790FF8FFBB8962A,
	U3COperateSwitchU3Ed__26_System_Collections_IEnumerator_get_Current_m3B262E84A7C9560B54C85646969FB84DF3458AA0,
	CorgiEngineEvent__ctor_m271C5C3FA61B0EBC5A1B763403A382C9A7B87029,
	CorgiEngineEvent_Trigger_mB2F63DDFFCA55CD90EA578E552FBC063E561F633,
	CorgiEngineStarEvent__ctor_mDE4FE213A164E3CFFE9D27FB421E145E4E88CF40,
	CorgiEngineStarEvent_Trigger_m59BBB26D3FC2B68976371674FB4730350BE19E3F,
	CorgiEnginePointsEvent__ctor_m4A569E9B13E1E553B656675A137DB2C0537F0200,
	CorgiEnginePointsEvent_Trigger_mE88B8E6726432F7D5F674D2057C173CABD647CC6,
	PointsOfEntryStorage__ctor_m7C17224A561FAF565AA86FEFB829AAB89522C0C8,
	GameManager_get_Points_mA6C2B3C7A921E385C0FD0B4F1CEA86A5933F133A,
	GameManager_set_Points_mB6BAC2DE32C446130A853C402DB23E782A2112CB,
	GameManager_get_Paused_m9174A539CD970E639B908A04A435CABA34B42A9D,
	GameManager_set_Paused_m1D0BB494FB062E819B14B03D7F06032EB85EDF9E,
	GameManager_get_StoredLevelMapPosition_m08B6869E7E9E4A0290804DE50C16976CC0E26F4E,
	GameManager_set_StoredLevelMapPosition_m5B58AFCCA29FDD58077A16E59D1A38246853AA67,
	GameManager_get_LevelMapPosition_m28E4FCF400D62585F90B3D8D311510C4EF76BBF1,
	GameManager_set_LevelMapPosition_m6F6B297ED7D9522CAD10244448B7F59C0587B1AC,
	GameManager_get_StoredCharacter_mCC7662792DA0A1467CA2A4160AE311B56D2A0D25,
	GameManager_set_StoredCharacter_m22668F25A12C107D17E1961C4B73CE76AB9682EC,
	GameManager_get_PersistentCharacter_m50680A37DDAA4C53558BCF87AF27FA2292DC1540,
	GameManager_set_PersistentCharacter_m7F4059ECAD7FF8F6C3E3C6FB8140B802E42EDCE4,
	GameManager_get_PointsOfEntry_m9B3ABDC522D65A00ABF3ED0165CF7B9F5935E073,
	GameManager_set_PointsOfEntry_mC3F482DFCDBDA6264B8F489AD77CC1435EB567AF,
	GameManager_Awake_mE79699E7E898268E6C7231B39D1FD15104F7E864,
	GameManager_Start_mB7A694BCBBB3B56646BF623255AD575180A56B37,
	GameManager_Reset_m6963D8CD865A8E48218EA9CD317A27FE15166373,
	GameManager_LoseLife_mB44A9A6FDCB455815A5F96D06F08DD76B7B1E842,
	GameManager_GainLives_mDFF2B9D0405FB678F69FD320E3A0995EA7819364,
	GameManager_AddLives_mAACABF63506F5F1C16EBE3C6A3EA738ECDB07DE4,
	GameManager_ResetLives_mAC5A951E9918711F4D53AB6179718F4AEC4F8612,
	GameManager_AddPoints_mEC78F37F940318214328C48DB00C1DC7D1BEA599,
	GameManager_SetPoints_m7A69A40B9F6BD775D54CD012A0024983FBA8BC2D,
	GameManager_SetActiveInventoryInputManager_m473B4DE1B595AE783D706944B2B4015FE8CDAD99,
	GameManager_Pause_m387390E4465FA45C9FDA74B22905878365912B68,
	GameManager_UnPause_m5D194DE3B834BC39699356AA0EE4F984D8180F8C,
	GameManager_ResetAllSaves_mFD981A6845A8A2DC1C33FE77206DAA557C5B0DD2,
	GameManager_StorePointsOfEntry_m6D8235A233A241D660FBEBACBE672C92C2C2AE42,
	GameManager_GetPointsOfEntry_mA815C42B3127B4F213EF8B6AD3B99D211A1E9AE9,
	GameManager_ClearPointOfEntry_m39712C670EDFBD3078BBE81F0557969641C8D352,
	GameManager_ClearAllPointsOfEntry_mECF481F4B4E44919DE04AC334AD51E70AE93717A,
	GameManager_SetPersistentCharacter_mE60B9578BB0BE60828E8914B76E8981E9745FE73,
	GameManager_DestroyPersistentCharacter_mEACDEB0474A111344B76FBFA96047650779FFE07,
	GameManager_StoreSelectedCharacter_m41795391983404FA714537B1F55B20F0DDFB1ED5,
	GameManager_ClearStoredCharacter_m4236A377E7D6DA85C29B015CB4163ABE70080D6A,
	GameManager_OnMMEvent_m17BB0A37BAA6D8F2582227A5F4A996D4B09DBC0C,
	GameManager_OnMMEvent_mBB7088A7E550312216B19BE541352097B2DA0C0B,
	GameManager_OnMMEvent_m81C3CF93253446BDBB305CF836E1EAE0E9F68AC1,
	GameManager_OnEnable_mE7FFB42CC6620C31D8B6F32BB3B03348153A0F6B,
	GameManager_OnDisable_m2BCB8A0BEBF0B831B5095AA2B7BE72C4E9F358BA,
	GameManager__ctor_m311129C0AC5254A2643BFC255E9E46A432C62ACA,
	ControlsModeEvent__ctor_mF0BFBFC090EFE50574B19BD5DE2F6C36ABB9F6E1,
	ControlsModeEvent_Trigger_m14F7EABA84BA03A48DB4E55095D4FD9C74B53A69,
	InputManager_get_IsMobile_mD06485D9FB0011F9BB9F2F93716FD28CE192EC17,
	InputManager_set_IsMobile_mE83696E34EDE7CB686DA2FA9FA43FBA04898CE1D,
	InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641,
	InputManager_set_JumpButton_mF0611CFC5D4DCEF678610865A4E9FEBC4D1900EE,
	InputManager_get_SwimButton_m5519300F8D4DE3030F0DFA46C7B2E16D30BDBD29,
	InputManager_set_SwimButton_mBF538BD96562770320DEE979019CC8FABA2ABA68,
	InputManager_get_GlideButton_mE83F8C46DBFFCDAAE2DF29B3E6AA7662CD072CF1,
	InputManager_set_GlideButton_m05EA60FE16119943431B3C44C61872A2318D9995,
	InputManager_get_InteractButton_mE19955DDD59C98044E1D1A691ED12CB20CED0E22,
	InputManager_set_InteractButton_mA9F148ED5D504E27F7BD3667815EA90BB6E59D1A,
	InputManager_get_JetpackButton_mDD8A0EEEC4592EE09E2612A6B38097050BA4D5E1,
	InputManager_set_JetpackButton_mAEB6C4DD90A8D0C15B8F2A9AA7E8089B59A2FC05,
	InputManager_get_FlyButton_m8FE2143D47033FF22CF4537A6EEB6373A79AB0AE,
	InputManager_set_FlyButton_m3834005F5963B7CAC79A1E5107A98BA35BED1091,
	InputManager_get_RunButton_m1BF0C918767106B3814D73F3550245C440035A66,
	InputManager_set_RunButton_m0415BFBAEDC99B349F64229FF379C853AF1E03D0,
	InputManager_get_DashButton_m039788AFC1F68B651EF3179B1FF1739E0515225C,
	InputManager_set_DashButton_m6DEF997E0D9748D7010B0B9C1C87C38FAF0F9BE4,
	InputManager_get_RollButton_m24B6F0B7438ACF95C52AFA73BD4C28299A079F55,
	InputManager_set_RollButton_m6A3EBFEFAD1C6DB711D7EF5314A3276AA975519D,
	InputManager_get_GrabButton_m35B3226127EA6466B0DC4DFF7DC4BF7989242BD4,
	InputManager_set_GrabButton_mDC705CE610874D3B4824860C411CA20AC263EF4B,
	InputManager_get_ThrowButton_m36E076F34EE4A98E8A26E301D3D8914BB52C66EA,
	InputManager_set_ThrowButton_mFD1805CB173D31741E35FEA0CA1EF47D86AE5333,
	InputManager_get_ShootButton_m67631ED77F704CF4C3B470F57B7649B399484B03,
	InputManager_set_ShootButton_m74658C556B88698DCF7739AB997A7F1886A8092B,
	InputManager_get_SecondaryShootButton_m089E50BE434BEEFD14863D19A45E8FD81099CFAA,
	InputManager_set_SecondaryShootButton_m0AC0003444AF61A2E0092A0D8A81F466F57AB402,
	InputManager_get_ReloadButton_m42E2F7163F6D6D0A54BFB82AB63F5A5E9DC44E36,
	InputManager_set_ReloadButton_mA722196BE6CAA90762EA238B0BA648AB5E38B16E,
	InputManager_get_PushButton_mC45D8348FAB555B5AD423A4411B6629B42F3E8F7,
	InputManager_set_PushButton_mAB6181CCBFC870DCB16A07852EEA86168C352E99,
	InputManager_get_GripButton_m411C2464701DF74A8E05104BBC8F07222EE408D9,
	InputManager_set_GripButton_m290C218A7F8A6B551491F77746C0FF4DEE70C489,
	InputManager_get_PauseButton_mDA714172CF94F6397EC18BBD8A9CB1535BBDC1A5,
	InputManager_set_PauseButton_m01DEDDEEA3F0B3CBB35E6AC20F123002AC108C66,
	InputManager_get_SwitchCharacterButton_mD19BFC6172D61F4E182C03EB13D961C86CB58A70,
	InputManager_set_SwitchCharacterButton_mF5E79FC292FAE059F2C462141486DDAA780D2ECE,
	InputManager_get_SwitchWeaponButton_m0290A37CB978A5F5CA6996B3E4B2D5CA961002F2,
	InputManager_set_SwitchWeaponButton_m4996A2354CE3EE2686AF74542AD4784A673E2174,
	InputManager_get_TimeControlButton_m06F1083AE3A4A9BE95C3473B859C2FE9FB208461,
	InputManager_set_TimeControlButton_m6C82676EDACF45214A733D34AC923363845DFACD,
	InputManager_get_ShootAxis_m21A084BB2DCF4FF9A7D413501EFAD24A280B54A0,
	InputManager_set_ShootAxis_m7811EEBFB6F6AA2FFB5FDCE18284132E2ECB1421,
	InputManager_get_SecondaryShootAxis_m2474EE4142A30C7B6B65F2AF5E1C149E94D92480,
	InputManager_set_SecondaryShootAxis_m2277D295BF43B67CFF7B258DCDD6DF3300F11C09,
	InputManager_get_PrimaryMovement_m67D3A20043621ACB7B8436051BB09487FFAC5DFD,
	InputManager_get_SecondaryMovement_mE4A326EEB872065F2DE44EF8F499C3A5BEC07B04,
	InputManager_Start_mC92AF37B2946CAF89B484EBD2DEE990B21629881,
	InputManager_Initialization_m1D51ADD53BEDAB595CB0819EB2EA164D0BD61C71,
	InputManager_ControlsModeDetection_m0A7A6712F26EB0117842F4139F3632304FB938A8,
	InputManager_InitializeButtons_m1DF29416B3E1FE85AFFCD927C39A46E7E2DD85B9,
	InputManager_InitializeAxis_mAD5CEDA5BA5FF7DE0E0BD41DE40F23C45DCC58AF,
	InputManager_LateUpdate_mD4FCA0A5BDF37427E5C0AF14A15E518DD9436DE8,
	InputManager_Update_m98806EDC5F225A93DD117C744C5DF296CC6F4164,
	InputManager_GetInputButtons_mAB7863F012ECC7A283B3F977DAACE8AB759563C1,
	InputManager_ProcessButtonStates_m1AF619776329621E61CF5004F3BA65D720DC2C41,
	InputManager_DelayButtonPress_m723F39166B11B8D4AB46990842264D812C363876,
	InputManager_DelayButtonRelease_mE77481814D3679E4B2410A6BA769713315875256,
	InputManager_SetMovement_m823A76758FE520DE5D745DAD290EA2C4FFB9D049,
	InputManager_SetSecondaryMovement_mAB537815A89158FBCA5D459049F76C883D8BB74A,
	InputManager_SetShootAxis_mEE160BE32D4D2229BC8C66F3BC2BCC33CD24A640,
	InputManager_SetMovement_mABF31A3F0AE5C43FCAD3C94997E02248FAE16F06,
	InputManager_SetSecondaryMovement_m877926EE30286E52E4463E67EE1288A63EDCF6EF,
	InputManager_SetHorizontalMovement_m68444D881C2E129ACC6490686737D010E4EDA14E,
	InputManager_SetVerticalMovement_m4E62B0D57FF2207FDC0447174AECF28E34FBAD70,
	InputManager_SetSecondaryHorizontalMovement_m1E47105772E78F9B8C8BEE0B7EB23883F6A3A02A,
	InputManager_SetSecondaryVerticalMovement_m133F06CB1FD4974ABC382CB4810CDE459EE14FD8,
	InputManager_OnApplicationFocus_m06D4C28672DBDBE1898C510BFEE38EADAC477C63,
	InputManager_JumpButtonDown_mBA46B77B5C71798D025862F6684F817CFABEF3CA,
	InputManager_JumpButtonPressed_m6FDB0DE4AAD41F4BCE3CFD64EBA9453FB354CC50,
	InputManager_JumpButtonUp_m573431B291398A22A4EA5145A9C48163E705194E,
	InputManager_SwimButtonDown_mDABE74C23188478E482A07A501121A85C8CF4AF6,
	InputManager_SwimButtonPressed_m071F6E25BB87BCEC5EE7DD8CF197F92A01A8FCE1,
	InputManager_SwimButtonUp_mC63E7A723ACB0EAF7DEA9799B4A4F793159B2341,
	InputManager_GlideButtonDown_mB5DEA1529E6F578DEB1ED438A731FFDA4536A331,
	InputManager_GlideButtonPressed_mBDDE6066F18C9BC90E98F3ECDC632B8A3014EB9C,
	InputManager_GlideButtonUp_m947EE05AC1602788E95C7F6BC61DA23DFB6ADB4D,
	InputManager_InteractButtonDown_m20F969ED3DE95FEB2223262B8F5DE7342E947736,
	InputManager_InteractButtonPressed_m015476D2345027BA6AC92B916B47F75E8DE67E30,
	InputManager_InteractButtonUp_m09306C95B86D75BBEFC3698058ED782ACC38661D,
	InputManager_DashButtonDown_m654D57B9C690DD5FCE66B156165561E5CDE4C09D,
	InputManager_DashButtonPressed_m6E0C61616EC5FD5E9B629B8ED8486B0448681227,
	InputManager_DashButtonUp_mC585988764745A5E95E4FC7E5D49EE967C40B2F1,
	InputManager_RollButtonDown_m22007CC1874984CE4C7C104553BE8C6D9B6FB8D6,
	InputManager_RollButtonPressed_m29556619D7B5D52E5493F3B7CED80ECE7EDD5E43,
	InputManager_RollButtonUp_mBD23A39A76AFF9DA7C8F7535866EA49074C247F2,
	InputManager_FlyButtonDown_m12F637B2B9A27EBC0D2FFC82975B91AF9C26E842,
	InputManager_FlyButtonPressed_m7B654BBB4DCC9911506F41E2C46224FE3EF9FB6A,
	InputManager_FlyButtonUp_m5B3DB98FD6CD2E271EB73C8D387A9AE720FB419D,
	InputManager_RunButtonDown_mAF1E7D1F1998F0A81C7BAA21C27BE301324AFB8D,
	InputManager_RunButtonPressed_mFCB33D75203D0B8F337065988491F2720ADE2757,
	InputManager_RunButtonUp_m0191DFFBBEAF1D0BBCBAB82AE99A5C03D750A529,
	InputManager_JetpackButtonDown_m7F52CA3280C6BBE9AC1D831C3561B37AE3AFF343,
	InputManager_JetpackButtonPressed_mD3654E4118923978F4C6345C40DDE1CFEF9064C8,
	InputManager_JetpackButtonUp_m6A62A9CEBEB32EF3760F0D2F1B3EC9A53C122E85,
	InputManager_ReloadButtonDown_m08A76864F60F1F1793FB6B2E8EE3C372F7309867,
	InputManager_ReloadButtonPressed_mCFB151C091B9CEADF6070117C48F23A9E784A636,
	InputManager_ReloadButtonUp_mC86B335275C8FC0E5B28421A7397C0C56CF54364,
	InputManager_PushButtonDown_mF75A67242CF6B1F6E64F4FECA859FE23527C09C3,
	InputManager_PushButtonPressed_mE61AB82EF201049A5DE24EF9FE2CF90173D2F397,
	InputManager_PushButtonUp_mFEFB8510884BC1BE39BD4404601EE45B66A53419,
	InputManager_ShootButtonDown_mDD047441E4EA77A6CD96EFF348DE905DFF077DB9,
	InputManager_ShootButtonPressed_m961D763383FFDF4ECE0BC4EF656F0BB8D85C0ABA,
	InputManager_ShootButtonUp_mE52241598B28588D3317E2904D9A7817C2C312A4,
	InputManager_GripButtonDown_m45AD414784F8AEF804F4C22A107557C9DE05DAAA,
	InputManager_GripButtonPressed_m25C7AA25F7F908E81639D4903AE6F533A7A73331,
	InputManager_GripButtonUp_mEDDCD4AEA7498FCCEE1BC259F9CBC98EC3F1B91C,
	InputManager_SecondaryShootButtonDown_mD088DFA78718A9F28A962F86DE86075FCC632E63,
	InputManager_SecondaryShootButtonPressed_mF2E875589DAED34AF7E31463D0B90CEF51936239,
	InputManager_SecondaryShootButtonUp_m124CDE2DD7A5EF98EF5487B3B47384AAFA1CE8BA,
	InputManager_PauseButtonDown_mFAB5620C7E508ED476743AB0AADAF1844FF36C5A,
	InputManager_PauseButtonPressed_mC99F4CD24AF983B3E8CE78143D25AFC23AAC1345,
	InputManager_PauseButtonUp_mABF827D36DD5ADFD867629174850425C6F1099A0,
	InputManager_SwitchWeaponButtonDown_m660CEBC61B3ED645DA805E7F32EEC2A4F0971AC6,
	InputManager_SwitchWeaponButtonPressed_mCD9CBB9C11DEB7DD7EDB6326AF4598EE0954BB1B,
	InputManager_SwitchWeaponButtonUp_m42E89A3BF9341DD25CF8F36D36278EADFD8D8754,
	InputManager_SwitchCharacterButtonDown_m38552D6DFB015AFA5D4A50CE3A86FE3C60875C41,
	InputManager_SwitchCharacterButtonPressed_m092BE8CF9AA69962881ACCFCBCD1A9F563BEED4C,
	InputManager_SwitchCharacterButtonUp_m64A551BDEB2A7593A4025F938E3E51B7632A8280,
	InputManager_TimeControlButtonDown_m52E28E33B600B9D3622C6289EB7413000AE826F0,
	InputManager_TimeControlButtonPressed_mC1F30DB869A398A13EC40704703A0CA024EEB914,
	InputManager_TimeControlButtonUp_m691E1F901247103D193A32401A7C11BB61A069E7,
	InputManager_GrabButtonDown_m3AE7C86E252F08C4CA76EB7E1879A2324BFACD83,
	InputManager_GrabButtonPressed_m6978C95682DE47022A1E689443F6F52522E0B08F,
	InputManager_GrabButtonUp_m90089FFFA0DC24F2716D9A3039895AB5E3DB4853,
	InputManager_ThrowButtonDown_mFADAB06658616AC246245546DDC20A9A041070DD,
	InputManager_ThrowButtonPressed_m39C613CA1B8D7C429DEF4BD7D077723963EE1845,
	InputManager_ThrowButtonUp_mC3C1BF8F5073096F605844E722C0A0F078027903,
	InputManager__ctor_m8C9F54C63E83C19DEAB926944E5CCC11FCD2EF78,
	U3CDelayButtonPressU3Ed__126__ctor_mAC4CF5653FB48B7732171CD585161D8A70ACD119,
	U3CDelayButtonPressU3Ed__126_System_IDisposable_Dispose_mC51D42389E6DF930F84B59C55756000DC0ABCCC0,
	U3CDelayButtonPressU3Ed__126_MoveNext_m4ADA1AF2668FDC2D6720B40B7C01B860FA05CBA1,
	U3CDelayButtonPressU3Ed__126_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m23D7BE989362C5AEDE604A88801FF93B73E5E40E,
	U3CDelayButtonPressU3Ed__126_System_Collections_IEnumerator_Reset_m64C80CAA37B8744D75349DB5DA606F0E2C1CE3EF,
	U3CDelayButtonPressU3Ed__126_System_Collections_IEnumerator_get_Current_mE20B5FC773311C807A1E36952CD0C273ACE67A14,
	U3CDelayButtonReleaseU3Ed__127__ctor_mD46DD939D600F2A822855CB7538AE66C4DCEDACD,
	U3CDelayButtonReleaseU3Ed__127_System_IDisposable_Dispose_m6601669EA33E611957E8766EBC353C629A69A531,
	U3CDelayButtonReleaseU3Ed__127_MoveNext_m48869728468B37310A76C82C4C7BA532C8028D97,
	U3CDelayButtonReleaseU3Ed__127_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8BCC5829459C931A182BD2FBD9E1D9CC14EF9627,
	U3CDelayButtonReleaseU3Ed__127_System_Collections_IEnumerator_Reset_m5DE2670AC5FC794627E1FF71B5AAF7B8B4C34936,
	U3CDelayButtonReleaseU3Ed__127_System_Collections_IEnumerator_get_Current_mBFAFFECF5F685151959AB0B3524F846DE45A5752,
	LayerManager__cctor_m470D4EB01D68C7B4D23677187B2313518FD3603E,
	LevelNameEvent__ctor_mA505A3AAFBAA2FE2B3B975077CFE9635C6ACCDCD,
	LevelNameEvent_Trigger_mC787686ABDB2DED88363E8A01D83D1E27E8AEADC,
	LevelManager_get_BoundsCollider_mF941E9DDAD3B216DE55E1A7ECD29F1A3D349E922,
	LevelManager_set_BoundsCollider_m44E20473650E6AD06BDF66B22918BF97579199CE,
	LevelManager_get_BoundsCollider2D_m38E36A905EB6DF0E0B21758DDA955453641339B0,
	LevelManager_set_BoundsCollider2D_mBC2BBF7279D702EB3DAE3C6D63D1B009A6BA5AF6,
	LevelManager_get_RunningTime_m9E9475EBB8686A0E1F2832A34A77E6F48F3E0828,
	LevelManager_get_LevelCameraController_m81BFB44DD7452381F90CE3F622A7FF33A5953152,
	LevelManager_set_LevelCameraController_m5F06B18F9583BFDE427343793B96FBA1F1FCBC03,
	LevelManager_get_Players_m0B775B167DBB299A157CBD0E367696AC850486F7,
	LevelManager_set_Players_mA2DF40751ACA47C16085490346C62C1F6AEBA78E,
	LevelManager_get_Checkpoints_m60A1768AC654A00CB7061A3A7E6E508B43606617,
	LevelManager_set_Checkpoints_mF60E720FDAA9EFB922600772B12AC0D47BB4CD54,
	LevelManager_Awake_m6663A05293EE49A8B761C2CC06E7839867BF3C6D,
	LevelManager_InstantiatePlayableCharacters_mB2ACB590651DB6C41E1D5D4513D9E88F65796751,
	LevelManager_Start_m606417779B17ECCDC0EE2BEBA335B3810E409676,
	LevelManager_Initialization_mDE8D6D1DF642695C7F2258230B2F564ECEC8955C,
	LevelManager_CheckpointAssignment_m4035FACE16ADE006C76696D1C2A073481B1B1F9D,
	LevelManager_LevelGUIStart_m2CB604FBCF9B8A78F529F5BC7451FDABB29182D5,
	LevelManager_SpawnSingleCharacter_m7388D91F3AA85EBDB4590353A26D67C0D0DF8495,
	LevelManager_RegularSpawnSingleCharacter_mA5302AC157AD878DC52105B8DFC077D04C8603E0,
	LevelManager_SpawnMultipleCharacters_m4B2312CEFD02A9482E4CC0E3105329099CF369E2,
	LevelManager_SetCurrentCheckpoint_m20D3DA12F673D300F1D826611BE92E491AC544D4,
	LevelManager_SetNextLevel_mD95E12975C5C79462A32EAF795941F001989360C,
	LevelManager_GotoNextLevel_m4B136E7FC13A8D165B7587E0197C7ED6F5314548,
	LevelManager_GotoLevel_mB68FBCB4780C290C60298DE5FFED11CAE4D3CB75,
	LevelManager_GotoLevelCo_m20304615D717DDE00BE332CF051ECF95828AEB79,
	LevelManager_KillPlayer_m14BDFAE690266DBC75496EE70576660CEF09C968,
	LevelManager_Cleanup_m4F358D708A18997242B8B0837DC02DBD004B423A,
	LevelManager_SoloModeRestart_m5B0ECB363FE6DF71D246F99D6AAF286317DBF494,
	LevelManager_FreezeCharacters_m8D619F9AF7F4FF1EFE349ABE236686942390BB29,
	LevelManager_UnFreezeCharacters_m0B9EF058DEC38CF8164B484318C8834918489C74,
	LevelManager_ToggleCharacterPause_m2F9F02306FA5BF0033A54DF061E50CD2A09682EE,
	LevelManager_ResetLevelBoundsToOriginalBounds_mC1FE8AEC43C5C04CAA92F2870D8CD7AF5187D461,
	LevelManager_SetNewMinLevelBounds_mD35B9E95E1FD2CC44BB8F848FE629601E4EE2EA2,
	LevelManager_SetNewMaxLevelBounds_mE7E84DCC2E27B2DDB3CCB30F51911E9FA1DDFC24,
	LevelManager_SetNewLevelBounds_m5C1CDDCE8ED365A3752702CF69466EEDFB121B14,
	LevelManager_UpdateBoundsCollider_mDE3576E4D6320BD5FFFFAA359B044744E1699573,
	LevelManager_GenerateColliderBounds_m71CB19B1F13861BA54698B282824F5BCE605C7E5,
	LevelManager__ctor_m9173267A7501E5FC7465F02A4475F62AF793E04C,
	U3CU3Ec__cctor_m7A5B85089DDA03F6A24E6C725C4464AE0D3D8E82,
	U3CU3Ec__ctor_m80561866531FD9296EF176A1AF80830086873032,
	U3CU3Ec_U3CInitializationU3Eb__53_0_m7852DD0D858FAEBB65EBD28059CF51D5F021C1AB,
	U3CU3Ec_U3CInitializationU3Eb__53_1_mA377F3F6166EB42C09ADB76EFCF63D6DCCF75C50,
	U3CU3Ec_U3CInitializationU3Eb__53_2_m0C2001C2AC0F6D8C95CE3C7CF3F5CA469DF0DF9B,
	U3CU3Ec_U3CInitializationU3Eb__53_3_mCD9627595B0E51B1D7B4A25F4E2831A8034FBCB2,
	U3CU3Ec_U3CInitializationU3Eb__53_4_mFA082451DFC33B2AE9C309704489C21777C9DBF7,
	U3CU3Ec_U3CInitializationU3Eb__53_5_mC178C56A614CEB7F64673011CB6991C0FF1FEAF8,
	U3CU3Ec_U3CInitializationU3Eb__53_6_m177590BCE789DCDAAB33119B640B6C1EE912AF23,
	U3CGotoLevelCoU3Ed__63__ctor_m54C25D3CC52247F6097BCF1D2398F0074A922299,
	U3CGotoLevelCoU3Ed__63_System_IDisposable_Dispose_mA252953C2F811BFCCD957B3CC301C9394272E934,
	U3CGotoLevelCoU3Ed__63_MoveNext_mB22B77A03FB11F69B3BB1E539BE9722BC8268BEC,
	U3CGotoLevelCoU3Ed__63_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m90C4C40BED85C61338E36D8567B6B98908AC4A79,
	U3CGotoLevelCoU3Ed__63_System_Collections_IEnumerator_Reset_m770A78158227D664C272C936D64986E88AD30912,
	U3CGotoLevelCoU3Ed__63_System_Collections_IEnumerator_get_Current_mF4AD1F04E1AE9E994B1027B541D648ABE2C16949,
	U3CSoloModeRestartU3Ed__66__ctor_mC989B5660989A9C5CAC3455ED0DE021F7868BF5B,
	U3CSoloModeRestartU3Ed__66_System_IDisposable_Dispose_mF2EF84CC336BA905F4EE93D71CE5275D5985FBC0,
	U3CSoloModeRestartU3Ed__66_MoveNext_mA0018D2D66AC126D14C9B2F2B0526D31C6FB30C7,
	U3CSoloModeRestartU3Ed__66_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32A94B3DAA4F9066742EEAEC9628A72301D3A95B,
	U3CSoloModeRestartU3Ed__66_System_Collections_IEnumerator_Reset_m19B364D73ADFF729A1BE70721A88A228AF218724,
	U3CSoloModeRestartU3Ed__66_System_Collections_IEnumerator_get_Current_m0591E492485FE51D1CEA9C057187B2DA66444A2D,
	MultiplayerLevelManager_CheckMultiplayerEndGame_mB707BA9168A83F4AE542272C5D51565E2B4D0E9D,
	MultiplayerLevelManager_MultiplayerEndGame_m6573546B15C81907C79A22050393F52E790E0019,
	MultiplayerLevelManager_KillPlayer_mF878CF218D2EC6B0F7381CD3E258BAC6B7E18D78,
	MultiplayerLevelManager_RemovePlayer_mA5B3BC884CDE86253BFA3D05207F47ED138F6A26,
	MultiplayerLevelManager__ctor_m0147E74E961D2503F145E18997619068868A40C5,
	U3CMultiplayerEndGameU3Ed__1__ctor_m2FD1F85559F2D52A7739D671283ED4C338886E38,
	U3CMultiplayerEndGameU3Ed__1_System_IDisposable_Dispose_mB0747C8358903F67216A106AE7C0521D5963EA57,
	U3CMultiplayerEndGameU3Ed__1_MoveNext_m7BF00B691FCBC3446748D2EE76D5F219A9EE274A,
	U3CMultiplayerEndGameU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m20B173E8AC86C43F20E8B23A43145CAC95EA2AF8,
	U3CMultiplayerEndGameU3Ed__1_System_Collections_IEnumerator_Reset_m43B353AE82D732015F2B9F78C54EE8E21A9A8081,
	U3CMultiplayerEndGameU3Ed__1_System_Collections_IEnumerator_get_Current_m2800EE7C4B6AFD4C35FBDDADAEDFA76E795E0707,
	U3CRemovePlayerU3Ed__3__ctor_m14C7E0979678D2948DFEDBB7394FF04C81C8C63A,
	U3CRemovePlayerU3Ed__3_System_IDisposable_Dispose_mF5034DA0562D1571A7C20F3D7BCECF7C75303C5B,
	U3CRemovePlayerU3Ed__3_MoveNext_m4BAD30513EE5C402AE8BB906ECE15FDB0DE25C98,
	U3CRemovePlayerU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m522AD5255BB066B43B63561D7900E7CA8670A948,
	U3CRemovePlayerU3Ed__3_System_Collections_IEnumerator_Reset_m9D7B8803DB320B65B0B5CCAE8D76ED6290D50EFA,
	U3CRemovePlayerU3Ed__3_System_Collections_IEnumerator_get_Current_m28B25CBA4093A84DE18C6D8DAD112B04F18350F4,
	OneWayLevelManager_SetPreventGoingBack_m40881610A852D7AF8878133D9EFA09556B993EF7,
	OneWayLevelManager_SetOneWayLevelAutoScrolling_mFEB1C1B540872E749D49D830D9B543074227CA53,
	OneWayLevelManager_SetOneWayLevelAutoScrollingSpeed_m16E942BBBEB0D061DF1D3AF5706C0B5A3925164A,
	OneWayLevelManager_Initialization_m918161CF39826EC1C8A31B1D45A398B612CD23BD,
	OneWayLevelManager_Update_m707F58CBBD92450C8F55CB351E5A0A203894AD9A,
	OneWayLevelManager_HandlePreventGoingBack_m2B526139BC6D6B5F7217BFDF02509E6F12250456,
	OneWayLevelManager_HandleAutoMovement_mA60BFE478A58AD6B4073D3EBF1084612A9C9AE19,
	OneWayLevelManager_OnMMEvent_m7EA1AEEAA57CFB59C1AC5BE7E37D08157347B3BA,
	OneWayLevelManager_OnEnable_mE91D8668768AD3BC8AEFA605AC84F28CB2AD4848,
	OneWayLevelManager_OnDisable_m266A78FCCF297B31FBD77FD1643C4417EF2BBBD4,
	OneWayLevelManager__ctor_m3913B0760E00498C52367C59DBDFBE5FF7878B3E,
	ProximityManager_Start_m933E957AE61DE895F3B6C62691BEC533E46198A9,
	ProximityManager_GrabControlledObjects_m9946DB3A9FAD2725870CBD03EE0A731EC0656FC4,
	ProximityManager_AddControlledObject_m7A388F048A231A858CE0B25228F20F3BDCFEC7BC,
	ProximityManager_SetPlayerAsTarget_mB6A89CE4F2582D5E894E54258FB7E2247111C9AB,
	ProximityManager_Update_mD0EDB6FFFCA7110143CC0879687F6CCBD4585134,
	ProximityManager_EvaluateDistance_m32DEE4E3FFFBD782F15FFC37670EFF2213B1BEC4,
	ProximityManager_OnMMEvent_mBB06A9BD84692C80A7444DE6C774265B8FEC594D,
	ProximityManager_OnEnable_m73511187D113CE40E04A483C26C8A479B268C699,
	ProximityManager_OnDisable_mF7E20C464204672A73814F36720A6AB0B9E10086,
	ProximityManager__ctor_m3E8C316C792B7243B323A3B0C8EB7AA452B2EED9,
	SoundSettings__ctor_m74D825F974A0E3818DDDA6E9AF23B6B54B5BDBEE,
	SoundManager_PlayBackgroundMusic_m7480100934A9A10DDE5551505C5421A1BF78058F,
	SoundManager_PlaySound_m6DA66D64E1BE804EFB8FD8564E95719BF1B2B905,
	SoundManager_PlaySound_m52351C8F80B1934BB7F6ED8164F1E7D97B029A78,
	SoundManager_StopLoopingSound_mA3C807031796D5ECFFCD4E858D586AB1900FE068,
	SoundManager_SetMusic_m3C6E6809B43CCEF5890647BB74DD128429684DE6,
	SoundManager_SetSfx_mB2759880AA04E7A806088BF4E185B170D12CFC00,
	SoundManager_MusicOn_mF5D6EC78106A338922300569162A6640D14F2A6F,
	SoundManager_MusicOff_mBE8723A71F2D590B59BEA4CE91FA5A3DD3198B38,
	SoundManager_SfxOn_m26469A260353484608AF4189FFB5BFDDE0BB135E,
	SoundManager_SfxOff_mBE76DFD942F418FA78C3FDD9FA50E4DCC0990640,
	SoundManager_SaveSoundSettings_m951107F11B1AE635F8B8E9F154311A95188CBE8E,
	SoundManager_LoadSoundSettings_m26F288CD6350D80B73AEAFC600DD544285A3939A,
	SoundManager_ResetSoundSettings_m4F2026396638CD4E2DF1BDF246698AE7FA2A5381,
	SoundManager_OnMMSfxEvent_m8105170A03B322B9555E4C3D724FA9CC4B93BA6B,
	SoundManager_OnMMEvent_m6EBD080A9DD1C55EA74A9FF7BFB4C7FFF81EA2FD,
	SoundManager_OnMMEvent_m8AB2A1EF39B5CD0B4E7EE8267464DDB479C58EF0,
	SoundManager_MuteAllSfx_m214A0097CF75C548955071D7CA7FB18EB22AD7E9,
	SoundManager_UnmuteAllSfx_m58D505875FB2D0E269A950E043F91D0F0708CADB,
	SoundManager_UnmuteBackgroundMusic_mB67C356A39B10F4BA6918EBCA0A1C48A341DA958,
	SoundManager_MuteBackgroundMusic_mEB6C64A6C4ACA1DE9C1026A56B2A0CE466D101DD,
	SoundManager_OnEnable_m59E8D0568748FE6DDB27818A492A0A8230C89BC7,
	SoundManager_OnDisable_mC321240BE759E3B6C7E8585962F62EE0D7892C83,
	SoundManager__ctor_m8CD89E3BF4DF30118A310A8EE7B8E9BCCB0EFC37,
	TilemapLevelGenerator_Awake_m65E588995EE3B36FAAEB3E1B6738809E7C7BD752,
	TilemapLevelGenerator_Generate_m1511614AAD0D47FDBBC6BE3330532791451A7A10,
	TilemapLevelGenerator_ResizeLevelManager_mCD299A9211F850E247CAC01DA1C4CA84ACF8C112,
	TilemapLevelGenerator_PlaceEntryAndExit_mAADBBDC1011DCF8380AA18471DA9F18B5CA55155,
	TilemapLevelGenerator_PlaceFloatingObjects_mF919C637BDE5F20E7A790600EC12424E0F895E0C,
	TilemapLevelGenerator__ctor_mF7EA39306A073843D62350CF9E14E30738A15928,
	BackgroundMusic_Start_mB5D8ED68407EC48A0B7AAA36126268D396B4B391,
	BackgroundMusic__ctor_m6C59FAC9058123FB7D9A2B9DE1F1C7F9E8CCCE00,
	AutoRespawn_Start_m3C7733608EB514362F618ED2638793C5E50B03DB,
	AutoRespawn_OnPlayerRespawn_m8045F2226FF743E929A39B8D711FAA2F24E46A9C,
	AutoRespawn_Update_mD42D92AF53947F15848601E12D2FE58727D372D8,
	AutoRespawn_Kill_m05DD0D123B242AB38463CC78CD0B85CF6EBAB6E1,
	AutoRespawn_Revive_mCA571251F8E82CDF313EA7959A3BF12B2BC7917E,
	AutoRespawn__ctor_m2DE5FE2CDC1AD2B5449CA493CAB8D594DB97BFF5,
	OnReviveDelegate__ctor_mB932216BFD1E603279DC64EEA152CB80B4D02EF4,
	OnReviveDelegate_Invoke_m5AE88D0C530285B224C29CEFB3D450D9EFBAD605,
	OnReviveDelegate_BeginInvoke_mC2F811CE7856F2C070276C62FBD12BE564C106D9,
	OnReviveDelegate_EndInvoke_m1F2F937FC7CB813F4E4DBABB3EE04109E1ADBE2B,
	CharacterSelector_StoreCharacterSelection_m7CAEF44E40213AD165462A6910466670E3053AB6,
	CharacterSelector_LoadNextScene_m056B6C8BC6C263931F50569994C2E2CA90D95EB5,
	CharacterSelector__ctor_mB94BB67E790CD9083A3D4B3DD85008E4B0EFE875,
	CheckPoint_Awake_mAE8CEDAFABEBF879CF0AEE8CB1AE9F85B3F95403,
	CheckPoint_SpawnPlayer_m0F1353E4BD7EE34F41524E9CECD4046396D7484B,
	CheckPoint_AssignObjectToCheckPoint_m24BB28E388D9BBBB911930F0D10850111E06433B,
	CheckPoint_OnTriggerEnter2D_m75BAEABB7E7FC92522255E8AAFE1078E07C96CAC,
	CheckPoint_OnDrawGizmos_m0D19398D6ED9E2C577A9E8242DA0129DDF6D0B13,
	CheckPoint__ctor_mB43D6F915DF9F644DAA12AB3224971B3B4E5C9D1,
	FinishLevel_Initialization_m22A453317168ACA7C69FD3C7B92AA7B091C5B534,
	FinishLevel_TriggerButtonAction_mC129DA187BBF045FBB38C782BF6DCE69A54BEA98,
	FinishLevel_GoToNextLevelCoroutine_m41BE1F809323D567B2932316FA8BBED5C485D61B,
	FinishLevel_GoToNextLevel_mB3160F319410C34441CCD519517A9DA4B63DB19E,
	FinishLevel__ctor_m01928DBC9628666ADFA3760A58298DC85566498F,
	U3CGoToNextLevelCoroutineU3Ed__11__ctor_m3D343D49C6472E9ED51658DA9B1E0B3CFB5A9C20,
	U3CGoToNextLevelCoroutineU3Ed__11_System_IDisposable_Dispose_m75CAA28400D03CD10E1E404A32CA71548B9F16C5,
	U3CGoToNextLevelCoroutineU3Ed__11_MoveNext_m07275CD0B3040F46C80CF32E2D44987D49C5CC74,
	U3CGoToNextLevelCoroutineU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7097FBEA4F979310809478774DEF9822109304BD,
	U3CGoToNextLevelCoroutineU3Ed__11_System_Collections_IEnumerator_Reset_m7FB796138113BF05CA15FA90B9F86363501411D7,
	U3CGoToNextLevelCoroutineU3Ed__11_System_Collections_IEnumerator_get_Current_mAA6E375544B528CE8BB013638E292BF99AF1C8E9,
	GoToLevelEntryPoint_GoToNextLevel_mBF5F0C7CAF1497956D471307280E6A7E160D64E5,
	GoToLevelEntryPoint__ctor_m80E1E6929251098AD07466A8C871B9753D4E97DF,
	LevelRestarter_OnTriggerEnter2D_m9566117FE5366530ACB6BBFAE4972C1E63EC23C1,
	LevelRestarter__ctor_m1B25336E2CCCA966C3394EAF034914252089029C,
	NULL,
};
extern void MMSpriteMaskEvent__ctor_m6C3949E81BA53ADBF7BC370886DDFAAEFC6C40B7_AdjustorThunk (void);
extern void CorgiEngineCharacterAnimationParameter__ctor_m422C90AF2970DD2AB766173152F7E3962D1A22CD_AdjustorThunk (void);
extern void MMCharacterEvent__ctor_mE6B7788A4EDF79D0DEB62E4E58569FA2AE2A4D52_AdjustorThunk (void);
extern void MMDamageTakenEvent__ctor_m0BFC19EF73E4AC6B6B61A5B7AE47EFAC5E1FE6AF_AdjustorThunk (void);
extern void MMCameraEvent__ctor_mBE0879CC29D68FAEA3CEBF1FA047E13543BC0EB0_AdjustorThunk (void);
extern void LedgeEvent__ctor_mF87F02B223BF24015920133ECA5E206185B3B372_AdjustorThunk (void);
extern void U3CStartU3Ed__8_MoveNext_m6657946AA5BE7DB9648EE3CD5E3B54E30B01096C_AdjustorThunk (void);
extern void U3CStartU3Ed__8_SetStateMachine_m74E3151055CC61E7A5033703DF6031249BAC0790_AdjustorThunk (void);
extern void PickableItemEvent__ctor_m9007FE4203B4D474DECD5E651631D3C2BB82E3C1_AdjustorThunk (void);
extern void CorgiEngineEvent__ctor_m271C5C3FA61B0EBC5A1B763403A382C9A7B87029_AdjustorThunk (void);
extern void CorgiEngineStarEvent__ctor_mDE4FE213A164E3CFFE9D27FB421E145E4E88CF40_AdjustorThunk (void);
extern void CorgiEnginePointsEvent__ctor_m4A569E9B13E1E553B656675A137DB2C0537F0200_AdjustorThunk (void);
extern void ControlsModeEvent__ctor_mF0BFBFC090EFE50574B19BD5DE2F6C36ABB9F6E1_AdjustorThunk (void);
extern void LevelNameEvent__ctor_mA505A3AAFBAA2FE2B3B975077CFE9635C6ACCDCD_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[14] = 
{
	{ 0x0600000E, MMSpriteMaskEvent__ctor_m6C3949E81BA53ADBF7BC370886DDFAAEFC6C40B7_AdjustorThunk },
	{ 0x060000F4, CorgiEngineCharacterAnimationParameter__ctor_m422C90AF2970DD2AB766173152F7E3962D1A22CD_AdjustorThunk },
	{ 0x06000372, MMCharacterEvent__ctor_mE6B7788A4EDF79D0DEB62E4E58569FA2AE2A4D52_AdjustorThunk },
	{ 0x06000374, MMDamageTakenEvent__ctor_m0BFC19EF73E4AC6B6B61A5B7AE47EFAC5E1FE6AF_AdjustorThunk },
	{ 0x06000595, MMCameraEvent__ctor_mBE0879CC29D68FAEA3CEBF1FA047E13543BC0EB0_AdjustorThunk },
	{ 0x06000616, LedgeEvent__ctor_mF87F02B223BF24015920133ECA5E206185B3B372_AdjustorThunk },
	{ 0x060006EC, U3CStartU3Ed__8_MoveNext_m6657946AA5BE7DB9648EE3CD5E3B54E30B01096C_AdjustorThunk },
	{ 0x060006ED, U3CStartU3Ed__8_SetStateMachine_m74E3151055CC61E7A5033703DF6031249BAC0790_AdjustorThunk },
	{ 0x06000720, PickableItemEvent__ctor_m9007FE4203B4D474DECD5E651631D3C2BB82E3C1_AdjustorThunk },
	{ 0x06000764, CorgiEngineEvent__ctor_m271C5C3FA61B0EBC5A1B763403A382C9A7B87029_AdjustorThunk },
	{ 0x06000766, CorgiEngineStarEvent__ctor_mDE4FE213A164E3CFFE9D27FB421E145E4E88CF40_AdjustorThunk },
	{ 0x06000768, CorgiEnginePointsEvent__ctor_m4A569E9B13E1E553B656675A137DB2C0537F0200_AdjustorThunk },
	{ 0x06000794, ControlsModeEvent__ctor_mF0BFBFC090EFE50574B19BD5DE2F6C36ABB9F6E1_AdjustorThunk },
	{ 0x06000825, LevelNameEvent__ctor_mA505A3AAFBAA2FE2B3B975077CFE9635C6ACCDCD_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2250] = 
{
	3506,
	3506,
	2206,
	2807,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	294,
	3964,
	3476,
	3506,
	3506,
	716,
	716,
	513,
	513,
	2610,
	2610,
	2807,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2797,
	2791,
	2732,
	2817,
	2681,
	2680,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3473,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	3473,
	3506,
	3473,
	3473,
	3506,
	3506,
	3473,
	3506,
	3473,
	3473,
	3506,
	3473,
	3473,
	3506,
	3473,
	3473,
	3506,
	3473,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	1701,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	2837,
	3506,
	3506,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1689,
	3506,
	3506,
	3473,
	3473,
	3445,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	1005,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2839,
	3506,
	2839,
	3506,
	2837,
	3506,
	3506,
	3506,
	3506,
	3445,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	926,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	2775,
	3473,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3506,
	3506,
	3473,
	2839,
	3506,
	3506,
	3506,
	3445,
	3476,
	2839,
	3506,
	3506,
	2839,
	2839,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3476,
	2839,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3476,
	3500,
	3473,
	2837,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	2815,
	2815,
	2815,
	2815,
	3506,
	1721,
	3473,
	2839,
	3506,
	3506,
	3506,
	3506,
	2732,
	3506,
	3506,
	3506,
	3445,
	3473,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3424,
	3506,
	3506,
	3445,
	3424,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1023,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3476,
	2839,
	3476,
	3473,
	2837,
	3506,
	3506,
	3506,
	2839,
	2839,
	3506,
	3506,
	3506,
	3506,
	2567,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	2798,
	2732,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3473,
	2837,
	3473,
	2837,
	3473,
	3506,
	3506,
	3506,
	3473,
	3473,
	3506,
	2837,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3506,
	3506,
	3506,
	3506,
	3500,
	2860,
	3473,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	2785,
	2815,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1689,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	2837,
	3506,
	3506,
	2797,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	2775,
	3473,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3473,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3476,
	2568,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3473,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3473,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1692,
	3506,
	1220,
	2815,
	3445,
	2815,
	3473,
	2837,
	3473,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3473,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	-1,
	2815,
	3506,
	3506,
	2815,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1689,
	2837,
	3506,
	3506,
	2775,
	3506,
	2862,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1689,
	4948,
	318,
	3983,
	3506,
	3506,
	1586,
	3506,
	3506,
	3445,
	2815,
	3445,
	3445,
	2815,
	3445,
	2815,
	3500,
	3500,
	3500,
	2860,
	3445,
	2815,
	3502,
	3502,
	3502,
	3502,
	3502,
	3502,
	3473,
	3476,
	3476,
	3476,
	3476,
	3500,
	3502,
	3502,
	3502,
	3502,
	3502,
	3502,
	3502,
	3502,
	3502,
	3476,
	3500,
	3506,
	3506,
	3506,
	3506,
	2860,
	2839,
	2839,
	2860,
	2839,
	2839,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	564,
	3506,
	3506,
	2839,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2211,
	3506,
	3506,
	2211,
	2211,
	3506,
	3506,
	3506,
	3506,
	3506,
	2839,
	2837,
	2860,
	3506,
	3473,
	2862,
	2605,
	3506,
	3506,
	2815,
	2815,
	2815,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3473,
	3476,
	2839,
	3476,
	2839,
	3476,
	2839,
	3473,
	2837,
	3473,
	2837,
	3473,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3473,
	2837,
	3506,
	3445,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	2815,
	2815,
	2815,
	2815,
	3506,
	3506,
	3506,
	2775,
	3506,
	3506,
	1692,
	3506,
	1220,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	2815,
	3506,
	3424,
	2775,
	3502,
	2862,
	3506,
	3506,
	3506,
	3506,
	3506,
	291,
	3506,
	3506,
	3506,
	1549,
	1549,
	3506,
	2837,
	3506,
	3506,
	3506,
	3506,
	2211,
	2211,
	3506,
	3506,
	3506,
	3506,
	1692,
	3506,
	1220,
	2815,
	1692,
	3506,
	1220,
	2815,
	1692,
	3506,
	1220,
	2815,
	1692,
	3506,
	1220,
	2815,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	2830,
	2830,
	2830,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3460,
	2829,
	3461,
	2830,
	3502,
	2862,
	3506,
	3506,
	3506,
	3506,
	1742,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	1066,
	3506,
	2815,
	2815,
	2775,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3445,
	2815,
	3506,
	3506,
	514,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3473,
	2837,
	3445,
	2815,
	3506,
	3506,
	3506,
	1695,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	1718,
	2209,
	2815,
	3500,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1695,
	3506,
	1695,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3458,
	3476,
	2839,
	3476,
	3500,
	2860,
	3506,
	3506,
	2862,
	3506,
	3506,
	3506,
	3506,
	2827,
	3506,
	3506,
	3506,
	2839,
	3506,
	3506,
	3445,
	2815,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	2837,
	2682,
	2798,
	2797,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	1549,
	2775,
	1695,
	3506,
	3506,
	3506,
	3506,
	3506,
	2837,
	3506,
	3473,
	2837,
	3506,
	3506,
	2815,
	3506,
	2862,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2732,
	3506,
	3506,
	2790,
	50,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2839,
	3506,
	634,
	4254,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3476,
	3476,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	2837,
	3506,
	2815,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	1695,
	3506,
	3506,
	3506,
	2815,
	2815,
	2815,
	2427,
	3506,
	2815,
	2815,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	2815,
	1566,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3500,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3445,
	2815,
	3445,
	2815,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	1695,
	4954,
	2815,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3473,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	1695,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	2815,
	3506,
	2815,
	2815,
	3506,
	3506,
	3473,
	2815,
	2815,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1743,
	3506,
	1743,
	1743,
	3506,
	3506,
	2815,
	201,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	2815,
	2815,
	2815,
	2427,
	3473,
	2427,
	3506,
	3506,
	2815,
	2723,
	2723,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2815,
	2837,
	1419,
	2839,
	2839,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3445,
	3445,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	2837,
	2837,
	1717,
	2837,
	1718,
	1050,
	2837,
	3506,
	713,
	713,
	79,
	2815,
	2787,
	2730,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3506,
	2815,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	2427,
	3506,
	2427,
	3506,
	2427,
	3506,
	2427,
	3506,
	2427,
	2427,
	1695,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	2815,
	5339,
	3506,
	2815,
	2815,
	3473,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3473,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	4669,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3445,
	2201,
	3445,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	5314,
	1689,
	4948,
	1532,
	4927,
	1008,
	3424,
	2775,
	3473,
	2837,
	3473,
	2837,
	3500,
	2860,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	2775,
	1564,
	3506,
	2775,
	2775,
	2837,
	2775,
	2775,
	3506,
	1008,
	2209,
	2815,
	3506,
	2815,
	3506,
	2815,
	3506,
	2797,
	2732,
	2733,
	3506,
	3506,
	3506,
	1717,
	4971,
	3473,
	2837,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3424,
	2775,
	3424,
	2775,
	3500,
	3500,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2209,
	2209,
	3506,
	3506,
	3506,
	2860,
	2860,
	2839,
	2839,
	2839,
	2839,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	5460,
	2815,
	5339,
	3445,
	2815,
	3445,
	2815,
	3490,
	3445,
	2815,
	3445,
	2815,
	3445,
	2815,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	3506,
	1035,
	1221,
	2815,
	3506,
	3445,
	3506,
	3506,
	3506,
	3506,
	2862,
	2862,
	2718,
	3506,
	3506,
	3506,
	5460,
	3506,
	2565,
	2565,
	2565,
	2565,
	2565,
	2565,
	2035,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	2209,
	2815,
	2209,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	2837,
	2837,
	2839,
	3506,
	3506,
	3506,
	3506,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	2815,
	3506,
	3506,
	3506,
	2732,
	3506,
	3506,
	3506,
	3506,
	2815,
	805,
	38,
	2815,
	2837,
	2837,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	680,
	2797,
	2732,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	3506,
	1695,
	3506,
	3506,
	3506,
	3506,
	1692,
	3506,
	1220,
	2815,
	3506,
	3506,
	3506,
	3506,
	2815,
	2815,
	2815,
	3506,
	3506,
	3506,
	2815,
	3445,
	3506,
	3506,
	2775,
	3506,
	3473,
	3445,
	3506,
	3445,
	3506,
	3506,
	2815,
	3506,
	1695,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x0600034F, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)2, 98 },
};
extern const CustomAttributesCacheGenerator g_MoreMountains_CorgiEngine_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_MoreMountains_CorgiEngine_CodeGenModule;
const Il2CppCodeGenModule g_MoreMountains_CorgiEngine_CodeGenModule = 
{
	"MoreMountains.CorgiEngine.dll",
	2250,
	s_methodPointers,
	14,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
	g_MoreMountains_CorgiEngine_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

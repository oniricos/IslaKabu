﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color32)
extern void ParticleSystem_Emit_mC489C467AAF3C3721AC3315AF78DC4CE469E7AAC (void);
// 0x00000002 System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/Particle)
extern void ParticleSystem_Emit_mF1E108B9BF7E0094C35CF71870B5B0EA72ABB485 (void);
// 0x00000003 System.Boolean UnityEngine.ParticleSystem::get_isPlaying()
extern void ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA (void);
// 0x00000004 System.Void UnityEngine.ParticleSystem::Play(System.Boolean)
extern void ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A (void);
// 0x00000005 System.Void UnityEngine.ParticleSystem::Play()
extern void ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50 (void);
// 0x00000006 System.Void UnityEngine.ParticleSystem::Pause(System.Boolean)
extern void ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD (void);
// 0x00000007 System.Void UnityEngine.ParticleSystem::Pause()
extern void ParticleSystem_Pause_mA5AE4D5A290E9DD75A0572738CB0910D6A7E2121 (void);
// 0x00000008 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean,UnityEngine.ParticleSystemStopBehavior)
extern void ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A (void);
// 0x00000009 System.Void UnityEngine.ParticleSystem::Stop(System.Boolean)
extern void ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB (void);
// 0x0000000A System.Void UnityEngine.ParticleSystem::Stop()
extern void ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D (void);
// 0x0000000B System.Void UnityEngine.ParticleSystem::Clear(System.Boolean)
extern void ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057 (void);
// 0x0000000C System.Void UnityEngine.ParticleSystem::Clear()
extern void ParticleSystem_Clear_mD8C9DCD1267F221B0546E4B9B55DBD9354893797 (void);
// 0x0000000D System.Void UnityEngine.ParticleSystem::Emit(System.Int32)
extern void ParticleSystem_Emit_m07EF0D2DA84EB04814DA7EE6B8618B008DE75F28 (void);
// 0x0000000E System.Void UnityEngine.ParticleSystem::Emit_Internal(System.Int32)
extern void ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646 (void);
// 0x0000000F System.Void UnityEngine.ParticleSystem::Emit(UnityEngine.ParticleSystem/EmitParams,System.Int32)
extern void ParticleSystem_Emit_m1598252E2EF701A5010EFA395A87368495E9F9F7 (void);
// 0x00000010 System.Void UnityEngine.ParticleSystem::EmitOld_Internal(UnityEngine.ParticleSystem/Particle&)
extern void ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993 (void);
// 0x00000011 UnityEngine.ParticleSystem/MainModule UnityEngine.ParticleSystem::get_main()
extern void ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0 (void);
// 0x00000012 UnityEngine.ParticleSystem/EmissionModule UnityEngine.ParticleSystem::get_emission()
extern void ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1 (void);
// 0x00000013 System.Void UnityEngine.ParticleSystem::Emit_Injected(UnityEngine.ParticleSystem/EmitParams&,System.Int32)
extern void ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72 (void);
// 0x00000014 System.Void UnityEngine.ParticleSystem/MainModule::.ctor(UnityEngine.ParticleSystem)
extern void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76 (void);
// 0x00000015 System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop()
extern void MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8 (void);
// 0x00000016 UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MainModule::get_startDelay()
extern void MainModule_get_startDelay_m6CA464DDA126D5A848376094643AEB097BDF204C (void);
// 0x00000017 System.Void UnityEngine.ParticleSystem/MainModule::set_startDelay(UnityEngine.ParticleSystem/MinMaxCurve)
extern void MainModule_set_startDelay_m95CEAEE97E462DEAF4787B9C6AD6389D9912F93A (void);
// 0x00000018 System.Void UnityEngine.ParticleSystem/MainModule::set_startColor(UnityEngine.ParticleSystem/MinMaxGradient)
extern void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42 (void);
// 0x00000019 System.Boolean UnityEngine.ParticleSystem/MainModule::get_loop_Injected(UnityEngine.ParticleSystem/MainModule&)
extern void MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2 (void);
// 0x0000001A System.Void UnityEngine.ParticleSystem/MainModule::get_startDelay_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxCurve&)
extern void MainModule_get_startDelay_Injected_m0C120FBF2902673C91EC416DD78B37C6718179F7 (void);
// 0x0000001B System.Void UnityEngine.ParticleSystem/MainModule::set_startDelay_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxCurve&)
extern void MainModule_set_startDelay_Injected_mBC29B6AA6EE435D36BD5E9A69E073B7EEE0E4CAB (void);
// 0x0000001C System.Void UnityEngine.ParticleSystem/MainModule::set_startColor_Injected(UnityEngine.ParticleSystem/MainModule&,UnityEngine.ParticleSystem/MinMaxGradient&)
extern void MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6 (void);
// 0x0000001D System.Void UnityEngine.ParticleSystem/EmissionModule::.ctor(UnityEngine.ParticleSystem)
extern void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51 (void);
// 0x0000001E System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled(System.Boolean)
extern void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1 (void);
// 0x0000001F System.Void UnityEngine.ParticleSystem/EmissionModule::set_enabled_Injected(UnityEngine.ParticleSystem/EmissionModule&,System.Boolean)
extern void EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82 (void);
// 0x00000020 System.Void UnityEngine.ParticleSystem/Particle::set_lifetime(System.Single)
extern void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3 (void);
// 0x00000021 System.Void UnityEngine.ParticleSystem/Particle::set_position(UnityEngine.Vector3)
extern void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2 (void);
// 0x00000022 System.Void UnityEngine.ParticleSystem/Particle::set_velocity(UnityEngine.Vector3)
extern void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B (void);
// 0x00000023 System.Void UnityEngine.ParticleSystem/Particle::set_remainingLifetime(System.Single)
extern void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE (void);
// 0x00000024 System.Void UnityEngine.ParticleSystem/Particle::set_startLifetime(System.Single)
extern void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82 (void);
// 0x00000025 System.Void UnityEngine.ParticleSystem/Particle::set_startColor(UnityEngine.Color32)
extern void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554 (void);
// 0x00000026 System.Void UnityEngine.ParticleSystem/Particle::set_randomSeed(System.UInt32)
extern void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04 (void);
// 0x00000027 System.Void UnityEngine.ParticleSystem/Particle::set_startSize(System.Single)
extern void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466 (void);
// 0x00000028 System.Void UnityEngine.ParticleSystem/Particle::set_rotation3D(UnityEngine.Vector3)
extern void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0 (void);
// 0x00000029 System.Void UnityEngine.ParticleSystem/Particle::set_angularVelocity3D(UnityEngine.Vector3)
extern void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8 (void);
// 0x0000002A System.Void UnityEngine.ParticleSystem/MinMaxCurve::.ctor(System.Single)
extern void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3 (void);
// 0x0000002B System.Single UnityEngine.ParticleSystem/MinMaxCurve::get_constant()
extern void MinMaxCurve_get_constant_mBC0C29DF6F1C6C999931E28BC1F8DD26BD3BB624 (void);
// 0x0000002C UnityEngine.ParticleSystem/MinMaxCurve UnityEngine.ParticleSystem/MinMaxCurve::op_Implicit(System.Single)
extern void MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5 (void);
// 0x0000002D System.Void UnityEngine.ParticleSystem/MinMaxGradient::.ctor(UnityEngine.Color)
extern void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA (void);
// 0x0000002E UnityEngine.ParticleSystem/MinMaxGradient UnityEngine.ParticleSystem/MinMaxGradient::op_Implicit(UnityEngine.Color)
extern void MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA (void);
// 0x0000002F System.Void UnityEngine.ParticleSystemRenderer::set_maskInteraction(UnityEngine.SpriteMaskInteraction)
extern void ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB (void);
// 0x00000030 System.Int32 UnityEngine.ParticleSystemRenderer::GetMeshes(UnityEngine.Mesh[])
extern void ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75 (void);
static Il2CppMethodPointer s_methodPointers[48] = 
{
	ParticleSystem_Emit_mC489C467AAF3C3721AC3315AF78DC4CE469E7AAC,
	ParticleSystem_Emit_mF1E108B9BF7E0094C35CF71870B5B0EA72ABB485,
	ParticleSystem_get_isPlaying_m36FD03CBF99EE4C243B01F37D77CB6B1CFA526BA,
	ParticleSystem_Play_m97D70BAF373265E633ACD91833E56D981B19958A,
	ParticleSystem_Play_m28D27CC4CDC1D93195C75647E6F6DAECF8B6BC50,
	ParticleSystem_Pause_m116581497C92159D3210C733917CD2515399A0CD,
	ParticleSystem_Pause_mA5AE4D5A290E9DD75A0572738CB0910D6A7E2121,
	ParticleSystem_Stop_m275B200BC21580C60987EC8FC8B2DD0FEB365C1A,
	ParticleSystem_Stop_m5EC8B81A8BD1F8C90729E766789EE5D8D4EC64FB,
	ParticleSystem_Stop_m8CBF9268DE7B5A40952B4977462B857B5F5AFB9D,
	ParticleSystem_Clear_m9E873134B8055ACFEE9DC66F7E4FABE178E27057,
	ParticleSystem_Clear_mD8C9DCD1267F221B0546E4B9B55DBD9354893797,
	ParticleSystem_Emit_m07EF0D2DA84EB04814DA7EE6B8618B008DE75F28,
	ParticleSystem_Emit_Internal_m7C72C31F7F4875B54B00E255A450B045A4449646,
	ParticleSystem_Emit_m1598252E2EF701A5010EFA395A87368495E9F9F7,
	ParticleSystem_EmitOld_Internal_m4F094DC523986298D5626F0F3F2335DFF596C993,
	ParticleSystem_get_main_m8F17DCC63679B15CE548BE83332FDB6635AE74A0,
	ParticleSystem_get_emission_m0BAA1EDF775A8712DA7D62BF3C42A2B1A6C96CA1,
	ParticleSystem_Emit_Injected_mC31E36D577A2D3135436438BFC27B6C76D9ADC72,
	MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76,
	MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8,
	MainModule_get_startDelay_m6CA464DDA126D5A848376094643AEB097BDF204C,
	MainModule_set_startDelay_m95CEAEE97E462DEAF4787B9C6AD6389D9912F93A,
	MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42,
	MainModule_get_loop_Injected_mD8FD195A3CF5008FD319636A3945DE2F4F0EEBD2,
	MainModule_get_startDelay_Injected_m0C120FBF2902673C91EC416DD78B37C6718179F7,
	MainModule_set_startDelay_Injected_mBC29B6AA6EE435D36BD5E9A69E073B7EEE0E4CAB,
	MainModule_set_startColor_Injected_mBDC348394A0F1920C4123AF0329DECB2869D6EE6,
	EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51,
	EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1,
	EmissionModule_set_enabled_Injected_m35208A8279D12A78D5DBCF751BF1760869051C82,
	Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3,
	Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2,
	Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B,
	Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE,
	Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82,
	Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554,
	Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04,
	Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466,
	Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0,
	Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8,
	MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3,
	MinMaxCurve_get_constant_mBC0C29DF6F1C6C999931E28BC1F8DD26BD3BB624,
	MinMaxCurve_op_Implicit_m8D746D40E6CCBF5E8C4CE39F23A45712BFC372F5,
	MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA,
	MinMaxGradient_op_Implicit_m5F938B5644AF6BE5DF525839E922B88456B614FA,
	ParticleSystemRenderer_set_maskInteraction_mFC9DD6C5CDADD003946F4CDA508D5A814992F2BB,
	ParticleSystemRenderer_GetMeshes_m1B36A6BFF152AAE5520D727976E3DA26722C3A75,
};
extern void MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76_AdjustorThunk (void);
extern void MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8_AdjustorThunk (void);
extern void MainModule_get_startDelay_m6CA464DDA126D5A848376094643AEB097BDF204C_AdjustorThunk (void);
extern void MainModule_set_startDelay_m95CEAEE97E462DEAF4787B9C6AD6389D9912F93A_AdjustorThunk (void);
extern void MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42_AdjustorThunk (void);
extern void EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51_AdjustorThunk (void);
extern void EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1_AdjustorThunk (void);
extern void Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3_AdjustorThunk (void);
extern void Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2_AdjustorThunk (void);
extern void Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B_AdjustorThunk (void);
extern void Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE_AdjustorThunk (void);
extern void Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82_AdjustorThunk (void);
extern void Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554_AdjustorThunk (void);
extern void Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04_AdjustorThunk (void);
extern void Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466_AdjustorThunk (void);
extern void Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0_AdjustorThunk (void);
extern void Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8_AdjustorThunk (void);
extern void MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3_AdjustorThunk (void);
extern void MinMaxCurve_get_constant_mBC0C29DF6F1C6C999931E28BC1F8DD26BD3BB624_AdjustorThunk (void);
extern void MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[20] = 
{
	{ 0x06000014, MainModule__ctor_m34F626B568C6D3C80A036409049761C8316E6A76_AdjustorThunk },
	{ 0x06000015, MainModule_get_loop_mE45A1A4A1DF6E2590B7B13A73A9A95197DA346F8_AdjustorThunk },
	{ 0x06000016, MainModule_get_startDelay_m6CA464DDA126D5A848376094643AEB097BDF204C_AdjustorThunk },
	{ 0x06000017, MainModule_set_startDelay_m95CEAEE97E462DEAF4787B9C6AD6389D9912F93A_AdjustorThunk },
	{ 0x06000018, MainModule_set_startColor_m19663CAE16C6A546B7BC4B949EBA0CCE3DD51A42_AdjustorThunk },
	{ 0x0600001D, EmissionModule__ctor_m9DFD2A0BB8BFAD2191C54E6627FC3B0E641EDD51_AdjustorThunk },
	{ 0x0600001E, EmissionModule_set_enabled_mD9FD50C9F5EECD04B22D03E5C00C7DD64D2AC1F1_AdjustorThunk },
	{ 0x06000020, Particle_set_lifetime_mCE97F9D17D1E660DB9D9F3244150CB6624A74DC3_AdjustorThunk },
	{ 0x06000021, Particle_set_position_mB749E41CD3C4C6DF501C0D68B6211CF7E2217FC2_AdjustorThunk },
	{ 0x06000022, Particle_set_velocity_m4894BD6A75E7A1FCD433927F93396AA29A59709B_AdjustorThunk },
	{ 0x06000023, Particle_set_remainingLifetime_mE521DF9387892C00B3F21D4F76F3A55E77AFC6BE_AdjustorThunk },
	{ 0x06000024, Particle_set_startLifetime_m7AC09262BBFE8818EA6B61BDB62264184CD27A82_AdjustorThunk },
	{ 0x06000025, Particle_set_startColor_mA7B0363E82B7A7CBB1C83F8C5D49FB7B7AF75554_AdjustorThunk },
	{ 0x06000026, Particle_set_randomSeed_m8FD7A4DB7F8E7EBDEF2C51A28197F8D9D7CB6E04_AdjustorThunk },
	{ 0x06000027, Particle_set_startSize_m7CDEE5B620B3D26B4CC5C1DA7C6E24ACCCF64466_AdjustorThunk },
	{ 0x06000028, Particle_set_rotation3D_m0F402760524A81307FA4940751751C44DF2F77D0_AdjustorThunk },
	{ 0x06000029, Particle_set_angularVelocity3D_mE9A9544DF33CD0CCF4F1CA14994A2C436E4DF1F8_AdjustorThunk },
	{ 0x0600002A, MinMaxCurve__ctor_mF14A2DBABFDCF1E855911241A555949CAF212AE3_AdjustorThunk },
	{ 0x0600002B, MinMaxCurve_get_constant_mBC0C29DF6F1C6C999931E28BC1F8DD26BD3BB624_AdjustorThunk },
	{ 0x0600002D, MinMaxGradient__ctor_m8D0DE766F67FF065E4C2B6DCD841752C447977AA_AdjustorThunk },
};
static const int32_t s_InvokerIndices[48] = 
{
	336,
	2936,
	3473,
	2837,
	3506,
	2837,
	3506,
	1717,
	2837,
	3506,
	2837,
	3506,
	2775,
	2775,
	1754,
	2714,
	3587,
	3586,
	1410,
	2815,
	3473,
	3588,
	2934,
	2935,
	5239,
	4913,
	4913,
	4913,
	2815,
	2837,
	4919,
	2839,
	2862,
	2862,
	2839,
	2839,
	2724,
	2775,
	2839,
	2862,
	2862,
	2839,
	3476,
	5379,
	2723,
	5380,
	2775,
	2035,
};
extern const CustomAttributesCacheGenerator g_UnityEngine_ParticleSystemModule_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModule_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_ParticleSystemModule_CodeGenModule = 
{
	"UnityEngine.ParticleSystemModule.dll",
	48,
	s_methodPointers,
	20,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UnityEngine_ParticleSystemModule_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

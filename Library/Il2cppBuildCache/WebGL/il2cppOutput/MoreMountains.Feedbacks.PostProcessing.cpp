﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R, typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtFuncInvoker5
{
	typedef R (*Func)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3, typename T4>
struct VirtFuncInvoker4
{
	typedef R (*Func)(void*, T1, T2, T3, T4, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, p4, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
struct VirtActionInvoker14
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
struct VirtActionInvoker15
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct VirtActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
struct VirtActionInvoker12
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20>
struct VirtActionInvoker20
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21>
struct VirtActionInvoker21
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20, T21 p21)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
struct VirtActionInvoker17
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18>
struct VirtActionInvoker18
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
struct GenericVirtActionInvoker14
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
struct GenericVirtActionInvoker15
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct GenericVirtActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
struct GenericVirtActionInvoker12
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20>
struct GenericVirtActionInvoker20
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21>
struct GenericVirtActionInvoker21
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20, T21 p21)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
struct GenericVirtActionInvoker17
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18>
struct GenericVirtActionInvoker18
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
struct InterfaceActionInvoker14
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
struct InterfaceActionInvoker15
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct InterfaceActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
struct InterfaceActionInvoker12
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20>
struct InterfaceActionInvoker20
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21>
struct InterfaceActionInvoker21
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20, T21 p21)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
struct InterfaceActionInvoker17
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18>
struct InterfaceActionInvoker18
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14>
struct GenericInterfaceActionInvoker14
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15>
struct GenericInterfaceActionInvoker15
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11>
struct GenericInterfaceActionInvoker11
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12>
struct GenericInterfaceActionInvoker12
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20>
struct GenericInterfaceActionInvoker20
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18, typename T19, typename T20, typename T21>
struct GenericInterfaceActionInvoker21
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, T19, T20, T21, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18, T19 p19, T20 p20, T21 p21)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17>
struct GenericInterfaceActionInvoker17
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6, typename T7, typename T8, typename T9, typename T10, typename T11, typename T12, typename T13, typename T14, typename T15, typename T16, typename T17, typename T18>
struct GenericInterfaceActionInvoker18
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16, T17, T18, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7, T8 p8, T9 p9, T10 p10, T11 p11, T12 p12, T13 p13, T14 p14, T15 p15, T16 p16, T17 p17, T18 p18)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.Collider>
struct List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B;
// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings>
struct List_1_tA5E70279ED9F9FF2F7A6E8C0814C9D3DADD1228B;
// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>
struct ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride>
struct ReadOnlyCollection_1_tB4110F279A79A84C732E919515BBB6EEC1128F83;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// UnityEngine.Rendering.PostProcessing.Bloom
struct Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8;
// UnityEngine.Rendering.PostProcessing.BoolParameter
struct BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80;
// UnityEngine.Rendering.PostProcessing.ChromaticAberration
struct ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229;
// UnityEngine.Rendering.PostProcessing.ColorGrading
struct ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5;
// UnityEngine.Rendering.PostProcessing.ColorParameter
struct ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.Rendering.PostProcessing.DepthOfField
struct DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731;
// UnityEngine.Rendering.PostProcessing.FloatParameter
struct FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.Rendering.PostProcessing.GradingModeParameter
struct GradingModeParameter_tA53C77634D098FA15A6CDFBEF2C5EBB3BBF97913;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.Rendering.PostProcessing.KernelSizeParameter
struct KernelSizeParameter_tD40A9C16004C0686D34035CCC8E1F6E2D8A26D89;
// UnityEngine.Rendering.PostProcessing.LensDistortion
struct LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2;
// MoreMountains.FeedbacksForThirdParty.MMAutoFocus
struct MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA;
// MoreMountains.FeedbacksForThirdParty.MMBloomShaker
struct MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE;
// MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker
struct MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6;
// MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker
struct MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9;
// MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker
struct MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645;
// MoreMountains.Feedbacks.MMFeedback
struct MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom
struct MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration
struct MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading
struct MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField
struct MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend
struct MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion
struct MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7;
// MoreMountains.Feedbacks.MMFeedbackTiming
struct MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6;
// MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette
struct MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1;
// MoreMountains.Feedbacks.MMFeedbacks
struct MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914;
// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend
struct MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744;
// MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker
struct MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1;
// MoreMountains.Feedbacks.MMSequence
struct MMSequence_t1E28CC4D7E62BA36AF7F110B2033372FBB54F88A;
// MoreMountains.Feedbacks.MMShaker
struct MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1;
// MoreMountains.FeedbacksForThirdParty.MMVignetteShaker
struct MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Rendering.PostProcessing.PostProcessProfile
struct PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E;
// UnityEngine.Rendering.PostProcessing.PostProcessVolume
struct PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF;
// UnityEngine.Rendering.PostProcessing.SplineParameter
struct SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339;
// System.String
struct String_t;
// UnityEngine.Rendering.PostProcessing.TextureParameter
struct TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2;
// UnityEngine.Rendering.PostProcessing.TonemapperParameter
struct TonemapperParameter_t135C78E99EB670872D17A05A7CBF0F4E3F8A222B;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Rendering.PostProcessing.Vector2Parameter
struct Vector2Parameter_t0E1CE8C27130B36A75688B706DE0EACCE9DE41A3;
// UnityEngine.Rendering.PostProcessing.Vector4Parameter
struct Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627;
// UnityEngine.Rendering.PostProcessing.Vignette
struct Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139;
// UnityEngine.Rendering.PostProcessing.VignetteModeParameter
struct VignetteModeParameter_tA57BF7F5798A90AE3DFE38CF80A85E931F3DD613;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate
struct Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837;
// MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate
struct Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81;
// MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate
struct Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27;
// MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate
struct Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C;
// MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate
struct Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95;
// MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate
struct Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD;

IL2CPP_EXTERN_C RuntimeClass* AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral85AF6E8AEABE7BAAAA9895F7C68A5F1AF46920DC;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_mC2BDCEAA8F3C5E4523A7F58F4F03D9AA3C1399FE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PostProcessProfile_TryGetSettings_TisBloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8_mCDEF10AC8C4B9996079DCEC638986263A42D479E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PostProcessProfile_TryGetSettings_TisChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229_m99B57DE78E9EEE2FA3EFB8BB50911DA28CB6F25F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PostProcessProfile_TryGetSettings_TisColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5_mCD75893765EDF7E8B5D3381AFBCDDA86EA602F97_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PostProcessProfile_TryGetSettings_TisLensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2_mB931D1A60AFCD5DEE42FD9B8430B4B74629BBF2A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* PostProcessProfile_TryGetSettings_TisVignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139_m2FD6F6813492CA2C85F67666F4B699C733783BD8_RuntimeMethod_var;
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;;
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke;
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke;;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC;
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tA85BC73AF8EF16EDCBDF8472BD7679ACF7EF8953 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// UnityEngine.Rendering.PostProcessing.ParameterOverride
struct ParameterOverride_tED887E59B87A5DD3BFDAAAF745F82D9080FA4784  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.ParameterOverride::overrideState
	bool ___overrideState_0;

public:
	inline static int32_t get_offset_of_overrideState_0() { return static_cast<int32_t>(offsetof(ParameterOverride_tED887E59B87A5DD3BFDAAAF745F82D9080FA4784, ___overrideState_0)); }
	inline bool get_overrideState_0() const { return ___overrideState_0; }
	inline bool* get_address_of_overrideState_0() { return &___overrideState_0; }
	inline void set_overrideState_0(bool value)
	{
		___overrideState_0 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>
struct ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1  : public ParameterOverride_tED887E59B87A5DD3BFDAAAF745F82D9080FA4784
{
public:
	// T UnityEngine.Rendering.PostProcessing.ParameterOverride`1::value
	float ___value_1;

public:
	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1, ___value_1)); }
	inline float get_value_1() const { return ___value_1; }
	inline float* get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(float value)
	{
		___value_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Keyframe
struct Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F 
{
public:
	// System.Single UnityEngine.Keyframe::m_Time
	float ___m_Time_0;
	// System.Single UnityEngine.Keyframe::m_Value
	float ___m_Value_1;
	// System.Single UnityEngine.Keyframe::m_InTangent
	float ___m_InTangent_2;
	// System.Single UnityEngine.Keyframe::m_OutTangent
	float ___m_OutTangent_3;
	// System.Int32 UnityEngine.Keyframe::m_WeightedMode
	int32_t ___m_WeightedMode_4;
	// System.Single UnityEngine.Keyframe::m_InWeight
	float ___m_InWeight_5;
	// System.Single UnityEngine.Keyframe::m_OutWeight
	float ___m_OutWeight_6;

public:
	inline static int32_t get_offset_of_m_Time_0() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Time_0)); }
	inline float get_m_Time_0() const { return ___m_Time_0; }
	inline float* get_address_of_m_Time_0() { return &___m_Time_0; }
	inline void set_m_Time_0(float value)
	{
		___m_Time_0 = value;
	}

	inline static int32_t get_offset_of_m_Value_1() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_Value_1)); }
	inline float get_m_Value_1() const { return ___m_Value_1; }
	inline float* get_address_of_m_Value_1() { return &___m_Value_1; }
	inline void set_m_Value_1(float value)
	{
		___m_Value_1 = value;
	}

	inline static int32_t get_offset_of_m_InTangent_2() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InTangent_2)); }
	inline float get_m_InTangent_2() const { return ___m_InTangent_2; }
	inline float* get_address_of_m_InTangent_2() { return &___m_InTangent_2; }
	inline void set_m_InTangent_2(float value)
	{
		___m_InTangent_2 = value;
	}

	inline static int32_t get_offset_of_m_OutTangent_3() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutTangent_3)); }
	inline float get_m_OutTangent_3() const { return ___m_OutTangent_3; }
	inline float* get_address_of_m_OutTangent_3() { return &___m_OutTangent_3; }
	inline void set_m_OutTangent_3(float value)
	{
		___m_OutTangent_3 = value;
	}

	inline static int32_t get_offset_of_m_WeightedMode_4() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_WeightedMode_4)); }
	inline int32_t get_m_WeightedMode_4() const { return ___m_WeightedMode_4; }
	inline int32_t* get_address_of_m_WeightedMode_4() { return &___m_WeightedMode_4; }
	inline void set_m_WeightedMode_4(int32_t value)
	{
		___m_WeightedMode_4 = value;
	}

	inline static int32_t get_offset_of_m_InWeight_5() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_InWeight_5)); }
	inline float get_m_InWeight_5() const { return ___m_InWeight_5; }
	inline float* get_address_of_m_InWeight_5() { return &___m_InWeight_5; }
	inline void set_m_InWeight_5(float value)
	{
		___m_InWeight_5 = value;
	}

	inline static int32_t get_offset_of_m_OutWeight_6() { return static_cast<int32_t>(offsetof(Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F, ___m_OutWeight_6)); }
	inline float get_m_OutWeight_6() const { return ___m_OutWeight_6; }
	inline float* get_address_of_m_OutWeight_6() { return &___m_OutWeight_6; }
	inline void set_m_OutWeight_6(float value)
	{
		___m_OutWeight_6 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent
struct MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED__padding[1];
	};

public:
};

struct MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::OnEvent
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___OnEvent_0;

public:
	inline static int32_t get_offset_of_OnEvent_0() { return static_cast<int32_t>(offsetof(MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields, ___OnEvent_0)); }
	inline Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * get_OnEvent_0() const { return ___OnEvent_0; }
	inline Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 ** get_address_of_OnEvent_0() { return &___OnEvent_0; }
	inline void set_OnEvent_0(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * value)
	{
		___OnEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEvent_0), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent
struct MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD__padding[1];
	};

public:
};

struct MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::OnEvent
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___OnEvent_0;

public:
	inline static int32_t get_offset_of_OnEvent_0() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields, ___OnEvent_0)); }
	inline Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * get_OnEvent_0() const { return ___OnEvent_0; }
	inline Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 ** get_address_of_OnEvent_0() { return &___OnEvent_0; }
	inline void set_OnEvent_0(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * value)
	{
		___OnEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEvent_0), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent
struct MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5__padding[1];
	};

public:
};

struct MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::OnEvent
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___OnEvent_0;

public:
	inline static int32_t get_offset_of_OnEvent_0() { return static_cast<int32_t>(offsetof(MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields, ___OnEvent_0)); }
	inline Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * get_OnEvent_0() const { return ___OnEvent_0; }
	inline Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 ** get_address_of_OnEvent_0() { return &___OnEvent_0; }
	inline void set_OnEvent_0(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * value)
	{
		___OnEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEvent_0), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent
struct MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB__padding[1];
	};

public:
};

struct MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::OnEvent
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___OnEvent_0;

public:
	inline static int32_t get_offset_of_OnEvent_0() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields, ___OnEvent_0)); }
	inline Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * get_OnEvent_0() const { return ___OnEvent_0; }
	inline Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C ** get_address_of_OnEvent_0() { return &___OnEvent_0; }
	inline void set_OnEvent_0(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * value)
	{
		___OnEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEvent_0), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent
struct MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266__padding[1];
	};

public:
};

struct MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::OnEvent
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___OnEvent_0;

public:
	inline static int32_t get_offset_of_OnEvent_0() { return static_cast<int32_t>(offsetof(MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields, ___OnEvent_0)); }
	inline Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * get_OnEvent_0() const { return ___OnEvent_0; }
	inline Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 ** get_address_of_OnEvent_0() { return &___OnEvent_0; }
	inline void set_OnEvent_0(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * value)
	{
		___OnEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEvent_0), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent
struct MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A__padding[1];
	};

public:
};

struct MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::OnEvent
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___OnEvent_0;

public:
	inline static int32_t get_offset_of_OnEvent_0() { return static_cast<int32_t>(offsetof(MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields, ___OnEvent_0)); }
	inline Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * get_OnEvent_0() const { return ___OnEvent_0; }
	inline Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD ** get_address_of_OnEvent_0() { return &___OnEvent_0; }
	inline void set_OnEvent_0(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * value)
	{
		___OnEvent_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnEvent_0), (void*)value);
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.AnimationCurve::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Rendering.PostProcessing.FloatParameter
struct FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0  : public ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1
{
public:

public:
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// MoreMountains.Feedbacks.TimescaleModes
struct TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412 
{
public:
	// System.Int32 MoreMountains.Feedbacks.TimescaleModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend/Actions
struct Actions_t18DB1D8607ACB48615F4E195026ED68890BB33FE 
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend/Actions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Actions_t18DB1D8607ACB48615F4E195026ED68890BB33FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend/Modes
struct Modes_t6BA74809D65080F727FDEEBC523B1B45F165429A 
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend/Modes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Modes_t6BA74809D65080F727FDEEBC523B1B45F165429A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbackTiming/MMFeedbacksDirectionConditions
struct MMFeedbacksDirectionConditions_t34D4A1D0D8FB230B7400FFE3E172A76F8E27413A 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbackTiming/MMFeedbacksDirectionConditions::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MMFeedbacksDirectionConditions_t34D4A1D0D8FB230B7400FFE3E172A76F8E27413A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Feedbacks.MMFeedbackTiming/PlayDirections
struct PlayDirections_tAFB208E21C6EBC665CE57841DF3E8457BF56A5B7 
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMFeedbackTiming/PlayDirections::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayDirections_tAFB208E21C6EBC665CE57841DF3E8457BF56A5B7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend/BlendTriggerModes
struct BlendTriggerModes_t7C540BE4C9581EDF44D517CE703611B37C0B4BC0 
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend/BlendTriggerModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendTriggerModes_t7C540BE4C9581EDF44D517CE703611B37C0B4BC0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend/TimeScales
struct TimeScales_tAE85293A3268BEA2DCDEAC12D304FB384368265C 
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend/TimeScales::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimeScales_tAE85293A3268BEA2DCDEAC12D304FB384368265C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// MoreMountains.Feedbacks.MMFeedbackTiming
struct MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6  : public RuntimeObject
{
public:
	// MoreMountains.Feedbacks.TimescaleModes MoreMountains.Feedbacks.MMFeedbackTiming::TimescaleMode
	int32_t ___TimescaleMode_0;
	// System.Single MoreMountains.Feedbacks.MMFeedbackTiming::InitialDelay
	float ___InitialDelay_1;
	// System.Single MoreMountains.Feedbacks.MMFeedbackTiming::CooldownDuration
	float ___CooldownDuration_2;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbackTiming::ExcludeFromHoldingPauses
	bool ___ExcludeFromHoldingPauses_3;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbackTiming::InterruptsOnStop
	bool ___InterruptsOnStop_4;
	// System.Int32 MoreMountains.Feedbacks.MMFeedbackTiming::NumberOfRepeats
	int32_t ___NumberOfRepeats_5;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbackTiming::RepeatForever
	bool ___RepeatForever_6;
	// System.Single MoreMountains.Feedbacks.MMFeedbackTiming::DelayBetweenRepeats
	float ___DelayBetweenRepeats_7;
	// MoreMountains.Feedbacks.MMFeedbackTiming/MMFeedbacksDirectionConditions MoreMountains.Feedbacks.MMFeedbackTiming::MMFeedbacksDirectionCondition
	int32_t ___MMFeedbacksDirectionCondition_8;
	// MoreMountains.Feedbacks.MMFeedbackTiming/PlayDirections MoreMountains.Feedbacks.MMFeedbackTiming::PlayDirection
	int32_t ___PlayDirection_9;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbackTiming::ConstantIntensity
	bool ___ConstantIntensity_10;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbackTiming::UseIntensityInterval
	bool ___UseIntensityInterval_11;
	// System.Single MoreMountains.Feedbacks.MMFeedbackTiming::IntensityIntervalMin
	float ___IntensityIntervalMin_12;
	// System.Single MoreMountains.Feedbacks.MMFeedbackTiming::IntensityIntervalMax
	float ___IntensityIntervalMax_13;
	// MoreMountains.Feedbacks.MMSequence MoreMountains.Feedbacks.MMFeedbackTiming::Sequence
	MMSequence_t1E28CC4D7E62BA36AF7F110B2033372FBB54F88A * ___Sequence_14;
	// System.Int32 MoreMountains.Feedbacks.MMFeedbackTiming::TrackID
	int32_t ___TrackID_15;
	// System.Boolean MoreMountains.Feedbacks.MMFeedbackTiming::Quantized
	bool ___Quantized_16;
	// System.Int32 MoreMountains.Feedbacks.MMFeedbackTiming::TargetBPM
	int32_t ___TargetBPM_17;

public:
	inline static int32_t get_offset_of_TimescaleMode_0() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___TimescaleMode_0)); }
	inline int32_t get_TimescaleMode_0() const { return ___TimescaleMode_0; }
	inline int32_t* get_address_of_TimescaleMode_0() { return &___TimescaleMode_0; }
	inline void set_TimescaleMode_0(int32_t value)
	{
		___TimescaleMode_0 = value;
	}

	inline static int32_t get_offset_of_InitialDelay_1() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___InitialDelay_1)); }
	inline float get_InitialDelay_1() const { return ___InitialDelay_1; }
	inline float* get_address_of_InitialDelay_1() { return &___InitialDelay_1; }
	inline void set_InitialDelay_1(float value)
	{
		___InitialDelay_1 = value;
	}

	inline static int32_t get_offset_of_CooldownDuration_2() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___CooldownDuration_2)); }
	inline float get_CooldownDuration_2() const { return ___CooldownDuration_2; }
	inline float* get_address_of_CooldownDuration_2() { return &___CooldownDuration_2; }
	inline void set_CooldownDuration_2(float value)
	{
		___CooldownDuration_2 = value;
	}

	inline static int32_t get_offset_of_ExcludeFromHoldingPauses_3() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___ExcludeFromHoldingPauses_3)); }
	inline bool get_ExcludeFromHoldingPauses_3() const { return ___ExcludeFromHoldingPauses_3; }
	inline bool* get_address_of_ExcludeFromHoldingPauses_3() { return &___ExcludeFromHoldingPauses_3; }
	inline void set_ExcludeFromHoldingPauses_3(bool value)
	{
		___ExcludeFromHoldingPauses_3 = value;
	}

	inline static int32_t get_offset_of_InterruptsOnStop_4() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___InterruptsOnStop_4)); }
	inline bool get_InterruptsOnStop_4() const { return ___InterruptsOnStop_4; }
	inline bool* get_address_of_InterruptsOnStop_4() { return &___InterruptsOnStop_4; }
	inline void set_InterruptsOnStop_4(bool value)
	{
		___InterruptsOnStop_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfRepeats_5() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___NumberOfRepeats_5)); }
	inline int32_t get_NumberOfRepeats_5() const { return ___NumberOfRepeats_5; }
	inline int32_t* get_address_of_NumberOfRepeats_5() { return &___NumberOfRepeats_5; }
	inline void set_NumberOfRepeats_5(int32_t value)
	{
		___NumberOfRepeats_5 = value;
	}

	inline static int32_t get_offset_of_RepeatForever_6() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___RepeatForever_6)); }
	inline bool get_RepeatForever_6() const { return ___RepeatForever_6; }
	inline bool* get_address_of_RepeatForever_6() { return &___RepeatForever_6; }
	inline void set_RepeatForever_6(bool value)
	{
		___RepeatForever_6 = value;
	}

	inline static int32_t get_offset_of_DelayBetweenRepeats_7() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___DelayBetweenRepeats_7)); }
	inline float get_DelayBetweenRepeats_7() const { return ___DelayBetweenRepeats_7; }
	inline float* get_address_of_DelayBetweenRepeats_7() { return &___DelayBetweenRepeats_7; }
	inline void set_DelayBetweenRepeats_7(float value)
	{
		___DelayBetweenRepeats_7 = value;
	}

	inline static int32_t get_offset_of_MMFeedbacksDirectionCondition_8() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___MMFeedbacksDirectionCondition_8)); }
	inline int32_t get_MMFeedbacksDirectionCondition_8() const { return ___MMFeedbacksDirectionCondition_8; }
	inline int32_t* get_address_of_MMFeedbacksDirectionCondition_8() { return &___MMFeedbacksDirectionCondition_8; }
	inline void set_MMFeedbacksDirectionCondition_8(int32_t value)
	{
		___MMFeedbacksDirectionCondition_8 = value;
	}

	inline static int32_t get_offset_of_PlayDirection_9() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___PlayDirection_9)); }
	inline int32_t get_PlayDirection_9() const { return ___PlayDirection_9; }
	inline int32_t* get_address_of_PlayDirection_9() { return &___PlayDirection_9; }
	inline void set_PlayDirection_9(int32_t value)
	{
		___PlayDirection_9 = value;
	}

	inline static int32_t get_offset_of_ConstantIntensity_10() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___ConstantIntensity_10)); }
	inline bool get_ConstantIntensity_10() const { return ___ConstantIntensity_10; }
	inline bool* get_address_of_ConstantIntensity_10() { return &___ConstantIntensity_10; }
	inline void set_ConstantIntensity_10(bool value)
	{
		___ConstantIntensity_10 = value;
	}

	inline static int32_t get_offset_of_UseIntensityInterval_11() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___UseIntensityInterval_11)); }
	inline bool get_UseIntensityInterval_11() const { return ___UseIntensityInterval_11; }
	inline bool* get_address_of_UseIntensityInterval_11() { return &___UseIntensityInterval_11; }
	inline void set_UseIntensityInterval_11(bool value)
	{
		___UseIntensityInterval_11 = value;
	}

	inline static int32_t get_offset_of_IntensityIntervalMin_12() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___IntensityIntervalMin_12)); }
	inline float get_IntensityIntervalMin_12() const { return ___IntensityIntervalMin_12; }
	inline float* get_address_of_IntensityIntervalMin_12() { return &___IntensityIntervalMin_12; }
	inline void set_IntensityIntervalMin_12(float value)
	{
		___IntensityIntervalMin_12 = value;
	}

	inline static int32_t get_offset_of_IntensityIntervalMax_13() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___IntensityIntervalMax_13)); }
	inline float get_IntensityIntervalMax_13() const { return ___IntensityIntervalMax_13; }
	inline float* get_address_of_IntensityIntervalMax_13() { return &___IntensityIntervalMax_13; }
	inline void set_IntensityIntervalMax_13(float value)
	{
		___IntensityIntervalMax_13 = value;
	}

	inline static int32_t get_offset_of_Sequence_14() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___Sequence_14)); }
	inline MMSequence_t1E28CC4D7E62BA36AF7F110B2033372FBB54F88A * get_Sequence_14() const { return ___Sequence_14; }
	inline MMSequence_t1E28CC4D7E62BA36AF7F110B2033372FBB54F88A ** get_address_of_Sequence_14() { return &___Sequence_14; }
	inline void set_Sequence_14(MMSequence_t1E28CC4D7E62BA36AF7F110B2033372FBB54F88A * value)
	{
		___Sequence_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Sequence_14), (void*)value);
	}

	inline static int32_t get_offset_of_TrackID_15() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___TrackID_15)); }
	inline int32_t get_TrackID_15() const { return ___TrackID_15; }
	inline int32_t* get_address_of_TrackID_15() { return &___TrackID_15; }
	inline void set_TrackID_15(int32_t value)
	{
		___TrackID_15 = value;
	}

	inline static int32_t get_offset_of_Quantized_16() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___Quantized_16)); }
	inline bool get_Quantized_16() const { return ___Quantized_16; }
	inline bool* get_address_of_Quantized_16() { return &___Quantized_16; }
	inline void set_Quantized_16(bool value)
	{
		___Quantized_16 = value;
	}

	inline static int32_t get_offset_of_TargetBPM_17() { return static_cast<int32_t>(offsetof(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6, ___TargetBPM_17)); }
	inline int32_t get_TargetBPM_17() const { return ___TargetBPM_17; }
	inline int32_t* get_address_of_TargetBPM_17() { return &___TargetBPM_17; }
	inline void set_TargetBPM_17(int32_t value)
	{
		___TargetBPM_17 = value;
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings
struct PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::active
	bool ___active_4;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::enabled
	BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * ___enabled_5;
	// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.PostProcessing.ParameterOverride> UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings::parameters
	ReadOnlyCollection_1_tB4110F279A79A84C732E919515BBB6EEC1128F83 * ___parameters_6;

public:
	inline static int32_t get_offset_of_active_4() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0, ___active_4)); }
	inline bool get_active_4() const { return ___active_4; }
	inline bool* get_address_of_active_4() { return &___active_4; }
	inline void set_active_4(bool value)
	{
		___active_4 = value;
	}

	inline static int32_t get_offset_of_enabled_5() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0, ___enabled_5)); }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * get_enabled_5() const { return ___enabled_5; }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 ** get_address_of_enabled_5() { return &___enabled_5; }
	inline void set_enabled_5(BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * value)
	{
		___enabled_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enabled_5), (void*)value);
	}

	inline static int32_t get_offset_of_parameters_6() { return static_cast<int32_t>(offsetof(PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0, ___parameters_6)); }
	inline ReadOnlyCollection_1_tB4110F279A79A84C732E919515BBB6EEC1128F83 * get_parameters_6() const { return ___parameters_6; }
	inline ReadOnlyCollection_1_tB4110F279A79A84C732E919515BBB6EEC1128F83 ** get_address_of_parameters_6() { return &___parameters_6; }
	inline void set_parameters_6(ReadOnlyCollection_1_tB4110F279A79A84C732E919515BBB6EEC1128F83 * value)
	{
		___parameters_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parameters_6), (void*)value);
	}
};


// UnityEngine.Rendering.PostProcessing.PostProcessProfile
struct PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Rendering.PostProcessing.PostProcessEffectSettings> UnityEngine.Rendering.PostProcessing.PostProcessProfile::settings
	List_1_tA5E70279ED9F9FF2F7A6E8C0814C9D3DADD1228B * ___settings_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::isDirty
	bool ___isDirty_5;

public:
	inline static int32_t get_offset_of_settings_4() { return static_cast<int32_t>(offsetof(PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E, ___settings_4)); }
	inline List_1_tA5E70279ED9F9FF2F7A6E8C0814C9D3DADD1228B * get_settings_4() const { return ___settings_4; }
	inline List_1_tA5E70279ED9F9FF2F7A6E8C0814C9D3DADD1228B ** get_address_of_settings_4() { return &___settings_4; }
	inline void set_settings_4(List_1_tA5E70279ED9F9FF2F7A6E8C0814C9D3DADD1228B * value)
	{
		___settings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___settings_4), (void*)value);
	}

	inline static int32_t get_offset_of_isDirty_5() { return static_cast<int32_t>(offsetof(PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E, ___isDirty_5)); }
	inline bool get_isDirty_5() const { return ___isDirty_5; }
	inline bool* get_address_of_isDirty_5() { return &___isDirty_5; }
	inline void set_isDirty_5(bool value)
	{
		___isDirty_5 = value;
	}
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate
struct Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837  : public MulticastDelegate_t
{
public:

public:
};


// MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate
struct Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81  : public MulticastDelegate_t
{
public:

public:
};


// MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate
struct Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27  : public MulticastDelegate_t
{
public:

public:
};


// MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate
struct Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C  : public MulticastDelegate_t
{
public:

public:
};


// MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate
struct Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95  : public MulticastDelegate_t
{
public:

public:
};


// MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate
struct Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Rendering.PostProcessing.Bloom
struct Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8  : public PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::intensity
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___intensity_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::threshold
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___threshold_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::softKnee
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___softKnee_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::clamp
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___clamp_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::diffusion
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___diffusion_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::anamorphicRatio
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___anamorphicRatio_12;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.Bloom::color
	ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * ___color_13;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Bloom::fastMode
	BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * ___fastMode_14;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.Bloom::dirtTexture
	TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * ___dirtTexture_15;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Bloom::dirtIntensity
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___dirtIntensity_16;

public:
	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___intensity_7)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_intensity_7() const { return ___intensity_7; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___intensity_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intensity_7), (void*)value);
	}

	inline static int32_t get_offset_of_threshold_8() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___threshold_8)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_threshold_8() const { return ___threshold_8; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_threshold_8() { return &___threshold_8; }
	inline void set_threshold_8(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___threshold_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___threshold_8), (void*)value);
	}

	inline static int32_t get_offset_of_softKnee_9() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___softKnee_9)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_softKnee_9() const { return ___softKnee_9; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_softKnee_9() { return &___softKnee_9; }
	inline void set_softKnee_9(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___softKnee_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___softKnee_9), (void*)value);
	}

	inline static int32_t get_offset_of_clamp_10() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___clamp_10)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_clamp_10() const { return ___clamp_10; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_clamp_10() { return &___clamp_10; }
	inline void set_clamp_10(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___clamp_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clamp_10), (void*)value);
	}

	inline static int32_t get_offset_of_diffusion_11() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___diffusion_11)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_diffusion_11() const { return ___diffusion_11; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_diffusion_11() { return &___diffusion_11; }
	inline void set_diffusion_11(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___diffusion_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___diffusion_11), (void*)value);
	}

	inline static int32_t get_offset_of_anamorphicRatio_12() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___anamorphicRatio_12)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_anamorphicRatio_12() const { return ___anamorphicRatio_12; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_anamorphicRatio_12() { return &___anamorphicRatio_12; }
	inline void set_anamorphicRatio_12(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___anamorphicRatio_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___anamorphicRatio_12), (void*)value);
	}

	inline static int32_t get_offset_of_color_13() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___color_13)); }
	inline ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * get_color_13() const { return ___color_13; }
	inline ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 ** get_address_of_color_13() { return &___color_13; }
	inline void set_color_13(ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * value)
	{
		___color_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___color_13), (void*)value);
	}

	inline static int32_t get_offset_of_fastMode_14() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___fastMode_14)); }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * get_fastMode_14() const { return ___fastMode_14; }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 ** get_address_of_fastMode_14() { return &___fastMode_14; }
	inline void set_fastMode_14(BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * value)
	{
		___fastMode_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fastMode_14), (void*)value);
	}

	inline static int32_t get_offset_of_dirtTexture_15() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___dirtTexture_15)); }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * get_dirtTexture_15() const { return ___dirtTexture_15; }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 ** get_address_of_dirtTexture_15() { return &___dirtTexture_15; }
	inline void set_dirtTexture_15(TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * value)
	{
		___dirtTexture_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dirtTexture_15), (void*)value);
	}

	inline static int32_t get_offset_of_dirtIntensity_16() { return static_cast<int32_t>(offsetof(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8, ___dirtIntensity_16)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_dirtIntensity_16() const { return ___dirtIntensity_16; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_dirtIntensity_16() { return &___dirtIntensity_16; }
	inline void set_dirtIntensity_16(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___dirtIntensity_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dirtIntensity_16), (void*)value);
	}
};


// UnityEngine.Rendering.PostProcessing.ChromaticAberration
struct ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229  : public PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0
{
public:
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::spectralLut
	TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * ___spectralLut_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::intensity
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___intensity_8;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.ChromaticAberration::fastMode
	BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * ___fastMode_9;

public:
	inline static int32_t get_offset_of_spectralLut_7() { return static_cast<int32_t>(offsetof(ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229, ___spectralLut_7)); }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * get_spectralLut_7() const { return ___spectralLut_7; }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 ** get_address_of_spectralLut_7() { return &___spectralLut_7; }
	inline void set_spectralLut_7(TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * value)
	{
		___spectralLut_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spectralLut_7), (void*)value);
	}

	inline static int32_t get_offset_of_intensity_8() { return static_cast<int32_t>(offsetof(ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229, ___intensity_8)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_intensity_8() const { return ___intensity_8; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_intensity_8() { return &___intensity_8; }
	inline void set_intensity_8(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___intensity_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intensity_8), (void*)value);
	}

	inline static int32_t get_offset_of_fastMode_9() { return static_cast<int32_t>(offsetof(ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229, ___fastMode_9)); }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * get_fastMode_9() const { return ___fastMode_9; }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 ** get_address_of_fastMode_9() { return &___fastMode_9; }
	inline void set_fastMode_9(BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * value)
	{
		___fastMode_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fastMode_9), (void*)value);
	}
};


// UnityEngine.Rendering.PostProcessing.ColorGrading
struct ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5  : public PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0
{
public:
	// UnityEngine.Rendering.PostProcessing.GradingModeParameter UnityEngine.Rendering.PostProcessing.ColorGrading::gradingMode
	GradingModeParameter_tA53C77634D098FA15A6CDFBEF2C5EBB3BBF97913 * ___gradingMode_7;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ColorGrading::externalLut
	TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * ___externalLut_8;
	// UnityEngine.Rendering.PostProcessing.TonemapperParameter UnityEngine.Rendering.PostProcessing.ColorGrading::tonemapper
	TonemapperParameter_t135C78E99EB670872D17A05A7CBF0F4E3F8A222B * ___tonemapper_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveToeStrength
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___toneCurveToeStrength_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveToeLength
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___toneCurveToeLength_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderStrength
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___toneCurveShoulderStrength_12;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderLength
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___toneCurveShoulderLength_13;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveShoulderAngle
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___toneCurveShoulderAngle_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::toneCurveGamma
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___toneCurveGamma_15;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.ColorGrading::ldrLut
	TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * ___ldrLut_16;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::ldrLutContribution
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___ldrLutContribution_17;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::temperature
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___temperature_18;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::tint
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___tint_19;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.ColorGrading::colorFilter
	ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * ___colorFilter_20;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueShift
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___hueShift_21;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::saturation
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___saturation_22;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::brightness
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___brightness_23;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::postExposure
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___postExposure_24;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::contrast
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___contrast_25;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutRedIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerRedOutRedIn_26;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutGreenIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerRedOutGreenIn_27;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerRedOutBlueIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerRedOutBlueIn_28;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutRedIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerGreenOutRedIn_29;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutGreenIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerGreenOutGreenIn_30;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerGreenOutBlueIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerGreenOutBlueIn_31;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutRedIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerBlueOutRedIn_32;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutGreenIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerBlueOutGreenIn_33;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.ColorGrading::mixerBlueOutBlueIn
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___mixerBlueOutBlueIn_34;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::lift
	Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * ___lift_35;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::gamma
	Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * ___gamma_36;
	// UnityEngine.Rendering.PostProcessing.Vector4Parameter UnityEngine.Rendering.PostProcessing.ColorGrading::gain
	Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * ___gain_37;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::masterCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___masterCurve_38;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::redCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___redCurve_39;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::greenCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___greenCurve_40;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::blueCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___blueCurve_41;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueVsHueCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___hueVsHueCurve_42;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::hueVsSatCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___hueVsSatCurve_43;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::satVsSatCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___satVsSatCurve_44;
	// UnityEngine.Rendering.PostProcessing.SplineParameter UnityEngine.Rendering.PostProcessing.ColorGrading::lumVsSatCurve
	SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * ___lumVsSatCurve_45;

public:
	inline static int32_t get_offset_of_gradingMode_7() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___gradingMode_7)); }
	inline GradingModeParameter_tA53C77634D098FA15A6CDFBEF2C5EBB3BBF97913 * get_gradingMode_7() const { return ___gradingMode_7; }
	inline GradingModeParameter_tA53C77634D098FA15A6CDFBEF2C5EBB3BBF97913 ** get_address_of_gradingMode_7() { return &___gradingMode_7; }
	inline void set_gradingMode_7(GradingModeParameter_tA53C77634D098FA15A6CDFBEF2C5EBB3BBF97913 * value)
	{
		___gradingMode_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gradingMode_7), (void*)value);
	}

	inline static int32_t get_offset_of_externalLut_8() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___externalLut_8)); }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * get_externalLut_8() const { return ___externalLut_8; }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 ** get_address_of_externalLut_8() { return &___externalLut_8; }
	inline void set_externalLut_8(TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * value)
	{
		___externalLut_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___externalLut_8), (void*)value);
	}

	inline static int32_t get_offset_of_tonemapper_9() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___tonemapper_9)); }
	inline TonemapperParameter_t135C78E99EB670872D17A05A7CBF0F4E3F8A222B * get_tonemapper_9() const { return ___tonemapper_9; }
	inline TonemapperParameter_t135C78E99EB670872D17A05A7CBF0F4E3F8A222B ** get_address_of_tonemapper_9() { return &___tonemapper_9; }
	inline void set_tonemapper_9(TonemapperParameter_t135C78E99EB670872D17A05A7CBF0F4E3F8A222B * value)
	{
		___tonemapper_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tonemapper_9), (void*)value);
	}

	inline static int32_t get_offset_of_toneCurveToeStrength_10() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___toneCurveToeStrength_10)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_toneCurveToeStrength_10() const { return ___toneCurveToeStrength_10; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_toneCurveToeStrength_10() { return &___toneCurveToeStrength_10; }
	inline void set_toneCurveToeStrength_10(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___toneCurveToeStrength_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toneCurveToeStrength_10), (void*)value);
	}

	inline static int32_t get_offset_of_toneCurveToeLength_11() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___toneCurveToeLength_11)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_toneCurveToeLength_11() const { return ___toneCurveToeLength_11; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_toneCurveToeLength_11() { return &___toneCurveToeLength_11; }
	inline void set_toneCurveToeLength_11(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___toneCurveToeLength_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toneCurveToeLength_11), (void*)value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderStrength_12() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___toneCurveShoulderStrength_12)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_toneCurveShoulderStrength_12() const { return ___toneCurveShoulderStrength_12; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_toneCurveShoulderStrength_12() { return &___toneCurveShoulderStrength_12; }
	inline void set_toneCurveShoulderStrength_12(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___toneCurveShoulderStrength_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toneCurveShoulderStrength_12), (void*)value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderLength_13() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___toneCurveShoulderLength_13)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_toneCurveShoulderLength_13() const { return ___toneCurveShoulderLength_13; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_toneCurveShoulderLength_13() { return &___toneCurveShoulderLength_13; }
	inline void set_toneCurveShoulderLength_13(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___toneCurveShoulderLength_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toneCurveShoulderLength_13), (void*)value);
	}

	inline static int32_t get_offset_of_toneCurveShoulderAngle_14() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___toneCurveShoulderAngle_14)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_toneCurveShoulderAngle_14() const { return ___toneCurveShoulderAngle_14; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_toneCurveShoulderAngle_14() { return &___toneCurveShoulderAngle_14; }
	inline void set_toneCurveShoulderAngle_14(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___toneCurveShoulderAngle_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toneCurveShoulderAngle_14), (void*)value);
	}

	inline static int32_t get_offset_of_toneCurveGamma_15() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___toneCurveGamma_15)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_toneCurveGamma_15() const { return ___toneCurveGamma_15; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_toneCurveGamma_15() { return &___toneCurveGamma_15; }
	inline void set_toneCurveGamma_15(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___toneCurveGamma_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___toneCurveGamma_15), (void*)value);
	}

	inline static int32_t get_offset_of_ldrLut_16() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___ldrLut_16)); }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * get_ldrLut_16() const { return ___ldrLut_16; }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 ** get_address_of_ldrLut_16() { return &___ldrLut_16; }
	inline void set_ldrLut_16(TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * value)
	{
		___ldrLut_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ldrLut_16), (void*)value);
	}

	inline static int32_t get_offset_of_ldrLutContribution_17() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___ldrLutContribution_17)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_ldrLutContribution_17() const { return ___ldrLutContribution_17; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_ldrLutContribution_17() { return &___ldrLutContribution_17; }
	inline void set_ldrLutContribution_17(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___ldrLutContribution_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ldrLutContribution_17), (void*)value);
	}

	inline static int32_t get_offset_of_temperature_18() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___temperature_18)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_temperature_18() const { return ___temperature_18; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_temperature_18() { return &___temperature_18; }
	inline void set_temperature_18(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___temperature_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___temperature_18), (void*)value);
	}

	inline static int32_t get_offset_of_tint_19() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___tint_19)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_tint_19() const { return ___tint_19; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_tint_19() { return &___tint_19; }
	inline void set_tint_19(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___tint_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tint_19), (void*)value);
	}

	inline static int32_t get_offset_of_colorFilter_20() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___colorFilter_20)); }
	inline ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * get_colorFilter_20() const { return ___colorFilter_20; }
	inline ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 ** get_address_of_colorFilter_20() { return &___colorFilter_20; }
	inline void set_colorFilter_20(ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * value)
	{
		___colorFilter_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colorFilter_20), (void*)value);
	}

	inline static int32_t get_offset_of_hueShift_21() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___hueShift_21)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_hueShift_21() const { return ___hueShift_21; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_hueShift_21() { return &___hueShift_21; }
	inline void set_hueShift_21(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___hueShift_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hueShift_21), (void*)value);
	}

	inline static int32_t get_offset_of_saturation_22() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___saturation_22)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_saturation_22() const { return ___saturation_22; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_saturation_22() { return &___saturation_22; }
	inline void set_saturation_22(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___saturation_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___saturation_22), (void*)value);
	}

	inline static int32_t get_offset_of_brightness_23() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___brightness_23)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_brightness_23() const { return ___brightness_23; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_brightness_23() { return &___brightness_23; }
	inline void set_brightness_23(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___brightness_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___brightness_23), (void*)value);
	}

	inline static int32_t get_offset_of_postExposure_24() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___postExposure_24)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_postExposure_24() const { return ___postExposure_24; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_postExposure_24() { return &___postExposure_24; }
	inline void set_postExposure_24(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___postExposure_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___postExposure_24), (void*)value);
	}

	inline static int32_t get_offset_of_contrast_25() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___contrast_25)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_contrast_25() const { return ___contrast_25; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_contrast_25() { return &___contrast_25; }
	inline void set_contrast_25(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___contrast_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___contrast_25), (void*)value);
	}

	inline static int32_t get_offset_of_mixerRedOutRedIn_26() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerRedOutRedIn_26)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerRedOutRedIn_26() const { return ___mixerRedOutRedIn_26; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerRedOutRedIn_26() { return &___mixerRedOutRedIn_26; }
	inline void set_mixerRedOutRedIn_26(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerRedOutRedIn_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerRedOutRedIn_26), (void*)value);
	}

	inline static int32_t get_offset_of_mixerRedOutGreenIn_27() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerRedOutGreenIn_27)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerRedOutGreenIn_27() const { return ___mixerRedOutGreenIn_27; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerRedOutGreenIn_27() { return &___mixerRedOutGreenIn_27; }
	inline void set_mixerRedOutGreenIn_27(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerRedOutGreenIn_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerRedOutGreenIn_27), (void*)value);
	}

	inline static int32_t get_offset_of_mixerRedOutBlueIn_28() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerRedOutBlueIn_28)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerRedOutBlueIn_28() const { return ___mixerRedOutBlueIn_28; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerRedOutBlueIn_28() { return &___mixerRedOutBlueIn_28; }
	inline void set_mixerRedOutBlueIn_28(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerRedOutBlueIn_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerRedOutBlueIn_28), (void*)value);
	}

	inline static int32_t get_offset_of_mixerGreenOutRedIn_29() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerGreenOutRedIn_29)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerGreenOutRedIn_29() const { return ___mixerGreenOutRedIn_29; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerGreenOutRedIn_29() { return &___mixerGreenOutRedIn_29; }
	inline void set_mixerGreenOutRedIn_29(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerGreenOutRedIn_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerGreenOutRedIn_29), (void*)value);
	}

	inline static int32_t get_offset_of_mixerGreenOutGreenIn_30() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerGreenOutGreenIn_30)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerGreenOutGreenIn_30() const { return ___mixerGreenOutGreenIn_30; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerGreenOutGreenIn_30() { return &___mixerGreenOutGreenIn_30; }
	inline void set_mixerGreenOutGreenIn_30(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerGreenOutGreenIn_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerGreenOutGreenIn_30), (void*)value);
	}

	inline static int32_t get_offset_of_mixerGreenOutBlueIn_31() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerGreenOutBlueIn_31)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerGreenOutBlueIn_31() const { return ___mixerGreenOutBlueIn_31; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerGreenOutBlueIn_31() { return &___mixerGreenOutBlueIn_31; }
	inline void set_mixerGreenOutBlueIn_31(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerGreenOutBlueIn_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerGreenOutBlueIn_31), (void*)value);
	}

	inline static int32_t get_offset_of_mixerBlueOutRedIn_32() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerBlueOutRedIn_32)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerBlueOutRedIn_32() const { return ___mixerBlueOutRedIn_32; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerBlueOutRedIn_32() { return &___mixerBlueOutRedIn_32; }
	inline void set_mixerBlueOutRedIn_32(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerBlueOutRedIn_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerBlueOutRedIn_32), (void*)value);
	}

	inline static int32_t get_offset_of_mixerBlueOutGreenIn_33() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerBlueOutGreenIn_33)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerBlueOutGreenIn_33() const { return ___mixerBlueOutGreenIn_33; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerBlueOutGreenIn_33() { return &___mixerBlueOutGreenIn_33; }
	inline void set_mixerBlueOutGreenIn_33(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerBlueOutGreenIn_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerBlueOutGreenIn_33), (void*)value);
	}

	inline static int32_t get_offset_of_mixerBlueOutBlueIn_34() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___mixerBlueOutBlueIn_34)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_mixerBlueOutBlueIn_34() const { return ___mixerBlueOutBlueIn_34; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_mixerBlueOutBlueIn_34() { return &___mixerBlueOutBlueIn_34; }
	inline void set_mixerBlueOutBlueIn_34(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___mixerBlueOutBlueIn_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mixerBlueOutBlueIn_34), (void*)value);
	}

	inline static int32_t get_offset_of_lift_35() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___lift_35)); }
	inline Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * get_lift_35() const { return ___lift_35; }
	inline Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 ** get_address_of_lift_35() { return &___lift_35; }
	inline void set_lift_35(Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * value)
	{
		___lift_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lift_35), (void*)value);
	}

	inline static int32_t get_offset_of_gamma_36() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___gamma_36)); }
	inline Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * get_gamma_36() const { return ___gamma_36; }
	inline Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 ** get_address_of_gamma_36() { return &___gamma_36; }
	inline void set_gamma_36(Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * value)
	{
		___gamma_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gamma_36), (void*)value);
	}

	inline static int32_t get_offset_of_gain_37() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___gain_37)); }
	inline Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * get_gain_37() const { return ___gain_37; }
	inline Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 ** get_address_of_gain_37() { return &___gain_37; }
	inline void set_gain_37(Vector4Parameter_t1AFAD06AB301389B859FAFA51D2C3C7E066E1627 * value)
	{
		___gain_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___gain_37), (void*)value);
	}

	inline static int32_t get_offset_of_masterCurve_38() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___masterCurve_38)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_masterCurve_38() const { return ___masterCurve_38; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_masterCurve_38() { return &___masterCurve_38; }
	inline void set_masterCurve_38(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___masterCurve_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___masterCurve_38), (void*)value);
	}

	inline static int32_t get_offset_of_redCurve_39() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___redCurve_39)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_redCurve_39() const { return ___redCurve_39; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_redCurve_39() { return &___redCurve_39; }
	inline void set_redCurve_39(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___redCurve_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___redCurve_39), (void*)value);
	}

	inline static int32_t get_offset_of_greenCurve_40() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___greenCurve_40)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_greenCurve_40() const { return ___greenCurve_40; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_greenCurve_40() { return &___greenCurve_40; }
	inline void set_greenCurve_40(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___greenCurve_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___greenCurve_40), (void*)value);
	}

	inline static int32_t get_offset_of_blueCurve_41() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___blueCurve_41)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_blueCurve_41() const { return ___blueCurve_41; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_blueCurve_41() { return &___blueCurve_41; }
	inline void set_blueCurve_41(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___blueCurve_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blueCurve_41), (void*)value);
	}

	inline static int32_t get_offset_of_hueVsHueCurve_42() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___hueVsHueCurve_42)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_hueVsHueCurve_42() const { return ___hueVsHueCurve_42; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_hueVsHueCurve_42() { return &___hueVsHueCurve_42; }
	inline void set_hueVsHueCurve_42(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___hueVsHueCurve_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hueVsHueCurve_42), (void*)value);
	}

	inline static int32_t get_offset_of_hueVsSatCurve_43() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___hueVsSatCurve_43)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_hueVsSatCurve_43() const { return ___hueVsSatCurve_43; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_hueVsSatCurve_43() { return &___hueVsSatCurve_43; }
	inline void set_hueVsSatCurve_43(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___hueVsSatCurve_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___hueVsSatCurve_43), (void*)value);
	}

	inline static int32_t get_offset_of_satVsSatCurve_44() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___satVsSatCurve_44)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_satVsSatCurve_44() const { return ___satVsSatCurve_44; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_satVsSatCurve_44() { return &___satVsSatCurve_44; }
	inline void set_satVsSatCurve_44(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___satVsSatCurve_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___satVsSatCurve_44), (void*)value);
	}

	inline static int32_t get_offset_of_lumVsSatCurve_45() { return static_cast<int32_t>(offsetof(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5, ___lumVsSatCurve_45)); }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * get_lumVsSatCurve_45() const { return ___lumVsSatCurve_45; }
	inline SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 ** get_address_of_lumVsSatCurve_45() { return &___lumVsSatCurve_45; }
	inline void set_lumVsSatCurve_45(SplineParameter_t34BEB0723CDE19AAB7E784B20834CABC7B490339 * value)
	{
		___lumVsSatCurve_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lumVsSatCurve_45), (void*)value);
	}
};


// UnityEngine.Rendering.PostProcessing.DepthOfField
struct DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731  : public PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::focusDistance
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___focusDistance_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::aperture
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___aperture_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.DepthOfField::focalLength
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___focalLength_9;
	// UnityEngine.Rendering.PostProcessing.KernelSizeParameter UnityEngine.Rendering.PostProcessing.DepthOfField::kernelSize
	KernelSizeParameter_tD40A9C16004C0686D34035CCC8E1F6E2D8A26D89 * ___kernelSize_10;

public:
	inline static int32_t get_offset_of_focusDistance_7() { return static_cast<int32_t>(offsetof(DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731, ___focusDistance_7)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_focusDistance_7() const { return ___focusDistance_7; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_focusDistance_7() { return &___focusDistance_7; }
	inline void set_focusDistance_7(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___focusDistance_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___focusDistance_7), (void*)value);
	}

	inline static int32_t get_offset_of_aperture_8() { return static_cast<int32_t>(offsetof(DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731, ___aperture_8)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_aperture_8() const { return ___aperture_8; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_aperture_8() { return &___aperture_8; }
	inline void set_aperture_8(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___aperture_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___aperture_8), (void*)value);
	}

	inline static int32_t get_offset_of_focalLength_9() { return static_cast<int32_t>(offsetof(DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731, ___focalLength_9)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_focalLength_9() const { return ___focalLength_9; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_focalLength_9() { return &___focalLength_9; }
	inline void set_focalLength_9(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___focalLength_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___focalLength_9), (void*)value);
	}

	inline static int32_t get_offset_of_kernelSize_10() { return static_cast<int32_t>(offsetof(DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731, ___kernelSize_10)); }
	inline KernelSizeParameter_tD40A9C16004C0686D34035CCC8E1F6E2D8A26D89 * get_kernelSize_10() const { return ___kernelSize_10; }
	inline KernelSizeParameter_tD40A9C16004C0686D34035CCC8E1F6E2D8A26D89 ** get_address_of_kernelSize_10() { return &___kernelSize_10; }
	inline void set_kernelSize_10(KernelSizeParameter_tD40A9C16004C0686D34035CCC8E1F6E2D8A26D89 * value)
	{
		___kernelSize_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___kernelSize_10), (void*)value);
	}
};


// UnityEngine.Rendering.PostProcessing.LensDistortion
struct LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2  : public PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0
{
public:
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensity
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___intensity_7;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensityX
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___intensityX_8;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::intensityY
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___intensityY_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::centerX
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___centerX_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::centerY
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___centerY_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.LensDistortion::scale
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___scale_12;

public:
	inline static int32_t get_offset_of_intensity_7() { return static_cast<int32_t>(offsetof(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2, ___intensity_7)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_intensity_7() const { return ___intensity_7; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_intensity_7() { return &___intensity_7; }
	inline void set_intensity_7(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___intensity_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intensity_7), (void*)value);
	}

	inline static int32_t get_offset_of_intensityX_8() { return static_cast<int32_t>(offsetof(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2, ___intensityX_8)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_intensityX_8() const { return ___intensityX_8; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_intensityX_8() { return &___intensityX_8; }
	inline void set_intensityX_8(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___intensityX_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intensityX_8), (void*)value);
	}

	inline static int32_t get_offset_of_intensityY_9() { return static_cast<int32_t>(offsetof(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2, ___intensityY_9)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_intensityY_9() const { return ___intensityY_9; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_intensityY_9() { return &___intensityY_9; }
	inline void set_intensityY_9(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___intensityY_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intensityY_9), (void*)value);
	}

	inline static int32_t get_offset_of_centerX_10() { return static_cast<int32_t>(offsetof(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2, ___centerX_10)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_centerX_10() const { return ___centerX_10; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_centerX_10() { return &___centerX_10; }
	inline void set_centerX_10(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___centerX_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___centerX_10), (void*)value);
	}

	inline static int32_t get_offset_of_centerY_11() { return static_cast<int32_t>(offsetof(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2, ___centerY_11)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_centerY_11() const { return ___centerY_11; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_centerY_11() { return &___centerY_11; }
	inline void set_centerY_11(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___centerY_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___centerY_11), (void*)value);
	}

	inline static int32_t get_offset_of_scale_12() { return static_cast<int32_t>(offsetof(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2, ___scale_12)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_scale_12() const { return ___scale_12; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_scale_12() { return &___scale_12; }
	inline void set_scale_12(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___scale_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scale_12), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Rendering.PostProcessing.Vignette
struct Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139  : public PostProcessEffectSettings_t2F43C0693DFF9BD09B921BB6278EC086A6F817F0
{
public:
	// UnityEngine.Rendering.PostProcessing.VignetteModeParameter UnityEngine.Rendering.PostProcessing.Vignette::mode
	VignetteModeParameter_tA57BF7F5798A90AE3DFE38CF80A85E931F3DD613 * ___mode_7;
	// UnityEngine.Rendering.PostProcessing.ColorParameter UnityEngine.Rendering.PostProcessing.Vignette::color
	ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * ___color_8;
	// UnityEngine.Rendering.PostProcessing.Vector2Parameter UnityEngine.Rendering.PostProcessing.Vignette::center
	Vector2Parameter_t0E1CE8C27130B36A75688B706DE0EACCE9DE41A3 * ___center_9;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::intensity
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___intensity_10;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::smoothness
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___smoothness_11;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::roundness
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___roundness_12;
	// UnityEngine.Rendering.PostProcessing.BoolParameter UnityEngine.Rendering.PostProcessing.Vignette::rounded
	BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * ___rounded_13;
	// UnityEngine.Rendering.PostProcessing.TextureParameter UnityEngine.Rendering.PostProcessing.Vignette::mask
	TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * ___mask_14;
	// UnityEngine.Rendering.PostProcessing.FloatParameter UnityEngine.Rendering.PostProcessing.Vignette::opacity
	FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * ___opacity_15;

public:
	inline static int32_t get_offset_of_mode_7() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___mode_7)); }
	inline VignetteModeParameter_tA57BF7F5798A90AE3DFE38CF80A85E931F3DD613 * get_mode_7() const { return ___mode_7; }
	inline VignetteModeParameter_tA57BF7F5798A90AE3DFE38CF80A85E931F3DD613 ** get_address_of_mode_7() { return &___mode_7; }
	inline void set_mode_7(VignetteModeParameter_tA57BF7F5798A90AE3DFE38CF80A85E931F3DD613 * value)
	{
		___mode_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mode_7), (void*)value);
	}

	inline static int32_t get_offset_of_color_8() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___color_8)); }
	inline ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * get_color_8() const { return ___color_8; }
	inline ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 ** get_address_of_color_8() { return &___color_8; }
	inline void set_color_8(ColorParameter_t2C43807B2FABC978D5B12345759389816D9CA7C5 * value)
	{
		___color_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___color_8), (void*)value);
	}

	inline static int32_t get_offset_of_center_9() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___center_9)); }
	inline Vector2Parameter_t0E1CE8C27130B36A75688B706DE0EACCE9DE41A3 * get_center_9() const { return ___center_9; }
	inline Vector2Parameter_t0E1CE8C27130B36A75688B706DE0EACCE9DE41A3 ** get_address_of_center_9() { return &___center_9; }
	inline void set_center_9(Vector2Parameter_t0E1CE8C27130B36A75688B706DE0EACCE9DE41A3 * value)
	{
		___center_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___center_9), (void*)value);
	}

	inline static int32_t get_offset_of_intensity_10() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___intensity_10)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_intensity_10() const { return ___intensity_10; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_intensity_10() { return &___intensity_10; }
	inline void set_intensity_10(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___intensity_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intensity_10), (void*)value);
	}

	inline static int32_t get_offset_of_smoothness_11() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___smoothness_11)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_smoothness_11() const { return ___smoothness_11; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_smoothness_11() { return &___smoothness_11; }
	inline void set_smoothness_11(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___smoothness_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___smoothness_11), (void*)value);
	}

	inline static int32_t get_offset_of_roundness_12() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___roundness_12)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_roundness_12() const { return ___roundness_12; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_roundness_12() { return &___roundness_12; }
	inline void set_roundness_12(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___roundness_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___roundness_12), (void*)value);
	}

	inline static int32_t get_offset_of_rounded_13() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___rounded_13)); }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * get_rounded_13() const { return ___rounded_13; }
	inline BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 ** get_address_of_rounded_13() { return &___rounded_13; }
	inline void set_rounded_13(BoolParameter_t4EDC330A9E3F5830FCE384CE1FF707D3276EBF80 * value)
	{
		___rounded_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rounded_13), (void*)value);
	}

	inline static int32_t get_offset_of_mask_14() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___mask_14)); }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * get_mask_14() const { return ___mask_14; }
	inline TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 ** get_address_of_mask_14() { return &___mask_14; }
	inline void set_mask_14(TextureParameter_tC6A936F3D3C6DD5834DC7866D62E1BE3073BD8C2 * value)
	{
		___mask_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mask_14), (void*)value);
	}

	inline static int32_t get_offset_of_opacity_15() { return static_cast<int32_t>(offsetof(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139, ___opacity_15)); }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * get_opacity_15() const { return ___opacity_15; }
	inline FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 ** get_address_of_opacity_15() { return &___opacity_15; }
	inline void set_opacity_15(FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * value)
	{
		___opacity_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___opacity_15), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMAutoFocus
struct MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform MoreMountains.FeedbacksForThirdParty.MMAutoFocus::CameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___CameraTransform_4;
	// UnityEngine.Transform[] MoreMountains.FeedbacksForThirdParty.MMAutoFocus::FocusTargets
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___FocusTargets_5;
	// UnityEngine.Vector3 MoreMountains.FeedbacksForThirdParty.MMAutoFocus::Offset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___Offset_6;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMAutoFocus::FocusTargetID
	float ___FocusTargetID_7;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMAutoFocus::Aperture
	float ___Aperture_8;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMAutoFocus::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_9;
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile MoreMountains.FeedbacksForThirdParty.MMAutoFocus::_profile
	PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * ____profile_10;
	// UnityEngine.Rendering.PostProcessing.DepthOfField MoreMountains.FeedbacksForThirdParty.MMAutoFocus::_depthOfField
	DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * ____depthOfField_11;

public:
	inline static int32_t get_offset_of_CameraTransform_4() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ___CameraTransform_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_CameraTransform_4() const { return ___CameraTransform_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_CameraTransform_4() { return &___CameraTransform_4; }
	inline void set_CameraTransform_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___CameraTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CameraTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of_FocusTargets_5() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ___FocusTargets_5)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_FocusTargets_5() const { return ___FocusTargets_5; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_FocusTargets_5() { return &___FocusTargets_5; }
	inline void set_FocusTargets_5(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___FocusTargets_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FocusTargets_5), (void*)value);
	}

	inline static int32_t get_offset_of_Offset_6() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ___Offset_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_Offset_6() const { return ___Offset_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_Offset_6() { return &___Offset_6; }
	inline void set_Offset_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___Offset_6 = value;
	}

	inline static int32_t get_offset_of_FocusTargetID_7() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ___FocusTargetID_7)); }
	inline float get_FocusTargetID_7() const { return ___FocusTargetID_7; }
	inline float* get_address_of_FocusTargetID_7() { return &___FocusTargetID_7; }
	inline void set_FocusTargetID_7(float value)
	{
		___FocusTargetID_7 = value;
	}

	inline static int32_t get_offset_of_Aperture_8() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ___Aperture_8)); }
	inline float get_Aperture_8() const { return ___Aperture_8; }
	inline float* get_address_of_Aperture_8() { return &___Aperture_8; }
	inline void set_Aperture_8(float value)
	{
		___Aperture_8 = value;
	}

	inline static int32_t get_offset_of__volume_9() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ____volume_9)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_9() const { return ____volume_9; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_9() { return &____volume_9; }
	inline void set__volume_9(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_9), (void*)value);
	}

	inline static int32_t get_offset_of__profile_10() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ____profile_10)); }
	inline PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * get__profile_10() const { return ____profile_10; }
	inline PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E ** get_address_of__profile_10() { return &____profile_10; }
	inline void set__profile_10(PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * value)
	{
		____profile_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____profile_10), (void*)value);
	}

	inline static int32_t get_offset_of__depthOfField_11() { return static_cast<int32_t>(offsetof(MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA, ____depthOfField_11)); }
	inline DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * get__depthOfField_11() const { return ____depthOfField_11; }
	inline DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 ** get_address_of__depthOfField_11() { return &____depthOfField_11; }
	inline void set__depthOfField_11(DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * value)
	{
		____depthOfField_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____depthOfField_11), (void*)value);
	}
};


// MoreMountains.Feedbacks.MMFeedback
struct MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::Active
	bool ___Active_4;
	// System.String MoreMountains.Feedbacks.MMFeedback::Label
	String_t* ___Label_5;
	// System.Single MoreMountains.Feedbacks.MMFeedback::Chance
	float ___Chance_6;
	// MoreMountains.Feedbacks.MMFeedbackTiming MoreMountains.Feedbacks.MMFeedback::Timing
	MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * ___Timing_7;
	// UnityEngine.GameObject MoreMountains.Feedbacks.MMFeedback::<Owner>k__BackingField
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___U3COwnerU3Ek__BackingField_8;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::DebugActive
	bool ___DebugActive_9;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::<ScriptDrivenPause>k__BackingField
	bool ___U3CScriptDrivenPauseU3Ek__BackingField_10;
	// System.Single MoreMountains.Feedbacks.MMFeedback::<ScriptDrivenPauseAutoResume>k__BackingField
	float ___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11;
	// System.Single MoreMountains.Feedbacks.MMFeedback::_lastPlayTimestamp
	float ____lastPlayTimestamp_12;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::_playsLeft
	int32_t ____playsLeft_13;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::_initialized
	bool ____initialized_14;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_playCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____playCoroutine_15;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_infinitePlayCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____infinitePlayCoroutine_16;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_sequenceCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____sequenceCoroutine_17;
	// UnityEngine.Coroutine MoreMountains.Feedbacks.MMFeedback::_repeatedPlayCoroutine
	Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * ____repeatedPlayCoroutine_18;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::_sequenceTrackID
	int32_t ____sequenceTrackID_19;
	// MoreMountains.Feedbacks.MMFeedbacks MoreMountains.Feedbacks.MMFeedback::_hostMMFeedbacks
	MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * ____hostMMFeedbacks_20;
	// System.Single MoreMountains.Feedbacks.MMFeedback::_beatInterval
	float ____beatInterval_21;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::BeatThisFrame
	bool ___BeatThisFrame_22;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::LastBeatIndex
	int32_t ___LastBeatIndex_23;
	// System.Int32 MoreMountains.Feedbacks.MMFeedback::CurrentSequenceIndex
	int32_t ___CurrentSequenceIndex_24;
	// System.Single MoreMountains.Feedbacks.MMFeedback::LastBeatTimestamp
	float ___LastBeatTimestamp_25;
	// System.Boolean MoreMountains.Feedbacks.MMFeedback::_isHostMMFeedbacksNotNull
	bool ____isHostMMFeedbacksNotNull_26;

public:
	inline static int32_t get_offset_of_Active_4() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Active_4)); }
	inline bool get_Active_4() const { return ___Active_4; }
	inline bool* get_address_of_Active_4() { return &___Active_4; }
	inline void set_Active_4(bool value)
	{
		___Active_4 = value;
	}

	inline static int32_t get_offset_of_Label_5() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Label_5)); }
	inline String_t* get_Label_5() const { return ___Label_5; }
	inline String_t** get_address_of_Label_5() { return &___Label_5; }
	inline void set_Label_5(String_t* value)
	{
		___Label_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Label_5), (void*)value);
	}

	inline static int32_t get_offset_of_Chance_6() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Chance_6)); }
	inline float get_Chance_6() const { return ___Chance_6; }
	inline float* get_address_of_Chance_6() { return &___Chance_6; }
	inline void set_Chance_6(float value)
	{
		___Chance_6 = value;
	}

	inline static int32_t get_offset_of_Timing_7() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___Timing_7)); }
	inline MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * get_Timing_7() const { return ___Timing_7; }
	inline MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 ** get_address_of_Timing_7() { return &___Timing_7; }
	inline void set_Timing_7(MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * value)
	{
		___Timing_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Timing_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3COwnerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___U3COwnerU3Ek__BackingField_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_U3COwnerU3Ek__BackingField_8() const { return ___U3COwnerU3Ek__BackingField_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_U3COwnerU3Ek__BackingField_8() { return &___U3COwnerU3Ek__BackingField_8; }
	inline void set_U3COwnerU3Ek__BackingField_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___U3COwnerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3COwnerU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_DebugActive_9() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___DebugActive_9)); }
	inline bool get_DebugActive_9() const { return ___DebugActive_9; }
	inline bool* get_address_of_DebugActive_9() { return &___DebugActive_9; }
	inline void set_DebugActive_9(bool value)
	{
		___DebugActive_9 = value;
	}

	inline static int32_t get_offset_of_U3CScriptDrivenPauseU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___U3CScriptDrivenPauseU3Ek__BackingField_10)); }
	inline bool get_U3CScriptDrivenPauseU3Ek__BackingField_10() const { return ___U3CScriptDrivenPauseU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CScriptDrivenPauseU3Ek__BackingField_10() { return &___U3CScriptDrivenPauseU3Ek__BackingField_10; }
	inline void set_U3CScriptDrivenPauseU3Ek__BackingField_10(bool value)
	{
		___U3CScriptDrivenPauseU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11)); }
	inline float get_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11() const { return ___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11; }
	inline float* get_address_of_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11() { return &___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11; }
	inline void set_U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11(float value)
	{
		___U3CScriptDrivenPauseAutoResumeU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of__lastPlayTimestamp_12() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____lastPlayTimestamp_12)); }
	inline float get__lastPlayTimestamp_12() const { return ____lastPlayTimestamp_12; }
	inline float* get_address_of__lastPlayTimestamp_12() { return &____lastPlayTimestamp_12; }
	inline void set__lastPlayTimestamp_12(float value)
	{
		____lastPlayTimestamp_12 = value;
	}

	inline static int32_t get_offset_of__playsLeft_13() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____playsLeft_13)); }
	inline int32_t get__playsLeft_13() const { return ____playsLeft_13; }
	inline int32_t* get_address_of__playsLeft_13() { return &____playsLeft_13; }
	inline void set__playsLeft_13(int32_t value)
	{
		____playsLeft_13 = value;
	}

	inline static int32_t get_offset_of__initialized_14() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____initialized_14)); }
	inline bool get__initialized_14() const { return ____initialized_14; }
	inline bool* get_address_of__initialized_14() { return &____initialized_14; }
	inline void set__initialized_14(bool value)
	{
		____initialized_14 = value;
	}

	inline static int32_t get_offset_of__playCoroutine_15() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____playCoroutine_15)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__playCoroutine_15() const { return ____playCoroutine_15; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__playCoroutine_15() { return &____playCoroutine_15; }
	inline void set__playCoroutine_15(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____playCoroutine_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____playCoroutine_15), (void*)value);
	}

	inline static int32_t get_offset_of__infinitePlayCoroutine_16() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____infinitePlayCoroutine_16)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__infinitePlayCoroutine_16() const { return ____infinitePlayCoroutine_16; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__infinitePlayCoroutine_16() { return &____infinitePlayCoroutine_16; }
	inline void set__infinitePlayCoroutine_16(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____infinitePlayCoroutine_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____infinitePlayCoroutine_16), (void*)value);
	}

	inline static int32_t get_offset_of__sequenceCoroutine_17() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____sequenceCoroutine_17)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__sequenceCoroutine_17() const { return ____sequenceCoroutine_17; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__sequenceCoroutine_17() { return &____sequenceCoroutine_17; }
	inline void set__sequenceCoroutine_17(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____sequenceCoroutine_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sequenceCoroutine_17), (void*)value);
	}

	inline static int32_t get_offset_of__repeatedPlayCoroutine_18() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____repeatedPlayCoroutine_18)); }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * get__repeatedPlayCoroutine_18() const { return ____repeatedPlayCoroutine_18; }
	inline Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 ** get_address_of__repeatedPlayCoroutine_18() { return &____repeatedPlayCoroutine_18; }
	inline void set__repeatedPlayCoroutine_18(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * value)
	{
		____repeatedPlayCoroutine_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____repeatedPlayCoroutine_18), (void*)value);
	}

	inline static int32_t get_offset_of__sequenceTrackID_19() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____sequenceTrackID_19)); }
	inline int32_t get__sequenceTrackID_19() const { return ____sequenceTrackID_19; }
	inline int32_t* get_address_of__sequenceTrackID_19() { return &____sequenceTrackID_19; }
	inline void set__sequenceTrackID_19(int32_t value)
	{
		____sequenceTrackID_19 = value;
	}

	inline static int32_t get_offset_of__hostMMFeedbacks_20() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____hostMMFeedbacks_20)); }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * get__hostMMFeedbacks_20() const { return ____hostMMFeedbacks_20; }
	inline MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 ** get_address_of__hostMMFeedbacks_20() { return &____hostMMFeedbacks_20; }
	inline void set__hostMMFeedbacks_20(MMFeedbacks_t85E1CCEB3EF536B1B61EEA20ACEBB974F3FE2914 * value)
	{
		____hostMMFeedbacks_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____hostMMFeedbacks_20), (void*)value);
	}

	inline static int32_t get_offset_of__beatInterval_21() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____beatInterval_21)); }
	inline float get__beatInterval_21() const { return ____beatInterval_21; }
	inline float* get_address_of__beatInterval_21() { return &____beatInterval_21; }
	inline void set__beatInterval_21(float value)
	{
		____beatInterval_21 = value;
	}

	inline static int32_t get_offset_of_BeatThisFrame_22() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___BeatThisFrame_22)); }
	inline bool get_BeatThisFrame_22() const { return ___BeatThisFrame_22; }
	inline bool* get_address_of_BeatThisFrame_22() { return &___BeatThisFrame_22; }
	inline void set_BeatThisFrame_22(bool value)
	{
		___BeatThisFrame_22 = value;
	}

	inline static int32_t get_offset_of_LastBeatIndex_23() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___LastBeatIndex_23)); }
	inline int32_t get_LastBeatIndex_23() const { return ___LastBeatIndex_23; }
	inline int32_t* get_address_of_LastBeatIndex_23() { return &___LastBeatIndex_23; }
	inline void set_LastBeatIndex_23(int32_t value)
	{
		___LastBeatIndex_23 = value;
	}

	inline static int32_t get_offset_of_CurrentSequenceIndex_24() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___CurrentSequenceIndex_24)); }
	inline int32_t get_CurrentSequenceIndex_24() const { return ___CurrentSequenceIndex_24; }
	inline int32_t* get_address_of_CurrentSequenceIndex_24() { return &___CurrentSequenceIndex_24; }
	inline void set_CurrentSequenceIndex_24(int32_t value)
	{
		___CurrentSequenceIndex_24 = value;
	}

	inline static int32_t get_offset_of_LastBeatTimestamp_25() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ___LastBeatTimestamp_25)); }
	inline float get_LastBeatTimestamp_25() const { return ___LastBeatTimestamp_25; }
	inline float* get_address_of_LastBeatTimestamp_25() { return &___LastBeatTimestamp_25; }
	inline void set_LastBeatTimestamp_25(float value)
	{
		___LastBeatTimestamp_25 = value;
	}

	inline static int32_t get_offset_of__isHostMMFeedbacksNotNull_26() { return static_cast<int32_t>(offsetof(MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4, ____isHostMMFeedbacksNotNull_26)); }
	inline bool get__isHostMMFeedbacksNotNull_26() const { return ____isHostMMFeedbacksNotNull_26; }
	inline bool* get_address_of__isHostMMFeedbacksNotNull_26() { return &____isHostMMFeedbacksNotNull_26; }
	inline void set__isHostMMFeedbacksNotNull_26(bool value)
	{
		____isHostMMFeedbacksNotNull_26 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend
struct MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend/BlendTriggerModes MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::BlendTriggerMode
	int32_t ___BlendTriggerMode_4;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::BlendDuration
	float ___BlendDuration_5;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Curve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___Curve_6;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::InitialWeight
	float ___InitialWeight_7;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::FinalWeight
	float ___FinalWeight_8;
	// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend/TimeScales MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::TimeScale
	int32_t ___TimeScale_9;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::DisableVolumeOnZeroWeight
	bool ___DisableVolumeOnZeroWeight_10;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::DisableSelfAfterEnd
	bool ___DisableSelfAfterEnd_11;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Interruptable
	bool ___Interruptable_12;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StartFromCurrentValue
	bool ___StartFromCurrentValue_13;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::ResetToInitialValueOnEnd
	bool ___ResetToInitialValueOnEnd_14;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::TestBlend
	bool ___TestBlend_15;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::TestBlendBackwards
	bool ___TestBlendBackwards_16;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::_initial
	float ____initial_17;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::_destination
	float ____destination_18;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::_startTime
	float ____startTime_19;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::_blending
	bool ____blending_20;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_21;

public:
	inline static int32_t get_offset_of_BlendTriggerMode_4() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___BlendTriggerMode_4)); }
	inline int32_t get_BlendTriggerMode_4() const { return ___BlendTriggerMode_4; }
	inline int32_t* get_address_of_BlendTriggerMode_4() { return &___BlendTriggerMode_4; }
	inline void set_BlendTriggerMode_4(int32_t value)
	{
		___BlendTriggerMode_4 = value;
	}

	inline static int32_t get_offset_of_BlendDuration_5() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___BlendDuration_5)); }
	inline float get_BlendDuration_5() const { return ___BlendDuration_5; }
	inline float* get_address_of_BlendDuration_5() { return &___BlendDuration_5; }
	inline void set_BlendDuration_5(float value)
	{
		___BlendDuration_5 = value;
	}

	inline static int32_t get_offset_of_Curve_6() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___Curve_6)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_Curve_6() const { return ___Curve_6; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_Curve_6() { return &___Curve_6; }
	inline void set_Curve_6(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___Curve_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Curve_6), (void*)value);
	}

	inline static int32_t get_offset_of_InitialWeight_7() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___InitialWeight_7)); }
	inline float get_InitialWeight_7() const { return ___InitialWeight_7; }
	inline float* get_address_of_InitialWeight_7() { return &___InitialWeight_7; }
	inline void set_InitialWeight_7(float value)
	{
		___InitialWeight_7 = value;
	}

	inline static int32_t get_offset_of_FinalWeight_8() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___FinalWeight_8)); }
	inline float get_FinalWeight_8() const { return ___FinalWeight_8; }
	inline float* get_address_of_FinalWeight_8() { return &___FinalWeight_8; }
	inline void set_FinalWeight_8(float value)
	{
		___FinalWeight_8 = value;
	}

	inline static int32_t get_offset_of_TimeScale_9() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___TimeScale_9)); }
	inline int32_t get_TimeScale_9() const { return ___TimeScale_9; }
	inline int32_t* get_address_of_TimeScale_9() { return &___TimeScale_9; }
	inline void set_TimeScale_9(int32_t value)
	{
		___TimeScale_9 = value;
	}

	inline static int32_t get_offset_of_DisableVolumeOnZeroWeight_10() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___DisableVolumeOnZeroWeight_10)); }
	inline bool get_DisableVolumeOnZeroWeight_10() const { return ___DisableVolumeOnZeroWeight_10; }
	inline bool* get_address_of_DisableVolumeOnZeroWeight_10() { return &___DisableVolumeOnZeroWeight_10; }
	inline void set_DisableVolumeOnZeroWeight_10(bool value)
	{
		___DisableVolumeOnZeroWeight_10 = value;
	}

	inline static int32_t get_offset_of_DisableSelfAfterEnd_11() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___DisableSelfAfterEnd_11)); }
	inline bool get_DisableSelfAfterEnd_11() const { return ___DisableSelfAfterEnd_11; }
	inline bool* get_address_of_DisableSelfAfterEnd_11() { return &___DisableSelfAfterEnd_11; }
	inline void set_DisableSelfAfterEnd_11(bool value)
	{
		___DisableSelfAfterEnd_11 = value;
	}

	inline static int32_t get_offset_of_Interruptable_12() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___Interruptable_12)); }
	inline bool get_Interruptable_12() const { return ___Interruptable_12; }
	inline bool* get_address_of_Interruptable_12() { return &___Interruptable_12; }
	inline void set_Interruptable_12(bool value)
	{
		___Interruptable_12 = value;
	}

	inline static int32_t get_offset_of_StartFromCurrentValue_13() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___StartFromCurrentValue_13)); }
	inline bool get_StartFromCurrentValue_13() const { return ___StartFromCurrentValue_13; }
	inline bool* get_address_of_StartFromCurrentValue_13() { return &___StartFromCurrentValue_13; }
	inline void set_StartFromCurrentValue_13(bool value)
	{
		___StartFromCurrentValue_13 = value;
	}

	inline static int32_t get_offset_of_ResetToInitialValueOnEnd_14() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___ResetToInitialValueOnEnd_14)); }
	inline bool get_ResetToInitialValueOnEnd_14() const { return ___ResetToInitialValueOnEnd_14; }
	inline bool* get_address_of_ResetToInitialValueOnEnd_14() { return &___ResetToInitialValueOnEnd_14; }
	inline void set_ResetToInitialValueOnEnd_14(bool value)
	{
		___ResetToInitialValueOnEnd_14 = value;
	}

	inline static int32_t get_offset_of_TestBlend_15() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___TestBlend_15)); }
	inline bool get_TestBlend_15() const { return ___TestBlend_15; }
	inline bool* get_address_of_TestBlend_15() { return &___TestBlend_15; }
	inline void set_TestBlend_15(bool value)
	{
		___TestBlend_15 = value;
	}

	inline static int32_t get_offset_of_TestBlendBackwards_16() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ___TestBlendBackwards_16)); }
	inline bool get_TestBlendBackwards_16() const { return ___TestBlendBackwards_16; }
	inline bool* get_address_of_TestBlendBackwards_16() { return &___TestBlendBackwards_16; }
	inline void set_TestBlendBackwards_16(bool value)
	{
		___TestBlendBackwards_16 = value;
	}

	inline static int32_t get_offset_of__initial_17() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ____initial_17)); }
	inline float get__initial_17() const { return ____initial_17; }
	inline float* get_address_of__initial_17() { return &____initial_17; }
	inline void set__initial_17(float value)
	{
		____initial_17 = value;
	}

	inline static int32_t get_offset_of__destination_18() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ____destination_18)); }
	inline float get__destination_18() const { return ____destination_18; }
	inline float* get_address_of__destination_18() { return &____destination_18; }
	inline void set__destination_18(float value)
	{
		____destination_18 = value;
	}

	inline static int32_t get_offset_of__startTime_19() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ____startTime_19)); }
	inline float get__startTime_19() const { return ____startTime_19; }
	inline float* get_address_of__startTime_19() { return &____startTime_19; }
	inline void set__startTime_19(float value)
	{
		____startTime_19 = value;
	}

	inline static int32_t get_offset_of__blending_20() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ____blending_20)); }
	inline bool get__blending_20() const { return ____blending_20; }
	inline bool* get_address_of__blending_20() { return &____blending_20; }
	inline void set__blending_20(bool value)
	{
		____blending_20 = value;
	}

	inline static int32_t get_offset_of__volume_21() { return static_cast<int32_t>(offsetof(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744, ____volume_21)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_21() const { return ____volume_21; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_21() { return &____volume_21; }
	inline void set__volume_21(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_21), (void*)value);
	}
};


// MoreMountains.Feedbacks.MMShaker
struct MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 MoreMountains.Feedbacks.MMShaker::Channel
	int32_t ___Channel_4;
	// System.Single MoreMountains.Feedbacks.MMShaker::ShakeDuration
	float ___ShakeDuration_5;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::PlayOnAwake
	bool ___PlayOnAwake_6;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::Interruptible
	bool ___Interruptible_7;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::AlwaysResetTargetValuesAfterShake
	bool ___AlwaysResetTargetValuesAfterShake_8;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::Shaking
	bool ___Shaking_9;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::ForwardDirection
	bool ___ForwardDirection_10;
	// MoreMountains.Feedbacks.TimescaleModes MoreMountains.Feedbacks.MMShaker::TimescaleMode
	int32_t ___TimescaleMode_11;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::_listeningToEvents
	bool ____listeningToEvents_12;
	// System.Single MoreMountains.Feedbacks.MMShaker::_shakeStartedTimestamp
	float ____shakeStartedTimestamp_13;
	// System.Single MoreMountains.Feedbacks.MMShaker::_remappedTimeSinceStart
	float ____remappedTimeSinceStart_14;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::_resetShakerValuesAfterShake
	bool ____resetShakerValuesAfterShake_15;
	// System.Boolean MoreMountains.Feedbacks.MMShaker::_resetTargetValuesAfterShake
	bool ____resetTargetValuesAfterShake_16;
	// System.Single MoreMountains.Feedbacks.MMShaker::_journey
	float ____journey_17;

public:
	inline static int32_t get_offset_of_Channel_4() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___Channel_4)); }
	inline int32_t get_Channel_4() const { return ___Channel_4; }
	inline int32_t* get_address_of_Channel_4() { return &___Channel_4; }
	inline void set_Channel_4(int32_t value)
	{
		___Channel_4 = value;
	}

	inline static int32_t get_offset_of_ShakeDuration_5() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___ShakeDuration_5)); }
	inline float get_ShakeDuration_5() const { return ___ShakeDuration_5; }
	inline float* get_address_of_ShakeDuration_5() { return &___ShakeDuration_5; }
	inline void set_ShakeDuration_5(float value)
	{
		___ShakeDuration_5 = value;
	}

	inline static int32_t get_offset_of_PlayOnAwake_6() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___PlayOnAwake_6)); }
	inline bool get_PlayOnAwake_6() const { return ___PlayOnAwake_6; }
	inline bool* get_address_of_PlayOnAwake_6() { return &___PlayOnAwake_6; }
	inline void set_PlayOnAwake_6(bool value)
	{
		___PlayOnAwake_6 = value;
	}

	inline static int32_t get_offset_of_Interruptible_7() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___Interruptible_7)); }
	inline bool get_Interruptible_7() const { return ___Interruptible_7; }
	inline bool* get_address_of_Interruptible_7() { return &___Interruptible_7; }
	inline void set_Interruptible_7(bool value)
	{
		___Interruptible_7 = value;
	}

	inline static int32_t get_offset_of_AlwaysResetTargetValuesAfterShake_8() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___AlwaysResetTargetValuesAfterShake_8)); }
	inline bool get_AlwaysResetTargetValuesAfterShake_8() const { return ___AlwaysResetTargetValuesAfterShake_8; }
	inline bool* get_address_of_AlwaysResetTargetValuesAfterShake_8() { return &___AlwaysResetTargetValuesAfterShake_8; }
	inline void set_AlwaysResetTargetValuesAfterShake_8(bool value)
	{
		___AlwaysResetTargetValuesAfterShake_8 = value;
	}

	inline static int32_t get_offset_of_Shaking_9() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___Shaking_9)); }
	inline bool get_Shaking_9() const { return ___Shaking_9; }
	inline bool* get_address_of_Shaking_9() { return &___Shaking_9; }
	inline void set_Shaking_9(bool value)
	{
		___Shaking_9 = value;
	}

	inline static int32_t get_offset_of_ForwardDirection_10() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___ForwardDirection_10)); }
	inline bool get_ForwardDirection_10() const { return ___ForwardDirection_10; }
	inline bool* get_address_of_ForwardDirection_10() { return &___ForwardDirection_10; }
	inline void set_ForwardDirection_10(bool value)
	{
		___ForwardDirection_10 = value;
	}

	inline static int32_t get_offset_of_TimescaleMode_11() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ___TimescaleMode_11)); }
	inline int32_t get_TimescaleMode_11() const { return ___TimescaleMode_11; }
	inline int32_t* get_address_of_TimescaleMode_11() { return &___TimescaleMode_11; }
	inline void set_TimescaleMode_11(int32_t value)
	{
		___TimescaleMode_11 = value;
	}

	inline static int32_t get_offset_of__listeningToEvents_12() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ____listeningToEvents_12)); }
	inline bool get__listeningToEvents_12() const { return ____listeningToEvents_12; }
	inline bool* get_address_of__listeningToEvents_12() { return &____listeningToEvents_12; }
	inline void set__listeningToEvents_12(bool value)
	{
		____listeningToEvents_12 = value;
	}

	inline static int32_t get_offset_of__shakeStartedTimestamp_13() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ____shakeStartedTimestamp_13)); }
	inline float get__shakeStartedTimestamp_13() const { return ____shakeStartedTimestamp_13; }
	inline float* get_address_of__shakeStartedTimestamp_13() { return &____shakeStartedTimestamp_13; }
	inline void set__shakeStartedTimestamp_13(float value)
	{
		____shakeStartedTimestamp_13 = value;
	}

	inline static int32_t get_offset_of__remappedTimeSinceStart_14() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ____remappedTimeSinceStart_14)); }
	inline float get__remappedTimeSinceStart_14() const { return ____remappedTimeSinceStart_14; }
	inline float* get_address_of__remappedTimeSinceStart_14() { return &____remappedTimeSinceStart_14; }
	inline void set__remappedTimeSinceStart_14(float value)
	{
		____remappedTimeSinceStart_14 = value;
	}

	inline static int32_t get_offset_of__resetShakerValuesAfterShake_15() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ____resetShakerValuesAfterShake_15)); }
	inline bool get__resetShakerValuesAfterShake_15() const { return ____resetShakerValuesAfterShake_15; }
	inline bool* get_address_of__resetShakerValuesAfterShake_15() { return &____resetShakerValuesAfterShake_15; }
	inline void set__resetShakerValuesAfterShake_15(bool value)
	{
		____resetShakerValuesAfterShake_15 = value;
	}

	inline static int32_t get_offset_of__resetTargetValuesAfterShake_16() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ____resetTargetValuesAfterShake_16)); }
	inline bool get__resetTargetValuesAfterShake_16() const { return ____resetTargetValuesAfterShake_16; }
	inline bool* get_address_of__resetTargetValuesAfterShake_16() { return &____resetTargetValuesAfterShake_16; }
	inline void set__resetTargetValuesAfterShake_16(bool value)
	{
		____resetTargetValuesAfterShake_16 = value;
	}

	inline static int32_t get_offset_of__journey_17() { return static_cast<int32_t>(offsetof(MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1, ____journey_17)); }
	inline float get__journey_17() const { return ____journey_17; }
	inline float* get_address_of__journey_17() { return &____journey_17; }
	inline void set__journey_17(float value)
	{
		____journey_17 = value;
	}
};


// UnityEngine.Rendering.PostProcessing.PostProcessVolume
struct PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::sharedProfile
	PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * ___sharedProfile_4;
	// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessVolume::isGlobal
	bool ___isGlobal_5;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::blendDistance
	float ___blendDistance_6;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::weight
	float ___weight_7;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::priority
	float ___priority_8;
	// System.Int32 UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_PreviousLayer
	int32_t ___m_PreviousLayer_9;
	// System.Single UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_PreviousPriority
	float ___m_PreviousPriority_10;
	// System.Collections.Generic.List`1<UnityEngine.Collider> UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_TempColliders
	List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * ___m_TempColliders_11;
	// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::m_InternalProfile
	PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * ___m_InternalProfile_12;

public:
	inline static int32_t get_offset_of_sharedProfile_4() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___sharedProfile_4)); }
	inline PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * get_sharedProfile_4() const { return ___sharedProfile_4; }
	inline PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E ** get_address_of_sharedProfile_4() { return &___sharedProfile_4; }
	inline void set_sharedProfile_4(PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * value)
	{
		___sharedProfile_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sharedProfile_4), (void*)value);
	}

	inline static int32_t get_offset_of_isGlobal_5() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___isGlobal_5)); }
	inline bool get_isGlobal_5() const { return ___isGlobal_5; }
	inline bool* get_address_of_isGlobal_5() { return &___isGlobal_5; }
	inline void set_isGlobal_5(bool value)
	{
		___isGlobal_5 = value;
	}

	inline static int32_t get_offset_of_blendDistance_6() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___blendDistance_6)); }
	inline float get_blendDistance_6() const { return ___blendDistance_6; }
	inline float* get_address_of_blendDistance_6() { return &___blendDistance_6; }
	inline void set_blendDistance_6(float value)
	{
		___blendDistance_6 = value;
	}

	inline static int32_t get_offset_of_weight_7() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___weight_7)); }
	inline float get_weight_7() const { return ___weight_7; }
	inline float* get_address_of_weight_7() { return &___weight_7; }
	inline void set_weight_7(float value)
	{
		___weight_7 = value;
	}

	inline static int32_t get_offset_of_priority_8() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___priority_8)); }
	inline float get_priority_8() const { return ___priority_8; }
	inline float* get_address_of_priority_8() { return &___priority_8; }
	inline void set_priority_8(float value)
	{
		___priority_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviousLayer_9() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___m_PreviousLayer_9)); }
	inline int32_t get_m_PreviousLayer_9() const { return ___m_PreviousLayer_9; }
	inline int32_t* get_address_of_m_PreviousLayer_9() { return &___m_PreviousLayer_9; }
	inline void set_m_PreviousLayer_9(int32_t value)
	{
		___m_PreviousLayer_9 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPriority_10() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___m_PreviousPriority_10)); }
	inline float get_m_PreviousPriority_10() const { return ___m_PreviousPriority_10; }
	inline float* get_address_of_m_PreviousPriority_10() { return &___m_PreviousPriority_10; }
	inline void set_m_PreviousPriority_10(float value)
	{
		___m_PreviousPriority_10 = value;
	}

	inline static int32_t get_offset_of_m_TempColliders_11() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___m_TempColliders_11)); }
	inline List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * get_m_TempColliders_11() const { return ___m_TempColliders_11; }
	inline List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B ** get_address_of_m_TempColliders_11() { return &___m_TempColliders_11; }
	inline void set_m_TempColliders_11(List_1_tA906B1694E65A6E14892A3A8A80F1A939222B15B * value)
	{
		___m_TempColliders_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempColliders_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_InternalProfile_12() { return static_cast<int32_t>(offsetof(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF, ___m_InternalProfile_12)); }
	inline PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * get_m_InternalProfile_12() const { return ___m_InternalProfile_12; }
	inline PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E ** get_address_of_m_InternalProfile_12() { return &___m_InternalProfile_12; }
	inline void set_m_InternalProfile_12(PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * value)
	{
		___m_InternalProfile_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InternalProfile_12), (void*)value);
	}
};


// MoreMountains.FeedbacksForThirdParty.MMBloomShaker
struct MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE  : public MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMBloomShaker::RelativeValues
	bool ___RelativeValues_18;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMBloomShaker::ShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeIntensity_19;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::RemapIntensityZero
	float ___RemapIntensityZero_20;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::RemapIntensityOne
	float ___RemapIntensityOne_21;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMBloomShaker::ShakeThreshold
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeThreshold_22;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::RemapThresholdZero
	float ___RemapThresholdZero_23;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::RemapThresholdOne
	float ___RemapThresholdOne_24;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_25;
	// UnityEngine.Rendering.PostProcessing.Bloom MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_bloom
	Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * ____bloom_26;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_initialIntensity
	float ____initialIntensity_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_initialThreshold
	float ____initialThreshold_28;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalShakeDuration
	float ____originalShakeDuration_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalRelativeIntensity
	bool ____originalRelativeIntensity_30;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeIntensity_31;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalRemapIntensityZero
	float ____originalRemapIntensityZero_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalRemapIntensityOne
	float ____originalRemapIntensityOne_33;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalShakeThreshold
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeThreshold_34;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalRemapThresholdZero
	float ____originalRemapThresholdZero_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMBloomShaker::_originalRemapThresholdOne
	float ____originalRemapThresholdOne_36;

public:
	inline static int32_t get_offset_of_RelativeValues_18() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___RelativeValues_18)); }
	inline bool get_RelativeValues_18() const { return ___RelativeValues_18; }
	inline bool* get_address_of_RelativeValues_18() { return &___RelativeValues_18; }
	inline void set_RelativeValues_18(bool value)
	{
		___RelativeValues_18 = value;
	}

	inline static int32_t get_offset_of_ShakeIntensity_19() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___ShakeIntensity_19)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeIntensity_19() const { return ___ShakeIntensity_19; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeIntensity_19() { return &___ShakeIntensity_19; }
	inline void set_ShakeIntensity_19(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeIntensity_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeIntensity_19), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_20() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___RemapIntensityZero_20)); }
	inline float get_RemapIntensityZero_20() const { return ___RemapIntensityZero_20; }
	inline float* get_address_of_RemapIntensityZero_20() { return &___RemapIntensityZero_20; }
	inline void set_RemapIntensityZero_20(float value)
	{
		___RemapIntensityZero_20 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_21() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___RemapIntensityOne_21)); }
	inline float get_RemapIntensityOne_21() const { return ___RemapIntensityOne_21; }
	inline float* get_address_of_RemapIntensityOne_21() { return &___RemapIntensityOne_21; }
	inline void set_RemapIntensityOne_21(float value)
	{
		___RemapIntensityOne_21 = value;
	}

	inline static int32_t get_offset_of_ShakeThreshold_22() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___ShakeThreshold_22)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeThreshold_22() const { return ___ShakeThreshold_22; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeThreshold_22() { return &___ShakeThreshold_22; }
	inline void set_ShakeThreshold_22(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeThreshold_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeThreshold_22), (void*)value);
	}

	inline static int32_t get_offset_of_RemapThresholdZero_23() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___RemapThresholdZero_23)); }
	inline float get_RemapThresholdZero_23() const { return ___RemapThresholdZero_23; }
	inline float* get_address_of_RemapThresholdZero_23() { return &___RemapThresholdZero_23; }
	inline void set_RemapThresholdZero_23(float value)
	{
		___RemapThresholdZero_23 = value;
	}

	inline static int32_t get_offset_of_RemapThresholdOne_24() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ___RemapThresholdOne_24)); }
	inline float get_RemapThresholdOne_24() const { return ___RemapThresholdOne_24; }
	inline float* get_address_of_RemapThresholdOne_24() { return &___RemapThresholdOne_24; }
	inline void set_RemapThresholdOne_24(float value)
	{
		___RemapThresholdOne_24 = value;
	}

	inline static int32_t get_offset_of__volume_25() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____volume_25)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_25() const { return ____volume_25; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_25() { return &____volume_25; }
	inline void set__volume_25(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_25), (void*)value);
	}

	inline static int32_t get_offset_of__bloom_26() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____bloom_26)); }
	inline Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * get__bloom_26() const { return ____bloom_26; }
	inline Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 ** get_address_of__bloom_26() { return &____bloom_26; }
	inline void set__bloom_26(Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * value)
	{
		____bloom_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bloom_26), (void*)value);
	}

	inline static int32_t get_offset_of__initialIntensity_27() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____initialIntensity_27)); }
	inline float get__initialIntensity_27() const { return ____initialIntensity_27; }
	inline float* get_address_of__initialIntensity_27() { return &____initialIntensity_27; }
	inline void set__initialIntensity_27(float value)
	{
		____initialIntensity_27 = value;
	}

	inline static int32_t get_offset_of__initialThreshold_28() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____initialThreshold_28)); }
	inline float get__initialThreshold_28() const { return ____initialThreshold_28; }
	inline float* get_address_of__initialThreshold_28() { return &____initialThreshold_28; }
	inline void set__initialThreshold_28(float value)
	{
		____initialThreshold_28 = value;
	}

	inline static int32_t get_offset_of__originalShakeDuration_29() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalShakeDuration_29)); }
	inline float get__originalShakeDuration_29() const { return ____originalShakeDuration_29; }
	inline float* get_address_of__originalShakeDuration_29() { return &____originalShakeDuration_29; }
	inline void set__originalShakeDuration_29(float value)
	{
		____originalShakeDuration_29 = value;
	}

	inline static int32_t get_offset_of__originalRelativeIntensity_30() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalRelativeIntensity_30)); }
	inline bool get__originalRelativeIntensity_30() const { return ____originalRelativeIntensity_30; }
	inline bool* get_address_of__originalRelativeIntensity_30() { return &____originalRelativeIntensity_30; }
	inline void set__originalRelativeIntensity_30(bool value)
	{
		____originalRelativeIntensity_30 = value;
	}

	inline static int32_t get_offset_of__originalShakeIntensity_31() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalShakeIntensity_31)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeIntensity_31() const { return ____originalShakeIntensity_31; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeIntensity_31() { return &____originalShakeIntensity_31; }
	inline void set__originalShakeIntensity_31(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeIntensity_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeIntensity_31), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapIntensityZero_32() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalRemapIntensityZero_32)); }
	inline float get__originalRemapIntensityZero_32() const { return ____originalRemapIntensityZero_32; }
	inline float* get_address_of__originalRemapIntensityZero_32() { return &____originalRemapIntensityZero_32; }
	inline void set__originalRemapIntensityZero_32(float value)
	{
		____originalRemapIntensityZero_32 = value;
	}

	inline static int32_t get_offset_of__originalRemapIntensityOne_33() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalRemapIntensityOne_33)); }
	inline float get__originalRemapIntensityOne_33() const { return ____originalRemapIntensityOne_33; }
	inline float* get_address_of__originalRemapIntensityOne_33() { return &____originalRemapIntensityOne_33; }
	inline void set__originalRemapIntensityOne_33(float value)
	{
		____originalRemapIntensityOne_33 = value;
	}

	inline static int32_t get_offset_of__originalShakeThreshold_34() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalShakeThreshold_34)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeThreshold_34() const { return ____originalShakeThreshold_34; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeThreshold_34() { return &____originalShakeThreshold_34; }
	inline void set__originalShakeThreshold_34(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeThreshold_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeThreshold_34), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapThresholdZero_35() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalRemapThresholdZero_35)); }
	inline float get__originalRemapThresholdZero_35() const { return ____originalRemapThresholdZero_35; }
	inline float* get_address_of__originalRemapThresholdZero_35() { return &____originalRemapThresholdZero_35; }
	inline void set__originalRemapThresholdZero_35(float value)
	{
		____originalRemapThresholdZero_35 = value;
	}

	inline static int32_t get_offset_of__originalRemapThresholdOne_36() { return static_cast<int32_t>(offsetof(MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE, ____originalRemapThresholdOne_36)); }
	inline float get__originalRemapThresholdOne_36() const { return ____originalRemapThresholdOne_36; }
	inline float* get_address_of__originalRemapThresholdOne_36() { return &____originalRemapThresholdOne_36; }
	inline void set__originalRemapThresholdOne_36(float value)
	{
		____originalRemapThresholdOne_36 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker
struct MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6  : public MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::RelativeIntensity
	bool ___RelativeIntensity_18;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::ShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeIntensity_19;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::RemapIntensityZero
	float ___RemapIntensityZero_20;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::RemapIntensityOne
	float ___RemapIntensityOne_21;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_22;
	// UnityEngine.Rendering.PostProcessing.ChromaticAberration MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_chromaticAberration
	ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 * ____chromaticAberration_23;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_initialIntensity
	float ____initialIntensity_24;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_originalShakeDuration
	float ____originalShakeDuration_25;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_originalShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeIntensity_26;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_originalRemapIntensityZero
	float ____originalRemapIntensityZero_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_originalRemapIntensityOne
	float ____originalRemapIntensityOne_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::_originalRelativeIntensity
	bool ____originalRelativeIntensity_29;

public:
	inline static int32_t get_offset_of_RelativeIntensity_18() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ___RelativeIntensity_18)); }
	inline bool get_RelativeIntensity_18() const { return ___RelativeIntensity_18; }
	inline bool* get_address_of_RelativeIntensity_18() { return &___RelativeIntensity_18; }
	inline void set_RelativeIntensity_18(bool value)
	{
		___RelativeIntensity_18 = value;
	}

	inline static int32_t get_offset_of_ShakeIntensity_19() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ___ShakeIntensity_19)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeIntensity_19() const { return ___ShakeIntensity_19; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeIntensity_19() { return &___ShakeIntensity_19; }
	inline void set_ShakeIntensity_19(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeIntensity_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeIntensity_19), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_20() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ___RemapIntensityZero_20)); }
	inline float get_RemapIntensityZero_20() const { return ___RemapIntensityZero_20; }
	inline float* get_address_of_RemapIntensityZero_20() { return &___RemapIntensityZero_20; }
	inline void set_RemapIntensityZero_20(float value)
	{
		___RemapIntensityZero_20 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_21() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ___RemapIntensityOne_21)); }
	inline float get_RemapIntensityOne_21() const { return ___RemapIntensityOne_21; }
	inline float* get_address_of_RemapIntensityOne_21() { return &___RemapIntensityOne_21; }
	inline void set_RemapIntensityOne_21(float value)
	{
		___RemapIntensityOne_21 = value;
	}

	inline static int32_t get_offset_of__volume_22() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____volume_22)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_22() const { return ____volume_22; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_22() { return &____volume_22; }
	inline void set__volume_22(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_22), (void*)value);
	}

	inline static int32_t get_offset_of__chromaticAberration_23() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____chromaticAberration_23)); }
	inline ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 * get__chromaticAberration_23() const { return ____chromaticAberration_23; }
	inline ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 ** get_address_of__chromaticAberration_23() { return &____chromaticAberration_23; }
	inline void set__chromaticAberration_23(ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 * value)
	{
		____chromaticAberration_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____chromaticAberration_23), (void*)value);
	}

	inline static int32_t get_offset_of__initialIntensity_24() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____initialIntensity_24)); }
	inline float get__initialIntensity_24() const { return ____initialIntensity_24; }
	inline float* get_address_of__initialIntensity_24() { return &____initialIntensity_24; }
	inline void set__initialIntensity_24(float value)
	{
		____initialIntensity_24 = value;
	}

	inline static int32_t get_offset_of__originalShakeDuration_25() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____originalShakeDuration_25)); }
	inline float get__originalShakeDuration_25() const { return ____originalShakeDuration_25; }
	inline float* get_address_of__originalShakeDuration_25() { return &____originalShakeDuration_25; }
	inline void set__originalShakeDuration_25(float value)
	{
		____originalShakeDuration_25 = value;
	}

	inline static int32_t get_offset_of__originalShakeIntensity_26() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____originalShakeIntensity_26)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeIntensity_26() const { return ____originalShakeIntensity_26; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeIntensity_26() { return &____originalShakeIntensity_26; }
	inline void set__originalShakeIntensity_26(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeIntensity_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeIntensity_26), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapIntensityZero_27() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____originalRemapIntensityZero_27)); }
	inline float get__originalRemapIntensityZero_27() const { return ____originalRemapIntensityZero_27; }
	inline float* get_address_of__originalRemapIntensityZero_27() { return &____originalRemapIntensityZero_27; }
	inline void set__originalRemapIntensityZero_27(float value)
	{
		____originalRemapIntensityZero_27 = value;
	}

	inline static int32_t get_offset_of__originalRemapIntensityOne_28() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____originalRemapIntensityOne_28)); }
	inline float get__originalRemapIntensityOne_28() const { return ____originalRemapIntensityOne_28; }
	inline float* get_address_of__originalRemapIntensityOne_28() { return &____originalRemapIntensityOne_28; }
	inline void set__originalRemapIntensityOne_28(float value)
	{
		____originalRemapIntensityOne_28 = value;
	}

	inline static int32_t get_offset_of__originalRelativeIntensity_29() { return static_cast<int32_t>(offsetof(MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6, ____originalRelativeIntensity_29)); }
	inline bool get__originalRelativeIntensity_29() const { return ____originalRelativeIntensity_29; }
	inline bool* get_address_of__originalRelativeIntensity_29() { return &____originalRelativeIntensity_29; }
	inline void set__originalRelativeIntensity_29(bool value)
	{
		____originalRelativeIntensity_29 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker
struct MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9  : public MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RelativeValues
	bool ___RelativeValues_18;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ShakePostExposure
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakePostExposure_19;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapPostExposureZero
	float ___RemapPostExposureZero_20;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapPostExposureOne
	float ___RemapPostExposureOne_21;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ShakeHueShift
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeHueShift_22;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapHueShiftZero
	float ___RemapHueShiftZero_23;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapHueShiftOne
	float ___RemapHueShiftOne_24;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ShakeSaturation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeSaturation_25;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapSaturationZero
	float ___RemapSaturationZero_26;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapSaturationOne
	float ___RemapSaturationOne_27;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ShakeContrast
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeContrast_28;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapContrastZero
	float ___RemapContrastZero_29;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::RemapContrastOne
	float ___RemapContrastOne_30;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_31;
	// UnityEngine.Rendering.PostProcessing.ColorGrading MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_colorGrading
	ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * ____colorGrading_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_initialPostExposure
	float ____initialPostExposure_33;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_initialHueShift
	float ____initialHueShift_34;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_initialSaturation
	float ____initialSaturation_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_initialContrast
	float ____initialContrast_36;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalShakeDuration
	float ____originalShakeDuration_37;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRelativeValues
	bool ____originalRelativeValues_38;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalShakePostExposure
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakePostExposure_39;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapPostExposureZero
	float ____originalRemapPostExposureZero_40;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapPostExposureOne
	float ____originalRemapPostExposureOne_41;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalShakeHueShift
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeHueShift_42;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapHueShiftZero
	float ____originalRemapHueShiftZero_43;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapHueShiftOne
	float ____originalRemapHueShiftOne_44;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalShakeSaturation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeSaturation_45;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapSaturationZero
	float ____originalRemapSaturationZero_46;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapSaturationOne
	float ____originalRemapSaturationOne_47;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalShakeContrast
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeContrast_48;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapContrastZero
	float ____originalRemapContrastZero_49;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::_originalRemapContrastOne
	float ____originalRemapContrastOne_50;

public:
	inline static int32_t get_offset_of_RelativeValues_18() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RelativeValues_18)); }
	inline bool get_RelativeValues_18() const { return ___RelativeValues_18; }
	inline bool* get_address_of_RelativeValues_18() { return &___RelativeValues_18; }
	inline void set_RelativeValues_18(bool value)
	{
		___RelativeValues_18 = value;
	}

	inline static int32_t get_offset_of_ShakePostExposure_19() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___ShakePostExposure_19)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakePostExposure_19() const { return ___ShakePostExposure_19; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakePostExposure_19() { return &___ShakePostExposure_19; }
	inline void set_ShakePostExposure_19(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakePostExposure_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakePostExposure_19), (void*)value);
	}

	inline static int32_t get_offset_of_RemapPostExposureZero_20() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapPostExposureZero_20)); }
	inline float get_RemapPostExposureZero_20() const { return ___RemapPostExposureZero_20; }
	inline float* get_address_of_RemapPostExposureZero_20() { return &___RemapPostExposureZero_20; }
	inline void set_RemapPostExposureZero_20(float value)
	{
		___RemapPostExposureZero_20 = value;
	}

	inline static int32_t get_offset_of_RemapPostExposureOne_21() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapPostExposureOne_21)); }
	inline float get_RemapPostExposureOne_21() const { return ___RemapPostExposureOne_21; }
	inline float* get_address_of_RemapPostExposureOne_21() { return &___RemapPostExposureOne_21; }
	inline void set_RemapPostExposureOne_21(float value)
	{
		___RemapPostExposureOne_21 = value;
	}

	inline static int32_t get_offset_of_ShakeHueShift_22() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___ShakeHueShift_22)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeHueShift_22() const { return ___ShakeHueShift_22; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeHueShift_22() { return &___ShakeHueShift_22; }
	inline void set_ShakeHueShift_22(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeHueShift_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeHueShift_22), (void*)value);
	}

	inline static int32_t get_offset_of_RemapHueShiftZero_23() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapHueShiftZero_23)); }
	inline float get_RemapHueShiftZero_23() const { return ___RemapHueShiftZero_23; }
	inline float* get_address_of_RemapHueShiftZero_23() { return &___RemapHueShiftZero_23; }
	inline void set_RemapHueShiftZero_23(float value)
	{
		___RemapHueShiftZero_23 = value;
	}

	inline static int32_t get_offset_of_RemapHueShiftOne_24() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapHueShiftOne_24)); }
	inline float get_RemapHueShiftOne_24() const { return ___RemapHueShiftOne_24; }
	inline float* get_address_of_RemapHueShiftOne_24() { return &___RemapHueShiftOne_24; }
	inline void set_RemapHueShiftOne_24(float value)
	{
		___RemapHueShiftOne_24 = value;
	}

	inline static int32_t get_offset_of_ShakeSaturation_25() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___ShakeSaturation_25)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeSaturation_25() const { return ___ShakeSaturation_25; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeSaturation_25() { return &___ShakeSaturation_25; }
	inline void set_ShakeSaturation_25(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeSaturation_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeSaturation_25), (void*)value);
	}

	inline static int32_t get_offset_of_RemapSaturationZero_26() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapSaturationZero_26)); }
	inline float get_RemapSaturationZero_26() const { return ___RemapSaturationZero_26; }
	inline float* get_address_of_RemapSaturationZero_26() { return &___RemapSaturationZero_26; }
	inline void set_RemapSaturationZero_26(float value)
	{
		___RemapSaturationZero_26 = value;
	}

	inline static int32_t get_offset_of_RemapSaturationOne_27() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapSaturationOne_27)); }
	inline float get_RemapSaturationOne_27() const { return ___RemapSaturationOne_27; }
	inline float* get_address_of_RemapSaturationOne_27() { return &___RemapSaturationOne_27; }
	inline void set_RemapSaturationOne_27(float value)
	{
		___RemapSaturationOne_27 = value;
	}

	inline static int32_t get_offset_of_ShakeContrast_28() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___ShakeContrast_28)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeContrast_28() const { return ___ShakeContrast_28; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeContrast_28() { return &___ShakeContrast_28; }
	inline void set_ShakeContrast_28(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeContrast_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeContrast_28), (void*)value);
	}

	inline static int32_t get_offset_of_RemapContrastZero_29() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapContrastZero_29)); }
	inline float get_RemapContrastZero_29() const { return ___RemapContrastZero_29; }
	inline float* get_address_of_RemapContrastZero_29() { return &___RemapContrastZero_29; }
	inline void set_RemapContrastZero_29(float value)
	{
		___RemapContrastZero_29 = value;
	}

	inline static int32_t get_offset_of_RemapContrastOne_30() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ___RemapContrastOne_30)); }
	inline float get_RemapContrastOne_30() const { return ___RemapContrastOne_30; }
	inline float* get_address_of_RemapContrastOne_30() { return &___RemapContrastOne_30; }
	inline void set_RemapContrastOne_30(float value)
	{
		___RemapContrastOne_30 = value;
	}

	inline static int32_t get_offset_of__volume_31() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____volume_31)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_31() const { return ____volume_31; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_31() { return &____volume_31; }
	inline void set__volume_31(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_31), (void*)value);
	}

	inline static int32_t get_offset_of__colorGrading_32() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____colorGrading_32)); }
	inline ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * get__colorGrading_32() const { return ____colorGrading_32; }
	inline ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 ** get_address_of__colorGrading_32() { return &____colorGrading_32; }
	inline void set__colorGrading_32(ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * value)
	{
		____colorGrading_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____colorGrading_32), (void*)value);
	}

	inline static int32_t get_offset_of__initialPostExposure_33() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____initialPostExposure_33)); }
	inline float get__initialPostExposure_33() const { return ____initialPostExposure_33; }
	inline float* get_address_of__initialPostExposure_33() { return &____initialPostExposure_33; }
	inline void set__initialPostExposure_33(float value)
	{
		____initialPostExposure_33 = value;
	}

	inline static int32_t get_offset_of__initialHueShift_34() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____initialHueShift_34)); }
	inline float get__initialHueShift_34() const { return ____initialHueShift_34; }
	inline float* get_address_of__initialHueShift_34() { return &____initialHueShift_34; }
	inline void set__initialHueShift_34(float value)
	{
		____initialHueShift_34 = value;
	}

	inline static int32_t get_offset_of__initialSaturation_35() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____initialSaturation_35)); }
	inline float get__initialSaturation_35() const { return ____initialSaturation_35; }
	inline float* get_address_of__initialSaturation_35() { return &____initialSaturation_35; }
	inline void set__initialSaturation_35(float value)
	{
		____initialSaturation_35 = value;
	}

	inline static int32_t get_offset_of__initialContrast_36() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____initialContrast_36)); }
	inline float get__initialContrast_36() const { return ____initialContrast_36; }
	inline float* get_address_of__initialContrast_36() { return &____initialContrast_36; }
	inline void set__initialContrast_36(float value)
	{
		____initialContrast_36 = value;
	}

	inline static int32_t get_offset_of__originalShakeDuration_37() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalShakeDuration_37)); }
	inline float get__originalShakeDuration_37() const { return ____originalShakeDuration_37; }
	inline float* get_address_of__originalShakeDuration_37() { return &____originalShakeDuration_37; }
	inline void set__originalShakeDuration_37(float value)
	{
		____originalShakeDuration_37 = value;
	}

	inline static int32_t get_offset_of__originalRelativeValues_38() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRelativeValues_38)); }
	inline bool get__originalRelativeValues_38() const { return ____originalRelativeValues_38; }
	inline bool* get_address_of__originalRelativeValues_38() { return &____originalRelativeValues_38; }
	inline void set__originalRelativeValues_38(bool value)
	{
		____originalRelativeValues_38 = value;
	}

	inline static int32_t get_offset_of__originalShakePostExposure_39() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalShakePostExposure_39)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakePostExposure_39() const { return ____originalShakePostExposure_39; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakePostExposure_39() { return &____originalShakePostExposure_39; }
	inline void set__originalShakePostExposure_39(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakePostExposure_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakePostExposure_39), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapPostExposureZero_40() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapPostExposureZero_40)); }
	inline float get__originalRemapPostExposureZero_40() const { return ____originalRemapPostExposureZero_40; }
	inline float* get_address_of__originalRemapPostExposureZero_40() { return &____originalRemapPostExposureZero_40; }
	inline void set__originalRemapPostExposureZero_40(float value)
	{
		____originalRemapPostExposureZero_40 = value;
	}

	inline static int32_t get_offset_of__originalRemapPostExposureOne_41() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapPostExposureOne_41)); }
	inline float get__originalRemapPostExposureOne_41() const { return ____originalRemapPostExposureOne_41; }
	inline float* get_address_of__originalRemapPostExposureOne_41() { return &____originalRemapPostExposureOne_41; }
	inline void set__originalRemapPostExposureOne_41(float value)
	{
		____originalRemapPostExposureOne_41 = value;
	}

	inline static int32_t get_offset_of__originalShakeHueShift_42() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalShakeHueShift_42)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeHueShift_42() const { return ____originalShakeHueShift_42; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeHueShift_42() { return &____originalShakeHueShift_42; }
	inline void set__originalShakeHueShift_42(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeHueShift_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeHueShift_42), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapHueShiftZero_43() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapHueShiftZero_43)); }
	inline float get__originalRemapHueShiftZero_43() const { return ____originalRemapHueShiftZero_43; }
	inline float* get_address_of__originalRemapHueShiftZero_43() { return &____originalRemapHueShiftZero_43; }
	inline void set__originalRemapHueShiftZero_43(float value)
	{
		____originalRemapHueShiftZero_43 = value;
	}

	inline static int32_t get_offset_of__originalRemapHueShiftOne_44() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapHueShiftOne_44)); }
	inline float get__originalRemapHueShiftOne_44() const { return ____originalRemapHueShiftOne_44; }
	inline float* get_address_of__originalRemapHueShiftOne_44() { return &____originalRemapHueShiftOne_44; }
	inline void set__originalRemapHueShiftOne_44(float value)
	{
		____originalRemapHueShiftOne_44 = value;
	}

	inline static int32_t get_offset_of__originalShakeSaturation_45() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalShakeSaturation_45)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeSaturation_45() const { return ____originalShakeSaturation_45; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeSaturation_45() { return &____originalShakeSaturation_45; }
	inline void set__originalShakeSaturation_45(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeSaturation_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeSaturation_45), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapSaturationZero_46() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapSaturationZero_46)); }
	inline float get__originalRemapSaturationZero_46() const { return ____originalRemapSaturationZero_46; }
	inline float* get_address_of__originalRemapSaturationZero_46() { return &____originalRemapSaturationZero_46; }
	inline void set__originalRemapSaturationZero_46(float value)
	{
		____originalRemapSaturationZero_46 = value;
	}

	inline static int32_t get_offset_of__originalRemapSaturationOne_47() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapSaturationOne_47)); }
	inline float get__originalRemapSaturationOne_47() const { return ____originalRemapSaturationOne_47; }
	inline float* get_address_of__originalRemapSaturationOne_47() { return &____originalRemapSaturationOne_47; }
	inline void set__originalRemapSaturationOne_47(float value)
	{
		____originalRemapSaturationOne_47 = value;
	}

	inline static int32_t get_offset_of__originalShakeContrast_48() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalShakeContrast_48)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeContrast_48() const { return ____originalShakeContrast_48; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeContrast_48() { return &____originalShakeContrast_48; }
	inline void set__originalShakeContrast_48(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeContrast_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeContrast_48), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapContrastZero_49() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapContrastZero_49)); }
	inline float get__originalRemapContrastZero_49() const { return ____originalRemapContrastZero_49; }
	inline float* get_address_of__originalRemapContrastZero_49() { return &____originalRemapContrastZero_49; }
	inline void set__originalRemapContrastZero_49(float value)
	{
		____originalRemapContrastZero_49 = value;
	}

	inline static int32_t get_offset_of__originalRemapContrastOne_50() { return static_cast<int32_t>(offsetof(MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9, ____originalRemapContrastOne_50)); }
	inline float get__originalRemapContrastOne_50() const { return ____originalRemapContrastOne_50; }
	inline float* get_address_of__originalRemapContrastOne_50() { return &____originalRemapContrastOne_50; }
	inline void set__originalRemapContrastOne_50(float value)
	{
		____originalRemapContrastOne_50 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker
struct MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645  : public MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RelativeValues
	bool ___RelativeValues_18;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ShakeFocusDistance
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeFocusDistance_19;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RemapFocusDistanceZero
	float ___RemapFocusDistanceZero_20;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RemapFocusDistanceOne
	float ___RemapFocusDistanceOne_21;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ShakeAperture
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeAperture_22;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RemapApertureZero
	float ___RemapApertureZero_23;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RemapApertureOne
	float ___RemapApertureOne_24;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ShakeFocalLength
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeFocalLength_25;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RemapFocalLengthZero
	float ___RemapFocalLengthZero_26;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::RemapFocalLengthOne
	float ___RemapFocalLengthOne_27;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_28;
	// UnityEngine.Rendering.PostProcessing.DepthOfField MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_depthOfField
	DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * ____depthOfField_29;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_initialFocusDistance
	float ____initialFocusDistance_30;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_initialAperture
	float ____initialAperture_31;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_initialFocalLength
	float ____initialFocalLength_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalShakeDuration
	float ____originalShakeDuration_33;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRelativeValues
	bool ____originalRelativeValues_34;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalShakeFocusDistance
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeFocusDistance_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRemapFocusDistanceZero
	float ____originalRemapFocusDistanceZero_36;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRemapFocusDistanceOne
	float ____originalRemapFocusDistanceOne_37;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalShakeAperture
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeAperture_38;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRemapApertureZero
	float ____originalRemapApertureZero_39;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRemapApertureOne
	float ____originalRemapApertureOne_40;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalShakeFocalLength
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeFocalLength_41;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRemapFocalLengthZero
	float ____originalRemapFocalLengthZero_42;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::_originalRemapFocalLengthOne
	float ____originalRemapFocalLengthOne_43;

public:
	inline static int32_t get_offset_of_RelativeValues_18() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RelativeValues_18)); }
	inline bool get_RelativeValues_18() const { return ___RelativeValues_18; }
	inline bool* get_address_of_RelativeValues_18() { return &___RelativeValues_18; }
	inline void set_RelativeValues_18(bool value)
	{
		___RelativeValues_18 = value;
	}

	inline static int32_t get_offset_of_ShakeFocusDistance_19() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___ShakeFocusDistance_19)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeFocusDistance_19() const { return ___ShakeFocusDistance_19; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeFocusDistance_19() { return &___ShakeFocusDistance_19; }
	inline void set_ShakeFocusDistance_19(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeFocusDistance_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeFocusDistance_19), (void*)value);
	}

	inline static int32_t get_offset_of_RemapFocusDistanceZero_20() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RemapFocusDistanceZero_20)); }
	inline float get_RemapFocusDistanceZero_20() const { return ___RemapFocusDistanceZero_20; }
	inline float* get_address_of_RemapFocusDistanceZero_20() { return &___RemapFocusDistanceZero_20; }
	inline void set_RemapFocusDistanceZero_20(float value)
	{
		___RemapFocusDistanceZero_20 = value;
	}

	inline static int32_t get_offset_of_RemapFocusDistanceOne_21() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RemapFocusDistanceOne_21)); }
	inline float get_RemapFocusDistanceOne_21() const { return ___RemapFocusDistanceOne_21; }
	inline float* get_address_of_RemapFocusDistanceOne_21() { return &___RemapFocusDistanceOne_21; }
	inline void set_RemapFocusDistanceOne_21(float value)
	{
		___RemapFocusDistanceOne_21 = value;
	}

	inline static int32_t get_offset_of_ShakeAperture_22() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___ShakeAperture_22)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeAperture_22() const { return ___ShakeAperture_22; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeAperture_22() { return &___ShakeAperture_22; }
	inline void set_ShakeAperture_22(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeAperture_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeAperture_22), (void*)value);
	}

	inline static int32_t get_offset_of_RemapApertureZero_23() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RemapApertureZero_23)); }
	inline float get_RemapApertureZero_23() const { return ___RemapApertureZero_23; }
	inline float* get_address_of_RemapApertureZero_23() { return &___RemapApertureZero_23; }
	inline void set_RemapApertureZero_23(float value)
	{
		___RemapApertureZero_23 = value;
	}

	inline static int32_t get_offset_of_RemapApertureOne_24() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RemapApertureOne_24)); }
	inline float get_RemapApertureOne_24() const { return ___RemapApertureOne_24; }
	inline float* get_address_of_RemapApertureOne_24() { return &___RemapApertureOne_24; }
	inline void set_RemapApertureOne_24(float value)
	{
		___RemapApertureOne_24 = value;
	}

	inline static int32_t get_offset_of_ShakeFocalLength_25() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___ShakeFocalLength_25)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeFocalLength_25() const { return ___ShakeFocalLength_25; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeFocalLength_25() { return &___ShakeFocalLength_25; }
	inline void set_ShakeFocalLength_25(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeFocalLength_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeFocalLength_25), (void*)value);
	}

	inline static int32_t get_offset_of_RemapFocalLengthZero_26() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RemapFocalLengthZero_26)); }
	inline float get_RemapFocalLengthZero_26() const { return ___RemapFocalLengthZero_26; }
	inline float* get_address_of_RemapFocalLengthZero_26() { return &___RemapFocalLengthZero_26; }
	inline void set_RemapFocalLengthZero_26(float value)
	{
		___RemapFocalLengthZero_26 = value;
	}

	inline static int32_t get_offset_of_RemapFocalLengthOne_27() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ___RemapFocalLengthOne_27)); }
	inline float get_RemapFocalLengthOne_27() const { return ___RemapFocalLengthOne_27; }
	inline float* get_address_of_RemapFocalLengthOne_27() { return &___RemapFocalLengthOne_27; }
	inline void set_RemapFocalLengthOne_27(float value)
	{
		___RemapFocalLengthOne_27 = value;
	}

	inline static int32_t get_offset_of__volume_28() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____volume_28)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_28() const { return ____volume_28; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_28() { return &____volume_28; }
	inline void set__volume_28(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_28), (void*)value);
	}

	inline static int32_t get_offset_of__depthOfField_29() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____depthOfField_29)); }
	inline DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * get__depthOfField_29() const { return ____depthOfField_29; }
	inline DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 ** get_address_of__depthOfField_29() { return &____depthOfField_29; }
	inline void set__depthOfField_29(DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * value)
	{
		____depthOfField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____depthOfField_29), (void*)value);
	}

	inline static int32_t get_offset_of__initialFocusDistance_30() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____initialFocusDistance_30)); }
	inline float get__initialFocusDistance_30() const { return ____initialFocusDistance_30; }
	inline float* get_address_of__initialFocusDistance_30() { return &____initialFocusDistance_30; }
	inline void set__initialFocusDistance_30(float value)
	{
		____initialFocusDistance_30 = value;
	}

	inline static int32_t get_offset_of__initialAperture_31() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____initialAperture_31)); }
	inline float get__initialAperture_31() const { return ____initialAperture_31; }
	inline float* get_address_of__initialAperture_31() { return &____initialAperture_31; }
	inline void set__initialAperture_31(float value)
	{
		____initialAperture_31 = value;
	}

	inline static int32_t get_offset_of__initialFocalLength_32() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____initialFocalLength_32)); }
	inline float get__initialFocalLength_32() const { return ____initialFocalLength_32; }
	inline float* get_address_of__initialFocalLength_32() { return &____initialFocalLength_32; }
	inline void set__initialFocalLength_32(float value)
	{
		____initialFocalLength_32 = value;
	}

	inline static int32_t get_offset_of__originalShakeDuration_33() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalShakeDuration_33)); }
	inline float get__originalShakeDuration_33() const { return ____originalShakeDuration_33; }
	inline float* get_address_of__originalShakeDuration_33() { return &____originalShakeDuration_33; }
	inline void set__originalShakeDuration_33(float value)
	{
		____originalShakeDuration_33 = value;
	}

	inline static int32_t get_offset_of__originalRelativeValues_34() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRelativeValues_34)); }
	inline bool get__originalRelativeValues_34() const { return ____originalRelativeValues_34; }
	inline bool* get_address_of__originalRelativeValues_34() { return &____originalRelativeValues_34; }
	inline void set__originalRelativeValues_34(bool value)
	{
		____originalRelativeValues_34 = value;
	}

	inline static int32_t get_offset_of__originalShakeFocusDistance_35() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalShakeFocusDistance_35)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeFocusDistance_35() const { return ____originalShakeFocusDistance_35; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeFocusDistance_35() { return &____originalShakeFocusDistance_35; }
	inline void set__originalShakeFocusDistance_35(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeFocusDistance_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeFocusDistance_35), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapFocusDistanceZero_36() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRemapFocusDistanceZero_36)); }
	inline float get__originalRemapFocusDistanceZero_36() const { return ____originalRemapFocusDistanceZero_36; }
	inline float* get_address_of__originalRemapFocusDistanceZero_36() { return &____originalRemapFocusDistanceZero_36; }
	inline void set__originalRemapFocusDistanceZero_36(float value)
	{
		____originalRemapFocusDistanceZero_36 = value;
	}

	inline static int32_t get_offset_of__originalRemapFocusDistanceOne_37() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRemapFocusDistanceOne_37)); }
	inline float get__originalRemapFocusDistanceOne_37() const { return ____originalRemapFocusDistanceOne_37; }
	inline float* get_address_of__originalRemapFocusDistanceOne_37() { return &____originalRemapFocusDistanceOne_37; }
	inline void set__originalRemapFocusDistanceOne_37(float value)
	{
		____originalRemapFocusDistanceOne_37 = value;
	}

	inline static int32_t get_offset_of__originalShakeAperture_38() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalShakeAperture_38)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeAperture_38() const { return ____originalShakeAperture_38; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeAperture_38() { return &____originalShakeAperture_38; }
	inline void set__originalShakeAperture_38(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeAperture_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeAperture_38), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapApertureZero_39() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRemapApertureZero_39)); }
	inline float get__originalRemapApertureZero_39() const { return ____originalRemapApertureZero_39; }
	inline float* get_address_of__originalRemapApertureZero_39() { return &____originalRemapApertureZero_39; }
	inline void set__originalRemapApertureZero_39(float value)
	{
		____originalRemapApertureZero_39 = value;
	}

	inline static int32_t get_offset_of__originalRemapApertureOne_40() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRemapApertureOne_40)); }
	inline float get__originalRemapApertureOne_40() const { return ____originalRemapApertureOne_40; }
	inline float* get_address_of__originalRemapApertureOne_40() { return &____originalRemapApertureOne_40; }
	inline void set__originalRemapApertureOne_40(float value)
	{
		____originalRemapApertureOne_40 = value;
	}

	inline static int32_t get_offset_of__originalShakeFocalLength_41() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalShakeFocalLength_41)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeFocalLength_41() const { return ____originalShakeFocalLength_41; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeFocalLength_41() { return &____originalShakeFocalLength_41; }
	inline void set__originalShakeFocalLength_41(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeFocalLength_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeFocalLength_41), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapFocalLengthZero_42() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRemapFocalLengthZero_42)); }
	inline float get__originalRemapFocalLengthZero_42() const { return ____originalRemapFocalLengthZero_42; }
	inline float* get_address_of__originalRemapFocalLengthZero_42() { return &____originalRemapFocalLengthZero_42; }
	inline void set__originalRemapFocalLengthZero_42(float value)
	{
		____originalRemapFocalLengthZero_42 = value;
	}

	inline static int32_t get_offset_of__originalRemapFocalLengthOne_43() { return static_cast<int32_t>(offsetof(MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645, ____originalRemapFocalLengthOne_43)); }
	inline float get__originalRemapFocalLengthOne_43() const { return ____originalRemapFocalLengthOne_43; }
	inline float* get_address_of__originalRemapFocalLengthOne_43() { return &____originalRemapFocalLengthOne_43; }
	inline void set__originalRemapFocalLengthOne_43(float value)
	{
		____originalRemapFocalLengthOne_43 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom
struct MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::Channel
	int32_t ___Channel_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::ShakeDuration
	float ___ShakeDuration_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::ResetShakerValuesAfterShake
	bool ___ResetShakerValuesAfterShake_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::ResetTargetValuesAfterShake
	bool ___ResetTargetValuesAfterShake_30;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::RelativeValues
	bool ___RelativeValues_31;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::ShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeIntensity_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::RemapIntensityZero
	float ___RemapIntensityZero_33;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::RemapIntensityOne
	float ___RemapIntensityOne_34;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::ShakeThreshold
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeThreshold_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::RemapThresholdZero
	float ___RemapThresholdZero_36;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::RemapThresholdOne
	float ___RemapThresholdOne_37;

public:
	inline static int32_t get_offset_of_Channel_27() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___Channel_27)); }
	inline int32_t get_Channel_27() const { return ___Channel_27; }
	inline int32_t* get_address_of_Channel_27() { return &___Channel_27; }
	inline void set_Channel_27(int32_t value)
	{
		___Channel_27 = value;
	}

	inline static int32_t get_offset_of_ShakeDuration_28() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___ShakeDuration_28)); }
	inline float get_ShakeDuration_28() const { return ___ShakeDuration_28; }
	inline float* get_address_of_ShakeDuration_28() { return &___ShakeDuration_28; }
	inline void set_ShakeDuration_28(float value)
	{
		___ShakeDuration_28 = value;
	}

	inline static int32_t get_offset_of_ResetShakerValuesAfterShake_29() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___ResetShakerValuesAfterShake_29)); }
	inline bool get_ResetShakerValuesAfterShake_29() const { return ___ResetShakerValuesAfterShake_29; }
	inline bool* get_address_of_ResetShakerValuesAfterShake_29() { return &___ResetShakerValuesAfterShake_29; }
	inline void set_ResetShakerValuesAfterShake_29(bool value)
	{
		___ResetShakerValuesAfterShake_29 = value;
	}

	inline static int32_t get_offset_of_ResetTargetValuesAfterShake_30() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___ResetTargetValuesAfterShake_30)); }
	inline bool get_ResetTargetValuesAfterShake_30() const { return ___ResetTargetValuesAfterShake_30; }
	inline bool* get_address_of_ResetTargetValuesAfterShake_30() { return &___ResetTargetValuesAfterShake_30; }
	inline void set_ResetTargetValuesAfterShake_30(bool value)
	{
		___ResetTargetValuesAfterShake_30 = value;
	}

	inline static int32_t get_offset_of_RelativeValues_31() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___RelativeValues_31)); }
	inline bool get_RelativeValues_31() const { return ___RelativeValues_31; }
	inline bool* get_address_of_RelativeValues_31() { return &___RelativeValues_31; }
	inline void set_RelativeValues_31(bool value)
	{
		___RelativeValues_31 = value;
	}

	inline static int32_t get_offset_of_ShakeIntensity_32() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___ShakeIntensity_32)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeIntensity_32() const { return ___ShakeIntensity_32; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeIntensity_32() { return &___ShakeIntensity_32; }
	inline void set_ShakeIntensity_32(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeIntensity_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeIntensity_32), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_33() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___RemapIntensityZero_33)); }
	inline float get_RemapIntensityZero_33() const { return ___RemapIntensityZero_33; }
	inline float* get_address_of_RemapIntensityZero_33() { return &___RemapIntensityZero_33; }
	inline void set_RemapIntensityZero_33(float value)
	{
		___RemapIntensityZero_33 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_34() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___RemapIntensityOne_34)); }
	inline float get_RemapIntensityOne_34() const { return ___RemapIntensityOne_34; }
	inline float* get_address_of_RemapIntensityOne_34() { return &___RemapIntensityOne_34; }
	inline void set_RemapIntensityOne_34(float value)
	{
		___RemapIntensityOne_34 = value;
	}

	inline static int32_t get_offset_of_ShakeThreshold_35() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___ShakeThreshold_35)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeThreshold_35() const { return ___ShakeThreshold_35; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeThreshold_35() { return &___ShakeThreshold_35; }
	inline void set_ShakeThreshold_35(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeThreshold_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeThreshold_35), (void*)value);
	}

	inline static int32_t get_offset_of_RemapThresholdZero_36() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___RemapThresholdZero_36)); }
	inline float get_RemapThresholdZero_36() const { return ___RemapThresholdZero_36; }
	inline float* get_address_of_RemapThresholdZero_36() { return &___RemapThresholdZero_36; }
	inline void set_RemapThresholdZero_36(float value)
	{
		___RemapThresholdZero_36 = value;
	}

	inline static int32_t get_offset_of_RemapThresholdOne_37() { return static_cast<int32_t>(offsetof(MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6, ___RemapThresholdOne_37)); }
	inline float get_RemapThresholdOne_37() const { return ___RemapThresholdOne_37; }
	inline float* get_address_of_RemapThresholdOne_37() { return &___RemapThresholdOne_37; }
	inline void set_RemapThresholdOne_37(float value)
	{
		___RemapThresholdOne_37 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration
struct MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::Channel
	int32_t ___Channel_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::Duration
	float ___Duration_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::ResetShakerValuesAfterShake
	bool ___ResetShakerValuesAfterShake_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::ResetTargetValuesAfterShake
	bool ___ResetTargetValuesAfterShake_30;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::RemapIntensityZero
	float ___RemapIntensityZero_31;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::RemapIntensityOne
	float ___RemapIntensityOne_32;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::Intensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___Intensity_33;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::Amplitude
	float ___Amplitude_34;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::RelativeIntensity
	bool ___RelativeIntensity_35;

public:
	inline static int32_t get_offset_of_Channel_27() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___Channel_27)); }
	inline int32_t get_Channel_27() const { return ___Channel_27; }
	inline int32_t* get_address_of_Channel_27() { return &___Channel_27; }
	inline void set_Channel_27(int32_t value)
	{
		___Channel_27 = value;
	}

	inline static int32_t get_offset_of_Duration_28() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___Duration_28)); }
	inline float get_Duration_28() const { return ___Duration_28; }
	inline float* get_address_of_Duration_28() { return &___Duration_28; }
	inline void set_Duration_28(float value)
	{
		___Duration_28 = value;
	}

	inline static int32_t get_offset_of_ResetShakerValuesAfterShake_29() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___ResetShakerValuesAfterShake_29)); }
	inline bool get_ResetShakerValuesAfterShake_29() const { return ___ResetShakerValuesAfterShake_29; }
	inline bool* get_address_of_ResetShakerValuesAfterShake_29() { return &___ResetShakerValuesAfterShake_29; }
	inline void set_ResetShakerValuesAfterShake_29(bool value)
	{
		___ResetShakerValuesAfterShake_29 = value;
	}

	inline static int32_t get_offset_of_ResetTargetValuesAfterShake_30() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___ResetTargetValuesAfterShake_30)); }
	inline bool get_ResetTargetValuesAfterShake_30() const { return ___ResetTargetValuesAfterShake_30; }
	inline bool* get_address_of_ResetTargetValuesAfterShake_30() { return &___ResetTargetValuesAfterShake_30; }
	inline void set_ResetTargetValuesAfterShake_30(bool value)
	{
		___ResetTargetValuesAfterShake_30 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityZero_31() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___RemapIntensityZero_31)); }
	inline float get_RemapIntensityZero_31() const { return ___RemapIntensityZero_31; }
	inline float* get_address_of_RemapIntensityZero_31() { return &___RemapIntensityZero_31; }
	inline void set_RemapIntensityZero_31(float value)
	{
		___RemapIntensityZero_31 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_32() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___RemapIntensityOne_32)); }
	inline float get_RemapIntensityOne_32() const { return ___RemapIntensityOne_32; }
	inline float* get_address_of_RemapIntensityOne_32() { return &___RemapIntensityOne_32; }
	inline void set_RemapIntensityOne_32(float value)
	{
		___RemapIntensityOne_32 = value;
	}

	inline static int32_t get_offset_of_Intensity_33() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___Intensity_33)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_Intensity_33() const { return ___Intensity_33; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_Intensity_33() { return &___Intensity_33; }
	inline void set_Intensity_33(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___Intensity_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Intensity_33), (void*)value);
	}

	inline static int32_t get_offset_of_Amplitude_34() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___Amplitude_34)); }
	inline float get_Amplitude_34() const { return ___Amplitude_34; }
	inline float* get_address_of_Amplitude_34() { return &___Amplitude_34; }
	inline void set_Amplitude_34(float value)
	{
		___Amplitude_34 = value;
	}

	inline static int32_t get_offset_of_RelativeIntensity_35() { return static_cast<int32_t>(offsetof(MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6, ___RelativeIntensity_35)); }
	inline bool get_RelativeIntensity_35() const { return ___RelativeIntensity_35; }
	inline bool* get_address_of_RelativeIntensity_35() { return &___RelativeIntensity_35; }
	inline void set_RelativeIntensity_35(bool value)
	{
		___RelativeIntensity_35 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading
struct MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::Channel
	int32_t ___Channel_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ShakeDuration
	float ___ShakeDuration_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RelativeIntensity
	bool ___RelativeIntensity_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ResetShakerValuesAfterShake
	bool ___ResetShakerValuesAfterShake_30;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ResetTargetValuesAfterShake
	bool ___ResetTargetValuesAfterShake_31;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ShakePostExposure
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakePostExposure_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapPostExposureZero
	float ___RemapPostExposureZero_33;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapPostExposureOne
	float ___RemapPostExposureOne_34;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ShakeHueShift
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeHueShift_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapHueShiftZero
	float ___RemapHueShiftZero_36;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapHueShiftOne
	float ___RemapHueShiftOne_37;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ShakeSaturation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeSaturation_38;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapSaturationZero
	float ___RemapSaturationZero_39;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapSaturationOne
	float ___RemapSaturationOne_40;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::ShakeContrast
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeContrast_41;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapContrastZero
	float ___RemapContrastZero_42;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::RemapContrastOne
	float ___RemapContrastOne_43;

public:
	inline static int32_t get_offset_of_Channel_27() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___Channel_27)); }
	inline int32_t get_Channel_27() const { return ___Channel_27; }
	inline int32_t* get_address_of_Channel_27() { return &___Channel_27; }
	inline void set_Channel_27(int32_t value)
	{
		___Channel_27 = value;
	}

	inline static int32_t get_offset_of_ShakeDuration_28() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ShakeDuration_28)); }
	inline float get_ShakeDuration_28() const { return ___ShakeDuration_28; }
	inline float* get_address_of_ShakeDuration_28() { return &___ShakeDuration_28; }
	inline void set_ShakeDuration_28(float value)
	{
		___ShakeDuration_28 = value;
	}

	inline static int32_t get_offset_of_RelativeIntensity_29() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RelativeIntensity_29)); }
	inline bool get_RelativeIntensity_29() const { return ___RelativeIntensity_29; }
	inline bool* get_address_of_RelativeIntensity_29() { return &___RelativeIntensity_29; }
	inline void set_RelativeIntensity_29(bool value)
	{
		___RelativeIntensity_29 = value;
	}

	inline static int32_t get_offset_of_ResetShakerValuesAfterShake_30() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ResetShakerValuesAfterShake_30)); }
	inline bool get_ResetShakerValuesAfterShake_30() const { return ___ResetShakerValuesAfterShake_30; }
	inline bool* get_address_of_ResetShakerValuesAfterShake_30() { return &___ResetShakerValuesAfterShake_30; }
	inline void set_ResetShakerValuesAfterShake_30(bool value)
	{
		___ResetShakerValuesAfterShake_30 = value;
	}

	inline static int32_t get_offset_of_ResetTargetValuesAfterShake_31() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ResetTargetValuesAfterShake_31)); }
	inline bool get_ResetTargetValuesAfterShake_31() const { return ___ResetTargetValuesAfterShake_31; }
	inline bool* get_address_of_ResetTargetValuesAfterShake_31() { return &___ResetTargetValuesAfterShake_31; }
	inline void set_ResetTargetValuesAfterShake_31(bool value)
	{
		___ResetTargetValuesAfterShake_31 = value;
	}

	inline static int32_t get_offset_of_ShakePostExposure_32() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ShakePostExposure_32)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakePostExposure_32() const { return ___ShakePostExposure_32; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakePostExposure_32() { return &___ShakePostExposure_32; }
	inline void set_ShakePostExposure_32(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakePostExposure_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakePostExposure_32), (void*)value);
	}

	inline static int32_t get_offset_of_RemapPostExposureZero_33() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapPostExposureZero_33)); }
	inline float get_RemapPostExposureZero_33() const { return ___RemapPostExposureZero_33; }
	inline float* get_address_of_RemapPostExposureZero_33() { return &___RemapPostExposureZero_33; }
	inline void set_RemapPostExposureZero_33(float value)
	{
		___RemapPostExposureZero_33 = value;
	}

	inline static int32_t get_offset_of_RemapPostExposureOne_34() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapPostExposureOne_34)); }
	inline float get_RemapPostExposureOne_34() const { return ___RemapPostExposureOne_34; }
	inline float* get_address_of_RemapPostExposureOne_34() { return &___RemapPostExposureOne_34; }
	inline void set_RemapPostExposureOne_34(float value)
	{
		___RemapPostExposureOne_34 = value;
	}

	inline static int32_t get_offset_of_ShakeHueShift_35() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ShakeHueShift_35)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeHueShift_35() const { return ___ShakeHueShift_35; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeHueShift_35() { return &___ShakeHueShift_35; }
	inline void set_ShakeHueShift_35(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeHueShift_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeHueShift_35), (void*)value);
	}

	inline static int32_t get_offset_of_RemapHueShiftZero_36() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapHueShiftZero_36)); }
	inline float get_RemapHueShiftZero_36() const { return ___RemapHueShiftZero_36; }
	inline float* get_address_of_RemapHueShiftZero_36() { return &___RemapHueShiftZero_36; }
	inline void set_RemapHueShiftZero_36(float value)
	{
		___RemapHueShiftZero_36 = value;
	}

	inline static int32_t get_offset_of_RemapHueShiftOne_37() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapHueShiftOne_37)); }
	inline float get_RemapHueShiftOne_37() const { return ___RemapHueShiftOne_37; }
	inline float* get_address_of_RemapHueShiftOne_37() { return &___RemapHueShiftOne_37; }
	inline void set_RemapHueShiftOne_37(float value)
	{
		___RemapHueShiftOne_37 = value;
	}

	inline static int32_t get_offset_of_ShakeSaturation_38() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ShakeSaturation_38)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeSaturation_38() const { return ___ShakeSaturation_38; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeSaturation_38() { return &___ShakeSaturation_38; }
	inline void set_ShakeSaturation_38(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeSaturation_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeSaturation_38), (void*)value);
	}

	inline static int32_t get_offset_of_RemapSaturationZero_39() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapSaturationZero_39)); }
	inline float get_RemapSaturationZero_39() const { return ___RemapSaturationZero_39; }
	inline float* get_address_of_RemapSaturationZero_39() { return &___RemapSaturationZero_39; }
	inline void set_RemapSaturationZero_39(float value)
	{
		___RemapSaturationZero_39 = value;
	}

	inline static int32_t get_offset_of_RemapSaturationOne_40() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapSaturationOne_40)); }
	inline float get_RemapSaturationOne_40() const { return ___RemapSaturationOne_40; }
	inline float* get_address_of_RemapSaturationOne_40() { return &___RemapSaturationOne_40; }
	inline void set_RemapSaturationOne_40(float value)
	{
		___RemapSaturationOne_40 = value;
	}

	inline static int32_t get_offset_of_ShakeContrast_41() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___ShakeContrast_41)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeContrast_41() const { return ___ShakeContrast_41; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeContrast_41() { return &___ShakeContrast_41; }
	inline void set_ShakeContrast_41(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeContrast_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeContrast_41), (void*)value);
	}

	inline static int32_t get_offset_of_RemapContrastZero_42() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapContrastZero_42)); }
	inline float get_RemapContrastZero_42() const { return ___RemapContrastZero_42; }
	inline float* get_address_of_RemapContrastZero_42() { return &___RemapContrastZero_42; }
	inline void set_RemapContrastZero_42(float value)
	{
		___RemapContrastZero_42 = value;
	}

	inline static int32_t get_offset_of_RemapContrastOne_43() { return static_cast<int32_t>(offsetof(MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9, ___RemapContrastOne_43)); }
	inline float get_RemapContrastOne_43() const { return ___RemapContrastOne_43; }
	inline float* get_address_of_RemapContrastOne_43() { return &___RemapContrastOne_43; }
	inline void set_RemapContrastOne_43(float value)
	{
		___RemapContrastOne_43 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField
struct MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::Channel
	int32_t ___Channel_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::ShakeDuration
	float ___ShakeDuration_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RelativeValues
	bool ___RelativeValues_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::ResetShakerValuesAfterShake
	bool ___ResetShakerValuesAfterShake_30;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::ResetTargetValuesAfterShake
	bool ___ResetTargetValuesAfterShake_31;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::ShakeFocusDistance
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeFocusDistance_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RemapFocusDistanceZero
	float ___RemapFocusDistanceZero_33;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RemapFocusDistanceOne
	float ___RemapFocusDistanceOne_34;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::ShakeAperture
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeAperture_35;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RemapApertureZero
	float ___RemapApertureZero_36;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RemapApertureOne
	float ___RemapApertureOne_37;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::ShakeFocalLength
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeFocalLength_38;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RemapFocalLengthZero
	float ___RemapFocalLengthZero_39;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::RemapFocalLengthOne
	float ___RemapFocalLengthOne_40;

public:
	inline static int32_t get_offset_of_Channel_27() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___Channel_27)); }
	inline int32_t get_Channel_27() const { return ___Channel_27; }
	inline int32_t* get_address_of_Channel_27() { return &___Channel_27; }
	inline void set_Channel_27(int32_t value)
	{
		___Channel_27 = value;
	}

	inline static int32_t get_offset_of_ShakeDuration_28() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___ShakeDuration_28)); }
	inline float get_ShakeDuration_28() const { return ___ShakeDuration_28; }
	inline float* get_address_of_ShakeDuration_28() { return &___ShakeDuration_28; }
	inline void set_ShakeDuration_28(float value)
	{
		___ShakeDuration_28 = value;
	}

	inline static int32_t get_offset_of_RelativeValues_29() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RelativeValues_29)); }
	inline bool get_RelativeValues_29() const { return ___RelativeValues_29; }
	inline bool* get_address_of_RelativeValues_29() { return &___RelativeValues_29; }
	inline void set_RelativeValues_29(bool value)
	{
		___RelativeValues_29 = value;
	}

	inline static int32_t get_offset_of_ResetShakerValuesAfterShake_30() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___ResetShakerValuesAfterShake_30)); }
	inline bool get_ResetShakerValuesAfterShake_30() const { return ___ResetShakerValuesAfterShake_30; }
	inline bool* get_address_of_ResetShakerValuesAfterShake_30() { return &___ResetShakerValuesAfterShake_30; }
	inline void set_ResetShakerValuesAfterShake_30(bool value)
	{
		___ResetShakerValuesAfterShake_30 = value;
	}

	inline static int32_t get_offset_of_ResetTargetValuesAfterShake_31() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___ResetTargetValuesAfterShake_31)); }
	inline bool get_ResetTargetValuesAfterShake_31() const { return ___ResetTargetValuesAfterShake_31; }
	inline bool* get_address_of_ResetTargetValuesAfterShake_31() { return &___ResetTargetValuesAfterShake_31; }
	inline void set_ResetTargetValuesAfterShake_31(bool value)
	{
		___ResetTargetValuesAfterShake_31 = value;
	}

	inline static int32_t get_offset_of_ShakeFocusDistance_32() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___ShakeFocusDistance_32)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeFocusDistance_32() const { return ___ShakeFocusDistance_32; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeFocusDistance_32() { return &___ShakeFocusDistance_32; }
	inline void set_ShakeFocusDistance_32(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeFocusDistance_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeFocusDistance_32), (void*)value);
	}

	inline static int32_t get_offset_of_RemapFocusDistanceZero_33() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RemapFocusDistanceZero_33)); }
	inline float get_RemapFocusDistanceZero_33() const { return ___RemapFocusDistanceZero_33; }
	inline float* get_address_of_RemapFocusDistanceZero_33() { return &___RemapFocusDistanceZero_33; }
	inline void set_RemapFocusDistanceZero_33(float value)
	{
		___RemapFocusDistanceZero_33 = value;
	}

	inline static int32_t get_offset_of_RemapFocusDistanceOne_34() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RemapFocusDistanceOne_34)); }
	inline float get_RemapFocusDistanceOne_34() const { return ___RemapFocusDistanceOne_34; }
	inline float* get_address_of_RemapFocusDistanceOne_34() { return &___RemapFocusDistanceOne_34; }
	inline void set_RemapFocusDistanceOne_34(float value)
	{
		___RemapFocusDistanceOne_34 = value;
	}

	inline static int32_t get_offset_of_ShakeAperture_35() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___ShakeAperture_35)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeAperture_35() const { return ___ShakeAperture_35; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeAperture_35() { return &___ShakeAperture_35; }
	inline void set_ShakeAperture_35(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeAperture_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeAperture_35), (void*)value);
	}

	inline static int32_t get_offset_of_RemapApertureZero_36() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RemapApertureZero_36)); }
	inline float get_RemapApertureZero_36() const { return ___RemapApertureZero_36; }
	inline float* get_address_of_RemapApertureZero_36() { return &___RemapApertureZero_36; }
	inline void set_RemapApertureZero_36(float value)
	{
		___RemapApertureZero_36 = value;
	}

	inline static int32_t get_offset_of_RemapApertureOne_37() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RemapApertureOne_37)); }
	inline float get_RemapApertureOne_37() const { return ___RemapApertureOne_37; }
	inline float* get_address_of_RemapApertureOne_37() { return &___RemapApertureOne_37; }
	inline void set_RemapApertureOne_37(float value)
	{
		___RemapApertureOne_37 = value;
	}

	inline static int32_t get_offset_of_ShakeFocalLength_38() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___ShakeFocalLength_38)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeFocalLength_38() const { return ___ShakeFocalLength_38; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeFocalLength_38() { return &___ShakeFocalLength_38; }
	inline void set_ShakeFocalLength_38(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeFocalLength_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeFocalLength_38), (void*)value);
	}

	inline static int32_t get_offset_of_RemapFocalLengthZero_39() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RemapFocalLengthZero_39)); }
	inline float get_RemapFocalLengthZero_39() const { return ___RemapFocalLengthZero_39; }
	inline float* get_address_of_RemapFocalLengthZero_39() { return &___RemapFocalLengthZero_39; }
	inline void set_RemapFocalLengthZero_39(float value)
	{
		___RemapFocalLengthZero_39 = value;
	}

	inline static int32_t get_offset_of_RemapFocalLengthOne_40() { return static_cast<int32_t>(offsetof(MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2, ___RemapFocalLengthOne_40)); }
	inline float get_RemapFocalLengthOne_40() const { return ___RemapFocalLengthOne_40; }
	inline float* get_address_of_RemapFocalLengthOne_40() { return &___RemapFocalLengthOne_40; }
	inline void set_RemapFocalLengthOne_40(float value)
	{
		___RemapFocalLengthOne_40 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend
struct MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::TargetAutoBlend
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * ___TargetAutoBlend_27;
	// MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend/Modes MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::Mode
	int32_t ___Mode_28;
	// MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend/Actions MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::BlendAction
	int32_t ___BlendAction_29;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::BlendDuration
	float ___BlendDuration_30;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::BlendCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___BlendCurve_31;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::InitialWeight
	float ___InitialWeight_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::FinalWeight
	float ___FinalWeight_33;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::ResetToInitialValueOnEnd
	bool ___ResetToInitialValueOnEnd_34;

public:
	inline static int32_t get_offset_of_TargetAutoBlend_27() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___TargetAutoBlend_27)); }
	inline MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * get_TargetAutoBlend_27() const { return ___TargetAutoBlend_27; }
	inline MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 ** get_address_of_TargetAutoBlend_27() { return &___TargetAutoBlend_27; }
	inline void set_TargetAutoBlend_27(MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * value)
	{
		___TargetAutoBlend_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TargetAutoBlend_27), (void*)value);
	}

	inline static int32_t get_offset_of_Mode_28() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___Mode_28)); }
	inline int32_t get_Mode_28() const { return ___Mode_28; }
	inline int32_t* get_address_of_Mode_28() { return &___Mode_28; }
	inline void set_Mode_28(int32_t value)
	{
		___Mode_28 = value;
	}

	inline static int32_t get_offset_of_BlendAction_29() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___BlendAction_29)); }
	inline int32_t get_BlendAction_29() const { return ___BlendAction_29; }
	inline int32_t* get_address_of_BlendAction_29() { return &___BlendAction_29; }
	inline void set_BlendAction_29(int32_t value)
	{
		___BlendAction_29 = value;
	}

	inline static int32_t get_offset_of_BlendDuration_30() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___BlendDuration_30)); }
	inline float get_BlendDuration_30() const { return ___BlendDuration_30; }
	inline float* get_address_of_BlendDuration_30() { return &___BlendDuration_30; }
	inline void set_BlendDuration_30(float value)
	{
		___BlendDuration_30 = value;
	}

	inline static int32_t get_offset_of_BlendCurve_31() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___BlendCurve_31)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_BlendCurve_31() const { return ___BlendCurve_31; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_BlendCurve_31() { return &___BlendCurve_31; }
	inline void set_BlendCurve_31(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___BlendCurve_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BlendCurve_31), (void*)value);
	}

	inline static int32_t get_offset_of_InitialWeight_32() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___InitialWeight_32)); }
	inline float get_InitialWeight_32() const { return ___InitialWeight_32; }
	inline float* get_address_of_InitialWeight_32() { return &___InitialWeight_32; }
	inline void set_InitialWeight_32(float value)
	{
		___InitialWeight_32 = value;
	}

	inline static int32_t get_offset_of_FinalWeight_33() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___FinalWeight_33)); }
	inline float get_FinalWeight_33() const { return ___FinalWeight_33; }
	inline float* get_address_of_FinalWeight_33() { return &___FinalWeight_33; }
	inline void set_FinalWeight_33(float value)
	{
		___FinalWeight_33 = value;
	}

	inline static int32_t get_offset_of_ResetToInitialValueOnEnd_34() { return static_cast<int32_t>(offsetof(MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68, ___ResetToInitialValueOnEnd_34)); }
	inline bool get_ResetToInitialValueOnEnd_34() const { return ___ResetToInitialValueOnEnd_34; }
	inline bool* get_address_of_ResetToInitialValueOnEnd_34() { return &___ResetToInitialValueOnEnd_34; }
	inline void set_ResetToInitialValueOnEnd_34(bool value)
	{
		___ResetToInitialValueOnEnd_34 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion
struct MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::Channel
	int32_t ___Channel_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::Duration
	float ___Duration_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::ResetShakerValuesAfterShake
	bool ___ResetShakerValuesAfterShake_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::ResetTargetValuesAfterShake
	bool ___ResetTargetValuesAfterShake_30;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::RelativeIntensity
	bool ___RelativeIntensity_31;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::Intensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___Intensity_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::RemapIntensityZero
	float ___RemapIntensityZero_33;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::RemapIntensityOne
	float ___RemapIntensityOne_34;

public:
	inline static int32_t get_offset_of_Channel_27() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___Channel_27)); }
	inline int32_t get_Channel_27() const { return ___Channel_27; }
	inline int32_t* get_address_of_Channel_27() { return &___Channel_27; }
	inline void set_Channel_27(int32_t value)
	{
		___Channel_27 = value;
	}

	inline static int32_t get_offset_of_Duration_28() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___Duration_28)); }
	inline float get_Duration_28() const { return ___Duration_28; }
	inline float* get_address_of_Duration_28() { return &___Duration_28; }
	inline void set_Duration_28(float value)
	{
		___Duration_28 = value;
	}

	inline static int32_t get_offset_of_ResetShakerValuesAfterShake_29() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___ResetShakerValuesAfterShake_29)); }
	inline bool get_ResetShakerValuesAfterShake_29() const { return ___ResetShakerValuesAfterShake_29; }
	inline bool* get_address_of_ResetShakerValuesAfterShake_29() { return &___ResetShakerValuesAfterShake_29; }
	inline void set_ResetShakerValuesAfterShake_29(bool value)
	{
		___ResetShakerValuesAfterShake_29 = value;
	}

	inline static int32_t get_offset_of_ResetTargetValuesAfterShake_30() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___ResetTargetValuesAfterShake_30)); }
	inline bool get_ResetTargetValuesAfterShake_30() const { return ___ResetTargetValuesAfterShake_30; }
	inline bool* get_address_of_ResetTargetValuesAfterShake_30() { return &___ResetTargetValuesAfterShake_30; }
	inline void set_ResetTargetValuesAfterShake_30(bool value)
	{
		___ResetTargetValuesAfterShake_30 = value;
	}

	inline static int32_t get_offset_of_RelativeIntensity_31() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___RelativeIntensity_31)); }
	inline bool get_RelativeIntensity_31() const { return ___RelativeIntensity_31; }
	inline bool* get_address_of_RelativeIntensity_31() { return &___RelativeIntensity_31; }
	inline void set_RelativeIntensity_31(bool value)
	{
		___RelativeIntensity_31 = value;
	}

	inline static int32_t get_offset_of_Intensity_32() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___Intensity_32)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_Intensity_32() const { return ___Intensity_32; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_Intensity_32() { return &___Intensity_32; }
	inline void set_Intensity_32(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___Intensity_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Intensity_32), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_33() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___RemapIntensityZero_33)); }
	inline float get_RemapIntensityZero_33() const { return ___RemapIntensityZero_33; }
	inline float* get_address_of_RemapIntensityZero_33() { return &___RemapIntensityZero_33; }
	inline void set_RemapIntensityZero_33(float value)
	{
		___RemapIntensityZero_33 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_34() { return static_cast<int32_t>(offsetof(MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7, ___RemapIntensityOne_34)); }
	inline float get_RemapIntensityOne_34() const { return ___RemapIntensityOne_34; }
	inline float* get_address_of_RemapIntensityOne_34() { return &___RemapIntensityOne_34; }
	inline void set_RemapIntensityOne_34(float value)
	{
		___RemapIntensityOne_34 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette
struct MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1  : public MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4
{
public:
	// System.Int32 MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::Channel
	int32_t ___Channel_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::Duration
	float ___Duration_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::ResetShakerValuesAfterShake
	bool ___ResetShakerValuesAfterShake_29;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::ResetTargetValuesAfterShake
	bool ___ResetTargetValuesAfterShake_30;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::Intensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___Intensity_31;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::RemapIntensityZero
	float ___RemapIntensityZero_32;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::RemapIntensityOne
	float ___RemapIntensityOne_33;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::RelativeIntensity
	bool ___RelativeIntensity_34;

public:
	inline static int32_t get_offset_of_Channel_27() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___Channel_27)); }
	inline int32_t get_Channel_27() const { return ___Channel_27; }
	inline int32_t* get_address_of_Channel_27() { return &___Channel_27; }
	inline void set_Channel_27(int32_t value)
	{
		___Channel_27 = value;
	}

	inline static int32_t get_offset_of_Duration_28() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___Duration_28)); }
	inline float get_Duration_28() const { return ___Duration_28; }
	inline float* get_address_of_Duration_28() { return &___Duration_28; }
	inline void set_Duration_28(float value)
	{
		___Duration_28 = value;
	}

	inline static int32_t get_offset_of_ResetShakerValuesAfterShake_29() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___ResetShakerValuesAfterShake_29)); }
	inline bool get_ResetShakerValuesAfterShake_29() const { return ___ResetShakerValuesAfterShake_29; }
	inline bool* get_address_of_ResetShakerValuesAfterShake_29() { return &___ResetShakerValuesAfterShake_29; }
	inline void set_ResetShakerValuesAfterShake_29(bool value)
	{
		___ResetShakerValuesAfterShake_29 = value;
	}

	inline static int32_t get_offset_of_ResetTargetValuesAfterShake_30() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___ResetTargetValuesAfterShake_30)); }
	inline bool get_ResetTargetValuesAfterShake_30() const { return ___ResetTargetValuesAfterShake_30; }
	inline bool* get_address_of_ResetTargetValuesAfterShake_30() { return &___ResetTargetValuesAfterShake_30; }
	inline void set_ResetTargetValuesAfterShake_30(bool value)
	{
		___ResetTargetValuesAfterShake_30 = value;
	}

	inline static int32_t get_offset_of_Intensity_31() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___Intensity_31)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_Intensity_31() const { return ___Intensity_31; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_Intensity_31() { return &___Intensity_31; }
	inline void set_Intensity_31(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___Intensity_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Intensity_31), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_32() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___RemapIntensityZero_32)); }
	inline float get_RemapIntensityZero_32() const { return ___RemapIntensityZero_32; }
	inline float* get_address_of_RemapIntensityZero_32() { return &___RemapIntensityZero_32; }
	inline void set_RemapIntensityZero_32(float value)
	{
		___RemapIntensityZero_32 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_33() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___RemapIntensityOne_33)); }
	inline float get_RemapIntensityOne_33() const { return ___RemapIntensityOne_33; }
	inline float* get_address_of_RemapIntensityOne_33() { return &___RemapIntensityOne_33; }
	inline void set_RemapIntensityOne_33(float value)
	{
		___RemapIntensityOne_33 = value;
	}

	inline static int32_t get_offset_of_RelativeIntensity_34() { return static_cast<int32_t>(offsetof(MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1, ___RelativeIntensity_34)); }
	inline bool get_RelativeIntensity_34() const { return ___RelativeIntensity_34; }
	inline bool* get_address_of_RelativeIntensity_34() { return &___RelativeIntensity_34; }
	inline void set_RelativeIntensity_34(bool value)
	{
		___RelativeIntensity_34 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker
struct MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1  : public MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::RelativeIntensity
	bool ___RelativeIntensity_18;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::ShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeIntensity_19;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::RemapIntensityZero
	float ___RemapIntensityZero_20;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::RemapIntensityOne
	float ___RemapIntensityOne_21;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_22;
	// UnityEngine.Rendering.PostProcessing.LensDistortion MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_lensDistortion
	LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 * ____lensDistortion_23;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_initialIntensity
	float ____initialIntensity_24;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_originalShakeDuration
	float ____originalShakeDuration_25;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_originalShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeIntensity_26;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_originalRemapIntensityZero
	float ____originalRemapIntensityZero_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_originalRemapIntensityOne
	float ____originalRemapIntensityOne_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::_originalRelativeIntensity
	bool ____originalRelativeIntensity_29;

public:
	inline static int32_t get_offset_of_RelativeIntensity_18() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ___RelativeIntensity_18)); }
	inline bool get_RelativeIntensity_18() const { return ___RelativeIntensity_18; }
	inline bool* get_address_of_RelativeIntensity_18() { return &___RelativeIntensity_18; }
	inline void set_RelativeIntensity_18(bool value)
	{
		___RelativeIntensity_18 = value;
	}

	inline static int32_t get_offset_of_ShakeIntensity_19() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ___ShakeIntensity_19)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeIntensity_19() const { return ___ShakeIntensity_19; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeIntensity_19() { return &___ShakeIntensity_19; }
	inline void set_ShakeIntensity_19(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeIntensity_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeIntensity_19), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_20() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ___RemapIntensityZero_20)); }
	inline float get_RemapIntensityZero_20() const { return ___RemapIntensityZero_20; }
	inline float* get_address_of_RemapIntensityZero_20() { return &___RemapIntensityZero_20; }
	inline void set_RemapIntensityZero_20(float value)
	{
		___RemapIntensityZero_20 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_21() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ___RemapIntensityOne_21)); }
	inline float get_RemapIntensityOne_21() const { return ___RemapIntensityOne_21; }
	inline float* get_address_of_RemapIntensityOne_21() { return &___RemapIntensityOne_21; }
	inline void set_RemapIntensityOne_21(float value)
	{
		___RemapIntensityOne_21 = value;
	}

	inline static int32_t get_offset_of__volume_22() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____volume_22)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_22() const { return ____volume_22; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_22() { return &____volume_22; }
	inline void set__volume_22(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_22), (void*)value);
	}

	inline static int32_t get_offset_of__lensDistortion_23() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____lensDistortion_23)); }
	inline LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 * get__lensDistortion_23() const { return ____lensDistortion_23; }
	inline LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 ** get_address_of__lensDistortion_23() { return &____lensDistortion_23; }
	inline void set__lensDistortion_23(LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 * value)
	{
		____lensDistortion_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lensDistortion_23), (void*)value);
	}

	inline static int32_t get_offset_of__initialIntensity_24() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____initialIntensity_24)); }
	inline float get__initialIntensity_24() const { return ____initialIntensity_24; }
	inline float* get_address_of__initialIntensity_24() { return &____initialIntensity_24; }
	inline void set__initialIntensity_24(float value)
	{
		____initialIntensity_24 = value;
	}

	inline static int32_t get_offset_of__originalShakeDuration_25() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____originalShakeDuration_25)); }
	inline float get__originalShakeDuration_25() const { return ____originalShakeDuration_25; }
	inline float* get_address_of__originalShakeDuration_25() { return &____originalShakeDuration_25; }
	inline void set__originalShakeDuration_25(float value)
	{
		____originalShakeDuration_25 = value;
	}

	inline static int32_t get_offset_of__originalShakeIntensity_26() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____originalShakeIntensity_26)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeIntensity_26() const { return ____originalShakeIntensity_26; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeIntensity_26() { return &____originalShakeIntensity_26; }
	inline void set__originalShakeIntensity_26(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeIntensity_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeIntensity_26), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapIntensityZero_27() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____originalRemapIntensityZero_27)); }
	inline float get__originalRemapIntensityZero_27() const { return ____originalRemapIntensityZero_27; }
	inline float* get_address_of__originalRemapIntensityZero_27() { return &____originalRemapIntensityZero_27; }
	inline void set__originalRemapIntensityZero_27(float value)
	{
		____originalRemapIntensityZero_27 = value;
	}

	inline static int32_t get_offset_of__originalRemapIntensityOne_28() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____originalRemapIntensityOne_28)); }
	inline float get__originalRemapIntensityOne_28() const { return ____originalRemapIntensityOne_28; }
	inline float* get_address_of__originalRemapIntensityOne_28() { return &____originalRemapIntensityOne_28; }
	inline void set__originalRemapIntensityOne_28(float value)
	{
		____originalRemapIntensityOne_28 = value;
	}

	inline static int32_t get_offset_of__originalRelativeIntensity_29() { return static_cast<int32_t>(offsetof(MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1, ____originalRelativeIntensity_29)); }
	inline bool get__originalRelativeIntensity_29() const { return ____originalRelativeIntensity_29; }
	inline bool* get_address_of__originalRelativeIntensity_29() { return &____originalRelativeIntensity_29; }
	inline void set__originalRelativeIntensity_29(bool value)
	{
		____originalRelativeIntensity_29 = value;
	}
};


// MoreMountains.FeedbacksForThirdParty.MMVignetteShaker
struct MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F  : public MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1
{
public:
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::RelativeIntensity
	bool ___RelativeIntensity_18;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::ShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___ShakeIntensity_19;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::RemapIntensityZero
	float ___RemapIntensityZero_20;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::RemapIntensityOne
	float ___RemapIntensityOne_21;
	// UnityEngine.Rendering.PostProcessing.PostProcessVolume MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_volume
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * ____volume_22;
	// UnityEngine.Rendering.PostProcessing.Vignette MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_vignette
	Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * ____vignette_23;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_initialIntensity
	float ____initialIntensity_24;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_originalShakeDuration
	float ____originalShakeDuration_25;
	// UnityEngine.AnimationCurve MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_originalShakeIntensity
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ____originalShakeIntensity_26;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_originalRemapIntensityZero
	float ____originalRemapIntensityZero_27;
	// System.Single MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_originalRemapIntensityOne
	float ____originalRemapIntensityOne_28;
	// System.Boolean MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::_originalRelativeIntensity
	bool ____originalRelativeIntensity_29;

public:
	inline static int32_t get_offset_of_RelativeIntensity_18() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ___RelativeIntensity_18)); }
	inline bool get_RelativeIntensity_18() const { return ___RelativeIntensity_18; }
	inline bool* get_address_of_RelativeIntensity_18() { return &___RelativeIntensity_18; }
	inline void set_RelativeIntensity_18(bool value)
	{
		___RelativeIntensity_18 = value;
	}

	inline static int32_t get_offset_of_ShakeIntensity_19() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ___ShakeIntensity_19)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_ShakeIntensity_19() const { return ___ShakeIntensity_19; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_ShakeIntensity_19() { return &___ShakeIntensity_19; }
	inline void set_ShakeIntensity_19(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___ShakeIntensity_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ShakeIntensity_19), (void*)value);
	}

	inline static int32_t get_offset_of_RemapIntensityZero_20() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ___RemapIntensityZero_20)); }
	inline float get_RemapIntensityZero_20() const { return ___RemapIntensityZero_20; }
	inline float* get_address_of_RemapIntensityZero_20() { return &___RemapIntensityZero_20; }
	inline void set_RemapIntensityZero_20(float value)
	{
		___RemapIntensityZero_20 = value;
	}

	inline static int32_t get_offset_of_RemapIntensityOne_21() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ___RemapIntensityOne_21)); }
	inline float get_RemapIntensityOne_21() const { return ___RemapIntensityOne_21; }
	inline float* get_address_of_RemapIntensityOne_21() { return &___RemapIntensityOne_21; }
	inline void set_RemapIntensityOne_21(float value)
	{
		___RemapIntensityOne_21 = value;
	}

	inline static int32_t get_offset_of__volume_22() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____volume_22)); }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * get__volume_22() const { return ____volume_22; }
	inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF ** get_address_of__volume_22() { return &____volume_22; }
	inline void set__volume_22(PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * value)
	{
		____volume_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____volume_22), (void*)value);
	}

	inline static int32_t get_offset_of__vignette_23() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____vignette_23)); }
	inline Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * get__vignette_23() const { return ____vignette_23; }
	inline Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 ** get_address_of__vignette_23() { return &____vignette_23; }
	inline void set__vignette_23(Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * value)
	{
		____vignette_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____vignette_23), (void*)value);
	}

	inline static int32_t get_offset_of__initialIntensity_24() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____initialIntensity_24)); }
	inline float get__initialIntensity_24() const { return ____initialIntensity_24; }
	inline float* get_address_of__initialIntensity_24() { return &____initialIntensity_24; }
	inline void set__initialIntensity_24(float value)
	{
		____initialIntensity_24 = value;
	}

	inline static int32_t get_offset_of__originalShakeDuration_25() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____originalShakeDuration_25)); }
	inline float get__originalShakeDuration_25() const { return ____originalShakeDuration_25; }
	inline float* get_address_of__originalShakeDuration_25() { return &____originalShakeDuration_25; }
	inline void set__originalShakeDuration_25(float value)
	{
		____originalShakeDuration_25 = value;
	}

	inline static int32_t get_offset_of__originalShakeIntensity_26() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____originalShakeIntensity_26)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get__originalShakeIntensity_26() const { return ____originalShakeIntensity_26; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of__originalShakeIntensity_26() { return &____originalShakeIntensity_26; }
	inline void set__originalShakeIntensity_26(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		____originalShakeIntensity_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____originalShakeIntensity_26), (void*)value);
	}

	inline static int32_t get_offset_of__originalRemapIntensityZero_27() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____originalRemapIntensityZero_27)); }
	inline float get__originalRemapIntensityZero_27() const { return ____originalRemapIntensityZero_27; }
	inline float* get_address_of__originalRemapIntensityZero_27() { return &____originalRemapIntensityZero_27; }
	inline void set__originalRemapIntensityZero_27(float value)
	{
		____originalRemapIntensityZero_27 = value;
	}

	inline static int32_t get_offset_of__originalRemapIntensityOne_28() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____originalRemapIntensityOne_28)); }
	inline float get__originalRemapIntensityOne_28() const { return ____originalRemapIntensityOne_28; }
	inline float* get_address_of__originalRemapIntensityOne_28() { return &____originalRemapIntensityOne_28; }
	inline void set__originalRemapIntensityOne_28(float value)
	{
		____originalRemapIntensityOne_28 = value;
	}

	inline static int32_t get_offset_of__originalRelativeIntensity_29() { return static_cast<int32_t>(offsetof(MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F, ____originalRelativeIntensity_29)); }
	inline bool get__originalRelativeIntensity_29() const { return ____originalRelativeIntensity_29; }
	inline bool* get_address_of__originalRelativeIntensity_29() { return &____originalRelativeIntensity_29; }
	inline void set__originalRelativeIntensity_29(bool value)
	{
		____originalRelativeIntensity_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * m_Items[1];

public:
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  m_Items[1];

public:
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  value)
	{
		m_Items[index] = value;
	}
};
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};

IL2CPP_EXTERN_C void AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(const AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03& unmarshaled, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke& marshaled);
IL2CPP_EXTERN_C void AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(const AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke& marshaled, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03& unmarshaled);
IL2CPP_EXTERN_C void AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke& marshaled);

// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<System.Object>(!!0&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, RuntimeObject ** ___outSetting0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>::Override(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_gshared (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 * __this, float ___x0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !0 UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>::op_Implicit(UnityEngine.Rendering.PostProcessing.ParameterOverride`1<!0>)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_gshared_inline (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 * ___prop0, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rendering.PostProcessing.PostProcessVolume>()
inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * Component_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_mC2BDCEAA8F3C5E4523A7F58F4F03D9AA3C1399FE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// UnityEngine.Rendering.PostProcessing.PostProcessProfile UnityEngine.Rendering.PostProcessing.PostProcessVolume::get_profile()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55 (PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<UnityEngine.Rendering.PostProcessing.DepthOfField>(!!0&)
inline bool PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 ** ___outSetting0, const RuntimeMethod* method)
{
	return ((  bool (*) (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E *, DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 **, const RuntimeMethod*))PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared)(__this, ___outSetting0, method);
}
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E (float ___f0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>::Override(!0)
inline void ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205 (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 * __this, float ___x0, const RuntimeMethod* method)
{
	((  void (*) (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 *, float, const RuntimeMethod*))ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_gshared)(__this, ___x0, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4 (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_m3933FF1EAF3078E6969918306D580A60B3A809BD (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3 (MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Rendering.PostProcessing.PostProcessVolume>()
inline PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<UnityEngine.Rendering.PostProcessing.Bloom>(!!0&)
inline bool PostProcessProfile_TryGetSettings_TisBloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8_mCDEF10AC8C4B9996079DCEC638986263A42D479E (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 ** ___outSetting0, const RuntimeMethod* method)
{
	return ((  bool (*) (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E *, Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 **, const RuntimeMethod*))PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared)(__this, ___outSetting0, method);
}
// !0 UnityEngine.Rendering.PostProcessing.ParameterOverride`1<System.Single>::op_Implicit(UnityEngine.Rendering.PostProcessing.ParameterOverride`1<!0>)
inline float ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 * ___prop0, const RuntimeMethod* method)
{
	return ((  float (*) (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 *, const RuntimeMethod*))ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_gshared_inline)(___prop0, method);
}
// System.Void MoreMountains.Feedbacks.MMShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2 (MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092 (MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F (MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_m8C6D6AA4764569D3426EEE34A5D435E30C3AD3B8 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_Register_mB817392B689A69D225281AAA0ACDFEA1341E0914 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078 (MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_Unregister_mA704D2598A9AEDABF75026B38A0540E05C79D868 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___callback0, const RuntimeMethod* method);
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F * __this, float ___time0, float ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* ___keys0, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74 (MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_mECA4D50519B0FE3849D754AEC245B86CC475A4A0 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<UnityEngine.Rendering.PostProcessing.ChromaticAberration>(!!0&)
inline bool PostProcessProfile_TryGetSettings_TisChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229_m99B57DE78E9EEE2FA3EFB8BB50911DA28CB6F25F (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 ** ___outSetting0, const RuntimeMethod* method)
{
	return ((  bool (*) (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E *, ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 **, const RuntimeMethod*))PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared)(__this, ___outSetting0, method);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_m909A6ABE40C1CF17C2A53173B250D8C91A23495D (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_Register_m8D835B5379A8107ADDE5E64F7778D1B43DA3E2ED (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_Unregister_m63CA7DF579B51EA0BA149C2D09D53360F3367AF3 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_mB73ED89FD1DDEC3416C222C3CDA8F4E9ECCA9A47 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<UnityEngine.Rendering.PostProcessing.ColorGrading>(!!0&)
inline bool PostProcessProfile_TryGetSettings_TisColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5_mCD75893765EDF7E8B5D3381AFBCDDA86EA602F97 (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 ** ___outSetting0, const RuntimeMethod* method)
{
	return ((  bool (*) (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E *, ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 **, const RuntimeMethod*))PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared)(__this, ___outSetting0, method);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_mAA8EA46ABCE74EEB9BB4EA63051A4923C9503903 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_Register_m0D3668E9A2FD2FDE7E425DBB5E932AE061B01CB1 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_Unregister_m40AFF09B682A0DF86DB0CDE993F452453276E50D (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553 (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_mE072A8E7C58AFE376DCBD8AFAC20701518D86AED (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_mD402128BE425C84FBC82232D1056363CFC3A76F7 (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_Register_mADB298BBCDE1DCCA93507597413039A398C8B3FA (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_Unregister_m3CAF5B2B4312D2FA80CF1B513C01573F1F15EBDB (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_Trigger_m23D912AE020BF3DFE52136F7AC63264EC004D592 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFeedback::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83 (MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method);
// System.Void MoreMountains.Feedbacks.MMFeedback::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0 (MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_Trigger_m454DA08E74CCB18E4F40FC35B0F80C3270644EA5 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_Trigger_mD517EE3BA035C260CE5D1F11AABD7B141A3AA7A1 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_Trigger_mEF87B2E979AAE6D96A885D4584372DD06A355790 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_Trigger_m1164932C876A6FE15F489A45F51C4E56BFAEA8B6 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_Trigger_m6BE5A124916B56590046731459A95210B469BC3B (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_time()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258 (const RuntimeMethod* method);
// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::GetTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMGlobalPostProcessingVolumeAutoBlend_GetTime_mE5641A532632B5EB3708754A94C68306BEABE131 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, bool ___value0, const RuntimeMethod* method);
// System.Single MoreMountains.Feedbacks.MMFeedbacksHelpers::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbacksHelpers_Remap_mF91639B3964272F011DC211808184EC25AF9571F (float ___x0, float ___A1, float ___B2, float ___C3, float ___D4, const RuntimeMethod* method);
// System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * __this, float ___time0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::LerpUnclamped(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_LerpUnclamped_mF68548D1AA22018863B6EBE911A5F7A959F94C1E (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_m1297C5964A2FEF0A3AB0D42415275F9992AD18A1 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<UnityEngine.Rendering.PostProcessing.LensDistortion>(!!0&)
inline bool PostProcessProfile_TryGetSettings_TisLensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2_mB931D1A60AFCD5DEE42FD9B8430B4B74629BBF2A (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 ** ___outSetting0, const RuntimeMethod* method)
{
	return ((  bool (*) (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E *, LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 **, const RuntimeMethod*))PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared)(__this, ___outSetting0, method);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_m6DD24C890F271356820106BCD3059A26CE2B05C5 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_Register_m229BE3C83EF1126656363CB35AC2C2B577E33B1D (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_Unregister_m1BD5E15B3D8C854DFE5B2603B65BA9423D543197 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_m7C0F4E59C8EBF4711224DFB293B92B7E9BC36D84 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method);
// System.Boolean UnityEngine.Rendering.PostProcessing.PostProcessProfile::TryGetSettings<UnityEngine.Rendering.PostProcessing.Vignette>(!!0&)
inline bool PostProcessProfile_TryGetSettings_TisVignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139_m2FD6F6813492CA2C85F67666F4B699C733783BD8 (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * __this, Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 ** ___outSetting0, const RuntimeMethod* method)
{
	return ((  bool (*) (PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E *, Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 **, const RuntimeMethod*))PostProcessProfile_TryGetSettings_TisRuntimeObject_mBD3243F587977D36D8208FF0272CA78D2D0A864B_gshared)(__this, ___outSetting0, method);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_mB4D10F50ECD2FE9F53574366D7BD1C9BF9635F9E (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_Register_m5D4664B4F351C71FA9CADC2A1026835F614EB11E (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___callback0, const RuntimeMethod* method);
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_Unregister_m96CD18AB24B6DAE4FF1DD38064621ABBA6B6E974 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___callback0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMAutoFocus::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMAutoFocus_Start_mFACF7CAB48B2D34B3239C8D101A9A1ED24DE0847 (MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_mC2BDCEAA8F3C5E4523A7F58F4F03D9AA3C1399FE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _volume = GetComponent<PostProcessVolume>();
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_0;
		L_0 = Component_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_mC2BDCEAA8F3C5E4523A7F58F4F03D9AA3C1399FE(__this, /*hidden argument*/Component_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_mC2BDCEAA8F3C5E4523A7F58F4F03D9AA3C1399FE_RuntimeMethod_var);
		__this->set__volume_9(L_0);
		// _profile = _volume.profile;
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1 = __this->get__volume_9();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_2;
		L_2 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_1, /*hidden argument*/NULL);
		__this->set__profile_10(L_2);
		// _profile.TryGetSettings<DepthOfField>(out _depthOfField);
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3 = __this->get__profile_10();
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 ** L_4 = __this->get_address_of__depthOfField_11();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D(L_3, (DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMAutoFocus::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMAutoFocus_Update_m9C977FB853A4C1F2835E9699736B6B9420BCA323 (MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		// int focusTargetID = Mathf.FloorToInt(FocusTargetID);
		float L_0 = __this->get_FocusTargetID_7();
		int32_t L_1;
		L_1 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		// if (focusTargetID < FocusTargets.Length)
		int32_t L_2 = V_0;
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_3 = __this->get_FocusTargets_5();
		if ((((int32_t)L_2) >= ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_3)->max_length))))))
		{
			goto IL_0067;
		}
	}
	{
		// float distance = Vector3.Distance(CameraTransform.position, FocusTargets[focusTargetID].position + Offset);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get_CameraTransform_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* L_6 = __this->get_FocusTargets_5();
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_9 = (L_6)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_8));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		L_10 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_9, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = __this->get_Offset_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_10, L_11, /*hidden argument*/NULL);
		float L_13;
		L_13 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_5, L_12, /*hidden argument*/NULL);
		V_1 = L_13;
		// _depthOfField.focusDistance.Override(distance);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_14 = __this->get__depthOfField_11();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_15 = L_14->get_focusDistance_7();
		float L_16 = V_1;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_15, L_16, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _depthOfField.aperture.Override(Aperture);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_17 = __this->get__depthOfField_11();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_18 = L_17->get_aperture_8();
		float L_19 = __this->get_Aperture_8();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_18, L_19, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
	}

IL_0067:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMAutoFocus::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMAutoFocus__ctor_m2A1704833FF8D28989A07E87FE9BA1B6E90AAC66 (MMAutoFocus_tD4AEEA7A2CA3D4BD582CACE32EE3703C67CFB7FA * __this, const RuntimeMethod* method)
{
	{
		// public float Aperture = 0.1f;
		__this->set_Aperture_8((0.100000001f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * V_0 = NULL;
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * V_1 = NULL;
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * V_2 = NULL;
	{
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = ((MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields*)il2cpp_codegen_static_fields_for(MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_1 = V_0;
		V_1 = L_1;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_2 = V_1;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)CastclassSealed((RuntimeObject*)L_4, Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var));
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_5 = V_2;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_6 = V_1;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *>((Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 **)(((MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields*)il2cpp_codegen_static_fields_for(MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_8 = V_0;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)L_8) == ((RuntimeObject*)(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * V_0 = NULL;
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * V_1 = NULL;
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * V_2 = NULL;
	{
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = ((MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields*)il2cpp_codegen_static_fields_for(MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_1 = V_0;
		V_1 = L_1;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_2 = V_1;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)CastclassSealed((RuntimeObject*)L_4, Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var));
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_5 = V_2;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_6 = V_1;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *>((Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 **)(((MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields*)il2cpp_codegen_static_fields_for(MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_8 = V_0;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)L_8) == ((RuntimeObject*)(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_Register_mB817392B689A69D225281AAA0ACDFEA1341E0914 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent += callback;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = ___callback0;
		MMBloomShakeEvent_add_OnEvent_m53DE991E046C1FC6254DD5C85C49F02A64DC6413(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_Unregister_mA704D2598A9AEDABF75026B38A0540E05C79D868 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent -= callback;
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = ___callback0;
		MMBloomShakeEvent_remove_OnEvent_m3D6699AB6E28B9F8B5F88A81E12D93B27E84B465(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShakeEvent_Trigger_m23D912AE020BF3DFE52136F7AC63264EC004D592 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * G_B2_0 = NULL;
	Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * G_B1_0 = NULL;
	{
		// OnEvent?.Invoke(intensity, duration, remapMin, remapMax, threshold, remapThresholdMin, remapThresholdMax, relativeIntensity,
		//     feedbacksIntensity, channel, resetShakerValuesAfterShake, resetTargetValuesAfterShake, forwardDirection, timescaleMode, stop);
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = ((MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_StaticFields*)il2cpp_codegen_static_fields_for(MMBloomShakeEvent_tE716BE8B1FEC5C248729759D59C424D5FC1536ED_il2cpp_TypeInfo_var))->get_OnEvent_0();
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___intensity0;
		float L_3 = ___duration1;
		float L_4 = ___remapMin2;
		float L_5 = ___remapMax3;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_6 = ___threshold4;
		float L_7 = ___remapThresholdMin5;
		float L_8 = ___remapThresholdMax6;
		bool L_9 = ___relativeIntensity7;
		float L_10 = ___feedbacksIntensity8;
		int32_t L_11 = ___channel9;
		bool L_12 = ___resetShakerValuesAfterShake10;
		bool L_13 = ___resetTargetValuesAfterShake11;
		bool L_14 = ___forwardDirection12;
		int32_t L_15 = ___timescaleMode13;
		bool L_16 = ___stop14;
		Delegate_Invoke_m3933FF1EAF3078E6969918306D580A60B3A809BD(G_B2_0, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_Initialization_m5F9C3D84BDD679D84C9222AA129FCDE0A418CE1D (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisBloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8_mCDEF10AC8C4B9996079DCEC638986263A42D479E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3(__this, /*hidden argument*/NULL);
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_25(L_1);
		// _volume.profile.TryGetSettings(out _bloom);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_25();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3;
		L_3 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_2, /*hidden argument*/NULL);
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 ** L_4 = __this->get_address_of__bloom_26();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisBloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8_mCDEF10AC8C4B9996079DCEC638986263A42D479E(L_3, (Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisBloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8_mCDEF10AC8C4B9996079DCEC638986263A42D479E_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::Shake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_Shake_m6C796CD567B86ABA75F08343FEA32C8726BF2F41 (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// float newIntensity = ShakeFloat(ShakeIntensity, RemapIntensityZero, RemapIntensityOne, RelativeValues, _initialIntensity);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_ShakeIntensity_19();
		float L_1 = __this->get_RemapIntensityZero_20();
		float L_2 = __this->get_RemapIntensityOne_21();
		bool L_3 = __this->get_RelativeValues_18();
		float L_4 = __this->get__initialIntensity_27();
		float L_5;
		L_5 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4);
		V_0 = L_5;
		// _bloom.intensity.Override(newIntensity);
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * L_6 = __this->get__bloom_26();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_intensity_7();
		float L_8 = V_0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// float newThreshold = ShakeFloat(ShakeThreshold, RemapThresholdZero, RemapThresholdOne, RelativeValues, _initialThreshold);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_9 = __this->get_ShakeThreshold_22();
		float L_10 = __this->get_RemapThresholdZero_23();
		float L_11 = __this->get_RemapThresholdOne_24();
		bool L_12 = __this->get_RelativeValues_18();
		float L_13 = __this->get__initialThreshold_28();
		float L_14;
		L_14 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_9, L_10, L_11, L_12, L_13);
		V_1 = L_14;
		// _bloom.threshold.Override(newThreshold);
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * L_15 = __this->get__bloom_26();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_16 = L_15->get_threshold_8();
		float L_17 = V_1;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_16, L_17, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::GrabInitialValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_GrabInitialValues_m312CE0BBDCC7DE6CF2EFA345960BC07ED7722CE7 (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _initialIntensity = _bloom.intensity;
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * L_0 = __this->get__bloom_26();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_7();
		float L_2;
		L_2 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_1, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialIntensity_27(L_2);
		// _initialThreshold = _bloom.threshold;
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * L_3 = __this->get__bloom_26();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_4 = L_3->get_threshold_8();
		float L_5;
		L_5 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_4, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialThreshold_28(L_5);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::OnBloomShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_OnBloomShakeEvent_mF42753963ACFC55FCBC7A8C076F5ED2C66359A51 (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!CheckEventAllowed(channel) || (!Interruptible && Shaking))
		int32_t L_0 = ___channel9;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		bool L_2;
		L_2 = VirtFuncInvoker4< bool, int32_t, bool, float, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(24 /* System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3) */, __this, L_0, (bool)0, (0.0f), L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Interruptible_7();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Shaking_9();
		if (!L_4)
		{
			goto IL_002a;
		}
	}

IL_0029:
	{
		// return;
		return;
	}

IL_002a:
	{
		// if (stop)
		bool L_5 = ___stop14;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// Stop();
		VirtActionInvoker0::Invoke(21 /* System.Void MoreMountains.Feedbacks.MMShaker::Stop() */, __this);
		// return;
		return;
	}

IL_0035:
	{
		// _resetShakerValuesAfterShake = resetShakerValuesAfterShake;
		bool L_6 = ___resetShakerValuesAfterShake10;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetShakerValuesAfterShake_15(L_6);
		// _resetTargetValuesAfterShake = resetTargetValuesAfterShake;
		bool L_7 = ___resetTargetValuesAfterShake11;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetTargetValuesAfterShake_16(L_7);
		// if (resetShakerValuesAfterShake)
		bool L_8 = ___resetShakerValuesAfterShake10;
		if (!L_8)
		{
			goto IL_00a9;
		}
	}
	{
		// _originalShakeDuration = ShakeDuration;
		float L_9 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_ShakeDuration_5();
		__this->set__originalShakeDuration_29(L_9);
		// _originalShakeIntensity = ShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_10 = __this->get_ShakeIntensity_19();
		__this->set__originalShakeIntensity_31(L_10);
		// _originalRemapIntensityZero = RemapIntensityZero;
		float L_11 = __this->get_RemapIntensityZero_20();
		__this->set__originalRemapIntensityZero_32(L_11);
		// _originalRemapIntensityOne = RemapIntensityOne;
		float L_12 = __this->get_RemapIntensityOne_21();
		__this->set__originalRemapIntensityOne_33(L_12);
		// _originalRelativeIntensity = RelativeValues;
		bool L_13 = __this->get_RelativeValues_18();
		__this->set__originalRelativeIntensity_30(L_13);
		// _originalShakeThreshold = ShakeThreshold;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_14 = __this->get_ShakeThreshold_22();
		__this->set__originalShakeThreshold_34(L_14);
		// _originalRemapThresholdZero = RemapThresholdZero;
		float L_15 = __this->get_RemapThresholdZero_23();
		__this->set__originalRemapThresholdZero_35(L_15);
		// _originalRemapThresholdOne = RemapThresholdOne;
		float L_16 = __this->get_RemapThresholdOne_24();
		__this->set__originalRemapThresholdOne_36(L_16);
	}

IL_00a9:
	{
		// TimescaleMode = timescaleMode;
		int32_t L_17 = ___timescaleMode13;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_TimescaleMode_11(L_17);
		// ShakeDuration = duration;
		float L_18 = ___duration1;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_18);
		// ShakeIntensity = intensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_19 = ___intensity0;
		__this->set_ShakeIntensity_19(L_19);
		// RemapIntensityZero = remapMin * feedbacksIntensity;
		float L_20 = ___remapMin2;
		float L_21 = ___feedbacksIntensity8;
		__this->set_RemapIntensityZero_20(((float)il2cpp_codegen_multiply((float)L_20, (float)L_21)));
		// RemapIntensityOne = remapMax * feedbacksIntensity;
		float L_22 = ___remapMax3;
		float L_23 = ___feedbacksIntensity8;
		__this->set_RemapIntensityOne_21(((float)il2cpp_codegen_multiply((float)L_22, (float)L_23)));
		// RelativeValues = relativeIntensity;
		bool L_24 = ___relativeIntensity7;
		__this->set_RelativeValues_18(L_24);
		// ShakeThreshold = threshold;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_25 = ___threshold4;
		__this->set_ShakeThreshold_22(L_25);
		// RemapThresholdZero = remapThresholdMin;
		float L_26 = ___remapThresholdMin5;
		__this->set_RemapThresholdZero_23(L_26);
		// RemapThresholdOne = remapThresholdMax;
		float L_27 = ___remapThresholdMax6;
		__this->set_RemapThresholdOne_24(L_27);
		// ForwardDirection = forwardDirection;
		bool L_28 = ___forwardDirection12;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ForwardDirection_10(L_28);
		// Play();
		VirtActionInvoker0::Invoke(20 /* System.Void MoreMountains.Feedbacks.MMShaker::Play() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_ResetTargetValues_m84B854B8DFBD5876B78701C558A4696AB312ACC4 (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetTargetValues();
		MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2(__this, /*hidden argument*/NULL);
		// _bloom.intensity.Override(_initialIntensity);
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * L_0 = __this->get__bloom_26();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_7();
		float L_2 = __this->get__initialIntensity_27();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _bloom.threshold.Override(_initialThreshold);
		Bloom_t457A960B8113F4FC7B406EDC9EBFBAF7070D65A8 * L_3 = __this->get__bloom_26();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_4 = L_3->get_threshold_8();
		float L_5 = __this->get__initialThreshold_28();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_4, L_5, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_ResetShakerValues_m93B752AF2C32CA70988BD87FEBBDC6C44FD3B878 (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	{
		// base.ResetShakerValues();
		MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092(__this, /*hidden argument*/NULL);
		// ShakeDuration = _originalShakeDuration;
		float L_0 = __this->get__originalShakeDuration_29();
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_0);
		// ShakeIntensity = _originalShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_1 = __this->get__originalShakeIntensity_31();
		__this->set_ShakeIntensity_19(L_1);
		// RemapIntensityZero = _originalRemapIntensityZero;
		float L_2 = __this->get__originalRemapIntensityZero_32();
		__this->set_RemapIntensityZero_20(L_2);
		// RemapIntensityOne = _originalRemapIntensityOne;
		float L_3 = __this->get__originalRemapIntensityOne_33();
		__this->set_RemapIntensityOne_21(L_3);
		// RelativeValues = _originalRelativeIntensity;
		bool L_4 = __this->get__originalRelativeIntensity_30();
		__this->set_RelativeValues_18(L_4);
		// ShakeThreshold = _originalShakeThreshold;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = __this->get__originalShakeThreshold_34();
		__this->set_ShakeThreshold_22(L_5);
		// RemapThresholdZero = _originalRemapThresholdZero;
		float L_6 = __this->get__originalRemapThresholdZero_35();
		__this->set_RemapThresholdZero_23(L_6);
		// RemapThresholdOne = _originalRemapThresholdOne;
		float L_7 = __this->get__originalRemapThresholdOne_36();
		__this->set_RemapThresholdOne_24(L_7);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_StartListening_mF2BC5C839BD98E2D26B65FDBFA2DA378F81EB1F8 (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StartListening();
		MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F(__this, /*hidden argument*/NULL);
		// MMBloomShakeEvent.Register(OnBloomShakeEvent);
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)il2cpp_codegen_object_new(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var);
		Delegate__ctor_m8C6D6AA4764569D3426EEE34A5D435E30C3AD3B8(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 25)), /*hidden argument*/NULL);
		MMBloomShakeEvent_Register_mB817392B689A69D225281AAA0ACDFEA1341E0914(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker_StopListening_m24FFEA7F65B5812FAAF1171B7E3EAC8D881D3B6E (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StopListening();
		MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078(__this, /*hidden argument*/NULL);
		// MMBloomShakeEvent.Unregister(OnBloomShakeEvent);
		Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * L_0 = (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 *)il2cpp_codegen_object_new(Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837_il2cpp_TypeInfo_var);
		Delegate__ctor_m8C6D6AA4764569D3426EEE34A5D435E30C3AD3B8(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 25)), /*hidden argument*/NULL);
		MMBloomShakeEvent_Unregister_mA704D2598A9AEDABF75026B38A0540E05C79D868(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMBloomShaker__ctor_mB49BF5B24241D85D9F75A83677186448637CB92E (MMBloomShaker_t64E5D9C345561DDE801E8E17B789ABE2D9A4DDDE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool RelativeValues = true;
		__this->set_RelativeValues_18((bool)1);
		// public AnimationCurve ShakeIntensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakeIntensity_19(L_7);
		// public float RemapIntensityOne = 10f;
		__this->set_RemapIntensityOne_21((10.0f));
		// public AnimationCurve ShakeThreshold = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_8 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_8;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_15 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_15, L_13, /*hidden argument*/NULL);
		__this->set_ShakeThreshold_22(L_15);
		MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * V_0 = NULL;
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * V_1 = NULL;
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * V_2 = NULL;
	{
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = ((MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields*)il2cpp_codegen_static_fields_for(MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_1 = V_0;
		V_1 = L_1;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_2 = V_1;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)CastclassSealed((RuntimeObject*)L_4, Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var));
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_5 = V_2;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_6 = V_1;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *>((Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 **)(((MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields*)il2cpp_codegen_static_fields_for(MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_8 = V_0;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)L_8) == ((RuntimeObject*)(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * V_0 = NULL;
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * V_1 = NULL;
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * V_2 = NULL;
	{
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = ((MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields*)il2cpp_codegen_static_fields_for(MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_1 = V_0;
		V_1 = L_1;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_2 = V_1;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)CastclassSealed((RuntimeObject*)L_4, Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var));
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_5 = V_2;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_6 = V_1;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *>((Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 **)(((MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields*)il2cpp_codegen_static_fields_for(MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_8 = V_0;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)L_8) == ((RuntimeObject*)(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_Register_m8D835B5379A8107ADDE5E64F7778D1B43DA3E2ED (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent += callback;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = ___callback0;
		MMChromaticAberrationShakeEvent_add_OnEvent_m0E75B2CF28ECF5C6ADA7B1D4B9A599AEE88E9704(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_Unregister_m63CA7DF579B51EA0BA149C2D09D53360F3367AF3 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent -= callback;
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = ___callback0;
		MMChromaticAberrationShakeEvent_remove_OnEvent_m35F3FD43E2E7584A642DBF6F04B5F6FC677CC07B(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShakeEvent_Trigger_m454DA08E74CCB18E4F40FC35B0F80C3270644EA5 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * G_B2_0 = NULL;
	Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * G_B1_0 = NULL;
	{
		// OnEvent?.Invoke(intensity, duration, remapMin, remapMax, relativeIntensity, feedbacksIntensity, channel, resetShakerValuesAfterShake, resetTargetValuesAfterShake, forwardDirection, timescaleMode, stop);
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = ((MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_StaticFields*)il2cpp_codegen_static_fields_for(MMChromaticAberrationShakeEvent_t46E8D90075DA7164E341E2A8FB7CD27F98D5D8BD_il2cpp_TypeInfo_var))->get_OnEvent_0();
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___intensity0;
		float L_3 = ___duration1;
		float L_4 = ___remapMin2;
		float L_5 = ___remapMax3;
		bool L_6 = ___relativeIntensity4;
		float L_7 = ___feedbacksIntensity5;
		int32_t L_8 = ___channel6;
		bool L_9 = ___resetShakerValuesAfterShake7;
		bool L_10 = ___resetTargetValuesAfterShake8;
		bool L_11 = ___forwardDirection9;
		int32_t L_12 = ___timescaleMode10;
		bool L_13 = ___stop11;
		Delegate_Invoke_mECA4D50519B0FE3849D754AEC245B86CC475A4A0(G_B2_0, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_Initialization_mC29C07BA5BDD10578E4215320989AE31D305BD02 (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229_m99B57DE78E9EEE2FA3EFB8BB50911DA28CB6F25F_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3(__this, /*hidden argument*/NULL);
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_22(L_1);
		// _volume.profile.TryGetSettings(out _chromaticAberration);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_22();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3;
		L_3 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_2, /*hidden argument*/NULL);
		ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 ** L_4 = __this->get_address_of__chromaticAberration_23();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229_m99B57DE78E9EEE2FA3EFB8BB50911DA28CB6F25F(L_3, (ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229_m99B57DE78E9EEE2FA3EFB8BB50911DA28CB6F25F_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::Shake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_Shake_m6C865F2AD8467CA1F81AAE4C0B1EFFECC9089C62 (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float newValue = ShakeFloat(ShakeIntensity, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, _initialIntensity);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_ShakeIntensity_19();
		float L_1 = __this->get_RemapIntensityZero_20();
		float L_2 = __this->get_RemapIntensityOne_21();
		bool L_3 = __this->get_RelativeIntensity_18();
		float L_4 = __this->get__initialIntensity_24();
		float L_5;
		L_5 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4);
		V_0 = L_5;
		// _chromaticAberration.intensity.Override(newValue);
		ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 * L_6 = __this->get__chromaticAberration_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_intensity_8();
		float L_8 = V_0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::GrabInitialValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_GrabInitialValues_mCB4860BD757AE0B901FD634E2D03016FD437A4E9 (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _initialIntensity = _chromaticAberration.intensity;
		ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 * L_0 = __this->get__chromaticAberration_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_8();
		float L_2;
		L_2 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_1, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialIntensity_24(L_2);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::OnMMChromaticAberrationShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_OnMMChromaticAberrationShakeEvent_m18F22A2670E1E4304B28AC53EA0FADBD0D6E2790 (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!CheckEventAllowed(channel) || (!Interruptible && Shaking))
		int32_t L_0 = ___channel6;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		bool L_2;
		L_2 = VirtFuncInvoker4< bool, int32_t, bool, float, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(24 /* System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3) */, __this, L_0, (bool)0, (0.0f), L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Interruptible_7();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Shaking_9();
		if (!L_4)
		{
			goto IL_002a;
		}
	}

IL_0029:
	{
		// return;
		return;
	}

IL_002a:
	{
		// if (stop)
		bool L_5 = ___stop11;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// Stop();
		VirtActionInvoker0::Invoke(21 /* System.Void MoreMountains.Feedbacks.MMShaker::Stop() */, __this);
		// return;
		return;
	}

IL_0035:
	{
		// _resetShakerValuesAfterShake = resetShakerValuesAfterShake;
		bool L_6 = ___resetShakerValuesAfterShake7;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetShakerValuesAfterShake_15(L_6);
		// _resetTargetValuesAfterShake = resetTargetValuesAfterShake;
		bool L_7 = ___resetTargetValuesAfterShake8;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetTargetValuesAfterShake_16(L_7);
		// if (resetShakerValuesAfterShake)
		bool L_8 = ___resetShakerValuesAfterShake7;
		if (!L_8)
		{
			goto IL_0085;
		}
	}
	{
		// _originalShakeDuration = ShakeDuration;
		float L_9 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_ShakeDuration_5();
		__this->set__originalShakeDuration_25(L_9);
		// _originalShakeIntensity = ShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_10 = __this->get_ShakeIntensity_19();
		__this->set__originalShakeIntensity_26(L_10);
		// _originalRemapIntensityZero = RemapIntensityZero;
		float L_11 = __this->get_RemapIntensityZero_20();
		__this->set__originalRemapIntensityZero_27(L_11);
		// _originalRemapIntensityOne = RemapIntensityOne;
		float L_12 = __this->get_RemapIntensityOne_21();
		__this->set__originalRemapIntensityOne_28(L_12);
		// _originalRelativeIntensity = RelativeIntensity;
		bool L_13 = __this->get_RelativeIntensity_18();
		__this->set__originalRelativeIntensity_29(L_13);
	}

IL_0085:
	{
		// TimescaleMode = timescaleMode;
		int32_t L_14 = ___timescaleMode10;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_TimescaleMode_11(L_14);
		// ShakeDuration = duration;
		float L_15 = ___duration1;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_15);
		// ShakeIntensity = intensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_16 = ___intensity0;
		__this->set_ShakeIntensity_19(L_16);
		// RemapIntensityZero = remapMin * feedbacksIntensity;
		float L_17 = ___remapMin2;
		float L_18 = ___feedbacksIntensity5;
		__this->set_RemapIntensityZero_20(((float)il2cpp_codegen_multiply((float)L_17, (float)L_18)));
		// RemapIntensityOne = remapMax * feedbacksIntensity;
		float L_19 = ___remapMax3;
		float L_20 = ___feedbacksIntensity5;
		__this->set_RemapIntensityOne_21(((float)il2cpp_codegen_multiply((float)L_19, (float)L_20)));
		// RelativeIntensity = relativeIntensity;
		bool L_21 = ___relativeIntensity4;
		__this->set_RelativeIntensity_18(L_21);
		// ForwardDirection = forwardDirection;
		bool L_22 = ___forwardDirection9;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ForwardDirection_10(L_22);
		// Play();
		VirtActionInvoker0::Invoke(20 /* System.Void MoreMountains.Feedbacks.MMShaker::Play() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_ResetTargetValues_m2BDC9F2D6425D62F8C1221EAC2C5FC2BE8DA66FC (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetTargetValues();
		MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2(__this, /*hidden argument*/NULL);
		// _chromaticAberration.intensity.Override(_initialIntensity);
		ChromaticAberration_t03FE8CF734E46DE303C0F17B5E76872F87FAE229 * L_0 = __this->get__chromaticAberration_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_8();
		float L_2 = __this->get__initialIntensity_24();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_ResetShakerValues_mE0C95B6F0ED9B0A86A779A6AED1286170CE5145C (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	{
		// base.ResetShakerValues();
		MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092(__this, /*hidden argument*/NULL);
		// ShakeDuration = _originalShakeDuration;
		float L_0 = __this->get__originalShakeDuration_25();
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_0);
		// ShakeIntensity = _originalShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_1 = __this->get__originalShakeIntensity_26();
		__this->set_ShakeIntensity_19(L_1);
		// RemapIntensityZero = _originalRemapIntensityZero;
		float L_2 = __this->get__originalRemapIntensityZero_27();
		__this->set_RemapIntensityZero_20(L_2);
		// RemapIntensityOne = _originalRemapIntensityOne;
		float L_3 = __this->get__originalRemapIntensityOne_28();
		__this->set_RemapIntensityOne_21(L_3);
		// RelativeIntensity = _originalRelativeIntensity;
		bool L_4 = __this->get__originalRelativeIntensity_29();
		__this->set_RelativeIntensity_18(L_4);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_StartListening_m8B172F3BB6FA926093BB3980E95299291531C89B (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StartListening();
		MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F(__this, /*hidden argument*/NULL);
		// MMChromaticAberrationShakeEvent.Register(OnMMChromaticAberrationShakeEvent);
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)il2cpp_codegen_object_new(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var);
		Delegate__ctor_m909A6ABE40C1CF17C2A53173B250D8C91A23495D(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 25)), /*hidden argument*/NULL);
		MMChromaticAberrationShakeEvent_Register_m8D835B5379A8107ADDE5E64F7778D1B43DA3E2ED(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker_StopListening_m43C5BAC155B46FE85E95732C28CF9629C82803FC (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StopListening();
		MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078(__this, /*hidden argument*/NULL);
		// MMChromaticAberrationShakeEvent.Unregister(OnMMChromaticAberrationShakeEvent);
		Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * L_0 = (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 *)il2cpp_codegen_object_new(Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81_il2cpp_TypeInfo_var);
		Delegate__ctor_m909A6ABE40C1CF17C2A53173B250D8C91A23495D(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 25)), /*hidden argument*/NULL);
		MMChromaticAberrationShakeEvent_Unregister_m63CA7DF579B51EA0BA149C2D09D53360F3367AF3(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMChromaticAberrationShaker__ctor_m25ADDA7CD7BEC48A695F7DE0C28A3021AAA51983 (MMChromaticAberrationShaker_t2BCC8CD9515D2FF4786A99750653E54A44D2E3C6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AnimationCurve ShakeIntensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakeIntensity_19(L_7);
		// public float RemapIntensityOne = 1f;
		__this->set_RemapIntensityOne_21((1.0f));
		MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * V_0 = NULL;
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * V_1 = NULL;
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * V_2 = NULL;
	{
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = ((MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields*)il2cpp_codegen_static_fields_for(MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_1 = V_0;
		V_1 = L_1;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_2 = V_1;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)CastclassSealed((RuntimeObject*)L_4, Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var));
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_5 = V_2;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_6 = V_1;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *>((Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 **)(((MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields*)il2cpp_codegen_static_fields_for(MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_8 = V_0;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)L_8) == ((RuntimeObject*)(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * V_0 = NULL;
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * V_1 = NULL;
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * V_2 = NULL;
	{
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = ((MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields*)il2cpp_codegen_static_fields_for(MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_1 = V_0;
		V_1 = L_1;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_2 = V_1;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)CastclassSealed((RuntimeObject*)L_4, Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var));
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_5 = V_2;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_6 = V_1;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *>((Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 **)(((MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields*)il2cpp_codegen_static_fields_for(MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_8 = V_0;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)L_8) == ((RuntimeObject*)(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_Register_m0D3668E9A2FD2FDE7E425DBB5E932AE061B01CB1 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent += callback;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = ___callback0;
		MMColorGradingShakeEvent_add_OnEvent_m9A770B22852955C8650D27D181F0AACE705D4653(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_Unregister_m40AFF09B682A0DF86DB0CDE993F452453276E50D (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent -= callback;
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = ___callback0;
		MMColorGradingShakeEvent_remove_OnEvent_m0F1C80AC35B82C982BB01FA26B24FAA10D2F1E24(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShakeEvent_Trigger_mD517EE3BA035C260CE5D1F11AABD7B141A3AA7A1 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * G_B2_0 = NULL;
	Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * G_B1_0 = NULL;
	{
		// OnEvent?.Invoke(shakePostExposure, remapPostExposureZero, remapPostExposureOne,
		//     shakeHueShift, remapHueShiftZero, remapHueShiftOne,
		//     shakeSaturation, remapSaturationZero, remapSaturationOne,
		//     shakeContrast, remapContrastZero, remapContrastOne,
		//     duration, relativeValues, feedbacksIntensity, channel, resetShakerValuesAfterShake, resetTargetValuesAfterShake, forwardDirection, timescaleMode, stop);
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = ((MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_StaticFields*)il2cpp_codegen_static_fields_for(MMColorGradingShakeEvent_tA6692561B3F8A0BA1C825A3BA266707F174D37D5_il2cpp_TypeInfo_var))->get_OnEvent_0();
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___shakePostExposure0;
		float L_3 = ___remapPostExposureZero1;
		float L_4 = ___remapPostExposureOne2;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = ___shakeHueShift3;
		float L_6 = ___remapHueShiftZero4;
		float L_7 = ___remapHueShiftOne5;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_8 = ___shakeSaturation6;
		float L_9 = ___remapSaturationZero7;
		float L_10 = ___remapSaturationOne8;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = ___shakeContrast9;
		float L_12 = ___remapContrastZero10;
		float L_13 = ___remapContrastOne11;
		float L_14 = ___duration12;
		bool L_15 = ___relativeValues13;
		float L_16 = ___feedbacksIntensity14;
		int32_t L_17 = ___channel15;
		bool L_18 = ___resetShakerValuesAfterShake16;
		bool L_19 = ___resetTargetValuesAfterShake17;
		bool L_20 = ___forwardDirection18;
		int32_t L_21 = ___timescaleMode19;
		bool L_22 = ___stop20;
		Delegate_Invoke_mB73ED89FD1DDEC3416C222C3CDA8F4E9ECCA9A47(G_B2_0, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_Initialization_m94371A09517400534D8372AA021A67E4551115E7 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5_mCD75893765EDF7E8B5D3381AFBCDDA86EA602F97_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3(__this, /*hidden argument*/NULL);
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_31(L_1);
		// _volume.profile.TryGetSettings(out _colorGrading);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_31();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3;
		L_3 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_2, /*hidden argument*/NULL);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 ** L_4 = __this->get_address_of__colorGrading_32();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5_mCD75893765EDF7E8B5D3381AFBCDDA86EA602F97(L_3, (ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5_mCD75893765EDF7E8B5D3381AFBCDDA86EA602F97_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_Reset_m27E6423275C138EFEE12041569966520DB35C7B7 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	{
		// ShakeDuration = 0.8f;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5((0.800000012f));
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::Shake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_Shake_m9E1CBD8C217766D250F8607BC9121F3EFD911DF6 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	{
		// float newPostExposure = ShakeFloat(ShakePostExposure, RemapPostExposureZero, RemapPostExposureOne, RelativeValues, _initialPostExposure);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_ShakePostExposure_19();
		float L_1 = __this->get_RemapPostExposureZero_20();
		float L_2 = __this->get_RemapPostExposureOne_21();
		bool L_3 = __this->get_RelativeValues_18();
		float L_4 = __this->get__initialPostExposure_33();
		float L_5;
		L_5 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4);
		V_0 = L_5;
		// _colorGrading.postExposure.Override(newPostExposure);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_6 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_postExposure_24();
		float L_8 = V_0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// float newHueShift = ShakeFloat(ShakeHueShift, RemapHueShiftZero, RemapHueShiftOne, RelativeValues, _initialHueShift);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_9 = __this->get_ShakeHueShift_22();
		float L_10 = __this->get_RemapHueShiftZero_23();
		float L_11 = __this->get_RemapHueShiftOne_24();
		bool L_12 = __this->get_RelativeValues_18();
		float L_13 = __this->get__initialHueShift_34();
		float L_14;
		L_14 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_9, L_10, L_11, L_12, L_13);
		V_1 = L_14;
		// _colorGrading.hueShift.Override(newHueShift);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_15 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_16 = L_15->get_hueShift_21();
		float L_17 = V_1;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_16, L_17, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// float newSaturation = ShakeFloat(ShakeSaturation, RemapSaturationZero, RemapSaturationOne, RelativeValues, _initialSaturation);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_18 = __this->get_ShakeSaturation_25();
		float L_19 = __this->get_RemapSaturationZero_26();
		float L_20 = __this->get_RemapSaturationOne_27();
		bool L_21 = __this->get_RelativeValues_18();
		float L_22 = __this->get__initialSaturation_35();
		float L_23;
		L_23 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_18, L_19, L_20, L_21, L_22);
		V_2 = L_23;
		// _colorGrading.saturation.Override(newSaturation);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_24 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_25 = L_24->get_saturation_22();
		float L_26 = V_2;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_25, L_26, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// float newContrast = ShakeFloat(ShakeContrast, RemapContrastZero, RemapContrastOne, RelativeValues, _initialContrast);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_27 = __this->get_ShakeContrast_28();
		float L_28 = __this->get_RemapContrastZero_29();
		float L_29 = __this->get_RemapContrastOne_30();
		bool L_30 = __this->get_RelativeValues_18();
		float L_31 = __this->get__initialContrast_36();
		float L_32;
		L_32 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_27, L_28, L_29, L_30, L_31);
		V_3 = L_32;
		// _colorGrading.contrast.Override(newContrast);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_33 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_34 = L_33->get_contrast_25();
		float L_35 = V_3;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_34, L_35, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::GrabInitialValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_GrabInitialValues_m515090936C5165609704CDBDDB555F9FF31D6F27 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _initialPostExposure = _colorGrading.postExposure;
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_0 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_postExposure_24();
		float L_2;
		L_2 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_1, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialPostExposure_33(L_2);
		// _initialHueShift = _colorGrading.hueShift;
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_3 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_4 = L_3->get_hueShift_21();
		float L_5;
		L_5 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_4, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialHueShift_34(L_5);
		// _initialSaturation = _colorGrading.saturation;
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_6 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_saturation_22();
		float L_8;
		L_8 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_7, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialSaturation_35(L_8);
		// _initialContrast = _colorGrading.contrast;
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_9 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_10 = L_9->get_contrast_25();
		float L_11;
		L_11 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_10, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialContrast_36(L_11);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::OnMMColorGradingShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_OnMMColorGradingShakeEvent_mD715EFFF4CFC86E7FF117A022D05975CA7AFC76A (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!CheckEventAllowed(channel) || (!Interruptible && Shaking))
		int32_t L_0 = ___channel15;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		bool L_2;
		L_2 = VirtFuncInvoker4< bool, int32_t, bool, float, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(24 /* System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3) */, __this, L_0, (bool)0, (0.0f), L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Interruptible_7();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Shaking_9();
		if (!L_4)
		{
			goto IL_002a;
		}
	}

IL_0029:
	{
		// return;
		return;
	}

IL_002a:
	{
		// if (stop)
		bool L_5 = ___stop20;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// Stop();
		VirtActionInvoker0::Invoke(21 /* System.Void MoreMountains.Feedbacks.MMShaker::Stop() */, __this);
		// return;
		return;
	}

IL_0035:
	{
		// _resetShakerValuesAfterShake = resetShakerValuesAfterShake;
		bool L_6 = ___resetShakerValuesAfterShake16;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetShakerValuesAfterShake_15(L_6);
		// _resetTargetValuesAfterShake = resetTargetValuesAfterShake;
		bool L_7 = ___resetTargetValuesAfterShake17;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetTargetValuesAfterShake_16(L_7);
		// if (resetShakerValuesAfterShake)
		bool L_8 = ___resetShakerValuesAfterShake16;
		if (!L_8)
		{
			goto IL_00f4;
		}
	}
	{
		// _originalShakeDuration = ShakeDuration;
		float L_9 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_ShakeDuration_5();
		__this->set__originalShakeDuration_37(L_9);
		// _originalRelativeValues = RelativeValues;
		bool L_10 = __this->get_RelativeValues_18();
		__this->set__originalRelativeValues_38(L_10);
		// _originalShakePostExposure = ShakePostExposure;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = __this->get_ShakePostExposure_19();
		__this->set__originalShakePostExposure_39(L_11);
		// _originalRemapPostExposureZero = RemapPostExposureZero;
		float L_12 = __this->get_RemapPostExposureZero_20();
		__this->set__originalRemapPostExposureZero_40(L_12);
		// _originalRemapPostExposureOne = RemapPostExposureOne;
		float L_13 = __this->get_RemapPostExposureOne_21();
		__this->set__originalRemapPostExposureOne_41(L_13);
		// _originalShakeHueShift = ShakeHueShift;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_14 = __this->get_ShakeHueShift_22();
		__this->set__originalShakeHueShift_42(L_14);
		// _originalRemapHueShiftZero = RemapHueShiftZero;
		float L_15 = __this->get_RemapHueShiftZero_23();
		__this->set__originalRemapHueShiftZero_43(L_15);
		// _originalRemapHueShiftOne = RemapHueShiftOne;
		float L_16 = __this->get_RemapHueShiftOne_24();
		__this->set__originalRemapHueShiftOne_44(L_16);
		// _originalShakeSaturation = ShakeSaturation;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_17 = __this->get_ShakeSaturation_25();
		__this->set__originalShakeSaturation_45(L_17);
		// _originalRemapSaturationZero = RemapSaturationZero;
		float L_18 = __this->get_RemapSaturationZero_26();
		__this->set__originalRemapSaturationZero_46(L_18);
		// _originalRemapSaturationOne = RemapSaturationOne;
		float L_19 = __this->get_RemapSaturationOne_27();
		__this->set__originalRemapSaturationOne_47(L_19);
		// _originalShakeContrast = ShakeContrast;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_20 = __this->get_ShakeContrast_28();
		__this->set__originalShakeContrast_48(L_20);
		// _originalRemapContrastZero = RemapContrastZero;
		float L_21 = __this->get_RemapContrastZero_29();
		__this->set__originalRemapContrastZero_49(L_21);
		// _originalRemapContrastOne = RemapContrastOne;
		float L_22 = __this->get_RemapContrastOne_30();
		__this->set__originalRemapContrastOne_50(L_22);
	}

IL_00f4:
	{
		// TimescaleMode = timescaleMode;
		int32_t L_23 = ___timescaleMode19;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_TimescaleMode_11(L_23);
		// ShakeDuration = duration;
		float L_24 = ___duration12;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_24);
		// RelativeValues = relativeValues;
		bool L_25 = ___relativeValues13;
		__this->set_RelativeValues_18(L_25);
		// ShakePostExposure = shakePostExposure;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_26 = ___shakePostExposure0;
		__this->set_ShakePostExposure_19(L_26);
		// RemapPostExposureZero = remapPostExposureZero;
		float L_27 = ___remapPostExposureZero1;
		__this->set_RemapPostExposureZero_20(L_27);
		// RemapPostExposureOne = remapPostExposureOne;
		float L_28 = ___remapPostExposureOne2;
		__this->set_RemapPostExposureOne_21(L_28);
		// ShakeHueShift = shakeHueShift;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_29 = ___shakeHueShift3;
		__this->set_ShakeHueShift_22(L_29);
		// RemapHueShiftZero = remapHueShiftZero;
		float L_30 = ___remapHueShiftZero4;
		__this->set_RemapHueShiftZero_23(L_30);
		// RemapHueShiftOne  = remapHueShiftOne;
		float L_31 = ___remapHueShiftOne5;
		__this->set_RemapHueShiftOne_24(L_31);
		// ShakeSaturation = shakeSaturation;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_32 = ___shakeSaturation6;
		__this->set_ShakeSaturation_25(L_32);
		// RemapSaturationZero = remapSaturationZero;
		float L_33 = ___remapSaturationZero7;
		__this->set_RemapSaturationZero_26(L_33);
		// RemapSaturationOne = remapSaturationOne;
		float L_34 = ___remapSaturationOne8;
		__this->set_RemapSaturationOne_27(L_34);
		// ShakeContrast = shakeContrast;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_35 = ___shakeContrast9;
		__this->set_ShakeContrast_28(L_35);
		// RemapContrastZero = remapContrastZero;
		float L_36 = ___remapContrastZero10;
		__this->set_RemapContrastZero_29(L_36);
		// RemapContrastOne = remapContrastOne;
		float L_37 = ___remapContrastOne11;
		__this->set_RemapContrastOne_30(L_37);
		// ForwardDirection = forwardDirection;
		bool L_38 = ___forwardDirection18;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ForwardDirection_10(L_38);
		// Play();
		VirtActionInvoker0::Invoke(20 /* System.Void MoreMountains.Feedbacks.MMShaker::Play() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_ResetTargetValues_mA03B27DEBE6D72AE5FD8E5BDD606EC7696AB8ED0 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetTargetValues();
		MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2(__this, /*hidden argument*/NULL);
		// _colorGrading.postExposure.Override(_initialPostExposure);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_0 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_postExposure_24();
		float L_2 = __this->get__initialPostExposure_33();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _colorGrading.hueShift.Override(_initialHueShift);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_3 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_4 = L_3->get_hueShift_21();
		float L_5 = __this->get__initialHueShift_34();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_4, L_5, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _colorGrading.saturation.Override(_initialSaturation);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_6 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_saturation_22();
		float L_8 = __this->get__initialSaturation_35();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _colorGrading.contrast.Override(_initialContrast);
		ColorGrading_t05D75BC0D6AFBF7D1EA14308794E8737F1E62AB5 * L_9 = __this->get__colorGrading_32();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_10 = L_9->get_contrast_25();
		float L_11 = __this->get__initialContrast_36();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_10, L_11, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_ResetShakerValues_mA7993047153E5050A018A3880866C6AE084AA348 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	{
		// base.ResetShakerValues();
		MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092(__this, /*hidden argument*/NULL);
		// ShakeDuration = _originalShakeDuration;
		float L_0 = __this->get__originalShakeDuration_37();
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_0);
		// RelativeValues = _originalRelativeValues;
		bool L_1 = __this->get__originalRelativeValues_38();
		__this->set_RelativeValues_18(L_1);
		// ShakePostExposure = _originalShakePostExposure;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = __this->get__originalShakePostExposure_39();
		__this->set_ShakePostExposure_19(L_2);
		// RemapPostExposureZero = _originalRemapPostExposureZero;
		float L_3 = __this->get__originalRemapPostExposureZero_40();
		__this->set_RemapPostExposureZero_20(L_3);
		// RemapPostExposureOne = _originalRemapPostExposureOne;
		float L_4 = __this->get__originalRemapPostExposureOne_41();
		__this->set_RemapPostExposureOne_21(L_4);
		// ShakeHueShift = _originalShakeHueShift;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = __this->get__originalShakeHueShift_42();
		__this->set_ShakeHueShift_22(L_5);
		// RemapHueShiftZero = _originalRemapHueShiftZero;
		float L_6 = __this->get__originalRemapHueShiftZero_43();
		__this->set_RemapHueShiftZero_23(L_6);
		// RemapHueShiftOne = _originalRemapHueShiftOne;
		float L_7 = __this->get__originalRemapHueShiftOne_44();
		__this->set_RemapHueShiftOne_24(L_7);
		// ShakeSaturation = _originalShakeSaturation;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_8 = __this->get__originalShakeSaturation_45();
		__this->set_ShakeSaturation_25(L_8);
		// RemapSaturationZero = _originalRemapSaturationZero;
		float L_9 = __this->get__originalRemapSaturationZero_46();
		__this->set_RemapSaturationZero_26(L_9);
		// RemapSaturationOne = _originalRemapSaturationOne;
		float L_10 = __this->get__originalRemapSaturationOne_47();
		__this->set_RemapSaturationOne_27(L_10);
		// ShakeContrast = _originalShakeContrast;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = __this->get__originalShakeContrast_48();
		__this->set_ShakeContrast_28(L_11);
		// RemapContrastZero = _originalRemapContrastZero;
		float L_12 = __this->get__originalRemapContrastZero_49();
		__this->set_RemapContrastZero_29(L_12);
		// RemapContrastOne = _originalRemapContrastOne;
		float L_13 = __this->get__originalRemapContrastOne_50();
		__this->set_RemapContrastOne_30(L_13);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_StartListening_m7DD3279C64EF4CB1E3BD44F21B8150922C44EC33 (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StartListening();
		MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F(__this, /*hidden argument*/NULL);
		// MMColorGradingShakeEvent.Register(OnMMColorGradingShakeEvent);
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)il2cpp_codegen_object_new(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var);
		Delegate__ctor_mAA8EA46ABCE74EEB9BB4EA63051A4923C9503903(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMColorGradingShakeEvent_Register_m0D3668E9A2FD2FDE7E425DBB5E932AE061B01CB1(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker_StopListening_m71A1B540F66E721CAC97DF0B724288322A41F14D (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StopListening();
		MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078(__this, /*hidden argument*/NULL);
		// MMColorGradingShakeEvent.Unregister(OnMMColorGradingShakeEvent);
		Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * L_0 = (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 *)il2cpp_codegen_object_new(Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27_il2cpp_TypeInfo_var);
		Delegate__ctor_mAA8EA46ABCE74EEB9BB4EA63051A4923C9503903(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMColorGradingShakeEvent_Unregister_m40AFF09B682A0DF86DB0CDE993F452453276E50D(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMColorGradingShaker__ctor_m66FDCF22D6D5FC87F79225A6AF6FCC6C6FA49A9A (MMColorGradingShaker_t65D8E36D65ED3BA366ACD513A900578249937FC9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool RelativeValues = true;
		__this->set_RelativeValues_18((bool)1);
		// public AnimationCurve ShakePostExposure = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakePostExposure_19(L_7);
		// public float RemapPostExposureOne = 1f;
		__this->set_RemapPostExposureOne_21((1.0f));
		// public AnimationCurve ShakeHueShift = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_8 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_8;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_15 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_15, L_13, /*hidden argument*/NULL);
		__this->set_ShakeHueShift_22(L_15);
		// public float RemapHueShiftOne = 180f;
		__this->set_RemapHueShiftOne_24((180.0f));
		// public AnimationCurve ShakeSaturation = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_16 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_17 = L_16;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_18), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_18);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_19 = L_17;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_20), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_20);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_21 = L_19;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_22), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_22);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_23 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_23, L_21, /*hidden argument*/NULL);
		__this->set_ShakeSaturation_25(L_23);
		// public float RemapSaturationOne = 100f;
		__this->set_RemapSaturationOne_27((100.0f));
		// public AnimationCurve ShakeContrast = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_24 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_25 = L_24;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_26), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_26);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_27 = L_25;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_28), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_28);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_29 = L_27;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_30;
		memset((&L_30), 0, sizeof(L_30));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_30), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_30);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_31 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_31, L_29, /*hidden argument*/NULL);
		__this->set_ShakeContrast_28(L_31);
		// public float RemapContrastOne = 100f;
		__this->set_RemapContrastOne_30((100.0f));
		MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553 (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * V_0 = NULL;
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * V_1 = NULL;
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * V_2 = NULL;
	{
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = ((MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields*)il2cpp_codegen_static_fields_for(MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_1 = V_0;
		V_1 = L_1;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_2 = V_1;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)CastclassSealed((RuntimeObject*)L_4, Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var));
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_5 = V_2;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_6 = V_1;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *>((Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C **)(((MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields*)il2cpp_codegen_static_fields_for(MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_8 = V_0;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)L_8) == ((RuntimeObject*)(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * V_0 = NULL;
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * V_1 = NULL;
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * V_2 = NULL;
	{
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = ((MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields*)il2cpp_codegen_static_fields_for(MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_1 = V_0;
		V_1 = L_1;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_2 = V_1;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)CastclassSealed((RuntimeObject*)L_4, Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var));
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_5 = V_2;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_6 = V_1;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *>((Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C **)(((MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields*)il2cpp_codegen_static_fields_for(MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_8 = V_0;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)L_8) == ((RuntimeObject*)(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_Register_mADB298BBCDE1DCCA93507597413039A398C8B3FA (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent += callback;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = ___callback0;
		MMDepthOfFieldShakeEvent_add_OnEvent_m3B576BAA11A777FE183CF2BDBA9D354A6B32B553(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_Unregister_m3CAF5B2B4312D2FA80CF1B513C01573F1F15EBDB (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent -= callback;
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = ___callback0;
		MMDepthOfFieldShakeEvent_remove_OnEvent_mBA1B1A78BB69A223E7DAED6994867F2DDFB2ADAC(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShakeEvent_Trigger_mEF87B2E979AAE6D96A885D4584372DD06A355790 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * G_B2_0 = NULL;
	Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * G_B1_0 = NULL;
	{
		// OnEvent?.Invoke(focusDistance, duration, remapFocusDistanceMin, remapFocusDistanceMax,
		//     aperture, remapApertureMin, remapApertureMax,
		//     focalLength, remapFocalLengthMin, remapFocalLengthMax, relativeValues,
		//     feedbacksIntensity, channel, resetShakerValuesAfterShake, resetTargetValuesAfterShake, forwardDirection, timescaleMode, stop);
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = ((MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_StaticFields*)il2cpp_codegen_static_fields_for(MMDepthOfFieldShakeEvent_t1AA7229E75FB1B116CD7E1E6A32B9DAE9D6053FB_il2cpp_TypeInfo_var))->get_OnEvent_0();
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___focusDistance0;
		float L_3 = ___duration1;
		float L_4 = ___remapFocusDistanceMin2;
		float L_5 = ___remapFocusDistanceMax3;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_6 = ___aperture4;
		float L_7 = ___remapApertureMin5;
		float L_8 = ___remapApertureMax6;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_9 = ___focalLength7;
		float L_10 = ___remapFocalLengthMin8;
		float L_11 = ___remapFocalLengthMax9;
		bool L_12 = ___relativeValues10;
		float L_13 = ___feedbacksIntensity11;
		int32_t L_14 = ___channel12;
		bool L_15 = ___resetShakerValuesAfterShake13;
		bool L_16 = ___resetTargetValuesAfterShake14;
		bool L_17 = ___forwardDirection15;
		int32_t L_18 = ___timescaleMode16;
		bool L_19 = ___stop17;
		Delegate_Invoke_mE072A8E7C58AFE376DCBD8AFAC20701518D86AED(G_B2_0, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_Initialization_m5F358999FD2FBDAF0380D04BF1A14C4564E56322 (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3(__this, /*hidden argument*/NULL);
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_28(L_1);
		// _volume.profile.TryGetSettings(out _depthOfField);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_28();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3;
		L_3 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_2, /*hidden argument*/NULL);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 ** L_4 = __this->get_address_of__depthOfField_29();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D(L_3, (DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisDepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731_m0194D4E5A82F711D55BE1037110269C47BD76E5D_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::Shake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_Shake_m4103958B1D5123E8666E7CB327DF3BBB5C50503F (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// float newFocusDistance = ShakeFloat(ShakeFocusDistance, RemapFocusDistanceZero, RemapFocusDistanceOne, RelativeValues, _initialFocusDistance);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_ShakeFocusDistance_19();
		float L_1 = __this->get_RemapFocusDistanceZero_20();
		float L_2 = __this->get_RemapFocusDistanceOne_21();
		bool L_3 = __this->get_RelativeValues_18();
		float L_4 = __this->get__initialFocusDistance_30();
		float L_5;
		L_5 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4);
		V_0 = L_5;
		// _depthOfField.focusDistance.Override(newFocusDistance);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_6 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_focusDistance_7();
		float L_8 = V_0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// float newAperture = ShakeFloat(ShakeAperture, RemapApertureZero, RemapApertureOne, RelativeValues, _initialAperture);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_9 = __this->get_ShakeAperture_22();
		float L_10 = __this->get_RemapApertureZero_23();
		float L_11 = __this->get_RemapApertureOne_24();
		bool L_12 = __this->get_RelativeValues_18();
		float L_13 = __this->get__initialAperture_31();
		float L_14;
		L_14 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_9, L_10, L_11, L_12, L_13);
		V_1 = L_14;
		// _depthOfField.aperture.Override(newAperture);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_15 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_16 = L_15->get_aperture_8();
		float L_17 = V_1;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_16, L_17, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// float newFocalLength = ShakeFloat(ShakeFocalLength, RemapFocalLengthZero, RemapFocalLengthOne, RelativeValues, _initialFocalLength);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_18 = __this->get_ShakeFocalLength_25();
		float L_19 = __this->get_RemapFocalLengthZero_26();
		float L_20 = __this->get_RemapFocalLengthOne_27();
		bool L_21 = __this->get_RelativeValues_18();
		float L_22 = __this->get__initialFocalLength_32();
		float L_23;
		L_23 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_18, L_19, L_20, L_21, L_22);
		V_2 = L_23;
		// _depthOfField.focalLength.Override(newFocalLength);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_24 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_25 = L_24->get_focalLength_9();
		float L_26 = V_2;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_25, L_26, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_Reset_mE0CA98B71F265623F84EE00D59414656664D0A38 (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	{
		// ShakeDuration = 2f;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5((2.0f));
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::GrabInitialValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_GrabInitialValues_m5CAB12298172802EA5B64686A91EF7A1C0793651 (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _initialFocusDistance = _depthOfField.focusDistance;
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_0 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_focusDistance_7();
		float L_2;
		L_2 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_1, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialFocusDistance_30(L_2);
		// _initialAperture = _depthOfField.aperture;
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_3 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_4 = L_3->get_aperture_8();
		float L_5;
		L_5 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_4, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialAperture_31(L_5);
		// _initialFocalLength = _depthOfField.focalLength;
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_6 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_focalLength_9();
		float L_8;
		L_8 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_7, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialFocalLength_32(L_8);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::OnDepthOfFieldShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_OnDepthOfFieldShakeEvent_m5C16E32C69542F08172F6F9845D508D5303C239B (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!CheckEventAllowed(channel) || (!Interruptible && Shaking))
		int32_t L_0 = ___channel12;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		bool L_2;
		L_2 = VirtFuncInvoker4< bool, int32_t, bool, float, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(24 /* System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3) */, __this, L_0, (bool)0, (0.0f), L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Interruptible_7();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Shaking_9();
		if (!L_4)
		{
			goto IL_002a;
		}
	}

IL_0029:
	{
		// return;
		return;
	}

IL_002a:
	{
		// if (stop)
		bool L_5 = ___stop17;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// Stop();
		VirtActionInvoker0::Invoke(21 /* System.Void MoreMountains.Feedbacks.MMShaker::Stop() */, __this);
		// return;
		return;
	}

IL_0035:
	{
		// _resetShakerValuesAfterShake = resetShakerValuesAfterShake;
		bool L_6 = ___resetShakerValuesAfterShake13;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetShakerValuesAfterShake_15(L_6);
		// _resetTargetValuesAfterShake = resetTargetValuesAfterShake;
		bool L_7 = ___resetTargetValuesAfterShake14;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetTargetValuesAfterShake_16(L_7);
		// if (resetShakerValuesAfterShake)
		bool L_8 = ___resetShakerValuesAfterShake13;
		if (!L_8)
		{
			goto IL_00d0;
		}
	}
	{
		// _originalShakeDuration = ShakeDuration;
		float L_9 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_ShakeDuration_5();
		__this->set__originalShakeDuration_33(L_9);
		// _originalRelativeValues = RelativeValues;
		bool L_10 = __this->get_RelativeValues_18();
		__this->set__originalRelativeValues_34(L_10);
		// _originalShakeFocusDistance = ShakeFocusDistance;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = __this->get_ShakeFocusDistance_19();
		__this->set__originalShakeFocusDistance_35(L_11);
		// _originalRemapFocusDistanceZero = RemapFocusDistanceZero;
		float L_12 = __this->get_RemapFocusDistanceZero_20();
		__this->set__originalRemapFocusDistanceZero_36(L_12);
		// _originalRemapFocusDistanceOne = RemapFocusDistanceOne;
		float L_13 = __this->get_RemapFocusDistanceOne_21();
		__this->set__originalRemapFocusDistanceOne_37(L_13);
		// _originalShakeAperture = ShakeAperture;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_14 = __this->get_ShakeAperture_22();
		__this->set__originalShakeAperture_38(L_14);
		// _originalRemapApertureZero = RemapApertureZero;
		float L_15 = __this->get_RemapApertureZero_23();
		__this->set__originalRemapApertureZero_39(L_15);
		// _originalRemapApertureOne = RemapApertureOne;
		float L_16 = __this->get_RemapApertureOne_24();
		__this->set__originalRemapApertureOne_40(L_16);
		// _originalShakeFocalLength = ShakeFocalLength;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_17 = __this->get_ShakeFocalLength_25();
		__this->set__originalShakeFocalLength_41(L_17);
		// _originalRemapFocalLengthZero = RemapFocalLengthZero;
		float L_18 = __this->get_RemapFocalLengthZero_26();
		__this->set__originalRemapFocalLengthZero_42(L_18);
		// _originalRemapFocalLengthOne = RemapFocalLengthOne;
		float L_19 = __this->get_RemapFocalLengthOne_27();
		__this->set__originalRemapFocalLengthOne_43(L_19);
	}

IL_00d0:
	{
		// TimescaleMode = timescaleMode;
		int32_t L_20 = ___timescaleMode16;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_TimescaleMode_11(L_20);
		// ShakeDuration = duration;
		float L_21 = ___duration1;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_21);
		// RelativeValues = relativeValues;
		bool L_22 = ___relativeValues10;
		__this->set_RelativeValues_18(L_22);
		// ShakeFocusDistance = focusDistance;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_23 = ___focusDistance0;
		__this->set_ShakeFocusDistance_19(L_23);
		// RemapFocusDistanceZero = remapFocusDistanceMin;
		float L_24 = ___remapFocusDistanceMin2;
		__this->set_RemapFocusDistanceZero_20(L_24);
		// RemapFocusDistanceOne = remapFocusDistanceMax;
		float L_25 = ___remapFocusDistanceMax3;
		__this->set_RemapFocusDistanceOne_21(L_25);
		// ShakeAperture = aperture;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_26 = ___aperture4;
		__this->set_ShakeAperture_22(L_26);
		// RemapApertureZero = remapApertureMin;
		float L_27 = ___remapApertureMin5;
		__this->set_RemapApertureZero_23(L_27);
		// RemapApertureOne = remapApertureMax;
		float L_28 = ___remapApertureMax6;
		__this->set_RemapApertureOne_24(L_28);
		// ShakeFocalLength = focalLength;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_29 = ___focalLength7;
		__this->set_ShakeFocalLength_25(L_29);
		// RemapFocalLengthZero = remapFocalLengthMin;
		float L_30 = ___remapFocalLengthMin8;
		__this->set_RemapFocalLengthZero_26(L_30);
		// RemapFocalLengthOne = remapFocalLengthMax;
		float L_31 = ___remapFocalLengthMax9;
		__this->set_RemapFocalLengthOne_27(L_31);
		// ForwardDirection = forwardDirection;
		bool L_32 = ___forwardDirection15;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ForwardDirection_10(L_32);
		// Play();
		VirtActionInvoker0::Invoke(20 /* System.Void MoreMountains.Feedbacks.MMShaker::Play() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_ResetTargetValues_mF5778C81F4B849EAE780D6E6740F5233DFCDDD17 (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetTargetValues();
		MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2(__this, /*hidden argument*/NULL);
		// _depthOfField.focusDistance.Override(_initialFocusDistance);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_0 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_focusDistance_7();
		float L_2 = __this->get__initialFocusDistance_30();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _depthOfField.aperture.Override(_initialAperture);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_3 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_4 = L_3->get_aperture_8();
		float L_5 = __this->get__initialAperture_31();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_4, L_5, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// _depthOfField.focalLength.Override(_initialFocalLength);
		DepthOfField_tEEDE8392226AF593EBEF5537776E4A87A8FE0731 * L_6 = __this->get__depthOfField_29();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_focalLength_9();
		float L_8 = __this->get__initialFocalLength_32();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_ResetShakerValues_m7B76C6072FE77464215C7907C70DAC15896CEF8E (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	{
		// base.ResetShakerValues();
		MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092(__this, /*hidden argument*/NULL);
		// ShakeDuration = _originalShakeDuration;
		float L_0 = __this->get__originalShakeDuration_33();
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_0);
		// RelativeValues = _originalRelativeValues;
		bool L_1 = __this->get__originalRelativeValues_34();
		__this->set_RelativeValues_18(L_1);
		// ShakeFocusDistance = _originalShakeFocusDistance;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = __this->get__originalShakeFocusDistance_35();
		__this->set_ShakeFocusDistance_19(L_2);
		// RemapFocusDistanceZero = _originalRemapFocusDistanceZero;
		float L_3 = __this->get__originalRemapFocusDistanceZero_36();
		__this->set_RemapFocusDistanceZero_20(L_3);
		// RemapFocusDistanceOne = _originalRemapFocusDistanceOne;
		float L_4 = __this->get__originalRemapFocusDistanceOne_37();
		__this->set_RemapFocusDistanceOne_21(L_4);
		// ShakeAperture = _originalShakeAperture;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = __this->get__originalShakeAperture_38();
		__this->set_ShakeAperture_22(L_5);
		// RemapApertureZero = _originalRemapApertureZero;
		float L_6 = __this->get__originalRemapApertureZero_39();
		__this->set_RemapApertureZero_23(L_6);
		// RemapApertureOne = _originalRemapApertureOne;
		float L_7 = __this->get__originalRemapApertureOne_40();
		__this->set_RemapApertureOne_24(L_7);
		// ShakeFocalLength = _originalShakeFocalLength;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_8 = __this->get__originalShakeFocalLength_41();
		__this->set_ShakeFocalLength_25(L_8);
		// RemapFocalLengthZero = _originalRemapFocalLengthZero;
		float L_9 = __this->get__originalRemapFocalLengthZero_42();
		__this->set_RemapFocalLengthZero_26(L_9);
		// RemapFocalLengthOne = _originalRemapFocalLengthOne;
		float L_10 = __this->get__originalRemapFocalLengthOne_43();
		__this->set_RemapFocalLengthOne_27(L_10);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_StartListening_mB41FB46056BE61E90C5E3089A1A9B560D3E6F1BF (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StartListening();
		MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F(__this, /*hidden argument*/NULL);
		// MMDepthOfFieldShakeEvent.Register(OnDepthOfFieldShakeEvent);
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)il2cpp_codegen_object_new(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var);
		Delegate__ctor_mD402128BE425C84FBC82232D1056363CFC3A76F7(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMDepthOfFieldShakeEvent_Register_mADB298BBCDE1DCCA93507597413039A398C8B3FA(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker_StopListening_m83D3B4F4AD828B24461674C665FA016DEBF9D156 (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StopListening();
		MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078(__this, /*hidden argument*/NULL);
		// MMDepthOfFieldShakeEvent.Unregister(OnDepthOfFieldShakeEvent);
		Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * L_0 = (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C *)il2cpp_codegen_object_new(Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C_il2cpp_TypeInfo_var);
		Delegate__ctor_mD402128BE425C84FBC82232D1056363CFC3A76F7(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMDepthOfFieldShakeEvent_Unregister_m3CAF5B2B4312D2FA80CF1B513C01573F1F15EBDB(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMDepthOfFieldShaker__ctor_m2E8A4336941FD00B67EF6D883233D59978D32FC1 (MMDepthOfFieldShaker_t424BFEC172612FBD9DC9F94F525515DC57C0D645 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool RelativeValues = true;
		__this->set_RelativeValues_18((bool)1);
		// public AnimationCurve ShakeFocusDistance = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakeFocusDistance_19(L_7);
		// public float RemapFocusDistanceOne = 3f;
		__this->set_RemapFocusDistanceOne_21((3.0f));
		// public AnimationCurve ShakeAperture = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_8 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_8;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_15 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_15, L_13, /*hidden argument*/NULL);
		__this->set_ShakeAperture_22(L_15);
		// public AnimationCurve ShakeFocalLength = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_16 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_17 = L_16;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_18), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_18);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_19 = L_17;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_20), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_20);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_21 = L_19;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_22), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_22);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_23 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_23, L_21, /*hidden argument*/NULL);
		__this->set_ShakeFocalLength_25(L_23);
		MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackBloom_get_FeedbackDuration_m9F634FB92CF18DE6E0ABCB9691C1B71FF7E1E0C8 (MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6 * __this, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); }  set { ShakeDuration = value;  } }
		float L_0 = __this->get_ShakeDuration_28();
		float L_1;
		L_1 = VirtFuncInvoker1< float, float >::Invoke(34 /* System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single) */, __this, L_0);
		return L_1;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::set_FeedbackDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackBloom_set_FeedbackDuration_m1DA3D9F8A614794C72335892C986027AEB91523A (MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); }  set { ShakeDuration = value;  } }
		float L_0 = ___value0;
		__this->set_ShakeDuration_28(L_0);
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); }  set { ShakeDuration = value;  } }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackBloom_CustomPlayFeedback_m06D97480F2D6CBCAFBC0A68329B45E4063B34446 (MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_0078;
		}
	}
	{
		// float intensityMultiplier = Timing.ConstantIntensity ? 1f : feedbacksIntensity;
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_1 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		bool L_2 = L_1->get_ConstantIntensity_10();
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		float L_3 = ___feedbacksIntensity1;
		G_B4_0 = L_3;
		goto IL_001d;
	}

IL_0018:
	{
		G_B4_0 = (1.0f);
	}

IL_001d:
	{
		V_0 = G_B4_0;
		// MMBloomShakeEvent.Trigger(ShakeIntensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, ShakeThreshold, RemapThresholdZero, RemapThresholdOne,
		//     RelativeValues, intensityMultiplier, Channel, ResetShakerValuesAfterShake, ResetTargetValuesAfterShake, NormalPlayDirection, Timing.TimescaleMode);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_4 = __this->get_ShakeIntensity_32();
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_6 = __this->get_RemapIntensityZero_33();
		float L_7 = __this->get_RemapIntensityOne_34();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_8 = __this->get_ShakeThreshold_35();
		float L_9 = __this->get_RemapThresholdZero_36();
		float L_10 = __this->get_RemapThresholdOne_37();
		bool L_11 = __this->get_RelativeValues_31();
		float L_12 = V_0;
		int32_t L_13 = __this->get_Channel_27();
		bool L_14 = __this->get_ResetShakerValuesAfterShake_29();
		bool L_15 = __this->get_ResetTargetValuesAfterShake_30();
		bool L_16;
		L_16 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_17 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		int32_t L_18 = L_17->get_TimescaleMode_0();
		MMBloomShakeEvent_Trigger_m23D912AE020BF3DFE52136F7AC63264EC004D592(L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_18, (bool)0, /*hidden argument*/NULL);
	}

IL_0078:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackBloom_CustomStopFeedback_m6F5CF7DB5AA8E7179542DB37AF491D317D7E37FE (MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active)
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_0050;
		}
	}
	{
		// MMBloomShakeEvent.Trigger(ShakeIntensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, ShakeThreshold, RemapThresholdZero, RemapThresholdOne,
		//     RelativeValues, stop:true);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_3 = __this->get_ShakeIntensity_32();
		float L_4;
		L_4 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_5 = __this->get_RemapIntensityZero_33();
		float L_6 = __this->get_RemapIntensityOne_34();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = __this->get_ShakeThreshold_35();
		float L_8 = __this->get_RemapThresholdZero_36();
		float L_9 = __this->get_RemapThresholdOne_37();
		bool L_10 = __this->get_RelativeValues_31();
		MMBloomShakeEvent_Trigger_m23D912AE020BF3DFE52136F7AC63264EC004D592(L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, (1.0f), 0, (bool)1, (bool)1, (bool)1, 0, (bool)1, /*hidden argument*/NULL);
	}

IL_0050:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackBloom::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackBloom__ctor_m893917B5812AA8BD05FCD71C197D130D6DF35333 (MMFeedbackBloom_t7128B73A9DD5071DEBA5ED21890A53120E3AB4A6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float ShakeDuration = 0.2f;
		__this->set_ShakeDuration_28((0.200000003f));
		// public bool ResetShakerValuesAfterShake = true;
		__this->set_ResetShakerValuesAfterShake_29((bool)1);
		// public bool ResetTargetValuesAfterShake = true;
		__this->set_ResetTargetValuesAfterShake_30((bool)1);
		// public bool RelativeValues = true;
		__this->set_RelativeValues_31((bool)1);
		// public AnimationCurve ShakeIntensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakeIntensity_32(L_7);
		// public float RemapIntensityOne = 1f;
		__this->set_RemapIntensityOne_34((1.0f));
		// public AnimationCurve ShakeThreshold = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_8 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_8;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_15 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_15, L_13, /*hidden argument*/NULL);
		__this->set_ShakeThreshold_35(L_15);
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackChromaticAberration_get_FeedbackDuration_mA9CF2B362AAC259B8B40B0DBBF362CF9B3D22466 (MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6 * __this, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); } set { Duration = value;  } }
		float L_0 = __this->get_Duration_28();
		float L_1;
		L_1 = VirtFuncInvoker1< float, float >::Invoke(34 /* System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single) */, __this, L_0);
		return L_1;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::set_FeedbackDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackChromaticAberration_set_FeedbackDuration_mBD42A1C970C33EAF4AE2541088DCCF2FB3B001E5 (MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); } set { Duration = value;  } }
		float L_0 = ___value0;
		__this->set_Duration_28(L_0);
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); } set { Duration = value;  } }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackChromaticAberration_CustomPlayFeedback_mDC0FA33616B2E58F7E49B8EC3599B8EB65275ECC (MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		// float intensityMultiplier = Timing.ConstantIntensity ? 1f : feedbacksIntensity;
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_1 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		bool L_2 = L_1->get_ConstantIntensity_10();
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		float L_3 = ___feedbacksIntensity1;
		G_B4_0 = L_3;
		goto IL_001d;
	}

IL_0018:
	{
		G_B4_0 = (1.0f);
	}

IL_001d:
	{
		V_0 = G_B4_0;
		// MMChromaticAberrationShakeEvent.Trigger(Intensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, intensityMultiplier,
		//     Channel, ResetShakerValuesAfterShake, ResetTargetValuesAfterShake, NormalPlayDirection, Timing.TimescaleMode);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_4 = __this->get_Intensity_33();
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_6 = __this->get_RemapIntensityZero_31();
		float L_7 = __this->get_RemapIntensityOne_32();
		bool L_8 = __this->get_RelativeIntensity_35();
		float L_9 = V_0;
		int32_t L_10 = __this->get_Channel_27();
		bool L_11 = __this->get_ResetShakerValuesAfterShake_29();
		bool L_12 = __this->get_ResetTargetValuesAfterShake_30();
		bool L_13;
		L_13 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_14 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		int32_t L_15 = L_14->get_TimescaleMode_0();
		MMChromaticAberrationShakeEvent_Trigger_m454DA08E74CCB18E4F40FC35B0F80C3270644EA5(L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_15, (bool)0, /*hidden argument*/NULL);
	}

IL_0066:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackChromaticAberration_CustomStopFeedback_mE6E41DFF28E36848B7BEC2D1C2E9575206096DDE (MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active)
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		// MMChromaticAberrationShakeEvent.Trigger(Intensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, stop:true);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_3 = __this->get_Intensity_33();
		float L_4;
		L_4 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_5 = __this->get_RemapIntensityZero_31();
		float L_6 = __this->get_RemapIntensityOne_32();
		bool L_7 = __this->get_RelativeIntensity_35();
		MMChromaticAberrationShakeEvent_Trigger_m454DA08E74CCB18E4F40FC35B0F80C3270644EA5(L_3, L_4, L_5, L_6, L_7, (1.0f), 0, (bool)1, (bool)1, (bool)1, 0, (bool)1, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackChromaticAberration::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackChromaticAberration__ctor_m6D993AA2741C2531AD6A63C740A2B5FEAE1CB799 (MMFeedbackChromaticAberration_t67B6DB352719FF85A70629B63643E27D7D855FD6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float Duration = 0.2f;
		__this->set_Duration_28((0.200000003f));
		// public bool ResetShakerValuesAfterShake = true;
		__this->set_ResetShakerValuesAfterShake_29((bool)1);
		// public bool ResetTargetValuesAfterShake = true;
		__this->set_ResetTargetValuesAfterShake_30((bool)1);
		// public float RemapIntensityOne = 1f;
		__this->set_RemapIntensityOne_32((1.0f));
		// public AnimationCurve Intensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_Intensity_33(L_7);
		// public float Amplitude = 1.0f;
		__this->set_Amplitude_34((1.0f));
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackColorGrading_get_FeedbackDuration_m990844942C8A307F304B831DBFEEB29AFB1A4A8D (MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9 * __this, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); }  set { ShakeDuration = value;  } }
		float L_0 = __this->get_ShakeDuration_28();
		float L_1;
		L_1 = VirtFuncInvoker1< float, float >::Invoke(34 /* System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single) */, __this, L_0);
		return L_1;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::set_FeedbackDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackColorGrading_set_FeedbackDuration_m381EA911DBB6B925F25CE45105769EAD829625DB (MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); }  set { ShakeDuration = value;  } }
		float L_0 = ___value0;
		__this->set_ShakeDuration_28(L_0);
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); }  set { ShakeDuration = value;  } }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackColorGrading_CustomPlayFeedback_m5B19EBBE04D72F12F0C8DFEF10BB7D4BC03D52D2 (MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_009f;
		}
	}
	{
		// float intensityMultiplier = Timing.ConstantIntensity ? 1f : feedbacksIntensity;
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_1 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		bool L_2 = L_1->get_ConstantIntensity_10();
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		float L_3 = ___feedbacksIntensity1;
		G_B4_0 = L_3;
		goto IL_0020;
	}

IL_001b:
	{
		G_B4_0 = (1.0f);
	}

IL_0020:
	{
		V_0 = G_B4_0;
		// MMColorGradingShakeEvent.Trigger(ShakePostExposure, RemapPostExposureZero, RemapPostExposureOne,
		//     ShakeHueShift, RemapHueShiftZero, RemapHueShiftOne,
		//     ShakeSaturation, RemapSaturationZero, RemapSaturationOne,
		//     ShakeContrast, RemapContrastZero, RemapContrastOne,
		//     FeedbackDuration,
		//     RelativeIntensity, intensityMultiplier, Channel, ResetShakerValuesAfterShake, ResetTargetValuesAfterShake, NormalPlayDirection, Timing.TimescaleMode);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_4 = __this->get_ShakePostExposure_32();
		float L_5 = __this->get_RemapPostExposureZero_33();
		float L_6 = __this->get_RemapPostExposureOne_34();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = __this->get_ShakeHueShift_35();
		float L_8 = __this->get_RemapHueShiftZero_36();
		float L_9 = __this->get_RemapHueShiftOne_37();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_10 = __this->get_ShakeSaturation_38();
		float L_11 = __this->get_RemapSaturationZero_39();
		float L_12 = __this->get_RemapSaturationOne_40();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_13 = __this->get_ShakeContrast_41();
		float L_14 = __this->get_RemapContrastZero_42();
		float L_15 = __this->get_RemapContrastOne_43();
		float L_16;
		L_16 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		bool L_17 = __this->get_RelativeIntensity_29();
		float L_18 = V_0;
		int32_t L_19 = __this->get_Channel_27();
		bool L_20 = __this->get_ResetShakerValuesAfterShake_30();
		bool L_21 = __this->get_ResetTargetValuesAfterShake_31();
		bool L_22;
		L_22 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_23 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		int32_t L_24 = L_23->get_TimescaleMode_0();
		MMColorGradingShakeEvent_Trigger_mD517EE3BA035C260CE5D1F11AABD7B141A3AA7A1(L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, L_24, (bool)0, /*hidden argument*/NULL);
	}

IL_009f:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackColorGrading_CustomStopFeedback_mEED0EAF0BD8203C08A427D8E65BCB21A260D11C2 (MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active)
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_006f;
		}
	}
	{
		// MMColorGradingShakeEvent.Trigger(ShakePostExposure, RemapPostExposureZero, RemapPostExposureOne,
		//     ShakeHueShift, RemapHueShiftZero, RemapHueShiftOne,
		//     ShakeSaturation, RemapSaturationZero, RemapSaturationOne,
		//     ShakeContrast, RemapContrastZero, RemapContrastOne,
		//     FeedbackDuration,
		//     stop:true);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_3 = __this->get_ShakePostExposure_32();
		float L_4 = __this->get_RemapPostExposureZero_33();
		float L_5 = __this->get_RemapPostExposureOne_34();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_6 = __this->get_ShakeHueShift_35();
		float L_7 = __this->get_RemapHueShiftZero_36();
		float L_8 = __this->get_RemapHueShiftOne_37();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_9 = __this->get_ShakeSaturation_38();
		float L_10 = __this->get_RemapSaturationZero_39();
		float L_11 = __this->get_RemapSaturationOne_40();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_12 = __this->get_ShakeContrast_41();
		float L_13 = __this->get_RemapContrastZero_42();
		float L_14 = __this->get_RemapContrastOne_43();
		float L_15;
		L_15 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		MMColorGradingShakeEvent_Trigger_mD517EE3BA035C260CE5D1F11AABD7B141A3AA7A1(L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, (bool)0, (1.0f), 0, (bool)1, (bool)1, (bool)1, 0, (bool)1, /*hidden argument*/NULL);
	}

IL_006f:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackColorGrading::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackColorGrading__ctor_m168385EE3B18AD7FC47DBB107BBE9E34ADCEEFB6 (MMFeedbackColorGrading_tD9D98186A4C70C5F23A8CAA451C9ABC40DF4F2B9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float ShakeDuration = 1f;
		__this->set_ShakeDuration_28((1.0f));
		// public bool RelativeIntensity = true;
		__this->set_RelativeIntensity_29((bool)1);
		// public bool ResetShakerValuesAfterShake = true;
		__this->set_ResetShakerValuesAfterShake_30((bool)1);
		// public bool ResetTargetValuesAfterShake = true;
		__this->set_ResetTargetValuesAfterShake_31((bool)1);
		// public AnimationCurve ShakePostExposure = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakePostExposure_32(L_7);
		// public float RemapPostExposureOne = 1f;
		__this->set_RemapPostExposureOne_34((1.0f));
		// public AnimationCurve ShakeHueShift = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_8 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_8;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_15 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_15, L_13, /*hidden argument*/NULL);
		__this->set_ShakeHueShift_35(L_15);
		// public float RemapHueShiftOne = 180f;
		__this->set_RemapHueShiftOne_37((180.0f));
		// public AnimationCurve ShakeSaturation = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_16 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_17 = L_16;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_18), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_18);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_19 = L_17;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_20), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_20);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_21 = L_19;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_22), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_22);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_23 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_23, L_21, /*hidden argument*/NULL);
		__this->set_ShakeSaturation_38(L_23);
		// public float RemapSaturationOne = 100f;
		__this->set_RemapSaturationOne_40((100.0f));
		// public AnimationCurve ShakeContrast = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_24 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_25 = L_24;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_26;
		memset((&L_26), 0, sizeof(L_26));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_26), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_26);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_27 = L_25;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_28;
		memset((&L_28), 0, sizeof(L_28));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_28), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_28);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_29 = L_27;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_30;
		memset((&L_30), 0, sizeof(L_30));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_30), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_30);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_31 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_31, L_29, /*hidden argument*/NULL);
		__this->set_ShakeContrast_41(L_31);
		// public float RemapContrastOne = 100f;
		__this->set_RemapContrastOne_43((100.0f));
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackDepthOfField_get_FeedbackDuration_mFEF739B972CCDBCB852CABB1BE9B6A624D6D16F5 (MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2 * __this, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); } set { ShakeDuration = value;  } }
		float L_0 = __this->get_ShakeDuration_28();
		float L_1;
		L_1 = VirtFuncInvoker1< float, float >::Invoke(34 /* System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single) */, __this, L_0);
		return L_1;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::set_FeedbackDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackDepthOfField_set_FeedbackDuration_m28DEB54B2CA8CEA69301063C53BC474BB7E761C5 (MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); } set { ShakeDuration = value;  } }
		float L_0 = ___value0;
		__this->set_ShakeDuration_28(L_0);
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(ShakeDuration); } set { ShakeDuration = value;  } }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackDepthOfField_CustomPlayFeedback_m964964960B383FC9CE6D8A00545A3D34BFD691A3 (MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_008d;
		}
	}
	{
		// float intensityMultiplier = Timing.ConstantIntensity ? 1f : feedbacksIntensity;
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_1 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		bool L_2 = L_1->get_ConstantIntensity_10();
		if (L_2)
		{
			goto IL_001b;
		}
	}
	{
		float L_3 = ___feedbacksIntensity1;
		G_B4_0 = L_3;
		goto IL_0020;
	}

IL_001b:
	{
		G_B4_0 = (1.0f);
	}

IL_0020:
	{
		V_0 = G_B4_0;
		// MMDepthOfFieldShakeEvent.Trigger(ShakeFocusDistance, FeedbackDuration, RemapFocusDistanceZero, RemapFocusDistanceOne,
		//     ShakeAperture, RemapApertureZero, RemapApertureOne,
		//     ShakeFocalLength, RemapFocalLengthZero, RemapFocalLengthOne,
		//     RelativeValues, intensityMultiplier, Channel, ResetShakerValuesAfterShake, ResetTargetValuesAfterShake, NormalPlayDirection, Timing.TimescaleMode);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_4 = __this->get_ShakeFocusDistance_32();
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_6 = __this->get_RemapFocusDistanceZero_33();
		float L_7 = __this->get_RemapFocusDistanceOne_34();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_8 = __this->get_ShakeAperture_35();
		float L_9 = __this->get_RemapApertureZero_36();
		float L_10 = __this->get_RemapApertureOne_37();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = __this->get_ShakeFocalLength_38();
		float L_12 = __this->get_RemapFocalLengthZero_39();
		float L_13 = __this->get_RemapFocalLengthOne_40();
		bool L_14 = __this->get_RelativeValues_29();
		float L_15 = V_0;
		int32_t L_16 = __this->get_Channel_27();
		bool L_17 = __this->get_ResetShakerValuesAfterShake_30();
		bool L_18 = __this->get_ResetTargetValuesAfterShake_31();
		bool L_19;
		L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_20 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		int32_t L_21 = L_20->get_TimescaleMode_0();
		MMDepthOfFieldShakeEvent_Trigger_mEF87B2E979AAE6D96A885D4584372DD06A355790(L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_21, (bool)0, /*hidden argument*/NULL);
	}

IL_008d:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackDepthOfField_CustomStopFeedback_m056FB5F98585F46CD29F85502AF59290A53D9569 (MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active)
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_0062;
		}
	}
	{
		// MMDepthOfFieldShakeEvent.Trigger(ShakeFocusDistance, FeedbackDuration, RemapFocusDistanceZero, RemapFocusDistanceOne,
		//     ShakeAperture, RemapApertureZero, RemapApertureOne,
		//     ShakeFocalLength, RemapFocalLengthZero, RemapFocalLengthOne,
		//     RelativeValues, stop:true);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_3 = __this->get_ShakeFocusDistance_32();
		float L_4;
		L_4 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_5 = __this->get_RemapFocusDistanceZero_33();
		float L_6 = __this->get_RemapFocusDistanceOne_34();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = __this->get_ShakeAperture_35();
		float L_8 = __this->get_RemapApertureZero_36();
		float L_9 = __this->get_RemapApertureOne_37();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_10 = __this->get_ShakeFocalLength_38();
		float L_11 = __this->get_RemapFocalLengthZero_39();
		float L_12 = __this->get_RemapFocalLengthOne_40();
		bool L_13 = __this->get_RelativeValues_29();
		MMDepthOfFieldShakeEvent_Trigger_mEF87B2E979AAE6D96A885D4584372DD06A355790(L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, (1.0f), 0, (bool)1, (bool)1, (bool)1, 0, (bool)1, /*hidden argument*/NULL);
	}

IL_0062:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackDepthOfField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackDepthOfField__ctor_mD9E25AD23C0D1EF14BCE5AC1BB7CEA7F13247AF2 (MMFeedbackDepthOfField_t46B877BA3912A140285613A3626440FD56D716B2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float ShakeDuration = 2f;
		__this->set_ShakeDuration_28((2.0f));
		// public bool RelativeValues = true;
		__this->set_RelativeValues_29((bool)1);
		// public bool ResetShakerValuesAfterShake = true;
		__this->set_ResetShakerValuesAfterShake_30((bool)1);
		// public bool ResetTargetValuesAfterShake = true;
		__this->set_ResetTargetValuesAfterShake_31((bool)1);
		// public AnimationCurve ShakeFocusDistance = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakeFocusDistance_32(L_7);
		// public float RemapFocusDistanceOne = 3f;
		__this->set_RemapFocusDistanceOne_34((3.0f));
		// public AnimationCurve ShakeAperture = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_8 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_8;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_15 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_15, L_13, /*hidden argument*/NULL);
		__this->set_ShakeAperture_35(L_15);
		// public AnimationCurve ShakeFocalLength = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_16 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_17 = L_16;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_18), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_18);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_19 = L_17;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_20), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_20);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_21 = L_19;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_22;
		memset((&L_22), 0, sizeof(L_22));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_22), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_22);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_23 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_23, L_21, /*hidden argument*/NULL);
		__this->set_ShakeFocalLength_38(L_23);
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackGlobalPPVolumeAutoBlend_get_FeedbackDuration_m22F7C55A9F37A73B5A954618427319612AD7B259 (MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Mode == Modes.Override)
		int32_t L_0 = __this->get_Mode_28();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0010;
		}
	}
	{
		// return BlendDuration;
		float L_1 = __this->get_BlendDuration_30();
		return L_1;
	}

IL_0010:
	{
		// if (TargetAutoBlend == null)
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_2 = __this->get_TargetAutoBlend_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_2, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// return 0.1f;
		return (0.100000001f);
	}

IL_0024:
	{
		// return TargetAutoBlend.BlendDuration;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_4 = __this->get_TargetAutoBlend_27();
		float L_5 = L_4->get_BlendDuration_5();
		return L_5;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackGlobalPPVolumeAutoBlend_CustomPlayFeedback_m6D7CB85B59241B3E49A66D406068FFE7058E93BD (MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral85AF6E8AEABE7BAAAA9895F7C68A5F1AF46920DC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_0123;
		}
	}
	{
		// if (TargetAutoBlend == null)
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_1 = __this->get_TargetAutoBlend_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_1, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		// Debug.LogWarning(this.name + " : this MMFeedbackGlobalPPVolumeAutoBlend needs a TargetAutoBlend, please set one in its inspector.");
		String_t* L_3;
		L_3 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(__this, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(L_3, _stringLiteral85AF6E8AEABE7BAAAA9895F7C68A5F1AF46920DC, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7(L_4, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_002f:
	{
		// if (Mode == Modes.Default)
		int32_t L_5 = __this->get_Mode_28();
		if (L_5)
		{
			goto IL_0097;
		}
	}
	{
		// if (!NormalPlayDirection)
		bool L_6;
		L_6 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		if (L_6)
		{
			goto IL_006b;
		}
	}
	{
		// if (BlendAction == Actions.Blend)
		int32_t L_7 = __this->get_BlendAction_29();
		if (L_7)
		{
			goto IL_0053;
		}
	}
	{
		// TargetAutoBlend.BlendBack();
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_8 = __this->get_TargetAutoBlend_27();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::BlendBack() */, L_8);
		// return;
		return;
	}

IL_0053:
	{
		// if (BlendAction == Actions.BlendBack)
		int32_t L_9 = __this->get_BlendAction_29();
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0123;
		}
	}
	{
		// TargetAutoBlend.Blend();
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_10 = __this->get_TargetAutoBlend_27();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Blend() */, L_10);
		// return;
		return;
	}

IL_006b:
	{
		// if (BlendAction == Actions.Blend)
		int32_t L_11 = __this->get_BlendAction_29();
		if (L_11)
		{
			goto IL_007f;
		}
	}
	{
		// TargetAutoBlend.Blend();
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_12 = __this->get_TargetAutoBlend_27();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Blend() */, L_12);
		// return;
		return;
	}

IL_007f:
	{
		// if (BlendAction == Actions.BlendBack)
		int32_t L_13 = __this->get_BlendAction_29();
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_0123;
		}
	}
	{
		// TargetAutoBlend.BlendBack();
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_14 = __this->get_TargetAutoBlend_27();
		VirtActionInvoker0::Invoke(7 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::BlendBack() */, L_14);
		// return;
		return;
	}

IL_0097:
	{
		// TargetAutoBlend.BlendDuration = BlendDuration;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_15 = __this->get_TargetAutoBlend_27();
		float L_16 = __this->get_BlendDuration_30();
		L_15->set_BlendDuration_5(L_16);
		// TargetAutoBlend.Curve = BlendCurve;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_17 = __this->get_TargetAutoBlend_27();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_18 = __this->get_BlendCurve_31();
		L_17->set_Curve_6(L_18);
		// if (!NormalPlayDirection)
		bool L_19;
		L_19 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		if (L_19)
		{
			goto IL_00e5;
		}
	}
	{
		// TargetAutoBlend.InitialWeight = FinalWeight;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_20 = __this->get_TargetAutoBlend_27();
		float L_21 = __this->get_FinalWeight_33();
		L_20->set_InitialWeight_7(L_21);
		// TargetAutoBlend.FinalWeight = InitialWeight;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_22 = __this->get_TargetAutoBlend_27();
		float L_23 = __this->get_InitialWeight_32();
		L_22->set_FinalWeight_8(L_23);
		// }
		goto IL_0107;
	}

IL_00e5:
	{
		// TargetAutoBlend.InitialWeight = InitialWeight;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_24 = __this->get_TargetAutoBlend_27();
		float L_25 = __this->get_InitialWeight_32();
		L_24->set_InitialWeight_7(L_25);
		// TargetAutoBlend.FinalWeight = FinalWeight;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_26 = __this->get_TargetAutoBlend_27();
		float L_27 = __this->get_FinalWeight_33();
		L_26->set_FinalWeight_8(L_27);
	}

IL_0107:
	{
		// TargetAutoBlend.ResetToInitialValueOnEnd = ResetToInitialValueOnEnd;
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_28 = __this->get_TargetAutoBlend_27();
		bool L_29 = __this->get_ResetToInitialValueOnEnd_34();
		L_28->set_ResetToInitialValueOnEnd_14(L_29);
		// TargetAutoBlend.Blend();
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_30 = __this->get_TargetAutoBlend_27();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Blend() */, L_30);
	}

IL_0123:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackGlobalPPVolumeAutoBlend_CustomStopFeedback_m8054F90E9A7019655A1BEAD48A52C7A09B6F9AD2 (MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active && (TargetAutoBlend != null))
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_3 = __this->get_TargetAutoBlend_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0029;
		}
	}
	{
		// TargetAutoBlend.StopBlending();
		MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * L_5 = __this->get_TargetAutoBlend_27();
		VirtActionInvoker0::Invoke(9 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StopBlending() */, L_5);
	}

IL_0029:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackGlobalPPVolumeAutoBlend::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackGlobalPPVolumeAutoBlend__ctor_m880BD008B6B8FA389221EA7328802D209C2F31D6 (MMFeedbackGlobalPPVolumeAutoBlend_t3F1F9A0711351FA4CE1A5B0D155D53C961FF8F68 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float BlendDuration = 1f;
		__this->set_BlendDuration_30((1.0f));
		// public AnimationCurve BlendCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1f));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (1.0f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_5, L_3, /*hidden argument*/NULL);
		__this->set_BlendCurve_31(L_5);
		// public float FinalWeight = 1f;
		__this->set_FinalWeight_33((1.0f));
		// public bool ResetToInitialValueOnEnd = true;
		__this->set_ResetToInitialValueOnEnd_34((bool)1);
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackLensDistortion_get_FeedbackDuration_m1BBD1E3C6EB2C204731618BAA7B80455D816ABE4 (MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7 * __this, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); } set { Duration = value;  } }
		float L_0 = __this->get_Duration_28();
		float L_1;
		L_1 = VirtFuncInvoker1< float, float >::Invoke(34 /* System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single) */, __this, L_0);
		return L_1;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::set_FeedbackDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackLensDistortion_set_FeedbackDuration_m19B09D64E30BF3C10C4BB633CFC131925716F09D (MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); } set { Duration = value;  } }
		float L_0 = ___value0;
		__this->set_Duration_28(L_0);
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); } set { Duration = value;  } }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackLensDistortion_CustomPlayFeedback_mD62704646E4A16E40D92BCF4D1BA4A6AF6B8C433 (MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		// float intensityMultiplier = Timing.ConstantIntensity ? 1f : feedbacksIntensity;
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_1 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		bool L_2 = L_1->get_ConstantIntensity_10();
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		float L_3 = ___feedbacksIntensity1;
		G_B4_0 = L_3;
		goto IL_001d;
	}

IL_0018:
	{
		G_B4_0 = (1.0f);
	}

IL_001d:
	{
		V_0 = G_B4_0;
		// MMLensDistortionShakeEvent.Trigger(Intensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, intensityMultiplier,
		//     Channel, ResetShakerValuesAfterShake, ResetTargetValuesAfterShake, NormalPlayDirection, Timing.TimescaleMode);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_4 = __this->get_Intensity_32();
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_6 = __this->get_RemapIntensityZero_33();
		float L_7 = __this->get_RemapIntensityOne_34();
		bool L_8 = __this->get_RelativeIntensity_31();
		float L_9 = V_0;
		int32_t L_10 = __this->get_Channel_27();
		bool L_11 = __this->get_ResetShakerValuesAfterShake_29();
		bool L_12 = __this->get_ResetTargetValuesAfterShake_30();
		bool L_13;
		L_13 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_14 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		int32_t L_15 = L_14->get_TimescaleMode_0();
		MMLensDistortionShakeEvent_Trigger_m1164932C876A6FE15F489A45F51C4E56BFAEA8B6(L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_15, (bool)0, /*hidden argument*/NULL);
	}

IL_0066:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackLensDistortion_CustomStopFeedback_mFECD9E021EBEBFD6F44176E9D9AEF00A89C320D6 (MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active)
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		// MMLensDistortionShakeEvent.Trigger(Intensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, stop:true);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_3 = __this->get_Intensity_32();
		float L_4;
		L_4 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_5 = __this->get_RemapIntensityZero_33();
		float L_6 = __this->get_RemapIntensityOne_34();
		bool L_7 = __this->get_RelativeIntensity_31();
		MMLensDistortionShakeEvent_Trigger_m1164932C876A6FE15F489A45F51C4E56BFAEA8B6(L_3, L_4, L_5, L_6, L_7, (1.0f), 0, (bool)1, (bool)1, (bool)1, 0, (bool)1, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackLensDistortion::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackLensDistortion__ctor_m840EBA0C416822C558A5109CEDF47AADBF7C1B8B (MMFeedbackLensDistortion_tC2E22B32BC44432D52EDD922A426F6E9E1557BB7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float Duration = 0.5f;
		__this->set_Duration_28((0.5f));
		// public bool ResetShakerValuesAfterShake = true;
		__this->set_ResetShakerValuesAfterShake_29((bool)1);
		// public bool ResetTargetValuesAfterShake = true;
		__this->set_ResetTargetValuesAfterShake_30((bool)1);
		// public AnimationCurve Intensity = new AnimationCurve(new Keyframe(0, 0),
		//                                                             new Keyframe(0.2f, 1),
		//                                                             new Keyframe(0.25f, -1),
		//                                                             new Keyframe(0.35f, 0.7f),
		//                                                             new Keyframe(0.4f, -0.7f),
		//                                                             new Keyframe(0.6f, 0.3f),
		//                                                             new Keyframe(0.65f, -0.3f),
		//                                                             new Keyframe(0.8f, 0.1f),
		//                                                             new Keyframe(0.85f, -0.1f),
		//                                                             new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.200000003f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (0.25f), (-1.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_7 = L_5;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_8), (0.349999994f), (0.699999988f), /*hidden argument*/NULL);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_8);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_7;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.400000006f), (-0.699999988f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.600000024f), (0.300000012f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (0.649999976f), (-0.300000012f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_15 = L_13;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_16), (0.800000012f), (0.100000001f), /*hidden argument*/NULL);
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_16);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_17 = L_15;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_18), (0.850000024f), (-0.100000001f), /*hidden argument*/NULL);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_18);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_19 = L_17;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_20), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_20);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_21 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_21, L_19, /*hidden argument*/NULL);
		__this->set_Intensity_32(L_21);
		// public float RemapIntensityOne = 20f;
		__this->set_RemapIntensityOne_34((20.0f));
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::get_FeedbackDuration()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMFeedbackVignette_get_FeedbackDuration_mEC5404D60CC1E906346734038EAE463FF0A5888C (MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1 * __this, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); }  set { Duration = value;  } }
		float L_0 = __this->get_Duration_28();
		float L_1;
		L_1 = VirtFuncInvoker1< float, float >::Invoke(34 /* System.Single MoreMountains.Feedbacks.MMFeedback::ApplyTimeMultiplier(System.Single) */, __this, L_0);
		return L_1;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::set_FeedbackDuration(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackVignette_set_FeedbackDuration_m853FB36BD888634092DCEFCC94D967E090127606 (MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); }  set { Duration = value;  } }
		float L_0 = ___value0;
		__this->set_Duration_28(L_0);
		// public override float FeedbackDuration { get { return ApplyTimeMultiplier(Duration); }  set { Duration = value;  } }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::CustomPlayFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackVignette_CustomPlayFeedback_mD30CA31E4C3B4E7FDC0B91E495DF1EAEFCDA4306 (MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float G_B4_0 = 0.0f;
	{
		// if (Active)
		bool L_0 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_0)
		{
			goto IL_0066;
		}
	}
	{
		// float intensityMultiplier = Timing.ConstantIntensity ? 1f : feedbacksIntensity;
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_1 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		bool L_2 = L_1->get_ConstantIntensity_10();
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		float L_3 = ___feedbacksIntensity1;
		G_B4_0 = L_3;
		goto IL_001d;
	}

IL_0018:
	{
		G_B4_0 = (1.0f);
	}

IL_001d:
	{
		V_0 = G_B4_0;
		// MMVignetteShakeEvent.Trigger(Intensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, intensityMultiplier,
		//     Channel, ResetShakerValuesAfterShake, ResetTargetValuesAfterShake, NormalPlayDirection, Timing.TimescaleMode);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_4 = __this->get_Intensity_31();
		float L_5;
		L_5 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_6 = __this->get_RemapIntensityZero_32();
		float L_7 = __this->get_RemapIntensityOne_33();
		bool L_8 = __this->get_RelativeIntensity_34();
		float L_9 = V_0;
		int32_t L_10 = __this->get_Channel_27();
		bool L_11 = __this->get_ResetShakerValuesAfterShake_29();
		bool L_12 = __this->get_ResetTargetValuesAfterShake_30();
		bool L_13;
		L_13 = VirtFuncInvoker0< bool >::Invoke(31 /* System.Boolean MoreMountains.Feedbacks.MMFeedback::get_NormalPlayDirection() */, __this);
		MMFeedbackTiming_tA2ECBE1844BDDE1A8986173D25959D9A863892A6 * L_14 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Timing_7();
		int32_t L_15 = L_14->get_TimescaleMode_0();
		MMVignetteShakeEvent_Trigger_m6BE5A124916B56590046731459A95210B469BC3B(L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_15, (bool)0, /*hidden argument*/NULL);
	}

IL_0066:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::CustomStopFeedback(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackVignette_CustomStopFeedback_m559407CD1535750F28EC07D1553947D023B8757D (MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position0, float ___feedbacksIntensity1, const RuntimeMethod* method)
{
	{
		// base.CustomStopFeedback(position, feedbacksIntensity);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___position0;
		float L_1 = ___feedbacksIntensity1;
		MMFeedback_CustomStopFeedback_m637BE3F6114F85830A2E7073DEEF05B95CA79A83(__this, L_0, L_1, /*hidden argument*/NULL);
		// if (Active)
		bool L_2 = ((MMFeedback_t2F8D23D2254B4FDA6B88DE22430F36F4FC5880C4 *)__this)->get_Active_4();
		if (!L_2)
		{
			goto IL_003e;
		}
	}
	{
		// MMVignetteShakeEvent.Trigger(Intensity, FeedbackDuration, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, stop:true);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_3 = __this->get_Intensity_31();
		float L_4;
		L_4 = VirtFuncInvoker0< float >::Invoke(14 /* System.Single MoreMountains.Feedbacks.MMFeedback::get_FeedbackDuration() */, __this);
		float L_5 = __this->get_RemapIntensityZero_32();
		float L_6 = __this->get_RemapIntensityOne_33();
		bool L_7 = __this->get_RelativeIntensity_34();
		MMVignetteShakeEvent_Trigger_m6BE5A124916B56590046731459A95210B469BC3B(L_3, L_4, L_5, L_6, L_7, (1.0f), 0, (bool)1, (bool)1, (bool)1, 0, (bool)1, /*hidden argument*/NULL);
	}

IL_003e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMFeedbackVignette::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMFeedbackVignette__ctor_mE0361C4036F149D29E49A6414E142B52A1563A0F (MMFeedbackVignette_t6AD1C27A149184AD603934C1C4A36D64ABD229F1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float Duration = 0.2f;
		__this->set_Duration_28((0.200000003f));
		// public bool ResetShakerValuesAfterShake = true;
		__this->set_ResetShakerValuesAfterShake_29((bool)1);
		// public bool ResetTargetValuesAfterShake = true;
		__this->set_ResetTargetValuesAfterShake_30((bool)1);
		// public AnimationCurve Intensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_Intensity_31(L_7);
		// public float RemapIntensityOne = 1.0f;
		__this->set_RemapIntensityOne_33((1.0f));
		MMFeedback__ctor_m3A0FE0C8FEDD2D759EC31C4432C65DBD4B1A06A0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::GetTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float MMGlobalPostProcessingVolumeAutoBlend_GetTime_mE5641A532632B5EB3708754A94C68306BEABE131 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	{
		// return (TimeScale == TimeScales.Unscaled) ? Time.unscaledTime : Time.time;
		int32_t L_0 = __this->get_TimeScale_9();
		if ((((int32_t)L_0) == ((int32_t)1)))
		{
			goto IL_000f;
		}
	}
	{
		float L_1;
		L_1 = Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844(/*hidden argument*/NULL);
		return L_1;
	}

IL_000f:
	{
		float L_2;
		L_2 = Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258(/*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_Awake_mD3834A1F6F08F496BDFD987B632D68CA7CEF9EE1 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_21(L_1);
		// _volume.weight = InitialWeight;
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_21();
		float L_3 = __this->get_InitialWeight_7();
		L_2->set_weight_7(L_3);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_OnEnable_mA5A6C44F00C1719B4E5F56D491E084B0FB843933 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	{
		// if ((BlendTriggerMode == BlendTriggerModes.OnEnable) && !_blending)
		int32_t L_0 = __this->get_BlendTriggerMode_4();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get__blending_20();
		if (L_1)
		{
			goto IL_0016;
		}
	}
	{
		// Blend();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Blend() */, __this);
	}

IL_0016:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Blend()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_Blend_m2B4F75A614EB9A4727FEA395CBFBCD71A76C3363 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * G_B5_0 = NULL;
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * G_B6_1 = NULL;
	{
		// if (_blending && !Interruptable)
		bool L_0 = __this->get__blending_20();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = __this->get_Interruptable_12();
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		return;
	}

IL_0011:
	{
		// _initial = StartFromCurrentValue ? _volume.weight : InitialWeight;
		bool L_2 = __this->get_StartFromCurrentValue_13();
		G_B4_0 = __this;
		if (L_2)
		{
			G_B5_0 = __this;
			goto IL_0022;
		}
	}
	{
		float L_3 = __this->get_InitialWeight_7();
		G_B6_0 = L_3;
		G_B6_1 = G_B4_0;
		goto IL_002d;
	}

IL_0022:
	{
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_4 = __this->get__volume_21();
		float L_5 = L_4->get_weight_7();
		G_B6_0 = L_5;
		G_B6_1 = G_B5_0;
	}

IL_002d:
	{
		G_B6_1->set__initial_17(G_B6_0);
		// _destination = FinalWeight;
		float L_6 = __this->get_FinalWeight_8();
		__this->set__destination_18(L_6);
		// StartBlending();
		VirtActionInvoker0::Invoke(8 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StartBlending() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::BlendBack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_BlendBack_mCE347B8A9C52874236AC8E2336EB52C992C5C4D0 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * G_B5_0 = NULL;
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * G_B6_1 = NULL;
	{
		// if (_blending && !Interruptable)
		bool L_0 = __this->get__blending_20();
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		bool L_1 = __this->get_Interruptable_12();
		if (L_1)
		{
			goto IL_0011;
		}
	}
	{
		// return;
		return;
	}

IL_0011:
	{
		// _initial = StartFromCurrentValue ? _volume.weight : FinalWeight;
		bool L_2 = __this->get_StartFromCurrentValue_13();
		G_B4_0 = __this;
		if (L_2)
		{
			G_B5_0 = __this;
			goto IL_0022;
		}
	}
	{
		float L_3 = __this->get_FinalWeight_8();
		G_B6_0 = L_3;
		G_B6_1 = G_B4_0;
		goto IL_002d;
	}

IL_0022:
	{
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_4 = __this->get__volume_21();
		float L_5 = L_4->get_weight_7();
		G_B6_0 = L_5;
		G_B6_1 = G_B5_0;
	}

IL_002d:
	{
		G_B6_1->set__initial_17(G_B6_0);
		// _destination = InitialWeight;
		float L_6 = __this->get_InitialWeight_7();
		__this->set__destination_18(L_6);
		// StartBlending();
		VirtActionInvoker0::Invoke(8 /* System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StartBlending() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StartBlending()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_StartBlending_m43672D0EEFCB5097DFC58946257CC865729EE21A (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	{
		// _startTime = GetTime();
		float L_0;
		L_0 = MMGlobalPostProcessingVolumeAutoBlend_GetTime_mE5641A532632B5EB3708754A94C68306BEABE131(__this, /*hidden argument*/NULL);
		__this->set__startTime_19(L_0);
		// _blending = true;
		__this->set__blending_20((bool)1);
		// this.enabled = true;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)1, /*hidden argument*/NULL);
		// if (DisableVolumeOnZeroWeight)
		bool L_1 = __this->get_DisableVolumeOnZeroWeight_10();
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		// _volume.enabled = true;
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_21();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_2, (bool)1, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::StopBlending()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_StopBlending_mD64D6573421A3F34AB85DEC7325781AA1AA40435 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	{
		// _blending = false;
		__this->set__blending_20((bool)0);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend_Update_mAB3103D2A3D74DED8868617A031A03EB50A004C5 (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * G_B6_0 = NULL;
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * G_B5_0 = NULL;
	float G_B7_0 = 0.0f;
	PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * G_B7_1 = NULL;
	{
		// if (!_blending)
		bool L_0 = __this->get__blending_20();
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// float timeElapsed = (GetTime() - _startTime);
		float L_1;
		L_1 = MMGlobalPostProcessingVolumeAutoBlend_GetTime_mE5641A532632B5EB3708754A94C68306BEABE131(__this, /*hidden argument*/NULL);
		float L_2 = __this->get__startTime_19();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_2));
		// if (timeElapsed < BlendDuration)
		float L_3 = V_0;
		float L_4 = __this->get_BlendDuration_5();
		if ((!(((float)L_3) < ((float)L_4))))
		{
			goto IL_0065;
		}
	}
	{
		// float remapped = MMFeedbacksHelpers.Remap(timeElapsed, 0f, BlendDuration, 0f, 1f);
		float L_5 = V_0;
		float L_6 = __this->get_BlendDuration_5();
		float L_7;
		L_7 = MMFeedbacksHelpers_Remap_mF91639B3964272F011DC211808184EC25AF9571F(L_5, (0.0f), L_6, (0.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_7;
		// _volume.weight = Mathf.LerpUnclamped(_initial, _destination, Curve.Evaluate(remapped));
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_8 = __this->get__volume_21();
		float L_9 = __this->get__initial_17();
		float L_10 = __this->get__destination_18();
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_11 = __this->get_Curve_6();
		float L_12 = V_1;
		float L_13;
		L_13 = AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A(L_11, L_12, /*hidden argument*/NULL);
		float L_14;
		L_14 = Mathf_LerpUnclamped_mF68548D1AA22018863B6EBE911A5F7A959F94C1E(L_9, L_10, L_13, /*hidden argument*/NULL);
		L_8->set_weight_7(L_14);
		// }
		return;
	}

IL_0065:
	{
		// _volume.weight = ResetToInitialValueOnEnd ? _initial : _destination;
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_15 = __this->get__volume_21();
		bool L_16 = __this->get_ResetToInitialValueOnEnd_14();
		G_B5_0 = L_15;
		if (L_16)
		{
			G_B6_0 = L_15;
			goto IL_007b;
		}
	}
	{
		float L_17 = __this->get__destination_18();
		G_B7_0 = L_17;
		G_B7_1 = G_B5_0;
		goto IL_0081;
	}

IL_007b:
	{
		float L_18 = __this->get__initial_17();
		G_B7_0 = L_18;
		G_B7_1 = G_B6_0;
	}

IL_0081:
	{
		G_B7_1->set_weight_7(G_B7_0);
		// _blending = false;
		__this->set__blending_20((bool)0);
		// if (DisableVolumeOnZeroWeight && (_volume.weight == 0f))
		bool L_19 = __this->get_DisableVolumeOnZeroWeight_10();
		if (!L_19)
		{
			goto IL_00b3;
		}
	}
	{
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_20 = __this->get__volume_21();
		float L_21 = L_20->get_weight_7();
		if ((!(((float)L_21) == ((float)(0.0f)))))
		{
			goto IL_00b3;
		}
	}
	{
		// _volume.enabled = false;
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_22 = __this->get__volume_21();
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(L_22, (bool)0, /*hidden argument*/NULL);
	}

IL_00b3:
	{
		// if (DisableSelfAfterEnd)
		bool L_23 = __this->get_DisableSelfAfterEnd_11();
		if (!L_23)
		{
			goto IL_00c2;
		}
	}
	{
		// this.enabled = false;
		Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32(__this, (bool)0, /*hidden argument*/NULL);
	}

IL_00c2:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMGlobalPostProcessingVolumeAutoBlend::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMGlobalPostProcessingVolumeAutoBlend__ctor_m9FE0953B52ECB231DE17B3CBF30078D6A2BB4DBD (MMGlobalPostProcessingVolumeAutoBlend_t2EAC1A780682DA007ECF86C53CCADE80BDE3F744 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public float BlendDuration = 1f;
		__this->set_BlendDuration_5((1.0f));
		// public AnimationCurve Curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1f));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (1.0f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_5 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_5, L_3, /*hidden argument*/NULL);
		__this->set_Curve_6(L_5);
		// public float FinalWeight = 1f;
		__this->set_FinalWeight_8((1.0f));
		// public TimeScales TimeScale = TimeScales.Unscaled;
		__this->set_TimeScale_9(1);
		// public bool DisableVolumeOnZeroWeight = true;
		__this->set_DisableVolumeOnZeroWeight_10((bool)1);
		// public bool DisableSelfAfterEnd = true;
		__this->set_DisableSelfAfterEnd_11((bool)1);
		// public bool Interruptable = true;
		__this->set_Interruptable_12((bool)1);
		// public bool StartFromCurrentValue = true;
		__this->set_StartFromCurrentValue_13((bool)1);
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * V_0 = NULL;
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * V_1 = NULL;
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * V_2 = NULL;
	{
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = ((MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields*)il2cpp_codegen_static_fields_for(MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_1 = V_0;
		V_1 = L_1;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_2 = V_1;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)CastclassSealed((RuntimeObject*)L_4, Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var));
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_5 = V_2;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_6 = V_1;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *>((Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 **)(((MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields*)il2cpp_codegen_static_fields_for(MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_8 = V_0;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)L_8) == ((RuntimeObject*)(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * V_0 = NULL;
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * V_1 = NULL;
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * V_2 = NULL;
	{
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = ((MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields*)il2cpp_codegen_static_fields_for(MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_1 = V_0;
		V_1 = L_1;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_2 = V_1;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)CastclassSealed((RuntimeObject*)L_4, Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var));
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_5 = V_2;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_6 = V_1;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *>((Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 **)(((MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields*)il2cpp_codegen_static_fields_for(MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_8 = V_0;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)L_8) == ((RuntimeObject*)(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_Register_m229BE3C83EF1126656363CB35AC2C2B577E33B1D (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent += callback;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = ___callback0;
		MMLensDistortionShakeEvent_add_OnEvent_mCECB53DB75529ECEA5AB811B577F1630D150D2BE(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_Unregister_m1BD5E15B3D8C854DFE5B2603B65BA9423D543197 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent -= callback;
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = ___callback0;
		MMLensDistortionShakeEvent_remove_OnEvent_mF5997CB05B2677D1529A664CDC5DCBE521571398(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShakeEvent_Trigger_m1164932C876A6FE15F489A45F51C4E56BFAEA8B6 (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * G_B2_0 = NULL;
	Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * G_B1_0 = NULL;
	{
		// OnEvent?.Invoke(intensity, duration, remapMin, remapMax, relativeIntensity, feedbacksIntensity, channel, resetShakerValuesAfterShake, resetTargetValuesAfterShake, forwardDirection, timescaleMode, stop);
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = ((MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_StaticFields*)il2cpp_codegen_static_fields_for(MMLensDistortionShakeEvent_t5BEF04C05AD6EE3110EC3A14DA74F00236120266_il2cpp_TypeInfo_var))->get_OnEvent_0();
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___intensity0;
		float L_3 = ___duration1;
		float L_4 = ___remapMin2;
		float L_5 = ___remapMax3;
		bool L_6 = ___relativeIntensity4;
		float L_7 = ___feedbacksIntensity5;
		int32_t L_8 = ___channel6;
		bool L_9 = ___resetShakerValuesAfterShake7;
		bool L_10 = ___resetTargetValuesAfterShake8;
		bool L_11 = ___forwardDirection9;
		int32_t L_12 = ___timescaleMode10;
		bool L_13 = ___stop11;
		Delegate_Invoke_m1297C5964A2FEF0A3AB0D42415275F9992AD18A1(G_B2_0, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_Initialization_m3F2D70137D5A32BFC0036599991155A2B8E03F71 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisLensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2_mB931D1A60AFCD5DEE42FD9B8430B4B74629BBF2A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3(__this, /*hidden argument*/NULL);
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_22(L_1);
		// _volume.profile.TryGetSettings(out _lensDistortion);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_22();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3;
		L_3 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_2, /*hidden argument*/NULL);
		LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 ** L_4 = __this->get_address_of__lensDistortion_23();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisLensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2_mB931D1A60AFCD5DEE42FD9B8430B4B74629BBF2A(L_3, (LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisLensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2_mB931D1A60AFCD5DEE42FD9B8430B4B74629BBF2A_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_Reset_m91A2648670EB98E2B5C2B3693315F27BDC8AAC6B (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	{
		// ShakeDuration = 0.8f;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5((0.800000012f));
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::Shake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_Shake_mE4AE6178E588A3628CDEDE41A48F7FF182FFDE35 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float newValue = ShakeFloat(ShakeIntensity, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, _initialIntensity);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_ShakeIntensity_19();
		float L_1 = __this->get_RemapIntensityZero_20();
		float L_2 = __this->get_RemapIntensityOne_21();
		bool L_3 = __this->get_RelativeIntensity_18();
		float L_4 = __this->get__initialIntensity_24();
		float L_5;
		L_5 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4);
		V_0 = L_5;
		// _lensDistortion.intensity.Override(newValue);
		LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 * L_6 = __this->get__lensDistortion_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_intensity_7();
		float L_8 = V_0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::GrabInitialValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_GrabInitialValues_m8217D2424E69AA5D97D4AAC8680B114C8AA98230 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _initialIntensity = _lensDistortion.intensity;
		LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 * L_0 = __this->get__lensDistortion_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_7();
		float L_2;
		L_2 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_1, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialIntensity_24(L_2);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::OnMMLensDistortionShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_OnMMLensDistortionShakeEvent_m8075A8A961BFD278296E52689D0304FBEF3C29F5 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!CheckEventAllowed(channel) || (!Interruptible && Shaking))
		int32_t L_0 = ___channel6;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		bool L_2;
		L_2 = VirtFuncInvoker4< bool, int32_t, bool, float, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(24 /* System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3) */, __this, L_0, (bool)0, (0.0f), L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Interruptible_7();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Shaking_9();
		if (!L_4)
		{
			goto IL_002a;
		}
	}

IL_0029:
	{
		// return;
		return;
	}

IL_002a:
	{
		// if (stop)
		bool L_5 = ___stop11;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// Stop();
		VirtActionInvoker0::Invoke(21 /* System.Void MoreMountains.Feedbacks.MMShaker::Stop() */, __this);
		// return;
		return;
	}

IL_0035:
	{
		// _resetShakerValuesAfterShake = resetShakerValuesAfterShake;
		bool L_6 = ___resetShakerValuesAfterShake7;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetShakerValuesAfterShake_15(L_6);
		// _resetTargetValuesAfterShake = resetTargetValuesAfterShake;
		bool L_7 = ___resetTargetValuesAfterShake8;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetTargetValuesAfterShake_16(L_7);
		// if (resetShakerValuesAfterShake)
		bool L_8 = ___resetShakerValuesAfterShake7;
		if (!L_8)
		{
			goto IL_0085;
		}
	}
	{
		// _originalShakeDuration = ShakeDuration;
		float L_9 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_ShakeDuration_5();
		__this->set__originalShakeDuration_25(L_9);
		// _originalShakeIntensity = ShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_10 = __this->get_ShakeIntensity_19();
		__this->set__originalShakeIntensity_26(L_10);
		// _originalRemapIntensityZero = RemapIntensityZero;
		float L_11 = __this->get_RemapIntensityZero_20();
		__this->set__originalRemapIntensityZero_27(L_11);
		// _originalRemapIntensityOne = RemapIntensityOne;
		float L_12 = __this->get_RemapIntensityOne_21();
		__this->set__originalRemapIntensityOne_28(L_12);
		// _originalRelativeIntensity = RelativeIntensity;
		bool L_13 = __this->get_RelativeIntensity_18();
		__this->set__originalRelativeIntensity_29(L_13);
	}

IL_0085:
	{
		// TimescaleMode = timescaleMode;
		int32_t L_14 = ___timescaleMode10;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_TimescaleMode_11(L_14);
		// ShakeDuration = duration;
		float L_15 = ___duration1;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_15);
		// ShakeIntensity = intensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_16 = ___intensity0;
		__this->set_ShakeIntensity_19(L_16);
		// RemapIntensityZero = remapMin * feedbacksIntensity;
		float L_17 = ___remapMin2;
		float L_18 = ___feedbacksIntensity5;
		__this->set_RemapIntensityZero_20(((float)il2cpp_codegen_multiply((float)L_17, (float)L_18)));
		// RemapIntensityOne = remapMax * feedbacksIntensity;
		float L_19 = ___remapMax3;
		float L_20 = ___feedbacksIntensity5;
		__this->set_RemapIntensityOne_21(((float)il2cpp_codegen_multiply((float)L_19, (float)L_20)));
		// RelativeIntensity = relativeIntensity;
		bool L_21 = ___relativeIntensity4;
		__this->set_RelativeIntensity_18(L_21);
		// ForwardDirection = forwardDirection;
		bool L_22 = ___forwardDirection9;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ForwardDirection_10(L_22);
		// Play();
		VirtActionInvoker0::Invoke(20 /* System.Void MoreMountains.Feedbacks.MMShaker::Play() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_ResetTargetValues_mC22604AA6A91B8EC00B5EC4B77BE92BC4F55F5C1 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetTargetValues();
		MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2(__this, /*hidden argument*/NULL);
		// _lensDistortion.intensity.Override(_initialIntensity);
		LensDistortion_t6D6F5FBE380034E4890743C7AA799114CB548EC2 * L_0 = __this->get__lensDistortion_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_7();
		float L_2 = __this->get__initialIntensity_24();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_ResetShakerValues_m3617D23C7C43C812DAD87DD1587F6746605C37E4 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	{
		// base.ResetShakerValues();
		MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092(__this, /*hidden argument*/NULL);
		// ShakeDuration = _originalShakeDuration;
		float L_0 = __this->get__originalShakeDuration_25();
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_0);
		// ShakeIntensity = _originalShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_1 = __this->get__originalShakeIntensity_26();
		__this->set_ShakeIntensity_19(L_1);
		// RemapIntensityZero = _originalRemapIntensityZero;
		float L_2 = __this->get__originalRemapIntensityZero_27();
		__this->set_RemapIntensityZero_20(L_2);
		// RemapIntensityOne = _originalRemapIntensityOne;
		float L_3 = __this->get__originalRemapIntensityOne_28();
		__this->set_RemapIntensityOne_21(L_3);
		// RelativeIntensity = _originalRelativeIntensity;
		bool L_4 = __this->get__originalRelativeIntensity_29();
		__this->set_RelativeIntensity_18(L_4);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_StartListening_m2F22B87B5C8DB6E4D3952F7DE72661D3F0C45EA5 (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StartListening();
		MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F(__this, /*hidden argument*/NULL);
		// MMLensDistortionShakeEvent.Register(OnMMLensDistortionShakeEvent);
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)il2cpp_codegen_object_new(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var);
		Delegate__ctor_m6DD24C890F271356820106BCD3059A26CE2B05C5(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMLensDistortionShakeEvent_Register_m229BE3C83EF1126656363CB35AC2C2B577E33B1D(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker_StopListening_mEED6066F0DCA2FC1713BE34B6F9A542558248A0E (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StopListening();
		MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078(__this, /*hidden argument*/NULL);
		// MMLensDistortionShakeEvent.Unregister(OnMMLensDistortionShakeEvent);
		Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * L_0 = (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 *)il2cpp_codegen_object_new(Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95_il2cpp_TypeInfo_var);
		Delegate__ctor_m6DD24C890F271356820106BCD3059A26CE2B05C5(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMLensDistortionShakeEvent_Unregister_m1BD5E15B3D8C854DFE5B2603B65BA9423D543197(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMLensDistortionShaker__ctor_m5DF778FA2EF55456DFFF16300D6595DA5275344A (MMLensDistortionShaker_t168A4D9C5DA738B955839DAFE4B610F520888EE1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public AnimationCurve ShakeIntensity = new AnimationCurve(new Keyframe(0, 0),
		//                                                             new Keyframe(0.2f, 1),
		//                                                             new Keyframe(0.25f, -1),
		//                                                             new Keyframe(0.35f, 0.7f),
		//                                                             new Keyframe(0.4f, -0.7f),
		//                                                             new Keyframe(0.6f, 0.3f),
		//                                                             new Keyframe(0.65f, -0.3f),
		//                                                             new Keyframe(0.8f, 0.1f),
		//                                                             new Keyframe(0.85f, -0.1f),
		//                                                             new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.200000003f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (0.25f), (-1.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_7 = L_5;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_8), (0.349999994f), (0.699999988f), /*hidden argument*/NULL);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_8);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_9 = L_7;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_10), (0.400000006f), (-0.699999988f), /*hidden argument*/NULL);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_10);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_11 = L_9;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_12), (0.600000024f), (0.300000012f), /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(5), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_12);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_13 = L_11;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_14), (0.649999976f), (-0.300000012f), /*hidden argument*/NULL);
		(L_13)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(6), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_14);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_15 = L_13;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_16;
		memset((&L_16), 0, sizeof(L_16));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_16), (0.800000012f), (0.100000001f), /*hidden argument*/NULL);
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(7), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_16);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_17 = L_15;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_18;
		memset((&L_18), 0, sizeof(L_18));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_18), (0.850000024f), (-0.100000001f), /*hidden argument*/NULL);
		(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(8), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_18);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_19 = L_17;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_20), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_19)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)9)), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_20);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_21 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_21, L_19, /*hidden argument*/NULL);
		__this->set_ShakeIntensity_19(L_21);
		// public float RemapIntensityOne = 50f;
		__this->set_RemapIntensityOne_21((50.0f));
		MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::add_OnEvent(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * V_0 = NULL;
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * V_1 = NULL;
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * V_2 = NULL;
	{
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = ((MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields*)il2cpp_codegen_static_fields_for(MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_1 = V_0;
		V_1 = L_1;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_2 = V_1;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Combine_m631D10D6CFF81AB4F237B9D549B235A54F45FA55(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)CastclassSealed((RuntimeObject*)L_4, Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var));
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_5 = V_2;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_6 = V_1;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *>((Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD **)(((MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields*)il2cpp_codegen_static_fields_for(MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_8 = V_0;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)L_8) == ((RuntimeObject*)(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::remove_OnEvent(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * V_0 = NULL;
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * V_1 = NULL;
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * V_2 = NULL;
	{
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = ((MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields*)il2cpp_codegen_static_fields_for(MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var))->get_OnEvent_0();
		V_0 = L_0;
	}

IL_0006:
	{
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_1 = V_0;
		V_1 = L_1;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_2 = V_1;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_3 = ___value0;
		Delegate_t * L_4;
		L_4 = Delegate_Remove_m8B4AD17254118B2904720D55C9B34FB3DCCBD7D4(L_2, L_3, /*hidden argument*/NULL);
		V_2 = ((Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)CastclassSealed((RuntimeObject*)L_4, Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var));
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_5 = V_2;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_6 = V_1;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_7;
		L_7 = InterlockedCompareExchangeImpl<Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *>((Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD **)(((MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields*)il2cpp_codegen_static_fields_for(MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var))->get_address_of_OnEvent_0()), L_5, L_6);
		V_0 = L_7;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_8 = V_0;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_9 = V_1;
		if ((!(((RuntimeObject*)(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)L_8) == ((RuntimeObject*)(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)L_9))))
		{
			goto IL_0006;
		}
	}
	{
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Register(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_Register_m5D4664B4F351C71FA9CADC2A1026835F614EB11E (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent += callback;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = ___callback0;
		MMVignetteShakeEvent_add_OnEvent_m6AB8331DCEB2CB15E715E80E1E3C1249A098C290(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Unregister(MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_Unregister_m96CD18AB24B6DAE4FF1DD38064621ABBA6B6E974 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * ___callback0, const RuntimeMethod* method)
{
	{
		// OnEvent -= callback;
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = ___callback0;
		MMVignetteShakeEvent_remove_OnEvent_m9B45A2C1666BAFABF3DC4636384E38E3C99D5563(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent::Trigger(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShakeEvent_Trigger_m6BE5A124916B56590046731459A95210B469BC3B (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * G_B2_0 = NULL;
	Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * G_B1_0 = NULL;
	{
		// OnEvent?.Invoke(intensity, duration, remapMin, remapMax, relativeIntensity, feedbacksIntensity, channel, resetShakerValuesAfterShake, resetTargetValuesAfterShake, forwardDirection, timescaleMode, stop);
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = ((MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_StaticFields*)il2cpp_codegen_static_fields_for(MMVignetteShakeEvent_tAD299E5E355F788165DA5492875E1BFEBD69BF6A_il2cpp_TypeInfo_var))->get_OnEvent_0();
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_000a;
		}
	}
	{
		return;
	}

IL_000a:
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_2 = ___intensity0;
		float L_3 = ___duration1;
		float L_4 = ___remapMin2;
		float L_5 = ___remapMax3;
		bool L_6 = ___relativeIntensity4;
		float L_7 = ___feedbacksIntensity5;
		int32_t L_8 = ___channel6;
		bool L_9 = ___resetShakerValuesAfterShake7;
		bool L_10 = ___resetTargetValuesAfterShake8;
		bool L_11 = ___forwardDirection9;
		int32_t L_12 = ___timescaleMode10;
		bool L_13 = ___stop11;
		Delegate_Invoke_m7C0F4E59C8EBF4711224DFB293B92B7E9BC36D84(G_B2_0, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_Initialization_m71D7EF0DF87972186BA2EDB5929F3D7E9623376F (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PostProcessProfile_TryGetSettings_TisVignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139_m2FD6F6813492CA2C85F67666F4B699C733783BD8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.Initialization();
		MMShaker_Initialization_mA0C4D4855C7877F7FD4BE6DB7C55D309A53753B3(__this, /*hidden argument*/NULL);
		// _volume = this.gameObject.GetComponent<PostProcessVolume>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_1;
		L_1 = GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997(L_0, /*hidden argument*/GameObject_GetComponent_TisPostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF_m632F81C41B0A479BC797669550F90ED21AE2B997_RuntimeMethod_var);
		__this->set__volume_22(L_1);
		// _volume.profile.TryGetSettings(out _vignette);
		PostProcessVolume_tB8BE89D63E45B7CB8C7F634E2F57059C40B426AF * L_2 = __this->get__volume_22();
		PostProcessProfile_tD3601AEC44875A647F659988085F06DE07BCD94E * L_3;
		L_3 = PostProcessVolume_get_profile_m40F01FBBA48C74FBCAA82CC71390FE5289418F55(L_2, /*hidden argument*/NULL);
		Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 ** L_4 = __this->get_address_of__vignette_23();
		bool L_5;
		L_5 = PostProcessProfile_TryGetSettings_TisVignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139_m2FD6F6813492CA2C85F67666F4B699C733783BD8(L_3, (Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 **)L_4, /*hidden argument*/PostProcessProfile_TryGetSettings_TisVignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139_m2FD6F6813492CA2C85F67666F4B699C733783BD8_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::SetVignette(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_SetVignette_m23161572032684FEEE32D330EBD7BDBF3455F505 (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, float ___newValue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _vignette.intensity.Override(newValue);
		Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * L_0 = __this->get__vignette_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_10();
		float L_2 = ___newValue0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::Shake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_Shake_m1C4FEF73285B6D8A0F8FD6BECB2F3F4E1C149E2F (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		// float newValue = ShakeFloat(ShakeIntensity, RemapIntensityZero, RemapIntensityOne, RelativeIntensity, _initialIntensity);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_0 = __this->get_ShakeIntensity_19();
		float L_1 = __this->get_RemapIntensityZero_20();
		float L_2 = __this->get_RemapIntensityOne_21();
		bool L_3 = __this->get_RelativeIntensity_18();
		float L_4 = __this->get__initialIntensity_24();
		float L_5;
		L_5 = VirtFuncInvoker5< float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float >::Invoke(13 /* System.Single MoreMountains.Feedbacks.MMShaker::ShakeFloat(UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single) */, __this, L_0, L_1, L_2, L_3, L_4);
		V_0 = L_5;
		// _vignette.intensity.Override(newValue);
		Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * L_6 = __this->get__vignette_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_7 = L_6->get_intensity_10();
		float L_8 = V_0;
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_7, L_8, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::GrabInitialValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_GrabInitialValues_mE19E836C8042A6CDE21E85F5EB0CB8C4DEEBCDAB (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _initialIntensity = _vignette.intensity;
		Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * L_0 = __this->get__vignette_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_10();
		float L_2;
		L_2 = ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_inline(L_1, /*hidden argument*/ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_RuntimeMethod_var);
		__this->set__initialIntensity_24(L_2);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::OnVignetteShakeEvent(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_OnVignetteShakeEvent_m08B6C9651953FCE804164A9C74AA96568D8AB2AA (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!CheckEventAllowed(channel) || (!Interruptible && Shaking))
		int32_t L_0 = ___channel6;
		il2cpp_codegen_initobj((&V_0), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = V_0;
		bool L_2;
		L_2 = VirtFuncInvoker4< bool, int32_t, bool, float, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  >::Invoke(24 /* System.Boolean MoreMountains.Feedbacks.MMShaker::CheckEventAllowed(System.Int32,System.Boolean,System.Single,UnityEngine.Vector3) */, __this, L_0, (bool)0, (0.0f), L_1);
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		bool L_3 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Interruptible_7();
		if (L_3)
		{
			goto IL_002a;
		}
	}
	{
		bool L_4 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_Shaking_9();
		if (!L_4)
		{
			goto IL_002a;
		}
	}

IL_0029:
	{
		// return;
		return;
	}

IL_002a:
	{
		// if (stop)
		bool L_5 = ___stop11;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// Stop();
		VirtActionInvoker0::Invoke(21 /* System.Void MoreMountains.Feedbacks.MMShaker::Stop() */, __this);
		// return;
		return;
	}

IL_0035:
	{
		// _resetShakerValuesAfterShake = resetShakerValuesAfterShake;
		bool L_6 = ___resetShakerValuesAfterShake7;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetShakerValuesAfterShake_15(L_6);
		// _resetTargetValuesAfterShake = resetTargetValuesAfterShake;
		bool L_7 = ___resetTargetValuesAfterShake8;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set__resetTargetValuesAfterShake_16(L_7);
		// if (resetShakerValuesAfterShake)
		bool L_8 = ___resetShakerValuesAfterShake7;
		if (!L_8)
		{
			goto IL_0085;
		}
	}
	{
		// _originalShakeDuration = ShakeDuration;
		float L_9 = ((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->get_ShakeDuration_5();
		__this->set__originalShakeDuration_25(L_9);
		// _originalShakeIntensity = ShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_10 = __this->get_ShakeIntensity_19();
		__this->set__originalShakeIntensity_26(L_10);
		// _originalRemapIntensityZero = RemapIntensityZero;
		float L_11 = __this->get_RemapIntensityZero_20();
		__this->set__originalRemapIntensityZero_27(L_11);
		// _originalRemapIntensityOne = RemapIntensityOne;
		float L_12 = __this->get_RemapIntensityOne_21();
		__this->set__originalRemapIntensityOne_28(L_12);
		// _originalRelativeIntensity = RelativeIntensity;
		bool L_13 = __this->get_RelativeIntensity_18();
		__this->set__originalRelativeIntensity_29(L_13);
	}

IL_0085:
	{
		// TimescaleMode = timescaleMode;
		int32_t L_14 = ___timescaleMode10;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_TimescaleMode_11(L_14);
		// ShakeDuration = duration;
		float L_15 = ___duration1;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_15);
		// ShakeIntensity = intensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_16 = ___intensity0;
		__this->set_ShakeIntensity_19(L_16);
		// RemapIntensityZero = remapMin * feedbacksIntensity;
		float L_17 = ___remapMin2;
		float L_18 = ___feedbacksIntensity5;
		__this->set_RemapIntensityZero_20(((float)il2cpp_codegen_multiply((float)L_17, (float)L_18)));
		// RemapIntensityOne = remapMax * feedbacksIntensity;
		float L_19 = ___remapMax3;
		float L_20 = ___feedbacksIntensity5;
		__this->set_RemapIntensityOne_21(((float)il2cpp_codegen_multiply((float)L_19, (float)L_20)));
		// RelativeIntensity = relativeIntensity;
		bool L_21 = ___relativeIntensity4;
		__this->set_RelativeIntensity_18(L_21);
		// ForwardDirection = forwardDirection;
		bool L_22 = ___forwardDirection9;
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ForwardDirection_10(L_22);
		// Play();
		VirtActionInvoker0::Invoke(20 /* System.Void MoreMountains.Feedbacks.MMShaker::Play() */, __this);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::ResetTargetValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_ResetTargetValues_m0F8F6BCF1D3F08E86742486FC2C876B6DDC41502 (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.ResetTargetValues();
		MMShaker_ResetTargetValues_m4A621683BB61D3FD391AAC8F7983528F725B24C2(__this, /*hidden argument*/NULL);
		// _vignette.intensity.Override(_initialIntensity);
		Vignette_tFBCC5617358CAABF8B6D55C40CEA6406FB852139 * L_0 = __this->get__vignette_23();
		FloatParameter_t4C8D8D2C3227E9CAAC57869A84F0AE22F63E1AD0 * L_1 = L_0->get_intensity_10();
		float L_2 = __this->get__initialIntensity_24();
		ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205(L_1, L_2, /*hidden argument*/ParameterOverride_1_Override_m7EBEFD038D07C54BA52641BF095F9D78A59F8205_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::ResetShakerValues()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_ResetShakerValues_m42FF67C335EE9AC14DA12E3B0948D21789F5EEC6 (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	{
		// base.ResetShakerValues();
		MMShaker_ResetShakerValues_m93AA8579FE316AD1BBCB482D7DC47593B5F70092(__this, /*hidden argument*/NULL);
		// ShakeDuration = _originalShakeDuration;
		float L_0 = __this->get__originalShakeDuration_25();
		((MMShaker_t8F894BEBCBCFAB1CAF0AD67F76B98DE9C80E98F1 *)__this)->set_ShakeDuration_5(L_0);
		// ShakeIntensity = _originalShakeIntensity;
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_1 = __this->get__originalShakeIntensity_26();
		__this->set_ShakeIntensity_19(L_1);
		// RemapIntensityZero = _originalRemapIntensityZero;
		float L_2 = __this->get__originalRemapIntensityZero_27();
		__this->set_RemapIntensityZero_20(L_2);
		// RemapIntensityOne = _originalRemapIntensityOne;
		float L_3 = __this->get__originalRemapIntensityOne_28();
		__this->set_RemapIntensityOne_21(L_3);
		// RelativeIntensity = _originalRelativeIntensity;
		bool L_4 = __this->get__originalRelativeIntensity_29();
		__this->set_RelativeIntensity_18(L_4);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::StartListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_StartListening_m7965906DC4243A36D24F3FE1BE331124E6206C52 (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StartListening();
		MMShaker_StartListening_m2FD2D6DA250E2613C19EB5F54D1B4F78803D644F(__this, /*hidden argument*/NULL);
		// MMVignetteShakeEvent.Register(OnVignetteShakeEvent);
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)il2cpp_codegen_object_new(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var);
		Delegate__ctor_mB4D10F50ECD2FE9F53574366D7BD1C9BF9635F9E(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMVignetteShakeEvent_Register_m5D4664B4F351C71FA9CADC2A1026835F614EB11E(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::StopListening()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker_StopListening_m082550932C49B13099CC168F6AC01B2BFE6F5B4F (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.StopListening();
		MMShaker_StopListening_m1D60A4786564D907C4378080302EC52998682078(__this, /*hidden argument*/NULL);
		// MMVignetteShakeEvent.Unregister(OnVignetteShakeEvent);
		Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * L_0 = (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD *)il2cpp_codegen_object_new(Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD_il2cpp_TypeInfo_var);
		Delegate__ctor_mB4D10F50ECD2FE9F53574366D7BD1C9BF9635F9E(L_0, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 26)), /*hidden argument*/NULL);
		MMVignetteShakeEvent_Unregister_m96CD18AB24B6DAE4FF1DD38064621ABBA6B6E974(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShaker::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MMVignetteShaker__ctor_mAA731A262733B046A3B9E040519821CD7101ABA7 (MMVignetteShaker_t74896ABF30BCA70C39962B5E650A762645D2A48F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public bool RelativeIntensity = true;
		__this->set_RelativeIntensity_18((bool)1);
		// public AnimationCurve ShakeIntensity = new AnimationCurve(new Keyframe(0, 0), new Keyframe(0.5f, 1), new Keyframe(1, 0));
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_0 = (KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC*)SZArrayNew(KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC_il2cpp_TypeInfo_var, (uint32_t)3);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_1 = L_0;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_2), (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_2);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_3 = L_1;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_4), (0.5f), (1.0f), /*hidden argument*/NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_4);
		KeyframeU5BU5D_tF817D582FE6B521008A03DDBB5D1326DD78DB0BC* L_5 = L_3;
		Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Keyframe__ctor_mE08ED8666CB420F48B8D95B7D6B305A5ED0CFD9C((&L_6), (1.0f), (0.0f), /*hidden argument*/NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Keyframe_tBEEE79DF5E970E48A8972FFFCE8B25A6068ACE9F )L_6);
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * L_7 = (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *)il2cpp_codegen_object_new(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0(L_7, L_5, /*hidden argument*/NULL);
		__this->set_ShakeIntensity_19(L_7);
		// public float RemapIntensityOne = 0.1f;
		__this->set_RemapIntensityOne_21((0.100000001f));
		MMShaker__ctor_m19139ACF2B49DCE913269ED0C2D23D9B08E85C74(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, const RuntimeMethod* method)
{




	typedef void (DEFAULT_CALL *PInvokeFunc)(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, int32_t, float, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___intensity0' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____intensity0_marshaled = {};
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___intensity0, ____intensity0_marshaled);
	}

	// Marshaling of parameter '___threshold4' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____threshold4_marshaled = {};
	if (___threshold4 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___threshold4, ____threshold4_marshaled);
	}

	// Native function invocation
	il2cppPInvokeFunc(___intensity0 != NULL ? (&____intensity0_marshaled) : NULL, ___duration1, ___remapMin2, ___remapMax3, ___threshold4 != NULL ? (&____threshold4_marshaled) : NULL, ___remapThresholdMin5, ___remapThresholdMax6, static_cast<int32_t>(___relativeIntensity7), ___feedbacksIntensity8, ___channel9, static_cast<int32_t>(___resetShakerValuesAfterShake10), static_cast<int32_t>(___resetTargetValuesAfterShake11), static_cast<int32_t>(___forwardDirection12), ___timescaleMode13, static_cast<int32_t>(___stop14));

	// Marshaling of parameter '___intensity0' back from native representation
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____intensity0_marshaled, *___intensity0);
	}

	// Marshaling cleanup of parameter '___intensity0' native representation
	if ((&____intensity0_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____intensity0_marshaled);
	}

	// Marshaling of parameter '___threshold4' back from native representation
	if (___threshold4 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____threshold4_marshaled, *___threshold4);
	}

	// Marshaling cleanup of parameter '___threshold4' native representation
	if ((&____threshold4_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____threshold4_marshaled);
	}

}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_m8C6D6AA4764569D3426EEE34A5D435E30C3AD3B8 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_m3933FF1EAF3078E6969918306D580A60B3A809BD (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 15)
			{
				// open
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14, targetMethod);
			}
		}
		else if (___parameterCount != 15)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker14< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
					else
						GenericVirtActionInvoker14< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker14< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
					else
						VirtActionInvoker14< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker15< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
					else
						GenericVirtActionInvoker15< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker15< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
					else
						VirtActionInvoker15< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___threshold4, ___remapThresholdMin5, ___remapThresholdMax6, ___relativeIntensity7, ___feedbacksIntensity8, ___channel9, ___resetShakerValuesAfterShake10, ___resetTargetValuesAfterShake11, ___forwardDirection12, ___timescaleMode13, ___stop14, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Delegate_BeginInvoke_mF54F95B39682E08AE8BD0E4A85701B690B534CFE (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___threshold4, float ___remapThresholdMin5, float ___remapThresholdMax6, bool ___relativeIntensity7, float ___feedbacksIntensity8, int32_t ___channel9, bool ___resetShakerValuesAfterShake10, bool ___resetTargetValuesAfterShake11, bool ___forwardDirection12, int32_t ___timescaleMode13, bool ___stop14, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback15, RuntimeObject * ___object16, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[16] = {0};
	__d_args[0] = ___intensity0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___duration1);
	__d_args[2] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMin2);
	__d_args[3] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMax3);
	__d_args[4] = ___threshold4;
	__d_args[5] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapThresholdMin5);
	__d_args[6] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapThresholdMax6);
	__d_args[7] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___relativeIntensity7);
	__d_args[8] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___feedbacksIntensity8);
	__d_args[9] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___channel9);
	__d_args[10] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetShakerValuesAfterShake10);
	__d_args[11] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetTargetValuesAfterShake11);
	__d_args[12] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___forwardDirection12);
	__d_args[13] = Box(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var, &___timescaleMode13);
	__d_args[14] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___stop14);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback15, (RuntimeObject*)___object16);;
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMBloomShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_EndInvoke_mA38D6C5C060336B651DDBF6F6646EB0F33E1AF86 (Delegate_t9907AD71B986BEDD24F2373E82BA8E4262703837 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc)(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, float, int32_t, float, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___intensity0' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____intensity0_marshaled = {};
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___intensity0, ____intensity0_marshaled);
	}

	// Native function invocation
	il2cppPInvokeFunc(___intensity0 != NULL ? (&____intensity0_marshaled) : NULL, ___duration1, ___remapMin2, ___remapMax3, static_cast<int32_t>(___relativeIntensity4), ___feedbacksIntensity5, ___channel6, static_cast<int32_t>(___resetShakerValuesAfterShake7), static_cast<int32_t>(___resetTargetValuesAfterShake8), static_cast<int32_t>(___forwardDirection9), ___timescaleMode10, static_cast<int32_t>(___stop11));

	// Marshaling of parameter '___intensity0' back from native representation
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____intensity0_marshaled, *___intensity0);
	}

	// Marshaling cleanup of parameter '___intensity0' native representation
	if ((&____intensity0_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____intensity0_marshaled);
	}

}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_m909A6ABE40C1CF17C2A53173B250D8C91A23495D (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_mECA4D50519B0FE3849D754AEC245B86CC475A4A0 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 12)
			{
				// open
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
		}
		else if (___parameterCount != 12)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						GenericVirtActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						VirtActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						GenericVirtActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						VirtActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Delegate_BeginInvoke_m7A3D32292938BA78C59115F75BD58131FFAA82D4 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback12, RuntimeObject * ___object13, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[13] = {0};
	__d_args[0] = ___intensity0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___duration1);
	__d_args[2] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMin2);
	__d_args[3] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMax3);
	__d_args[4] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___relativeIntensity4);
	__d_args[5] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___feedbacksIntensity5);
	__d_args[6] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___channel6);
	__d_args[7] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetShakerValuesAfterShake7);
	__d_args[8] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetTargetValuesAfterShake8);
	__d_args[9] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___forwardDirection9);
	__d_args[10] = Box(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var, &___timescaleMode10);
	__d_args[11] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___stop11);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback12, (RuntimeObject*)___object13);;
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMChromaticAberrationShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_EndInvoke_m27AD3D9F84BE060F6CC7A2ADE2156671B1618472 (Delegate_t88F68EB3CABE636A04DCD098BC916BE1F5851A81 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, const RuntimeMethod* method)
{








	typedef void (DEFAULT_CALL *PInvokeFunc)(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, float, int32_t, float, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___shakePostExposure0' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____shakePostExposure0_marshaled = {};
	if (___shakePostExposure0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___shakePostExposure0, ____shakePostExposure0_marshaled);
	}

	// Marshaling of parameter '___shakeHueShift3' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____shakeHueShift3_marshaled = {};
	if (___shakeHueShift3 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___shakeHueShift3, ____shakeHueShift3_marshaled);
	}

	// Marshaling of parameter '___shakeSaturation6' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____shakeSaturation6_marshaled = {};
	if (___shakeSaturation6 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___shakeSaturation6, ____shakeSaturation6_marshaled);
	}

	// Marshaling of parameter '___shakeContrast9' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____shakeContrast9_marshaled = {};
	if (___shakeContrast9 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___shakeContrast9, ____shakeContrast9_marshaled);
	}

	// Native function invocation
	il2cppPInvokeFunc(___shakePostExposure0 != NULL ? (&____shakePostExposure0_marshaled) : NULL, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3 != NULL ? (&____shakeHueShift3_marshaled) : NULL, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6 != NULL ? (&____shakeSaturation6_marshaled) : NULL, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9 != NULL ? (&____shakeContrast9_marshaled) : NULL, ___remapContrastZero10, ___remapContrastOne11, ___duration12, static_cast<int32_t>(___relativeValues13), ___feedbacksIntensity14, ___channel15, static_cast<int32_t>(___resetShakerValuesAfterShake16), static_cast<int32_t>(___resetTargetValuesAfterShake17), static_cast<int32_t>(___forwardDirection18), ___timescaleMode19, static_cast<int32_t>(___stop20));

	// Marshaling of parameter '___shakePostExposure0' back from native representation
	if (___shakePostExposure0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____shakePostExposure0_marshaled, *___shakePostExposure0);
	}

	// Marshaling cleanup of parameter '___shakePostExposure0' native representation
	if ((&____shakePostExposure0_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____shakePostExposure0_marshaled);
	}

	// Marshaling of parameter '___shakeHueShift3' back from native representation
	if (___shakeHueShift3 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____shakeHueShift3_marshaled, *___shakeHueShift3);
	}

	// Marshaling cleanup of parameter '___shakeHueShift3' native representation
	if ((&____shakeHueShift3_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____shakeHueShift3_marshaled);
	}

	// Marshaling of parameter '___shakeSaturation6' back from native representation
	if (___shakeSaturation6 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____shakeSaturation6_marshaled, *___shakeSaturation6);
	}

	// Marshaling cleanup of parameter '___shakeSaturation6' native representation
	if ((&____shakeSaturation6_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____shakeSaturation6_marshaled);
	}

	// Marshaling of parameter '___shakeContrast9' back from native representation
	if (___shakeContrast9 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____shakeContrast9_marshaled, *___shakeContrast9);
	}

	// Marshaling cleanup of parameter '___shakeContrast9' native representation
	if ((&____shakeContrast9_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____shakeContrast9_marshaled);
	}

}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_mAA8EA46ABCE74EEB9BB4EA63051A4923C9503903 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_mB73ED89FD1DDEC3416C222C3CDA8F4E9ECCA9A47 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 21)
			{
				// open
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20, targetMethod);
			}
		}
		else if (___parameterCount != 21)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker20< float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
					else
						GenericVirtActionInvoker20< float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker20< float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
					else
						VirtActionInvoker20< float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker21< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
					else
						GenericVirtActionInvoker21< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker21< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
					else
						VirtActionInvoker21< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___shakePostExposure0, ___remapPostExposureZero1, ___remapPostExposureOne2, ___shakeHueShift3, ___remapHueShiftZero4, ___remapHueShiftOne5, ___shakeSaturation6, ___remapSaturationZero7, ___remapSaturationOne8, ___shakeContrast9, ___remapContrastZero10, ___remapContrastOne11, ___duration12, ___relativeValues13, ___feedbacksIntensity14, ___channel15, ___resetShakerValuesAfterShake16, ___resetTargetValuesAfterShake17, ___forwardDirection18, ___timescaleMode19, ___stop20, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Delegate_BeginInvoke_m5E718F50FC0646F4B85B385FF307EA6661746E47 (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakePostExposure0, float ___remapPostExposureZero1, float ___remapPostExposureOne2, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeHueShift3, float ___remapHueShiftZero4, float ___remapHueShiftOne5, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeSaturation6, float ___remapSaturationZero7, float ___remapSaturationOne8, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___shakeContrast9, float ___remapContrastZero10, float ___remapContrastOne11, float ___duration12, bool ___relativeValues13, float ___feedbacksIntensity14, int32_t ___channel15, bool ___resetShakerValuesAfterShake16, bool ___resetTargetValuesAfterShake17, bool ___forwardDirection18, int32_t ___timescaleMode19, bool ___stop20, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback21, RuntimeObject * ___object22, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[22] = {0};
	__d_args[0] = ___shakePostExposure0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapPostExposureZero1);
	__d_args[2] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapPostExposureOne2);
	__d_args[3] = ___shakeHueShift3;
	__d_args[4] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapHueShiftZero4);
	__d_args[5] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapHueShiftOne5);
	__d_args[6] = ___shakeSaturation6;
	__d_args[7] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapSaturationZero7);
	__d_args[8] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapSaturationOne8);
	__d_args[9] = ___shakeContrast9;
	__d_args[10] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapContrastZero10);
	__d_args[11] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapContrastOne11);
	__d_args[12] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___duration12);
	__d_args[13] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___relativeValues13);
	__d_args[14] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___feedbacksIntensity14);
	__d_args[15] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___channel15);
	__d_args[16] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetShakerValuesAfterShake16);
	__d_args[17] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetTargetValuesAfterShake17);
	__d_args[18] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___forwardDirection18);
	__d_args[19] = Box(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var, &___timescaleMode19);
	__d_args[20] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___stop20);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback21, (RuntimeObject*)___object22);;
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMColorGradingShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_EndInvoke_m119D754048E54FA56706DABB0D5D16E1D68C2E7D (Delegate_t7B3862ADDA0D1A4078DEA9E1B4B9A06407954A27 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, const RuntimeMethod* method)
{






	typedef void (DEFAULT_CALL *PInvokeFunc)(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, int32_t, float, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___focusDistance0' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____focusDistance0_marshaled = {};
	if (___focusDistance0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___focusDistance0, ____focusDistance0_marshaled);
	}

	// Marshaling of parameter '___aperture4' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____aperture4_marshaled = {};
	if (___aperture4 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___aperture4, ____aperture4_marshaled);
	}

	// Marshaling of parameter '___focalLength7' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____focalLength7_marshaled = {};
	if (___focalLength7 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___focalLength7, ____focalLength7_marshaled);
	}

	// Native function invocation
	il2cppPInvokeFunc(___focusDistance0 != NULL ? (&____focusDistance0_marshaled) : NULL, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4 != NULL ? (&____aperture4_marshaled) : NULL, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7 != NULL ? (&____focalLength7_marshaled) : NULL, ___remapFocalLengthMin8, ___remapFocalLengthMax9, static_cast<int32_t>(___relativeValues10), ___feedbacksIntensity11, ___channel12, static_cast<int32_t>(___resetShakerValuesAfterShake13), static_cast<int32_t>(___resetTargetValuesAfterShake14), static_cast<int32_t>(___forwardDirection15), ___timescaleMode16, static_cast<int32_t>(___stop17));

	// Marshaling of parameter '___focusDistance0' back from native representation
	if (___focusDistance0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____focusDistance0_marshaled, *___focusDistance0);
	}

	// Marshaling cleanup of parameter '___focusDistance0' native representation
	if ((&____focusDistance0_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____focusDistance0_marshaled);
	}

	// Marshaling of parameter '___aperture4' back from native representation
	if (___aperture4 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____aperture4_marshaled, *___aperture4);
	}

	// Marshaling cleanup of parameter '___aperture4' native representation
	if ((&____aperture4_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____aperture4_marshaled);
	}

	// Marshaling of parameter '___focalLength7' back from native representation
	if (___focalLength7 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____focalLength7_marshaled, *___focalLength7);
	}

	// Marshaling cleanup of parameter '___focalLength7' native representation
	if ((&____focalLength7_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____focalLength7_marshaled);
	}

}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_mD402128BE425C84FBC82232D1056363CFC3A76F7 (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_mE072A8E7C58AFE376DCBD8AFAC20701518D86AED (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 18)
			{
				// open
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17, targetMethod);
			}
		}
		else if (___parameterCount != 18)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker17< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
					else
						GenericVirtActionInvoker17< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker17< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
					else
						VirtActionInvoker17< float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker18< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
					else
						GenericVirtActionInvoker18< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker18< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
					else
						VirtActionInvoker18< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___focusDistance0, ___duration1, ___remapFocusDistanceMin2, ___remapFocusDistanceMax3, ___aperture4, ___remapApertureMin5, ___remapApertureMax6, ___focalLength7, ___remapFocalLengthMin8, ___remapFocalLengthMax9, ___relativeValues10, ___feedbacksIntensity11, ___channel12, ___resetShakerValuesAfterShake13, ___resetTargetValuesAfterShake14, ___forwardDirection15, ___timescaleMode16, ___stop17, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,UnityEngine.AnimationCurve,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Delegate_BeginInvoke_m2A8D5C241FB345616C72C0232EB41F9AF5BBD8F4 (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focusDistance0, float ___duration1, float ___remapFocusDistanceMin2, float ___remapFocusDistanceMax3, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___aperture4, float ___remapApertureMin5, float ___remapApertureMax6, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___focalLength7, float ___remapFocalLengthMin8, float ___remapFocalLengthMax9, bool ___relativeValues10, float ___feedbacksIntensity11, int32_t ___channel12, bool ___resetShakerValuesAfterShake13, bool ___resetTargetValuesAfterShake14, bool ___forwardDirection15, int32_t ___timescaleMode16, bool ___stop17, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback18, RuntimeObject * ___object19, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[19] = {0};
	__d_args[0] = ___focusDistance0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___duration1);
	__d_args[2] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapFocusDistanceMin2);
	__d_args[3] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapFocusDistanceMax3);
	__d_args[4] = ___aperture4;
	__d_args[5] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapApertureMin5);
	__d_args[6] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapApertureMax6);
	__d_args[7] = ___focalLength7;
	__d_args[8] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapFocalLengthMin8);
	__d_args[9] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapFocalLengthMax9);
	__d_args[10] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___relativeValues10);
	__d_args[11] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___feedbacksIntensity11);
	__d_args[12] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___channel12);
	__d_args[13] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetShakerValuesAfterShake13);
	__d_args[14] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetTargetValuesAfterShake14);
	__d_args[15] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___forwardDirection15);
	__d_args[16] = Box(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var, &___timescaleMode16);
	__d_args[17] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___stop17);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback18, (RuntimeObject*)___object19);;
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMDepthOfFieldShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_EndInvoke_mBD6EF50191821D9D5EB46E9A79260919E8E7A779 (Delegate_tAB0A4A699175D2074BB6687246F7F932C539C13C * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc)(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, float, int32_t, float, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___intensity0' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____intensity0_marshaled = {};
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___intensity0, ____intensity0_marshaled);
	}

	// Native function invocation
	il2cppPInvokeFunc(___intensity0 != NULL ? (&____intensity0_marshaled) : NULL, ___duration1, ___remapMin2, ___remapMax3, static_cast<int32_t>(___relativeIntensity4), ___feedbacksIntensity5, ___channel6, static_cast<int32_t>(___resetShakerValuesAfterShake7), static_cast<int32_t>(___resetTargetValuesAfterShake8), static_cast<int32_t>(___forwardDirection9), ___timescaleMode10, static_cast<int32_t>(___stop11));

	// Marshaling of parameter '___intensity0' back from native representation
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____intensity0_marshaled, *___intensity0);
	}

	// Marshaling cleanup of parameter '___intensity0' native representation
	if ((&____intensity0_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____intensity0_marshaled);
	}

}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_m6DD24C890F271356820106BCD3059A26CE2B05C5 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_m1297C5964A2FEF0A3AB0D42415275F9992AD18A1 (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 12)
			{
				// open
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
		}
		else if (___parameterCount != 12)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						GenericVirtActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						VirtActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						GenericVirtActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						VirtActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Delegate_BeginInvoke_m8FD1DFF7AD9BA9D90AABD595B209BF01CC9E5CCB (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback12, RuntimeObject * ___object13, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[13] = {0};
	__d_args[0] = ___intensity0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___duration1);
	__d_args[2] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMin2);
	__d_args[3] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMax3);
	__d_args[4] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___relativeIntensity4);
	__d_args[5] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___feedbacksIntensity5);
	__d_args[6] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___channel6);
	__d_args[7] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetShakerValuesAfterShake7);
	__d_args[8] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetTargetValuesAfterShake8);
	__d_args[9] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___forwardDirection9);
	__d_args[10] = Box(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var, &___timescaleMode10);
	__d_args[11] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___stop11);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback12, (RuntimeObject*)___object13);;
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMLensDistortionShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_EndInvoke_m3C760D0354ABB45A816916AABDF1D57B48DD9A1B (Delegate_tE88A3D8060AC8979D5D0D7BD67AD183DA2C4FB95 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{


	typedef void (DEFAULT_CALL *PInvokeFunc)(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke*, float, float, float, int32_t, float, int32_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(((RuntimeDelegate*)__this)->method->nativeFunction);

	// Marshaling of parameter '___intensity0' to native representation
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshaled_pinvoke ____intensity0_marshaled = {};
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke(*___intensity0, ____intensity0_marshaled);
	}

	// Native function invocation
	il2cppPInvokeFunc(___intensity0 != NULL ? (&____intensity0_marshaled) : NULL, ___duration1, ___remapMin2, ___remapMax3, static_cast<int32_t>(___relativeIntensity4), ___feedbacksIntensity5, ___channel6, static_cast<int32_t>(___resetShakerValuesAfterShake7), static_cast<int32_t>(___resetTargetValuesAfterShake8), static_cast<int32_t>(___forwardDirection9), ___timescaleMode10, static_cast<int32_t>(___stop11));

	// Marshaling of parameter '___intensity0' back from native representation
	if (___intensity0 != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_back(____intensity0_marshaled, *___intensity0);
	}

	// Marshaling cleanup of parameter '___intensity0' native representation
	if ((&____intensity0_marshaled) != NULL)
	{
		AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03_marshal_pinvoke_cleanup(____intensity0_marshaled);
	}

}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate__ctor_mB4D10F50ECD2FE9F53574366D7BD1C9BF9635F9E (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::Invoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_Invoke_m7C0F4E59C8EBF4711224DFB293B92B7E9BC36D84 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, const RuntimeMethod* method)
{
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 12)
			{
				// open
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
		}
		else if (___parameterCount != 12)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						GenericVirtActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						VirtActionInvoker11< float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
			}
		}
		else
		{
			// closed
			if (targetThis != NULL && il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						GenericVirtActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(targetMethod, targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
					else
						VirtActionInvoker12< AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11);
				}
			}
			else
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 *, float, float, float, bool, float, int32_t, bool, bool, bool, int32_t, bool, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, ___intensity0, ___duration1, ___remapMin2, ___remapMax3, ___relativeIntensity4, ___feedbacksIntensity5, ___channel6, ___resetShakerValuesAfterShake7, ___resetTargetValuesAfterShake8, ___forwardDirection9, ___timescaleMode10, ___stop11, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::BeginInvoke(UnityEngine.AnimationCurve,System.Single,System.Single,System.Single,System.Boolean,System.Single,System.Int32,System.Boolean,System.Boolean,System.Boolean,MoreMountains.Feedbacks.TimescaleModes,System.Boolean,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Delegate_BeginInvoke_mF4EAB67DF64F58FC3E626789A2A57AC1AC0412F4 (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___intensity0, float ___duration1, float ___remapMin2, float ___remapMax3, bool ___relativeIntensity4, float ___feedbacksIntensity5, int32_t ___channel6, bool ___resetShakerValuesAfterShake7, bool ___resetTargetValuesAfterShake8, bool ___forwardDirection9, int32_t ___timescaleMode10, bool ___stop11, AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA * ___callback12, RuntimeObject * ___object13, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[13] = {0};
	__d_args[0] = ___intensity0;
	__d_args[1] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___duration1);
	__d_args[2] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMin2);
	__d_args[3] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___remapMax3);
	__d_args[4] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___relativeIntensity4);
	__d_args[5] = Box(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E_il2cpp_TypeInfo_var, &___feedbacksIntensity5);
	__d_args[6] = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &___channel6);
	__d_args[7] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetShakerValuesAfterShake7);
	__d_args[8] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___resetTargetValuesAfterShake8);
	__d_args[9] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___forwardDirection9);
	__d_args[10] = Box(TimescaleModes_t9BB44B0A2F1D4489D501D478A3CAF34DD53D0412_il2cpp_TypeInfo_var, &___timescaleMode10);
	__d_args[11] = Box(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_il2cpp_TypeInfo_var, &___stop11);
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback12, (RuntimeObject*)___object13);;
}
// System.Void MoreMountains.FeedbacksForThirdParty.MMVignetteShakeEvent/Delegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Delegate_EndInvoke_m9DDF25A4ABE96DDC818EC98F83286D9C911EE87B (Delegate_t4E129F19ECA9D5A18694250E7A0F492FAD4D5CCD * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float ParameterOverride_1_op_Implicit_m23C2459757EFA9C34D4DE07B6A82143A079A6DEA_gshared_inline (ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 * ___prop0, const RuntimeMethod* method)
{
	{
		// return prop.value;
		ParameterOverride_1_tE564C5FA9AEBC9DB9DEB9AA8668AB1C53698D7C1 * L_0 = ___prop0;
		float L_1 = (float)L_0->get_value_1();
		return (float)L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}

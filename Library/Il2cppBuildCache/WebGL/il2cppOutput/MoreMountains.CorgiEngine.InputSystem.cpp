﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>
struct Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.InputBinding>
struct IEnumerable_1_t83AE986FCA1F44A12641275484FF33E3D6F098F5;
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction>
struct IEnumerator_1_tA4D05AF4E1C13B34106C92C331765C5D358A33EE;
// UnityEngine.InputSystem.InputProcessor`1<System.Single>
struct InputProcessor_1_tD5A5E4581DBFCA8996AC18CA2B238890303AFD91;
// System.Collections.Generic.List`1<MoreMountains.Tools.MMInput/IMButton>
struct List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B;
// MoreMountains.Tools.MMStateMachine`1<System.Int32Enum>
struct MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA;
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>
struct MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE;
// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<System.Int32Enum>
struct OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17;
// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<MoreMountains.Tools.MMInput/ButtonStates>
struct OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321;
// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>[]
struct Action_1U5BU5D_t5FBD6652385299C05FB838F4ED3119509A97465F;
// UnityEngine.InputSystem.InputProcessor`1<System.Single>[]
struct InputProcessor_1U5BU5D_tBF842A7A689B26B12E2ACBB7ACD3ED6FEB30D781;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// UnityEngine.InputSystem.InputAction[]
struct InputActionU5BU5D_t63AB2DEF6F9057B50B7AB65EA0C0030BE607A883;
// UnityEngine.InputSystem.InputActionMap[]
struct InputActionMapU5BU5D_t71F679B29A7609B04476AA165F6BC5205472C9C8;
// UnityEngine.InputSystem.InputBinding[]
struct InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B;
// UnityEngine.InputSystem.InputControl[]
struct InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F;
// UnityEngine.InputSystem.InputControlScheme[]
struct InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12;
// UnityEngine.InputSystem.InputDevice[]
struct InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A;
// UnityEngine.InputSystem.InputControlScheme/DeviceRequirement[]
struct DeviceRequirementU5BU5D_tBEFA5CADFC07A3A326E46B22E1D3A630F022511F;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.InputSystem.Controls.ButtonControl
struct ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D;
// MoreMountains.CorgiEngine.CorgiEngineInputActions
struct CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.InputSystem.InputAction
struct InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B;
// UnityEngine.InputSystem.InputActionAsset
struct InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB;
// UnityEngine.InputSystem.InputActionMap
struct InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4;
// UnityEngine.InputSystem.InputActionState
struct InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1;
// UnityEngine.InputSystem.InputControl
struct InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713;
// UnityEngine.InputSystem.InputDevice
struct InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154;
// MoreMountains.CorgiEngine.InputManager
struct InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA;
// MoreMountains.CorgiEngine.InputSystemManager
struct InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80;
// MoreMountains.CorgiEngine.InputSystemManagerEventsBased
struct InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E;
// UnityEngine.InputSystem.InputValue
struct InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// MoreMountains.CorgiEngine.CorgiEngineInputActions/IPlayerControlsActions
struct IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F;
// MoreMountains.Tools.MMInput/IMButton
struct IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B;
// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate
struct ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705;
// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate
struct ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B;
// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate
struct ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral052C17E9D6BC1FE3DB31F3277AED6D12A2C6BDAC;
IL2CPP_EXTERN_C String_t* _stringLiteral0A3EF69D14EE38BA43AC0515A8C06B877F0D161F;
IL2CPP_EXTERN_C String_t* _stringLiteral2A474FBC7EC4EA2C1A762D0FB32562808DDDA351;
IL2CPP_EXTERN_C String_t* _stringLiteral515E152AE9864F9AEE9ADB9C0487E55BF594D5DF;
IL2CPP_EXTERN_C String_t* _stringLiteral539886CC0D6E626CD59CCCB9A251E15C3F8DC3EE;
IL2CPP_EXTERN_C String_t* _stringLiteral61225B335304C6BE91EE0EC8CF385BBE9B633F17;
IL2CPP_EXTERN_C String_t* _stringLiteral63D5B7B61C9A932F0CFFA0506AD506F2DC0E23A8;
IL2CPP_EXTERN_C String_t* _stringLiteral6B514E260236F16463F3E75F706DCC5449640861;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral78DB8AA40D3B780BCB7B7076042CF600254368BC;
IL2CPP_EXTERN_C String_t* _stringLiteral7FAE997056826D296DB6568629E5D4A1BB440293;
IL2CPP_EXTERN_C String_t* _stringLiteral8866EA3E0BC985BCCB33306B5606BA3D21875437;
IL2CPP_EXTERN_C String_t* _stringLiteral8978DD6DA21EA64066F90784F7852458B1D54CA8;
IL2CPP_EXTERN_C String_t* _stringLiteral9E48D88C3471247D01F96214CD4DB51A245D5A05;
IL2CPP_EXTERN_C String_t* _stringLiteralAAAA401E86E41E6120BB9E96B9892141CF5A81F8;
IL2CPP_EXTERN_C String_t* _stringLiteralB0EF07816378D87F80141959937F780914E0D8D3;
IL2CPP_EXTERN_C String_t* _stringLiteralC068740B1398F61F47B7773BB27A4A828FA47A6F;
IL2CPP_EXTERN_C String_t* _stringLiteralC312F88FB3C35D22BE7CDEB894CC152447D26A16;
IL2CPP_EXTERN_C String_t* _stringLiteralC4C8C3123441B0B03C3A7F20F50CBF14D5E64B63;
IL2CPP_EXTERN_C String_t* _stringLiteralE17E01A6CDB454BE09B74C544A2901D6C9F990AF;
IL2CPP_EXTERN_C String_t* _stringLiteralE4CD985488306FB575FF327ABBAD1D041A7BB68C;
IL2CPP_EXTERN_C String_t* _stringLiteralF60A604A8904268B2FCB7A84B9C54E433B6C1C27;
IL2CPP_EXTERN_C String_t* _stringLiteralF7B071257B0A0FEB1A12B0FD1786CBD4D6D1ADC9;
IL2CPP_EXTERN_C String_t* _stringLiteralFA39B819B390DDA4BFA806E31149DC99CE38040A;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_0_m46ACF796CE6662C47F5F6F7DF3F93D66458546C7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_10_mEFDA6A719B33D59A1768E47E6B3D7E618BD56F40_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_11_m6F724C37CE09DD1C52D6D5636CCA616F99F74246_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_12_m0212D2D2FA4CBB4E27D20BCBE20F389F94C0E52B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_13_m00B1CE3F5790A3F641BED8A7254629F943AC4E05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_14_m4DD059256DE4FED79F39CF0E3A0EE07672534B01_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_15_m708B678A008CC12A1FE7A62E3133F528BCC5CEBE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_16_m0EF285474B25D28254204813684E18243A720237_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_17_m960D219A1C16790E3519F35094204360452DEAC9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_18_mDB44A20E9422F4F04200E018DD1AC60474420D29_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_19_m90E4403D1EE32D1A0991ABFDC802E3144BCC6958_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_1_m2F8C9BF720B21AFFEA59C51DAB9AFDB00F1784FC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_2_m446C76D83D4632E0FF67A9592B6847E671D5080B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_3_m0FC1B7B16A1FA07223B493FFBC7EBA28B4AD20EE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_4_m8BE231372655FFED47110BA4F1FD2C669F2F5269_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_5_m13CCF14FE346DE32BA377629591B961298E42B9A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_6_m7CDD3C382E16BDAA0879403900170E3A843BFB72_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_7_mFEAC9CC38469CFB33F91332564BD32D2E6C698E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_8_m70C3E7F0E5D68F4B1FE4BE9C596A17D9B8A57CB7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputSystemManager_U3CInitializationU3Eb__4_9_m0E5555C114C1B5F7E716B1C78E185CBC626491AB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct DeviceRequirement_t02A255131C3F591837F4E6FA3241BD329A0E1807_marshaled_com;
struct DeviceRequirement_t02A255131C3F591837F4E6FA3241BD329A0E1807_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tEAB3F3158AE99CD832EDAB65FC752DFCAD6881FB 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// MoreMountains.CorgiEngine.CorgiEngineInputActions
struct CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.InputActionAsset MoreMountains.CorgiEngine.CorgiEngineInputActions::<asset>k__BackingField
	InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * ___U3CassetU3Ek__BackingField_0;
	// UnityEngine.InputSystem.InputActionMap MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls
	InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * ___m_PlayerControls_1;
	// MoreMountains.CorgiEngine.CorgiEngineInputActions/IPlayerControlsActions MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControlsActionsCallbackInterface
	RuntimeObject* ___m_PlayerControlsActionsCallbackInterface_2;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_PrimaryMovement
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_PrimaryMovement_3;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_SecondaryMovement
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_SecondaryMovement_4;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Jump
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Jump_5;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Swim
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Swim_6;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Glide
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Glide_7;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Jetpack
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Jetpack_8;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Fly
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Fly_9;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Throw
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Throw_10;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Push
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Push_11;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Grab
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Grab_12;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Run
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Run_13;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Dash
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Dash_14;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Shoot
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Shoot_15;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_SecondaryShoot
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_SecondaryShoot_16;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Interact
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Interact_17;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Reload
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Reload_18;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_Pause
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_Pause_19;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_SwitchWeapon
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_SwitchWeapon_20;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_SwitchCharacter
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_SwitchCharacter_21;
	// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::m_PlayerControls_TimeControl
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_PlayerControls_TimeControl_22;
	// System.Int32 MoreMountains.CorgiEngine.CorgiEngineInputActions::m_KeyboardSchemeIndex
	int32_t ___m_KeyboardSchemeIndex_23;
	// System.Int32 MoreMountains.CorgiEngine.CorgiEngineInputActions::m_GamepadSchemeIndex
	int32_t ___m_GamepadSchemeIndex_24;

public:
	inline static int32_t get_offset_of_U3CassetU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___U3CassetU3Ek__BackingField_0)); }
	inline InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * get_U3CassetU3Ek__BackingField_0() const { return ___U3CassetU3Ek__BackingField_0; }
	inline InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB ** get_address_of_U3CassetU3Ek__BackingField_0() { return &___U3CassetU3Ek__BackingField_0; }
	inline void set_U3CassetU3Ek__BackingField_0(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * value)
	{
		___U3CassetU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CassetU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_1() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_1)); }
	inline InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * get_m_PlayerControls_1() const { return ___m_PlayerControls_1; }
	inline InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 ** get_address_of_m_PlayerControls_1() { return &___m_PlayerControls_1; }
	inline void set_m_PlayerControls_1(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * value)
	{
		___m_PlayerControls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControlsActionsCallbackInterface_2() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControlsActionsCallbackInterface_2)); }
	inline RuntimeObject* get_m_PlayerControlsActionsCallbackInterface_2() const { return ___m_PlayerControlsActionsCallbackInterface_2; }
	inline RuntimeObject** get_address_of_m_PlayerControlsActionsCallbackInterface_2() { return &___m_PlayerControlsActionsCallbackInterface_2; }
	inline void set_m_PlayerControlsActionsCallbackInterface_2(RuntimeObject* value)
	{
		___m_PlayerControlsActionsCallbackInterface_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControlsActionsCallbackInterface_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_PrimaryMovement_3() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_PrimaryMovement_3)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_PrimaryMovement_3() const { return ___m_PlayerControls_PrimaryMovement_3; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_PrimaryMovement_3() { return &___m_PlayerControls_PrimaryMovement_3; }
	inline void set_m_PlayerControls_PrimaryMovement_3(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_PrimaryMovement_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_PrimaryMovement_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_SecondaryMovement_4() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_SecondaryMovement_4)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_SecondaryMovement_4() const { return ___m_PlayerControls_SecondaryMovement_4; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_SecondaryMovement_4() { return &___m_PlayerControls_SecondaryMovement_4; }
	inline void set_m_PlayerControls_SecondaryMovement_4(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_SecondaryMovement_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_SecondaryMovement_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Jump_5() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Jump_5)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Jump_5() const { return ___m_PlayerControls_Jump_5; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Jump_5() { return &___m_PlayerControls_Jump_5; }
	inline void set_m_PlayerControls_Jump_5(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Jump_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Jump_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Swim_6() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Swim_6)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Swim_6() const { return ___m_PlayerControls_Swim_6; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Swim_6() { return &___m_PlayerControls_Swim_6; }
	inline void set_m_PlayerControls_Swim_6(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Swim_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Swim_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Glide_7() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Glide_7)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Glide_7() const { return ___m_PlayerControls_Glide_7; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Glide_7() { return &___m_PlayerControls_Glide_7; }
	inline void set_m_PlayerControls_Glide_7(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Glide_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Glide_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Jetpack_8() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Jetpack_8)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Jetpack_8() const { return ___m_PlayerControls_Jetpack_8; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Jetpack_8() { return &___m_PlayerControls_Jetpack_8; }
	inline void set_m_PlayerControls_Jetpack_8(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Jetpack_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Jetpack_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Fly_9() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Fly_9)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Fly_9() const { return ___m_PlayerControls_Fly_9; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Fly_9() { return &___m_PlayerControls_Fly_9; }
	inline void set_m_PlayerControls_Fly_9(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Fly_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Fly_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Throw_10() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Throw_10)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Throw_10() const { return ___m_PlayerControls_Throw_10; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Throw_10() { return &___m_PlayerControls_Throw_10; }
	inline void set_m_PlayerControls_Throw_10(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Throw_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Throw_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Push_11() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Push_11)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Push_11() const { return ___m_PlayerControls_Push_11; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Push_11() { return &___m_PlayerControls_Push_11; }
	inline void set_m_PlayerControls_Push_11(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Push_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Push_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Grab_12() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Grab_12)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Grab_12() const { return ___m_PlayerControls_Grab_12; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Grab_12() { return &___m_PlayerControls_Grab_12; }
	inline void set_m_PlayerControls_Grab_12(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Grab_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Grab_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Run_13() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Run_13)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Run_13() const { return ___m_PlayerControls_Run_13; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Run_13() { return &___m_PlayerControls_Run_13; }
	inline void set_m_PlayerControls_Run_13(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Run_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Run_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Dash_14() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Dash_14)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Dash_14() const { return ___m_PlayerControls_Dash_14; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Dash_14() { return &___m_PlayerControls_Dash_14; }
	inline void set_m_PlayerControls_Dash_14(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Dash_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Dash_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Shoot_15() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Shoot_15)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Shoot_15() const { return ___m_PlayerControls_Shoot_15; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Shoot_15() { return &___m_PlayerControls_Shoot_15; }
	inline void set_m_PlayerControls_Shoot_15(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Shoot_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Shoot_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_SecondaryShoot_16() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_SecondaryShoot_16)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_SecondaryShoot_16() const { return ___m_PlayerControls_SecondaryShoot_16; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_SecondaryShoot_16() { return &___m_PlayerControls_SecondaryShoot_16; }
	inline void set_m_PlayerControls_SecondaryShoot_16(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_SecondaryShoot_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_SecondaryShoot_16), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Interact_17() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Interact_17)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Interact_17() const { return ___m_PlayerControls_Interact_17; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Interact_17() { return &___m_PlayerControls_Interact_17; }
	inline void set_m_PlayerControls_Interact_17(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Interact_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Interact_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Reload_18() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Reload_18)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Reload_18() const { return ___m_PlayerControls_Reload_18; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Reload_18() { return &___m_PlayerControls_Reload_18; }
	inline void set_m_PlayerControls_Reload_18(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Reload_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Reload_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_Pause_19() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_Pause_19)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_Pause_19() const { return ___m_PlayerControls_Pause_19; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_Pause_19() { return &___m_PlayerControls_Pause_19; }
	inline void set_m_PlayerControls_Pause_19(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_Pause_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_Pause_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_SwitchWeapon_20() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_SwitchWeapon_20)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_SwitchWeapon_20() const { return ___m_PlayerControls_SwitchWeapon_20; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_SwitchWeapon_20() { return &___m_PlayerControls_SwitchWeapon_20; }
	inline void set_m_PlayerControls_SwitchWeapon_20(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_SwitchWeapon_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_SwitchWeapon_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_SwitchCharacter_21() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_SwitchCharacter_21)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_SwitchCharacter_21() const { return ___m_PlayerControls_SwitchCharacter_21; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_SwitchCharacter_21() { return &___m_PlayerControls_SwitchCharacter_21; }
	inline void set_m_PlayerControls_SwitchCharacter_21(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_SwitchCharacter_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_SwitchCharacter_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_PlayerControls_TimeControl_22() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_PlayerControls_TimeControl_22)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_PlayerControls_TimeControl_22() const { return ___m_PlayerControls_TimeControl_22; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_PlayerControls_TimeControl_22() { return &___m_PlayerControls_TimeControl_22; }
	inline void set_m_PlayerControls_TimeControl_22(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_PlayerControls_TimeControl_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PlayerControls_TimeControl_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_KeyboardSchemeIndex_23() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_KeyboardSchemeIndex_23)); }
	inline int32_t get_m_KeyboardSchemeIndex_23() const { return ___m_KeyboardSchemeIndex_23; }
	inline int32_t* get_address_of_m_KeyboardSchemeIndex_23() { return &___m_KeyboardSchemeIndex_23; }
	inline void set_m_KeyboardSchemeIndex_23(int32_t value)
	{
		___m_KeyboardSchemeIndex_23 = value;
	}

	inline static int32_t get_offset_of_m_GamepadSchemeIndex_24() { return static_cast<int32_t>(offsetof(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151, ___m_GamepadSchemeIndex_24)); }
	inline int32_t get_m_GamepadSchemeIndex_24() const { return ___m_GamepadSchemeIndex_24; }
	inline int32_t* get_address_of_m_GamepadSchemeIndex_24() { return &___m_GamepadSchemeIndex_24; }
	inline void set_m_GamepadSchemeIndex_24(int32_t value)
	{
		___m_GamepadSchemeIndex_24 = value;
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// MoreMountains.Tools.MMInput/IMButton
struct IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B  : public RuntimeObject
{
public:
	// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates> MoreMountains.Tools.MMInput/IMButton::<State>k__BackingField
	MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * ___U3CStateU3Ek__BackingField_0;
	// System.String MoreMountains.Tools.MMInput/IMButton::ButtonID
	String_t* ___ButtonID_1;
	// MoreMountains.Tools.MMInput/IMButton/ButtonDownMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonDownMethod
	ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 * ___ButtonDownMethod_2;
	// MoreMountains.Tools.MMInput/IMButton/ButtonPressedMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonPressedMethod
	ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B * ___ButtonPressedMethod_3;
	// MoreMountains.Tools.MMInput/IMButton/ButtonUpMethodDelegate MoreMountains.Tools.MMInput/IMButton::ButtonUpMethod
	ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C * ___ButtonUpMethod_4;
	// System.Single MoreMountains.Tools.MMInput/IMButton::_lastButtonDownAt
	float ____lastButtonDownAt_5;
	// System.Single MoreMountains.Tools.MMInput/IMButton::_lastButtonUpAt
	float ____lastButtonUpAt_6;

public:
	inline static int32_t get_offset_of_U3CStateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___U3CStateU3Ek__BackingField_0)); }
	inline MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * get_U3CStateU3Ek__BackingField_0() const { return ___U3CStateU3Ek__BackingField_0; }
	inline MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE ** get_address_of_U3CStateU3Ek__BackingField_0() { return &___U3CStateU3Ek__BackingField_0; }
	inline void set_U3CStateU3Ek__BackingField_0(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * value)
	{
		___U3CStateU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonID_1() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonID_1)); }
	inline String_t* get_ButtonID_1() const { return ___ButtonID_1; }
	inline String_t** get_address_of_ButtonID_1() { return &___ButtonID_1; }
	inline void set_ButtonID_1(String_t* value)
	{
		___ButtonID_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonID_1), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonDownMethod_2() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonDownMethod_2)); }
	inline ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 * get_ButtonDownMethod_2() const { return ___ButtonDownMethod_2; }
	inline ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 ** get_address_of_ButtonDownMethod_2() { return &___ButtonDownMethod_2; }
	inline void set_ButtonDownMethod_2(ButtonDownMethodDelegate_tEAD0F8082ADF18F5A4B75DC9537AB476936C0705 * value)
	{
		___ButtonDownMethod_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonDownMethod_2), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonPressedMethod_3() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonPressedMethod_3)); }
	inline ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B * get_ButtonPressedMethod_3() const { return ___ButtonPressedMethod_3; }
	inline ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B ** get_address_of_ButtonPressedMethod_3() { return &___ButtonPressedMethod_3; }
	inline void set_ButtonPressedMethod_3(ButtonPressedMethodDelegate_t43D6759B02ECF8D0D95FFE6D6A7631177CB3004B * value)
	{
		___ButtonPressedMethod_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonPressedMethod_3), (void*)value);
	}

	inline static int32_t get_offset_of_ButtonUpMethod_4() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ___ButtonUpMethod_4)); }
	inline ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C * get_ButtonUpMethod_4() const { return ___ButtonUpMethod_4; }
	inline ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C ** get_address_of_ButtonUpMethod_4() { return &___ButtonUpMethod_4; }
	inline void set_ButtonUpMethod_4(ButtonUpMethodDelegate_tA2C0D2EA1AA9D4C113039CF27742E0BA1C72862C * value)
	{
		___ButtonUpMethod_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonUpMethod_4), (void*)value);
	}

	inline static int32_t get_offset_of__lastButtonDownAt_5() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ____lastButtonDownAt_5)); }
	inline float get__lastButtonDownAt_5() const { return ____lastButtonDownAt_5; }
	inline float* get_address_of__lastButtonDownAt_5() { return &____lastButtonDownAt_5; }
	inline void set__lastButtonDownAt_5(float value)
	{
		____lastButtonDownAt_5 = value;
	}

	inline static int32_t get_offset_of__lastButtonUpAt_6() { return static_cast<int32_t>(offsetof(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B, ____lastButtonUpAt_6)); }
	inline float get__lastButtonUpAt_6() const { return ____lastButtonUpAt_6; }
	inline float* get_address_of__lastButtonUpAt_6() { return &____lastButtonUpAt_6; }
	inline void set__lastButtonUpAt_6(float value)
	{
		____lastButtonUpAt_6 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>>
struct InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	Action_1U5BU5D_t5FBD6652385299C05FB838F4ED3119509A97465F* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2, ___firstValue_1)); }
	inline Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * get_firstValue_1() const { return ___firstValue_1; }
	inline Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2, ___additionalValues_2)); }
	inline Action_1U5BU5D_t5FBD6652385299C05FB838F4ED3119509A97465F* get_additionalValues_2() const { return ___additionalValues_2; }
	inline Action_1U5BU5D_t5FBD6652385299C05FB838F4ED3119509A97465F** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(Action_1U5BU5D_t5FBD6652385299C05FB838F4ED3119509A97465F* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.InputProcessor`1<System.Single>>
struct InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.InlinedArray`1::length
	int32_t ___length_0;
	// TValue UnityEngine.InputSystem.Utilities.InlinedArray`1::firstValue
	InputProcessor_1_tD5A5E4581DBFCA8996AC18CA2B238890303AFD91 * ___firstValue_1;
	// TValue[] UnityEngine.InputSystem.Utilities.InlinedArray`1::additionalValues
	InputProcessor_1U5BU5D_tBF842A7A689B26B12E2ACBB7ACD3ED6FEB30D781* ___additionalValues_2;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_firstValue_1() { return static_cast<int32_t>(offsetof(InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA, ___firstValue_1)); }
	inline InputProcessor_1_tD5A5E4581DBFCA8996AC18CA2B238890303AFD91 * get_firstValue_1() const { return ___firstValue_1; }
	inline InputProcessor_1_tD5A5E4581DBFCA8996AC18CA2B238890303AFD91 ** get_address_of_firstValue_1() { return &___firstValue_1; }
	inline void set_firstValue_1(InputProcessor_1_tD5A5E4581DBFCA8996AC18CA2B238890303AFD91 * value)
	{
		___firstValue_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___firstValue_1), (void*)value);
	}

	inline static int32_t get_offset_of_additionalValues_2() { return static_cast<int32_t>(offsetof(InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA, ___additionalValues_2)); }
	inline InputProcessor_1U5BU5D_tBF842A7A689B26B12E2ACBB7ACD3ED6FEB30D781* get_additionalValues_2() const { return ___additionalValues_2; }
	inline InputProcessor_1U5BU5D_tBF842A7A689B26B12E2ACBB7ACD3ED6FEB30D781** get_address_of_additionalValues_2() { return &___additionalValues_2; }
	inline void set_additionalValues_2(InputProcessor_1U5BU5D_tBF842A7A689B26B12E2ACBB7ACD3ED6FEB30D781* value)
	{
		___additionalValues_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___additionalValues_2), (void*)value);
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>
struct ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16, ___m_Array_0)); }
	inline InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12* get_m_Array_0() const { return ___m_Array_0; }
	inline InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>
struct ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760 
{
public:
	// TValue[] UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Array
	InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* ___m_Array_0;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_StartIndex
	int32_t ___m_StartIndex_1;
	// System.Int32 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1::m_Length
	int32_t ___m_Length_2;

public:
	inline static int32_t get_offset_of_m_Array_0() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760, ___m_Array_0)); }
	inline InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* get_m_Array_0() const { return ___m_Array_0; }
	inline InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A** get_address_of_m_Array_0() { return &___m_Array_0; }
	inline void set_m_Array_0(InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* value)
	{
		___m_Array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Array_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StartIndex_1() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760, ___m_StartIndex_1)); }
	inline int32_t get_m_StartIndex_1() const { return ___m_StartIndex_1; }
	inline int32_t* get_address_of_m_StartIndex_1() { return &___m_StartIndex_1; }
	inline void set_m_StartIndex_1(int32_t value)
	{
		___m_StartIndex_1 = value;
	}

	inline static int32_t get_offset_of_m_Length_2() { return static_cast<int32_t>(offsetof(ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760, ___m_Length_2)); }
	inline int32_t get_m_Length_2() const { return ___m_Length_2; }
	inline int32_t* get_address_of_m_Length_2() { return &___m_Length_2; }
	inline void set_m_Length_2(int32_t value)
	{
		___m_Length_2 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.InputSystem.Utilities.FourCC
struct FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A 
{
public:
	// System.Int32 UnityEngine.InputSystem.Utilities.FourCC::m_Code
	int32_t ___m_Code_0;

public:
	inline static int32_t get_offset_of_m_Code_0() { return static_cast<int32_t>(offsetof(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A, ___m_Code_0)); }
	inline int32_t get_m_Code_0() const { return ___m_Code_0; }
	inline int32_t* get_address_of_m_Code_0() { return &___m_Code_0; }
	inline void set_m_Code_0(int32_t value)
	{
		___m_Code_0 = value;
	}
};


// UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288 
{
public:
	// System.String UnityEngine.InputSystem.InputControlScheme::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.InputSystem.InputControlScheme::m_BindingGroup
	String_t* ___m_BindingGroup_1;
	// UnityEngine.InputSystem.InputControlScheme/DeviceRequirement[] UnityEngine.InputSystem.InputControlScheme::m_DeviceRequirements
	DeviceRequirementU5BU5D_tBEFA5CADFC07A3A326E46B22E1D3A630F022511F* ___m_DeviceRequirements_2;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingGroup_1() { return static_cast<int32_t>(offsetof(InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288, ___m_BindingGroup_1)); }
	inline String_t* get_m_BindingGroup_1() const { return ___m_BindingGroup_1; }
	inline String_t** get_address_of_m_BindingGroup_1() { return &___m_BindingGroup_1; }
	inline void set_m_BindingGroup_1(String_t* value)
	{
		___m_BindingGroup_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BindingGroup_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_DeviceRequirements_2() { return static_cast<int32_t>(offsetof(InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288, ___m_DeviceRequirements_2)); }
	inline DeviceRequirementU5BU5D_tBEFA5CADFC07A3A326E46B22E1D3A630F022511F* get_m_DeviceRequirements_2() const { return ___m_DeviceRequirements_2; }
	inline DeviceRequirementU5BU5D_tBEFA5CADFC07A3A326E46B22E1D3A630F022511F** get_address_of_m_DeviceRequirements_2() { return &___m_DeviceRequirements_2; }
	inline void set_m_DeviceRequirements_2(DeviceRequirementU5BU5D_tBEFA5CADFC07A3A326E46B22E1D3A630F022511F* value)
	{
		___m_DeviceRequirements_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeviceRequirements_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288_marshaled_pinvoke
{
	char* ___m_Name_0;
	char* ___m_BindingGroup_1;
	DeviceRequirement_t02A255131C3F591837F4E6FA3241BD329A0E1807_marshaled_pinvoke* ___m_DeviceRequirements_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputControlScheme
struct InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288_marshaled_com
{
	Il2CppChar* ___m_Name_0;
	Il2CppChar* ___m_BindingGroup_1;
	DeviceRequirement_t02A255131C3F591837F4E6FA3241BD329A0E1807_marshaled_com* ___m_DeviceRequirements_2;
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 
{
public:
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringOriginalCase
	String_t* ___m_StringOriginalCase_0;
	// System.String UnityEngine.InputSystem.Utilities.InternedString::m_StringLowerCase
	String_t* ___m_StringLowerCase_1;

public:
	inline static int32_t get_offset_of_m_StringOriginalCase_0() { return static_cast<int32_t>(offsetof(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4, ___m_StringOriginalCase_0)); }
	inline String_t* get_m_StringOriginalCase_0() const { return ___m_StringOriginalCase_0; }
	inline String_t** get_address_of_m_StringOriginalCase_0() { return &___m_StringOriginalCase_0; }
	inline void set_m_StringOriginalCase_0(String_t* value)
	{
		___m_StringOriginalCase_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringOriginalCase_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_StringLowerCase_1() { return static_cast<int32_t>(offsetof(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4, ___m_StringLowerCase_1)); }
	inline String_t* get_m_StringLowerCase_1() const { return ___m_StringLowerCase_1; }
	inline String_t** get_address_of_m_StringLowerCase_1() { return &___m_StringLowerCase_1; }
	inline void set_m_StringLowerCase_1(String_t* value)
	{
		___m_StringLowerCase_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_StringLowerCase_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4_marshaled_pinvoke
{
	char* ___m_StringOriginalCase_0;
	char* ___m_StringLowerCase_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.InternedString
struct InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4_marshaled_com
{
	Il2CppChar* ___m_StringOriginalCase_0;
	Il2CppChar* ___m_StringLowerCase_1;
};

// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
struct PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA 
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineInputActions MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::m_Wrapper
	CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___m_Wrapper_0;

public:
	inline static int32_t get_offset_of_m_Wrapper_0() { return static_cast<int32_t>(offsetof(PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA, ___m_Wrapper_0)); }
	inline CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * get_m_Wrapper_0() const { return ___m_Wrapper_0; }
	inline CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 ** get_address_of_m_Wrapper_0() { return &___m_Wrapper_0; }
	inline void set_m_Wrapper_0(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * value)
	{
		___m_Wrapper_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Wrapper_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
struct PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_pinvoke
{
	CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___m_Wrapper_0;
};
// Native definition for COM marshalling of MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
struct PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_com
{
	CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___m_Wrapper_0;
};

// UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 
{
public:
	// UnityEngine.InputSystem.InputActionState UnityEngine.InputSystem.InputAction/CallbackContext::m_State
	InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * ___m_State_0;
	// System.Int32 UnityEngine.InputSystem.InputAction/CallbackContext::m_ActionIndex
	int32_t ___m_ActionIndex_1;

public:
	inline static int32_t get_offset_of_m_State_0() { return static_cast<int32_t>(offsetof(CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58, ___m_State_0)); }
	inline InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * get_m_State_0() const { return ___m_State_0; }
	inline InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 ** get_address_of_m_State_0() { return &___m_State_0; }
	inline void set_m_State_0(InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * value)
	{
		___m_State_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_State_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ActionIndex_1() { return static_cast<int32_t>(offsetof(CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58, ___m_ActionIndex_1)); }
	inline int32_t get_m_ActionIndex_1() const { return ___m_ActionIndex_1; }
	inline int32_t* get_address_of_m_ActionIndex_1() { return &___m_ActionIndex_1; }
	inline void set_m_ActionIndex_1(int32_t value)
	{
		___m_ActionIndex_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58_marshaled_pinvoke
{
	InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * ___m_State_0;
	int32_t ___m_ActionIndex_1;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputAction/CallbackContext
struct CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58_marshaled_com
{
	InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * ___m_State_0;
	int32_t ___m_ActionIndex_1;
};

// UnityEngine.InputSystem.InputActionMap/DeviceArray
struct DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E 
{
public:
	// System.Boolean UnityEngine.InputSystem.InputActionMap/DeviceArray::m_HaveValue
	bool ___m_HaveValue_0;
	// System.Int32 UnityEngine.InputSystem.InputActionMap/DeviceArray::m_DeviceCount
	int32_t ___m_DeviceCount_1;
	// UnityEngine.InputSystem.InputDevice[] UnityEngine.InputSystem.InputActionMap/DeviceArray::m_DeviceArray
	InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* ___m_DeviceArray_2;

public:
	inline static int32_t get_offset_of_m_HaveValue_0() { return static_cast<int32_t>(offsetof(DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E, ___m_HaveValue_0)); }
	inline bool get_m_HaveValue_0() const { return ___m_HaveValue_0; }
	inline bool* get_address_of_m_HaveValue_0() { return &___m_HaveValue_0; }
	inline void set_m_HaveValue_0(bool value)
	{
		___m_HaveValue_0 = value;
	}

	inline static int32_t get_offset_of_m_DeviceCount_1() { return static_cast<int32_t>(offsetof(DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E, ___m_DeviceCount_1)); }
	inline int32_t get_m_DeviceCount_1() const { return ___m_DeviceCount_1; }
	inline int32_t* get_address_of_m_DeviceCount_1() { return &___m_DeviceCount_1; }
	inline void set_m_DeviceCount_1(int32_t value)
	{
		___m_DeviceCount_1 = value;
	}

	inline static int32_t get_offset_of_m_DeviceArray_2() { return static_cast<int32_t>(offsetof(DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E, ___m_DeviceArray_2)); }
	inline InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* get_m_DeviceArray_2() const { return ___m_DeviceArray_2; }
	inline InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A** get_address_of_m_DeviceArray_2() { return &___m_DeviceArray_2; }
	inline void set_m_DeviceArray_2(InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* value)
	{
		___m_DeviceArray_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DeviceArray_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputActionMap/DeviceArray
struct DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E_marshaled_pinvoke
{
	int32_t ___m_HaveValue_0;
	int32_t ___m_DeviceCount_1;
	InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* ___m_DeviceArray_2;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputActionMap/DeviceArray
struct DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E_marshaled_com
{
	int32_t ___m_HaveValue_0;
	int32_t ___m_DeviceCount_1;
	InputDeviceU5BU5D_t2A471806FF5D6AEC78E15D7F97CA67DD3A50FF6A* ___m_DeviceArray_2;
};

// UnityEngine.InputSystem.Utilities.CallbackArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>>
struct CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F 
{
public:
	// System.Boolean UnityEngine.InputSystem.Utilities.CallbackArray`1::m_CannotMutateCallbacksArray
	bool ___m_CannotMutateCallbacksArray_0;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<TDelegate> UnityEngine.InputSystem.Utilities.CallbackArray`1::m_Callbacks
	InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  ___m_Callbacks_1;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<TDelegate> UnityEngine.InputSystem.Utilities.CallbackArray`1::m_CallbacksToAdd
	InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  ___m_CallbacksToAdd_2;
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<TDelegate> UnityEngine.InputSystem.Utilities.CallbackArray`1::m_CallbacksToRemove
	InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  ___m_CallbacksToRemove_3;

public:
	inline static int32_t get_offset_of_m_CannotMutateCallbacksArray_0() { return static_cast<int32_t>(offsetof(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F, ___m_CannotMutateCallbacksArray_0)); }
	inline bool get_m_CannotMutateCallbacksArray_0() const { return ___m_CannotMutateCallbacksArray_0; }
	inline bool* get_address_of_m_CannotMutateCallbacksArray_0() { return &___m_CannotMutateCallbacksArray_0; }
	inline void set_m_CannotMutateCallbacksArray_0(bool value)
	{
		___m_CannotMutateCallbacksArray_0 = value;
	}

	inline static int32_t get_offset_of_m_Callbacks_1() { return static_cast<int32_t>(offsetof(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F, ___m_Callbacks_1)); }
	inline InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  get_m_Callbacks_1() const { return ___m_Callbacks_1; }
	inline InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2 * get_address_of_m_Callbacks_1() { return &___m_Callbacks_1; }
	inline void set_m_Callbacks_1(InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  value)
	{
		___m_Callbacks_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Callbacks_1))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Callbacks_1))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_CallbacksToAdd_2() { return static_cast<int32_t>(offsetof(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F, ___m_CallbacksToAdd_2)); }
	inline InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  get_m_CallbacksToAdd_2() const { return ___m_CallbacksToAdd_2; }
	inline InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2 * get_address_of_m_CallbacksToAdd_2() { return &___m_CallbacksToAdd_2; }
	inline void set_m_CallbacksToAdd_2(InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  value)
	{
		___m_CallbacksToAdd_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CallbacksToAdd_2))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CallbacksToAdd_2))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_CallbacksToRemove_3() { return static_cast<int32_t>(offsetof(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F, ___m_CallbacksToRemove_3)); }
	inline InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  get_m_CallbacksToRemove_3() const { return ___m_CallbacksToRemove_3; }
	inline InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2 * get_address_of_m_CallbacksToRemove_3() { return &___m_CallbacksToRemove_3; }
	inline void set_m_CallbacksToRemove_3(InlinedArray_1_t7933FCE4B201C22FDF77DB3EF00D5E2FDFE2D4F2  value)
	{
		___m_CallbacksToRemove_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CallbacksToRemove_3))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_CallbacksToRemove_3))->___additionalValues_2), (void*)NULL);
		#endif
	}
};


// System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>
struct Nullable_1_t989E006C949E739D03D77F64841F9E3503807634 
{
public:
	// T System.Nullable`1::value
	ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t989E006C949E739D03D77F64841F9E3503807634, ___value_0)); }
	inline ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760  get_value_0() const { return ___value_0; }
	inline ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(ReadOnlyArray_1_tCFFC22293978D347B2AE03BF46334F9CE59BA760  value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Array_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t989E006C949E739D03D77F64841F9E3503807634, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Nullable`1<UnityEngine.InputSystem.InputAction/CallbackContext>
struct Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54 
{
public:
	// T System.Nullable`1::value
	CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54, ___value_0)); }
	inline CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  get_value_0() const { return ___value_0; }
	inline CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_State_0), (void*)NULL);
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.InputSystem.InputActionType
struct InputActionType_tE661F45D5272117B0C4314260BF7291D05FA98F8 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputActionType_tE661F45D5272117B0C4314260BF7291D05FA98F8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.LowLevel.InputStateBlock
struct InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C 
{
public:
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::<format>k__BackingField
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___U3CformatU3Ek__BackingField_33;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<byteOffset>k__BackingField
	uint32_t ___U3CbyteOffsetU3Ek__BackingField_34;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<bitOffset>k__BackingField
	uint32_t ___U3CbitOffsetU3Ek__BackingField_35;
	// System.UInt32 UnityEngine.InputSystem.LowLevel.InputStateBlock::<sizeInBits>k__BackingField
	uint32_t ___U3CsizeInBitsU3Ek__BackingField_36;

public:
	inline static int32_t get_offset_of_U3CformatU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CformatU3Ek__BackingField_33)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_U3CformatU3Ek__BackingField_33() const { return ___U3CformatU3Ek__BackingField_33; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_U3CformatU3Ek__BackingField_33() { return &___U3CformatU3Ek__BackingField_33; }
	inline void set_U3CformatU3Ek__BackingField_33(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___U3CformatU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CbyteOffsetU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CbyteOffsetU3Ek__BackingField_34)); }
	inline uint32_t get_U3CbyteOffsetU3Ek__BackingField_34() const { return ___U3CbyteOffsetU3Ek__BackingField_34; }
	inline uint32_t* get_address_of_U3CbyteOffsetU3Ek__BackingField_34() { return &___U3CbyteOffsetU3Ek__BackingField_34; }
	inline void set_U3CbyteOffsetU3Ek__BackingField_34(uint32_t value)
	{
		___U3CbyteOffsetU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CbitOffsetU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CbitOffsetU3Ek__BackingField_35)); }
	inline uint32_t get_U3CbitOffsetU3Ek__BackingField_35() const { return ___U3CbitOffsetU3Ek__BackingField_35; }
	inline uint32_t* get_address_of_U3CbitOffsetU3Ek__BackingField_35() { return &___U3CbitOffsetU3Ek__BackingField_35; }
	inline void set_U3CbitOffsetU3Ek__BackingField_35(uint32_t value)
	{
		___U3CbitOffsetU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of_U3CsizeInBitsU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C, ___U3CsizeInBitsU3Ek__BackingField_36)); }
	inline uint32_t get_U3CsizeInBitsU3Ek__BackingField_36() const { return ___U3CsizeInBitsU3Ek__BackingField_36; }
	inline uint32_t* get_address_of_U3CsizeInBitsU3Ek__BackingField_36() { return &___U3CsizeInBitsU3Ek__BackingField_36; }
	inline void set_U3CsizeInBitsU3Ek__BackingField_36(uint32_t value)
	{
		___U3CsizeInBitsU3Ek__BackingField_36 = value;
	}
};

struct InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields
{
public:
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatBit
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatBit_2;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatSBit
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatSBit_4;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatInt
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatInt_6;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatUInt
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatUInt_8;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatShort
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatShort_10;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatUShort
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatUShort_12;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatByte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatByte_14;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatSByte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatSByte_16;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatLong
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatLong_18;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatULong
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatULong_20;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatFloat
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatFloat_22;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatDouble
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatDouble_24;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector2_26;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector3_27;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatQuaternion
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatQuaternion_28;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2Short
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector2Short_29;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3Short
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector3Short_30;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector2Byte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector2Byte_31;
	// UnityEngine.InputSystem.Utilities.FourCC UnityEngine.InputSystem.LowLevel.InputStateBlock::FormatVector3Byte
	FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  ___FormatVector3Byte_32;

public:
	inline static int32_t get_offset_of_FormatBit_2() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatBit_2)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatBit_2() const { return ___FormatBit_2; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatBit_2() { return &___FormatBit_2; }
	inline void set_FormatBit_2(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatBit_2 = value;
	}

	inline static int32_t get_offset_of_FormatSBit_4() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatSBit_4)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatSBit_4() const { return ___FormatSBit_4; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatSBit_4() { return &___FormatSBit_4; }
	inline void set_FormatSBit_4(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatSBit_4 = value;
	}

	inline static int32_t get_offset_of_FormatInt_6() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatInt_6)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatInt_6() const { return ___FormatInt_6; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatInt_6() { return &___FormatInt_6; }
	inline void set_FormatInt_6(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatInt_6 = value;
	}

	inline static int32_t get_offset_of_FormatUInt_8() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatUInt_8)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatUInt_8() const { return ___FormatUInt_8; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatUInt_8() { return &___FormatUInt_8; }
	inline void set_FormatUInt_8(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatUInt_8 = value;
	}

	inline static int32_t get_offset_of_FormatShort_10() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatShort_10)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatShort_10() const { return ___FormatShort_10; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatShort_10() { return &___FormatShort_10; }
	inline void set_FormatShort_10(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatShort_10 = value;
	}

	inline static int32_t get_offset_of_FormatUShort_12() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatUShort_12)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatUShort_12() const { return ___FormatUShort_12; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatUShort_12() { return &___FormatUShort_12; }
	inline void set_FormatUShort_12(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatUShort_12 = value;
	}

	inline static int32_t get_offset_of_FormatByte_14() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatByte_14)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatByte_14() const { return ___FormatByte_14; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatByte_14() { return &___FormatByte_14; }
	inline void set_FormatByte_14(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatByte_14 = value;
	}

	inline static int32_t get_offset_of_FormatSByte_16() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatSByte_16)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatSByte_16() const { return ___FormatSByte_16; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatSByte_16() { return &___FormatSByte_16; }
	inline void set_FormatSByte_16(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatSByte_16 = value;
	}

	inline static int32_t get_offset_of_FormatLong_18() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatLong_18)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatLong_18() const { return ___FormatLong_18; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatLong_18() { return &___FormatLong_18; }
	inline void set_FormatLong_18(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatLong_18 = value;
	}

	inline static int32_t get_offset_of_FormatULong_20() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatULong_20)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatULong_20() const { return ___FormatULong_20; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatULong_20() { return &___FormatULong_20; }
	inline void set_FormatULong_20(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatULong_20 = value;
	}

	inline static int32_t get_offset_of_FormatFloat_22() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatFloat_22)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatFloat_22() const { return ___FormatFloat_22; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatFloat_22() { return &___FormatFloat_22; }
	inline void set_FormatFloat_22(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatFloat_22 = value;
	}

	inline static int32_t get_offset_of_FormatDouble_24() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatDouble_24)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatDouble_24() const { return ___FormatDouble_24; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatDouble_24() { return &___FormatDouble_24; }
	inline void set_FormatDouble_24(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatDouble_24 = value;
	}

	inline static int32_t get_offset_of_FormatVector2_26() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector2_26)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector2_26() const { return ___FormatVector2_26; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector2_26() { return &___FormatVector2_26; }
	inline void set_FormatVector2_26(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector2_26 = value;
	}

	inline static int32_t get_offset_of_FormatVector3_27() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector3_27)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector3_27() const { return ___FormatVector3_27; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector3_27() { return &___FormatVector3_27; }
	inline void set_FormatVector3_27(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector3_27 = value;
	}

	inline static int32_t get_offset_of_FormatQuaternion_28() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatQuaternion_28)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatQuaternion_28() const { return ___FormatQuaternion_28; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatQuaternion_28() { return &___FormatQuaternion_28; }
	inline void set_FormatQuaternion_28(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatQuaternion_28 = value;
	}

	inline static int32_t get_offset_of_FormatVector2Short_29() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector2Short_29)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector2Short_29() const { return ___FormatVector2Short_29; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector2Short_29() { return &___FormatVector2Short_29; }
	inline void set_FormatVector2Short_29(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector2Short_29 = value;
	}

	inline static int32_t get_offset_of_FormatVector3Short_30() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector3Short_30)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector3Short_30() const { return ___FormatVector3Short_30; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector3Short_30() { return &___FormatVector3Short_30; }
	inline void set_FormatVector3Short_30(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector3Short_30 = value;
	}

	inline static int32_t get_offset_of_FormatVector2Byte_31() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector2Byte_31)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector2Byte_31() const { return ___FormatVector2Byte_31; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector2Byte_31() { return &___FormatVector2Byte_31; }
	inline void set_FormatVector2Byte_31(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector2Byte_31 = value;
	}

	inline static int32_t get_offset_of_FormatVector3Byte_32() { return static_cast<int32_t>(offsetof(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C_StaticFields, ___FormatVector3Byte_32)); }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  get_FormatVector3Byte_32() const { return ___FormatVector3Byte_32; }
	inline FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A * get_address_of_FormatVector3Byte_32() { return &___FormatVector3Byte_32; }
	inline void set_FormatVector3Byte_32(FourCC_tBEC8228886DFE3C6BC4C72B4500954E34FB99D5A  value)
	{
		___FormatVector3Byte_32 = value;
	}
};


// System.Int32Enum
struct Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.TypeCode
struct TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C 
{
public:
	// System.Int32 System.TypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeCode_tCB39BAB5CFB7A1E0BCB521413E3C46B81C31AA7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.Controls.AxisControl/Clamp
struct Clamp_tA75145797DA57CB8885A8E5C8D7D0EF665DB4A78 
{
public:
	// System.Int32 UnityEngine.InputSystem.Controls.AxisControl/Clamp::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Clamp_tA75145797DA57CB8885A8E5C8D7D0EF665DB4A78, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputAction/ActionFlags
struct ActionFlags_tF7A7A714E4DA5D9B6EEA7A28BC14644997F86AB8 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputAction/ActionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ActionFlags_tF7A7A714E4DA5D9B6EEA7A28BC14644997F86AB8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputBinding/Flags
struct Flags_tF2A878BBBCA83F95D1D271ACD2096B98E1C99CB7 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputBinding/Flags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_tF2A878BBBCA83F95D1D271ACD2096B98E1C99CB7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.InputSystem.InputControl/ControlFlags
struct ControlFlags_tC210B095E24C31AC8E07C6E09B02CAA80C2563B6 
{
public:
	// System.Int32 UnityEngine.InputSystem.InputControl/ControlFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ControlFlags_tC210B095E24C31AC8E07C6E09B02CAA80C2563B6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.InputManager/InputForcedMode
struct InputForcedMode_tA0F203CE522FC1DC3ED85CAFE5A11AAFD0333838 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.InputManager/InputForcedMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InputForcedMode_tA0F203CE522FC1DC3ED85CAFE5A11AAFD0333838, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.CorgiEngine.InputManager/MovementControls
struct MovementControls_tD4A1D5B3031A26CDEB639251D532B1FDFB22E979 
{
public:
	// System.Int32 MoreMountains.CorgiEngine.InputManager/MovementControls::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MovementControls_tD4A1D5B3031A26CDEB639251D532B1FDFB22E979, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMInput/ButtonStates
struct ButtonStates_t3674E31E79E34E31FDF435834C5B1A41761F5FAC 
{
public:
	// System.Int32 MoreMountains.Tools.MMInput/ButtonStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ButtonStates_t3674E31E79E34E31FDF435834C5B1A41761F5FAC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// MoreMountains.Tools.MMStateMachine`1<System.Int32Enum>
struct MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA  : public RuntimeObject
{
public:
	// System.Boolean MoreMountains.Tools.MMStateMachine`1::<TriggerEvents>k__BackingField
	bool ___U3CTriggerEventsU3Ek__BackingField_0;
	// UnityEngine.GameObject MoreMountains.Tools.MMStateMachine`1::Target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Target_1;
	// T MoreMountains.Tools.MMStateMachine`1::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_2;
	// T MoreMountains.Tools.MMStateMachine`1::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_3;
	// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<T> MoreMountains.Tools.MMStateMachine`1::OnStateChange
	OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 * ___OnStateChange_4;

public:
	inline static int32_t get_offset_of_U3CTriggerEventsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___U3CTriggerEventsU3Ek__BackingField_0)); }
	inline bool get_U3CTriggerEventsU3Ek__BackingField_0() const { return ___U3CTriggerEventsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTriggerEventsU3Ek__BackingField_0() { return &___U3CTriggerEventsU3Ek__BackingField_0; }
	inline void set_U3CTriggerEventsU3Ek__BackingField_0(bool value)
	{
		___U3CTriggerEventsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_Target_1() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___Target_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Target_1() const { return ___Target_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Target_1() { return &___Target_1; }
	inline void set_Target_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___U3CCurrentStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_2() const { return ___U3CCurrentStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_2() { return &___U3CCurrentStateU3Ek__BackingField_2; }
	inline void set_U3CCurrentStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___U3CPreviousStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_3() const { return ___U3CPreviousStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_3() { return &___U3CPreviousStateU3Ek__BackingField_3; }
	inline void set_U3CPreviousStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_OnStateChange_4() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA, ___OnStateChange_4)); }
	inline OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 * get_OnStateChange_4() const { return ___OnStateChange_4; }
	inline OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 ** get_address_of_OnStateChange_4() { return &___OnStateChange_4; }
	inline void set_OnStateChange_4(OnStateChangeDelegate_tAC10083983DC1963B6E32E7EC3469A1B01554B17 * value)
	{
		___OnStateChange_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStateChange_4), (void*)value);
	}
};


// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>
struct MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE  : public RuntimeObject
{
public:
	// System.Boolean MoreMountains.Tools.MMStateMachine`1::<TriggerEvents>k__BackingField
	bool ___U3CTriggerEventsU3Ek__BackingField_0;
	// UnityEngine.GameObject MoreMountains.Tools.MMStateMachine`1::Target
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___Target_1;
	// T MoreMountains.Tools.MMStateMachine`1::<CurrentState>k__BackingField
	int32_t ___U3CCurrentStateU3Ek__BackingField_2;
	// T MoreMountains.Tools.MMStateMachine`1::<PreviousState>k__BackingField
	int32_t ___U3CPreviousStateU3Ek__BackingField_3;
	// MoreMountains.Tools.MMStateMachine`1/OnStateChangeDelegate<T> MoreMountains.Tools.MMStateMachine`1::OnStateChange
	OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 * ___OnStateChange_4;

public:
	inline static int32_t get_offset_of_U3CTriggerEventsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___U3CTriggerEventsU3Ek__BackingField_0)); }
	inline bool get_U3CTriggerEventsU3Ek__BackingField_0() const { return ___U3CTriggerEventsU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTriggerEventsU3Ek__BackingField_0() { return &___U3CTriggerEventsU3Ek__BackingField_0; }
	inline void set_U3CTriggerEventsU3Ek__BackingField_0(bool value)
	{
		___U3CTriggerEventsU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_Target_1() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___Target_1)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_Target_1() const { return ___Target_1; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_Target_1() { return &___Target_1; }
	inline void set_Target_1(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___Target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Target_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCurrentStateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___U3CCurrentStateU3Ek__BackingField_2)); }
	inline int32_t get_U3CCurrentStateU3Ek__BackingField_2() const { return ___U3CCurrentStateU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CCurrentStateU3Ek__BackingField_2() { return &___U3CCurrentStateU3Ek__BackingField_2; }
	inline void set_U3CCurrentStateU3Ek__BackingField_2(int32_t value)
	{
		___U3CCurrentStateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPreviousStateU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___U3CPreviousStateU3Ek__BackingField_3)); }
	inline int32_t get_U3CPreviousStateU3Ek__BackingField_3() const { return ___U3CPreviousStateU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CPreviousStateU3Ek__BackingField_3() { return &___U3CPreviousStateU3Ek__BackingField_3; }
	inline void set_U3CPreviousStateU3Ek__BackingField_3(int32_t value)
	{
		___U3CPreviousStateU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_OnStateChange_4() { return static_cast<int32_t>(offsetof(MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE, ___OnStateChange_4)); }
	inline OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 * get_OnStateChange_4() const { return ___OnStateChange_4; }
	inline OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 ** get_address_of_OnStateChange_4() { return &___OnStateChange_4; }
	inline void set_OnStateChange_4(OnStateChangeDelegate_t638CEA54BB89E158689DE3A81E48A62D7658E321 * value)
	{
		___OnStateChange_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnStateChange_4), (void*)value);
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.InputSystem.InputBinding
struct InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB 
{
public:
	// System.String UnityEngine.InputSystem.InputBinding::m_Name
	String_t* ___m_Name_2;
	// System.String UnityEngine.InputSystem.InputBinding::m_Id
	String_t* ___m_Id_3;
	// System.String UnityEngine.InputSystem.InputBinding::m_Path
	String_t* ___m_Path_4;
	// System.String UnityEngine.InputSystem.InputBinding::m_Interactions
	String_t* ___m_Interactions_5;
	// System.String UnityEngine.InputSystem.InputBinding::m_Processors
	String_t* ___m_Processors_6;
	// System.String UnityEngine.InputSystem.InputBinding::m_Groups
	String_t* ___m_Groups_7;
	// System.String UnityEngine.InputSystem.InputBinding::m_Action
	String_t* ___m_Action_8;
	// UnityEngine.InputSystem.InputBinding/Flags UnityEngine.InputSystem.InputBinding::m_Flags
	int32_t ___m_Flags_9;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverridePath
	String_t* ___m_OverridePath_10;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideInteractions
	String_t* ___m_OverrideInteractions_11;
	// System.String UnityEngine.InputSystem.InputBinding::m_OverrideProcessors
	String_t* ___m_OverrideProcessors_12;

public:
	inline static int32_t get_offset_of_m_Name_2() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Name_2)); }
	inline String_t* get_m_Name_2() const { return ___m_Name_2; }
	inline String_t** get_address_of_m_Name_2() { return &___m_Name_2; }
	inline void set_m_Name_2(String_t* value)
	{
		___m_Name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_3() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Id_3)); }
	inline String_t* get_m_Id_3() const { return ___m_Id_3; }
	inline String_t** get_address_of_m_Id_3() { return &___m_Id_3; }
	inline void set_m_Id_3(String_t* value)
	{
		___m_Id_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Path_4() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Path_4)); }
	inline String_t* get_m_Path_4() const { return ___m_Path_4; }
	inline String_t** get_address_of_m_Path_4() { return &___m_Path_4; }
	inline void set_m_Path_4(String_t* value)
	{
		___m_Path_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactions_5() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Interactions_5)); }
	inline String_t* get_m_Interactions_5() const { return ___m_Interactions_5; }
	inline String_t** get_address_of_m_Interactions_5() { return &___m_Interactions_5; }
	inline void set_m_Interactions_5(String_t* value)
	{
		___m_Interactions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactions_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Processors_6() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Processors_6)); }
	inline String_t* get_m_Processors_6() const { return ___m_Processors_6; }
	inline String_t** get_address_of_m_Processors_6() { return &___m_Processors_6; }
	inline void set_m_Processors_6(String_t* value)
	{
		___m_Processors_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Processors_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Groups_7() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Groups_7)); }
	inline String_t* get_m_Groups_7() const { return ___m_Groups_7; }
	inline String_t** get_address_of_m_Groups_7() { return &___m_Groups_7; }
	inline void set_m_Groups_7(String_t* value)
	{
		___m_Groups_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Groups_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Action_8() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Action_8)); }
	inline String_t* get_m_Action_8() const { return ___m_Action_8; }
	inline String_t** get_address_of_m_Action_8() { return &___m_Action_8; }
	inline void set_m_Action_8(String_t* value)
	{
		___m_Action_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Action_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_9() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_Flags_9)); }
	inline int32_t get_m_Flags_9() const { return ___m_Flags_9; }
	inline int32_t* get_address_of_m_Flags_9() { return &___m_Flags_9; }
	inline void set_m_Flags_9(int32_t value)
	{
		___m_Flags_9 = value;
	}

	inline static int32_t get_offset_of_m_OverridePath_10() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_OverridePath_10)); }
	inline String_t* get_m_OverridePath_10() const { return ___m_OverridePath_10; }
	inline String_t** get_address_of_m_OverridePath_10() { return &___m_OverridePath_10; }
	inline void set_m_OverridePath_10(String_t* value)
	{
		___m_OverridePath_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverridePath_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideInteractions_11() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_OverrideInteractions_11)); }
	inline String_t* get_m_OverrideInteractions_11() const { return ___m_OverrideInteractions_11; }
	inline String_t** get_address_of_m_OverrideInteractions_11() { return &___m_OverrideInteractions_11; }
	inline void set_m_OverrideInteractions_11(String_t* value)
	{
		___m_OverrideInteractions_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideInteractions_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideProcessors_12() { return static_cast<int32_t>(offsetof(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB, ___m_OverrideProcessors_12)); }
	inline String_t* get_m_OverrideProcessors_12() const { return ___m_OverrideProcessors_12; }
	inline String_t** get_address_of_m_OverrideProcessors_12() { return &___m_OverrideProcessors_12; }
	inline void set_m_OverrideProcessors_12(String_t* value)
	{
		___m_OverrideProcessors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideProcessors_12), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB_marshaled_pinvoke
{
	char* ___m_Name_2;
	char* ___m_Id_3;
	char* ___m_Path_4;
	char* ___m_Interactions_5;
	char* ___m_Processors_6;
	char* ___m_Groups_7;
	char* ___m_Action_8;
	int32_t ___m_Flags_9;
	char* ___m_OverridePath_10;
	char* ___m_OverrideInteractions_11;
	char* ___m_OverrideProcessors_12;
};
// Native definition for COM marshalling of UnityEngine.InputSystem.InputBinding
struct InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB_marshaled_com
{
	Il2CppChar* ___m_Name_2;
	Il2CppChar* ___m_Id_3;
	Il2CppChar* ___m_Path_4;
	Il2CppChar* ___m_Interactions_5;
	Il2CppChar* ___m_Processors_6;
	Il2CppChar* ___m_Groups_7;
	Il2CppChar* ___m_Action_8;
	int32_t ___m_Flags_9;
	Il2CppChar* ___m_OverridePath_10;
	Il2CppChar* ___m_OverrideInteractions_11;
	Il2CppChar* ___m_OverrideProcessors_12;
};

// UnityEngine.InputSystem.InputValue
struct InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971  : public RuntimeObject
{
public:
	// System.Nullable`1<UnityEngine.InputSystem.InputAction/CallbackContext> UnityEngine.InputSystem.InputValue::m_Context
	Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54  ___m_Context_0;

public:
	inline static int32_t get_offset_of_m_Context_0() { return static_cast<int32_t>(offsetof(InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971, ___m_Context_0)); }
	inline Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54  get_m_Context_0() const { return ___m_Context_0; }
	inline Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54 * get_address_of_m_Context_0() { return &___m_Context_0; }
	inline void set_m_Context_0(Nullable_1_tF98918829055F430BF8F54C29C1A766B290DFF54  value)
	{
		___m_Context_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_Context_0))->___value_0))->___m_State_0), (void*)NULL);
	}
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.TypeCode UnityEngine.InputSystem.Utilities.PrimitiveValue::m_Type
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			// System.Boolean UnityEngine.InputSystem.Utilities.PrimitiveValue::m_BoolValue
			bool ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			bool ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			// System.Char UnityEngine.InputSystem.Utilities.PrimitiveValue::m_CharValue
			Il2CppChar ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			Il2CppChar ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			// System.Byte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ByteValue
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			// System.SByte UnityEngine.InputSystem.Utilities.PrimitiveValue::m_SByteValue
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			// System.Int16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ShortValue
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			// System.UInt16 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UShortValue
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			// System.Int32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_IntValue
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			// System.UInt32 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_UIntValue
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			// System.Int64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_LongValue
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			// System.UInt64 UnityEngine.InputSystem.Utilities.PrimitiveValue::m_ULongValue
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			// System.Single UnityEngine.InputSystem.Utilities.PrimitiveValue::m_FloatValue
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			// System.Double UnityEngine.InputSystem.Utilities.PrimitiveValue::m_DoubleValue
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_m_Type_0() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_Type_0)); }
	inline int32_t get_m_Type_0() const { return ___m_Type_0; }
	inline int32_t* get_address_of_m_Type_0() { return &___m_Type_0; }
	inline void set_m_Type_0(int32_t value)
	{
		___m_Type_0 = value;
	}

	inline static int32_t get_offset_of_m_BoolValue_1() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_BoolValue_1)); }
	inline bool get_m_BoolValue_1() const { return ___m_BoolValue_1; }
	inline bool* get_address_of_m_BoolValue_1() { return &___m_BoolValue_1; }
	inline void set_m_BoolValue_1(bool value)
	{
		___m_BoolValue_1 = value;
	}

	inline static int32_t get_offset_of_m_CharValue_2() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_CharValue_2)); }
	inline Il2CppChar get_m_CharValue_2() const { return ___m_CharValue_2; }
	inline Il2CppChar* get_address_of_m_CharValue_2() { return &___m_CharValue_2; }
	inline void set_m_CharValue_2(Il2CppChar value)
	{
		___m_CharValue_2 = value;
	}

	inline static int32_t get_offset_of_m_ByteValue_3() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ByteValue_3)); }
	inline uint8_t get_m_ByteValue_3() const { return ___m_ByteValue_3; }
	inline uint8_t* get_address_of_m_ByteValue_3() { return &___m_ByteValue_3; }
	inline void set_m_ByteValue_3(uint8_t value)
	{
		___m_ByteValue_3 = value;
	}

	inline static int32_t get_offset_of_m_SByteValue_4() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_SByteValue_4)); }
	inline int8_t get_m_SByteValue_4() const { return ___m_SByteValue_4; }
	inline int8_t* get_address_of_m_SByteValue_4() { return &___m_SByteValue_4; }
	inline void set_m_SByteValue_4(int8_t value)
	{
		___m_SByteValue_4 = value;
	}

	inline static int32_t get_offset_of_m_ShortValue_5() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ShortValue_5)); }
	inline int16_t get_m_ShortValue_5() const { return ___m_ShortValue_5; }
	inline int16_t* get_address_of_m_ShortValue_5() { return &___m_ShortValue_5; }
	inline void set_m_ShortValue_5(int16_t value)
	{
		___m_ShortValue_5 = value;
	}

	inline static int32_t get_offset_of_m_UShortValue_6() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_UShortValue_6)); }
	inline uint16_t get_m_UShortValue_6() const { return ___m_UShortValue_6; }
	inline uint16_t* get_address_of_m_UShortValue_6() { return &___m_UShortValue_6; }
	inline void set_m_UShortValue_6(uint16_t value)
	{
		___m_UShortValue_6 = value;
	}

	inline static int32_t get_offset_of_m_IntValue_7() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_IntValue_7)); }
	inline int32_t get_m_IntValue_7() const { return ___m_IntValue_7; }
	inline int32_t* get_address_of_m_IntValue_7() { return &___m_IntValue_7; }
	inline void set_m_IntValue_7(int32_t value)
	{
		___m_IntValue_7 = value;
	}

	inline static int32_t get_offset_of_m_UIntValue_8() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_UIntValue_8)); }
	inline uint32_t get_m_UIntValue_8() const { return ___m_UIntValue_8; }
	inline uint32_t* get_address_of_m_UIntValue_8() { return &___m_UIntValue_8; }
	inline void set_m_UIntValue_8(uint32_t value)
	{
		___m_UIntValue_8 = value;
	}

	inline static int32_t get_offset_of_m_LongValue_9() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_LongValue_9)); }
	inline int64_t get_m_LongValue_9() const { return ___m_LongValue_9; }
	inline int64_t* get_address_of_m_LongValue_9() { return &___m_LongValue_9; }
	inline void set_m_LongValue_9(int64_t value)
	{
		___m_LongValue_9 = value;
	}

	inline static int32_t get_offset_of_m_ULongValue_10() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_ULongValue_10)); }
	inline uint64_t get_m_ULongValue_10() const { return ___m_ULongValue_10; }
	inline uint64_t* get_address_of_m_ULongValue_10() { return &___m_ULongValue_10; }
	inline void set_m_ULongValue_10(uint64_t value)
	{
		___m_ULongValue_10 = value;
	}

	inline static int32_t get_offset_of_m_FloatValue_11() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_FloatValue_11)); }
	inline float get_m_FloatValue_11() const { return ___m_FloatValue_11; }
	inline float* get_address_of_m_FloatValue_11() { return &___m_FloatValue_11; }
	inline void set_m_FloatValue_11(float value)
	{
		___m_FloatValue_11 = value;
	}

	inline static int32_t get_offset_of_m_DoubleValue_12() { return static_cast<int32_t>(offsetof(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA, ___m_DoubleValue_12)); }
	inline double get_m_DoubleValue_12() const { return ___m_DoubleValue_12; }
	inline double* get_address_of_m_DoubleValue_12() { return &___m_DoubleValue_12; }
	inline void set_m_DoubleValue_12(double value)
	{
		___m_DoubleValue_12 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_pinvoke
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};
// Native definition for COM marshalling of UnityEngine.InputSystem.Utilities.PrimitiveValue
struct PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA_marshaled_com
{
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			int32_t ___m_Type_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___m_Type_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding[4];
			int32_t ___m_BoolValue_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_BoolValue_1_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_BoolValue_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_CharValue_2_OffsetPadding[4];
			uint8_t ___m_CharValue_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_CharValue_2_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_CharValue_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding[4];
			uint8_t ___m_ByteValue_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ByteValue_3_OffsetPadding_forAlignmentOnly[4];
			uint8_t ___m_ByteValue_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding[4];
			int8_t ___m_SByteValue_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_SByteValue_4_OffsetPadding_forAlignmentOnly[4];
			int8_t ___m_SByteValue_4_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding[4];
			int16_t ___m_ShortValue_5;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ShortValue_5_OffsetPadding_forAlignmentOnly[4];
			int16_t ___m_ShortValue_5_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding[4];
			uint16_t ___m_UShortValue_6;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UShortValue_6_OffsetPadding_forAlignmentOnly[4];
			uint16_t ___m_UShortValue_6_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_IntValue_7_OffsetPadding[4];
			int32_t ___m_IntValue_7;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_IntValue_7_OffsetPadding_forAlignmentOnly[4];
			int32_t ___m_IntValue_7_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding[4];
			uint32_t ___m_UIntValue_8;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_UIntValue_8_OffsetPadding_forAlignmentOnly[4];
			uint32_t ___m_UIntValue_8_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_LongValue_9_OffsetPadding[4];
			int64_t ___m_LongValue_9;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_LongValue_9_OffsetPadding_forAlignmentOnly[4];
			int64_t ___m_LongValue_9_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding[4];
			uint64_t ___m_ULongValue_10;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_ULongValue_10_OffsetPadding_forAlignmentOnly[4];
			uint64_t ___m_ULongValue_10_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding[4];
			float ___m_FloatValue_11;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_FloatValue_11_OffsetPadding_forAlignmentOnly[4];
			float ___m_FloatValue_11_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding[4];
			double ___m_DoubleValue_12;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___m_DoubleValue_12_OffsetPadding_forAlignmentOnly[4];
			double ___m_DoubleValue_12_forAlignmentOnly;
		};
	};
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>
struct Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D  : public MulticastDelegate_t
{
public:

public:
};


// System.Nullable`1<UnityEngine.InputSystem.InputBinding>
struct Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597 
{
public:
	// T System.Nullable`1::value
	InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597, ___value_0)); }
	inline InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB  get_value_0() const { return ___value_0; }
	inline InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB  value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.InputSystem.InputControl
struct InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713  : public RuntimeObject
{
public:
	// UnityEngine.InputSystem.LowLevel.InputStateBlock UnityEngine.InputSystem.InputControl::m_StateBlock
	InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C  ___m_StateBlock_0;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Name
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___m_Name_1;
	// System.String UnityEngine.InputSystem.InputControl::m_Path
	String_t* ___m_Path_2;
	// System.String UnityEngine.InputSystem.InputControl::m_DisplayName
	String_t* ___m_DisplayName_3;
	// System.String UnityEngine.InputSystem.InputControl::m_DisplayNameFromLayout
	String_t* ___m_DisplayNameFromLayout_4;
	// System.String UnityEngine.InputSystem.InputControl::m_ShortDisplayName
	String_t* ___m_ShortDisplayName_5;
	// System.String UnityEngine.InputSystem.InputControl::m_ShortDisplayNameFromLayout
	String_t* ___m_ShortDisplayNameFromLayout_6;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Layout
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___m_Layout_7;
	// UnityEngine.InputSystem.Utilities.InternedString UnityEngine.InputSystem.InputControl::m_Variants
	InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  ___m_Variants_8;
	// UnityEngine.InputSystem.InputDevice UnityEngine.InputSystem.InputControl::m_Device
	InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 * ___m_Device_9;
	// UnityEngine.InputSystem.InputControl UnityEngine.InputSystem.InputControl::m_Parent
	InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * ___m_Parent_10;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_UsageCount
	int32_t ___m_UsageCount_11;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_UsageStartIndex
	int32_t ___m_UsageStartIndex_12;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_AliasCount
	int32_t ___m_AliasCount_13;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_AliasStartIndex
	int32_t ___m_AliasStartIndex_14;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_ChildCount
	int32_t ___m_ChildCount_15;
	// System.Int32 UnityEngine.InputSystem.InputControl::m_ChildStartIndex
	int32_t ___m_ChildStartIndex_16;
	// UnityEngine.InputSystem.InputControl/ControlFlags UnityEngine.InputSystem.InputControl::m_ControlFlags
	int32_t ___m_ControlFlags_17;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_DefaultState
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___m_DefaultState_18;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_MinValue
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___m_MinValue_19;
	// UnityEngine.InputSystem.Utilities.PrimitiveValue UnityEngine.InputSystem.InputControl::m_MaxValue
	PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  ___m_MaxValue_20;

public:
	inline static int32_t get_offset_of_m_StateBlock_0() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_StateBlock_0)); }
	inline InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C  get_m_StateBlock_0() const { return ___m_StateBlock_0; }
	inline InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C * get_address_of_m_StateBlock_0() { return &___m_StateBlock_0; }
	inline void set_m_StateBlock_0(InputStateBlock_t37A383311518FFFA424D1F8F7E355DBFC215624C  value)
	{
		___m_StateBlock_0 = value;
	}

	inline static int32_t get_offset_of_m_Name_1() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Name_1)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_m_Name_1() const { return ___m_Name_1; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_m_Name_1() { return &___m_Name_1; }
	inline void set_m_Name_1(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___m_Name_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_1))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Name_1))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Path_2() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Path_2)); }
	inline String_t* get_m_Path_2() const { return ___m_Path_2; }
	inline String_t** get_address_of_m_Path_2() { return &___m_Path_2; }
	inline void set_m_Path_2(String_t* value)
	{
		___m_Path_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Path_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayName_3() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_DisplayName_3)); }
	inline String_t* get_m_DisplayName_3() const { return ___m_DisplayName_3; }
	inline String_t** get_address_of_m_DisplayName_3() { return &___m_DisplayName_3; }
	inline void set_m_DisplayName_3(String_t* value)
	{
		___m_DisplayName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayName_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisplayNameFromLayout_4() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_DisplayNameFromLayout_4)); }
	inline String_t* get_m_DisplayNameFromLayout_4() const { return ___m_DisplayNameFromLayout_4; }
	inline String_t** get_address_of_m_DisplayNameFromLayout_4() { return &___m_DisplayNameFromLayout_4; }
	inline void set_m_DisplayNameFromLayout_4(String_t* value)
	{
		___m_DisplayNameFromLayout_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisplayNameFromLayout_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShortDisplayName_5() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ShortDisplayName_5)); }
	inline String_t* get_m_ShortDisplayName_5() const { return ___m_ShortDisplayName_5; }
	inline String_t** get_address_of_m_ShortDisplayName_5() { return &___m_ShortDisplayName_5; }
	inline void set_m_ShortDisplayName_5(String_t* value)
	{
		___m_ShortDisplayName_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShortDisplayName_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShortDisplayNameFromLayout_6() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ShortDisplayNameFromLayout_6)); }
	inline String_t* get_m_ShortDisplayNameFromLayout_6() const { return ___m_ShortDisplayNameFromLayout_6; }
	inline String_t** get_address_of_m_ShortDisplayNameFromLayout_6() { return &___m_ShortDisplayNameFromLayout_6; }
	inline void set_m_ShortDisplayNameFromLayout_6(String_t* value)
	{
		___m_ShortDisplayNameFromLayout_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ShortDisplayNameFromLayout_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Layout_7() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Layout_7)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_m_Layout_7() const { return ___m_Layout_7; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_m_Layout_7() { return &___m_Layout_7; }
	inline void set_m_Layout_7(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___m_Layout_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Layout_7))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Layout_7))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Variants_8() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Variants_8)); }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  get_m_Variants_8() const { return ___m_Variants_8; }
	inline InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4 * get_address_of_m_Variants_8() { return &___m_Variants_8; }
	inline void set_m_Variants_8(InternedString_t01D20018001F1112F6D24F765D888CA7E8DCF0B4  value)
	{
		___m_Variants_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_8))->___m_StringOriginalCase_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Variants_8))->___m_StringLowerCase_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Device_9() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Device_9)); }
	inline InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 * get_m_Device_9() const { return ___m_Device_9; }
	inline InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 ** get_address_of_m_Device_9() { return &___m_Device_9; }
	inline void set_m_Device_9(InputDevice_t32674BAC770EE0590FA5023A70AE7D3B3AA9A154 * value)
	{
		___m_Device_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Device_9), (void*)value);
	}

	inline static int32_t get_offset_of_m_Parent_10() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_Parent_10)); }
	inline InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * get_m_Parent_10() const { return ___m_Parent_10; }
	inline InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 ** get_address_of_m_Parent_10() { return &___m_Parent_10; }
	inline void set_m_Parent_10(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * value)
	{
		___m_Parent_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Parent_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_UsageCount_11() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_UsageCount_11)); }
	inline int32_t get_m_UsageCount_11() const { return ___m_UsageCount_11; }
	inline int32_t* get_address_of_m_UsageCount_11() { return &___m_UsageCount_11; }
	inline void set_m_UsageCount_11(int32_t value)
	{
		___m_UsageCount_11 = value;
	}

	inline static int32_t get_offset_of_m_UsageStartIndex_12() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_UsageStartIndex_12)); }
	inline int32_t get_m_UsageStartIndex_12() const { return ___m_UsageStartIndex_12; }
	inline int32_t* get_address_of_m_UsageStartIndex_12() { return &___m_UsageStartIndex_12; }
	inline void set_m_UsageStartIndex_12(int32_t value)
	{
		___m_UsageStartIndex_12 = value;
	}

	inline static int32_t get_offset_of_m_AliasCount_13() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_AliasCount_13)); }
	inline int32_t get_m_AliasCount_13() const { return ___m_AliasCount_13; }
	inline int32_t* get_address_of_m_AliasCount_13() { return &___m_AliasCount_13; }
	inline void set_m_AliasCount_13(int32_t value)
	{
		___m_AliasCount_13 = value;
	}

	inline static int32_t get_offset_of_m_AliasStartIndex_14() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_AliasStartIndex_14)); }
	inline int32_t get_m_AliasStartIndex_14() const { return ___m_AliasStartIndex_14; }
	inline int32_t* get_address_of_m_AliasStartIndex_14() { return &___m_AliasStartIndex_14; }
	inline void set_m_AliasStartIndex_14(int32_t value)
	{
		___m_AliasStartIndex_14 = value;
	}

	inline static int32_t get_offset_of_m_ChildCount_15() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ChildCount_15)); }
	inline int32_t get_m_ChildCount_15() const { return ___m_ChildCount_15; }
	inline int32_t* get_address_of_m_ChildCount_15() { return &___m_ChildCount_15; }
	inline void set_m_ChildCount_15(int32_t value)
	{
		___m_ChildCount_15 = value;
	}

	inline static int32_t get_offset_of_m_ChildStartIndex_16() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ChildStartIndex_16)); }
	inline int32_t get_m_ChildStartIndex_16() const { return ___m_ChildStartIndex_16; }
	inline int32_t* get_address_of_m_ChildStartIndex_16() { return &___m_ChildStartIndex_16; }
	inline void set_m_ChildStartIndex_16(int32_t value)
	{
		___m_ChildStartIndex_16 = value;
	}

	inline static int32_t get_offset_of_m_ControlFlags_17() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_ControlFlags_17)); }
	inline int32_t get_m_ControlFlags_17() const { return ___m_ControlFlags_17; }
	inline int32_t* get_address_of_m_ControlFlags_17() { return &___m_ControlFlags_17; }
	inline void set_m_ControlFlags_17(int32_t value)
	{
		___m_ControlFlags_17 = value;
	}

	inline static int32_t get_offset_of_m_DefaultState_18() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_DefaultState_18)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_m_DefaultState_18() const { return ___m_DefaultState_18; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_m_DefaultState_18() { return &___m_DefaultState_18; }
	inline void set_m_DefaultState_18(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___m_DefaultState_18 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_19() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_MinValue_19)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_m_MinValue_19() const { return ___m_MinValue_19; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_m_MinValue_19() { return &___m_MinValue_19; }
	inline void set_m_MinValue_19(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___m_MinValue_19 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_20() { return static_cast<int32_t>(offsetof(InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713, ___m_MaxValue_20)); }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  get_m_MaxValue_20() const { return ___m_MaxValue_20; }
	inline PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA * get_address_of_m_MaxValue_20() { return &___m_MaxValue_20; }
	inline void set_m_MaxValue_20(PrimitiveValue_tB100F9C42BB438CE358E5989697410B27AFB3ADA  value)
	{
		___m_MaxValue_20 = value;
	}
};


// UnityEngine.InputSystem.InputControl`1<System.Single>
struct InputControl_1_t2273064B5872656E668E007A1CAB0661D3FD40B8  : public InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713
{
public:
	// UnityEngine.InputSystem.Utilities.InlinedArray`1<UnityEngine.InputSystem.InputProcessor`1<TValue>> UnityEngine.InputSystem.InputControl`1::m_ProcessorStack
	InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA  ___m_ProcessorStack_21;

public:
	inline static int32_t get_offset_of_m_ProcessorStack_21() { return static_cast<int32_t>(offsetof(InputControl_1_t2273064B5872656E668E007A1CAB0661D3FD40B8, ___m_ProcessorStack_21)); }
	inline InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA  get_m_ProcessorStack_21() const { return ___m_ProcessorStack_21; }
	inline InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA * get_address_of_m_ProcessorStack_21() { return &___m_ProcessorStack_21; }
	inline void set_m_ProcessorStack_21(InlinedArray_1_t79EADA767F4F018EC1BCDD4B31505DF941ACACDA  value)
	{
		___m_ProcessorStack_21 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ProcessorStack_21))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_ProcessorStack_21))->___additionalValues_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.InputSystem.InputAction
struct InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B  : public RuntimeObject
{
public:
	// System.String UnityEngine.InputSystem.InputAction::m_Name
	String_t* ___m_Name_0;
	// UnityEngine.InputSystem.InputActionType UnityEngine.InputSystem.InputAction::m_Type
	int32_t ___m_Type_1;
	// System.String UnityEngine.InputSystem.InputAction::m_ExpectedControlType
	String_t* ___m_ExpectedControlType_2;
	// System.String UnityEngine.InputSystem.InputAction::m_Id
	String_t* ___m_Id_3;
	// System.String UnityEngine.InputSystem.InputAction::m_Processors
	String_t* ___m_Processors_4;
	// System.String UnityEngine.InputSystem.InputAction::m_Interactions
	String_t* ___m_Interactions_5;
	// UnityEngine.InputSystem.InputBinding[] UnityEngine.InputSystem.InputAction::m_SingletonActionBindings
	InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* ___m_SingletonActionBindings_6;
	// UnityEngine.InputSystem.InputAction/ActionFlags UnityEngine.InputSystem.InputAction::m_Flags
	int32_t ___m_Flags_7;
	// System.Nullable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputAction::m_BindingMask
	Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  ___m_BindingMask_8;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_BindingsStartIndex
	int32_t ___m_BindingsStartIndex_9;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_BindingsCount
	int32_t ___m_BindingsCount_10;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_ControlStartIndex
	int32_t ___m_ControlStartIndex_11;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_ControlCount
	int32_t ___m_ControlCount_12;
	// System.Int32 UnityEngine.InputSystem.InputAction::m_ActionIndexInState
	int32_t ___m_ActionIndexInState_13;
	// UnityEngine.InputSystem.InputActionMap UnityEngine.InputSystem.InputAction::m_ActionMap
	InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * ___m_ActionMap_14;
	// UnityEngine.InputSystem.Utilities.CallbackArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputAction::m_OnStarted
	CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  ___m_OnStarted_15;
	// UnityEngine.InputSystem.Utilities.CallbackArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputAction::m_OnCanceled
	CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  ___m_OnCanceled_16;
	// UnityEngine.InputSystem.Utilities.CallbackArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputAction::m_OnPerformed
	CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  ___m_OnPerformed_17;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_ExpectedControlType_2() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_ExpectedControlType_2)); }
	inline String_t* get_m_ExpectedControlType_2() const { return ___m_ExpectedControlType_2; }
	inline String_t** get_address_of_m_ExpectedControlType_2() { return &___m_ExpectedControlType_2; }
	inline void set_m_ExpectedControlType_2(String_t* value)
	{
		___m_ExpectedControlType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ExpectedControlType_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_3() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_Id_3)); }
	inline String_t* get_m_Id_3() const { return ___m_Id_3; }
	inline String_t** get_address_of_m_Id_3() { return &___m_Id_3; }
	inline void set_m_Id_3(String_t* value)
	{
		___m_Id_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Processors_4() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_Processors_4)); }
	inline String_t* get_m_Processors_4() const { return ___m_Processors_4; }
	inline String_t** get_address_of_m_Processors_4() { return &___m_Processors_4; }
	inline void set_m_Processors_4(String_t* value)
	{
		___m_Processors_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Processors_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactions_5() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_Interactions_5)); }
	inline String_t* get_m_Interactions_5() const { return ___m_Interactions_5; }
	inline String_t** get_address_of_m_Interactions_5() { return &___m_Interactions_5; }
	inline void set_m_Interactions_5(String_t* value)
	{
		___m_Interactions_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Interactions_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_SingletonActionBindings_6() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_SingletonActionBindings_6)); }
	inline InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* get_m_SingletonActionBindings_6() const { return ___m_SingletonActionBindings_6; }
	inline InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B** get_address_of_m_SingletonActionBindings_6() { return &___m_SingletonActionBindings_6; }
	inline void set_m_SingletonActionBindings_6(InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* value)
	{
		___m_SingletonActionBindings_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SingletonActionBindings_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Flags_7() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_Flags_7)); }
	inline int32_t get_m_Flags_7() const { return ___m_Flags_7; }
	inline int32_t* get_address_of_m_Flags_7() { return &___m_Flags_7; }
	inline void set_m_Flags_7(int32_t value)
	{
		___m_Flags_7 = value;
	}

	inline static int32_t get_offset_of_m_BindingMask_8() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_BindingMask_8)); }
	inline Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  get_m_BindingMask_8() const { return ___m_BindingMask_8; }
	inline Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597 * get_address_of_m_BindingMask_8() { return &___m_BindingMask_8; }
	inline void set_m_BindingMask_8(Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  value)
	{
		___m_BindingMask_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_BindingsStartIndex_9() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_BindingsStartIndex_9)); }
	inline int32_t get_m_BindingsStartIndex_9() const { return ___m_BindingsStartIndex_9; }
	inline int32_t* get_address_of_m_BindingsStartIndex_9() { return &___m_BindingsStartIndex_9; }
	inline void set_m_BindingsStartIndex_9(int32_t value)
	{
		___m_BindingsStartIndex_9 = value;
	}

	inline static int32_t get_offset_of_m_BindingsCount_10() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_BindingsCount_10)); }
	inline int32_t get_m_BindingsCount_10() const { return ___m_BindingsCount_10; }
	inline int32_t* get_address_of_m_BindingsCount_10() { return &___m_BindingsCount_10; }
	inline void set_m_BindingsCount_10(int32_t value)
	{
		___m_BindingsCount_10 = value;
	}

	inline static int32_t get_offset_of_m_ControlStartIndex_11() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_ControlStartIndex_11)); }
	inline int32_t get_m_ControlStartIndex_11() const { return ___m_ControlStartIndex_11; }
	inline int32_t* get_address_of_m_ControlStartIndex_11() { return &___m_ControlStartIndex_11; }
	inline void set_m_ControlStartIndex_11(int32_t value)
	{
		___m_ControlStartIndex_11 = value;
	}

	inline static int32_t get_offset_of_m_ControlCount_12() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_ControlCount_12)); }
	inline int32_t get_m_ControlCount_12() const { return ___m_ControlCount_12; }
	inline int32_t* get_address_of_m_ControlCount_12() { return &___m_ControlCount_12; }
	inline void set_m_ControlCount_12(int32_t value)
	{
		___m_ControlCount_12 = value;
	}

	inline static int32_t get_offset_of_m_ActionIndexInState_13() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_ActionIndexInState_13)); }
	inline int32_t get_m_ActionIndexInState_13() const { return ___m_ActionIndexInState_13; }
	inline int32_t* get_address_of_m_ActionIndexInState_13() { return &___m_ActionIndexInState_13; }
	inline void set_m_ActionIndexInState_13(int32_t value)
	{
		___m_ActionIndexInState_13 = value;
	}

	inline static int32_t get_offset_of_m_ActionMap_14() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_ActionMap_14)); }
	inline InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * get_m_ActionMap_14() const { return ___m_ActionMap_14; }
	inline InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 ** get_address_of_m_ActionMap_14() { return &___m_ActionMap_14; }
	inline void set_m_ActionMap_14(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * value)
	{
		___m_ActionMap_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionMap_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnStarted_15() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_OnStarted_15)); }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  get_m_OnStarted_15() const { return ___m_OnStarted_15; }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F * get_address_of_m_OnStarted_15() { return &___m_OnStarted_15; }
	inline void set_m_OnStarted_15(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  value)
	{
		___m_OnStarted_15 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnStarted_15))->___m_Callbacks_1))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnStarted_15))->___m_Callbacks_1))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnStarted_15))->___m_CallbacksToAdd_2))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnStarted_15))->___m_CallbacksToAdd_2))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnStarted_15))->___m_CallbacksToRemove_3))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnStarted_15))->___m_CallbacksToRemove_3))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_OnCanceled_16() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_OnCanceled_16)); }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  get_m_OnCanceled_16() const { return ___m_OnCanceled_16; }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F * get_address_of_m_OnCanceled_16() { return &___m_OnCanceled_16; }
	inline void set_m_OnCanceled_16(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  value)
	{
		___m_OnCanceled_16 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnCanceled_16))->___m_Callbacks_1))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnCanceled_16))->___m_Callbacks_1))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnCanceled_16))->___m_CallbacksToAdd_2))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnCanceled_16))->___m_CallbacksToAdd_2))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnCanceled_16))->___m_CallbacksToRemove_3))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnCanceled_16))->___m_CallbacksToRemove_3))->___additionalValues_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_OnPerformed_17() { return static_cast<int32_t>(offsetof(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B, ___m_OnPerformed_17)); }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  get_m_OnPerformed_17() const { return ___m_OnPerformed_17; }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F * get_address_of_m_OnPerformed_17() { return &___m_OnPerformed_17; }
	inline void set_m_OnPerformed_17(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  value)
	{
		___m_OnPerformed_17 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnPerformed_17))->___m_Callbacks_1))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnPerformed_17))->___m_Callbacks_1))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnPerformed_17))->___m_CallbacksToAdd_2))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnPerformed_17))->___m_CallbacksToAdd_2))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnPerformed_17))->___m_CallbacksToRemove_3))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_OnPerformed_17))->___m_CallbacksToRemove_3))->___additionalValues_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.InputSystem.InputActionAsset
struct InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// UnityEngine.InputSystem.InputActionMap[] UnityEngine.InputSystem.InputActionAsset::m_ActionMaps
	InputActionMapU5BU5D_t71F679B29A7609B04476AA165F6BC5205472C9C8* ___m_ActionMaps_5;
	// UnityEngine.InputSystem.InputControlScheme[] UnityEngine.InputSystem.InputActionAsset::m_ControlSchemes
	InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12* ___m_ControlSchemes_6;
	// UnityEngine.InputSystem.InputActionState UnityEngine.InputSystem.InputActionAsset::m_SharedStateForAllMaps
	InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * ___m_SharedStateForAllMaps_7;
	// System.Nullable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputActionAsset::m_BindingMask
	Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  ___m_BindingMask_8;
	// UnityEngine.InputSystem.InputActionMap/DeviceArray UnityEngine.InputSystem.InputActionAsset::m_Devices
	DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E  ___m_Devices_9;

public:
	inline static int32_t get_offset_of_m_ActionMaps_5() { return static_cast<int32_t>(offsetof(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB, ___m_ActionMaps_5)); }
	inline InputActionMapU5BU5D_t71F679B29A7609B04476AA165F6BC5205472C9C8* get_m_ActionMaps_5() const { return ___m_ActionMaps_5; }
	inline InputActionMapU5BU5D_t71F679B29A7609B04476AA165F6BC5205472C9C8** get_address_of_m_ActionMaps_5() { return &___m_ActionMaps_5; }
	inline void set_m_ActionMaps_5(InputActionMapU5BU5D_t71F679B29A7609B04476AA165F6BC5205472C9C8* value)
	{
		___m_ActionMaps_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ActionMaps_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ControlSchemes_6() { return static_cast<int32_t>(offsetof(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB, ___m_ControlSchemes_6)); }
	inline InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12* get_m_ControlSchemes_6() const { return ___m_ControlSchemes_6; }
	inline InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12** get_address_of_m_ControlSchemes_6() { return &___m_ControlSchemes_6; }
	inline void set_m_ControlSchemes_6(InputControlSchemeU5BU5D_tFBE961C14B97184FB4F101CDD2420D89F71E3B12* value)
	{
		___m_ControlSchemes_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ControlSchemes_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_SharedStateForAllMaps_7() { return static_cast<int32_t>(offsetof(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB, ___m_SharedStateForAllMaps_7)); }
	inline InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * get_m_SharedStateForAllMaps_7() const { return ___m_SharedStateForAllMaps_7; }
	inline InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 ** get_address_of_m_SharedStateForAllMaps_7() { return &___m_SharedStateForAllMaps_7; }
	inline void set_m_SharedStateForAllMaps_7(InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * value)
	{
		___m_SharedStateForAllMaps_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SharedStateForAllMaps_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingMask_8() { return static_cast<int32_t>(offsetof(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB, ___m_BindingMask_8)); }
	inline Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  get_m_BindingMask_8() const { return ___m_BindingMask_8; }
	inline Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597 * get_address_of_m_BindingMask_8() { return &___m_BindingMask_8; }
	inline void set_m_BindingMask_8(Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  value)
	{
		___m_BindingMask_8 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_8))->___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Devices_9() { return static_cast<int32_t>(offsetof(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB, ___m_Devices_9)); }
	inline DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E  get_m_Devices_9() const { return ___m_Devices_9; }
	inline DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E * get_address_of_m_Devices_9() { return &___m_Devices_9; }
	inline void set_m_Devices_9(DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E  value)
	{
		___m_Devices_9 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Devices_9))->___m_DeviceArray_2), (void*)NULL);
	}
};


// UnityEngine.InputSystem.InputActionMap
struct InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4  : public RuntimeObject
{
public:
	// System.String UnityEngine.InputSystem.InputActionMap::m_Name
	String_t* ___m_Name_0;
	// System.String UnityEngine.InputSystem.InputActionMap::m_Id
	String_t* ___m_Id_1;
	// UnityEngine.InputSystem.InputActionAsset UnityEngine.InputSystem.InputActionMap::m_Asset
	InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * ___m_Asset_2;
	// UnityEngine.InputSystem.InputAction[] UnityEngine.InputSystem.InputActionMap::m_Actions
	InputActionU5BU5D_t63AB2DEF6F9057B50B7AB65EA0C0030BE607A883* ___m_Actions_3;
	// UnityEngine.InputSystem.InputBinding[] UnityEngine.InputSystem.InputActionMap::m_Bindings
	InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* ___m_Bindings_4;
	// UnityEngine.InputSystem.InputBinding[] UnityEngine.InputSystem.InputActionMap::m_BindingsForEachAction
	InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* ___m_BindingsForEachAction_5;
	// UnityEngine.InputSystem.InputControl[] UnityEngine.InputSystem.InputActionMap::m_ControlsForEachAction
	InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* ___m_ControlsForEachAction_6;
	// System.Int32 UnityEngine.InputSystem.InputActionMap::m_EnabledActionsCount
	int32_t ___m_EnabledActionsCount_7;
	// UnityEngine.InputSystem.InputAction UnityEngine.InputSystem.InputActionMap::m_SingletonAction
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___m_SingletonAction_8;
	// System.Int32 UnityEngine.InputSystem.InputActionMap::m_MapIndexInState
	int32_t ___m_MapIndexInState_9;
	// UnityEngine.InputSystem.InputActionState UnityEngine.InputSystem.InputActionMap::m_State
	InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * ___m_State_10;
	// System.Boolean UnityEngine.InputSystem.InputActionMap::m_NeedToResolveBindings
	bool ___m_NeedToResolveBindings_11;
	// System.Nullable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputActionMap::m_BindingMask
	Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  ___m_BindingMask_12;
	// UnityEngine.InputSystem.InputActionMap/DeviceArray UnityEngine.InputSystem.InputActionMap::m_Devices
	DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E  ___m_Devices_13;
	// UnityEngine.InputSystem.Utilities.CallbackArray`1<System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>> UnityEngine.InputSystem.InputActionMap::m_ActionCallbacks
	CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  ___m_ActionCallbacks_14;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_Name_0)); }
	inline String_t* get_m_Name_0() const { return ___m_Name_0; }
	inline String_t** get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(String_t* value)
	{
		___m_Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Id_1() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_Id_1)); }
	inline String_t* get_m_Id_1() const { return ___m_Id_1; }
	inline String_t** get_address_of_m_Id_1() { return &___m_Id_1; }
	inline void set_m_Id_1(String_t* value)
	{
		___m_Id_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Id_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Asset_2() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_Asset_2)); }
	inline InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * get_m_Asset_2() const { return ___m_Asset_2; }
	inline InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB ** get_address_of_m_Asset_2() { return &___m_Asset_2; }
	inline void set_m_Asset_2(InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * value)
	{
		___m_Asset_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Asset_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Actions_3() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_Actions_3)); }
	inline InputActionU5BU5D_t63AB2DEF6F9057B50B7AB65EA0C0030BE607A883* get_m_Actions_3() const { return ___m_Actions_3; }
	inline InputActionU5BU5D_t63AB2DEF6F9057B50B7AB65EA0C0030BE607A883** get_address_of_m_Actions_3() { return &___m_Actions_3; }
	inline void set_m_Actions_3(InputActionU5BU5D_t63AB2DEF6F9057B50B7AB65EA0C0030BE607A883* value)
	{
		___m_Actions_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Actions_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Bindings_4() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_Bindings_4)); }
	inline InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* get_m_Bindings_4() const { return ___m_Bindings_4; }
	inline InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B** get_address_of_m_Bindings_4() { return &___m_Bindings_4; }
	inline void set_m_Bindings_4(InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* value)
	{
		___m_Bindings_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Bindings_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_BindingsForEachAction_5() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_BindingsForEachAction_5)); }
	inline InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* get_m_BindingsForEachAction_5() const { return ___m_BindingsForEachAction_5; }
	inline InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B** get_address_of_m_BindingsForEachAction_5() { return &___m_BindingsForEachAction_5; }
	inline void set_m_BindingsForEachAction_5(InputBindingU5BU5D_tD19508DA279576B1A85A197CC69413A48CE09D1B* value)
	{
		___m_BindingsForEachAction_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_BindingsForEachAction_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_ControlsForEachAction_6() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_ControlsForEachAction_6)); }
	inline InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* get_m_ControlsForEachAction_6() const { return ___m_ControlsForEachAction_6; }
	inline InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F** get_address_of_m_ControlsForEachAction_6() { return &___m_ControlsForEachAction_6; }
	inline void set_m_ControlsForEachAction_6(InputControlU5BU5D_t70A3E35F91F44D7B9476D69090DA050EE0B7418F* value)
	{
		___m_ControlsForEachAction_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ControlsForEachAction_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_EnabledActionsCount_7() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_EnabledActionsCount_7)); }
	inline int32_t get_m_EnabledActionsCount_7() const { return ___m_EnabledActionsCount_7; }
	inline int32_t* get_address_of_m_EnabledActionsCount_7() { return &___m_EnabledActionsCount_7; }
	inline void set_m_EnabledActionsCount_7(int32_t value)
	{
		___m_EnabledActionsCount_7 = value;
	}

	inline static int32_t get_offset_of_m_SingletonAction_8() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_SingletonAction_8)); }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * get_m_SingletonAction_8() const { return ___m_SingletonAction_8; }
	inline InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** get_address_of_m_SingletonAction_8() { return &___m_SingletonAction_8; }
	inline void set_m_SingletonAction_8(InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * value)
	{
		___m_SingletonAction_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SingletonAction_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_MapIndexInState_9() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_MapIndexInState_9)); }
	inline int32_t get_m_MapIndexInState_9() const { return ___m_MapIndexInState_9; }
	inline int32_t* get_address_of_m_MapIndexInState_9() { return &___m_MapIndexInState_9; }
	inline void set_m_MapIndexInState_9(int32_t value)
	{
		___m_MapIndexInState_9 = value;
	}

	inline static int32_t get_offset_of_m_State_10() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_State_10)); }
	inline InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * get_m_State_10() const { return ___m_State_10; }
	inline InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 ** get_address_of_m_State_10() { return &___m_State_10; }
	inline void set_m_State_10(InputActionState_t6592C6B535DF609BC27461C76CE2CEDC2E0745B1 * value)
	{
		___m_State_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_State_10), (void*)value);
	}

	inline static int32_t get_offset_of_m_NeedToResolveBindings_11() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_NeedToResolveBindings_11)); }
	inline bool get_m_NeedToResolveBindings_11() const { return ___m_NeedToResolveBindings_11; }
	inline bool* get_address_of_m_NeedToResolveBindings_11() { return &___m_NeedToResolveBindings_11; }
	inline void set_m_NeedToResolveBindings_11(bool value)
	{
		___m_NeedToResolveBindings_11 = value;
	}

	inline static int32_t get_offset_of_m_BindingMask_12() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_BindingMask_12)); }
	inline Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  get_m_BindingMask_12() const { return ___m_BindingMask_12; }
	inline Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597 * get_address_of_m_BindingMask_12() { return &___m_BindingMask_12; }
	inline void set_m_BindingMask_12(Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  value)
	{
		___m_BindingMask_12 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Name_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Id_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Path_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Interactions_5), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Processors_6), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Groups_7), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_Action_8), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_OverridePath_10), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_OverrideInteractions_11), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_BindingMask_12))->___value_0))->___m_OverrideProcessors_12), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Devices_13() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_Devices_13)); }
	inline DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E  get_m_Devices_13() const { return ___m_Devices_13; }
	inline DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E * get_address_of_m_Devices_13() { return &___m_Devices_13; }
	inline void set_m_Devices_13(DeviceArray_t819FEA31AAC78CD5296C369AF87BF9550C7D9E3E  value)
	{
		___m_Devices_13 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Devices_13))->___m_DeviceArray_2), (void*)NULL);
	}

	inline static int32_t get_offset_of_m_ActionCallbacks_14() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4, ___m_ActionCallbacks_14)); }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  get_m_ActionCallbacks_14() const { return ___m_ActionCallbacks_14; }
	inline CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F * get_address_of_m_ActionCallbacks_14() { return &___m_ActionCallbacks_14; }
	inline void set_m_ActionCallbacks_14(CallbackArray_1_t2E8C18AD8C738D858E5F97C865C86392E286343F  value)
	{
		___m_ActionCallbacks_14 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ActionCallbacks_14))->___m_Callbacks_1))->___firstValue_1), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ActionCallbacks_14))->___m_Callbacks_1))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ActionCallbacks_14))->___m_CallbacksToAdd_2))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ActionCallbacks_14))->___m_CallbacksToAdd_2))->___additionalValues_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ActionCallbacks_14))->___m_CallbacksToRemove_3))->___firstValue_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_ActionCallbacks_14))->___m_CallbacksToRemove_3))->___additionalValues_2), (void*)NULL);
		#endif
	}
};

struct InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4_StaticFields
{
public:
	// System.Int32 UnityEngine.InputSystem.InputActionMap::s_DeferBindingResolution
	int32_t ___s_DeferBindingResolution_15;

public:
	inline static int32_t get_offset_of_s_DeferBindingResolution_15() { return static_cast<int32_t>(offsetof(InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4_StaticFields, ___s_DeferBindingResolution_15)); }
	inline int32_t get_s_DeferBindingResolution_15() const { return ___s_DeferBindingResolution_15; }
	inline int32_t* get_address_of_s_DeferBindingResolution_15() { return &___s_DeferBindingResolution_15; }
	inline void set_s_DeferBindingResolution_15(int32_t value)
	{
		___s_DeferBindingResolution_15 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// MoreMountains.Tools.MMSingleton`1<MoreMountains.CorgiEngine.InputManager>
struct MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597_StaticFields
{
public:
	// T MoreMountains.Tools.MMSingleton`1::_instance
	InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * ____instance_4;

public:
	inline static int32_t get_offset_of__instance_4() { return static_cast<int32_t>(offsetof(MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597_StaticFields, ____instance_4)); }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * get__instance_4() const { return ____instance_4; }
	inline InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA ** get_address_of__instance_4() { return &____instance_4; }
	inline void set__instance_4(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * value)
	{
		____instance_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_4), (void*)value);
	}
};


// UnityEngine.InputSystem.Controls.AxisControl
struct AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22  : public InputControl_1_t2273064B5872656E668E007A1CAB0661D3FD40B8
{
public:
	// UnityEngine.InputSystem.Controls.AxisControl/Clamp UnityEngine.InputSystem.Controls.AxisControl::clamp
	int32_t ___clamp_22;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::clampMin
	float ___clampMin_23;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::clampMax
	float ___clampMax_24;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::clampConstant
	float ___clampConstant_25;
	// System.Boolean UnityEngine.InputSystem.Controls.AxisControl::invert
	bool ___invert_26;
	// System.Boolean UnityEngine.InputSystem.Controls.AxisControl::normalize
	bool ___normalize_27;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::normalizeMin
	float ___normalizeMin_28;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::normalizeMax
	float ___normalizeMax_29;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::normalizeZero
	float ___normalizeZero_30;
	// System.Boolean UnityEngine.InputSystem.Controls.AxisControl::scale
	bool ___scale_31;
	// System.Single UnityEngine.InputSystem.Controls.AxisControl::scaleFactor
	float ___scaleFactor_32;

public:
	inline static int32_t get_offset_of_clamp_22() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___clamp_22)); }
	inline int32_t get_clamp_22() const { return ___clamp_22; }
	inline int32_t* get_address_of_clamp_22() { return &___clamp_22; }
	inline void set_clamp_22(int32_t value)
	{
		___clamp_22 = value;
	}

	inline static int32_t get_offset_of_clampMin_23() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___clampMin_23)); }
	inline float get_clampMin_23() const { return ___clampMin_23; }
	inline float* get_address_of_clampMin_23() { return &___clampMin_23; }
	inline void set_clampMin_23(float value)
	{
		___clampMin_23 = value;
	}

	inline static int32_t get_offset_of_clampMax_24() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___clampMax_24)); }
	inline float get_clampMax_24() const { return ___clampMax_24; }
	inline float* get_address_of_clampMax_24() { return &___clampMax_24; }
	inline void set_clampMax_24(float value)
	{
		___clampMax_24 = value;
	}

	inline static int32_t get_offset_of_clampConstant_25() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___clampConstant_25)); }
	inline float get_clampConstant_25() const { return ___clampConstant_25; }
	inline float* get_address_of_clampConstant_25() { return &___clampConstant_25; }
	inline void set_clampConstant_25(float value)
	{
		___clampConstant_25 = value;
	}

	inline static int32_t get_offset_of_invert_26() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___invert_26)); }
	inline bool get_invert_26() const { return ___invert_26; }
	inline bool* get_address_of_invert_26() { return &___invert_26; }
	inline void set_invert_26(bool value)
	{
		___invert_26 = value;
	}

	inline static int32_t get_offset_of_normalize_27() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___normalize_27)); }
	inline bool get_normalize_27() const { return ___normalize_27; }
	inline bool* get_address_of_normalize_27() { return &___normalize_27; }
	inline void set_normalize_27(bool value)
	{
		___normalize_27 = value;
	}

	inline static int32_t get_offset_of_normalizeMin_28() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___normalizeMin_28)); }
	inline float get_normalizeMin_28() const { return ___normalizeMin_28; }
	inline float* get_address_of_normalizeMin_28() { return &___normalizeMin_28; }
	inline void set_normalizeMin_28(float value)
	{
		___normalizeMin_28 = value;
	}

	inline static int32_t get_offset_of_normalizeMax_29() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___normalizeMax_29)); }
	inline float get_normalizeMax_29() const { return ___normalizeMax_29; }
	inline float* get_address_of_normalizeMax_29() { return &___normalizeMax_29; }
	inline void set_normalizeMax_29(float value)
	{
		___normalizeMax_29 = value;
	}

	inline static int32_t get_offset_of_normalizeZero_30() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___normalizeZero_30)); }
	inline float get_normalizeZero_30() const { return ___normalizeZero_30; }
	inline float* get_address_of_normalizeZero_30() { return &___normalizeZero_30; }
	inline void set_normalizeZero_30(float value)
	{
		___normalizeZero_30 = value;
	}

	inline static int32_t get_offset_of_scale_31() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___scale_31)); }
	inline bool get_scale_31() const { return ___scale_31; }
	inline bool* get_address_of_scale_31() { return &___scale_31; }
	inline void set_scale_31(bool value)
	{
		___scale_31 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_32() { return static_cast<int32_t>(offsetof(AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22, ___scaleFactor_32)); }
	inline float get_scaleFactor_32() const { return ___scaleFactor_32; }
	inline float* get_address_of_scaleFactor_32() { return &___scaleFactor_32; }
	inline void set_scaleFactor_32(float value)
	{
		___scaleFactor_32 = value;
	}
};


// UnityEngine.InputSystem.Controls.ButtonControl
struct ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D  : public AxisControl_tF6BA6DA28DAB8BBBAD80A596F12BF340EE883B22
{
public:
	// System.Single UnityEngine.InputSystem.Controls.ButtonControl::pressPoint
	float ___pressPoint_33;

public:
	inline static int32_t get_offset_of_pressPoint_33() { return static_cast<int32_t>(offsetof(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D, ___pressPoint_33)); }
	inline float get_pressPoint_33() const { return ___pressPoint_33; }
	inline float* get_address_of_pressPoint_33() { return &___pressPoint_33; }
	inline void set_pressPoint_33(float value)
	{
		___pressPoint_33 = value;
	}
};

struct ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D_StaticFields
{
public:
	// System.Single UnityEngine.InputSystem.Controls.ButtonControl::s_GlobalDefaultButtonPressPoint
	float ___s_GlobalDefaultButtonPressPoint_34;
	// System.Single UnityEngine.InputSystem.Controls.ButtonControl::s_GlobalDefaultButtonReleaseThreshold
	float ___s_GlobalDefaultButtonReleaseThreshold_35;

public:
	inline static int32_t get_offset_of_s_GlobalDefaultButtonPressPoint_34() { return static_cast<int32_t>(offsetof(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D_StaticFields, ___s_GlobalDefaultButtonPressPoint_34)); }
	inline float get_s_GlobalDefaultButtonPressPoint_34() const { return ___s_GlobalDefaultButtonPressPoint_34; }
	inline float* get_address_of_s_GlobalDefaultButtonPressPoint_34() { return &___s_GlobalDefaultButtonPressPoint_34; }
	inline void set_s_GlobalDefaultButtonPressPoint_34(float value)
	{
		___s_GlobalDefaultButtonPressPoint_34 = value;
	}

	inline static int32_t get_offset_of_s_GlobalDefaultButtonReleaseThreshold_35() { return static_cast<int32_t>(offsetof(ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D_StaticFields, ___s_GlobalDefaultButtonReleaseThreshold_35)); }
	inline float get_s_GlobalDefaultButtonReleaseThreshold_35() const { return ___s_GlobalDefaultButtonReleaseThreshold_35; }
	inline float* get_address_of_s_GlobalDefaultButtonReleaseThreshold_35() { return &___s_GlobalDefaultButtonReleaseThreshold_35; }
	inline void set_s_GlobalDefaultButtonReleaseThreshold_35(float value)
	{
		___s_GlobalDefaultButtonReleaseThreshold_35 = value;
	}
};


// MoreMountains.CorgiEngine.InputManager
struct InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA  : public MMSingleton_1_tFAC95DD071831F7573C014F2AFA3A2FE78F86597
{
public:
	// System.Boolean MoreMountains.CorgiEngine.InputManager::InputDetectionActive
	bool ___InputDetectionActive_5;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::ResetButtonStatesOnFocusLoss
	bool ___ResetButtonStatesOnFocusLoss_6;
	// System.String MoreMountains.CorgiEngine.InputManager::PlayerID
	String_t* ___PlayerID_7;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::AutoMobileDetection
	bool ___AutoMobileDetection_8;
	// MoreMountains.CorgiEngine.InputManager/InputForcedMode MoreMountains.CorgiEngine.InputManager::ForcedMode
	int32_t ___ForcedMode_9;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::HideMobileControlsInEditor
	bool ___HideMobileControlsInEditor_10;
	// MoreMountains.CorgiEngine.InputManager/MovementControls MoreMountains.CorgiEngine.InputManager::MovementControl
	int32_t ___MovementControl_11;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::DelayedButtonPresses
	bool ___DelayedButtonPresses_12;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::<IsMobile>k__BackingField
	bool ___U3CIsMobileU3Ek__BackingField_13;
	// System.Boolean MoreMountains.CorgiEngine.InputManager::SmoothMovement
	bool ___SmoothMovement_14;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::Threshold
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___Threshold_15;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<JumpButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CJumpButtonU3Ek__BackingField_16;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SwimButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSwimButtonU3Ek__BackingField_17;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<GlideButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CGlideButtonU3Ek__BackingField_18;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<InteractButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CInteractButtonU3Ek__BackingField_19;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<JetpackButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CJetpackButtonU3Ek__BackingField_20;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<FlyButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CFlyButtonU3Ek__BackingField_21;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<RunButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CRunButtonU3Ek__BackingField_22;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<DashButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CDashButtonU3Ek__BackingField_23;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<RollButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CRollButtonU3Ek__BackingField_24;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<GrabButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CGrabButtonU3Ek__BackingField_25;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<ThrowButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CThrowButtonU3Ek__BackingField_26;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<ShootButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CShootButtonU3Ek__BackingField_27;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SecondaryShootButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSecondaryShootButtonU3Ek__BackingField_28;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<ReloadButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CReloadButtonU3Ek__BackingField_29;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<PushButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CPushButtonU3Ek__BackingField_30;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<GripButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CGripButtonU3Ek__BackingField_31;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<PauseButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CPauseButtonU3Ek__BackingField_32;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SwitchCharacterButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSwitchCharacterButtonU3Ek__BackingField_33;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<SwitchWeaponButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CSwitchWeaponButtonU3Ek__BackingField_34;
	// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::<TimeControlButton>k__BackingField
	IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___U3CTimeControlButtonU3Ek__BackingField_35;
	// MoreMountains.Tools.MMInput/ButtonStates MoreMountains.CorgiEngine.InputManager::<ShootAxis>k__BackingField
	int32_t ___U3CShootAxisU3Ek__BackingField_36;
	// MoreMountains.Tools.MMInput/ButtonStates MoreMountains.CorgiEngine.InputManager::<SecondaryShootAxis>k__BackingField
	int32_t ___U3CSecondaryShootAxisU3Ek__BackingField_37;
	// System.Collections.Generic.List`1<MoreMountains.Tools.MMInput/IMButton> MoreMountains.CorgiEngine.InputManager::ButtonList
	List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B * ___ButtonList_38;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::_primaryMovement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____primaryMovement_39;
	// UnityEngine.Vector2 MoreMountains.CorgiEngine.InputManager::_secondaryMovement
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ____secondaryMovement_40;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisHorizontal
	String_t* ____axisHorizontal_41;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisVertical
	String_t* ____axisVertical_42;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisSecondaryHorizontal
	String_t* ____axisSecondaryHorizontal_43;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisSecondaryVertical
	String_t* ____axisSecondaryVertical_44;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisShoot
	String_t* ____axisShoot_45;
	// System.String MoreMountains.CorgiEngine.InputManager::_axisShootSecondary
	String_t* ____axisShootSecondary_46;

public:
	inline static int32_t get_offset_of_InputDetectionActive_5() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___InputDetectionActive_5)); }
	inline bool get_InputDetectionActive_5() const { return ___InputDetectionActive_5; }
	inline bool* get_address_of_InputDetectionActive_5() { return &___InputDetectionActive_5; }
	inline void set_InputDetectionActive_5(bool value)
	{
		___InputDetectionActive_5 = value;
	}

	inline static int32_t get_offset_of_ResetButtonStatesOnFocusLoss_6() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___ResetButtonStatesOnFocusLoss_6)); }
	inline bool get_ResetButtonStatesOnFocusLoss_6() const { return ___ResetButtonStatesOnFocusLoss_6; }
	inline bool* get_address_of_ResetButtonStatesOnFocusLoss_6() { return &___ResetButtonStatesOnFocusLoss_6; }
	inline void set_ResetButtonStatesOnFocusLoss_6(bool value)
	{
		___ResetButtonStatesOnFocusLoss_6 = value;
	}

	inline static int32_t get_offset_of_PlayerID_7() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___PlayerID_7)); }
	inline String_t* get_PlayerID_7() const { return ___PlayerID_7; }
	inline String_t** get_address_of_PlayerID_7() { return &___PlayerID_7; }
	inline void set_PlayerID_7(String_t* value)
	{
		___PlayerID_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PlayerID_7), (void*)value);
	}

	inline static int32_t get_offset_of_AutoMobileDetection_8() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___AutoMobileDetection_8)); }
	inline bool get_AutoMobileDetection_8() const { return ___AutoMobileDetection_8; }
	inline bool* get_address_of_AutoMobileDetection_8() { return &___AutoMobileDetection_8; }
	inline void set_AutoMobileDetection_8(bool value)
	{
		___AutoMobileDetection_8 = value;
	}

	inline static int32_t get_offset_of_ForcedMode_9() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___ForcedMode_9)); }
	inline int32_t get_ForcedMode_9() const { return ___ForcedMode_9; }
	inline int32_t* get_address_of_ForcedMode_9() { return &___ForcedMode_9; }
	inline void set_ForcedMode_9(int32_t value)
	{
		___ForcedMode_9 = value;
	}

	inline static int32_t get_offset_of_HideMobileControlsInEditor_10() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___HideMobileControlsInEditor_10)); }
	inline bool get_HideMobileControlsInEditor_10() const { return ___HideMobileControlsInEditor_10; }
	inline bool* get_address_of_HideMobileControlsInEditor_10() { return &___HideMobileControlsInEditor_10; }
	inline void set_HideMobileControlsInEditor_10(bool value)
	{
		___HideMobileControlsInEditor_10 = value;
	}

	inline static int32_t get_offset_of_MovementControl_11() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___MovementControl_11)); }
	inline int32_t get_MovementControl_11() const { return ___MovementControl_11; }
	inline int32_t* get_address_of_MovementControl_11() { return &___MovementControl_11; }
	inline void set_MovementControl_11(int32_t value)
	{
		___MovementControl_11 = value;
	}

	inline static int32_t get_offset_of_DelayedButtonPresses_12() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___DelayedButtonPresses_12)); }
	inline bool get_DelayedButtonPresses_12() const { return ___DelayedButtonPresses_12; }
	inline bool* get_address_of_DelayedButtonPresses_12() { return &___DelayedButtonPresses_12; }
	inline void set_DelayedButtonPresses_12(bool value)
	{
		___DelayedButtonPresses_12 = value;
	}

	inline static int32_t get_offset_of_U3CIsMobileU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CIsMobileU3Ek__BackingField_13)); }
	inline bool get_U3CIsMobileU3Ek__BackingField_13() const { return ___U3CIsMobileU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CIsMobileU3Ek__BackingField_13() { return &___U3CIsMobileU3Ek__BackingField_13; }
	inline void set_U3CIsMobileU3Ek__BackingField_13(bool value)
	{
		___U3CIsMobileU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_SmoothMovement_14() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___SmoothMovement_14)); }
	inline bool get_SmoothMovement_14() const { return ___SmoothMovement_14; }
	inline bool* get_address_of_SmoothMovement_14() { return &___SmoothMovement_14; }
	inline void set_SmoothMovement_14(bool value)
	{
		___SmoothMovement_14 = value;
	}

	inline static int32_t get_offset_of_Threshold_15() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___Threshold_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_Threshold_15() const { return ___Threshold_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_Threshold_15() { return &___Threshold_15; }
	inline void set_Threshold_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___Threshold_15 = value;
	}

	inline static int32_t get_offset_of_U3CJumpButtonU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CJumpButtonU3Ek__BackingField_16)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CJumpButtonU3Ek__BackingField_16() const { return ___U3CJumpButtonU3Ek__BackingField_16; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CJumpButtonU3Ek__BackingField_16() { return &___U3CJumpButtonU3Ek__BackingField_16; }
	inline void set_U3CJumpButtonU3Ek__BackingField_16(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CJumpButtonU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CJumpButtonU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwimButtonU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSwimButtonU3Ek__BackingField_17)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSwimButtonU3Ek__BackingField_17() const { return ___U3CSwimButtonU3Ek__BackingField_17; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSwimButtonU3Ek__BackingField_17() { return &___U3CSwimButtonU3Ek__BackingField_17; }
	inline void set_U3CSwimButtonU3Ek__BackingField_17(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSwimButtonU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwimButtonU3Ek__BackingField_17), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGlideButtonU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CGlideButtonU3Ek__BackingField_18)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CGlideButtonU3Ek__BackingField_18() const { return ___U3CGlideButtonU3Ek__BackingField_18; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CGlideButtonU3Ek__BackingField_18() { return &___U3CGlideButtonU3Ek__BackingField_18; }
	inline void set_U3CGlideButtonU3Ek__BackingField_18(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CGlideButtonU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGlideButtonU3Ek__BackingField_18), (void*)value);
	}

	inline static int32_t get_offset_of_U3CInteractButtonU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CInteractButtonU3Ek__BackingField_19)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CInteractButtonU3Ek__BackingField_19() const { return ___U3CInteractButtonU3Ek__BackingField_19; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CInteractButtonU3Ek__BackingField_19() { return &___U3CInteractButtonU3Ek__BackingField_19; }
	inline void set_U3CInteractButtonU3Ek__BackingField_19(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CInteractButtonU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInteractButtonU3Ek__BackingField_19), (void*)value);
	}

	inline static int32_t get_offset_of_U3CJetpackButtonU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CJetpackButtonU3Ek__BackingField_20)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CJetpackButtonU3Ek__BackingField_20() const { return ___U3CJetpackButtonU3Ek__BackingField_20; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CJetpackButtonU3Ek__BackingField_20() { return &___U3CJetpackButtonU3Ek__BackingField_20; }
	inline void set_U3CJetpackButtonU3Ek__BackingField_20(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CJetpackButtonU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CJetpackButtonU3Ek__BackingField_20), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFlyButtonU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CFlyButtonU3Ek__BackingField_21)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CFlyButtonU3Ek__BackingField_21() const { return ___U3CFlyButtonU3Ek__BackingField_21; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CFlyButtonU3Ek__BackingField_21() { return &___U3CFlyButtonU3Ek__BackingField_21; }
	inline void set_U3CFlyButtonU3Ek__BackingField_21(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CFlyButtonU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFlyButtonU3Ek__BackingField_21), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRunButtonU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CRunButtonU3Ek__BackingField_22)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CRunButtonU3Ek__BackingField_22() const { return ___U3CRunButtonU3Ek__BackingField_22; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CRunButtonU3Ek__BackingField_22() { return &___U3CRunButtonU3Ek__BackingField_22; }
	inline void set_U3CRunButtonU3Ek__BackingField_22(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CRunButtonU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRunButtonU3Ek__BackingField_22), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDashButtonU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CDashButtonU3Ek__BackingField_23)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CDashButtonU3Ek__BackingField_23() const { return ___U3CDashButtonU3Ek__BackingField_23; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CDashButtonU3Ek__BackingField_23() { return &___U3CDashButtonU3Ek__BackingField_23; }
	inline void set_U3CDashButtonU3Ek__BackingField_23(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CDashButtonU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDashButtonU3Ek__BackingField_23), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRollButtonU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CRollButtonU3Ek__BackingField_24)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CRollButtonU3Ek__BackingField_24() const { return ___U3CRollButtonU3Ek__BackingField_24; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CRollButtonU3Ek__BackingField_24() { return &___U3CRollButtonU3Ek__BackingField_24; }
	inline void set_U3CRollButtonU3Ek__BackingField_24(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CRollButtonU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CRollButtonU3Ek__BackingField_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGrabButtonU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CGrabButtonU3Ek__BackingField_25)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CGrabButtonU3Ek__BackingField_25() const { return ___U3CGrabButtonU3Ek__BackingField_25; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CGrabButtonU3Ek__BackingField_25() { return &___U3CGrabButtonU3Ek__BackingField_25; }
	inline void set_U3CGrabButtonU3Ek__BackingField_25(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CGrabButtonU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGrabButtonU3Ek__BackingField_25), (void*)value);
	}

	inline static int32_t get_offset_of_U3CThrowButtonU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CThrowButtonU3Ek__BackingField_26)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CThrowButtonU3Ek__BackingField_26() const { return ___U3CThrowButtonU3Ek__BackingField_26; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CThrowButtonU3Ek__BackingField_26() { return &___U3CThrowButtonU3Ek__BackingField_26; }
	inline void set_U3CThrowButtonU3Ek__BackingField_26(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CThrowButtonU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CThrowButtonU3Ek__BackingField_26), (void*)value);
	}

	inline static int32_t get_offset_of_U3CShootButtonU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CShootButtonU3Ek__BackingField_27)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CShootButtonU3Ek__BackingField_27() const { return ___U3CShootButtonU3Ek__BackingField_27; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CShootButtonU3Ek__BackingField_27() { return &___U3CShootButtonU3Ek__BackingField_27; }
	inline void set_U3CShootButtonU3Ek__BackingField_27(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CShootButtonU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CShootButtonU3Ek__BackingField_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSecondaryShootButtonU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSecondaryShootButtonU3Ek__BackingField_28)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSecondaryShootButtonU3Ek__BackingField_28() const { return ___U3CSecondaryShootButtonU3Ek__BackingField_28; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSecondaryShootButtonU3Ek__BackingField_28() { return &___U3CSecondaryShootButtonU3Ek__BackingField_28; }
	inline void set_U3CSecondaryShootButtonU3Ek__BackingField_28(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSecondaryShootButtonU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSecondaryShootButtonU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of_U3CReloadButtonU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CReloadButtonU3Ek__BackingField_29)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CReloadButtonU3Ek__BackingField_29() const { return ___U3CReloadButtonU3Ek__BackingField_29; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CReloadButtonU3Ek__BackingField_29() { return &___U3CReloadButtonU3Ek__BackingField_29; }
	inline void set_U3CReloadButtonU3Ek__BackingField_29(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CReloadButtonU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CReloadButtonU3Ek__BackingField_29), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPushButtonU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CPushButtonU3Ek__BackingField_30)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CPushButtonU3Ek__BackingField_30() const { return ___U3CPushButtonU3Ek__BackingField_30; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CPushButtonU3Ek__BackingField_30() { return &___U3CPushButtonU3Ek__BackingField_30; }
	inline void set_U3CPushButtonU3Ek__BackingField_30(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CPushButtonU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPushButtonU3Ek__BackingField_30), (void*)value);
	}

	inline static int32_t get_offset_of_U3CGripButtonU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CGripButtonU3Ek__BackingField_31)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CGripButtonU3Ek__BackingField_31() const { return ___U3CGripButtonU3Ek__BackingField_31; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CGripButtonU3Ek__BackingField_31() { return &___U3CGripButtonU3Ek__BackingField_31; }
	inline void set_U3CGripButtonU3Ek__BackingField_31(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CGripButtonU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CGripButtonU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPauseButtonU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CPauseButtonU3Ek__BackingField_32)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CPauseButtonU3Ek__BackingField_32() const { return ___U3CPauseButtonU3Ek__BackingField_32; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CPauseButtonU3Ek__BackingField_32() { return &___U3CPauseButtonU3Ek__BackingField_32; }
	inline void set_U3CPauseButtonU3Ek__BackingField_32(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CPauseButtonU3Ek__BackingField_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPauseButtonU3Ek__BackingField_32), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwitchCharacterButtonU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSwitchCharacterButtonU3Ek__BackingField_33)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSwitchCharacterButtonU3Ek__BackingField_33() const { return ___U3CSwitchCharacterButtonU3Ek__BackingField_33; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSwitchCharacterButtonU3Ek__BackingField_33() { return &___U3CSwitchCharacterButtonU3Ek__BackingField_33; }
	inline void set_U3CSwitchCharacterButtonU3Ek__BackingField_33(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSwitchCharacterButtonU3Ek__BackingField_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwitchCharacterButtonU3Ek__BackingField_33), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwitchWeaponButtonU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSwitchWeaponButtonU3Ek__BackingField_34)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CSwitchWeaponButtonU3Ek__BackingField_34() const { return ___U3CSwitchWeaponButtonU3Ek__BackingField_34; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CSwitchWeaponButtonU3Ek__BackingField_34() { return &___U3CSwitchWeaponButtonU3Ek__BackingField_34; }
	inline void set_U3CSwitchWeaponButtonU3Ek__BackingField_34(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CSwitchWeaponButtonU3Ek__BackingField_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwitchWeaponButtonU3Ek__BackingField_34), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTimeControlButtonU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CTimeControlButtonU3Ek__BackingField_35)); }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * get_U3CTimeControlButtonU3Ek__BackingField_35() const { return ___U3CTimeControlButtonU3Ek__BackingField_35; }
	inline IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B ** get_address_of_U3CTimeControlButtonU3Ek__BackingField_35() { return &___U3CTimeControlButtonU3Ek__BackingField_35; }
	inline void set_U3CTimeControlButtonU3Ek__BackingField_35(IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * value)
	{
		___U3CTimeControlButtonU3Ek__BackingField_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTimeControlButtonU3Ek__BackingField_35), (void*)value);
	}

	inline static int32_t get_offset_of_U3CShootAxisU3Ek__BackingField_36() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CShootAxisU3Ek__BackingField_36)); }
	inline int32_t get_U3CShootAxisU3Ek__BackingField_36() const { return ___U3CShootAxisU3Ek__BackingField_36; }
	inline int32_t* get_address_of_U3CShootAxisU3Ek__BackingField_36() { return &___U3CShootAxisU3Ek__BackingField_36; }
	inline void set_U3CShootAxisU3Ek__BackingField_36(int32_t value)
	{
		___U3CShootAxisU3Ek__BackingField_36 = value;
	}

	inline static int32_t get_offset_of_U3CSecondaryShootAxisU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___U3CSecondaryShootAxisU3Ek__BackingField_37)); }
	inline int32_t get_U3CSecondaryShootAxisU3Ek__BackingField_37() const { return ___U3CSecondaryShootAxisU3Ek__BackingField_37; }
	inline int32_t* get_address_of_U3CSecondaryShootAxisU3Ek__BackingField_37() { return &___U3CSecondaryShootAxisU3Ek__BackingField_37; }
	inline void set_U3CSecondaryShootAxisU3Ek__BackingField_37(int32_t value)
	{
		___U3CSecondaryShootAxisU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_ButtonList_38() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ___ButtonList_38)); }
	inline List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B * get_ButtonList_38() const { return ___ButtonList_38; }
	inline List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B ** get_address_of_ButtonList_38() { return &___ButtonList_38; }
	inline void set_ButtonList_38(List_1_t803AFFEA36C4B1CB1E58D4A2B3BEFBC3D27AFB2B * value)
	{
		___ButtonList_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonList_38), (void*)value);
	}

	inline static int32_t get_offset_of__primaryMovement_39() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____primaryMovement_39)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__primaryMovement_39() const { return ____primaryMovement_39; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__primaryMovement_39() { return &____primaryMovement_39; }
	inline void set__primaryMovement_39(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____primaryMovement_39 = value;
	}

	inline static int32_t get_offset_of__secondaryMovement_40() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____secondaryMovement_40)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get__secondaryMovement_40() const { return ____secondaryMovement_40; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of__secondaryMovement_40() { return &____secondaryMovement_40; }
	inline void set__secondaryMovement_40(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		____secondaryMovement_40 = value;
	}

	inline static int32_t get_offset_of__axisHorizontal_41() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisHorizontal_41)); }
	inline String_t* get__axisHorizontal_41() const { return ____axisHorizontal_41; }
	inline String_t** get_address_of__axisHorizontal_41() { return &____axisHorizontal_41; }
	inline void set__axisHorizontal_41(String_t* value)
	{
		____axisHorizontal_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisHorizontal_41), (void*)value);
	}

	inline static int32_t get_offset_of__axisVertical_42() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisVertical_42)); }
	inline String_t* get__axisVertical_42() const { return ____axisVertical_42; }
	inline String_t** get_address_of__axisVertical_42() { return &____axisVertical_42; }
	inline void set__axisVertical_42(String_t* value)
	{
		____axisVertical_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisVertical_42), (void*)value);
	}

	inline static int32_t get_offset_of__axisSecondaryHorizontal_43() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisSecondaryHorizontal_43)); }
	inline String_t* get__axisSecondaryHorizontal_43() const { return ____axisSecondaryHorizontal_43; }
	inline String_t** get_address_of__axisSecondaryHorizontal_43() { return &____axisSecondaryHorizontal_43; }
	inline void set__axisSecondaryHorizontal_43(String_t* value)
	{
		____axisSecondaryHorizontal_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisSecondaryHorizontal_43), (void*)value);
	}

	inline static int32_t get_offset_of__axisSecondaryVertical_44() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisSecondaryVertical_44)); }
	inline String_t* get__axisSecondaryVertical_44() const { return ____axisSecondaryVertical_44; }
	inline String_t** get_address_of__axisSecondaryVertical_44() { return &____axisSecondaryVertical_44; }
	inline void set__axisSecondaryVertical_44(String_t* value)
	{
		____axisSecondaryVertical_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisSecondaryVertical_44), (void*)value);
	}

	inline static int32_t get_offset_of__axisShoot_45() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisShoot_45)); }
	inline String_t* get__axisShoot_45() const { return ____axisShoot_45; }
	inline String_t** get_address_of__axisShoot_45() { return &____axisShoot_45; }
	inline void set__axisShoot_45(String_t* value)
	{
		____axisShoot_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisShoot_45), (void*)value);
	}

	inline static int32_t get_offset_of__axisShootSecondary_46() { return static_cast<int32_t>(offsetof(InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA, ____axisShootSecondary_46)); }
	inline String_t* get__axisShootSecondary_46() const { return ____axisShootSecondary_46; }
	inline String_t** get_address_of__axisShootSecondary_46() { return &____axisShootSecondary_46; }
	inline void set__axisShootSecondary_46(String_t* value)
	{
		____axisShootSecondary_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____axisShootSecondary_46), (void*)value);
	}
};


// MoreMountains.CorgiEngine.InputSystemManager
struct InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80  : public InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA
{
public:
	// MoreMountains.CorgiEngine.CorgiEngineInputActions MoreMountains.CorgiEngine.InputSystemManager::InputActions
	CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___InputActions_47;
	// System.Boolean MoreMountains.CorgiEngine.InputSystemManager::_inputActionsEnabled
	bool ____inputActionsEnabled_48;
	// System.Boolean MoreMountains.CorgiEngine.InputSystemManager::_initialized
	bool ____initialized_49;

public:
	inline static int32_t get_offset_of_InputActions_47() { return static_cast<int32_t>(offsetof(InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80, ___InputActions_47)); }
	inline CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * get_InputActions_47() const { return ___InputActions_47; }
	inline CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 ** get_address_of_InputActions_47() { return &___InputActions_47; }
	inline void set_InputActions_47(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * value)
	{
		___InputActions_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___InputActions_47), (void*)value);
	}

	inline static int32_t get_offset_of__inputActionsEnabled_48() { return static_cast<int32_t>(offsetof(InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80, ____inputActionsEnabled_48)); }
	inline bool get__inputActionsEnabled_48() const { return ____inputActionsEnabled_48; }
	inline bool* get_address_of__inputActionsEnabled_48() { return &____inputActionsEnabled_48; }
	inline void set__inputActionsEnabled_48(bool value)
	{
		____inputActionsEnabled_48 = value;
	}

	inline static int32_t get_offset_of__initialized_49() { return static_cast<int32_t>(offsetof(InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80, ____initialized_49)); }
	inline bool get__initialized_49() const { return ____initialized_49; }
	inline bool* get_address_of__initialized_49() { return &____initialized_49; }
	inline void set__initialized_49(bool value)
	{
		____initialized_49 = value;
	}
};


// MoreMountains.CorgiEngine.InputSystemManagerEventsBased
struct InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E  : public InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif


// !0 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_gshared (ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_gshared (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_gshared (CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputValue::Get<UnityEngine.Vector2>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_gshared (InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * __this, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMStateMachine`1<System.Int32Enum>::get_CurrentState()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t MMStateMachine_1_get_CurrentState_mF8651D0B592EB2D243E9374DADBFD68A0D0C3C69_gshared_inline (MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA * __this, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionAsset UnityEngine.InputSystem.InputActionAsset::FromJson(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * InputActionAsset_FromJson_m43B2CC4FC1A031878F4E426233FA175ECAB24EB0 (String_t* ___json0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionAsset MoreMountains.CorgiEngine.CorgiEngineInputActions::get_asset()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputActionMap UnityEngine.InputSystem.InputActionAsset::FindActionMap(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * InputActionAsset_FindActionMap_m59C07E25367472F0084C661456553E21B306103C (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, String_t* ___nameOrId0, bool ___throwIfNotFound1, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction UnityEngine.InputSystem.InputActionMap::FindAction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C (InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * __this, String_t* ___actionNameOrId0, bool ___throwIfNotFound1, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Nullable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputActionAsset::get_bindingMask()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  InputActionAsset_get_bindingMask_m484893D9818539CC0F5EC637544941C4896B8F73_inline (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputActionAsset::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputActionAsset_set_bindingMask_mF5DEF2BD9AE6CBE2C9497248F61389747D013D22 (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  ___value0, const RuntimeMethod* method);
// System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> UnityEngine.InputSystem.InputActionAsset::get_devices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_t989E006C949E739D03D77F64841F9E3503807634  InputActionAsset_get_devices_mE0ABE391C146F682CDC30FD6313F7960FBC9F66A (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputActionAsset::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputActionAsset_set_devices_m44A67138E55F9F6CDAEDC9777810980EC66D741D (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, Nullable_1_t989E006C949E739D03D77F64841F9E3503807634  ___value0, const RuntimeMethod* method);
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> UnityEngine.InputSystem.InputActionAsset::get_controlSchemes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  InputActionAsset_get_controlSchemes_mC517DF89ED65963040002AB585D20C8E864F05F2 (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.InputSystem.InputActionAsset::Contains(UnityEngine.InputSystem.InputAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputActionAsset_Contains_mCBA5F48B7326E69CDA258C83D9BEB4FC62C99DEE (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___action0, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> UnityEngine.InputSystem.InputActionAsset::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InputActionAsset_GetEnumerator_m02AAE5BE032E6C5D59AD8EB5EB9390F3F46D5900 (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> MoreMountains.CorgiEngine.CorgiEngineInputActions::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiEngineInputActions_GetEnumerator_mA2D997DEABD39D867A940F22ABF29AE0D2589859 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputActionAsset::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputActionAsset_Enable_m7AF0060D1FCFA6DDF7C8AC34BC4FBE6566A47080 (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputActionAsset::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputActionAsset_Disable_m0E95B47AAE45C952F8FD1A9669EF60A5A18F316A (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.InputBinding> UnityEngine.InputSystem.InputActionAsset::get_bindings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* InputActionAsset_get_bindings_mFF83EDE59E1250EACDDD05F02892DB2AD64EF7CD (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction UnityEngine.InputSystem.InputActionAsset::FindAction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * InputActionAsset_FindAction_m301109AC75A819E577658319115687D7187FA8B4 (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, String_t* ___actionNameOrId0, bool ___throwIfNotFound1, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.InputActionAsset::FindBinding(UnityEngine.InputSystem.InputBinding,UnityEngine.InputSystem.InputAction&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InputActionAsset_FindBinding_m98B6FBE00F0C87F790B1EC17D0FE49BA2F20C54D (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB  ___mask0, InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** ___action1, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::.ctor(MoreMountains.CorgiEngine.CorgiEngineInputActions)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerControlsActions__ctor_mFBF92D7C2688CBF0FA55CD8903AEF59444DED9F2_inline (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___wrapper0, const RuntimeMethod* method);
// System.Int32 UnityEngine.InputSystem.InputActionAsset::FindControlSchemeIndex(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t InputActionAsset_FindControlSchemeIndex_mF36ECE7B40E919EE1E860CEAED159949636A02FB (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, String_t* ___name0, const RuntimeMethod* method);
// !0 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme>::get_Item(System.Int32)
inline InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B (ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  (*) (ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16 *, int32_t, const RuntimeMethod*))ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_gshared)(__this, ___index0, method);
}
// System.Void MoreMountains.CorgiEngine.InputManager::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_Initialization_m1D51ADD53BEDAB595CB0819EB2EA164D0BD61C71 (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions__ctor_m70F22674BA17D5AE8E998BC611A2EC2AB2780FE6 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method);
// MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions MoreMountains.CorgiEngine.CorgiEngineInputActions::get_PlayerControls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_PrimaryMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// System.Void System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.InputSystem.InputAction::add_performed(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * __this, Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___value0, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SecondaryMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Jump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Dash()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Shoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SecondaryShoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Interact()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Reload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SwitchWeapon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SwitchCharacter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_TimeControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Swim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Glide()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Jetpack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Fly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Grab()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Throw()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Push()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// UnityEngine.InputSystem.InputControl UnityEngine.InputSystem.InputAction/CallbackContext::get_control()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * CallbackContext_get_control_mEA6B6C22E347692F93A2422E7A9DCFCEAB310AB9 (CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.InputSystem.Controls.ButtonControl::get_wasPressedThisFrame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ButtonControl_get_wasPressedThisFrame_m23C7AF7B7B30B8751A72B2E9A73E089D5E69AE63 (ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates> MoreMountains.Tools.MMInput/IMButton::get_State()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline (IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.InputSystem.Controls.ButtonControl::get_wasReleasedThisFrame()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool ButtonControl_get_wasReleasedThisFrame_m692D3C2E8F4A01255580EF4192C0587CC6BDFD85 (ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * __this, const RuntimeMethod* method);
// System.Boolean MoreMountains.CorgiEngine.InputManager::get_IsMobile()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool InputManager_get_IsMobile_mD06485D9FB0011F9BB9F2F93716FD28CE192EC17_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_Disable_m046CFFA3CD3A7BE68443A3E9AF7A3431BFADAD3A (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_Enable_m760257AE247847240EC655FFDF50486ED9B72CC2 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.InputManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager__ctor_m8C9F54C63E83C19DEAB926944E5CCC11FCD2EF78 (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputAction/CallbackContext::ReadValue<UnityEngine.Vector2>()
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6 (CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 *, const RuntimeMethod*))CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_gshared)(__this, method);
}
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_JumpButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_RunButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_RunButton_m1BF0C918767106B3814D73F3550245C440035A66_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_DashButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_DashButton_m039788AFC1F68B651EF3179B1FF1739E0515225C_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_ShootButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_ShootButton_m67631ED77F704CF4C3B470F57B7649B399484B03_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SecondaryShootButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SecondaryShootButton_m089E50BE434BEEFD14863D19A45E8FD81099CFAA_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_InteractButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_InteractButton_mE19955DDD59C98044E1D1A691ED12CB20CED0E22_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_ReloadButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_ReloadButton_m42E2F7163F6D6D0A54BFB82AB63F5A5E9DC44E36_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_PauseButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_PauseButton_mDA714172CF94F6397EC18BBD8A9CB1535BBDC1A5_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SwitchWeaponButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SwitchWeaponButton_m0290A37CB978A5F5CA6996B3E4B2D5CA961002F2_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SwitchCharacterButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SwitchCharacterButton_mD19BFC6172D61F4E182C03EB13D961C86CB58A70_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_TimeControlButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_TimeControlButton_m06F1083AE3A4A9BE95C3473B859C2FE9FB208461_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_SwimButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SwimButton_m5519300F8D4DE3030F0DFA46C7B2E16D30BDBD29_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_GlideButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_GlideButton_mE83F8C46DBFFCDAAE2DF29B3E6AA7662CD072CF1_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_JetpackButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_JetpackButton_mDD8A0EEEC4592EE09E2612A6B38097050BA4D5E1_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_FlyButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_FlyButton_m8FE2143D47033FF22CF4537A6EEB6373A79AB0AE_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_GrabButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_GrabButton_m35B3226127EA6466B0DC4DFF7DC4BF7989242BD4_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_ThrowButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_ThrowButton_m36E076F34EE4A98E8A26E301D3D8914BB52C66EA_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// MoreMountains.Tools.MMInput/IMButton MoreMountains.CorgiEngine.InputManager::get_PushButton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_PushButton_mC45D8348FAB555B5AD423A4411B6629B42F3E8F7_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method);
// !!0 UnityEngine.InputSystem.InputValue::Get<UnityEngine.Vector2>()
inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328 (InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * __this, const RuntimeMethod* method)
{
	return ((  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  (*) (InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, const RuntimeMethod*))InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_gshared)(__this, method);
}
// System.Boolean UnityEngine.InputSystem.InputValue::get_isPressed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputValue_get_isPressed_mCD089D4B9994EA3CD1790F0CBB17447C39D2B31C (InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * __this, const RuntimeMethod* method);
// !0 MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::get_CurrentState()
inline int32_t MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_inline (MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE *, const RuntimeMethod*))MMStateMachine_1_get_CurrentState_mF8651D0B592EB2D243E9374DADBFD68A0D0C3C69_gshared_inline)(__this, method);
}
// UnityEngine.InputSystem.InputActionMap MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputActionMap::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputActionMap_Enable_mFC18A12540F92B660F3ECFA9EA87320EC3F937CA (InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions_Enable_mABD445BB121D241F5A8E7A6D76E541D5CE895788 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputActionMap::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputActionMap_Disable_m71364975C45CB3F319459C8597E1D69C9B8135BB (InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * __this, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions_Disable_mA3082B39EC508B89826A2ED364B5EAEED3301FE2 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.InputSystem.InputActionMap::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool InputActionMap_get_enabled_mAED55C9A85269C56CD451BCB0B28B6AE3DA8C736 (InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * __this, const RuntimeMethod* method);
// System.Boolean MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerControlsActions_get_enabled_m522FAB68D12100A9775C28553B0840BA9A5AC6DE (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::remove_started(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * __this, Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::remove_performed(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277 (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * __this, Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::remove_canceled(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3 (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * __this, Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::add_started(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66 (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * __this, Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.InputSystem.InputAction::add_canceled(System.Action`1<UnityEngine.InputSystem.InputAction/CallbackContext>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4 (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * __this, Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * ___value0, const RuntimeMethod* method);
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::SetCallbacks(MoreMountains.CorgiEngine.CorgiEngineInputActions/IPlayerControlsActions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions_SetCallbacks_m3A8659B2D15000858F1A09EF342D0566BA15BD66 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, RuntimeObject* ___instance0, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.InputSystem.InputActionAsset MoreMountains.CorgiEngine.CorgiEngineInputActions::get_asset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// public InputActionAsset asset { get; }
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0 = __this->get_U3CassetU3Ek__BackingField_0();
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions__ctor_m70F22674BA17D5AE8E998BC611A2EC2AB2780FE6 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral052C17E9D6BC1FE3DB31F3277AED6D12A2C6BDAC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0A3EF69D14EE38BA43AC0515A8C06B877F0D161F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2A474FBC7EC4EA2C1A762D0FB32562808DDDA351);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral515E152AE9864F9AEE9ADB9C0487E55BF594D5DF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral539886CC0D6E626CD59CCCB9A251E15C3F8DC3EE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral63D5B7B61C9A932F0CFFA0506AD506F2DC0E23A8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B514E260236F16463F3E75F706DCC5449640861);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral78DB8AA40D3B780BCB7B7076042CF600254368BC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7FAE997056826D296DB6568629E5D4A1BB440293);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8866EA3E0BC985BCCB33306B5606BA3D21875437);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8978DD6DA21EA64066F90784F7852458B1D54CA8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E48D88C3471247D01F96214CD4DB51A245D5A05);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAAAA401E86E41E6120BB9E96B9892141CF5A81F8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0EF07816378D87F80141959937F780914E0D8D3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC312F88FB3C35D22BE7CDEB894CC152447D26A16);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC4C8C3123441B0B03C3A7F20F50CBF14D5E64B63);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE17E01A6CDB454BE09B74C544A2901D6C9F990AF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE4CD985488306FB575FF327ABBAD1D041A7BB68C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF60A604A8904268B2FCB7A84B9C54E433B6C1C27);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF7B071257B0A0FEB1A12B0FD1786CBD4D6D1ADC9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFA39B819B390DDA4BFA806E31149DC99CE38040A);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private int m_KeyboardSchemeIndex = -1;
		__this->set_m_KeyboardSchemeIndex_23((-1));
		// private int m_GamepadSchemeIndex = -1;
		__this->set_m_GamepadSchemeIndex_24((-1));
		// public @CorgiEngineInputActions()
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		//             asset = InputActionAsset.FromJson(@"{
		//     ""name"": ""CorgiEngineInputActions"",
		//     ""maps"": [
		//         {
		//             ""name"": ""PlayerControls"",
		//             ""id"": ""aa0e1094-3ae9-4de6-877e-1fa7a26edd69"",
		//             ""actions"": [
		//                 {
		//                     ""name"": ""PrimaryMovement"",
		//                     ""type"": ""PassThrough"",
		//                     ""id"": ""265ae792-b274-4758-9c3f-67c2f395c758"",
		//                     ""expectedControlType"": ""Vector2"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""SecondaryMovement"",
		//                     ""type"": ""PassThrough"",
		//                     ""id"": ""e4146e01-7578-46fe-b394-b240c2256b2c"",
		//                     ""expectedControlType"": ""Vector2"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Jump"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""314ace32-fb21-4bab-a569-ebe4e48f2102"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Swim"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""bb397b32-8d84-4aad-9fe6-92d359b0a49b"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Glide"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""44f9f9bc-9a87-4ef7-9677-7acb02f39e5d"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Jetpack"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""0daa39dc-3d97-4c43-98ea-dfa4a4a5104e"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Fly"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""639199aa-bfdd-452d-9f7d-9476e5d54951"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Throw"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""9868b1c7-76ad-40fd-a811-5e8dcbacf8d6"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Push"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""f30d79d2-3ec6-4f11-8178-6bb248125162"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Grab"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""d65c765f-b4fa-4999-b444-f89d2170ebbf"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Run"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""6b5c4b16-7607-4f0a-ad5b-62efd798065d"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Dash"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""56a37787-c3be-4789-a219-0509391dc113"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Shoot"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""7016cc32-6ee0-4d53-b08c-02cbe6eb2c0e"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""SecondaryShoot"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""1b70a5ec-5da5-42ce-a7c0-8f54ff84052f"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Interact"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""ae461515-11b0-4cc9-b221-92ed2df4118e"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Reload"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""cf90ff3a-006b-4ae6-9594-cc2189d6f5f0"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""Pause"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""c01ffcc5-adff-4aa3-bb8d-e0139087e5ed"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""SwitchWeapon"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""6e3febaf-dba5-4b43-b4f8-3b720e7ad1be"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""SwitchCharacter"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""cde4a43f-3a08-47c8-afdf-2c5dca8d1241"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 },
		//                 {
		//                     ""name"": ""TimeControl"",
		//                     ""type"": ""Button"",
		//                     ""id"": ""c71e92f2-77ca-4de6-98ad-64b961ed706e"",
		//                     ""expectedControlType"": ""Button"",
		//                     ""processors"": """",
		//                     ""interactions"": """",
		//                     ""initialStateCheck"": false
		//                 }
		//             ],
		//             ""bindings"": [
		//                 {
		//                     ""name"": ""WASD"",
		//                     ""id"": ""ae9b37c8-3b37-4a3c-92de-a883c1eaec34"",
		//                     ""path"": ""2DVector(mode=2)"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": """",
		//                     ""action"": ""PrimaryMovement"",
		//                     ""isComposite"": true,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": ""up"",
		//                     ""id"": ""2e66d043-6153-419e-932e-0bbd8ef8df6b"",
		//                     ""path"": ""<Keyboard>/w"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""PrimaryMovement"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": true
		//                 },
		//                 {
		//                     ""name"": ""down"",
		//                     ""id"": ""f1c785e7-a3bf-4f5c-9d5a-2302a0b10975"",
		//                     ""path"": ""<Keyboard>/s"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""PrimaryMovement"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": true
		//                 },
		//                 {
		//                     ""name"": ""left"",
		//                     ""id"": ""36b9b939-b3ed-40a9-a305-7ed3251f5a9b"",
		//                     ""path"": ""<Keyboard>/a"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""PrimaryMovement"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": true
		//                 },
		//                 {
		//                     ""name"": ""right"",
		//                     ""id"": ""d7bfdd17-5246-4bde-aa16-bcb3ea0467a1"",
		//                     ""path"": ""<Keyboard>/d"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""PrimaryMovement"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": true
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""157de9f0-00a9-4767-9eca-18eb35914def"",
		//                     ""path"": ""<Gamepad>/leftStick"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""PrimaryMovement"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""8923c29f-eb06-45a4-9c25-5ed809d8fea2"",
		//                     ""path"": ""<Keyboard>/space"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Jump"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""f7f8488a-fd5a-4098-8088-d75677e01b98"",
		//                     ""path"": ""<Gamepad>/buttonSouth"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Jump"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""718dcfd4-5383-4a9f-bab4-92688fd5d743"",
		//                     ""path"": ""<Keyboard>/leftShift"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Run"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""385119bf-31ce-4c23-8780-028608d0481c"",
		//                     ""path"": ""<Gamepad>/buttonWest"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Run"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""d6b36ab1-5730-4ee6-934d-0da3fd9ad3c7"",
		//                     ""path"": ""<Gamepad>/buttonNorth"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Dash"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""44ef3905-a71f-42e8-b234-953799ede87d"",
		//                     ""path"": ""<Keyboard>/f"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Dash"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""c74dc322-5120-478a-af7a-0d716b64cfc1"",
		//                     ""path"": ""<Keyboard>/e"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Shoot"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""202fe3e6-b8da-4f61-9aa9-faaad6af7ebb"",
		//                     ""path"": ""<Mouse>/leftButton"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Shoot"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""f112453b-6f03-459b-a23e-70f29feb18bf"",
		//                     ""path"": ""<Gamepad>/rightTrigger"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Shoot"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""63bf589f-5ef3-4931-bb6e-63038c725e66"",
		//                     ""path"": ""<Keyboard>/leftAlt"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""SecondaryShoot"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""70b57a6b-cd02-4751-90a1-9027a8ed50f0"",
		//                     ""path"": ""<Keyboard>/space"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Interact"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""c93a01d5-38f1-43b3-9711-d66667df202e"",
		//                     ""path"": ""<Gamepad>/buttonSouth"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Interact"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""5149bd3a-469e-4f0d-bb3b-1f08823c3acd"",
		//                     ""path"": ""<Keyboard>/r"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Reload"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""dd9bdaac-271a-4f2a-bdd2-7e6718d1c076"",
		//                     ""path"": ""<Gamepad>/rightShoulder"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Reload"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""17af30da-1784-4cae-a214-838b1bc78a38"",
		//                     ""path"": ""<Gamepad>/start"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Pause"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""0edbe9a3-9973-478d-b677-437ae7b60275"",
		//                     ""path"": ""<Keyboard>/escape"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Pause"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""4c3ea5cb-6737-4947-ba0f-12fe1143e541"",
		//                     ""path"": ""<Gamepad>/leftShoulder"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""SwitchWeapon"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""31347280-3484-4765-8820-ab0871c9d6ad"",
		//                     ""path"": ""<Keyboard>/t"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""SwitchWeapon"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""5a170d0f-bc8a-4a8d-a2d3-31939a85f104"",
		//                     ""path"": ""<Keyboard>/p"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""SwitchCharacter"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""6fd51ad5-2a26-4828-8ad1-28016fbc2c3d"",
		//                     ""path"": ""<Keyboard>/k"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""TimeControl"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""f7d3e6ec-7190-4a72-81ec-d23f276d6b45"",
		//                     ""path"": ""<Gamepad>/buttonEast"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""TimeControl"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""2f940a20-9887-4115-a4cc-f2bcfaa3209c"",
		//                     ""path"": ""<Gamepad>/rightStick"",
		//                     ""interactions"": """",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""SecondaryMovement"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""e4614f8e-c09a-4251-a311-72807f90324e"",
		//                     ""path"": ""<Keyboard>/space"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Swim"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""a53a6351-c203-4a94-ae9c-6d53f7736aaa"",
		//                     ""path"": ""<Gamepad>/buttonSouth"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Swim"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""2ec7a94c-73bf-4232-b32e-bd131d124d5b"",
		//                     ""path"": ""<Keyboard>/space"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Glide"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""565c301f-a576-4f6e-84d1-fbee839a8ca0"",
		//                     ""path"": ""<Gamepad>/buttonSouth"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Glide"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""2c254d21-faee-4810-9b1b-5f55e4365beb"",
		//                     ""path"": ""<Keyboard>/2"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Jetpack"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""5bdec79f-0005-46e3-95a7-e9e728f0303f"",
		//                     ""path"": ""<Gamepad>/buttonWest"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Jetpack"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""a65a3042-eb6b-4d3e-b84e-aff7e4adb6b2"",
		//                     ""path"": ""<Keyboard>/v"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Fly"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""aa3528ae-3f22-41e3-ab8c-8509dcc22f7e"",
		//                     ""path"": ""<Gamepad>/select"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Fly"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""11fcd333-82b9-4634-a2e3-3ee519f53792"",
		//                     ""path"": ""<Gamepad>/rightShoulder"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Grab"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""67f12e56-4fa7-471c-99b6-cebd70a7cccc"",
		//                     ""path"": ""<Gamepad>/rightShoulder"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Grab"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""d9fae8f2-4551-45fd-a9e6-8a9b1f34964c"",
		//                     ""path"": ""<Keyboard>/e"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Throw"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""cc62f645-d609-44e3-97ea-4fa8590f4fc4"",
		//                     ""path"": ""<Gamepad>/rightShoulder"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Throw"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""934fca02-1df3-4f28-8734-fd87e09e6ccc"",
		//                     ""path"": ""<Keyboard>/3"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Keyboard"",
		//                     ""action"": ""Push"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 },
		//                 {
		//                     ""name"": """",
		//                     ""id"": ""25b5884f-a576-4725-a54c-15a48c075580"",
		//                     ""path"": ""<Gamepad>/buttonWest"",
		//                     ""interactions"": ""Press(behavior=2)"",
		//                     ""processors"": """",
		//                     ""groups"": ""Gamepad"",
		//                     ""action"": ""Push"",
		//                     ""isComposite"": false,
		//                     ""isPartOfComposite"": false
		//                 }
		//             ]
		//         }
		//     ],
		//     ""controlSchemes"": [
		//         {
		//             ""name"": ""Keyboard"",
		//             ""bindingGroup"": ""Keyboard"",
		//             ""devices"": [
		//                 {
		//                     ""devicePath"": ""<Keyboard>"",
		//                     ""isOptional"": false,
		//                     ""isOR"": false
		//                 }
		//             ]
		//         },
		//         {
		//             ""name"": ""Gamepad"",
		//             ""bindingGroup"": ""Gamepad"",
		//             ""devices"": [
		//                 {
		//                     ""devicePath"": ""<Gamepad>"",
		//                     ""isOptional"": false,
		//                     ""isOR"": false
		//                 }
		//             ]
		//         }
		//     ]
		// }");
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = InputActionAsset_FromJson_m43B2CC4FC1A031878F4E426233FA175ECAB24EB0(_stringLiteral9E48D88C3471247D01F96214CD4DB51A245D5A05, /*hidden argument*/NULL);
		__this->set_U3CassetU3Ek__BackingField_0(L_0);
		// m_PlayerControls = asset.FindActionMap("PlayerControls", throwIfNotFound: true);
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_1;
		L_1 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_2;
		L_2 = InputActionAsset_FindActionMap_m59C07E25367472F0084C661456553E21B306103C(L_1, _stringLiteral0A3EF69D14EE38BA43AC0515A8C06B877F0D161F, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_1(L_2);
		// m_PlayerControls_PrimaryMovement = m_PlayerControls.FindAction("PrimaryMovement", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_3 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_4;
		L_4 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_3, _stringLiteralFA39B819B390DDA4BFA806E31149DC99CE38040A, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_PrimaryMovement_3(L_4);
		// m_PlayerControls_SecondaryMovement = m_PlayerControls.FindAction("SecondaryMovement", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_5 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_6;
		L_6 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_5, _stringLiteral8978DD6DA21EA64066F90784F7852458B1D54CA8, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_SecondaryMovement_4(L_6);
		// m_PlayerControls_Jump = m_PlayerControls.FindAction("Jump", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_7 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_8;
		L_8 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_7, _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Jump_5(L_8);
		// m_PlayerControls_Swim = m_PlayerControls.FindAction("Swim", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_9 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_10;
		L_10 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_9, _stringLiteralF60A604A8904268B2FCB7A84B9C54E433B6C1C27, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Swim_6(L_10);
		// m_PlayerControls_Glide = m_PlayerControls.FindAction("Glide", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_11 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_12;
		L_12 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_11, _stringLiteral7FAE997056826D296DB6568629E5D4A1BB440293, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Glide_7(L_12);
		// m_PlayerControls_Jetpack = m_PlayerControls.FindAction("Jetpack", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_13 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_14;
		L_14 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_13, _stringLiteral8866EA3E0BC985BCCB33306B5606BA3D21875437, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Jetpack_8(L_14);
		// m_PlayerControls_Fly = m_PlayerControls.FindAction("Fly", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_15 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_16;
		L_16 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_15, _stringLiteral539886CC0D6E626CD59CCCB9A251E15C3F8DC3EE, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Fly_9(L_16);
		// m_PlayerControls_Throw = m_PlayerControls.FindAction("Throw", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_17 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_18;
		L_18 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_17, _stringLiteral2A474FBC7EC4EA2C1A762D0FB32562808DDDA351, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Throw_10(L_18);
		// m_PlayerControls_Push = m_PlayerControls.FindAction("Push", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_19 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_20;
		L_20 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_19, _stringLiteralB0EF07816378D87F80141959937F780914E0D8D3, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Push_11(L_20);
		// m_PlayerControls_Grab = m_PlayerControls.FindAction("Grab", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_21 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_22;
		L_22 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_21, _stringLiteralE17E01A6CDB454BE09B74C544A2901D6C9F990AF, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Grab_12(L_22);
		// m_PlayerControls_Run = m_PlayerControls.FindAction("Run", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_23 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_24;
		L_24 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_23, _stringLiteral052C17E9D6BC1FE3DB31F3277AED6D12A2C6BDAC, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Run_13(L_24);
		// m_PlayerControls_Dash = m_PlayerControls.FindAction("Dash", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_25 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_26;
		L_26 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_25, _stringLiteral78DB8AA40D3B780BCB7B7076042CF600254368BC, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Dash_14(L_26);
		// m_PlayerControls_Shoot = m_PlayerControls.FindAction("Shoot", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_27 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_28;
		L_28 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_27, _stringLiteral63D5B7B61C9A932F0CFFA0506AD506F2DC0E23A8, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Shoot_15(L_28);
		// m_PlayerControls_SecondaryShoot = m_PlayerControls.FindAction("SecondaryShoot", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_29 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_30;
		L_30 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_29, _stringLiteralF7B071257B0A0FEB1A12B0FD1786CBD4D6D1ADC9, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_SecondaryShoot_16(L_30);
		// m_PlayerControls_Interact = m_PlayerControls.FindAction("Interact", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_31 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_32;
		L_32 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_31, _stringLiteral6B514E260236F16463F3E75F706DCC5449640861, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Interact_17(L_32);
		// m_PlayerControls_Reload = m_PlayerControls.FindAction("Reload", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_33 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_34;
		L_34 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_33, _stringLiteralC312F88FB3C35D22BE7CDEB894CC152447D26A16, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Reload_18(L_34);
		// m_PlayerControls_Pause = m_PlayerControls.FindAction("Pause", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_35 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_36;
		L_36 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_35, _stringLiteralAAAA401E86E41E6120BB9E96B9892141CF5A81F8, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_Pause_19(L_36);
		// m_PlayerControls_SwitchWeapon = m_PlayerControls.FindAction("SwitchWeapon", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_37 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_38;
		L_38 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_37, _stringLiteral515E152AE9864F9AEE9ADB9C0487E55BF594D5DF, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_SwitchWeapon_20(L_38);
		// m_PlayerControls_SwitchCharacter = m_PlayerControls.FindAction("SwitchCharacter", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_39 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_40;
		L_40 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_39, _stringLiteralE4CD985488306FB575FF327ABBAD1D041A7BB68C, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_SwitchCharacter_21(L_40);
		// m_PlayerControls_TimeControl = m_PlayerControls.FindAction("TimeControl", throwIfNotFound: true);
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_41 = __this->get_m_PlayerControls_1();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_42;
		L_42 = InputActionMap_FindAction_mA5C4E757849677626DF1A73D18659A0E8AB9585C(L_41, _stringLiteralC4C8C3123441B0B03C3A7F20F50CBF14D5E64B63, (bool)1, /*hidden argument*/NULL);
		__this->set_m_PlayerControls_TimeControl_22(L_42);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_Dispose_m90C952B9436AFC1C6F62398DD5914D6E95152C7A (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// UnityEngine.Object.Destroy(asset);
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Nullable`1<UnityEngine.InputSystem.InputBinding> MoreMountains.CorgiEngine.CorgiEngineInputActions::get_bindingMask()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  CorgiEngineInputActions_get_bindingMask_m0E958674AF56234289D23B201645BE659B98874B (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// get => asset.bindingMask;
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  L_1;
		L_1 = InputActionAsset_get_bindingMask_m484893D9818539CC0F5EC637544941C4896B8F73_inline(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_set_bindingMask_m4A8A3B1BA8A94B56D565099EAB4E6653247BE6B5 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  ___value0, const RuntimeMethod* method)
{
	{
		// set => asset.bindingMask = value;
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  L_1 = ___value0;
		InputActionAsset_set_bindingMask_mF5DEF2BD9AE6CBE2C9497248F61389747D013D22(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> MoreMountains.CorgiEngine.CorgiEngineInputActions::get_devices()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Nullable_1_t989E006C949E739D03D77F64841F9E3503807634  CorgiEngineInputActions_get_devices_m945DF92FF538C31F28B32D538847463342A8167B (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// get => asset.devices;
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		Nullable_1_t989E006C949E739D03D77F64841F9E3503807634  L_1;
		L_1 = InputActionAsset_get_devices_mE0ABE391C146F682CDC30FD6313F7960FBC9F66A(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_set_devices_mC4672305DD9C9104183233DCD961F296AE3DFECA (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, Nullable_1_t989E006C949E739D03D77F64841F9E3503807634  ___value0, const RuntimeMethod* method)
{
	{
		// set => asset.devices = value;
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		Nullable_1_t989E006C949E739D03D77F64841F9E3503807634  L_1 = ___value0;
		InputActionAsset_set_devices_m44A67138E55F9F6CDAEDC9777810980EC66D741D(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> MoreMountains.CorgiEngine.CorgiEngineInputActions::get_controlSchemes()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  CorgiEngineInputActions_get_controlSchemes_m329BD21855FBC1D1350426F6CF9C6DEE72119876 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  L_1;
		L_1 = InputActionAsset_get_controlSchemes_mC517DF89ED65963040002AB585D20C8E864F05F2(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean MoreMountains.CorgiEngine.CorgiEngineInputActions::Contains(UnityEngine.InputSystem.InputAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool CorgiEngineInputActions_Contains_m96DAFCA9E4A31F143669D28E1E58CF47E55F4049 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * ___action0, const RuntimeMethod* method)
{
	{
		// return asset.Contains(action);
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = ___action0;
		bool L_2;
		L_2 = InputActionAsset_Contains_mCBA5F48B7326E69CDA258C83D9BEB4FC62C99DEE(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> MoreMountains.CorgiEngine.CorgiEngineInputActions::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiEngineInputActions_GetEnumerator_mA2D997DEABD39D867A940F22ABF29AE0D2589859 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// return asset.GetEnumerator();
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		RuntimeObject* L_1;
		L_1 = InputActionAsset_GetEnumerator_m02AAE5BE032E6C5D59AD8EB5EB9390F3F46D5900(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator MoreMountains.CorgiEngine.CorgiEngineInputActions::System.Collections.IEnumerable.GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiEngineInputActions_System_Collections_IEnumerable_GetEnumerator_m4C443E85BDFAA9370DFE72AF0097AADE4AFFD6D4 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// return GetEnumerator();
		RuntimeObject* L_0;
		L_0 = CorgiEngineInputActions_GetEnumerator_mA2D997DEABD39D867A940F22ABF29AE0D2589859(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_Enable_m760257AE247847240EC655FFDF50486ED9B72CC2 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// asset.Enable();
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		InputActionAsset_Enable_m7AF0060D1FCFA6DDF7C8AC34BC4FBE6566A47080(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CorgiEngineInputActions_Disable_m046CFFA3CD3A7BE68443A3E9AF7A3431BFADAD3A (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// asset.Disable();
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		InputActionAsset_Disable_m0E95B47AAE45C952F8FD1A9669EF60A5A18F316A(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.InputBinding> MoreMountains.CorgiEngine.CorgiEngineInputActions::get_bindings()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CorgiEngineInputActions_get_bindings_mA9C9D801FA264D283488104F61D01B705E2F2124 (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// public IEnumerable<InputBinding> bindings => asset.bindings;
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		RuntimeObject* L_1;
		L_1 = InputActionAsset_get_bindings_mFF83EDE59E1250EACDDD05F02892DB2AD64EF7CD(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions::FindAction(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * CorgiEngineInputActions_FindAction_m8E5830F056C0554369C21172C2089832E779547C (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, String_t* ___actionNameOrId0, bool ___throwIfNotFound1, const RuntimeMethod* method)
{
	{
		// return asset.FindAction(actionNameOrId, throwIfNotFound);
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___actionNameOrId0;
		bool L_2 = ___throwIfNotFound1;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_3;
		L_3 = InputActionAsset_FindAction_m301109AC75A819E577658319115687D7187FA8B4(L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 MoreMountains.CorgiEngine.CorgiEngineInputActions::FindBinding(UnityEngine.InputSystem.InputBinding,UnityEngine.InputSystem.InputAction&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t CorgiEngineInputActions_FindBinding_m8C6C130DA63248BCA95DB263CA36F17B63F9EF1D (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB  ___bindingMask0, InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** ___action1, const RuntimeMethod* method)
{
	{
		// return asset.FindBinding(bindingMask, out action);
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0;
		L_0 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		InputBinding_t1A8DA234454BB5B0F1D42761789DFE6155CE0ECB  L_1 = ___bindingMask0;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B ** L_2 = ___action1;
		int32_t L_3;
		L_3 = InputActionAsset_FindBinding_m98B6FBE00F0C87F790B1EC17D0FE49BA2F20C54D(L_0, L_1, (InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B **)L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions MoreMountains.CorgiEngine.CorgiEngineInputActions::get_PlayerControls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// public PlayerControlsActions @PlayerControls => new PlayerControlsActions(this);
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_0;
		memset((&L_0), 0, sizeof(L_0));
		PlayerControlsActions__ctor_mFBF92D7C2688CBF0FA55CD8903AEF59444DED9F2_inline((&L_0), __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.InputSystem.InputControlScheme MoreMountains.CorgiEngine.CorgiEngineInputActions::get_KeyboardScheme()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  CorgiEngineInputActions_get_KeyboardScheme_m1C83C9A621187BF30152C519827B56B095B99E1E (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC068740B1398F61F47B7773BB27A4A828FA47A6F);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
		int32_t L_0 = __this->get_m_KeyboardSchemeIndex_23();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		// if (m_KeyboardSchemeIndex == -1) m_KeyboardSchemeIndex = asset.FindControlSchemeIndex("Keyboard");
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_1;
		L_1 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = InputActionAsset_FindControlSchemeIndex_mF36ECE7B40E919EE1E860CEAED159949636A02FB(L_1, _stringLiteralC068740B1398F61F47B7773BB27A4A828FA47A6F, /*hidden argument*/NULL);
		__this->set_m_KeyboardSchemeIndex_23(L_2);
	}

IL_001f:
	{
		// return asset.controlSchemes[m_KeyboardSchemeIndex];
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_3;
		L_3 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  L_4;
		L_4 = InputActionAsset_get_controlSchemes_mC517DF89ED65963040002AB585D20C8E864F05F2(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = __this->get_m_KeyboardSchemeIndex_23();
		InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  L_6;
		L_6 = ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B((ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16 *)(&V_0), L_5, /*hidden argument*/ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_RuntimeMethod_var);
		return L_6;
	}
}
// UnityEngine.InputSystem.InputControlScheme MoreMountains.CorgiEngine.CorgiEngineInputActions::get_GamepadScheme()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  CorgiEngineInputActions_get_GamepadScheme_m3CCAC92E818F79D451E5DA3357297FDD1EF541BF (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral61225B335304C6BE91EE0EC8CF385BBE9B633F17);
		s_Il2CppMethodInitialized = true;
	}
	ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
		int32_t L_0 = __this->get_m_GamepadSchemeIndex_24();
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		// if (m_GamepadSchemeIndex == -1) m_GamepadSchemeIndex = asset.FindControlSchemeIndex("Gamepad");
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_1;
		L_1 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		int32_t L_2;
		L_2 = InputActionAsset_FindControlSchemeIndex_mF36ECE7B40E919EE1E860CEAED159949636A02FB(L_1, _stringLiteral61225B335304C6BE91EE0EC8CF385BBE9B633F17, /*hidden argument*/NULL);
		__this->set_m_GamepadSchemeIndex_24(L_2);
	}

IL_001f:
	{
		// return asset.controlSchemes[m_GamepadSchemeIndex];
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_3;
		L_3 = CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline(__this, /*hidden argument*/NULL);
		ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16  L_4;
		L_4 = InputActionAsset_get_controlSchemes_mC517DF89ED65963040002AB585D20C8E864F05F2(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = __this->get_m_GamepadSchemeIndex_24();
		InputControlScheme_tEC3828C076819E78DC5620AC154211EE72F08288  L_6;
		L_6 = ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B((ReadOnlyArray_1_tB48426420F62DFA0920766E8077E1D56AE506E16 *)(&V_0), L_5, /*hidden argument*/ReadOnlyArray_1_get_Item_mC36106CD2C6FFFC7FAED6A506831C7D2CA2F2B3B_RuntimeMethod_var);
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.InputSystemManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_Start_m93B86883D32FEB9E0C060919DE4BC8B203CED29B (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, const RuntimeMethod* method)
{
	{
		// if (!_initialized)
		bool L_0 = __this->get__initialized_49();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Initialization();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.InputManager::Initialization() */, __this);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::Initialization()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_Initialization_m7E5C8724D66796947953F59279083A5BAD86654E (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_0_m46ACF796CE6662C47F5F6F7DF3F93D66458546C7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_10_mEFDA6A719B33D59A1768E47E6B3D7E618BD56F40_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_11_m6F724C37CE09DD1C52D6D5636CCA616F99F74246_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_12_m0212D2D2FA4CBB4E27D20BCBE20F389F94C0E52B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_13_m00B1CE3F5790A3F641BED8A7254629F943AC4E05_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_14_m4DD059256DE4FED79F39CF0E3A0EE07672534B01_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_15_m708B678A008CC12A1FE7A62E3133F528BCC5CEBE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_16_m0EF285474B25D28254204813684E18243A720237_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_17_m960D219A1C16790E3519F35094204360452DEAC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_18_mDB44A20E9422F4F04200E018DD1AC60474420D29_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_19_m90E4403D1EE32D1A0991ABFDC802E3144BCC6958_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_1_m2F8C9BF720B21AFFEA59C51DAB9AFDB00F1784FC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_2_m446C76D83D4632E0FF67A9592B6847E671D5080B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_3_m0FC1B7B16A1FA07223B493FFBC7EBA28B4AD20EE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_4_m8BE231372655FFED47110BA4F1FD2C669F2F5269_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_5_m13CCF14FE346DE32BA377629591B961298E42B9A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_6_m7CDD3C382E16BDAA0879403900170E3A843BFB72_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_7_mFEAC9CC38469CFB33F91332564BD32D2E6C698E6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_8_m70C3E7F0E5D68F4B1FE4BE9C596A17D9B8A57CB7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputSystemManager_U3CInitializationU3Eb__4_9_m0E5555C114C1B5F7E716B1C78E185CBC626491AB_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// base.Initialization();
		InputManager_Initialization_m1D51ADD53BEDAB595CB0819EB2EA164D0BD61C71(__this, /*hidden argument*/NULL);
		// _inputActionsEnabled = true;
		__this->set__inputActionsEnabled_48((bool)1);
		// InputActions = new CorgiEngineInputActions();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 *)il2cpp_codegen_object_new(CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151_il2cpp_TypeInfo_var);
		CorgiEngineInputActions__ctor_m70F22674BA17D5AE8E998BC611A2EC2AB2780FE6(L_0, /*hidden argument*/NULL);
		__this->set_InputActions_47(L_0);
		// InputActions.PlayerControls.PrimaryMovement.performed += context => _primaryMovement = context.ReadValue<Vector2>();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_1 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_2;
		L_2 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_3;
		L_3 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_4 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_4, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_0_m46ACF796CE6662C47F5F6F7DF3F93D66458546C7_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_3, L_4, /*hidden argument*/NULL);
		// InputActions.PlayerControls.SecondaryMovement.performed += context => _secondaryMovement = context.ReadValue<Vector2>();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_5 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_6;
		L_6 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_7;
		L_7 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_8 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_8, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_1_m2F8C9BF720B21AFFEA59C51DAB9AFDB00F1784FC_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_7, L_8, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Jump.performed += context => { BindButton(context, JumpButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_9 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_10;
		L_10 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_11;
		L_11 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_12 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_12, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_2_m446C76D83D4632E0FF67A9592B6847E671D5080B_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_11, L_12, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Run.performed += context => { BindButton(context, RunButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_13 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_14;
		L_14 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_15;
		L_15 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_16 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_16, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_3_m0FC1B7B16A1FA07223B493FFBC7EBA28B4AD20EE_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_15, L_16, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Dash.performed += context => { BindButton(context, DashButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_17 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_18;
		L_18 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_19;
		L_19 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_20 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_20, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_4_m8BE231372655FFED47110BA4F1FD2C669F2F5269_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_19, L_20, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Shoot.performed += context => { BindButton(context, ShootButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_21 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_22;
		L_22 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_23;
		L_23 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_24 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_24, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_5_m13CCF14FE346DE32BA377629591B961298E42B9A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_23, L_24, /*hidden argument*/NULL);
		// InputActions.PlayerControls.SecondaryShoot.performed += context => { BindButton(context, SecondaryShootButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_25 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_26;
		L_26 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_25, /*hidden argument*/NULL);
		V_0 = L_26;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_27;
		L_27 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_28 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_28, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_6_m7CDD3C382E16BDAA0879403900170E3A843BFB72_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_27, L_28, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Interact.performed += context => { BindButton(context, InteractButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_29 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_30;
		L_30 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_31;
		L_31 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_32 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_32, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_7_mFEAC9CC38469CFB33F91332564BD32D2E6C698E6_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_31, L_32, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Reload.performed += context => { BindButton(context, ReloadButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_33 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_34;
		L_34 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_33, /*hidden argument*/NULL);
		V_0 = L_34;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_35;
		L_35 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_36 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_36, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_8_m70C3E7F0E5D68F4B1FE4BE9C596A17D9B8A57CB7_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_35, L_36, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Pause.performed += context => { BindButton(context, PauseButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_37 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_38;
		L_38 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_37, /*hidden argument*/NULL);
		V_0 = L_38;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_39;
		L_39 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_40 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_40, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_9_m0E5555C114C1B5F7E716B1C78E185CBC626491AB_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_39, L_40, /*hidden argument*/NULL);
		// InputActions.PlayerControls.SwitchWeapon.performed += context => { BindButton(context, SwitchWeaponButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_41 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_42;
		L_42 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_41, /*hidden argument*/NULL);
		V_0 = L_42;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_43;
		L_43 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_44 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_44, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_10_mEFDA6A719B33D59A1768E47E6B3D7E618BD56F40_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_43, L_44, /*hidden argument*/NULL);
		// InputActions.PlayerControls.SwitchCharacter.performed += context => { BindButton(context, SwitchCharacterButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_45 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_46;
		L_46 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_45, /*hidden argument*/NULL);
		V_0 = L_46;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_47;
		L_47 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_48 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_48, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_11_m6F724C37CE09DD1C52D6D5636CCA616F99F74246_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_47, L_48, /*hidden argument*/NULL);
		// InputActions.PlayerControls.TimeControl.performed += context => { BindButton(context, TimeControlButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_49 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_50;
		L_50 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_49, /*hidden argument*/NULL);
		V_0 = L_50;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_51;
		L_51 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_52 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_52, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_12_m0212D2D2FA4CBB4E27D20BCBE20F389F94C0E52B_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_51, L_52, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Swim.performed += context => { BindButton(context, SwimButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_53 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_54;
		L_54 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_53, /*hidden argument*/NULL);
		V_0 = L_54;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_55;
		L_55 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_56 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_56, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_13_m00B1CE3F5790A3F641BED8A7254629F943AC4E05_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_55, L_56, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Glide.performed += context => { BindButton(context, GlideButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_57 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_58;
		L_58 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_57, /*hidden argument*/NULL);
		V_0 = L_58;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_59;
		L_59 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_60 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_60, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_14_m4DD059256DE4FED79F39CF0E3A0EE07672534B01_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_59, L_60, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Jetpack.performed += context => { BindButton(context, JetpackButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_61 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_62;
		L_62 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_61, /*hidden argument*/NULL);
		V_0 = L_62;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_63;
		L_63 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_64 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_64, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_15_m708B678A008CC12A1FE7A62E3133F528BCC5CEBE_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_63, L_64, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Fly.performed += context => { BindButton(context, FlyButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_65 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_66;
		L_66 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_65, /*hidden argument*/NULL);
		V_0 = L_66;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_67;
		L_67 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_68 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_68, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_16_m0EF285474B25D28254204813684E18243A720237_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_67, L_68, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Grab.performed += context => { BindButton(context, GrabButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_69 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_70;
		L_70 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_69, /*hidden argument*/NULL);
		V_0 = L_70;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_71;
		L_71 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_72 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_72, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_17_m960D219A1C16790E3519F35094204360452DEAC9_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_71, L_72, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Throw.performed += context => { BindButton(context, ThrowButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_73 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_74;
		L_74 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_73, /*hidden argument*/NULL);
		V_0 = L_74;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_75;
		L_75 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_76 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_76, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_18_mDB44A20E9422F4F04200E018DD1AC60474420D29_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_75, L_76, /*hidden argument*/NULL);
		// InputActions.PlayerControls.Push.performed += context => { BindButton(context, PushButton); };
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_77 = __this->get_InputActions_47();
		PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  L_78;
		L_78 = CorgiEngineInputActions_get_PlayerControls_m3C177B575A9168345E45D4FE94D5308E3B8A0A8C(L_77, /*hidden argument*/NULL);
		V_0 = L_78;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_79;
		L_79 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&V_0), /*hidden argument*/NULL);
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_80 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_80, __this, (intptr_t)((intptr_t)InputSystemManager_U3CInitializationU3Eb__4_19_m90E4403D1EE32D1A0991ABFDC802E3144BCC6958_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_79, L_80, /*hidden argument*/NULL);
		// _initialized = true;
		__this->set__initialized_49((bool)1);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_BindButton_m00C2939E6060958C20CC599DFB85182E0B1B9DED (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___imButton1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * V_0 = NULL;
	{
		// var control = context.control;
		InputControl_tEC322F9612E643CD72B7759D8542E56DE84F9713 * L_0;
		L_0 = CallbackContext_get_control_mEA6B6C22E347692F93A2422E7A9DCFCEAB310AB9((CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 *)(&___context0), /*hidden argument*/NULL);
		// if (control is ButtonControl button)
		V_0 = ((ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D *)IsInstClass((RuntimeObject*)L_0, ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D_il2cpp_TypeInfo_var));
		ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0038;
		}
	}
	{
		// if (button.wasPressedThisFrame)
		ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * L_2 = V_0;
		bool L_3;
		L_3 = ButtonControl_get_wasPressedThisFrame_m23C7AF7B7B30B8751A72B2E9A73E089D5E69AE63(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0024;
		}
	}
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonDown);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_4 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_5;
		L_5 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_4, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_5, 1);
	}

IL_0024:
	{
		// if (button.wasReleasedThisFrame)
		ButtonControl_t7C71776B1EF54267612446DAC263673FCCA25E6D * L_6 = V_0;
		bool L_7;
		L_7 = ButtonControl_get_wasReleasedThisFrame_m692D3C2E8F4A01255580EF4192C0587CC6BDFD85(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonUp);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_8 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_9;
		L_9 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_8, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_9, 3);
	}

IL_0038:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_Update_m871642345C5464E635FED5CCD1B6A797235C4235 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, const RuntimeMethod* method)
{
	{
		// if (IsMobile && _inputActionsEnabled)
		bool L_0;
		L_0 = InputManager_get_IsMobile_mD06485D9FB0011F9BB9F2F93716FD28CE192EC17_inline(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		bool L_1 = __this->get__inputActionsEnabled_48();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		// _inputActionsEnabled = false;
		__this->set__inputActionsEnabled_48((bool)0);
		// InputActions.Disable();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_2 = __this->get_InputActions_47();
		CorgiEngineInputActions_Disable_m046CFFA3CD3A7BE68443A3E9AF7A3431BFADAD3A(L_2, /*hidden argument*/NULL);
	}

IL_0022:
	{
		// if (!IsMobile && (InputDetectionActive != _inputActionsEnabled))
		bool L_3;
		L_3 = InputManager_get_IsMobile_mD06485D9FB0011F9BB9F2F93716FD28CE192EC17_inline(__this, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0065;
		}
	}
	{
		bool L_4 = ((InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA *)__this)->get_InputDetectionActive_5();
		bool L_5 = __this->get__inputActionsEnabled_48();
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0065;
		}
	}
	{
		// if (InputDetectionActive)
		bool L_6 = ((InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA *)__this)->get_InputDetectionActive_5();
		if (!L_6)
		{
			goto IL_0053;
		}
	}
	{
		// _inputActionsEnabled = true;
		__this->set__inputActionsEnabled_48((bool)1);
		// InputActions.Enable();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_7 = __this->get_InputActions_47();
		CorgiEngineInputActions_Enable_m760257AE247847240EC655FFDF50486ED9B72CC2(L_7, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0053:
	{
		// _inputActionsEnabled = false;
		__this->set__inputActionsEnabled_48((bool)0);
		// InputActions.Disable();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_8 = __this->get_InputActions_47();
		CorgiEngineInputActions_Disable_m046CFFA3CD3A7BE68443A3E9AF7A3431BFADAD3A(L_8, /*hidden argument*/NULL);
	}

IL_0065:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_OnEnable_m427C60B49E6D230A63B3A34D172C7EB91B9C68FF (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, const RuntimeMethod* method)
{
	{
		// if (!_initialized)
		bool L_0 = __this->get__initialized_49();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// Initialization();
		VirtActionInvoker0::Invoke(6 /* System.Void MoreMountains.CorgiEngine.InputManager::Initialization() */, __this);
	}

IL_000e:
	{
		// InputActions.Enable();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_1 = __this->get_InputActions_47();
		CorgiEngineInputActions_Enable_m760257AE247847240EC655FFDF50486ED9B72CC2(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_OnDisable_m05CE370FB45CA6F6F891B198B23281CB64A2F222 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, const RuntimeMethod* method)
{
	{
		// InputActions.Disable();
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_InputActions_47();
		CorgiEngineInputActions_Disable_m046CFFA3CD3A7BE68443A3E9AF7A3431BFADAD3A(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager__ctor_mB898D8C85CAB8D45600F49D60AA281989BA2C149 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, const RuntimeMethod* method)
{
	{
		// protected bool _inputActionsEnabled = true;
		__this->set__inputActionsEnabled_48((bool)1);
		InputManager__ctor_m8C9F54C63E83C19DEAB926944E5CCC11FCD2EF78(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_0(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_0_m46ACF796CE6662C47F5F6F7DF3F93D66458546C7 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// InputActions.PlayerControls.PrimaryMovement.performed += context => _primaryMovement = context.ReadValue<Vector2>();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		L_0 = CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6((CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 *)(&___context0), /*hidden argument*/CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_RuntimeMethod_var);
		((InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA *)__this)->set__primaryMovement_39(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_1(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_1_m2F8C9BF720B21AFFEA59C51DAB9AFDB00F1784FC (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// InputActions.PlayerControls.SecondaryMovement.performed += context => _secondaryMovement = context.ReadValue<Vector2>();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0;
		L_0 = CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6((CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 *)(&___context0), /*hidden argument*/CallbackContext_ReadValue_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_mA35AAE0E2D6988A42C63F226BB518F180C0961A6_RuntimeMethod_var);
		((InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA *)__this)->set__secondaryMovement_40(L_0);
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_2(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_2_m446C76D83D4632E0FF67A9592B6847E671D5080B (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Jump.performed += context => { BindButton(context, JumpButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Jump.performed += context => { BindButton(context, JumpButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_3(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_3_m0FC1B7B16A1FA07223B493FFBC7EBA28B4AD20EE (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Run.performed += context => { BindButton(context, RunButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_RunButton_m1BF0C918767106B3814D73F3550245C440035A66_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Run.performed += context => { BindButton(context, RunButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_4(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_4_m8BE231372655FFED47110BA4F1FD2C669F2F5269 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Dash.performed += context => { BindButton(context, DashButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_DashButton_m039788AFC1F68B651EF3179B1FF1739E0515225C_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Dash.performed += context => { BindButton(context, DashButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_5(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_5_m13CCF14FE346DE32BA377629591B961298E42B9A (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Shoot.performed += context => { BindButton(context, ShootButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_ShootButton_m67631ED77F704CF4C3B470F57B7649B399484B03_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Shoot.performed += context => { BindButton(context, ShootButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_6(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_6_m7CDD3C382E16BDAA0879403900170E3A843BFB72 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.SecondaryShoot.performed += context => { BindButton(context, SecondaryShootButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SecondaryShootButton_m089E50BE434BEEFD14863D19A45E8FD81099CFAA_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.SecondaryShoot.performed += context => { BindButton(context, SecondaryShootButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_7(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_7_mFEAC9CC38469CFB33F91332564BD32D2E6C698E6 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Interact.performed += context => { BindButton(context, InteractButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_InteractButton_mE19955DDD59C98044E1D1A691ED12CB20CED0E22_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Interact.performed += context => { BindButton(context, InteractButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_8(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_8_m70C3E7F0E5D68F4B1FE4BE9C596A17D9B8A57CB7 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Reload.performed += context => { BindButton(context, ReloadButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_ReloadButton_m42E2F7163F6D6D0A54BFB82AB63F5A5E9DC44E36_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Reload.performed += context => { BindButton(context, ReloadButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_9(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_9_m0E5555C114C1B5F7E716B1C78E185CBC626491AB (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Pause.performed += context => { BindButton(context, PauseButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_PauseButton_mDA714172CF94F6397EC18BBD8A9CB1535BBDC1A5_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Pause.performed += context => { BindButton(context, PauseButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_10(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_10_mEFDA6A719B33D59A1768E47E6B3D7E618BD56F40 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.SwitchWeapon.performed += context => { BindButton(context, SwitchWeaponButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SwitchWeaponButton_m0290A37CB978A5F5CA6996B3E4B2D5CA961002F2_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.SwitchWeapon.performed += context => { BindButton(context, SwitchWeaponButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_11(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_11_m6F724C37CE09DD1C52D6D5636CCA616F99F74246 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.SwitchCharacter.performed += context => { BindButton(context, SwitchCharacterButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SwitchCharacterButton_mD19BFC6172D61F4E182C03EB13D961C86CB58A70_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.SwitchCharacter.performed += context => { BindButton(context, SwitchCharacterButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_12(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_12_m0212D2D2FA4CBB4E27D20BCBE20F389F94C0E52B (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.TimeControl.performed += context => { BindButton(context, TimeControlButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_TimeControlButton_m06F1083AE3A4A9BE95C3473B859C2FE9FB208461_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.TimeControl.performed += context => { BindButton(context, TimeControlButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_13(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_13_m00B1CE3F5790A3F641BED8A7254629F943AC4E05 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Swim.performed += context => { BindButton(context, SwimButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SwimButton_m5519300F8D4DE3030F0DFA46C7B2E16D30BDBD29_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Swim.performed += context => { BindButton(context, SwimButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_14(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_14_m4DD059256DE4FED79F39CF0E3A0EE07672534B01 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Glide.performed += context => { BindButton(context, GlideButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_GlideButton_mE83F8C46DBFFCDAAE2DF29B3E6AA7662CD072CF1_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Glide.performed += context => { BindButton(context, GlideButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_15(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_15_m708B678A008CC12A1FE7A62E3133F528BCC5CEBE (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Jetpack.performed += context => { BindButton(context, JetpackButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_JetpackButton_mDD8A0EEEC4592EE09E2612A6B38097050BA4D5E1_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Jetpack.performed += context => { BindButton(context, JetpackButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_16(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_16_m0EF285474B25D28254204813684E18243A720237 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Fly.performed += context => { BindButton(context, FlyButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_FlyButton_m8FE2143D47033FF22CF4537A6EEB6373A79AB0AE_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Fly.performed += context => { BindButton(context, FlyButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_17(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_17_m960D219A1C16790E3519F35094204360452DEAC9 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Grab.performed += context => { BindButton(context, GrabButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_GrabButton_m35B3226127EA6466B0DC4DFF7DC4BF7989242BD4_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Grab.performed += context => { BindButton(context, GrabButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_18(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_18_mDB44A20E9422F4F04200E018DD1AC60474420D29 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Throw.performed += context => { BindButton(context, ThrowButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_ThrowButton_m36E076F34EE4A98E8A26E301D3D8914BB52C66EA_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Throw.performed += context => { BindButton(context, ThrowButton); };
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManager::<Initialization>b__4_19(UnityEngine.InputSystem.InputAction/CallbackContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManager_U3CInitializationU3Eb__4_19_m90E4403D1EE32D1A0991ABFDC802E3144BCC6958 (InputSystemManager_tCA2E858CEA6DBBA9302747FB4E279A4F6C5C9A80 * __this, CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  ___context0, const RuntimeMethod* method)
{
	{
		// InputActions.PlayerControls.Push.performed += context => { BindButton(context, PushButton); };
		CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58  L_0 = ___context0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_PushButton_mC45D8348FAB555B5AD423A4411B6629B42F3E8F7_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< CallbackContext_t283664C85D429CB5D52BC288B66F376B0F5E8A58 , IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManager::BindButton(UnityEngine.InputSystem.InputAction/CallbackContext,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// InputActions.PlayerControls.Push.performed += context => { BindButton(context, PushButton); };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnPrimaryMovement(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnPrimaryMovement_mA72B35B7332E6ADDBEBFCCC527AB5DFFEE2D14E6 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public void OnPrimaryMovement(InputValue value) { _primaryMovement = value.Get<Vector2>(); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328(L_0, /*hidden argument*/InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_RuntimeMethod_var);
		((InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA *)__this)->set__primaryMovement_39(L_1);
		// public void OnPrimaryMovement(InputValue value) { _primaryMovement = value.Get<Vector2>(); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnSecondaryMovement(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnSecondaryMovement_mF62F43BC4FA841BC1E9D514320D3B459973F6FBA (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public void OnSecondaryMovement(InputValue value) { _primaryMovement = value.Get<Vector2>(); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		L_1 = InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328(L_0, /*hidden argument*/InputValue_Get_TisVector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_m229B840E4996536536403B5EC311286E4D263328_RuntimeMethod_var);
		((InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA *)__this)->set__primaryMovement_39(L_1);
		// public void OnSecondaryMovement(InputValue value) { _primaryMovement = value.Get<Vector2>(); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnJump(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnJump_mEB64ED4B2B651292E7DBC25CD0518A5568709544 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnJump(InputValue value) { BindButton(value, JumpButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnJump(InputValue value) { BindButton(value, JumpButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnRun(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnRun_m78046CFD6DF8025E2544BD9EE6B4C3E86372406E (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnRun(InputValue value) { BindButton(value, RunButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_RunButton_m1BF0C918767106B3814D73F3550245C440035A66_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnRun(InputValue value) { BindButton(value, RunButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnDash(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnDash_mD6D2C2019F34472AC7ECD2523D044C86604B6D86 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnDash(InputValue value) { BindButton(value, DashButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_DashButton_m039788AFC1F68B651EF3179B1FF1739E0515225C_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnDash(InputValue value) { BindButton(value, DashButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnShoot(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnShoot_m4A2FBFC6D6D665276F675E6E074A9C5ED0E714BE (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnShoot(InputValue value) { BindButton(value, ShootButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_ShootButton_m67631ED77F704CF4C3B470F57B7649B399484B03_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnShoot(InputValue value) { BindButton(value, ShootButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnSecondaryShoot(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnSecondaryShoot_m3E4B056B81BF0AA55144F5538AC46573A0A2166E (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnSecondaryShoot(InputValue value) { BindButton(value, SecondaryShootButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SecondaryShootButton_m089E50BE434BEEFD14863D19A45E8FD81099CFAA_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnSecondaryShoot(InputValue value) { BindButton(value, SecondaryShootButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnInteract(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnInteract_m3847E394B89947DC89EB38B49B0B2A08FE5B877D (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnInteract(InputValue value) { BindButton(value, InteractButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_InteractButton_mE19955DDD59C98044E1D1A691ED12CB20CED0E22_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnInteract(InputValue value) { BindButton(value, InteractButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnReload(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnReload_m8A958E56BAD10BBE7756D24B8FC3391387E57A3D (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnReload(InputValue value) { BindButton(value, ReloadButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_ReloadButton_m42E2F7163F6D6D0A54BFB82AB63F5A5E9DC44E36_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnReload(InputValue value) { BindButton(value, ReloadButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnPause(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnPause_m431D1AEE07EC28190A9E29E0AD040A6D8DD8BFD6 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnPause(InputValue value) { BindButton(value, PauseButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_PauseButton_mDA714172CF94F6397EC18BBD8A9CB1535BBDC1A5_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnPause(InputValue value) { BindButton(value, PauseButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnSwitchWeapon(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnSwitchWeapon_m7455D283EA7D3BCF8DFFEF2EB4D62289EB465A88 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnSwitchWeapon(InputValue value) { BindButton(value, SwitchWeaponButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SwitchWeaponButton_m0290A37CB978A5F5CA6996B3E4B2D5CA961002F2_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnSwitchWeapon(InputValue value) { BindButton(value, SwitchWeaponButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnSwitchCharacter(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnSwitchCharacter_m80E26ABBD1161B682FB35AC25026759E3CF1DCB4 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnSwitchCharacter(InputValue value) { BindButton(value, SwitchCharacterButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SwitchCharacterButton_mD19BFC6172D61F4E182C03EB13D961C86CB58A70_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnSwitchCharacter(InputValue value) { BindButton(value, SwitchCharacterButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnTimeControl(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnTimeControl_mB8C14C40A7894C6AD8A1E95C376295A5F994A522 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnTimeControl(InputValue value) { BindButton(value, TimeControlButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_TimeControlButton_m06F1083AE3A4A9BE95C3473B859C2FE9FB208461_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnTimeControl(InputValue value) { BindButton(value, TimeControlButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnSwim(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnSwim_mD6F2C2953B445EEFAE55AD924CCCFF7721167901 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnSwim(InputValue value) { BindButton(value, SwimButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_SwimButton_m5519300F8D4DE3030F0DFA46C7B2E16D30BDBD29_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnSwim(InputValue value) { BindButton(value, SwimButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnGlide(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnGlide_m6A560F1B67A3A4A7C4458344B7A1192CD6DAE370 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnGlide(InputValue value) { BindButton(value, GlideButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_GlideButton_mE83F8C46DBFFCDAAE2DF29B3E6AA7662CD072CF1_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnGlide(InputValue value) { BindButton(value, GlideButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnJetpack(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnJetpack_m203BDDE90D9694A02AD2597E6651610B987CFDEA (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnJetpack(InputValue value) { BindButton(value, JetpackButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_JetpackButton_mDD8A0EEEC4592EE09E2612A6B38097050BA4D5E1_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnJetpack(InputValue value) { BindButton(value, JetpackButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnFly(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnFly_m69DD5345C1BF91770544B1132D11FD0D398718A9 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnFly(InputValue value) { BindButton(value, FlyButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_FlyButton_m8FE2143D47033FF22CF4537A6EEB6373A79AB0AE_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnFly(InputValue value) { BindButton(value, FlyButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnGrab(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnGrab_m18D8E0EF8C4A821675C5B38C3C4E5534E7E29A7A (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnGrab(InputValue value) { BindButton(value, GrabButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_GrabButton_m35B3226127EA6466B0DC4DFF7DC4BF7989242BD4_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnGrab(InputValue value) { BindButton(value, GrabButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnThrow(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnThrow_m3826E00ABE62CA5EAB16BB5B47A71594B9F18E88 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnThrow(InputValue value) { BindButton(value, ThrowButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_ThrowButton_m36E076F34EE4A98E8A26E301D3D8914BB52C66EA_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnThrow(InputValue value) { BindButton(value, ThrowButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::OnPush(UnityEngine.InputSystem.InputValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_OnPush_mD4C3D967966D069394F65EF0C3DB418FE3FB85C5 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___value0, const RuntimeMethod* method)
{
	{
		// public void OnPush(InputValue value) { BindButton(value, PushButton); }
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___value0;
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_1;
		L_1 = InputManager_get_PushButton_mC45D8348FAB555B5AD423A4411B6629B42F3E8F7_inline(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 *, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * >::Invoke(83 /* System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton) */, __this, L_0, L_1);
		// public void OnPush(InputValue value) { BindButton(value, PushButton); }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::BindButton(UnityEngine.InputSystem.InputValue,MoreMountains.Tools.MMInput/IMButton)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_BindButton_mBF41C79A1CF195AD3830C555E84130546B6D5C12 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * ___inputValue0, IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * ___imButton1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// if (inputValue.isPressed)
		InputValue_t8A11E63A2306DB9F073812CA165110D3E2AAC971 * L_0 = ___inputValue0;
		bool L_1;
		L_1 = InputValue_get_isPressed_mCD089D4B9994EA3CD1790F0CBB17447C39D2B31C(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0052;
		}
	}
	{
		// switch (imButton.State.CurrentState)
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_2 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_3;
		L_3 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_2, /*hidden argument*/NULL);
		int32_t L_4;
		L_4 = MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_inline(L_3, /*hidden argument*/MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var);
		V_0 = L_4;
		int32_t L_5 = V_0;
		switch (L_5)
		{
			case 0:
			{
				goto IL_002b;
			}
			case 1:
			{
				goto IL_0038;
			}
			case 2:
			{
				goto IL_0099;
			}
			case 3:
			{
				goto IL_0045;
			}
		}
	}
	{
		return;
	}

IL_002b:
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonDown);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_6 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_7;
		L_7 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_6, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_7, 1);
		// break;
		return;
	}

IL_0038:
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonPressed);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_8 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_9;
		L_9 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_8, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_9, 2);
		// break;
		return;
	}

IL_0045:
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonDown);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_10 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_11;
		L_11 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_10, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_11, 1);
		// break;
		return;
	}

IL_0052:
	{
		// switch (imButton.State.CurrentState)
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_12 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_13;
		L_13 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_12, /*hidden argument*/NULL);
		int32_t L_14;
		L_14 = MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_inline(L_13, /*hidden argument*/MMStateMachine_1_get_CurrentState_m1909508D79F6E958DDAAF4814869BD0252C0E89D_RuntimeMethod_var);
		V_0 = L_14;
		int32_t L_15 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)1)))
		{
			case 0:
			{
				goto IL_008d;
			}
			case 1:
			{
				goto IL_0073;
			}
			case 2:
			{
				goto IL_0080;
			}
		}
	}
	{
		return;
	}

IL_0073:
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonUp);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_16 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_17;
		L_17 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_16, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_17, 3);
		// break;
		return;
	}

IL_0080:
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.Off);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_18 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_19;
		L_19 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_18, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_19, 0);
		// break;
		return;
	}

IL_008d:
	{
		// imButton.State.ChangeState(MMInput.ButtonStates.ButtonUp);
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_20 = ___imButton1;
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_21;
		L_21 = IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline(L_20, /*hidden argument*/NULL);
		VirtActionInvoker1< int32_t >::Invoke(6 /* System.Void MoreMountains.Tools.MMStateMachine`1<MoreMountains.Tools.MMInput/ButtonStates>::ChangeState(!0) */, L_21, 3);
	}

IL_0099:
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased_Update_mC19F2D20E2AB651EE3E20A40EC0DFA4CE01DE635 (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void MoreMountains.CorgiEngine.InputSystemManagerEventsBased::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputSystemManagerEventsBased__ctor_mB8FC7179A1C7E3F6AE1A3A1BA99D9D47488D61FE (InputSystemManagerEventsBased_tFF2BBEA391F8468E8B5FE47F77EA2467C9E81A8E * __this, const RuntimeMethod* method)
{
	{
		InputManager__ctor_m8C9F54C63E83C19DEAB926944E5CCC11FCD2EF78(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
IL2CPP_EXTERN_C void PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshal_pinvoke(const PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA& unmarshaled, PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_pinvoke& marshaled)
{
	Exception_t* ___m_Wrapper_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Wrapper' of type 'PlayerControlsActions': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Wrapper_0Exception, NULL);
}
IL2CPP_EXTERN_C void PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshal_pinvoke_back(const PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_pinvoke& marshaled, PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA& unmarshaled)
{
	Exception_t* ___m_Wrapper_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Wrapper' of type 'PlayerControlsActions': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Wrapper_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
IL2CPP_EXTERN_C void PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshal_pinvoke_cleanup(PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
IL2CPP_EXTERN_C void PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshal_com(const PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA& unmarshaled, PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_com& marshaled)
{
	Exception_t* ___m_Wrapper_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Wrapper' of type 'PlayerControlsActions': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Wrapper_0Exception, NULL);
}
IL2CPP_EXTERN_C void PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshal_com_back(const PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_com& marshaled, PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA& unmarshaled)
{
	Exception_t* ___m_Wrapper_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_Wrapper' of type 'PlayerControlsActions': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_Wrapper_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions
IL2CPP_EXTERN_C void PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshal_com_cleanup(PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA_marshaled_com& marshaled)
{
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::.ctor(MoreMountains.CorgiEngine.CorgiEngineInputActions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions__ctor_mFBF92D7C2688CBF0FA55CD8903AEF59444DED9F2 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___wrapper0, const RuntimeMethod* method)
{
	{
		// public PlayerControlsActions(@CorgiEngineInputActions wrapper) { m_Wrapper = wrapper; }
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = ___wrapper0;
		__this->set_m_Wrapper_0(L_0);
		// public PlayerControlsActions(@CorgiEngineInputActions wrapper) { m_Wrapper = wrapper; }
		return;
	}
}
IL2CPP_EXTERN_C  void PlayerControlsActions__ctor_mFBF92D7C2688CBF0FA55CD8903AEF59444DED9F2_AdjustorThunk (RuntimeObject * __this, CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___wrapper0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	PlayerControlsActions__ctor_mFBF92D7C2688CBF0FA55CD8903AEF59444DED9F2_inline(_thisAdjusted, ___wrapper0, method);
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_PrimaryMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @PrimaryMovement => m_Wrapper.m_PlayerControls_PrimaryMovement;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_PrimaryMovement_3();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SecondaryMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @SecondaryMovement => m_Wrapper.m_PlayerControls_SecondaryMovement;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_SecondaryMovement_4();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Jump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Jump => m_Wrapper.m_PlayerControls_Jump;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Jump_5();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Swim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Swim => m_Wrapper.m_PlayerControls_Swim;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Swim_6();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Glide()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Glide => m_Wrapper.m_PlayerControls_Glide;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Glide_7();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Jetpack()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Jetpack => m_Wrapper.m_PlayerControls_Jetpack;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Jetpack_8();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Fly()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Fly => m_Wrapper.m_PlayerControls_Fly;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Fly_9();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Throw()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Throw => m_Wrapper.m_PlayerControls_Throw;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Throw_10();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Push()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Push => m_Wrapper.m_PlayerControls_Push;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Push_11();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Grab()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Grab => m_Wrapper.m_PlayerControls_Grab;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Grab_12();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Run()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Run => m_Wrapper.m_PlayerControls_Run;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Run_13();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Dash()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Dash => m_Wrapper.m_PlayerControls_Dash;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Dash_14();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Shoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Shoot => m_Wrapper.m_PlayerControls_Shoot;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Shoot_15();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SecondaryShoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @SecondaryShoot => m_Wrapper.m_PlayerControls_SecondaryShoot;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_SecondaryShoot_16();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Interact()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Interact => m_Wrapper.m_PlayerControls_Interact;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Interact_17();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Reload()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Reload => m_Wrapper.m_PlayerControls_Reload;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Reload_18();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_Pause()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @Pause => m_Wrapper.m_PlayerControls_Pause;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_Pause_19();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SwitchWeapon()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @SwitchWeapon => m_Wrapper.m_PlayerControls_SwitchWeapon;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_SwitchWeapon_20();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_SwitchCharacter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @SwitchCharacter => m_Wrapper.m_PlayerControls_SwitchCharacter;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_SwitchCharacter_21();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputAction MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_TimeControl()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputAction @TimeControl => m_Wrapper.m_PlayerControls_TimeControl;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_1 = L_0->get_m_PlayerControls_TimeControl_22();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * _returnValue;
	_returnValue = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionMap MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::Get()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public InputActionMap Get() { return m_Wrapper.m_PlayerControls; }
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_1 = L_0->get_m_PlayerControls_1();
		return L_1;
	}
}
IL2CPP_EXTERN_C  InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * _returnValue;
	_returnValue = PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A(_thisAdjusted, method);
	return _returnValue;
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::Enable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions_Enable_mABD445BB121D241F5A8E7A6D76E541D5CE895788 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public void Enable() { Get().Enable(); }
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_0;
		L_0 = PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		InputActionMap_Enable_mFC18A12540F92B660F3ECFA9EA87320EC3F937CA(L_0, /*hidden argument*/NULL);
		// public void Enable() { Get().Enable(); }
		return;
	}
}
IL2CPP_EXTERN_C  void PlayerControlsActions_Enable_mABD445BB121D241F5A8E7A6D76E541D5CE895788_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	PlayerControlsActions_Enable_mABD445BB121D241F5A8E7A6D76E541D5CE895788(_thisAdjusted, method);
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::Disable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions_Disable_mA3082B39EC508B89826A2ED364B5EAEED3301FE2 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public void Disable() { Get().Disable(); }
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_0;
		L_0 = PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		InputActionMap_Disable_m71364975C45CB3F319459C8597E1D69C9B8135BB(L_0, /*hidden argument*/NULL);
		// public void Disable() { Get().Disable(); }
		return;
	}
}
IL2CPP_EXTERN_C  void PlayerControlsActions_Disable_mA3082B39EC508B89826A2ED364B5EAEED3301FE2_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	PlayerControlsActions_Disable_mA3082B39EC508B89826A2ED364B5EAEED3301FE2(_thisAdjusted, method);
}
// System.Boolean MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::get_enabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PlayerControlsActions_get_enabled_m522FAB68D12100A9775C28553B0840BA9A5AC6DE (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, const RuntimeMethod* method)
{
	{
		// public bool enabled => Get().enabled;
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_0;
		L_0 = PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		bool L_1;
		L_1 = InputActionMap_get_enabled_mAED55C9A85269C56CD451BCB0B28B6AE3DA8C736(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool PlayerControlsActions_get_enabled_m522FAB68D12100A9775C28553B0840BA9A5AC6DE_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	bool _returnValue;
	_returnValue = PlayerControlsActions_get_enabled_m522FAB68D12100A9775C28553B0840BA9A5AC6DE(_thisAdjusted, method);
	return _returnValue;
}
// UnityEngine.InputSystem.InputActionMap MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::op_Implicit(MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * PlayerControlsActions_op_Implicit_mCEE32A015934F9B4199F3B5EDA9EE3424F51C0CB (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA  ___set0, const RuntimeMethod* method)
{
	{
		// public static implicit operator InputActionMap(PlayerControlsActions set) { return set.Get(); }
		InputActionMap_tFE9996183C936AFC06E7EA61A765F0291E4206F4 * L_0;
		L_0 = PlayerControlsActions_Get_m8B3B5D58F022406BE5D7F06F59D11485E76F920A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)(&___set0), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void MoreMountains.CorgiEngine.CorgiEngineInputActions/PlayerControlsActions::SetCallbacks(MoreMountains.CorgiEngine.CorgiEngineInputActions/IPlayerControlsActions)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerControlsActions_SetCallbacks_m3A8659B2D15000858F1A09EF342D0566BA15BD66 (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, RuntimeObject* ___instance0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_Wrapper.m_PlayerControlsActionsCallbackInterface != null)
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = __this->get_m_Wrapper_0();
		RuntimeObject* L_1 = L_0->get_m_PlayerControlsActionsCallbackInterface_2();
		if (!L_1)
		{
			goto IL_0808;
		}
	}
	{
		// @PrimaryMovement.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPrimaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_2;
		L_2 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_3 = __this->get_m_Wrapper_0();
		RuntimeObject* L_4 = L_3->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_5 = L_4;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_6 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_6, L_5, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_5, 0, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_2, L_6, /*hidden argument*/NULL);
		// @PrimaryMovement.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPrimaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_7;
		L_7 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_8 = __this->get_m_Wrapper_0();
		RuntimeObject* L_9 = L_8->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_10 = L_9;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_11 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_11, L_10, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_10, 0, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_7, L_11, /*hidden argument*/NULL);
		// @PrimaryMovement.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPrimaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_12;
		L_12 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_13 = __this->get_m_Wrapper_0();
		RuntimeObject* L_14 = L_13->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_15 = L_14;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_16 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_16, L_15, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_15, 0, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_12, L_16, /*hidden argument*/NULL);
		// @SecondaryMovement.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSecondaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_17;
		L_17 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_18 = __this->get_m_Wrapper_0();
		RuntimeObject* L_19 = L_18->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_20 = L_19;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_21 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_21, L_20, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_20, 1, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_17, L_21, /*hidden argument*/NULL);
		// @SecondaryMovement.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSecondaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_22;
		L_22 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_23 = __this->get_m_Wrapper_0();
		RuntimeObject* L_24 = L_23->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_25 = L_24;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_26 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_26, L_25, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_25, 1, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_22, L_26, /*hidden argument*/NULL);
		// @SecondaryMovement.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSecondaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_27;
		L_27 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_28 = __this->get_m_Wrapper_0();
		RuntimeObject* L_29 = L_28->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_30 = L_29;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_31 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_31, L_30, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_30, 1, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_27, L_31, /*hidden argument*/NULL);
		// @Jump.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJump;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_32;
		L_32 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_33 = __this->get_m_Wrapper_0();
		RuntimeObject* L_34 = L_33->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_35 = L_34;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_36 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_36, L_35, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_35, 2, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_32, L_36, /*hidden argument*/NULL);
		// @Jump.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJump;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_37;
		L_37 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_38 = __this->get_m_Wrapper_0();
		RuntimeObject* L_39 = L_38->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_40 = L_39;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_41 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_41, L_40, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_40, 2, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_37, L_41, /*hidden argument*/NULL);
		// @Jump.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJump;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_42;
		L_42 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_43 = __this->get_m_Wrapper_0();
		RuntimeObject* L_44 = L_43->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_45 = L_44;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_46 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_46, L_45, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_45, 2, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_42, L_46, /*hidden argument*/NULL);
		// @Swim.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwim;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_47;
		L_47 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_48 = __this->get_m_Wrapper_0();
		RuntimeObject* L_49 = L_48->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_50 = L_49;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_51 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_51, L_50, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_50, 3, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_47, L_51, /*hidden argument*/NULL);
		// @Swim.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwim;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_52;
		L_52 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_53 = __this->get_m_Wrapper_0();
		RuntimeObject* L_54 = L_53->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_55 = L_54;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_56 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_56, L_55, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_55, 3, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_52, L_56, /*hidden argument*/NULL);
		// @Swim.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwim;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_57;
		L_57 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_58 = __this->get_m_Wrapper_0();
		RuntimeObject* L_59 = L_58->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_60 = L_59;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_61 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_61, L_60, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_60, 3, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_57, L_61, /*hidden argument*/NULL);
		// @Glide.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnGlide;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_62;
		L_62 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_63 = __this->get_m_Wrapper_0();
		RuntimeObject* L_64 = L_63->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_65 = L_64;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_66 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_66, L_65, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_65, 4, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_62, L_66, /*hidden argument*/NULL);
		// @Glide.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnGlide;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_67;
		L_67 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_68 = __this->get_m_Wrapper_0();
		RuntimeObject* L_69 = L_68->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_70 = L_69;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_71 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_71, L_70, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_70, 4, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_67, L_71, /*hidden argument*/NULL);
		// @Glide.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnGlide;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_72;
		L_72 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_73 = __this->get_m_Wrapper_0();
		RuntimeObject* L_74 = L_73->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_75 = L_74;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_76 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_76, L_75, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_75, 4, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_72, L_76, /*hidden argument*/NULL);
		// @Jetpack.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJetpack;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_77;
		L_77 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_78 = __this->get_m_Wrapper_0();
		RuntimeObject* L_79 = L_78->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_80 = L_79;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_81 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_81, L_80, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_80, 5, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_77, L_81, /*hidden argument*/NULL);
		// @Jetpack.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJetpack;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_82;
		L_82 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_83 = __this->get_m_Wrapper_0();
		RuntimeObject* L_84 = L_83->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_85 = L_84;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_86 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_86, L_85, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_85, 5, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_82, L_86, /*hidden argument*/NULL);
		// @Jetpack.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnJetpack;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_87;
		L_87 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_88 = __this->get_m_Wrapper_0();
		RuntimeObject* L_89 = L_88->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_90 = L_89;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_91 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_91, L_90, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_90, 5, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_87, L_91, /*hidden argument*/NULL);
		// @Fly.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnFly;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_92;
		L_92 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_93 = __this->get_m_Wrapper_0();
		RuntimeObject* L_94 = L_93->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_95 = L_94;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_96 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_96, L_95, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_95, 6, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_92, L_96, /*hidden argument*/NULL);
		// @Fly.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnFly;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_97;
		L_97 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_98 = __this->get_m_Wrapper_0();
		RuntimeObject* L_99 = L_98->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_100 = L_99;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_101 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_101, L_100, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_100, 6, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_97, L_101, /*hidden argument*/NULL);
		// @Fly.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnFly;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_102;
		L_102 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_103 = __this->get_m_Wrapper_0();
		RuntimeObject* L_104 = L_103->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_105 = L_104;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_106 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_106, L_105, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_105, 6, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_102, L_106, /*hidden argument*/NULL);
		// @Throw.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnThrow;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_107;
		L_107 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_108 = __this->get_m_Wrapper_0();
		RuntimeObject* L_109 = L_108->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_110 = L_109;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_111 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_111, L_110, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_110, 7, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_107, L_111, /*hidden argument*/NULL);
		// @Throw.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnThrow;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_112;
		L_112 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_113 = __this->get_m_Wrapper_0();
		RuntimeObject* L_114 = L_113->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_115 = L_114;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_116 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_116, L_115, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_115, 7, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_112, L_116, /*hidden argument*/NULL);
		// @Throw.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnThrow;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_117;
		L_117 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_118 = __this->get_m_Wrapper_0();
		RuntimeObject* L_119 = L_118->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_120 = L_119;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_121 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_121, L_120, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_120, 7, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_117, L_121, /*hidden argument*/NULL);
		// @Push.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPush;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_122;
		L_122 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_123 = __this->get_m_Wrapper_0();
		RuntimeObject* L_124 = L_123->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_125 = L_124;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_126 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_126, L_125, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_125, 8, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_122, L_126, /*hidden argument*/NULL);
		// @Push.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPush;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_127;
		L_127 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_128 = __this->get_m_Wrapper_0();
		RuntimeObject* L_129 = L_128->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_130 = L_129;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_131 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_131, L_130, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_130, 8, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_127, L_131, /*hidden argument*/NULL);
		// @Push.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPush;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_132;
		L_132 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_133 = __this->get_m_Wrapper_0();
		RuntimeObject* L_134 = L_133->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_135 = L_134;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_136 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_136, L_135, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_135, 8, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_132, L_136, /*hidden argument*/NULL);
		// @Grab.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnGrab;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_137;
		L_137 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_138 = __this->get_m_Wrapper_0();
		RuntimeObject* L_139 = L_138->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_140 = L_139;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_141 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_141, L_140, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_140, 9, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_137, L_141, /*hidden argument*/NULL);
		// @Grab.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnGrab;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_142;
		L_142 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_143 = __this->get_m_Wrapper_0();
		RuntimeObject* L_144 = L_143->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_145 = L_144;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_146 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_146, L_145, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_145, 9, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_142, L_146, /*hidden argument*/NULL);
		// @Grab.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnGrab;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_147;
		L_147 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_148 = __this->get_m_Wrapper_0();
		RuntimeObject* L_149 = L_148->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_150 = L_149;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_151 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_151, L_150, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_150, 9, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_147, L_151, /*hidden argument*/NULL);
		// @Run.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnRun;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_152;
		L_152 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_153 = __this->get_m_Wrapper_0();
		RuntimeObject* L_154 = L_153->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_155 = L_154;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_156 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_156, L_155, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_155, 10, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_152, L_156, /*hidden argument*/NULL);
		// @Run.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnRun;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_157;
		L_157 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_158 = __this->get_m_Wrapper_0();
		RuntimeObject* L_159 = L_158->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_160 = L_159;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_161 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_161, L_160, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_160, 10, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_157, L_161, /*hidden argument*/NULL);
		// @Run.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnRun;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_162;
		L_162 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_163 = __this->get_m_Wrapper_0();
		RuntimeObject* L_164 = L_163->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_165 = L_164;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_166 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_166, L_165, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_165, 10, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_162, L_166, /*hidden argument*/NULL);
		// @Dash.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnDash;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_167;
		L_167 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_168 = __this->get_m_Wrapper_0();
		RuntimeObject* L_169 = L_168->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_170 = L_169;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_171 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_171, L_170, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_170, 11, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_167, L_171, /*hidden argument*/NULL);
		// @Dash.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnDash;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_172;
		L_172 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_173 = __this->get_m_Wrapper_0();
		RuntimeObject* L_174 = L_173->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_175 = L_174;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_176 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_176, L_175, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_175, 11, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_172, L_176, /*hidden argument*/NULL);
		// @Dash.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnDash;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_177;
		L_177 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_178 = __this->get_m_Wrapper_0();
		RuntimeObject* L_179 = L_178->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_180 = L_179;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_181 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_181, L_180, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_180, 11, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_177, L_181, /*hidden argument*/NULL);
		// @Shoot.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_182;
		L_182 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_183 = __this->get_m_Wrapper_0();
		RuntimeObject* L_184 = L_183->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_185 = L_184;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_186 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_186, L_185, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_185, 12, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_182, L_186, /*hidden argument*/NULL);
		// @Shoot.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_187;
		L_187 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_188 = __this->get_m_Wrapper_0();
		RuntimeObject* L_189 = L_188->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_190 = L_189;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_191 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_191, L_190, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_190, 12, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_187, L_191, /*hidden argument*/NULL);
		// @Shoot.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_192;
		L_192 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_193 = __this->get_m_Wrapper_0();
		RuntimeObject* L_194 = L_193->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_195 = L_194;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_196 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_196, L_195, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_195, 12, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_192, L_196, /*hidden argument*/NULL);
		// @SecondaryShoot.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSecondaryShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_197;
		L_197 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_198 = __this->get_m_Wrapper_0();
		RuntimeObject* L_199 = L_198->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_200 = L_199;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_201 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_201, L_200, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_200, 13, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_197, L_201, /*hidden argument*/NULL);
		// @SecondaryShoot.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSecondaryShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_202;
		L_202 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_203 = __this->get_m_Wrapper_0();
		RuntimeObject* L_204 = L_203->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_205 = L_204;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_206 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_206, L_205, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_205, 13, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_202, L_206, /*hidden argument*/NULL);
		// @SecondaryShoot.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSecondaryShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_207;
		L_207 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_208 = __this->get_m_Wrapper_0();
		RuntimeObject* L_209 = L_208->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_210 = L_209;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_211 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_211, L_210, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_210, 13, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_207, L_211, /*hidden argument*/NULL);
		// @Interact.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnInteract;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_212;
		L_212 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_213 = __this->get_m_Wrapper_0();
		RuntimeObject* L_214 = L_213->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_215 = L_214;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_216 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_216, L_215, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_215, 14, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_212, L_216, /*hidden argument*/NULL);
		// @Interact.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnInteract;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_217;
		L_217 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_218 = __this->get_m_Wrapper_0();
		RuntimeObject* L_219 = L_218->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_220 = L_219;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_221 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_221, L_220, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_220, 14, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_217, L_221, /*hidden argument*/NULL);
		// @Interact.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnInteract;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_222;
		L_222 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_223 = __this->get_m_Wrapper_0();
		RuntimeObject* L_224 = L_223->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_225 = L_224;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_226 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_226, L_225, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_225, 14, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_222, L_226, /*hidden argument*/NULL);
		// @Reload.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnReload;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_227;
		L_227 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_228 = __this->get_m_Wrapper_0();
		RuntimeObject* L_229 = L_228->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_230 = L_229;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_231 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_231, L_230, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_230, 15, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_227, L_231, /*hidden argument*/NULL);
		// @Reload.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnReload;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_232;
		L_232 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_233 = __this->get_m_Wrapper_0();
		RuntimeObject* L_234 = L_233->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_235 = L_234;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_236 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_236, L_235, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_235, 15, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_232, L_236, /*hidden argument*/NULL);
		// @Reload.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnReload;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_237;
		L_237 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_238 = __this->get_m_Wrapper_0();
		RuntimeObject* L_239 = L_238->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_240 = L_239;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_241 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_241, L_240, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_240, 15, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_237, L_241, /*hidden argument*/NULL);
		// @Pause.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPause;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_242;
		L_242 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_243 = __this->get_m_Wrapper_0();
		RuntimeObject* L_244 = L_243->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_245 = L_244;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_246 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_246, L_245, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_245, 16, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_242, L_246, /*hidden argument*/NULL);
		// @Pause.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPause;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_247;
		L_247 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_248 = __this->get_m_Wrapper_0();
		RuntimeObject* L_249 = L_248->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_250 = L_249;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_251 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_251, L_250, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_250, 16, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_247, L_251, /*hidden argument*/NULL);
		// @Pause.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnPause;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_252;
		L_252 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_253 = __this->get_m_Wrapper_0();
		RuntimeObject* L_254 = L_253->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_255 = L_254;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_256 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_256, L_255, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_255, 16, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_252, L_256, /*hidden argument*/NULL);
		// @SwitchWeapon.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwitchWeapon;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_257;
		L_257 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_258 = __this->get_m_Wrapper_0();
		RuntimeObject* L_259 = L_258->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_260 = L_259;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_261 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_261, L_260, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_260, 17, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_257, L_261, /*hidden argument*/NULL);
		// @SwitchWeapon.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwitchWeapon;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_262;
		L_262 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_263 = __this->get_m_Wrapper_0();
		RuntimeObject* L_264 = L_263->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_265 = L_264;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_266 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_266, L_265, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_265, 17, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_262, L_266, /*hidden argument*/NULL);
		// @SwitchWeapon.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwitchWeapon;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_267;
		L_267 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_268 = __this->get_m_Wrapper_0();
		RuntimeObject* L_269 = L_268->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_270 = L_269;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_271 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_271, L_270, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_270, 17, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_267, L_271, /*hidden argument*/NULL);
		// @SwitchCharacter.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwitchCharacter;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_272;
		L_272 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_273 = __this->get_m_Wrapper_0();
		RuntimeObject* L_274 = L_273->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_275 = L_274;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_276 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_276, L_275, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_275, 18, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_272, L_276, /*hidden argument*/NULL);
		// @SwitchCharacter.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwitchCharacter;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_277;
		L_277 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_278 = __this->get_m_Wrapper_0();
		RuntimeObject* L_279 = L_278->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_280 = L_279;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_281 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_281, L_280, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_280, 18, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_277, L_281, /*hidden argument*/NULL);
		// @SwitchCharacter.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnSwitchCharacter;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_282;
		L_282 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_283 = __this->get_m_Wrapper_0();
		RuntimeObject* L_284 = L_283->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_285 = L_284;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_286 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_286, L_285, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_285, 18, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_282, L_286, /*hidden argument*/NULL);
		// @TimeControl.started -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnTimeControl;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_287;
		L_287 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_288 = __this->get_m_Wrapper_0();
		RuntimeObject* L_289 = L_288->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_290 = L_289;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_291 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_291, L_290, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_290, 19, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_started_m59CACC49AEC3ED890E5F11687C4BBADE7DC34CAD(L_287, L_291, /*hidden argument*/NULL);
		// @TimeControl.performed -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnTimeControl;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_292;
		L_292 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_293 = __this->get_m_Wrapper_0();
		RuntimeObject* L_294 = L_293->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_295 = L_294;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_296 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_296, L_295, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_295, 19, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_performed_mAE845461CC6D9AC60CFF80F8561E1198BE41A277(L_292, L_296, /*hidden argument*/NULL);
		// @TimeControl.canceled -= m_Wrapper.m_PlayerControlsActionsCallbackInterface.OnTimeControl;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_297;
		L_297 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_298 = __this->get_m_Wrapper_0();
		RuntimeObject* L_299 = L_298->get_m_PlayerControlsActionsCallbackInterface_2();
		RuntimeObject* L_300 = L_299;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_301 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_301, L_300, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_300, 19, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_remove_canceled_mDFB997885FA96483F7560D66A60293DE56C7ADC3(L_297, L_301, /*hidden argument*/NULL);
	}

IL_0808:
	{
		// m_Wrapper.m_PlayerControlsActionsCallbackInterface = instance;
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_302 = __this->get_m_Wrapper_0();
		RuntimeObject* L_303 = ___instance0;
		L_302->set_m_PlayerControlsActionsCallbackInterface_2(L_303);
		// if (instance != null)
		RuntimeObject* L_304 = ___instance0;
		if (!L_304)
		{
			goto IL_0dba;
		}
	}
	{
		// @PrimaryMovement.started += instance.OnPrimaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_305;
		L_305 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_306 = ___instance0;
		RuntimeObject* L_307 = L_306;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_308 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_308, L_307, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_307, 0, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_305, L_308, /*hidden argument*/NULL);
		// @PrimaryMovement.performed += instance.OnPrimaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_309;
		L_309 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_310 = ___instance0;
		RuntimeObject* L_311 = L_310;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_312 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_312, L_311, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_311, 0, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_309, L_312, /*hidden argument*/NULL);
		// @PrimaryMovement.canceled += instance.OnPrimaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_313;
		L_313 = PlayerControlsActions_get_PrimaryMovement_m21B3732ABDE17E1B03C23C69581D09BBB3E233B7((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_314 = ___instance0;
		RuntimeObject* L_315 = L_314;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_316 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_316, L_315, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_315, 0, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_313, L_316, /*hidden argument*/NULL);
		// @SecondaryMovement.started += instance.OnSecondaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_317;
		L_317 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_318 = ___instance0;
		RuntimeObject* L_319 = L_318;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_320 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_320, L_319, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_319, 1, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_317, L_320, /*hidden argument*/NULL);
		// @SecondaryMovement.performed += instance.OnSecondaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_321;
		L_321 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_322 = ___instance0;
		RuntimeObject* L_323 = L_322;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_324 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_324, L_323, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_323, 1, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_321, L_324, /*hidden argument*/NULL);
		// @SecondaryMovement.canceled += instance.OnSecondaryMovement;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_325;
		L_325 = PlayerControlsActions_get_SecondaryMovement_m8E9ABBC81232149CCB3B568F237573EA6C65B667((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_326 = ___instance0;
		RuntimeObject* L_327 = L_326;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_328 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_328, L_327, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_327, 1, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_325, L_328, /*hidden argument*/NULL);
		// @Jump.started += instance.OnJump;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_329;
		L_329 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_330 = ___instance0;
		RuntimeObject* L_331 = L_330;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_332 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_332, L_331, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_331, 2, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_329, L_332, /*hidden argument*/NULL);
		// @Jump.performed += instance.OnJump;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_333;
		L_333 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_334 = ___instance0;
		RuntimeObject* L_335 = L_334;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_336 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_336, L_335, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_335, 2, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_333, L_336, /*hidden argument*/NULL);
		// @Jump.canceled += instance.OnJump;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_337;
		L_337 = PlayerControlsActions_get_Jump_m2FBC872B09C53848C262165E2D8161A4CE99DD84((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_338 = ___instance0;
		RuntimeObject* L_339 = L_338;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_340 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_340, L_339, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_339, 2, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_337, L_340, /*hidden argument*/NULL);
		// @Swim.started += instance.OnSwim;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_341;
		L_341 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_342 = ___instance0;
		RuntimeObject* L_343 = L_342;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_344 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_344, L_343, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_343, 3, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_341, L_344, /*hidden argument*/NULL);
		// @Swim.performed += instance.OnSwim;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_345;
		L_345 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_346 = ___instance0;
		RuntimeObject* L_347 = L_346;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_348 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_348, L_347, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_347, 3, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_345, L_348, /*hidden argument*/NULL);
		// @Swim.canceled += instance.OnSwim;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_349;
		L_349 = PlayerControlsActions_get_Swim_mA3CBADBE4D18B5FFD593CABD3EFB86A1984BA66B((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_350 = ___instance0;
		RuntimeObject* L_351 = L_350;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_352 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_352, L_351, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_351, 3, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_349, L_352, /*hidden argument*/NULL);
		// @Glide.started += instance.OnGlide;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_353;
		L_353 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_354 = ___instance0;
		RuntimeObject* L_355 = L_354;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_356 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_356, L_355, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_355, 4, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_353, L_356, /*hidden argument*/NULL);
		// @Glide.performed += instance.OnGlide;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_357;
		L_357 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_358 = ___instance0;
		RuntimeObject* L_359 = L_358;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_360 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_360, L_359, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_359, 4, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_357, L_360, /*hidden argument*/NULL);
		// @Glide.canceled += instance.OnGlide;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_361;
		L_361 = PlayerControlsActions_get_Glide_mEAC0076C17D918CE24F3946C06D223249E4BAE3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_362 = ___instance0;
		RuntimeObject* L_363 = L_362;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_364 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_364, L_363, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_363, 4, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_361, L_364, /*hidden argument*/NULL);
		// @Jetpack.started += instance.OnJetpack;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_365;
		L_365 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_366 = ___instance0;
		RuntimeObject* L_367 = L_366;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_368 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_368, L_367, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_367, 5, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_365, L_368, /*hidden argument*/NULL);
		// @Jetpack.performed += instance.OnJetpack;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_369;
		L_369 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_370 = ___instance0;
		RuntimeObject* L_371 = L_370;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_372 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_372, L_371, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_371, 5, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_369, L_372, /*hidden argument*/NULL);
		// @Jetpack.canceled += instance.OnJetpack;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_373;
		L_373 = PlayerControlsActions_get_Jetpack_m83D17257F4CBD2EF0EE4B9B08D8F3F5AECEEC9FF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_374 = ___instance0;
		RuntimeObject* L_375 = L_374;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_376 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_376, L_375, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_375, 5, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_373, L_376, /*hidden argument*/NULL);
		// @Fly.started += instance.OnFly;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_377;
		L_377 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_378 = ___instance0;
		RuntimeObject* L_379 = L_378;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_380 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_380, L_379, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_379, 6, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_377, L_380, /*hidden argument*/NULL);
		// @Fly.performed += instance.OnFly;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_381;
		L_381 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_382 = ___instance0;
		RuntimeObject* L_383 = L_382;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_384 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_384, L_383, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_383, 6, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_381, L_384, /*hidden argument*/NULL);
		// @Fly.canceled += instance.OnFly;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_385;
		L_385 = PlayerControlsActions_get_Fly_m87412CC6DA7DF4BE487B41FD01AF23038D6F81A1((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_386 = ___instance0;
		RuntimeObject* L_387 = L_386;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_388 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_388, L_387, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_387, 6, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_385, L_388, /*hidden argument*/NULL);
		// @Throw.started += instance.OnThrow;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_389;
		L_389 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_390 = ___instance0;
		RuntimeObject* L_391 = L_390;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_392 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_392, L_391, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_391, 7, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_389, L_392, /*hidden argument*/NULL);
		// @Throw.performed += instance.OnThrow;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_393;
		L_393 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_394 = ___instance0;
		RuntimeObject* L_395 = L_394;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_396 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_396, L_395, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_395, 7, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_393, L_396, /*hidden argument*/NULL);
		// @Throw.canceled += instance.OnThrow;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_397;
		L_397 = PlayerControlsActions_get_Throw_mE365392BC5774AF5E9ED1FC3ADA3EBF12B919E45((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_398 = ___instance0;
		RuntimeObject* L_399 = L_398;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_400 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_400, L_399, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_399, 7, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_397, L_400, /*hidden argument*/NULL);
		// @Push.started += instance.OnPush;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_401;
		L_401 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_402 = ___instance0;
		RuntimeObject* L_403 = L_402;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_404 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_404, L_403, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_403, 8, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_401, L_404, /*hidden argument*/NULL);
		// @Push.performed += instance.OnPush;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_405;
		L_405 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_406 = ___instance0;
		RuntimeObject* L_407 = L_406;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_408 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_408, L_407, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_407, 8, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_405, L_408, /*hidden argument*/NULL);
		// @Push.canceled += instance.OnPush;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_409;
		L_409 = PlayerControlsActions_get_Push_m48FDBA196A56A33287DC5B171AAF97E640309DA9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_410 = ___instance0;
		RuntimeObject* L_411 = L_410;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_412 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_412, L_411, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_411, 8, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_409, L_412, /*hidden argument*/NULL);
		// @Grab.started += instance.OnGrab;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_413;
		L_413 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_414 = ___instance0;
		RuntimeObject* L_415 = L_414;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_416 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_416, L_415, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_415, 9, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_413, L_416, /*hidden argument*/NULL);
		// @Grab.performed += instance.OnGrab;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_417;
		L_417 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_418 = ___instance0;
		RuntimeObject* L_419 = L_418;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_420 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_420, L_419, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_419, 9, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_417, L_420, /*hidden argument*/NULL);
		// @Grab.canceled += instance.OnGrab;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_421;
		L_421 = PlayerControlsActions_get_Grab_mAC45EAE5B6C66B1907114E430D5705E920E3D261((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_422 = ___instance0;
		RuntimeObject* L_423 = L_422;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_424 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_424, L_423, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_423, 9, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_421, L_424, /*hidden argument*/NULL);
		// @Run.started += instance.OnRun;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_425;
		L_425 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_426 = ___instance0;
		RuntimeObject* L_427 = L_426;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_428 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_428, L_427, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_427, 10, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_425, L_428, /*hidden argument*/NULL);
		// @Run.performed += instance.OnRun;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_429;
		L_429 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_430 = ___instance0;
		RuntimeObject* L_431 = L_430;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_432 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_432, L_431, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_431, 10, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_429, L_432, /*hidden argument*/NULL);
		// @Run.canceled += instance.OnRun;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_433;
		L_433 = PlayerControlsActions_get_Run_mEA1B59937DF664B8C0102A2C25B21F68C37C862E((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_434 = ___instance0;
		RuntimeObject* L_435 = L_434;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_436 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_436, L_435, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_435, 10, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_433, L_436, /*hidden argument*/NULL);
		// @Dash.started += instance.OnDash;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_437;
		L_437 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_438 = ___instance0;
		RuntimeObject* L_439 = L_438;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_440 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_440, L_439, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_439, 11, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_437, L_440, /*hidden argument*/NULL);
		// @Dash.performed += instance.OnDash;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_441;
		L_441 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_442 = ___instance0;
		RuntimeObject* L_443 = L_442;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_444 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_444, L_443, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_443, 11, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_441, L_444, /*hidden argument*/NULL);
		// @Dash.canceled += instance.OnDash;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_445;
		L_445 = PlayerControlsActions_get_Dash_m3EAD59855D56DE6DEB0AA961753E83945402DAB3((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_446 = ___instance0;
		RuntimeObject* L_447 = L_446;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_448 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_448, L_447, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_447, 11, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_445, L_448, /*hidden argument*/NULL);
		// @Shoot.started += instance.OnShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_449;
		L_449 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_450 = ___instance0;
		RuntimeObject* L_451 = L_450;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_452 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_452, L_451, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_451, 12, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_449, L_452, /*hidden argument*/NULL);
		// @Shoot.performed += instance.OnShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_453;
		L_453 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_454 = ___instance0;
		RuntimeObject* L_455 = L_454;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_456 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_456, L_455, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_455, 12, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_453, L_456, /*hidden argument*/NULL);
		// @Shoot.canceled += instance.OnShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_457;
		L_457 = PlayerControlsActions_get_Shoot_mDC3F80785DCA7D1223DE68EBA10A3C8D9D687C3C((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_458 = ___instance0;
		RuntimeObject* L_459 = L_458;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_460 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_460, L_459, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_459, 12, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_457, L_460, /*hidden argument*/NULL);
		// @SecondaryShoot.started += instance.OnSecondaryShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_461;
		L_461 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_462 = ___instance0;
		RuntimeObject* L_463 = L_462;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_464 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_464, L_463, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_463, 13, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_461, L_464, /*hidden argument*/NULL);
		// @SecondaryShoot.performed += instance.OnSecondaryShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_465;
		L_465 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_466 = ___instance0;
		RuntimeObject* L_467 = L_466;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_468 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_468, L_467, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_467, 13, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_465, L_468, /*hidden argument*/NULL);
		// @SecondaryShoot.canceled += instance.OnSecondaryShoot;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_469;
		L_469 = PlayerControlsActions_get_SecondaryShoot_m16D598D31AFF8C3FD9E5B1CEEBD9A16141E9B891((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_470 = ___instance0;
		RuntimeObject* L_471 = L_470;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_472 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_472, L_471, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_471, 13, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_469, L_472, /*hidden argument*/NULL);
		// @Interact.started += instance.OnInteract;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_473;
		L_473 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_474 = ___instance0;
		RuntimeObject* L_475 = L_474;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_476 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_476, L_475, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_475, 14, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_473, L_476, /*hidden argument*/NULL);
		// @Interact.performed += instance.OnInteract;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_477;
		L_477 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_478 = ___instance0;
		RuntimeObject* L_479 = L_478;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_480 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_480, L_479, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_479, 14, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_477, L_480, /*hidden argument*/NULL);
		// @Interact.canceled += instance.OnInteract;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_481;
		L_481 = PlayerControlsActions_get_Interact_m856C9390380A6AFAC12B9498D70E67F32E3CBCD9((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_482 = ___instance0;
		RuntimeObject* L_483 = L_482;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_484 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_484, L_483, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_483, 14, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_481, L_484, /*hidden argument*/NULL);
		// @Reload.started += instance.OnReload;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_485;
		L_485 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_486 = ___instance0;
		RuntimeObject* L_487 = L_486;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_488 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_488, L_487, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_487, 15, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_485, L_488, /*hidden argument*/NULL);
		// @Reload.performed += instance.OnReload;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_489;
		L_489 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_490 = ___instance0;
		RuntimeObject* L_491 = L_490;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_492 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_492, L_491, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_491, 15, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_489, L_492, /*hidden argument*/NULL);
		// @Reload.canceled += instance.OnReload;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_493;
		L_493 = PlayerControlsActions_get_Reload_m964E11A37B65F78060497643F1A66E82BACBE889((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_494 = ___instance0;
		RuntimeObject* L_495 = L_494;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_496 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_496, L_495, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_495, 15, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_493, L_496, /*hidden argument*/NULL);
		// @Pause.started += instance.OnPause;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_497;
		L_497 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_498 = ___instance0;
		RuntimeObject* L_499 = L_498;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_500 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_500, L_499, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_499, 16, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_497, L_500, /*hidden argument*/NULL);
		// @Pause.performed += instance.OnPause;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_501;
		L_501 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_502 = ___instance0;
		RuntimeObject* L_503 = L_502;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_504 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_504, L_503, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_503, 16, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_501, L_504, /*hidden argument*/NULL);
		// @Pause.canceled += instance.OnPause;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_505;
		L_505 = PlayerControlsActions_get_Pause_m9918F562B6E599025FF45745022FA18F66C3B888((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_506 = ___instance0;
		RuntimeObject* L_507 = L_506;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_508 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_508, L_507, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_507, 16, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_505, L_508, /*hidden argument*/NULL);
		// @SwitchWeapon.started += instance.OnSwitchWeapon;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_509;
		L_509 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_510 = ___instance0;
		RuntimeObject* L_511 = L_510;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_512 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_512, L_511, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_511, 17, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_509, L_512, /*hidden argument*/NULL);
		// @SwitchWeapon.performed += instance.OnSwitchWeapon;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_513;
		L_513 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_514 = ___instance0;
		RuntimeObject* L_515 = L_514;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_516 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_516, L_515, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_515, 17, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_513, L_516, /*hidden argument*/NULL);
		// @SwitchWeapon.canceled += instance.OnSwitchWeapon;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_517;
		L_517 = PlayerControlsActions_get_SwitchWeapon_m4CC289B11CFC3B9709AB9D5FAB855E2C02D6793A((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_518 = ___instance0;
		RuntimeObject* L_519 = L_518;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_520 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_520, L_519, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_519, 17, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_517, L_520, /*hidden argument*/NULL);
		// @SwitchCharacter.started += instance.OnSwitchCharacter;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_521;
		L_521 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_522 = ___instance0;
		RuntimeObject* L_523 = L_522;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_524 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_524, L_523, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_523, 18, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_521, L_524, /*hidden argument*/NULL);
		// @SwitchCharacter.performed += instance.OnSwitchCharacter;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_525;
		L_525 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_526 = ___instance0;
		RuntimeObject* L_527 = L_526;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_528 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_528, L_527, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_527, 18, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_525, L_528, /*hidden argument*/NULL);
		// @SwitchCharacter.canceled += instance.OnSwitchCharacter;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_529;
		L_529 = PlayerControlsActions_get_SwitchCharacter_m260471D30CDF89CC4159856D9771BAE9DCBF92BF((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_530 = ___instance0;
		RuntimeObject* L_531 = L_530;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_532 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_532, L_531, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_531, 18, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_529, L_532, /*hidden argument*/NULL);
		// @TimeControl.started += instance.OnTimeControl;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_533;
		L_533 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_534 = ___instance0;
		RuntimeObject* L_535 = L_534;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_536 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_536, L_535, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_535, 19, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_started_m66841543A2CA4A86602367FDA2CA639A5BA62A66(L_533, L_536, /*hidden argument*/NULL);
		// @TimeControl.performed += instance.OnTimeControl;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_537;
		L_537 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_538 = ___instance0;
		RuntimeObject* L_539 = L_538;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_540 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_540, L_539, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_539, 19, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_performed_m6631B65227CDA3727650995AF7A27CDA52C87F8D(L_537, L_540, /*hidden argument*/NULL);
		// @TimeControl.canceled += instance.OnTimeControl;
		InputAction_tFCBDEFD42E78A8ED8D62A2B2E11E133A815C8E2B * L_541;
		L_541 = PlayerControlsActions_get_TimeControl_mCBED3F3B2803D7F2DEBF0DB19805643D9EA440B5((PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *)__this, /*hidden argument*/NULL);
		RuntimeObject* L_542 = ___instance0;
		RuntimeObject* L_543 = L_542;
		Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D * L_544 = (Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D *)il2cpp_codegen_object_new(Action_1_t2793F52C58CD21CB1D4EDE26D428E75CB84A106D_il2cpp_TypeInfo_var);
		Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA(L_544, L_543, (intptr_t)((intptr_t)GetInterfaceMethodInfo(L_543, 19, IPlayerControlsActions_t1FE2F763076A888A0109B2C3E3F3028906076F7F_il2cpp_TypeInfo_var)), /*hidden argument*/Action_1__ctor_m96E5351A63A64A257A0B7CF3600EC66DF4D4E5CA_RuntimeMethod_var);
		InputAction_add_canceled_m500FE9602403A86BF288C7029906CF0B05CDABA4(L_541, L_544, /*hidden argument*/NULL);
	}

IL_0dba:
	{
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void PlayerControlsActions_SetCallbacks_m3A8659B2D15000858F1A09EF342D0566BA15BD66_AdjustorThunk (RuntimeObject * __this, RuntimeObject* ___instance0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * _thisAdjusted = reinterpret_cast<PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA *>(__this + _offset);
	PlayerControlsActions_SetCallbacks_m3A8659B2D15000858F1A09EF342D0566BA15BD66(_thisAdjusted, ___instance0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * CorgiEngineInputActions_get_asset_m9904623D0D7BA871A0D9DF015288CE5F2B2739D6_inline (CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * __this, const RuntimeMethod* method)
{
	{
		// public InputActionAsset asset { get; }
		InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * L_0 = __this->get_U3CassetU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  InputActionAsset_get_bindingMask_m484893D9818539CC0F5EC637544941C4896B8F73_inline (InputActionAsset_tCA7525A08382E0CAD006CD44B9E5A903232BC8DB * __this, const RuntimeMethod* method)
{
	{
		// get => m_BindingMask;
		Nullable_1_tB121D9400606625499C57861D5E163E0D0A69597  L_0 = __this->get_m_BindingMask_8();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void PlayerControlsActions__ctor_mFBF92D7C2688CBF0FA55CD8903AEF59444DED9F2_inline (PlayerControlsActions_t3FA15DF50D658ABDBCCBAB798D5EEB4AE81E4CBA * __this, CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * ___wrapper0, const RuntimeMethod* method)
{
	{
		// public PlayerControlsActions(@CorgiEngineInputActions wrapper) { m_Wrapper = wrapper; }
		CorgiEngineInputActions_t7BC375E56D25355C45DE4A63FAF32553A1160151 * L_0 = ___wrapper0;
		__this->set_m_Wrapper_0(L_0);
		// public PlayerControlsActions(@CorgiEngineInputActions wrapper) { m_Wrapper = wrapper; }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * IMButton_get_State_mE435BCD1901D3C2025B49DBC3BE01E66D03A3B60_inline (IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * __this, const RuntimeMethod* method)
{
	{
		// public MMStateMachine<MMInput.ButtonStates> State {get;protected set;}
		MMStateMachine_1_tD5FA8BA98200BFBD9DB8875254FECE601BD3BBBE * L_0 = __this->get_U3CStateU3Ek__BackingField_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool InputManager_get_IsMobile_mD06485D9FB0011F9BB9F2F93716FD28CE192EC17_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public bool IsMobile { get; protected set; }
		bool L_0 = __this->get_U3CIsMobileU3Ek__BackingField_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_JumpButton_m2E6B504895162A2E1F286C2E7147DF85D71A2641_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton JumpButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CJumpButtonU3Ek__BackingField_16();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_RunButton_m1BF0C918767106B3814D73F3550245C440035A66_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton RunButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CRunButtonU3Ek__BackingField_22();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_DashButton_m039788AFC1F68B651EF3179B1FF1739E0515225C_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton DashButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CDashButtonU3Ek__BackingField_23();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_ShootButton_m67631ED77F704CF4C3B470F57B7649B399484B03_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton ShootButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CShootButtonU3Ek__BackingField_27();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SecondaryShootButton_m089E50BE434BEEFD14863D19A45E8FD81099CFAA_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton SecondaryShootButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CSecondaryShootButtonU3Ek__BackingField_28();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_InteractButton_mE19955DDD59C98044E1D1A691ED12CB20CED0E22_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton InteractButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CInteractButtonU3Ek__BackingField_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_ReloadButton_m42E2F7163F6D6D0A54BFB82AB63F5A5E9DC44E36_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton ReloadButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CReloadButtonU3Ek__BackingField_29();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_PauseButton_mDA714172CF94F6397EC18BBD8A9CB1535BBDC1A5_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton PauseButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CPauseButtonU3Ek__BackingField_32();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SwitchWeaponButton_m0290A37CB978A5F5CA6996B3E4B2D5CA961002F2_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton SwitchWeaponButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CSwitchWeaponButtonU3Ek__BackingField_34();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SwitchCharacterButton_mD19BFC6172D61F4E182C03EB13D961C86CB58A70_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton SwitchCharacterButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CSwitchCharacterButtonU3Ek__BackingField_33();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_TimeControlButton_m06F1083AE3A4A9BE95C3473B859C2FE9FB208461_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton TimeControlButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CTimeControlButtonU3Ek__BackingField_35();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_SwimButton_m5519300F8D4DE3030F0DFA46C7B2E16D30BDBD29_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton SwimButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CSwimButtonU3Ek__BackingField_17();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_GlideButton_mE83F8C46DBFFCDAAE2DF29B3E6AA7662CD072CF1_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton GlideButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CGlideButtonU3Ek__BackingField_18();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_JetpackButton_mDD8A0EEEC4592EE09E2612A6B38097050BA4D5E1_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton JetpackButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CJetpackButtonU3Ek__BackingField_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_FlyButton_m8FE2143D47033FF22CF4537A6EEB6373A79AB0AE_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton FlyButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CFlyButtonU3Ek__BackingField_21();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_GrabButton_m35B3226127EA6466B0DC4DFF7DC4BF7989242BD4_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton GrabButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CGrabButtonU3Ek__BackingField_25();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_ThrowButton_m36E076F34EE4A98E8A26E301D3D8914BB52C66EA_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton ThrowButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CThrowButtonU3Ek__BackingField_26();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * InputManager_get_PushButton_mC45D8348FAB555B5AD423A4411B6629B42F3E8F7_inline (InputManager_tB8A50F14EBEC13D6E24F73F7A02D482865815BEA * __this, const RuntimeMethod* method)
{
	{
		// public MMInput.IMButton PushButton { get; protected set; }
		IMButton_t5525A97A22F6F6CDA9EE7BBDA1169D99B77D2D8B * L_0 = __this->get_U3CPushButtonU3Ek__BackingField_30();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t MMStateMachine_1_get_CurrentState_mF8651D0B592EB2D243E9374DADBFD68A0D0C3C69_gshared_inline (MMStateMachine_1_tFE387390804279A0B3664BD1CE944D7E6D7A0BDA * __this, const RuntimeMethod* method)
{
	{
		// public T CurrentState { get; protected set; }
		int32_t L_0 = (int32_t)__this->get_U3CCurrentStateU3Ek__BackingField_2();
		return (int32_t)L_0;
	}
}

# Isla Kabu OniriaWorld

Un pequeño proyecto open source para todos los públicos, hecho por alumnos de CES. Inspirado en el estilo de Tim Burton. Basado en el mundo de Oniria World.

Este proyecto necesita el motor Corgi.

https://oniria.world/comunidad_/

https://oniria-world.fandom.com/es/wiki/Wiki_Oniria_World

## Instrucciones para colaborar

1.- Clona este proyecto

2.- Crea una rama nueva

3.- Dentro de Unity, crea una carpeta para tu facción

4.- No modifiques ninguna escena que no hayas hecho tú y trabaja siempre dentro de tu carpeta

5.- Actualiza y sube tu rama

6.- Espera a que un admin la incorpore


## Ayuda al desarrollo

### Paleta de colores

La paleta más usada es:

Violeta claro  - #B3AABF

Violeta oscuro - #5C5873

Ocre - #8C8961

Amarillo muy claro - #D9D4BF

Morado muy oscuro  - #0D0D0D

Amarillo - #e9e0ba

# Primer Nivel

Degradado de verde:
#A7D9CB
#86A69D
#5D7369
#35403A
#17261D

# Segundo Nivel

Degradado de morado:
#533343
#191026
#1C1C26
#4B434B
#8C5D5D

# Tercer Nivel

Degradado de rojo:
#400E12
#F2C36B
#E0A385
#D94436
#743D3B

### Créditos

Proyecto desarrollado por:

Arte 2D:  Natalia Méndez, Carlos Olivares, Helena Gonzalez

Programación: José Escobar

Producción y Game Design: Alberto Roldan

Este juego es parte del universo Oniria World.